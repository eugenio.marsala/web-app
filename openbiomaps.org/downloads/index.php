<?php
$files = rglob("files/*");
$files = array_combine($files, array_map("filemtime", $files));
arsort($files);
$latest_file = '';
foreach($files as $k=>$f) {
    if (is_file($k)) {
        $latest_file = preg_replace("/files\//","",$k);
        break;
    }
}
function rglob($pattern, $flags = 0) {
    $files = glob($pattern, $flags); 
    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }
    return $files;
}
$sort = 'time';
$asort = '';
$tsort = '';
if (isset($_GET['sort'])) $sort = $_GET['sort'];
if ($sort == 'time') $tsort = 'selected';
else $asort = 'selected';

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>File browser</title>

	<!-- Include our stylesheet -->
	<link href="assets/css/styles.css?4" rel="stylesheet"/>

</head>
<body>

	<div class="filemanager">

		<div class="search">
			<input type="search" placeholder="Find a file.." />
		</div>

		<div class="breadcrumbs"></div>

		<ul class="data"></ul>

		<div class="nothingfound">
			<div class="nofiles"></div>
			<span>No files here.</span>
		</div>

	</div>

    <footer>
        <a class="tz" href="https://openbiomaps.org"></a>
        <div class="close">
            <div>Latest file: <a class='lastfile' href='https://openbiomaps.org/downloads/files/<?php echo $latest_file ?>'><?php echo $latest_file ?></a></div>
            <div>Sort: <a href='?sort=alphabetical' class='sortclass <?php echo $asort ?>'>alphabetical</a> | <a href='?sort=time' class='sortclass <?php echo $tsort ?>'>time</a></div>
        </div>
    </footer>

	<!-- Include our script files -->
        <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script>var sort='<?php echo $sort?>';</script>
	<script src="assets/js/script.js?1"></script>

</body>
</html>
