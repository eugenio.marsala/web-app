<?php

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once('./server_vars.php.inc');
require_once('./common_functions.php');
require_once('./init.php');

// Database connect ID
if (!$BID = PGconnectSQL(mainpage_user,mainpage_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

?>
<!DOCTYPE html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title>openbiomaps.org - <?php echo title ?></title>
    <link rel="shortcut icon" href="<?php echo $protocol.'://'.$HOST?>/img/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="<?php echo $protocol.'://'.$HOST?>/img/favicon.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-16.png">
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-57.png">
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-114.png">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-72.png">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-144.png">
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-60.png">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-120.png">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-76.png">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-152.png">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-180.png">
    <script type="text/javascript" src="<?php echo $protocol.'://'.$HOST?>/js/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo  $protocol.'://'.$HOST?>/css/blog_style.css" />
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/Font-Awesome-4.7.0/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/dropit.css" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo $protocol.'://'.$HOST?>/js/dropit.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#menu1').dropit();
            $('#menu2').dropit();
            $('#cookie-ok').click(function(){
                $.post('index.php', {'cookies_accepted':'1' },
                    function(data){
                        $("#cookie_div").hide();
                });
            });
        })
    </script>
</head>
<body>
<div id='holder'>
<div id='header'>
    <div style='width:1000px;margin-left:auto;margin-right:auto'>
    <div style='float:left'>
    <ul id='menu1' class='menu'>
        <li><a href="<?php echo $protocol.'://'.$HOST?>/index.php"><?php echo str_home ?></a></li>
        <li><a href="#"><?php echo str_projects ?></a>
            <ul>
                <li></li>
                <li><a href="<?php echo $protocol.'://'.$HOST?>/projects/"><?php echo str_lista ?></a></li>
                <li><a href="<?php echo $protocol.'://'.$HOST?>/search/"><?php echo str_search ?></a></li>
            </ul></li>
        <li><a href="<?php echo $protocol.'://'.$HOST?>/community/"><?php echo str_community ?></a></li>
        <li><a href="<?php echo $protocol.'://'.$HOST?>/documents/"><?php echo str_documents ?></a></li>
        <li><a href="<?php echo $protocol.'://'.$HOST?>/downloads/"><?php echo str_downloads ?></a></li>
        <!--<li><a href="<?php echo $protocol.'://'.$HOST?>/faq/"><?php echo str_faq ?></a></li>-->
        <li><a href="<?php echo $protocol.'://'.$HOST?>/events/"><?php echo str_events ?></a></li>
        <li><a href='<?php echo $protocol.'://'.$HOST?>/development/'><?php echo t(str_development) ?></a></li>
        <li><a href="<?php echo $protocol.'://'.$HOST?>/contributions/"><?php echo t(str_contributions) ?></a></li>
        <li><a href="<?php echo $protocol.'://'.$HOST?>/resources/"><?php echo t(str_resources) ?></a></li>
        <!--<li><a href="#">Admin</a>
            <ul class="arrow">
            <li>
                <a href="/admin/info">System Info</a>
            <li>
            <li>
                <a href="/admin/psql">Postgre SQL</a>
            </li>
            <li>
                <a href="/admin/taxon">Taxon list</a>
            </li>
            <li>
                <a href="/admin/mirror">Mirrors</a>
            </li>
            </ul>
        </li>-->
    </ul>
    </div>
    <div style='float:right'>
        <ul id='menu2' class="menu">
        <li><a href="#"><?php echo str_languages ?></a><ul>
                    <li></li>
                    <li><a href="?lang=en">in english</a></li>
                    <li><a href="?lang=hu">magyarul</a></li>
                </ul></li>
        </ul>
    </div><!--/languages-->
    </div>
</div><!--/header-->
<div id='body'><div style='width: 800px;
  display: block;
  margin-left: auto;
  margin-right: auto;
  padding-top:60px;'>
<?php
if ($load_mainpage) {
?>

<p style='text-align:center;padding:40px;background: rgb(255, 255, 255);'><img src='<?php echo $protocol.'://'.$HOST?>/img/OpenBioMaps.png'></p>
<h1><?php print title; ?></h1>
<div class='helptext'>
<?php print WELCOME_TEXT; ?>

<!--<img src="./earth.png" style="position:absolute;top:5px;left:700px;z-index:2">
<img src="./thumbtack-blue.png" style="position:absolute;top:51px;left:878px;z-index:3">
<img src="./thumbtack-green.png" style="position:absolute;top:211px;left:728px;z-index:3">-->

<!--<div style="position:absolute;top:8px;left:930px;width:5px;height:90%;background-image:url(line.png);background-repeat:repeat-y;z-index:1">&nbsp;</div>-->

<p><br></p>
<h3><?php echo features ?></h3>
    <ul>
        <li><?php echo features_1; ?></li>
        <li><?php echo features_2 ?></li>
        <li><?php echo features_3 ?></li>
        <li><?php echo features_4 ?></li>
        <li><?php echo features_5 ?></li>
        <li><?php echo features_6 ?></li>
        <li><?php echo features_8 ?></li>
        <li><?php echo features_9 ?></li>
        <li><?php echo features_10 ?></li>
        <li><?php echo features_11 ?></li>
        <li><?php echo features_12 ?></li>
    </ul>
<p>
<!--<h3><a href='<?php echo $protocol.'://'.$HOST?>/blog/'>openbiomaps blog</a></h3>-->
</p>
<br>
</div>
<?php
} //mainpage
elseif ($load_documents) {
?>
    <br><br>
    <h2><?php print documents; ?> </h2>
    <div class='helptext'>
        <h3>OpenBioMaps consortium agreement</h3>
        <p></p><br>
        <h3>Publications using OpenBioMaps databases</h3>
        <p></p><br>

        <h3><a href='https://openbiomaps.org/documents/en/api.html#pds'>PDS (api) user manual</a></h3>
        <p></p><br>
        <h3><a href='https://openbiomaps.org/documents/en/'>Main documentations</a></h3>
        <p></p><br>
        <h3><a href='https://github.com/OpenBioMaps/documentation/tree/master/user_contributed_docs'>User contributed documentations</a></h3>
        <p></p><br>
        <h3><a href='https://youtu.be/qsu-0UeC46g'>Upload interface usage: Video tutorial (balkanherps example) english</a></h3>
        <p></p><br>

        <h3><a href='https://www.youtube.com/watch?v=BknizNC8pvc&t=102s'>Upload interface usage: Video tutorial (Milvus general forms) română</a></h3>
        <p></p><br>

        <h3><a href='https://www.youtube.com/watch?v=kFnSxYp4zNM&t=33s'>Upload interface usage: Video tutorial (Milvus atlas form) română</a></h3>
        <p></p><br>

        <h3><a href='https://www.youtube.com/watch?v=NmuIdfsXYjk'>Upload interface usage: Video tutorial (Milvus nocturnal birds form) română</a></h3>
        <p></p><br>
    </div>
<?php
}
elseif ($load_blog) {
    include('bblog/index.php');
}
# Community
# 2 matching mailing lists found:
#    Bioladat - Biotikai adatok �s adatb�zisok lista
#     Biomaps - ObenBioMaps mailing list
elseif ($load_community) { ?>

    <br><br>
    <h2><?php echo str_community ?></h2>
    <div class='helptext'>
    <h3><?php echo mailinglist ?></h3>
    <a href='http://lists.openbiomaps.org/'>list info</a>
    <ul>
    <?php
    ?>
    <li>[<a href='http://openbiomaps.org/cgi-bin/mailman/listinfo/biomaps' target='_blank'>biomaps</a>] - <?php echo str_list_biomaps ?></li>
        <li>[<a href='http://openbiomaps.org/cgi-bin/mailman/listinfo/bioladat' target='_blank'>bioladat</a>] - <?php echo str_list_bioladat ?></li>
        <li>[<a href='http://lists.openbiomaps.org/cgi-bin/mailman/listinfo/consortium' target='_blank'>consortium</a>] - <?php echo str_list_consortium ?></li>
        <li>[<a href='http://lists.openbiomaps.org/cgi-bin/mailman/listinfo/management' target='_blank'>management</a>] - <?php echo str_list_management ?></li>
        <li>[<a href='http://lists.openbiomaps.org/cgi-bin/mailman/listinfo/administrator' target='_blank'>administrator</a>] - <?php echo str_list_administrator ?></li>
        <li>[<a href='http://lists.openbiomaps.org/cgi-bin/mailman/listinfo/devel' target='_blank'>devel</a>] - <?php echo str_list_devel ?></li>
<?php
?>
    </ul>
    <br>
    <h3><?php echo str_social_media ?></h3>
        <a href='https://twitter.com/openbiomaps' style=''><img src='<?php echo $protocol.'://'.$HOST?>/img/twitter_logo_blue.png' style='width:32px;vertical-align:middle'>twitter</a>
        <br>
        <a href='https://m.facebook.com/groups/606832056387788/'><img src='<?php echo $protocol.'://'.$HOST?>/img/facebook_logo.png' style='width:32px;vertical-align:middle'>OpenBioMaps Facebook Group</a>
        
    <br>
    <br>
    <h3><?php echo str_issues ?></h3>
        <a href='https://gitlab.com/groups/openbiomaps/-/issues'><img src='<?php echo $protocol.'://'.$HOST?>/img/gitlab.png' style='width:32px;vertical-align:middle'><?php echo str_gitlab_issues ?></a>

    </div>
<?php 
} 

elseif ($load_projects) { ?>
    
    <br><br>
    <h2><?php echo str_registered_projects; ?></h2>
    <div class='helptext'>

<?php
    $cmd = sprintf("SELECT project_table FROM projects LEFT JOIN project_descriptions ON (project_table=projecttable AND language=%s) ORDER BY short,project_table",quote($_SESSION['LANG']));
    $res = pg_query($BID,$cmd);
    
    $stable = array();
    $testing = array();
    $experimental = array();
    $other = array();
    while ($e = pg_fetch_assoc($res)) {

        $cmd = sprintf("SELECT project_table,f_species_column,domain,stage,short,long,language,doi,creation_date,\"Creator\" as g,running_date,licence,rum FROM projects LEFT JOIN header_names ON (f_table_name=project_table) LEFT JOIN project_descriptions ON (project_table=projecttable) WHERE project_table='{$e['project_table']}' AND language=%s ORDER BY short,project_table",quote($_SESSION['LANG']));
        $res2 = pg_query($BID,$cmd);
        if (!pg_num_rows($res2)) {
            $cmd = "SELECT project_table,f_species_column,domain,stage,short,long,language,doi,creation_date,\"Creator\" as g,running_date,licence,rum FROM projects LEFT JOIN header_names ON (f_table_name=project_table) LEFT JOIN project_descriptions ON (project_table=projecttable) WHERE project_table='{$e['project_table']}' AND language='en' ORDER BY short,project_table";
            $res2 = pg_query($BID,$cmd);
            if (!pg_num_rows($res2)) {
                $cmd = "SELECT project_table,f_species_column,domain,stage,short,long,language,doi,creation_date,\"Creator\" as g,running_date,licence,rum FROM projects LEFT JOIN header_names ON (f_table_name=project_table) LEFT JOIN project_descriptions ON (project_table=projecttable) WHERE project_table='{$e['project_table']}' LIMIT 1";
                $res2 = pg_query($BID,$cmd);
            }
        }
        $row = pg_fetch_assoc($res2);

        $licence = 'n/a';
        if ($row['licence']!='') {
            $licence = $row['licence'];
            if($licence=='ODbL') $licence = "<a href='http://opendatacommons.org/licenses/odbl/1.0/' target='_blank'>ODbL</a>";
        }

        $rum = 'n/a';
        if ($row['rum']!='') {
            $rum = "";
            preg_match('/([0+-])([0+-])([0+-])/',$row['rum'],$m);
            if($m[1]=='+')
                $rum .= '<span style="color:green;font-weight:bold">R</span>';
            elseif($m[1]=='0')
                $rum .= '<span style="color:red;font-weight:bold">R</span>';
            else
                $rum .= '<span style="color:black;font-weight:bold">R</span>';
            if($m[2]=='+')
                $rum .= '<span style="color:green;font-weight:bold">U</span>';
            elseif($m[2]=='0')
                $rum .= '<span style="color:red;font-weight:bold">U</span>';
            else
                $rum .= '<span style="color:black;font-weight:bold">U</span>';
            if($m[3]=='+')
                $rum .= '<span style="color:green;font-weight:bold">M</span>';
            elseif($m[3]=='0')
                $rum .= '<span style="color:red;font-weight:bold">M</span>';
            else
                $rum .= '<span style="color:black;font-weight:bold">M</span>';
        }
        $rum = "<div style='padding:0 2px 0 2px;border:1px solid gray;font-family:Courier New;display:inline'><a href='https://openbiomaps.org/documents/en/faq.html#what-is-the-rum' target='_blank'>$rum</a></div>";

        $icon = '';
        if ($row['stage']=='stable') {
            if ($row['domain']=='' or $row['domain']=='openbiomaps.org')
                $domain = 'openbiomaps.org'.'/projects/'.$row['project_table'];
            else {
                if (!preg_match('/^openbiomaps.org/',$row['domain']))
                    $icon = "<i class='fa fa-external-link'></i>";
                
                $domain = $row['domain'];
            }
            $stable[] = sprintf('
                <li style="padding-bottom:10px">
                    <a href="'.$protocol.'://%1$s" class="project-name">%2$s %15$s</a>
                    <p style="padding-left:10px">%3$s</p>
                    <p style="padding-left:10px">DOI: <a href="'.$protocol.'://%1$s/doi/">%4$s</a></p>
                    <div style="padding-left:10px">%5$s: %6$s<br>
                                %7$s: %8$s - %9$s: %10$s <br>
                                %11$s: %13$s<br>
                                %12$s: %14$s</div></li>',
            $domain,$row['short'],
            $row['long'],
            sprintf($row['doi']=='') ? '----' : $row['doi'],
            str_creator,$row['g'],
            str_creation_date,$row['creation_date'],str_running_date,$row['running_date'],
            str_open_content_level,str_licence,$rum,$licence,$icon);
        } else if ($row['stage']=='testing') {
            if ($row['domain']=='' or $row['domain']=='openbiomaps.org') {
                $domain = 'openbiomaps.org'.'/projects/'.$row['project_table'];
            } else {
                if (!preg_match('/^openbiomaps.org/',$row['domain']))
                    $icon = "<i class='fa fa-external-link'></i>";
                $domain = $row['domain'];
            }
            $testing[] = sprintf('<li style="padding-bottom:10px"><a href="'.$protocol.'://%1$s" class="project-name">%2$s %6$s</a><p style="padding-left:10px">%3$s</p><p style="padding-left:10px">%4$s: %5$s</li>',$domain,$row['short'],$row['long'],str_creator,$row['g'],$icon);
        } else if ($row['stage']=='experimental') {
            if ($row['domain']=='' or $row['domain']=='openbiomaps.org')
                $domain = 'openbiomaps.org'.'/projects/'.$row['project_table'];
            else
                $domain = $row['domain'];
            $experimental[] = sprintf("<li style='padding-bottom:10px'><a href='$protocol://%s' class='project-name'>%s</a><p style='padding-left:10px'>%s</p></li>",$domain,$row['short'],$row['long']);
        } else if ($row['stage']=='other') {
            if ($row['domain']=='' or $row['domain']=='openbiomaps.org')
                $domain = 'openbiomaps.org'.'/projects/'.$row['project_table'];
            else
                $domain = $row['domain'];
            $other[] = sprintf("<li style='padding-bottom:10px'><a href='$protocol://%s' class='project-name'>%s</a><p style='padding-left:10px'>%s</p></li>",$domain,$row['short'],$row['long']);
        } else {
            if ($row['domain']=='' or $row['domain']=='openbiomaps.org')
                $domain = 'openbiomaps.org'.'/projects/'.$row['project_table'];
            else
                $domain = $row['domain'];
            $other[] = sprintf("<li style='padding-bottom:10px'><a href='$protocol://%s' class='project-name'>%s</a><p style='padding-left:10px'>%s</p></li>",$domain,$row['short'],$row['long']);
        }
    }
    echo '<h3>'.str_stable.':</h3>';
    echo '<ul style="list-style-type:none">';
    print implode('',$stable);
    echo '</ul>';
    echo  '<h3>'.str_testing.":</h3>";
    echo '<ul style="list-style-type:none">';
    print implode('',$testing);
    echo '</ul>';
    echo  '<h3>'.str_experimental.":</h3>";
    echo '<ul style="list-style-type:none">';
    print implode('',$experimental);
    echo '</ul>';
    echo  '<h3>'.str_other.":</h3>";
    echo '<ul style="list-style-type:none">';
    print implode('',$other);
    echo '</ul>';

?>
    </div>
<?php
}
elseif ($load_search) { ?>

    <br><br>
    <h2><?php echo str_search.' '.str_searchindb ?></h2>
    <div class='helptext'>
    <form method='post'>
        <input style='width:300px;height:25px' name='searchfor'><input type='submit' name='' value='<?php echo str_search ?>'>
    </form>
<?php
    if (isset($_POST['searchfor'])) {
        $p = array();
        $s = quote('%'.$_POST['searchfor'].'%');

        $cmd = "SELECT project_table,f_species_column,domain,long,short,stage FROM projects 
            LEFT JOIN header_names ON (f_table_name=project_table AND f_main_table=project_table) LEFT JOIN project_descriptions ON (projecttable=project_table) WHERE (long ILIKE $s) OR (short ILIKE $s) OR project_table ILIKE $s";

        $res = pg_query($BID,$cmd);
        while ($row = pg_fetch_assoc($res)) {
            if ($row['domain']=='' or $row['domain']=='openbiomaps.org')
                $domain = 'openbiomaps.org'.'/projects/'.$row['project_table'];
            else
                $domain = $row['domain'];

            $p[] = sprintf("<li><a href='$protocol://%s'>%s</a></li>",$domain,sepLangText($row['short']));
        }
    echo '<ul>';
    print implode('',$p);
    echo '</ul>';


    }
?>
    </div>
<?php
}


elseif ($load_contact) { ?>

    <br><br>
    <h2><?php echo str_contact_openbiomaps; ?></h2>
    <div  class='helptext'>
        <h3>openbiomaps.org <?php echo str_maintainer ?></h3>
        <p style='margin:0 0 20px 20px'>
        Server maintaining:<br>
        <a href='http://openbiomaps.org/technical/'>http://openbiomaps.org/technical/</a><br>
        Site maintaining:<br>
        Miklós Bán, email: banm 'at' vocs.unideb.hu<br>

        <h3>OpenBioMaps <?php echo str_consortium ?></h3>
        <p style='margin:0 0 20px 20px'>
        email: management 'at' openbiomaps.org<br>
        <h4>consortium institutes:</h4>
            <ul>
            <li><?php echo str_unideb ?> (Hungary)</li>
            <li><?php echo str_dinpi ?> (Hungary)</li>
            <li><?php echo str_elte ?> (Hungary)</li>
            <li><?php echo str_ektf ?> (Hungary)</li>
            <li><?php echo str_wwfhu ?> (Hungary)</li>
            <li><?php echo str_milvus ?> (Romania)</li>
            <li><?php echo str_ddnpi ?> (Hungary)</li>
            </ul>
        </p>
        <h4><?php echo str_consortium.' '.str_team ?></h4>
        <p style='margin:0 0 20px 20px'>
            <div style='float:left;width:100px;height:120px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/unideb_logo.jpg' style='height:120px'></div>
            <div>
            <b>Dr. Miklós Bán</b><br>
            email: banm 'at' vocs.unideb.hu<br>
            <b>Prof. Dr. Zoltán Barta</b><br>
            address: <?php echo str_unideb ?>, Department of Evolutionary Zoology and Humanbiology - "Lendület" Behavioural Ecology Research Groupi, H-4032 Debrecen, Egyetem tér 1. (Hungary)<br>
            </div>
        <br>
            <div style='float:left;width:100px;height:160px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/dinpi_kicsi.jpg' style='height:100px'></div>
            <div>
            <b>Sándor Bérces</b><br>
            email: bercess 'at' dinpi.hu<br>
            <b>Zsófia Mocskonyi</b><br>
            email: mocskonyizs 'at' dinpi.hu<br>
            <b>Zsolt Baranyai</b><br>
            email: baranyaizs 'at' dinpi.hu<br>
            address: <?php echo str_dinpi ?>, H-1121 Budapest, Költő u. 21. (Hungary)<br>
            </div>
        <br>
            <div style='float:left;width:100px;height:120px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/ELTE_logo.gif' style='height:90px'></div>
            <div>
            <b>Dávid Ritter</b><br>
            address: <?php echo str_elte.' '.str_elteiig ?><br>
            <b>Dr. Tibor Standovár</b><br>
            email: standy 'at' caesar.elte.hu<br>
	    address: <?php echo str_elte ?> (Hungary)<br>
            </div>
        <br>
            <div style='float:left;width:100px;height:90px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/EKE-logo.jpg' style='width:100px'></div>
            <div>
            <b>Dr. Erika Pénzesné Kónya</b><br>
            email: konya 'at' ektf.hu<br>
            address: <?php echo str_ektf ?>, H-3300 Eger, Eszterházy tér 1. (Hungary)<br>
            </div>
        <br>
            <div style='float:left;width:100px;height:90px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/WWF-logo.png' style='height:90px'></div>
            <div>
            <b>Katalin Sipos</b><br>
            email: katalin.sipos 'at' wwf.hu<br>
            address: <?php echo str_wwfhu ?>, H-1141 Budapest, Álmos vezér útja 69/a (Hungary)<br>
            </div>
        <br>
            <div style='float:left;width:100px;height:90px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/MILVUS-logo.jpg' style='height:80px'></div>
            <div>
            <b>István Kovács</b><br>
            email: istvan.kovacs 'at' milvus.ro<br>
            address: <?php echo str_milvus ?>, str. Crinului nr.  22, 540343 Tîrgu Mureş (Romania)<br>
            </div>
        <br>
            <div style='float:left;width:100px;height:90px;margin-right:10px;text-align:center;'><img src='<?php echo $protocol.'://'.$HOST?>/img/DDNPI-logo.png' style='width:100px'></div>
            <div>
            <b>Ákos Gáborik</b><br>
	    email: <br>
            address: <?php echo str_ddnpi ?>,  (Hungary)<br>
            </div>
        </p>
    </div>

<?php 
}

elseif ($load_faq) { ?>

    <br><br>
    <h2><?php print faq_l; ?></h2>
    <div  class='helptext'>
        <h3><a href='?#whatisopenbiomaps'><?php echo faq_what_is_openbiomaps ?></a></h3>
        <h3><a href='?#consortium'><?php echo faq_what_is_openbiomaps_consortium ?></a></h3>
        <h3><a href='?#createdb'><?php echo faq_create_db ?></a></h3>
        <h3><a href='?#uploaddata'><?php echo faq_upload ?></a></h3>
        <h3><a href='?#savequeries'><?php echo faq_saveq ?></a></h3>
        <h3><a href='?#login'><?php echo faq_login ?></a></h3>
        <h3><a href='?#pds'><?php echo faq_pds ?></a></h3>
        <h3><a href='?#languages'><?php echo faq_international ?></a></h3>
        <h3><a href='?#compatibility'><?php echo faq_compat ?></a></h3>
        <h3><a href='?#contribute'><?php echo faq_contribut ?></a></h3>
        <h3><a href='?#pay'><?php echo faq_pay ?></a></h3>
        <h3><a href='?#servers'><?php echo faq_servers ?></a></h3>
    
        <br><br>

    <a name='whatisopenbiomaps'></a>
    <h3><?php echo faq_what_is_openbiomaps ?></h3>
    <div>
    <?php echo faq_text_9?>
    <br>
    </div>
        <br><br>

    <a name='consortium'></a>
    <h3><?php echo faq_what_is_openbiomaps_consortium ?></h3>
    <div>
    <?php echo faq_text_10?>
    <br>
    </div>
        <br><br>


    <a name='createdb'></a>
    <h3><?php echo faq_create_db ?></h3>
    <div>
    <?php echo faq_text_1?>
    <br>
    </div>
        <br><br>

    <a name='uploaddata'></a>
    <h3><?php echo faq_upload ?></h3>
    <div>
    <?php echo faq_text_2?>
    <br>
    
    </div>
        <br><br>
    
    <a name='savequeries'></a>
    <h3><?php echo faq_saveq ?></h3>
    <div>
    <?php echo faq_text_3?>
    <br>
    </div>
        <br><br>
    
    <a name='login'></a>
    <h3><?php echo faq_login ?></h3>
    <div>
    <?php echo faq_text_4?>
    <br>
    </div>
        <br><br>

    <a name='pds'></a>
    <h3><?php echo faq_pds ?></h3>
    <div>
    <?php echo faq_text_5?>
    <br>
    </div>
        <br><br>
    
    <a name='languages'></a>
    <h3><?php echo faq_international ?></h3>
    <div>
    <?php echo faq_text_6?>
    <br>
    </div>
        <br><br>
    
    <a name='compatibility'></a>
    <h3><?php echo faq_compat ?></h3>
    <div>
    <?php echo faq_text_7?>
    <br>
    </div>
        <br><br>

    <a name='contribute'></a>
    <h3><?php echo faq_contribut ?></h3>
    <div>
    <?php echo faq_text_8?>
    <br>
    </div>
        <br><br>

    <a name='pay'></a>
    <h3><?php echo faq_pay ?></h3>
    <div>
    <?php echo faq_text_11?>
    <br>
    </div>
        <br><br>

    <a name='servers'></a>
    <h3><?php echo faq_servers ?></h3>
    <div>
    <?php echo faq_text_12?>
    <br>
    </div>
        <br><br>


  </div>
<?php 
}
elseif ($load_support) { ?>

    <br><br>
    <h2><?php echo str_contributions; ?></h2>
    <div  class='helptext'>
        <h3><?php echo str_collaborations; ?></h3>
        <ul style='list-style-type:none'>
           <li style='text-align:center'><img src='<?php echo $protocol.'://'.$HOST?>/img/evolzool.png' style='width:140px;'><br>
                <?php echo str_unideb.', '.str_deevol; ?> - Debrecen, Hungary<br><a href='http://zoology.unideb.hu'>http://zoology.unideb.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/unideb_logo.jpg' style='height:90px;'><br>
                <?php echo str_unideb.', '.str_deiszk; ?> - Debrecen, Hungary<br><a href='http://iszk.unideb.hu'>http://iszk.unideb.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/dinpi_kicsi.jpg' style='height:80px;'><br>
                <?php echo str_dinpi; ?> - Budapest, Hungary<br><a href='http://dinpi.hu'>http://dinpi.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/ELTE_logo.gif' style='height:90px;'><br>
                <?php echo str_elte.', '.str_elteiig; ?> - Budapest, Hungary<br><a href='http://iig.elte.hu'>http://iig.elte.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/MILVUS-logo.jpg' style='height:80px;'><br>
                <?php echo str_milvus; ?> - Tîrgu Mureș, Romania<br><a href='http://milvus.ro'>http://milvus.ro</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/BNPI_logo.jpg' style='height:80px;'><br>
                <?php echo str_bnpi; ?> - Eger, Hungary<br><a href='http://wwf.hu'>http://bnpi.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/NYME_logo.jpg' style='height:80px;'><br>
                <?php echo str_nyme; ?> - Sopron, Hungary<br><a href='http://nyme.hu'>http://nyme.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/FHNPI_logo.jpg' style='height:80px;'><br>
                <?php echo str_fhnpi; ?> - Fertőújlak, Hungary<br><a href='http://fhnpi.hu'>http://fhnpi.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/ONPI_logo.jpg' style='height:80px;'><br>
                <?php echo str_onpi; ?> - Őriszentpéter, Hungary<br><a href='http://onpi.hu'>http://onpi.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/HNP_logo.jpg' style='height:80px;'><br>
                <?php echo str_hnpi; ?> - Debrecen, Hungary<br><a href='https://hnp.hu'>https://hnp.hu</a></li>
           <li style='text-align:center;padding-top:10px'><img src='<?php echo $protocol.'://'.$HOST?>/img/mta_lib_logo.jpg' style='height:80px;'><br>
                <?php echo str_mta_library; ?> - Budapest, Hungary<br><a href='https://konyvtar.mta.hu'>https://konyvtar.mta.hu</a></li>
        </ul>
        <br>
        <h3><?php echo str_contributor_people; ?></h3>
        <?php echo str_cont_people_text; ?>:<br>
        <ul style='list-style-type:circle'>
            <li>Dr. Kovács Zita - <?php echo str_hnpi ?></li>
            <li>Gáspár Ákos - <?php echo str_hnpi ?></li>
            <li>Ferenc Attila - <?php echo str_bnpi ?></li>
            <li>Takács Gábor - <?php echo str_fhnpi ?></li>
            <li>Dr. Szentirmai István - <?php echo str_onpi ?></li>
            <li>Prof. Székely Tamás - <?php echo str_unideb ?></li>
            <li>Gáborik Ákos - <?php echo str_ddnpi ?></li>
            <li>Bóné Gábor - <?php echo str_milvus ?></li>
            <li>Kovács István - <?php echo str_milvus ?></li>
            <li>Pénzesné Dr. Erika Kónya -  <?php echo str_ektf ?></li>
            <li>Holl András -  <?php echo str_mta_library ?></li>
            <li>Prof. Dr. Barta Zoltán - <?php echo str_unideb ?></li>
            <li>Erdei János -  <?php echo str_adatmentes ?></li>
            <li>Ritter Dávid -  <?php echo str_elte ?></li>
            <li>Ecsedi Kornél -  <?php echo str_unideb ?></li>
            <li>Dr. Bán Miklós -  <?php echo str_unideb ?></li>
            <li>Bérces Sándor -  <?php echo str_dinpi ?></li>
            <li>Sipos Katalin -  <?php echo str_wwfhu ?></li>
        </ul>
        <br>
        <h3><?php echo str_support; ?></h3>
        <ul style='list-style-type:none'> 
           <li style='margin-bottom:20px;margin-top:30px;display:inline-block'><img src='<?php echo $protocol.'://'.$HOST?>/img/buki_konyv_logo.jpg' style='float:left;padding: 0 10px 20px 0; width:120px;margin-top:-10px'><?php echo str_buki_konyv; ?><br><a href='https://buki-konyv.hu' target='_blank'>https://buki-konyv.hu</a></li>
           <li style='margin-bottom:20px;margin-top:30px;display:inline-block'><img src='<?php echo $protocol.'://'.$HOST?>/img/adatmentes_logo.png' style='float:left;padding: 0 10px 20px 0; width:120px;margin-top:-10px'><?php echo str_adatmentes; ?><br><a href='http://www.adatmentes.com' target='_blank'>http://www.adatmentes.com</a></li>
           <li style='margin-bottom:20px;display:inline-block'><img src='<?php echo $protocol.'://'.$HOST?>/img/nemzeti_kivalosag_program.png' style='float:left;padding: 0 10px 20px 0; width:100px;margin-top:-10px'>
            <?php echo str_NEP_text; ?></li>
        </ul>
        <br>
        <br>
        <h3><?php echo str_volunteers; ?></h3>
        <?php echo str_volunteer_text; ?> 
        <ul style='list-style-type:circle'>
            <li>Attila Marton<br>
            <?php echo str_unideb; ?></li>
            <li>Flóra Sebestyén<br>
            <?php echo str_unideb; ?></li>
            <li>Melinda Babits<br>
            <?php echo str_unideb; ?></li>
            <li>Dr. Mihály Földvári<br>
            <?php echo str_unideb; ?></li>
            <li>Dr. Zsolt Végvári<br>
            Hortobágy National Park Directorate</li>
            <li>Alex Váradi<br>
            <?php echo str_unideb; ?></li>
        </ul>
        <br>
    </div>
<?php 
}
elseif ($load_about) { ?>
    
    <br><br> 
    <h2><?php echo str_about_openbiomaps; ?></h2>
    <div  class='helptext'>
    <?php

        if (file_exists('about_'.$_SESSION['LANG'].'.html'))
            include('about_'.$_SESSION['LANG'].'.html');
        else 
            include('about.html');
    ?>
    </div>
<?php 
}
elseif ($load_disclaimer) { ?>

    <br><br> 
    <h2><?php echo disclaimer ?> </h2>
    <?php 
        if (file_exists('disclaimer_'.$_SESSION['LANG'].'.html'))
            include('./disclaimer_'.$_SESSION['LANG'].'.html');
        else
            include('./disclaimer.html');
        ?>    

    <?php 
}
elseif ($load_devel) { 

    /*$f = glob("downloads/debian/pool/main/o/obm-gekko-update/*.deb");
    $m = array();
    //downloads/debian/pool/main/o/obm-gekko-update/obm-gekko-update_1.19_all.deb 
    if (preg_match("/_([0-9.-]+)_all\.deb$/",$f[0],$m)) {
        $app_ver = $m[1];
        $app_time = date("F d Y H:i:s.", filectime($f[0]));
    }*/
    // git tag -n
    // git describe --tags
    // git describe --long
    $app_ver = "2.2 stable";
    $app_time = "Tue Mar 27 09:56:03 2018 +0200";

    $f = glob("downloads/virtual-image/*.ova");
    $m = array();
    //downloads/debian/pool/main/o/obm-gekko-update/obm-gekko-update_1.19_all.deb 
    if (preg_match("/OpenBioMaps_(\d+\.\d+)\.ova$/",$f[0],$m)) {
        $gekko_ver = $m[1];
        $gekko_time = date("F d Y H:i:s.", filectime($f[0]));
    }
    $f = glob("mobil-app/*.apk");
    $m = array();
    $mvt = array();
    //downloads/debian/pool/main/o/obm-gekko-update/obm-gekko-update_1.19_all.deb 
    if (preg_match("/*.apk$/",$f[0],$m)) {
        $mver = $m[1];
        $mtime = date("F d Y H:i:s.", filectime($f[0]));
    }

?>

    <br><br> 
    <h2><?php echo development ?> </h2>
    <div  class='helptext'>
    <ul style='list-style-type:none'>
    <li style='margin-bottom:10px'>
	<b>mobile app v1 (development is not continued)</b><br> 
                JAVA application<br>
		&nbsp; &nbsp;	Csaba Szugyiczki</li>
    <li style='margin-bottom:10px'>
	<b>mobile app v2.1 (development is not continued)</b><br> 
                IONIC application<br>
        <a href='https://github.com/OpenBioMaps/ionic-app' target='_blank'>https://github.com/OpenBioMaps/ioninc-app</a><br>
		&nbsp; &nbsp;	Csaba Szugyiczki<br>
		&nbsp; &nbsp;	Kurják Viktória</li>

    <li style='margin-bottom:10px'>
        <b>mobile app v2.2 (development is not continued)</b><br> 
                JAVA application<br>
        <a href='https://github.com/OpenBioMaps/android-app' target='_blank'>https://github.com/OpenBioMaps/android-app</a><br>
		&nbsp; &nbsp;	Csaba Szugyiczki<br>
		&nbsp; &nbsp;	Kurják Viktória<br>
		&nbsp; &nbsp;	Csaba Pádár<br>
		&nbsp; &nbsp;	Gergő Péter</li>
    <li style='margin-bottom:10px'>

        <b>mobile app v3 (its development continues in v4)</b><br> 
                React-native application<br>
        <a href='https://github.com/OpenBioMaps/react-app' target='_blank'>https://github.com/OpenBioMaps/react-app</a><br>
		&nbsp; &nbsp;	Bene-Studió</li>

    <li style='margin-bottom:10px'>

        <b>mobile app v4 (current release)</b><br>
                React-native application<br>
        <a href='https://gitlab.com/openbiomaps/openbiomaps-mobile' target='_blank'>https://gitlab.com/openbiomaps/openbiomaps-mobile</a><br>
		&nbsp; &nbsp;	Mindtechapp</li>

    <li style='margin-bottom:10px'>
	<b>web application</b><br>
                PHP application<br>
        <a href='https://gitlab.com/openbiomaps/web-app' target='_blank'>https://gitlab.com/openbiomaps/web-app</a><br>
		&nbsp; &nbsp;	Miklós Bán<br>
		&nbsp; &nbsp;	Gábor Boné</li>

    <li style='margin-bottom:10px'>
        <b>web application custom modules</b><br>
                PHP applications<br>
        <a href='https://github.com/OpenBioMaps/custom-modules' target='_blank'>https://github.com/OpenBioMaps/custom-modules</a><br>
		&nbsp; &nbsp;	Miklós Bán<br>
		&nbsp; &nbsp;	Gábor Boné<br>
		&nbsp; &nbsp;	Attila Ferenc<br>
		<!--&nbsp; &nbsp;	Róbert Veres veresrobi18@gmail.com<br>-->
		<!--&nbsp; &nbsp;	Mircea Sarbu msarbu@intraweb.ro<br>-->
                                        </li>

    <li style='margin-bottom:10px'>
        <b>web api</b><br>
                PHP applications<br>
		&nbsp; &nbsp;	Miklós Bán</li>

    <li style='margin-bottom:10px'>
        <b>docker package</b><br>
        <a href='https://gitlab.com/openbiomaps/docker' target='_blank'>https://gitlab.com/openbiomaps/docker</a><br>
		&nbsp; &nbsp;	Ákos Gáspár</li>
    
    <li style='margin-bottom:10px'>
        <b>`Gekko` virtualbox image (development is not continued)</b><br>
		&nbsp; &nbsp;	Miklós Bán</li>

    <li style='margin-bottom:10px'>
        <b>R package</b><br>
                R application<br>
        <a href='https://github.com/OpenBioMaps/obm.r' target='_blank'>https://github.com/OpenBioMaps/obm.r</a><br>
		&nbsp; &nbsp;	Miklós Bán</li>
    </div>

<?php 
}
elseif ($load_resources) { 
    echo "<br><br><h2>".str_resources."</h2>";
?>
    <div  class='helptext'>
    <ul style='list-style-type:none'>
    <li style='margin-bottom:10px'>
        <img src='<?php echo $protocol.'://'.$HOST?>/img/github_logo.png' style='width:150px'><br>
        <a href='https://github.com/OpenBioMaps/' target='_blank'>https://github.com/OpenBioMaps/</a><br>
		</li>
    
    <li style='margin-bottom:10px'>
        <img src='<?php echo $protocol.'://'.$HOST?>/img/gitlab_logo.png' style='width:150px'><br>
        <a href='https://gitlab.com/openbiomaps/' target='_blank'>https://gitlab.com/openbiomaps/</a><br>
                </li>

    <li style='margin-bottom:10px'>
        <img src='<?php echo $protocol.'://'.$HOST?>/img/docker_logo.png' style='width:150px'><br>
        <a href='https://hub.docker.com/r/gaspara/obm-web-app' target='_blank'>https://hub.docker.com/r/gaspara/obm-web-app</a><br>
                </li>
    
    <li style='margin-bottom:10px'>
        <img src='<?php echo $protocol.'://'.$HOST?>/img/google-play_logo.png' style='width:150px'><br>
        <a href='https://play.google.com/store/apps/details?id=com.openbiomapsmobile' target='_blank'>https://play.google.com/store/apps/details?id=com.openbiomapsmobile</a>
                </li>
    </ul>
    </div>
<?php
}
elseif ($load_events) { 
    $cmd = "SELECT tipus,leiras,extract(year from idopont) as year,extract(epoch from idopont) as start,extract(epoch from idopont_vege) as end,TO_CHAR(idopont,'yyyy-mm-dd HH24:MI') AS idopont,TO_CHAR(idopont_vege,'yyyy-mm-dd HH24:MI') AS idopont_vege,hely FROM biomaps_events ORDER BY idopont DESC";
    $res = pg_query($BID,$cmd);
    $upc = 0;
    $pac = 0;
    $year = 0;
    $open_ul = 0;


    echo "<br><br><h2>".str_events."</h2>";

    while ($row = pg_fetch_assoc($res)) {

        $tipus = $row['tipus'];
        if (defined($tipus)) $tipus = constant($tipus);

        $ido = trim(preg_replace('/00:00/','',$row['idopont']));
        
        if ($row['idopont_vege'] == '')
            $ido_vege = '';
        else 
            $ido_vege = " - ".trim(preg_replace('/00:00/','',$row['idopont_vege']));

        if ($row['start'] > time()) {
            if (!$upc) {
                echo "<h3>".str_upcoming_events."</h3>";
                echo "<ul style='text-align:left'>";
                $upc++;
            }
        } else {
            if (!$pac) {
                echo "</ul><br>";
                echo "<h3>".str_past_events."</h3>";
                $pac++;
            }
        }

        if ($pac) {
            if ($year != $row['year']) {
                $year = $row['year'];
                if (!$open_ul) {
                    echo $year;
                    echo "<br><ul style='text-align:left'>";
                    $open_ul = 1;
                } else {
                    echo "</ul><br>";
                    echo $year;
                    echo "<br><ul style='text-align:left'>";
                }
            }
        } 

        echo sprintf('<li><b>%s</b> %s - %s. %s%s.</li>',$tipus,$row['leiras'],$row['hely'],$ido,$ido_vege);
        
    }
    echo "</ul>";
}
elseif ($load_privacy) { ?>

    <br><br> 
    <a href='http://openbiomaps.org/OBM_at.pdf'><h2><?php echo t(str_privacy_policy) ?></h2></a>
    <div class='helptext'>
        <?php 
        if (file_exists('at_'.$_SESSION['LANG'].'.html'))
            include('./at_'.$_SESSION['LANG'].'.html');
        else
            include('./at.html');
        ?>    
    </div>
<?php 
}

/**/
elseif ($load_useragreement) { ?>

    <br><br> 
    <a href='http://openbiomaps.org/OBM_aszf.pdf'><h2><?php echo str_user_agreement ?></h2></a>
    <div class='helptext' style='text-align:left;width:700px;white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */'>
    <?php 
        if (file_exists('aszf_'.$_SESSION['LANG'].'.html'))
            include('./aszf_'.$_SESSION['LANG'].'.html');
        else
            include('./aszf.html');
    ?>  
    </div>
<?php 
}

/**/
elseif ($load_technical) { ?>

    <br><br> 
    <h2><?php echo str_technical_info ?></h2>
    <div class='helptext'>
        <?php 
        if (file_exists('server_'.$_SESSION['LANG'].'.html'))
            include('./server_'.$_SESSION['LANG'].'.html');
        else
            include('./server.html');
        ?>    
    </div>
<?php 
}

/* Using Cookies */
elseif ($load_cookies) { ?>
    <h2><?php echo str_cookies ?></h2>
    <div class='helptext'>
        <?php 
        if (file_exists('cookies_'.$_SESSION['LANG'].'.html'))
            include('./cookies_'.$_SESSION['LANG'].'.html');
        else {
            include('./cookies.html');
        }
        ?>
    </div>
<?php
}


?>
</div>
</div>
<!--/body-->
<div id='footer'>
    <div class='grass'></div>
    <div class='footer'>
        <div style='font-size:75%;'>
            <ul style='list-style-type:none'>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/terms/'><?php echo t(str_terms) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/privacy/'><?php echo t(str_privacy_policy) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/disclaimer/'><?php echo t(str_disclaimer) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/about/'><?php echo t( str_about_openbiomaps) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/contact/'><?php echo t(str_contact_openbiomaps) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/technical/'><?php echo t(str_technical_info) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/cookies/'><?php echo t(str_cookies) ?></a></li>
            </ul>
        </div>
    </div>
    <div style='padding:10px 0px 0px 0px;
    text-align:center;
    margin:0;
    position:relative;
    background:transparent;
    display:table;
    width:100%;'>
        <div style='display:table-cell;color:#777;font-size:75%'>
            <p style="font-weight:bold"><?php echo "Openbiomaps Contributors 2020" ?></p>
            <?php echo str_ektf.', '.str_hungary;?><br>
            <?php echo str_elte.', '.str_hungary;?><br>
            <?php echo str_ddnpi.', '.str_hungary;?><br>
            <?php echo str_dinpi.', '.str_hungary;?><br>
            <?php echo str_fhnpi.', '.str_hungary;?><br>
            <?php echo str_milvus.', '.str_romania;?><br>
            <?php echo str_unideb.', '.str_hungary;?><br>
            <?php echo str_wwfhu.', '.str_hungary;?><br>
            <br>
        </div>
    </div>
     <?php
    if (!isset($_SESSION['cookies_accepted'])) {
        echo "<div id='cookie_div' style='text-align:center;position:fixed;z-index:1001;bottom:0;padding:20px 50px 20px 50px;background-color:black;color:#acacac'>";
        if ($_SESSION['LANG']!='hu') {
    ?>
        This site uses cookies to deliver our services and to offer you a better browsing experience. By using our site, you acknowledge that you have read and understand our <a href='http://openbiomaps.org/cookies/'>Cookie Policy</a>, <a href='http://openbiomaps.org/privacy/'>Privacy Policy</a>, and our <a href='http://openbiomaps.org/terms/'>Terms of Services</a>. Your use of OpenBioMaps’s Products and Services, including the OpenBioMaps Network, is subject to these policies and terms. <button class='pure-button' id='cookie-ok'>Accept</button>
    <?php
        } else {
    ?>
        Ez a weboldal Sütiket használ az oldal működésének biztosításához és a jobb felhasználói élmény biztosítása érdekében. A Sütik weboldalon történő használatával kapcsolatos részletes tájékoztatás az <a href='http://openbiomaps.org/privacy/'>Adatkezelési Tájékoztatóban</a> és a <a href='http://openbiomaps.org/cookies/'>Süti Tájékoztatóban</a> és a <a href='http://openbiomaps.org/terms/'>Felhasználói szabályzatban</a> található. <button class='pure-button' id='cookie-ok'>Rendben</button>
    <?php
        }
        echo "</div>";
    }
    ?>
   
</div><!--/footer-->
</div><!--/holder-->
</body>
</html>
