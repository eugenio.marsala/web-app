<?php
/* ***************************************************************************  /

This file contains the variable transformations and function calls for
REQUEST POST GET variables and some general variables definitions
 
****************************************************************************** */

session_start();

if (isset($_POST['cookies_accepted'])) {
    $_SESSION['cookies_accepted'] = 1;
    exit;
}

/* GET and POST variables processing  **************************************** */

if (isset($_GET['lang'])) {
    $_SESSION['LANG'] = preg_replace('/[^a-z]/i','',substr($_GET['lang'],0,2));
    unset($_GET['lang']);
}

if(!isset($_SESSION['LANG'])) {
    $browser_lang = '';
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
        $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
    if (isset($_COOKIE['obm_lang']))
        $browser_lang = $_COOKIE['obm_lang'];

    if ($browser_lang=='') {
        //project default language
        $_SESSION['LANG'] = LANG;
    } else {
        $_SESSION['LANG'] = preg_replace('/[^a-z]/i','',substr($browser_lang,0,2));
    }
}

if (file_exists(sprintf("languages/%s.php",preg_replace('/[^a-z]{2}/','',$_SESSION['LANG'])))) {
    include_once(sprintf("languages/%s.php",preg_replace('/[^a-z]{2}/','',$_SESSION['LANG'])));
    // include local language files
    if(file_exists(sprintf("languages/local_%s.php",preg_replace('/[^a-z]{2}/','',$_SESSION['LANG']))) and !isset($_SESSION['skip_local_lang'][$_SESSION['LANG']])) {
        include_once(sprintf("languages/local_%s.php",preg_replace('/[^a-z]{2}/','',$_SESSION['LANG'])));
    }
}
else {
    echo 2;
    // include local language files
    if(file_exists(sprintf("languages/%s.php",preg_replace('/[^a-z]{2}/','',LANG))))
        include_once(sprintf("languages/%s.php",preg_replace('/[^a-z]{2}/','',LANG)));
    if(file_exists(sprintf("languages/local_%s.php",preg_replace('/[^a-z]{2}/','',LANG))) and !isset($_SESSION['skip_local_lang'][LANG]))
        include_once(sprintf("languages/local_%s.php",preg_replace('/[^a-z]{2}/','',LANG)));
}


$load_useragreement = 0;
$load_projects = 0;
$load_search = 0;
$load_community = 0;
$load_faq = 0;
$load_contact = 0;
$load_mainpage = 0;
$load_blog = 0;
$load_support = 0;
$load_about = 0;
$load_privacy = 0;  
$load_documents = 0;
$load_disclaimer = 0;
$load_devel = 0;
$load_resources = 0;
$load_events = 0;
$load_cookies = 0;
$load_technical = 0;

if (isset($_GET['p']) and $_GET['p']=='faq') 
    $load_faq = 1;
if (isset($_GET['p']) and $_GET['p']=='contact') 
    $load_contact = 1;
if (isset($_GET['p']) and $_GET['p']=='search')
    $load_search = 1;
if (isset($_GET['p']) and $_GET['p']=='community')
    $load_community = 1;
if (isset($_GET['p']) and $_GET['p']=='documents')
    $load_documents = 1;
if (isset($_GET['p']) and $_GET['p']=='about')
    $load_about = 1;
if (isset($_GET['p']) and $_GET['p']=='privacy')
    $load_privacy = 1;
if (isset($_GET['p']) and $_GET['p']=='projects')
    $load_projects = 1;
if (isset($_GET['p']) and $_GET['p']=='terms')
    $load_useragreement = 1;
if (isset($_GET['p']) and $_GET['p']=='blog')
    $load_blog = 1;
if (isset($_GET['p']) and $_GET['p']=='contributions')
    $load_support = 1;
if (isset($_GET['p']) and $_GET['p']=='disclaimer')
    $load_disclaimer = 1;
if (isset($_GET['p']) and $_GET['p']=='development')
    $load_devel = 1;
if (isset($_GET['p']) and $_GET['p']=='resources')
    $load_resources = 1;
if (isset($_GET['p']) and $_GET['p']=='events')
    $load_events = 1;
if (isset($_GET['p']) and $_GET['p']=='cookies')
    $load_cookies = 1;
if (isset($_GET['p']) and $_GET['p']=='technical')
    $load_technical = 1;

if(count($_GET)==0) $load_mainpage=1;
$pages = array(
    'load_useagreement'=>$load_useragreement,
    'load_projects'=> $load_projects,
    'load_search'=> $load_search,
    'load_community'=> $load_community,
    'load_faq'=> $load_faq,
    'load_contact'=> $load_contact,
    'load_mainpage'=> $load_mainpage,
    'load_blog'=> $load_blog,
    'load_support'=>$load_support,
    'load_privacy'=>$load_privacy,
    'load_about'=>$load_about,
    'load_documents'=>$load_documents,
    'load_disclaimer'=>$load_disclaimer,
    'load_devel'=>$load_devel,
    'load_resources'=>$load_resources,
    'load_events'=>$load_events,
    'load_cookies'=>$load_cookies,
    'load_technical'=>$load_technical
);
?>
