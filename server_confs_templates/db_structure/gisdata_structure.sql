--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--CREATE DATABASE gisdata WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';

--\c gisdata;

--
-- Name: shared; Type: SCHEMA; Schema: -; Owner: gisadmin
--

CREATE SCHEMA shared;


ALTER SCHEMA shared OWNER TO gisadmin;

--
-- Name: SCHEMA shared; Type: COMMENT; Schema: -; Owner: gisadmin
--

COMMENT ON SCHEMA shared IS 'shared data';


--
-- Name: system; Type: SCHEMA; Schema: -; Owner: gisadmin
--

CREATE SCHEMA system;


ALTER SCHEMA system OWNER TO gisadmin;

--
-- Name: SCHEMA system; Type: COMMENT; Schema: -; Owner: gisadmin
--

COMMENT ON SCHEMA system IS 'system tables';


--
-- Name: temporary_tables; Type: SCHEMA; Schema: -; Owner: gisadmin
--

CREATE SCHEMA temporary_tables;


ALTER SCHEMA temporary_tables OWNER TO gisadmin;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

--
-- Name: bejelentes_trigger(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION bejelentes_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF tg_op = 'INSERT' THEN
         INSERT INTO sablon_alert (bejelentes_id,biotika,status)
         SELECT new.id,concat_ws(' ',faj,dt_from,dt_to,meret_kb,szamossag,egyedszam,modsz,adatkozlo,gyujto),'uj' FROM sablon WHERE ST_Contains(new.geometry,the_geom);
         RETURN new;
     END IF;
END$$;


ALTER FUNCTION public.bejelentes_trigger() OWNER TO gisadmin;

--
-- Name: drop_temp_table(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION drop_temp_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
        IF (TG_OP = 'DELETE') THEN
            execute format('DROP TABLE IF EXISTS temporary_tables.%I',OLD.table_name);
            RETURN OLD;
        END IF; 
END$$;


ALTER FUNCTION public.drop_temp_table() OWNER TO gisadmin;

--
-- Name: file_connect_status_update(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION file_connect_status_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
     IF (new.obm_files_id IS NOT NULL) THEN
         UPDATE file_connect SET temporal=false WHERE temporal=true AND conid=new.obm_files_id;
     END IF;
     RETURN new;
  END IF;
END$$;


ALTER FUNCTION public.file_connect_status_update() OWNER TO gisadmin;

--
-- Name: geom_accuracy_reduce(geometry, integer); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION geom_accuracy_reduce(geom geometry, precisity integer) RETURNS geometry
    LANGUAGE plpgsql
    AS $$DECLARE
  cgeom geometry;
BEGIN
  SELECT ST_Point(
    regexp_replace(st_x(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1')::float,
    regexp_replace(st_y(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1')::float) INTO cgeom;
  RETURN cgeom;
END;
$$;


ALTER FUNCTION public.geom_accuracy_reduce(geom geometry, precisity integer) OWNER TO gisadmin;

--
-- Name: geom_noise(geometry, integer); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION geom_noise(geom geometry, precisity integer) RETURNS geometry
    LANGUAGE plpgsql
    AS $$DECLARE
  cgeom geometry;
BEGIN
  SELECT ST_Point(
(regexp_replace(st_x(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1') ||
round(regexp_replace(st_x(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '})(.+)',E'\\2')::float*random()))::float,
(regexp_replace(st_y(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1') ||
round(regexp_replace(st_y(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '})(.+)',E'\\2')::float*random()))::float

) INTO cgeom;
  RETURN cgeom;
END;$$;


ALTER FUNCTION public.geom_noise(geom geometry, precisity integer) OWNER TO gisadmin;

--
-- Name: history_sablon(); Type: FUNCTION; Schema: public; Owner: sablon_admin
--

CREATE FUNCTION history_sablon() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        INSERT INTO sablon_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT 'D', now(), user, CONCAT(OLD.*), OLD.obm_id, OLD.obm_modifier_id, 'sablon';
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO sablon_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT 'U', now(), user, CONCAT(NEW.*), OLD.obm_id, OLD.obm_modifier_id, 'sablon';
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO sablon_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT 'I', now(), user, CONCAT(NEW.*), OLD.obm_id, OLD.obm_modifier_id, 'sablon';
        RETURN NEW;
    END IF; 
END $$;


ALTER FUNCTION public.history_sablon() OWNER TO sablon_admin;

--
-- Name: rules_sablon(); Type: FUNCTION; Schema: public; Owner: sablon_admin
--

CREATE FUNCTION rules_sablon() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        DELETE FROM sablon_rules WHERE row_id=OLD.obm_id ;
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO sablon_rules ("data_table",row_id,read,write) 
            SELECT 'sablon',NEW.obm_id,ARRAY(SELECT uploader_id 
            FROM uploadings WHERE uploadings.id=NEW.obm_uploading_id),ARRAY(SELECT uploader_id FROM uploadings 
            WHERE uploadings.id=NEW.obm_uploading_id);
        RETURN NEW;
    END IF; 
END $$;


ALTER FUNCTION public.rules_sablon() OWNER TO sablon_admin;

--
-- Name: update_sablon_taxonlist(); Type: FUNCTION; Schema: public; Owner: sablon_admin
--

CREATE FUNCTION update_sablon_taxonlist() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF tg_op = 'INSERT' THEN
        INSERT INTO sablon_taxon (meta,word,lang,modifier_id) 
            SELECT replace(new.faj,' ',''),new.faj,'faj',new.obm_modifier_id 
            WHERE NOT EXISTS ( 
                    SELECT taxon_id FROM sablon_taxon WHERE replace(new.faj,' ','')=meta
            ) AND new.faj IS NOT NULL; 
        INSERT INTO sablon_taxon (taxon_id,meta,word,lang,modifier_id)
        SELECT sq.taxon_id,replace(new.magyar,' ',''),new.magyar,'magyar',new.obm_modifier_id
        FROM (
            SELECT DISTINCT ON (h.taxon_id) h.taxon_id 
            FROM sablon_taxon s LEFT JOIN sablon_taxon h ON s.taxon_id=h.taxon_id WHERE replace(new.faj,' ','')=s.meta) as sq
        WHERE NOT EXISTS (
            SELECT 1 FROM sablon_taxon WHERE replace(new.magyar,' ','')=meta
        ) AND new.magyar IS NOT NULL;RETURN new;END IF;
    IF tg_op = 'UPDATE' THEN
        INSERT INTO sablon_taxon (meta,word,lang,modifier_id) 
            SELECT replace(new.faj,' ',''),new.faj,'faj',new.obm_modifier_id 
            WHERE NOT EXISTS ( 
                    SELECT taxon_id FROM sablon_taxon WHERE replace(new.faj,' ','')=meta
            ) AND new.faj IS NOT NULL; 
        INSERT INTO sablon_taxon (taxon_id,meta,word,lang,modifier_id)
        SELECT sq.taxon_id,replace(new.magyar,' ',''),new.magyar,'magyar',new.obm_modifier_id
        FROM (
            SELECT DISTINCT ON (h.taxon_id) h.taxon_id 
            FROM sablon_taxon s LEFT JOIN sablon_taxon h ON s.taxon_id=h.taxon_id WHERE replace(new.faj,' ','')=s.meta) as sq
        WHERE NOT EXISTS (
            SELECT 1 FROM sablon_taxon WHERE replace(new.magyar,' ','')=meta
        ) AND new.magyar IS NOT NULL;RETURN new;
    END IF;
END $$;


ALTER FUNCTION public.update_sablon_taxonlist() OWNER TO sablon_admin;

--
-- Name: update_sablon_taxonname(); Type: FUNCTION; Schema: public; Owner: sablon_admin
--

CREATE FUNCTION update_sablon_taxonname() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF new.taxon_db=old.taxon_db THEN
        IF tg_op = 'UPDATE' THEN
          IF new.lang='faj' THEN
            UPDATE sablon SET faj=new.word,obm_modifier_id=new.modifier_id WHERE sablon.faj=old.word;
          ELSE
            UPDATE sablon SET magyar=new.word,obm_modifier_id=new.modifier_id WHERE sablon.magyar=old.word;
          END IF; 
          RETURN new; 
        END IF; 
    END IF;
    RETURN new;
END $$;


ALTER FUNCTION public.update_sablon_taxonname() OWNER TO sablon_admin;

--
-- Name: update_taxon_db(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION update_taxon_db() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

    IF TG_OP = 'DELETE' THEN

        EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db - 1 WHERE word = ' || quote_literal(OLD.faj);
        EXECUTE 'DELETE FROM ' || TG_TABLE_NAME || '_taxon WHERE taxon_db = 0  AND word = ' || quote_literal(OLD.faj);

        EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db - 1 WHERE word = ' || quote_literal(OLD.magyar);
        EXECUTE 'DELETE FROM ' || TG_TABLE_NAME || '_taxon WHERE taxon_db = 0  AND word = ' || quote_literal(OLD.magyar);
 
        RETURN OLD;

    ELSIF TG_OP = 'INSERT' THEN

        EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db + 1 WHERE word = ' || quote_literal(NEW.faj);

        EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db + 1 WHERE word = ' || quote_literal(NEW.magyar);
        
        RETURN NEW;

    ELSIF TG_OP = 'UPDATE' THEN

        IF OLD.faj != NEW.faj THEN
                EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db + 1 WHERE word = ' || quote_literal(NEW.faj);
                EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db - 1 WHERE word = ' || quote_literal(OLD.faj);
                EXECUTE 'DELETE FROM ' || TG_TABLE_NAME || '_taxon WHERE taxon_db = 0  AND word = ' || quote_literal(OLD.faj);
        END IF;

            IF OLD.magyar != NEW.magyar THEN
                EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db + 1 WHERE word = ' || quote_literal(NEW.magyar);
                EXECUTE 'UPDATE ' || TG_TABLE_NAME || '_taxon SET taxon_db = taxon_db - 1 WHERE word = ' || quote_literal(OLD.magyar);
                EXECUTE 'DELETE FROM ' || TG_TABLE_NAME || '_taxon WHERE taxon_db = 0  AND word = ' || quote_literal(OLD.magyar);
            END IF;

        RETURN NEW;

    END IF;
END;$$;


ALTER FUNCTION public.update_taxon_db() OWNER TO gisadmin;


--
-- Name: update_uploadings_vote(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION update_uploadings_vote() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	IF NEW."table" = 'uploadings' AND NEW.valuation!=0 THEN
        UPDATE uploadings SET validation=(SELECT avg(valuation) FROM evaluations 
        WHERE "table"='uploadings' AND row=NEW.row AND valuation!=0) WHERE id=NEW.row;
      	END IF;
      	RETURN NEW;
   END;$$;


ALTER FUNCTION public.update_uploadings_vote() OWNER TO gisadmin;

--
-- Name: update_vote(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION update_vote() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
par character varying(32);
gid character varying(8);
val character varying(16);
BEGIN
      IF NEW.valuation!=0 THEN
         par = NEW."table";
         gid = 'obm_id';
         val = 'obm_validation';
         IF par='uploadings' THEN
             gid = 'id';
             val = 'validation';
         END IF;
         EXECUTE 'UPDATE ' || quote_ident(par) || ' SET ' || val || '=(SELECT avg(valuation) FROM evaluations WHERE "table"=' || quote_literal(par) || ' AND row=' || NEW.row || ' AND valuation!=0) WHERE ' || gid || '=' || NEW.row;
      END IF;
      RETURN NEW;
END;$$;


ALTER FUNCTION public.update_vote() OWNER TO biomapsadmin;

SET search_path = temporary_tables, pg_catalog;

--
-- Name: json_array_append(json, text); Type: FUNCTION; Schema: temporary_tables; Owner: gisadmin
--

CREATE FUNCTION json_array_append(j json, e text) RETURNS json
    LANGUAGE sql IMMUTABLE
    AS $$
    select array_to_json(array_append(array(select * from json_array_elements_text(j)), e))::json
$$;


ALTER FUNCTION temporary_tables.json_array_append(j json, e text) OWNER TO gisadmin;

SET search_path = hrsz_terkepek, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

SET search_path = public, pg_catalog;

--
-- Name: sablon; Type: TABLE; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE TABLE sablon (
    obm_id integer NOT NULL,
    objectid integer,
    faj character varying(255),
    magyar character varying(255),
    eov_x double precision,
    eov_y double precision,
    hely character varying(255),
    szamossag character varying(255),
    meret_kb character varying(255),
    egyedszam double precision,
    him double precision,
    nosteny double precision,
    eloford_al character varying(255),
    gyujto character varying(255),
    gyujto2 character varying(255),
    gyujto3 character varying(255),
    hatarozo character varying(255),
    adatkozlo character varying(255),
    modsz character varying(255),
    elohely character varying(255),
    veszteny character varying(255),
    kapcs_dok character varying(255),
    megj character varying(255),
    rogz_modsz character varying(255),
    altema character varying(255),
    obm_geometry geometry,
    obm_uploading_id integer,
    obm_validation numeric,
    obm_comments text[],
    obm_modifier_id integer,
    natura2000 boolean,
    vedettseg character varying(255),
    tema character varying(50),
    obm_datum timestamp with time zone DEFAULT now(),
    dt_from timestamp without time zone,
    dt_to timestamp without time zone,
    rogz_dt timestamp without time zone,
    obm_files_id character varying(32),
    "time" integer,
    CONSTRAINT enforce_dims_the_geom CHECK ((st_ndims(obm_geometry) = 2)),
    CONSTRAINT enforce_srid_the_geom CHECK ((st_srid(obm_geometry) = 4326))
);


ALTER TABLE sablon OWNER TO sablon_admin;

--
-- Name: TABLE sablon; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON TABLE sablon IS 'DINPI Biotika';


--
-- Name: COLUMN sablon.eov_x; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon.eov_x IS 'nincs értelme kitölteni';


--
-- Name: COLUMN sablon.eov_y; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon.eov_y IS 'nincs értelme kitölteni';


--
-- Name: sablon_alert; Type: TABLE; Schema: public; Owner: gisadmin; Tablespace: 
--

CREATE TABLE sablon_alert (
    id integer NOT NULL,
    bejelentes_id integer NOT NULL,
    biotika text,
    status character varying(6),
    datetime timestamp with time zone DEFAULT now()
);


ALTER TABLE sablon_alert OWNER TO gisadmin;

--
-- Name: TABLE sablon_alert; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON TABLE sablon_alert IS 'Automatikus figyelmeztetések';


--
-- Name: COLUMN sablon_alert.bejelentes_id; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_alert.bejelentes_id IS 'bejelentés azonosítója';


--
-- Name: COLUMN sablon_alert.biotika; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_alert.biotika IS 'lekérdezett biotikai adatok';


--
-- Name: COLUMN sablon_alert.status; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_alert.status IS 'státusz: új, olvasott, folyamatban,...';


--
-- Name: COLUMN sablon_alert.datetime; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_alert.datetime IS 'automatikus időpont';


--
-- Name: sablon_alert_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE sablon_alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_alert_id_seq OWNER TO gisadmin;

--
-- Name: sablon_alert_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE sablon_alert_id_seq OWNED BY sablon_alert.id;


--
-- Name: sablon_bejelentesek; Type: TABLE; Schema: public; Owner: gisadmin; Tablespace: 
--

CREATE TABLE sablon_bejelentesek (
    id integer NOT NULL,
    iktatoszam character varying(16),
    ugyfel character varying(64),
    cim character varying(255),
    telepules character varying(64),
    hrszkod character varying(16),
    alrkod character varying(2),
    blokkaz character varying(16),
    terulet numeric,
    terv_date date,
    bej_date date,
    n2000 boolean,
    mtet character varying(32),
    kultura character varying(32),
    megjegyzes text,
    dok text,
    dok_tipus character varying(32),
    felt_datum date DEFAULT now(),
    geometry geometry(Geometry,4326)
);


ALTER TABLE sablon_bejelentesek OWNER TO gisadmin;

--
-- Name: COLUMN sablon_bejelentesek.iktatoszam; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.iktatoszam IS 'iktatószám';


--
-- Name: COLUMN sablon_bejelentesek.ugyfel; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.ugyfel IS 'Név';


--
-- Name: COLUMN sablon_bejelentesek.cim; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.cim IS 'cím';


--
-- Name: COLUMN sablon_bejelentesek.telepules; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.telepules IS 'település';


--
-- Name: COLUMN sablon_bejelentesek.hrszkod; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.hrszkod IS 'hrsz';


--
-- Name: COLUMN sablon_bejelentesek.alrkod; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.alrkod IS 'alrészlet';


--
-- Name: COLUMN sablon_bejelentesek.blokkaz; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.blokkaz IS 'MEPAR blokk azonosító';


--
-- Name: COLUMN sablon_bejelentesek.terv_date; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.terv_date IS 'kaszálás tervezett ideje';


--
-- Name: COLUMN sablon_bejelentesek.bej_date; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.bej_date IS 'bejelentés dátuma';


--
-- Name: COLUMN sablon_bejelentesek.n2000; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.n2000 IS 'Natura 2000 bejelentés';


--
-- Name: COLUMN sablon_bejelentesek.mtet; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.mtet IS 'Valamelyik AKG miatt jelenti be.';


--
-- Name: COLUMN sablon_bejelentesek.kultura; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.kultura IS 'élőhelytípus (lehetne pl. lucerna is)';


--
-- Name: COLUMN sablon_bejelentesek.dok; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.dok IS 'eltárolt dokumentum';


--
-- Name: COLUMN sablon_bejelentesek.dok_tipus; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.dok_tipus IS 'dokumentum típusa';


--
-- Name: COLUMN sablon_bejelentesek.felt_datum; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.felt_datum IS 'adatfeltöltés dátuma';


--
-- Name: COLUMN sablon_bejelentesek.geometry; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_bejelentesek.geometry IS 'érintett terület térbelisége';


--
-- Name: sablon_bejelentesek_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE sablon_bejelentesek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_bejelentesek_id_seq OWNER TO gisadmin;

--
-- Name: sablon_bejelentesek_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE sablon_bejelentesek_id_seq OWNED BY sablon_bejelentesek.id;


--
-- Name: sablon_cite; Type: TABLE; Schema: public; Owner: gisadmin; Tablespace: 
--

CREATE TABLE sablon_cite (
    row_id integer NOT NULL,
    gyujto integer[],
    gyujto2 integer[],
    gyujto3 integer[],
    hatarozo integer[],
    adatkozlo integer[]
);


ALTER TABLE sablon_cite OWNER TO gisadmin;

--
-- Name: sablon_gid_seq; Type: SEQUENCE; Schema: public; Owner: sablon_admin
--

CREATE SEQUENCE sablon_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_gid_seq OWNER TO sablon_admin;

--
-- Name: sablon_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sablon_admin
--

ALTER SEQUENCE sablon_gid_seq OWNED BY sablon.obm_id;


--
-- Name: sablon_hatarozatok_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE sablon_hatarozatok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_hatarozatok_id_seq OWNER TO gisadmin;

--
-- Name: sablon_hatarozatok; Type: TABLE; Schema: public; Owner: gisadmin; Tablespace: 
--

CREATE TABLE sablon_hatarozatok (
    id integer DEFAULT nextval('sablon_hatarozatok_id_seq'::regclass) NOT NULL,
    iktatoszam character varying(16),
    ugyfel character varying(64),
    hatosag character varying(64),
    targy character varying(128),
    leiras text,
    kelt date,
    erv_kezd date,
    erv_vege date,
    ell_datum date,
    ell_all text,
    ellenorzo character varying(128),
    megjegyzes text,
    dok text,
    dok_tipus character varying(32),
    felt_datum date DEFAULT now()
);


ALTER TABLE sablon_hatarozatok OWNER TO gisadmin;

--
-- Name: COLUMN sablon_hatarozatok.hatosag; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.hatosag IS 'Kiadmányozó hatóság';


--
-- Name: COLUMN sablon_hatarozatok.targy; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.targy IS 'Határozat tárgya';


--
-- Name: COLUMN sablon_hatarozatok.leiras; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.leiras IS 'korlátozások, fontos információk röviden';


--
-- Name: COLUMN sablon_hatarozatok.kelt; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.kelt IS 'Kiadmányozás ideje';


--
-- Name: COLUMN sablon_hatarozatok.erv_kezd; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.erv_kezd IS 'határozat jogerőre emelkedése';


--
-- Name: COLUMN sablon_hatarozatok.erv_vege; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.erv_vege IS 'érvényesség vége';


--
-- Name: COLUMN sablon_hatarozatok.ell_datum; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.ell_datum IS 'Ellneőrzés dátuma';


--
-- Name: COLUMN sablon_hatarozatok.ell_all; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.ell_all IS 'Ellenőrzés megállapításai';


--
-- Name: COLUMN sablon_hatarozatok.ellenorzo; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.ellenorzo IS 'Ellenőrző személy';


--
-- Name: COLUMN sablon_hatarozatok.dok; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.dok IS 'eltárolt dokumentum';


--
-- Name: COLUMN sablon_hatarozatok.felt_datum; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON COLUMN sablon_hatarozatok.felt_datum IS 'adatfeltöltés dátuma';


--
-- Name: sablon_history; Type: TABLE; Schema: public; Owner: gisadmin; Tablespace: 
--

CREATE TABLE sablon_history (
    operation character(1) NOT NULL,
    hist_time timestamp without time zone NOT NULL,
    userid text NOT NULL,
    query text,
    row_id integer NOT NULL,
    hist_id integer NOT NULL,
    modifier_id integer,
    data_table character varying(64)
);


ALTER TABLE sablon_history OWNER TO gisadmin;

--
-- Name: sablon_history_hist_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE sablon_history_hist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_history_hist_id_seq OWNER TO gisadmin;

--
-- Name: sablon_history_hist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE sablon_history_hist_id_seq OWNED BY sablon_history.hist_id;

--
-- Name: sablon_rules; Type: TABLE; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE TABLE sablon_rules (
    data_table character varying(128) NOT NULL,
    row_id integer NOT NULL,
    read integer[],
    write integer[],
    sensitivity integer DEFAULT 0,
    owner character varying(255)
);


ALTER TABLE sablon_rules OWNER TO sablon_admin;

--
-- Name: COLUMN sablon_rules.sensitivity; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon_rules.sensitivity IS '0,1,2,3 (public, sens geom, restricted attributes, hidden)';


--
-- Name: sablon_taxon; Type: TABLE; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE TABLE sablon_taxon (
    meta character varying(254) NOT NULL,
    word character varying(254) NOT NULL,
    lang character varying(16) NOT NULL,
    taxon_id integer NOT NULL,
    status numeric(1,0) DEFAULT 0,
    modifier_id integer DEFAULT 0,
    frequency integer DEFAULT 0,
    taxon_db integer DEFAULT 0
);


ALTER TABLE sablon_taxon OWNER TO sablon_admin;

--
-- Name: COLUMN sablon_taxon.frequency; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon_taxon.frequency IS 'query counter';


--
-- Name: COLUMN sablon_taxon.taxon_db; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon_taxon.taxon_db IS 'number of related records';


--
-- Name: sablon_taxon_taxon_id_seq; Type: SEQUENCE; Schema: public; Owner: sablon_admin
--

CREATE SEQUENCE sablon_taxon_taxon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_taxon_taxon_id_seq OWNER TO sablon_admin;

--
-- Name: sablon_taxon_taxon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sablon_admin
--

ALTER SEQUENCE sablon_taxon_taxon_id_seq OWNED BY sablon_taxon.taxon_id;

--
-- Name: file_connect_conid_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE file_connect_conid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE file_connect_conid_seq OWNER TO biomapsadmin;

--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE files_id_seq OWNER TO biomapsadmin;

--
-- Name: world_gid_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE world_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE world_gid_seq OWNER TO gisadmin;

SET search_path = shared, pg_catalog;

--
-- Name: weather_data; Type: TABLE; Schema: shared; Owner: gisadmin; Tablespace: 
--

CREATE TABLE weather_data (
    id integer NOT NULL,
    the_geom public.geometry NOT NULL,
    rain numeric,
    wind numeric,
    moisture numeric,
    blast numeric,
    temperature numeric,
    datetime timestamp without time zone NOT NULL
);


ALTER TABLE weather_data OWNER TO gisadmin;

--
-- Name: TABLE weather_data; Type: COMMENT; Schema: shared; Owner: gisadmin
--

COMMENT ON TABLE weather_data IS 'project independent weather data';


--
-- Name: weather_data_id_seq; Type: SEQUENCE; Schema: shared; Owner: gisadmin
--

CREATE SEQUENCE weather_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE weather_data_id_seq OWNER TO gisadmin;

--
-- Name: weather_data_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: gisadmin
--

ALTER SEQUENCE weather_data_id_seq OWNED BY weather_data.id;


SET search_path = system, pg_catalog;

--
-- Name: evaluations; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE evaluations (
    "table" character varying(64) NOT NULL,
    "row" integer NOT NULL,
    user_name character varying(128) NOT NULL,
    valuation integer,
    id integer NOT NULL,
    datum timestamp without time zone DEFAULT now(),
    user_id integer,
    comments text,
    related_id integer
);


ALTER TABLE evaluations OWNER TO gisadmin;

--
-- Name: evaluations_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE evaluations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evaluations_id_seq OWNER TO gisadmin;

--
-- Name: evaluations_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE evaluations_id_seq OWNED BY evaluations.id;


--
-- Name: file_connect; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE file_connect (
    file_id integer NOT NULL,
    conid character varying(32) NOT NULL,
    temporal boolean DEFAULT true,
    sessionid character varying(64),
    rownum smallint
);


ALTER TABLE file_connect OWNER TO gisadmin;

--
-- Name: files; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE files (
    id integer DEFAULT nextval('public.files_id_seq'::regclass) NOT NULL,
    project_table character varying(32) NOT NULL,
    reference text NOT NULL,
    comment text,
    datum timestamp with time zone DEFAULT now(),
    access integer,
    user_id integer DEFAULT 0,
    status character varying(16) DEFAULT 'valid'::character varying,
    sessionid character varying(64),
    sum character varying(64),
    mimetype character varying(128),
    data_table character varying(256),
    exif json,
    slideshow boolean DEFAULT false
);


ALTER TABLE files OWNER TO gisadmin;

--
-- Name: imports; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE imports (
    project_table character varying(32) NOT NULL,
    user_id numeric NOT NULL,
    ref character varying(32) NOT NULL,
    datum timestamp without time zone,
    header text NOT NULL,
    data text NOT NULL,
    form_type character varying(6) NOT NULL,
    form_id numeric,
    file text,
    template_name character varying(128),
    massive_edit json
);


ALTER TABLE imports OWNER TO gisadmin;

--
-- Name: TABLE imports; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON TABLE imports IS 'saved import forms with data';


--
-- Name: COLUMN imports.template_name; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON COLUMN imports.template_name IS 'save as sablon - name';


--
-- Name: polygon_users; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE polygon_users (
    user_id integer NOT NULL,
    polygon_id integer NOT NULL,
    select_view character varying(13)
);


ALTER TABLE polygon_users OWNER TO gisadmin;

--
-- Name: query_buff; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE query_buff (
    user_id integer NOT NULL,
    datetime timestamp with time zone DEFAULT now(),
    "table" character varying(64),
    id integer NOT NULL,
    name character varying(100),
    access numeric DEFAULT 1,
    geometry public.geometry
);


ALTER TABLE query_buff OWNER TO gisadmin;

--
-- Name: query_buff_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE query_buff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE query_buff_id_seq OWNER TO gisadmin;

--
-- Name: query_buff_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE query_buff_id_seq OWNED BY query_buff.id;


--
-- Name: shared_polygons; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE shared_polygons (
    id integer NOT NULL,
    "timestamp" timestamp with time zone DEFAULT now(),
    project_table character varying(64),
    geometry public.geometry NOT NULL,
    name character varying(128) NOT NULL,
    access character varying(7) DEFAULT 'private'::character varying NOT NULL,
    user_id integer,
    original_name character varying
);


ALTER TABLE shared_polygons OWNER TO gisadmin;

--
-- Name: shared_polygons_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE shared_polygons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shared_polygons_id_seq OWNER TO gisadmin;

--
-- Name: shared_polygons_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE shared_polygons_id_seq OWNED BY shared_polygons.id;


--
-- Name: temp_index; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE temp_index (
    table_name character varying(128) NOT NULL,
    datum timestamp without time zone DEFAULT now() NOT NULL,
    interconnect boolean DEFAULT false
);


ALTER TABLE temp_index OWNER TO gisadmin;

--
-- Name: TABLE temp_index; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON TABLE temp_index IS 'unlogged temporary table indexes';


--
-- Name: uploadings; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE uploadings (
    id integer NOT NULL,
    uploading_date timestamp without time zone NOT NULL,
    uploader_id integer NOT NULL,
    uploader_name character varying(100),
    validation numeric,
    collectors text,
    description text,
    access numeric DEFAULT 0 NOT NULL,
    "group" integer[],
    project_table character varying(64),
    owner integer[],
    project character varying(32),
    form_id integer,
    metadata json
);


ALTER TABLE uploadings OWNER TO gisadmin;

--
-- Name: COLUMN uploadings.access; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON COLUMN uploadings.access IS 'Access level 0-public,1-logined users,2-specified';


--
-- Name: uploadings_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE uploadings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE uploadings_id_seq OWNER TO gisadmin;

--
-- Name: uploadings_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE uploadings_id_seq OWNED BY uploadings.id;


SET search_path = temporary_tables, pg_catalog;

SET search_path = public, pg_catalog;

--
-- Name: obm_id; Type: DEFAULT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon ALTER COLUMN obm_id SET DEFAULT nextval('sablon_gid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY sablon_alert ALTER COLUMN id SET DEFAULT nextval('sablon_alert_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY sablon_bejelentesek ALTER COLUMN id SET DEFAULT nextval('sablon_bejelentesek_id_seq'::regclass);


--
-- Name: hist_id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY sablon_history ALTER COLUMN hist_id SET DEFAULT nextval('sablon_history_hist_id_seq'::regclass);

--
-- Name: taxon_id; Type: DEFAULT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon_taxon ALTER COLUMN taxon_id SET DEFAULT nextval('sablon_taxon_taxon_id_seq'::regclass);


SET search_path = shared, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: shared; Owner: gisadmin
--

ALTER TABLE ONLY weather_data ALTER COLUMN id SET DEFAULT nextval('weather_data_id_seq'::regclass);


SET search_path = system, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY evaluations ALTER COLUMN id SET DEFAULT nextval('evaluations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY query_buff ALTER COLUMN id SET DEFAULT nextval('query_buff_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY shared_polygons ALTER COLUMN id SET DEFAULT nextval('shared_polygons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY uploadings ALTER COLUMN id SET DEFAULT nextval('uploadings_id_seq'::regclass);


SET search_path = hrsz_terkepek, pg_catalog;


SET search_path = public, pg_catalog;

--
-- Name: sablon_bejelentesek_pkey; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY sablon_bejelentesek
    ADD CONSTRAINT sablon_bejelentesek_pkey PRIMARY KEY (id);

--
-- Name: sablon_hatarozatok_pkey; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY sablon_hatarozatok
    ADD CONSTRAINT sablon_hatarozatok_pkey PRIMARY KEY (id);


--
-- Name: sablon_history_key; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY sablon_history
    ADD CONSTRAINT sablon_history_key PRIMARY KEY (hist_id);

--
-- Name: sablon_pkey; Type: CONSTRAINT; Schema: public; Owner: sablon_admin; Tablespace: 
--

ALTER TABLE ONLY sablon
    ADD CONSTRAINT sablon_pkey PRIMARY KEY (obm_id);


--
-- Name: sablon_rules_ukey; Type: CONSTRAINT; Schema: public; Owner: sablon_admin; Tablespace: 
--

ALTER TABLE ONLY sablon_rules
    ADD CONSTRAINT sablon_rules_ukey UNIQUE (data_table, row_id);


--
-- Name: sablon_taxon_metaword_key; Type: CONSTRAINT; Schema: public; Owner: sablon_admin; Tablespace: 
--

ALTER TABLE ONLY sablon_taxon
    ADD CONSTRAINT sablon_taxon_metaword_key UNIQUE (meta, word);

SET search_path = shared, pg_catalog;

--
-- Name: weather_data_pkey; Type: CONSTRAINT; Schema: shared; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY weather_data
    ADD CONSTRAINT weather_data_pkey PRIMARY KEY (id);


SET search_path = system, pg_catalog;

--
-- Name: file_connect_ukey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY file_connect
    ADD CONSTRAINT file_connect_ukey UNIQUE (file_id, conid);


--
-- Name: files_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: files_project_table_reference_key; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_project_table_reference_key UNIQUE (project_table, reference);


--
-- Name: imports_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY imports
    ADD CONSTRAINT imports_pkey PRIMARY KEY (ref);


--
-- Name: shared_polygons_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY shared_polygons
    ADD CONSTRAINT shared_polygons_pkey PRIMARY KEY (id);


--
-- Name: temp_index_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY temp_index
    ADD CONSTRAINT temp_index_pkey PRIMARY KEY (table_name);


--
-- Name: uploadings_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY uploadings
    ADD CONSTRAINT uploadings_pkey PRIMARY KEY (id);


--
-- Name: user_polygon_ukey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY polygon_users
    ADD CONSTRAINT user_polygon_ukey UNIQUE (user_id, polygon_id);


SET search_path = public, pg_catalog;

--
-- Name: sablon_geom; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX sablon_geom ON sablon USING gist (obm_geometry);

--
-- Name: sablon_obm_uploading_id_idx; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX sablon_obm_uploading_id_idx ON sablon USING btree (obm_uploading_id);


--
-- Name: fa_gin; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX faj_gin ON sablon USING gin (faj gin_trgm_ops);

--
-- Name: trgm_idx; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX trgm_idx ON sablon_taxon USING gist (meta gist_trgm_ops);



SET search_path = system, pg_catalog;

--
-- Name: buff_id; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE UNIQUE INDEX buff_id ON query_buff USING btree (id);


--
-- Name: evaluations_key; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE UNIQUE INDEX evaluations_key ON evaluations USING btree (id);


--
-- Name: uploadings_date_idx; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE INDEX uploadings_date_idx ON uploadings USING btree (uploading_date);


--
-- Name: uploadings_project_idx; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE INDEX uploadings_project_idx ON uploadings USING btree (project);


SET search_path = public, pg_catalog;

--
-- Name: bejelentes_trigger; Type: TRIGGER; Schema: public; Owner: gisadmin
--

CREATE TRIGGER bejelentes_trigger AFTER INSERT ON sablon_bejelentesek FOR EACH ROW EXECUTE PROCEDURE bejelentes_trigger();


--
-- Name: sablon_name_update; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER sablon_name_update AFTER UPDATE ON sablon_taxon FOR EACH ROW EXECUTE PROCEDURE update_sablon_taxonname();


--
-- Name: file_connection; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER file_connection AFTER INSERT ON sablon FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();

--
-- Name: history_update_sablon; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER history_update_sablon AFTER DELETE OR UPDATE ON sablon FOR EACH ROW EXECUTE PROCEDURE history_sablon();

--
-- Name: rules_sablon; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER rules_sablon AFTER INSERT OR DELETE OR UPDATE ON sablon FOR EACH ROW EXECUTE PROCEDURE rules_sablon();


--
-- Name: taxon_db_update; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER taxon_db_update AFTER INSERT OR DELETE OR UPDATE ON sablon FOR EACH ROW EXECUTE PROCEDURE update_taxon_db('faj', 'magyar');

ALTER TABLE sablon DISABLE TRIGGER taxon_db_update;

--
-- Name: taxon_update_sablon; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER taxon_update_sablon BEFORE INSERT OR UPDATE ON sablon FOR EACH ROW EXECUTE PROCEDURE update_sablon_taxonlist();

SET search_path = system, pg_catalog;

--
-- Name: drop_temporay_table; Type: TRIGGER; Schema: system; Owner: gisadmin
--

CREATE TRIGGER drop_temporay_table AFTER DELETE ON temp_index FOR EACH ROW EXECUTE PROCEDURE public.drop_temp_table();


--
-- Name: vote; Type: TRIGGER; Schema: system; Owner: gisadmin
--

CREATE TRIGGER vote AFTER INSERT OR UPDATE ON evaluations FOR EACH ROW EXECUTE PROCEDURE public.update_vote();


SET search_path = public, pg_catalog;

--
-- Name: bejelentesek_id; Type: FK CONSTRAINT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY sablon_alert
    ADD CONSTRAINT bejelentesek_id FOREIGN KEY (bejelentes_id) REFERENCES sablon_bejelentesek(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: uploading_id; Type: FK CONSTRAINT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon
    ADD CONSTRAINT uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES system.uploadings(id);


SET search_path = system, pg_catalog;

--
-- Name: files_fkey; Type: FK CONSTRAINT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY file_connect
    ADD CONSTRAINT files_fkey FOREIGN KEY (file_id) REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shared_polygons_id; Type: FK CONSTRAINT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY polygon_users
    ADD CONSTRAINT shared_polygons_id FOREIGN KEY (polygon_id) REFERENCES shared_polygons(id) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: public; Type: ACL; Schema: -; Owner: biomapsadmin
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO biomapsadmin;
GRANT ALL ON SCHEMA public TO project_admin;

--
-- Name: system; Type: ACL; Schema: -; Owner: gisadmin
--

REVOKE ALL ON SCHEMA system FROM PUBLIC;
REVOKE ALL ON SCHEMA system FROM gisadmin;
GRANT ALL ON SCHEMA system TO gisadmin;
GRANT USAGE ON SCHEMA system TO project_admin;

--
-- Name: temporary_tables; Type: ACL; Schema: -; Owner: gisadmin
--

REVOKE ALL ON SCHEMA temporary_tables FROM PUBLIC;
REVOKE ALL ON SCHEMA temporary_tables FROM gisadmin;
GRANT ALL ON SCHEMA temporary_tables TO gisadmin;
GRANT ALL ON SCHEMA temporary_tables TO project_admin;
GRANT ALL ON SCHEMA temporary_tables TO biomapsadmin;


SET search_path = public, pg_catalog;

--
-- Name: sablon; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON TABLE sablon FROM PUBLIC;
REVOKE ALL ON TABLE sablon FROM sablon_admin;
GRANT ALL ON TABLE sablon TO sablon_admin;
GRANT SELECT ON TABLE sablon TO sablon_user;
GRANT ALL ON TABLE sablon TO biomapsadmin;


--
-- Name: sablon_gid_seq; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON SEQUENCE sablon_gid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_gid_seq FROM sablon_admin;
GRANT ALL ON SEQUENCE sablon_gid_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_gid_seq TO biomapsadmin;


--
-- Name: sablon_history; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON TABLE sablon_history FROM PUBLIC;
REVOKE ALL ON TABLE sablon_history FROM gisadmin;
GRANT ALL ON TABLE sablon_history TO gisadmin;
GRANT ALL ON TABLE sablon_history TO sablon_admin;
GRANT ALL ON TABLE sablon_history TO biomapsadmin;


--
-- Name: sablon_history_hist_id_seq; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE sablon_history_hist_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_history_hist_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE sablon_history_hist_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE sablon_history_hist_id_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_history_hist_id_seq TO biomapsadmin;


--
-- Name: sablon_rules; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON TABLE sablon_rules FROM PUBLIC;
REVOKE ALL ON TABLE sablon_rules FROM sablon_admin;
GRANT ALL ON TABLE sablon_rules TO sablon_admin;
GRANT ALL ON TABLE sablon_rules TO biomapsadmin;


--
-- Name: sablon_taxon; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON TABLE sablon_taxon FROM PUBLIC;
REVOKE ALL ON TABLE sablon_taxon FROM sablon_admin;
GRANT ALL ON TABLE sablon_taxon TO sablon_admin;
GRANT ALL ON TABLE sablon_taxon TO biomapsadmin;


--
-- Name: sablon_taxon_taxon_id_seq; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON SEQUENCE sablon_taxon_taxon_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_taxon_taxon_id_seq FROM sablon_admin;
GRANT ALL ON SEQUENCE sablon_taxon_taxon_id_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_taxon_taxon_id_seq TO biomapsadmin;


--
-- Name: file_connect_conid_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE file_connect_conid_seq FROM PUBLIC;
GRANT ALL ON SEQUENCE file_connect_conid_seq TO gisadmin;
GRANT ALL ON SEQUENCE file_connect_conid_seq TO biomapsadmin;
GRANT ALL ON SEQUENCE file_connect_conid_seq TO project_admin;


--
-- Name: files_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE files_id_seq FROM PUBLIC;
GRANT ALL ON SEQUENCE files_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE files_id_seq TO biomapsadmin;
GRANT ALL ON SEQUENCE files_id_seq TO project_admin;


SET search_path = shared, pg_catalog;

--
-- Name: weather_data; Type: ACL; Schema: shared; Owner: gisadmin
--

REVOKE ALL ON TABLE weather_data FROM PUBLIC;
REVOKE ALL ON TABLE weather_data FROM gisadmin;
GRANT ALL ON TABLE weather_data TO gisadmin;
GRANT ALL ON TABLE weather_data TO sablon_admin;


SET search_path = system, pg_catalog;

--
-- Name: evaluations; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE evaluations FROM PUBLIC;
REVOKE ALL ON TABLE evaluations FROM gisadmin;
GRANT ALL ON TABLE evaluations TO gisadmin;
GRANT SELECT ON TABLE evaluations TO project_admin;
GRANT ALL ON TABLE evaluations TO biomapsadmin;


--
-- Name: evaluations_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE evaluations_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE evaluations_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE evaluations_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE evaluations_id_seq TO biomapsadmin;


--
-- Name: file_connect; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE file_connect FROM PUBLIC;
REVOKE ALL ON TABLE file_connect FROM gisadmin;
GRANT ALL ON TABLE file_connect TO gisadmin;
GRANT ALL ON TABLE file_connect TO project_admin;
GRANT ALL ON TABLE file_connect TO biomapsadmin;


--
-- Name: files; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE files FROM PUBLIC;
REVOKE ALL ON TABLE files FROM gisadmin;
GRANT ALL ON TABLE files TO gisadmin;
GRANT SELECT ON TABLE files TO project_admin;
GRANT ALL ON TABLE files TO biomapsadmin;


--
-- Name: imports; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE imports FROM PUBLIC;
REVOKE ALL ON TABLE imports FROM gisadmin;
GRANT ALL ON TABLE imports TO gisadmin;
GRANT SELECT ON TABLE imports TO project_admin;


--
-- Name: polygon_users; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE polygon_users FROM PUBLIC;
REVOKE ALL ON TABLE polygon_users FROM gisadmin;
GRANT ALL ON TABLE polygon_users TO gisadmin;
GRANT SELECT ON TABLE polygon_users TO project_admin;
GRANT ALL ON TABLE polygon_users TO biomapsadmin;


--
-- Name: query_buff; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE query_buff FROM PUBLIC;
REVOKE ALL ON TABLE query_buff FROM gisadmin;
GRANT ALL ON TABLE query_buff TO gisadmin;
GRANT SELECT ON TABLE query_buff TO project_admin;
GRANT ALL ON TABLE query_buff TO biomapsadmin;


--
-- Name: query_buff_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE query_buff_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE query_buff_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE query_buff_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE query_buff_id_seq TO biomapsadmin;


--
-- Name: shared_polygons; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE shared_polygons FROM PUBLIC;
REVOKE ALL ON TABLE shared_polygons FROM gisadmin;
GRANT ALL ON TABLE shared_polygons TO gisadmin;
GRANT SELECT ON TABLE shared_polygons TO project_admin;
GRANT ALL ON TABLE shared_polygons TO biomapsadmin;


--
-- Name: shared_polygons_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE shared_polygons_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shared_polygons_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE shared_polygons_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE shared_polygons_id_seq TO biomapsadmin;


--
-- Name: temp_index; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE temp_index FROM PUBLIC;
REVOKE ALL ON TABLE temp_index FROM gisadmin;
GRANT ALL ON TABLE temp_index TO gisadmin;
GRANT ALL ON TABLE temp_index TO project_admin;


--
-- Name: uploadings; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE uploadings FROM PUBLIC;
REVOKE ALL ON TABLE uploadings FROM gisadmin;
GRANT ALL ON TABLE uploadings TO gisadmin;
GRANT SELECT ON TABLE uploadings TO project_admin;
GRANT ALL ON TABLE uploadings TO biomapsadmin;


--
-- Name: uploadings_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE uploadings_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE uploadings_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE uploadings_id_seq TO gisadmin;
GRANT SELECT ON SEQUENCE uploadings_id_seq TO project_admin;
GRANT ALL ON SEQUENCE uploadings_id_seq TO biomapsadmin;


--
-- PostgreSQL database dump complete
--

