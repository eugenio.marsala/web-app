--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE biomapsadmin;
ALTER ROLE biomapsadmin WITH SUPERUSER NOINHERIT CREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5931990ef0e9da8e10b71cb5ae96793d4' VALID UNTIL 'infinity';


CREATE ROLE mainpage_admin;
ALTER ROLE mainpage_admin WITH NOINHERIT NOCREATEDB LOGIN NOREPLICATION PASSWORD 'NoosheiXej3paepe' VALID UNTIL 'infinity';

--
-- Role memberships
--


--
-- PostgreSQL database cluster dump complete
--

