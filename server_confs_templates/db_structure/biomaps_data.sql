\c biomaps;
--
-- Data for Name: db_updates; Type: TABLE DATA; Schema: public; Owner: gisadmin
--


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: gisadmin
-- Password: 12345

COPY users (id, "user", password, status, username, institute, address, email, inviter, validation, reactivate, orcid, "references", givenname, familyname, pwstate, terms_agree) FROM stdin;
1	1d98f5f03cc23fb8cabe964a4dbb758b	$2y$10$L6OStG5Q6DHouVSOazQ7DeazeDnWMAKca7Ax5ykRTtnT6mokjtR5S	1	Valaki Valahol	OpenBioMaps	H-4032, Debrecen, Egyetem tér 1.	valaki@openbiomaps.org	1	1	\N	\N	\N	\N	\N	t	1
\.


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.projects (comment, admin_uid, project_table, creation_date, email, validation, domain, "Creator", stage, doi, running_date, licence, rum, licence_uri, alternate_id, collection_dates, project_hash, subjects, geolocation, local_project) FROM stdin;
Sablon Database:::Sablon Faj Adatbázis	1	sablon	2012-08-21	valaki@openbiomaps.org	1	localhost:9880/projects/sablon	Valaki Valahol	stable	\N	\N	\N	\N	\N	\N	\N	7qaoqtihkd50	\N	\N	t
\.


--
-- Data for Name: header_names; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.header_names (f_table_schema, f_table_name, f_species_column, f_date_columns, f_quantity_column, f_order_columns, f_id_column, f_cite_person_columns, f_x_column, f_y_column, f_srid, f_geom_column, f_alter_speciesname_columns, f_file_id_columns, f_restrict_column, f_main_table) FROM stdin;
public	sablon	faj	{dt_to}	egyedszam	{"sablon":{"adatkozlo":"1","szamossag":"3","gyujto":"4"}}	obm_id	{gyujto}	eov_x	eov_y	4326	obm_geometry	{magyar}	{sablon}	objectid	sablon
\.


--
-- Data for Name: modules; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.modules (project_table, module_name, file, function, enabled, id, params, module_access, main_table, group_access) FROM stdin;
sablon	text_filter	box_filter.php	default	t	319	{obm_taxon,obm_datum}	0	sablon_mirror	{0}
sablon	results_specieslist	\N	default	t	324	{}	0	sablon_mirror	{0}
sablon	box_load_coord	box_load_coord.php	default	t	87	{wgs84:4326,eov=23700}	0	sablon	{0}
sablon	custom_notify	notify.php	default	f	317	{reference_inserted}	0	sablon	{0}
sablon	custom_data_check	custom_data_check.php	default	t	318	{colour_rings,kutyafule}	0	sablon	{0}
sablon	bold_yellow	bold_yellow.php	\N	t	290	{faj,dt_to}	0	sablon	{0}
sablon	secret_column	secret_column.php	\N	t	93	{obm_geometry:geom}	0	sablon	{0}
sablon	results_asList	results_asList.php	default	t	320	{}	0	sablon_mirror	{0}
sablon	allowed_columns	allowed_columns.php	default	t	287	{faj,obm_geometry,obm_id,egyedszam*}	0	sablon	{0}
sablon	default_values	default_values.php	default	f	4	{}	0	sablon	{0}
sablon	identify_point	identify_point.php	default	t	316	{faj,dt_from,dt_to}	0	sablon	{}
sablon	massive_edit	NO FILE	default	t	94	{}	0	sablon	{}
sablon	box_load_last_data	box_load_last_data.php	default	t	315	{}	1	sablon	{0}
sablon	box_load_selection	box_load_selection.php	default	f	78	{}	0	sablon	{0}
sablon	transform_geometries	snap_to_grid.php	default	t	291	{a}	0	sablon	{0}
sablon	results_asCSV	results_asCSV.php	default	t	10	{sep=;,quote='}	0	sablon	{0}
sablon	column_trigger	NOT EXISTS	default	t	79	{}	0	sablon	{0}
sablon	results_asStable	results_asStable.php	default	t	82	{faj,dt_to,egyedszam,meret_kb,szamossag,obm_geometry,obm_id,obm_files_id}	0	sablon	{0}
sablon	results_asTable	results_asTable.php		t	8	\N	0	sablon	{0}
sablon	results_asGPX	results_asGPX.php		t	11	\N	0	sablon	{0}
sablon	results_buttons	results_buttons.php		t	12	\N	0	sablon	{0}
sablon	results_summary	results_summary.php		t	13	\N	0	sablon	{0}
sablon	results_specieslist	results_specieslist.php		t	14	\N	0	sablon	{0}
sablon	results_summary	\N	default	t	322	{}	0	sablon_mirror	{0}
sablon	photos	photos.php		t	1	\N	0	sablon	{0}
sablon	results_asList	results_asList.php		t	7	\N	0	sablon	{0}
sablon	results_buttons	\N	default	t	323	{}	0	sablon_mirror	{0}
sablon	additional_columns	additional_columns.php	private	f	85	{meta_1,dmi.egyedszam}	0	sablon	{0}
sablon	transform_results	transform_data.php	default	t	84	{obm_geometry:geom,obm_uploading_id:uplid,tema:mmm}	0	sablon	{}
sablon	results_asJSON	\N	default	t	332	{}	0	sablon	{0}
sablon	join_tables	join_tables.php	default	f	289	{"LEFT JOIN:sablon_mirror:sablon_mirror.objectid=p.obm_id&sablon_mirror.objectid>1:1"}	1	sablon	{}
sablon	allowed_columns	allowed_columns.php	default	t	331	{faj,obm_geometry,obm_id,egyedszam*}	0	sablon_mirror	{0}
sablon	create_postgres_user	create_postgres_user.php	default	t	329	{}	1	sablon	{21}
sablon	box_custom	\N	default	t	367	{}	0	sablon	{0}
sablon	shared_geometries	box_load_selection.php	default	t	368	{}	0	sablon	{0}
sablon	text_filter	box_filter.php	default	t	73	{obm_taxon,obm_datum,obm_uploading_date,obm_uploader_user,d.szamossag:nested(d.egyedszam):autocomplete,d.egyedszam:values():,obm_files_id,faj}	0	sablon	{0}
sablon	grid_view	\N	default	f	369	{}	0	sablon	{0}
\.


--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.oauth_clients (client_id, client_secret, redirect_uri, grant_types, scope, user_id) FROM stdin;
mobile	123		\N	\N	\N
R	\N		\N	\N	\N
web	web		\N	\N	\N
webprofile	\N		\N	\N	\N
\.


--
-- Data for Name: oauth_scopes; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.oauth_scopes (scope, is_default) FROM stdin;
get_specieslist	f
get_form_list	f
get_form_data	f
get_profile	f
get_data	f
get_history	f
get_report	f
set_rules	f
put_data	f
webprofile	f
get_tables	f
get_project_list	t
\.


--
-- Data for Name: project_descriptions; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_descriptions (projecttable, language, short, long) FROM stdin;
sablon	hu	Teszt Adatbázis	 nyílt biotikai adatbázisának célja a közpénzen gyűjtött természetvédelmi döntéseket segítő alapadatbázis elérhetővé tétele az érdeklődők számára. Az adatbázis a nem regisztrált felhasználók számára a természetvédelmileg érzékeny adatok kivételével szabadon elérhető.
sablon	en	Test Database	Species Database.\nIts contains ~500.000 occurrence data of ~5000 species.\nThe database is public however it is working in testing stage. Please contact the National Park Directorate for recent data.
\.


--
-- Data for Name: project_forms; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_forms (form_id, project_table, form_name, form_type, form_access, user_id, active, groups, description, destination_table, srid, data_owners, data_groups, last_mod, ext_string) FROM stdin;
1	sablon	public upload	{file,web,api}	0	1	1	{}	\N	sablon	{}	\N	{}	2018-09-05 09:43:10.068486	\N
\.


--
-- Data for Name: project_forms_data; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_forms_data (form_id, "column", description, type, control, count, list, obl, fullist, default_value, genlist, api_params, relation, regexp, spatial, pseudo_columns, custom_function, list_definition) FROM stdin;
1	adatkozlo	\N	text	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	szamossag	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "true": [],\n        "false": []\n    }\n}
1	gyujto	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	altema	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": [],\n        "allatok": [\n            "állatok"\n        ],\n        "novenyek": [\n            "növények"\n        ]\n    }\n}
1	dt_from	\N	text	nocheck	{}	\N	2	0	\N	\N	[""]	(altema=allatok){obligatory(1)}	\N	\N	\N	\N	\N
1	dt_to	\N	date	nocheck	{}	\N	1	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	egyedszam	\N	numeric	minmax	{1,50}	\N	1	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	eloford_al	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	elohely	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	eov_x	\N	numeric	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	eov_y	\N	numeric	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	faj	Az állat neve	autocomplete	nocheck	{}	\N	2	0	\N	\N	["sticky"]	\N	\N	\N	\N	\N	{\n    "optionsTable": "sablon",\n    "valueColumn": "faj"\n}
1	obm_files_id	\N	file_id	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	hatarozo	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	him	\N	numeric	minmax	{1,100}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	magyar	\N	text	nocheck	{0,0}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	megj	\N	crings	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	natura2000	\N	boolean	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": [],\n        "false": [],\n        "true": []\n    }\n}
1	obm_geometry	\N	point	nocheck	{}	\N	1	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
\.



--
-- Data for Name: project_layers; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_layers (project_table, layer_name, layer_def, tipus, url, map, name, enabled, id, layer_order, ms_layer, singletile, legend) FROM stdin;
\N	layer_gstreet		Google			Google Streets	f	1	\N	\N	f	f
\N	layer_gphy	type:'google.maps.MapTypeId.TERRAIN','sphericalMercator':'true',numZoomLevels:'20'	Google			Google Physical	f	2	\N	\N	f	f
\N	layer_gsat	type:google.maps.MapTypeId.SATELLITE,'sphericalMercator':'true',numZoomLevels:'20'	Google			Google Satellite	f	3	\N	\N	f	f
\N	layer_osm		OSM			OpenStreetMap	t	4	\N	\N	f	f
sablon	layer_data_biotika	layers:'sablon_points',isBaseLayer:'false',visibility:'true',opacity:'1.0',format:'image/png',transparent:'true',numZoomLevels:'20'	WMS	proxy	default	Sablon Biotika Points	t	5	3	\N	f	f
sablon	layer_data_biotika	layers:'topo',isBaseLayer:'true',visibility:'true',format:'image/png',numZoomLevels:'20'	WMS	/maps/topomap	default	topo	f	6	\N	\N	f	f
\.

--
-- Data for Name: project_mapserver_layers; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY project_mapserver_layers (mapserv_layer_name, project_table, geometry_type) FROM stdin;
sablon_points	sablon	POINT
\.


--
-- Data for Name: project_metaname; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_metaname (project_table, column_name, description, short_name, rights, "order") FROM stdin;
sablon	szamossag	\N	szamossag	{0}	\N
sablon	gyujto	\N	gyujto	{0}	\N
sablon	faj	\N	faj	{0}	\N
sablon	magyar	\N	magyar	{0}	\N
sablon	eov_x	nincs értelme kitölteni	eov X	{0}	\N
sablon	eov_y	nincs értelme kitölteni	eov Y	{0}	\N
sablon	hely	\N	hely	{0}	\N
sablon	meret_kb	\N	meret_kb	{0}	\N
sablon	egyedszam	\N	egyedszam	{0}	\N
sablon	him	\N	hím	{0}	\N
sablon	nosteny	\N	nosteny	{0}	\N
sablon	eloford_al	\N	eloford_al	{0}	\N
sablon	gyujto2	\N	gyujto2	{0}	\N
sablon	gyujto3	\N	gyujto3	{0}	\N
sablon	hatarozo	\N	hatarozo	{0}	\N
sablon	adatkozlo	\N	adatkozlo	{0}	\N
sablon	modsz	\N	modsz	{0}	\N
sablon	elohely	\N	elohely	{0}	\N
sablon	veszteny	\N	veszteny	{0}	\N
sablon	kapcs_dok	\N	kapcs_dok	{0}	\N
sablon	megj	\N	megj	{0}	\N
sablon	rogz_modsz	\N	rogz_modsz	{0}	\N
sablon	altema	\N	altema	{0}	\N
sablon	obm_geometry	srid:4326	obm_geometry	{0}	\N
sablon	natura2000	\N	natura2000	{0}	\N
sablon	vedettseg	\N	vedettseg	{0}	\N
sablon	tema	\N	tema	{0}	\N
sablon	obm_datum	\N	obm_datum	{0}	\N
sablon	dt_from	\N	dt_from	{0}	\N
sablon	dt_to	\N	dátum	{0}	\N
sablon	rogz_dt	\N	rogz_dt	{0}	\N
sablon	obm_files_id	\N	files	{0}	\N
sablon	time	\N	time	{0}	\N
sablon	objectid	\N	objectid	{0}	\N
\.


--
-- Data for Name: project_queries; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_queries (project_table, layer_query, enabled, id, layer_type, rst, layer_cname, main_table) FROM stdin;
sablon	SELECT d.obm_id,obm_geometry\n%selected%\nFROM %F%sablon d%F%\n%rules_join%\n%taxon_join%\n%uploading_join%\n%morefilter%\nWHERE %geometry_type% %envelope% %qstr%	t	37	query	0	layer_data_biotika	sablon
\.


--
-- Data for Name: project_roles; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

--COPY public.project_roles (role_id, container, project_table, description, user_id) FROM stdin;
--1	{1}	sablon	Valaki Valahol	1
--\.


--
-- Data for Name: project_users; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_users (project_table, user_id, user_status, join_date,receive_mails, visible_mail) FROM stdin;
sablon	1	2	2016-07-06 15:38:04.795721+02	1	1
\.


--
-- Data for Name: project_variables; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_variables (project_table, map_center, map_zoom, click_buffer, zoom_wheel, default_results_view, default_layer, fixheader, turn_off_layers, subfilters, langs, legend, training, rserver) FROM stdin;
sablon	0101000020E61000001BD82AC1E238334058569A9482A64740	7	0	f	list	layer_gsat	\N		f	{"faj":"en","magyar":"hu"}	t	f	t
\.


--
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: biomapsadmin
--

COPY public.translations (id, scope, project, lang, const, translation) FROM stdin;
1	global	\N	en	str_about_db	about database
2	global	\N	hu	str_about_db	az adatbázisról
3	global	\N	ro	str_about_db	despre baza de date
4	global	\N	en	str_accepted_name	accepted name
5	global	\N	hu	str_accepted_name	elfogadott név
6	global	\N	ro	str_accepted_name	denumire acceptată
7	global	\N	en	str_access	access
8	global	\N	hu	str_access	hozzáférés
9	global	\N	ro	str_access	acces
10	global	\N	en	str_accuracy	Accuracy (GPS Satellites)
11	global	\N	hu	str_accuracy	Pontosság (műholdak száma)
12	global	\N	ro	str_accuracy	Accuracy (GPS Satellites)
13	global	\N	en	str_actions	actions
14	global	\N	hu	str_actions	műveletek
15	global	\N	ro	str_actions	acțiuni
16	global	\N	en	str_activate	activate
17	global	\N	hu	str_activate	aktivál
18	global	\N	ro	str_activate	activează
19	global	\N	en	str_active	active
20	global	\N	hu	str_active	aktív
21	global	\N	ro	str_active	activ
22	global	\N	en	str_activity	activity
23	global	\N	hu	str_activity	aktivitás
24	global	\N	ro	str_activity	activitate
25	global	\N	en	str_activity_area	activity area
26	global	\N	hu	str_activity_area	aktivitási terület
27	global	\N	ro	str_activity_area	arealul observațiilor încărcate
28	global	\N	en	str_add	add
29	global	\N	hu	str_add	hozzáad
30	global	\N	ro	str_add	adaugă
31	global	\N	en	str_add_features	Add feature
32	global	\N	hu	str_add_features	elem hozzáadása
33	global	\N	ro	str_add_features	adăugare element
34	global	\N	en	str_add_records_to	add records to
35	global	\N	hu	str_add_records_to	adatfeltöltés:
36	global	\N	ro	str_add_records_to	adaugă înregistrări la
37	global	\N	en	str_add_translation	add translation
38	global	\N	hu	str_add_translation	fordítás hozzáadása
39	global	\N	ro	str_add_translation	
40	global	\N	en	str_admin	admin
41	global	\N	hu	str_admin	gazda
42	global	\N	ro	str_admin	administrator
43	global	\N	en	str_administration	administration
44	global	\N	hu	str_administration	adminisztráció
45	global	\N	ro	str_administration	administrație
46	global	\N	en	str_administrator	administrator
47	global	\N	hu	str_administrator	intéző
48	global	\N	ro	str_administrator	administrator
49	global	\N	en	str_agree_and_continue	agree and continue
50	global	\N	hu	str_agree_and_continue	egyetértek és folytatom
51	global	\N	ro	str_agree_and_continue	acceptă și continuă
52	global	\N	en	str_agree_new_terms	To continue using OpenBioMaps, you will need to agree to the updated <a class="termsText" href="" data-target="privacy">Privacy Policy</a>, and <a class="termsText" href="" data-target="terms">Terms and Conditions</a>.
53	global	\N	hu	str_agree_new_terms	Az OpenBioMaps további használatához, egyet kell értsen a módosított <a class="termsText" href="" data-target="privacy">Adatvédelmi Szabályzattal</a> és a <a class="termsText" href="" data-target="terms">Felhasználói Szabályokkal</a>.
54	global	\N	ro	str_agree_new_terms	Pentru continuarea utilizării OpenBirdMaps, va trebui să acceptați  <a class="termsText" href="" data-target="privacy">Politica de confidență</a> și <a class="termsText" href="" data-target="terms">Termenii și condițiile de utilizare</a>.
55	global	\N	en	str_all_match	all names belonging to the selected species
56	global	\N	hu	str_all_match	összes névre ami a kiválasztott fajhoz tartozik
57	global	\N	ro	str_all_match	toate numele care aparțin speciei selectate
58	global	\N	en	str_all_names	load all names
59	global	\N	hu	str_all_names	összes név betöltése
60	global	\N	ro	str_all_names	încărcarea tuturor numelor
61	global	\N	en	str_already_activated_link	If you already activated your profile follow this link to check your profile page.
62	global	\N	hu	str_already_activated_link	Ha már aktiválta a fiókját, kövesse a következő linket a profil oldalára
63	global	\N	ro	str_already_activated_link	Dacă ați activat deja contul urmăriți acest link în vederea verificării paginii de profil
64	global	\N	en	str_alt_name	alternative name
65	global	\N	hu	str_alt_name	nemzeti név
66	global	\N	ro	str_alt_name	nume alternativ
67	global	\N	en	str_alt_names	alternative names
68	global	\N	hu	str_alt_names	alternatív nevek
69	global	\N	ro	str_alt_names	denumiri alternative
70	global	\N	en	str_a_mandatory_field_is_empty	a mandatory field is empty
71	global	\N	hu	str_a_mandatory_field_is_empty	egy kötelező mező üres
72	global	\N	ro	str_a_mandatory_field_is_empty	un câmp obligatoriu
73	global	\N	en	str_any	any
74	global	\N	hu	str_any	bármi
75	global	\N	ro	str_any	orice
76	global	\N	en	str_api_params	api form control parameters
77	global	\N	hu	str_api_params	api űrlap kontroll paraméterek
78	global	\N	ro	str_api_params	formular control parametrii api
79	global	\N	en	str_append	append
80	global	\N	hu	str_append	hozzáfűz
81	global	\N	ro	str_append	atașează
82	global	\N	en	str_apply_text_query	apply reqursive filters
83	global	\N	hu	str_apply_text_query	rekurzív szűrők alkalmazása
84	global	\N	ro	str_apply_text_query	filtrare recursivă
85	global	\N	en	str_arbitrary	arbitray
86	global	\N	hu	str_arbitrary	tetszőleges
87	global	\N	ro	str_arbitrary	arbitrar
88	global	\N	en	str_array	array
89	global	\N	hu	str_array	tömb
90	global	\N	ro	str_array	
91	global	\N	en	str_as_compact_table	compact table
92	global	\N	hu	str_as_compact_table	kompakt táblázat
93	global	\N	ro	str_as_compact_table	tabel compact
94	global	\N	en	str_as_foldable_list	folds
95	global	\N	hu	str_as_foldable_list	kinyitható lista
96	global	\N	ro	str_as_foldable_list	listă expansibilă
97	global	\N	en	str_as_full_table	full table
98	global	\N	hu	str_as_full_table	teljes táblázat
99	global	\N	ro	str_as_full_table	tabel complet
100	global	\N	en	str_attachment	attachment
101	global	\N	hu	str_attachment	csatolmány
102	global	\N	ro	str_attachment	atașament
103	global	\N	en	str_authors	authors
104	global	\N	hu	str_authors	szerzők
105	global	\N	ro	str_authors	autorii
106	global	\N	en	str_available_for	available for
107	global	\N	hu	str_available_for	elérhetőség
108	global	\N	ro	str_available_for	accesibil pentru
109	global	\N	en	str_avoid	avoid
110	global	\N	hu	str_avoid	óvakodjál
111	global	\N	ro	str_avoid	evită
112	global	\N	en	str_ban	ban
113	global	\N	hu	str_ban	tilt
114	global	\N	ro	str_ban	interzice
115	global	\N	en	str_beta_text	This database is under development and is not yet accepted in OpenBioMaps. This is a temporary state!
116	global	\N	hu	str_beta_text	Ez az adatbázis fejlesztés alatt van, nincs még az OpenBioMaps rendszerbe befogadva. Ez egy ideiglenes állapot!
117	global	\N	ro	str_beta_text	Această bază de date este sub construcție sau nu este în forma finală. Nu este încă o bază de date acomodată în sistemul OpenBio Maps. Acesta este un stadiu temporar al bazei de date.
118	global	\N	en	str_biomds	Open Biotic Map and Database Services
119	global	\N	hu	str_biomds	Nyitott biotikai térkép és adatbázis szolgáltatások
120	global	\N	ro	str_biomds	servicii de hărți și baze de date biotice deschise
121	global	\N	en	str_black	black
122	global	\N	hu	str_black	fekete
123	global	\N	ro	str_black	negru
124	global	\N	en	str_blue	blue
125	global	\N	hu	str_blue	kék
126	global	\N	ro	str_blue	albastru
127	global	\N	en	str_boolen	true/false
128	global	\N	hu	str_boolen	igaz/hamis
129	global	\N	ro	str_boolen	adevărat/fals
130	global	\N	en	str_brown	brown
131	global	\N	hu	str_brown	barna
132	global	\N	ro	str_brown	maroniu
133	global	\N	en	str_buffersize	clikk buffer size
134	global	\N	hu	str_buffersize	klikk buffer méret
135	global	\N	ro	str_buffersize	click mărime buffer
136	global	\N	en	str_cancel	cancel
137	global	\N	hu	str_cancel	mégse
138	global	\N	ro	str_cancel	renunțare
139	global	\N	en	str_center_x	map center X
140	global	\N	hu	str_center_x	térkép középpont X
141	global	\N	ro	str_center_x	centru X
142	global	\N	en	str_center_y	map center Y
143	global	\N	hu	str_center_y	térkép középpont Y
144	global	\N	ro	str_center_y	centru Y
145	global	\N	en	str_changes	changes
146	global	\N	hu	str_changes	változások
147	global	\N	ro	str_changes	schimbări
148	global	\N	en	str_characters	characters
149	global	\N	hu	str_characters	karakter
150	global	\N	ro	str_characters	caractere
151	global	\N	en	str_choose	choose
152	global	\N	hu	str_choose	kiválaszt
153	global	\N	ro	str_choose	selectează
154	global	\N	en	str_choose_file	choose file
155	global	\N	hu	str_choose_file	válasszon fájlt
156	global	\N	ro	str_choose_file	selectează fișierul
157	global	\N	en	str_choose_mtable	choose table
158	global	\N	hu	str_choose_mtable	válasszon táblát
159	global	\N	ro	str_choose_mtable	selectează tabelul
160	global	\N	en	str_citations	citations
161	global	\N	hu	str_citations	hivatkozások
162	global	\N	ro	str_citations	citații
163	global	\N	en	str_cite_person	citing
164	global	\N	hu	str_cite_person	hivatkozás
165	global	\N	ro	str_cite_person	citare
166	global	\N	en	str_collector	deliverer/Collector
167	global	\N	hu	str_collector	gyüjtő
168	global	\N	ro	str_collector	colectat de
169	global	\N	en	str_column	column
170	global	\N	hu	str_column	oszlop
171	global	\N	ro	str_column	coloană
172	global	\N	en	str_columnorder	columns' order
173	global	\N	hu	str_columnorder	oszlop sorrend
174	global	\N	ro	str_columnorder	ordinea coloanelor
175	global	\N	en	str_columns	columns
176	global	\N	hu	str_columns	oszlopok
177	global	\N	ro	str_columns	coloane
178	global	\N	en	str_comment	comment
179	global	\N	hu	str_comment	megjegyzés
180	global	\N	ro	str_comment	mențiune
181	global	\N	en	str_comments	comments
182	global	\N	hu	str_comments	megjegyzések
183	global	\N	ro	str_comments	mențiuni
184	global	\N	en	str_common_name	common name
185	global	\N	hu	str_common_name	közönséges név
186	global	\N	ro	str_common_name	denumire populară
187	global	\N	en	str_confirm	confirm
188	global	\N	hu	str_confirm	megerősít
189	global	\N	ro	str_confirm	confirmă
190	global	\N	en	str_confirmation_email_sending_failed	Sorry, we could not send a confirmation email, please contact your system administrator!
191	global	\N	hu	str_confirmation_email_sending_failed	Sajnáljuk, nem tudtunk visszaigazoló e-mailt küldeni, kérjük, lépjen kapcsolatba rendszergazdájával.
192	global	\N	ro	str_confirmation_email_sending_failed	Ne pare rău, dar nu s-a reușit trimiterea unui e-mail pentru confirmarea ștergerii contului. Vă rugăm contactați administratorii!
193	global	\N	en	str_confirmation_email_sent	Confirmation email sent! Please check your mailbox and follow the link to remove your profile.
194	global	\N	hu	str_confirmation_email_sent	Megerősítő email elküldve! Kérjük, ellenőrizze a postaládáját, és kövesse a linket a profil eltávolításához.
195	global	\N	ro	str_confirmation_email_sent	Mailul de confirmare a fost trimis! Vă rugăm verificați adresa Dumneavoastră de e-mail și accesați linkul din e-mailul primit pentru a finaliza ștergerea contului de utilizator.
196	global	\N	en	str_confirmation_no	Oh, no!
197	global	\N	hu	str_confirmation_no	Nem, dehogy!
198	global	\N	ro	str_confirmation_no	Nu!
199	global	\N	en	str_confirmation_yes	Yes, I am sure!
200	global	\N	hu	str_confirmation_yes	Igen, biztosan!
201	global	\N	ro	str_confirmation_yes	Da, sunt sigur!
202	global	\N	en	str_conf_necessary	a confirmation is required to modify it
203	global	\N	hu	str_conf_necessary	a módosításhoz megerősítés szükséges
204	global	\N	ro	str_conf_necessary	confirmare necesară
205	global	\N	en	str_connectionerror	Database connection error
206	global	\N	hu	str_connectionerror	Sikertelen csatlakozás az adatbázishoz
207	global	\N	ro	str_connectionerror	conectarea la baza de date a eșuat
208	global	\N	en	str_connections	connections
209	global	\N	hu	str_connections	kapcsolatok
210	global	\N	ro	str_connections	conexiuni
211	global	\N	en	str_constant_name	constant's name
212	global	\N	hu	str_constant_name	konstans neve
213	global	\N	ro	str_constant_name	
214	global	\N	en	str_constraints	constraints
215	global	\N	hu	str_constraints	megszorítások
216	global	\N	ro	str_constraints	
217	global	\N	en	str_contact	contact
218	global	\N	hu	str_contact	kapcsolat
219	global	\N	ro	str_contact	contact
220	global	\N	en	str_contact_the_maintainers	If you have any questions or need help, please contact the database maintainers at
221	global	\N	hu	str_contact_the_maintainers	Ha kérdése van vagy segítségre van szüksége, vegye fel a kapcsolatot az adminisztrátorokkal a
222	global	\N	ro	str_contact_the_maintainers	Dacă aveți întrebări sau aveți nevoie de ajutor, vă rugăm contactați administratorii pe adresa
223	global	\N	en	str_contains	contains
224	global	\N	hu	str_contains	tartalmazza
225	global	\N	ro	str_contains	conține
226	global	\N	en	str_continue_interrupted_imports	List of saved import entries. Imports can be continued or discarded here.
227	global	\N	hu	str_continue_interrupted_imports	Elmentett adat importolálások listája. A listában található adatbetöltéseket innen be lehet fejezni, vagy eldobni.
228	global	\N	ro	str_continue_interrupted_imports	Lista încărcărilor de date salvate. De aici pot fi continuate sau șterse încărcările de date salvate. 
229	global	\N	en	str_contrib_partner	contributitor partners
230	global	\N	hu	str_contrib_partner	együttműködő partnerek
231	global	\N	ro	str_contrib_partner	parteneri
232	global	\N	en	str_control	control
233	global	\N	hu	str_control	kontrol
234	global	\N	ro	str_control	control
235	global	\N	en	str_cookies	cookies
236	global	\N	hu	str_cookies	cookies
237	global	\N	ro	str_cookies	cookie-uri
238	global	\N	en	str_coordinate_transformation_failed	coordinate transformation unsuccessful
239	global	\N	hu	str_coordinate_transformation_failed	koordináta transzformáció sikertelen
240	global	\N	ro	str_coordinate_transformation_failed	transformarea coordonatelor a eșuat
241	global	\N	en	str_coordsys	coordinate system
242	global	\N	hu	str_coordsys	koordináta rendszer
243	global	\N	ro	str_coordsys	sistem de coordonate
244	global	\N	en	str_coord_view	view coordinates
245	global	\N	hu	str_coord_view	koordináta megmutatása
246	global	\N	ro	str_coord_view	vizualizează coordonatele
247	global	\N	en	str_copy_paste_link	If the links above are not clickable, try to copy and paste them into the address bar of your web browser.
248	global	\N	hu	str_copy_paste_link	Ha a fenti linkek nem kattinthatóak, másolja őket a böngészőbe.
249	global	\N	ro	str_copy_paste_link	Dacă linkurile de mai sus nu sunt active, copiați-le în browser.
250	global	\N	en	str_correctly	correctly
251	global	\N	hu	str_correctly	helyesen
252	global	\N	ro	str_correctly	corect
253	global	\N	en	str_create	create
254	global	\N	hu	str_create	létrehoz
255	global	\N	ro	str_create	creează
256	global	\N	en	str_create_new_password_link	If you didn't log in after you accept your invitation, you have to create a new password. Click the following link to create new password
257	global	\N	hu	str_create_new_password_link	Ha nem jelentkezett be miután elfogadta a meghívót, egy új jelszót kell megadnia. Ehhez kattintson az alábbi linkre.
258	global	\N	ro	str_create_new_password_link	Dacă nu v-ați logat după ce ați acceptat invitația, trebuie să creeați o parolă nouă. Urmăriți linkul următor pentru a crea o parolă nouă
259	global	\N	en	str_create_pg_user	create postgres user
260	global	\N	hu	str_create_pg_user	postgres felhasználó készítése
261	global	\N	ro	str_create_pg_user	creare utilizator postgres
262	global	\N	en	str_create_table	create table
263	global	\N	hu	str_create_table	tábla létrehozása
264	global	\N	ro	str_create_table	creare tabel
265	global	\N	en	str_crosses	crosses
266	global	\N	hu	str_crosses	keresztezi
267	global	\N	ro	str_crosses	crosses
268	global	\N	en	str_currentstate	continue later
269	global	\N	hu	str_currentstate	későbbi folytatás
270	global	\N	ro	str_currentstate	continuă mai târziu
271	global	\N	en	str_data	data
272	global	\N	hu	str_data	adat
273	global	\N	ro	str_data	dată
274	global	\N	en	str_database	database
275	global	\N	hu	str_database	adatbázis
276	global	\N	ro	str_database	bază de date
277	global	\N	en	str_data_evaluation	data evaluation
278	global	\N	hu	str_data_evaluation	adat véleményezés
279	global	\N	ro	str_data_evaluation	evaluarea observației
280	global	\N	en	str_dataheader	data header
281	global	\N	hu	str_dataheader	adat fejléc
282	global	\N	ro	str_dataheader	alte informații referitoare la observație
283	global	\N	en	str_datahistorypage	Data history page
284	global	\N	hu	str_datahistorypage	Adat történeti lap
285	global	\N	ro	str_datahistorypage	istoric date
286	global	\N	en	str_data_mod	data modification
287	global	\N	hu	str_data_mod	adat módosítás
288	global	\N	ro	str_data_mod	modificare date
289	global	\N	en	str_data_p	data points
290	global	\N	hu	str_data_p	adat pont
291	global	\N	ro	str_data_p	observații
292	global	\N	en	str_datapage	data page
293	global	\N	hu	str_datapage	adat lap
294	global	\N	ro	str_datapage	pagina cu date
295	global	\N	en	str_data_providers	data providers
296	global	\N	hu	str_data_providers	adatközlők
297	global	\N	ro	str_data_providers	sursa datelor
298	global	\N	en	str_data_query	data query
299	global	\N	hu	str_data_query	adat lekérdezés
300	global	\N	ro	str_data_query	interogare date
301	global	\N	en	str_datasheet	datasheet
302	global	\N	hu	str_datasheet	adatlap
303	global	\N	ro	str_datasheet	datasheet
304	global	\N	en	str_data_summary	data summary
305	global	\N	hu	str_data_summary	adat összefoglaló
306	global	\N	ro	str_data_summary	sumar de date
307	global	\N	en	str_dataupid	Uploading ID
308	global	\N	hu	str_dataupid	Adatfeltöltés azonosítója
309	global	\N	ro	str_dataupid	identificator încărcare de date
310	global	\N	en	str_datauploader	data uploader
311	global	\N	hu	str_datauploader	adatfeltöltő
312	global	\N	ro	str_datauploader	persoana care a încărcat observația
313	global	\N	en	str_data_upload_from_file	Data upload from file
314	global	\N	hu	str_data_upload_from_file	Adat felvitel fájlból
315	global	\N	ro	str_data_upload_from_file	încărcare date din fișier
316	global	\N	en	str_datauptime	uploading time
317	global	\N	hu	str_datauptime	adatfeltöltés időpontja
318	global	\N	ro	str_datauptime	data încărcării observației
319	global	\N	en	str_date	date
320	global	\N	hu	str_date	dátum
321	global	\N	ro	str_date	data
322	global	\N	en	str_date_period	date period
323	global	\N	hu	str_date_period	dátum időszak
324	global	\N	ro	str_date_period	perioadă
325	global	\N	en	str_datetime	date and time
326	global	\N	hu	str_datetime	dátum és idő
327	global	\N	ro	str_datetime	data și ora
328	global	\N	en	str_datum_filter	date filters
329	global	\N	hu	str_datum_filter	dátum szűrők
330	global	\N	ro	str_datum_filter	filtru dată
331	global	\N	en	str_db	database
332	global	\N	hu	str_db	adatbázis
333	global	\N	ro	str_db	bază de date
334	global	\N	en	str_db_cols	database columns
335	global	\N	hu	str_db_cols	adatbázis oszlopok
336	global	\N	ro	str_db_cols	coloane bază de date
337	global	\N	en	str_db_download	download database
338	global	\N	hu	str_db_download	adatbázis letöltése
339	global	\N	ro	str_db_download	descărcare bază de date
340	global	\N	en	str_dbs	databases
341	global	\N	hu	str_dbs	adatbázisok
342	global	\N	ro	str_dbs	baze de date
343	global	\N	en	str_db_summary	database summary
344	global	\N	hu	str_db_summary	adatbázis összefoglaló
345	global	\N	ro	str_db_summary	sumar bază de date
346	global	\N	en	str_dear	dear
347	global	\N	hu	str_dear	kedves
348	global	\N	ro	str_dear	stimate
349	global	\N	en	str_decimal_number	decimal number
350	global	\N	hu	str_decimal_number	tizedes érték
351	global	\N	ro	str_decimal_number	
352	global	\N	en	str_decline	decline
353	global	\N	hu	str_decline	elutasítom
354	global	\N	ro	str_decline	respinge
355	global	\N	en	str_decrease_text_size	decrease fonts
356	global	\N	hu	str_decrease_text_size	betűket csökkent
357	global	\N	ro	str_decrease_text_size	micșorează caracterele
358	global	\N	en	str_default_lang	default language
359	global	\N	hu	str_default_lang	alapértelmezett nyelv
360	global	\N	ro	str_default_lang	limba prestabilită
361	global	\N	en	str_default_value	default value
362	global	\N	hu	str_default_value	alapértelmezett érték
363	global	\N	ro	str_default_value	
364	global	\N	en	str_default_values	default values
365	global	\N	hu	str_default_values	alap értékek
366	global	\N	ro	str_default_values	valori predefinite
367	global	\N	en	str_define	define
368	global	\N	hu	str_define	definiál
369	global	\N	ro	str_define	definește
370	global	\N	en	str_definition	definition
371	global	\N	hu	str_definition	definíció
372	global	\N	ro	str_definition	definiție
373	global	\N	en	str_definitions	definitions
374	global	\N	hu	str_definitions	definíciók
375	global	\N	ro	str_definitions	definiții
376	global	\N	en	str_delete	delete
377	global	\N	hu	str_delete	töröl
378	global	\N	ro	str_delete	șterge
379	global	\N	en	str_description	description
380	global	\N	hu	str_description	leírás
381	global	\N	ro	str_description	descriere
382	global	\N	en	str_desktop_access	Database access from desktop applications
383	global	\N	hu	str_desktop_access	Adatbázis elérés asztali alkalmazásokból
384	global	\N	ro	str_desktop_access	accesare bază de date de pe aplicații desktop
385	global	\N	en	str_details	details
386	global	\N	hu	str_details	részletek
387	global	\N	ro	str_details	detalii
388	global	\N	en	str_deutsch	Deutsch
389	global	\N	hu	str_deutsch	Német
390	global	\N	ro	str_deutsch	germană
391	global	\N	en	str_dialog	dialog
392	global	\N	hu	str_dialog	párbeszéd
393	global	\N	ro	str_dialog	dialog
394	global	\N	en	str_diameter	Click query's diameter
395	global	\N	hu	str_diameter	Klikk lekérdezés átmérője
396	global	\N	ro	str_diameter	click diametru interogare
397	global	\N	en	str_dinpi	Duna-Ipoly National Park Board of Directors
398	global	\N	hu	str_dinpi	Duna-Ipoly Nemzeti Park Igazgatóság
399	global	\N	ro	str_dinpi	Directoratul Parcului Național Duna-Ipoly
400	global	\N	en	str_discl	Disclaimer
401	global	\N	hu	str_discl	Jogi nyilatkozat
402	global	\N	ro	str_discl	avertisment legal (disclaimer)
403	global	\N	en	str_disjoint	disjoint
404	global	\N	hu	str_disjoint	kivűl esik
405	global	\N	ro	str_disjoint	nu conține
406	global	\N	en	str_dist_explan	string distance value where the 0 means the exact match and 1 is the largest distance
407	global	\N	hu	str_dist_explan	Sztring távolság érték, ahol a 0 a pontos egyezés és 1 a legnagyobb távolság
408	global	\N	ro	str_dist_explan	valoare distanță string, unde valoare 0 denotă potrivire exactă și 1 este distanța cea mai mare
409	global	\N	en	str_dnlike	I don't like!
410	global	\N	hu	str_dnlike	nem jó!
411	global	\N	ro	str_dnlike	nu îmi place!
412	global	\N	en	str_domain	domain
413	global	\N	hu	str_domain	domain
414	global	\N	ro	str_domain	domain
415	global	\N	en	str_done	done
416	global	\N	hu	str_done	kész
417	global	\N	ro	str_done	rezolvat
418	global	\N	en	str_download	download
419	global	\N	hu	str_download	letöltés
420	global	\N	ro	str_download	descărcare
421	global	\N	en	str_downloadascsv	Download results as csv
422	global	\N	hu	str_downloadascsv	Adatok letöltése csv formátumban
423	global	\N	ro	str_downloadascsv	salvează datele în format .csv
424	global	\N	en	str_downloadasgpx	Download results as gpx
425	global	\N	hu	str_downloadasgpx	Adatok letöltése gpx formátumban
426	global	\N	ro	str_downloadasgpx	salvează datele în format .gpx
427	global	\N	en	str_download_full_size	download full size
428	global	\N	hu	str_download_full_size	letöltés teljes méretben
429	global	\N	ro	str_download_full_size	descărcare în mărimea originală
430	global	\N	en	str_downloads	downloads
431	global	\N	hu	str_downloads	letöltések
432	global	\N	ro	str_downloads	descărcări
433	global	\N	en	str_download_specieslist	download summary
434	global	\N	hu	str_download_specieslist	összefoglaló letöltése
435	global	\N	ro	str_download_specieslist	download summary
436	global	\N	en	str_drop_profile	drop profile
437	global	\N	hu	str_drop_profile	felhasználói fiók megszűntetése
438	global	\N	ro	str_drop_profile	ștergrea contului de utilizator
439	global	\N	en	str_drop_profile_confirm_text	Do you really want to delete your OpenBioMaps profile from this server???<br>You can't log in and use private services on this server till you will not receive an other invitation!
440	global	\N	hu	str_drop_profile_confirm_text	Tényleg le akarod törölni az OpenBioMaps fiókodat erről a szerverről???<br>Nem fogsz tudni bejelentkezni és használni a privát szolgáltatásokat ezen a szerveren amíg valaki újra nem küld meghívót!
441	global	\N	ro	str_drop_profile_confirm_text	Sunteți siguri că doriți să ștergeți contul Dumneavoastră de utilizator OpenBirdMaps???<br>Nu veți putea intra în OpenBirdMaps și nu veți putea utiliza serviciile disponibile utilizatorilor logați până când nu primiți o invitație nouă!
442	global	\N	en	str_drop_seleted_items	drop selected items
443	global	\N	hu	str_drop_seleted_items	kiválasztott elemek eldobása
444	global	\N	ro	str_drop_seleted_items	aruncă obiectele selectate
445	global	\N	en	str_dropselinv	drop selected invites
446	global	\N	hu	str_dropselinv	kiválasztott meghívások eldobása
447	global	\N	ro	str_dropselinv	ștergerea invitațiilor selectate
448	global	\N	en	str_dutch	Dutch
449	global	\N	hu	str_dutch	Holland
450	global	\N	ro	str_dutch	olandeză
451	global	\N	en	str_edit	edit
452	global	\N	hu	str_edit	szerkeszt
453	global	\N	ro	str_edit	editează
454	global	\N	en	str_edit_options	edit options
455	global	\N	hu	str_edit_options	szerkesztési opciók
456	global	\N	ro	str_edit_options	opțiuni de editare
457	global	\N	en	str_edit_these_data	edit these data
458	global	\N	hu	str_edit_these_data	ezen adatok szerkesztése
459	global	\N	ro	str_edit_these_data	editează aceste date
460	global	\N	en	str_email	e-mail address
461	global	\N	hu	str_email	e-mail cím
462	global	\N	ro	str_email	adresă e-mail
463	global	\N	en	str_email_receive	receive mails from the project
464	global	\N	hu	str_email_receive	emailek a projekttől
465	global	\N	ro	str_email_receive	Doriți să primiți email-uri despre baza de date?
466	global	\N	en	str_email_visibility	visibile email address for project members
467	global	\N	hu	str_email_visibility	email cím látható a projekt tagoknak
468	global	\N	ro	str_email_visibility	Permiteți vizualizarea adresei de e-mail membrilor înregistrați?
469	global	\N	en	str_enabled	enabled
470	global	\N	hu	str_enabled	engedélyezett
471	global	\N	ro	str_enabled	admis
472	global	\N	en	str_english	English
473	global	\N	hu	str_english	Angol
474	global	\N	ro	str_english	engleză
475	global	\N	en	str_enter	Enter
476	global	\N	hu	str_enter	Belépés
477	global	\N	ro	str_enter	intrare
478	global	\N	en	str_everyone	everyone
479	global	\N	hu	str_everyone	bárki
480	global	\N	ro	str_everyone	oricine
481	global	\N	en	str_exact_date	exact date
482	global	\N	hu	str_exact_date	pontos dátum
483	global	\N	ro	str_exact_date	data exactă
484	global	\N	en	str_exact_match	exact match
485	global	\N	hu	str_exact_match	pontos egyezés
486	global	\N	ro	str_exact_match	potrivire exactă
487	global	\N	en	str_excel_days	excel days
488	global	\N	hu	str_excel_days	excel napok
489	global	\N	ro	str_excel_days	zile excel
490	global	\N	en	str_excel_time	excel time
491	global	\N	hu	str_excel_time	excel idő
492	global	\N	ro	str_excel_time	timp excel
493	global	\N	en	str_expired	expired
494	global	\N	hu	str_expired	lejárt
495	global	\N	ro	str_expired	expirat
496	global	\N	en	str_external_refs	external references
497	global	\N	hu	str_external_refs	külső hivatkozások
498	global	\N	ro	str_external_refs	referințe externe
499	global	\N	en	str_failed_to_assemble_query_string	Failed to assemble the query string
500	global	\N	hu	str_failed_to_assemble_query_string	Nem sikerült összerakni a lekérdezést
501	global	\N	ro	str_failed_to_assemble_query_string	
502	global	\N	en	str_false	false
503	global	\N	hu	str_false	nem
504	global	\N	ro	str_false	nu
505	global	\N	en	str_family_name	family name
506	global	\N	hu	str_family_name	családnév
507	global	\N	ro	str_family_name	nume
508	global	\N	en	str_example_fields	example data fields
509	global	\N	hu	str_example_fields	példa adatmezők
510	global	\N	ro	str_example_fields	câmpuri de date
511	global	\N	en	str_file	file
512	global	\N	hu	str_file	fájl
513	global	\N	ro	str_file	fișier
514	global	\N	en	str_file_manager	file manager
515	global	\N	hu	str_file_manager	fájl kezelő
516	global	\N	ro	str_file_manager	manipulare fișiere
517	global	\N	en	str_file_upload	file upload
518	global	\N	hu	str_file_upload	fájl feltöltés
519	global	\N	ro	str_file_upload	încărcare fișier
520	global	\N	en	str_filter	filter
521	global	\N	hu	str_filter	szűrés
522	global	\N	ro	str_filter	filtrare
523	global	\N	en	str_filters_in_this_query	Filters in this query
524	global	\N	hu	str_filters_in_this_query	Szűrők a lekérdezésben
525	global	\N	ro	str_filters_in_this_query	filtre în această interogare
526	global	\N	en	str_font_size	font size
527	global	\N	hu	str_font_size	betű méret
528	global	\N	ro	str_font_size	mărime caractere
529	global	\N	en	str_form	form
530	global	\N	hu	str_form	ürlap
531	global	\N	ro	str_form	formular
532	global	\N	en	str_formname	form name
533	global	\N	hu	str_formname	űrlap név
534	global	\N	ro	str_formname	numele formularului
535	global	\N	en	str_formtype	form type
536	global	\N	hu	str_formtype	űrlap típus
537	global	\N	ro	str_formtype	tipul formularului
538	global	\N	en	str_foundation	founded
539	global	\N	hu	str_foundation	alapítva
540	global	\N	ro	str_foundation	fondat
541	global	\N	en	str_found_names	found names
542	global	\N	hu	str_found_names	Talált nevek
543	global	\N	ro	str_found_names	denumiri găsite
544	global	\N	en	str_french	French
545	global	\N	hu	str_french	Francia
546	global	\N	ro	str_french	franceză
547	global	\N	en	str_from	from
548	global	\N	hu	str_from	-tól
549	global	\N	ro	str_from	de la
550	global	\N	en	str_fulllist	full list
551	global	\N	hu	str_fulllist	teljes lista
552	global	\N	ro	str_fulllist	listă completă
553	global	\N	en	str_function	functions
554	global	\N	hu	str_function	függvények
555	global	\N	ro	str_function	funcții
556	global	\N	en	str_geomdesc	geometry managagement interface
557	global	\N	hu	str_geomdesc	geometria kezelési felület
558	global	\N	ro	str_geomdesc	interfața de manipulare a geometriilor încărcate
559	global	\N	en	str_geometry	geometry
560	global	\N	hu	str_geometry	geometria
561	global	\N	ro	str_geometry	geometrie
562	global	\N	en	str_geometry_type	geometry type
563	global	\N	hu	str_geometry_type	geometria típus
564	global	\N	ro	str_geometry_type	tip geometrie
565	global	\N	en	str_geometry_verification_failed	geometry verification failed
566	global	\N	hu	str_geometry_verification_failed	érvénytelen geometria
567	global	\N	ro	str_geometry_verification_failed	geometrie invalidă
568	global	\N	en	str_geom_x	X coordinate
569	global	\N	hu	str_geom_x	X koordináta
570	global	\N	ro	str_geom_x	coordonata X
571	global	\N	en	str_geom_y	Y coordinate
572	global	\N	hu	str_geom_y	Y koordináta
573	global	\N	ro	str_geom_y	coordonata Y
574	global	\N	en	str_get_coordinates_from_map	get coordinates from map
575	global	\N	hu	str_get_coordinates_from_map	koordiniták térképről
576	global	\N	ro	str_get_coordinates_from_map	desenează geometrie pe hartă
577	global	\N	en	str_get_geometry_from_named_list	get geometry from named list
578	global	\N	hu	str_get_geometry_from_named_list	geometria listából
579	global	\N	ro	str_get_geometry_from_named_list	geometrie din lista de geometrii salvate
580	global	\N	en	str_given_name	given name
581	global	\N	hu	str_given_name	keresztnév
582	global	\N	ro	str_given_name	prenume
583	global	\N	en	str_global_translations	global translations
584	global	\N	hu	str_global_translations	globális fordítások
585	global	\N	ro	str_global_translations	
586	global	\N	en	str_green	green
587	global	\N	hu	str_green	zöld
588	global	\N	ro	str_green	verde
589	global	\N	en	str_group	group
590	global	\N	hu	str_group	csoport
591	global	\N	ro	str_group	grup
592	global	\N	en	str_groupcreateexplain	Set the groups' rights here
593	global	\N	hu	str_groupcreateexplain	A csoport jogosultságok itt beállíthatóak
594	global	\N	ro	str_groupcreateexplain	setați drepturile grupului aici
595	global	\N	en	str_groupmembercreateexplain	Set the group's members here
596	global	\N	hu	str_groupmembercreateexplain	A csoport tagságok itt beállíthatóak
597	global	\N	ro	str_groupmembercreateexplain	setați membrii grupului aici
598	global	\N	en	str_groupped_fill	groupped fill
599	global	\N	hu	str_groupped_fill	csoportosított kitöltés
600	global	\N	ro	str_groupped_fill	completare grupată
601	global	\N	en	str_groups	groups
602	global	\N	hu	str_groups	csoportok
603	global	\N	ro	str_groups	grupuri
604	global	\N	en	str_header_information	header information
605	global	\N	hu	str_header_information	fejléc információk
606	global	\N	ro	str_header_information	informații încărcare
607	global	\N	en	str_help	Help
608	global	\N	hu	str_help	Segítség
609	global	\N	ro	str_help	ajutor
610	global	\N	en	str_hidden	hidden
611	global	\N	hu	str_hidden	rejtett
612	global	\N	ro	str_hidden	ascuns
613	global	\N	en	str_hungarian	Hungarian
614	global	\N	hu	str_hungarian	Magyar
615	global	\N	ro	str_hungarian	maghiară
616	global	\N	en	str_hun_name	Hungarian name
617	global	\N	hu	str_hun_name	Magyar név
618	global	\N	ro	str_hun_name	denumire maghiară
619	global	\N	en	str_identify	identify
620	global	\N	hu	str_identify	azonosít
621	global	\N	ro	str_identify	identifică
622	global	\N	en	str_if_defined_xy	If defined X,Y coords or Like this: [[1,2],[2,3],[2,1]])
623	global	\N	hu	str_if_defined_xy	Ha meg vannak adva koordináták vagy ilyenek: [[1,2],[2,3],[2,1]])
624	global	\N	ro	str_if_defined_xy	dacă sunt definite coordonatele X, Y sau : [[1,2],[2,3],[2,1]]
625	global	\N	en	str_iflogin	If you are signed in, you can save the query or download the results.
626	global	\N	hu	str_iflogin	Ha be lenne jelentkezve eltárolhatná a lekérdezést vagy letölthetné az eredményeket.
627	global	\N	ro	str_iflogin	Din cauza manipulării datelor sensibile detaliile pot fi accesate doar după autentificare. <br> Puteți salva căutarea avansată și descărca rezultatele numai după înregistrare.
628	global	\N	en	str_import	import
629	global	\N	hu	str_import	importálás
630	global	\N	ro	str_import	importă
631	global	\N	en	str_import_data	import data
632	global	\N	hu	str_import_data	adatok feltöltése
633	global	\N	ro	str_import_data	încărcare date
634	global	\N	en	str_inaccurate_name	inaccurate name
635	global	\N	hu	str_inaccurate_name	pontatlan név
636	global	\N	ro	str_inaccurate_name	
637	global	\N	en	str_included	included
638	global	\N	hu	str_included	tartalmazza
639	global	\N	ro	str_included	conține
640	global	\N	en	str_increase_text_size	increase fonts
641	global	\N	hu	str_increase_text_size	betűket növel
642	global	\N	ro	str_increase_text_size	crește caracterele
643	global	\N	en	str_increment	increment
644	global	\N	hu	str_increment	növekmény
645	global	\N	ro	str_increment	creștere
646	global	\N	en	str_initialzoom	initial zoom
647	global	\N	hu	str_initialzoom	kezdeti nagyítás
648	global	\N	ro	str_initialzoom	zoom inițial
649	global	\N	en	str_input	input
650	global	\N	hu	str_input	bevitel
651	global	\N	ro	str_input	introducere
652	global	\N	en	str_in_row	row
653	global	\N	hu	str_in_row	sorban
654	global	\N	ro	str_in_row	rândul
655	global	\N	en	str_institute	Institute
656	global	\N	hu	str_institute	Intézmény
657	global	\N	ro	str_institute	instituție
658	global	\N	en	str_integer	integer
659	global	\N	hu	str_integer	egész szám
660	global	\N	ro	str_integer	
661	global	\N	en	str_intersects	intersects
662	global	\N	hu	str_intersects	metszi
663	global	\N	ro	str_intersects	intersectează
664	global	\N	en	str_in_the	in the
665	global	\N	hu	str_in_the	a
666	global	\N	ro	str_in_the	
667	global	\N	en	str_inthisquerythefollowing	List of collectors
668	global	\N	hu	str_inthisquerythefollowing	A lekérdezésben az alábbi gyüjtők és adatközlők adatai szerepelnek
669	global	\N	ro	str_inthisquerythefollowing	Lista observatorilor din interogare
670	global	\N	en	str_inv_code	Invitation code
671	global	\N	hu	str_inv_code	Meghívó kód
672	global	\N	ro	str_inv_code	cod invitație
673	global	\N	en	str_invitate	An invitation  is required to sign up. Only <a href="?database#members">members</a> can send invitations. The registration and the database usage are free. For further assistance, please contact the database  <a href="?database#maintainers">maintainers</a> or its <a href="?database#founder">founder</a>.
854	global	\N	ro	str_message	mesaj
674	global	\N	hu	str_invitate	A regisztrációhoz meghívásra van szükség. Csak <a href="?database#members">tagok</a> küldhetnek meghívókat. A regisztráció és az adatbázis használata ingyenes. További segítségért kérjük, lépjen kapcsolatba az adatbázis <a href="?database#maintainers">üzemeltetőivel</a> vagy az <a href="?database#founder">alapítójával</a>.
675	global	\N	ro	str_invitate	Înregistrarea la baza de date se face pe bază de invitație. Invitație poate fi trimisă de oricare <a href="?database#members">membru</a> a bazei de date. Înregistrarea și folosirea bazei de date este gratis. Pentru asistență vă rugăm contactați <a href="?database#maintainers">maintainers</a> or its <a href="?database#founder">founder</a>.
676	global	\N	en	str_invitation	invitation
677	global	\N	hu	str_invitation	meghívás
678	global	\N	ro	str_invitation	invitație
679	global	\N	en	str_invitation_accomplished	Invitation accomplished
680	global	\N	hu	str_invitation_accomplished	A meghívó küldése sikeres volt
681	global	\N	ro	str_invitation_accomplished	Invitație cu succes
682	global	\N	en	str_invitation_success_but_use_your_existing_account_to_login	CONGRATULATION! You have joined the %PROJECTTABLE% database! Please note, that this email address is used as a username on this server for another database. Use that password to log in. If you do not remember the password, use the forgotten password option to create an other.
683	global	\N	hu	str_invitation_success_but_use_your_existing_account_to_login	GRATULÁLUNK! Csatlakozott a %PROJECTTABLE% adatbázishoz! Figyelem, ezzel az email címmel mint felhasználó azonosítóval ezen a szerveren már egy másik adatbázisnál regisztrált. Használja az ott megadott jelszavát a bejelentkezéshez. Ha nem emlékszik a jelszavára, használja az elfelejtett jelszó opciót.
684	global	\N	ro	str_invitation_success_but_use_your_existing_account_to_login	FELICITĂRI! V-ați alăturat cu succes bazei de date %PROJECTTABLE%! Vă reamintim că deja ați folosit această adresă de e-mail ca nume de utilizator într-o altă bază de date OpenBioMaps de pe acest server. Folosiți aceeași parolă și adresă e-mail ca nume de utilizator pentru a vă conecta în această bază de date. Dacă ați uitat parola, folosiți funcția de parolă uitată pentru a crea una nouă.
685	global	\N	en	str_invites	invites
686	global	\N	hu	str_invites	meghívások
687	global	\N	ro	str_invites	invitații
688	global	\N	en	str_invtext1	invites you to use a database that was created in the OpenBioMaps Data Management System
689	global	\N	hu	str_invtext1	meghívja önt egy adatbázis használatára ami az OpenBioMaps adatkezelő rendszerben készült
690	global	\N	ro	str_invtext1	vă invită să utilizați serviciile de hărți și baze de date OpenBioMaps
691	global	\N	en	str_invtext2	In the OpenBioMaps framework, you get access to the following project
692	global	\N	hu	str_invtext2	Az OpenBioMaps keretrendszerben ön az alábbi projekthez kap hozzáférést
693	global	\N	ro	str_invtext2	Veți primi acces la următorul proiect din sistemul OpenBioMaps
694	global	\N	en	str_invtext3	To accept the invitation follow this link
695	global	\N	hu	str_invtext3	A meghívás elfogadásához kövesse az alábbi linket
696	global	\N	ro	str_invtext3	Pentru acceptarea invitației accesați linkul
697	global	\N	en	str_italian	Italian
698	global	\N	hu	str_italian	Olasz
699	global	\N	ro	str_italian	italiană
700	global	\N	en	str_itisalready_evaluated	it's already evaluated, thanks
701	global	\N	hu	str_itisalready_evaluated	ezt már értékelte az előbb, köszi
702	global	\N	ro	str_itisalready_evaluated	deja evaluat
703	global	\N	en	str_join	join
704	global	\N	hu	str_join	összekapcsol
705	global	\N	ro	str_join	combinare
706	global	\N	en	str_jump	jump
707	global	\N	hu	str_jump	ugorj
708	global	\N	ro	str_jump	săriți
709	global	\N	en	str_key	key
710	global	\N	hu	str_key	kulcs
711	global	\N	ro	str_key	cheie
712	global	\N	en	str_label	label
713	global	\N	hu	str_label	cimke
714	global	\N	ro	str_label	etichetă
715	global	\N	en	str_label_download	Download
716	global	\N	hu	str_label_download	Letöltés
717	global	\N	ro	str_label_download	descărcare
718	global	\N	en	str_lang	language
719	global	\N	hu	str_lang	nyelv
720	global	\N	ro	str_lang	limbă
721	global	\N	en	str_lang_definitions	lang definitions
722	global	\N	hu	str_lang_definitions	nyelvi definíciók
723	global	\N	ro	str_lang_definitions	definiții de limbă
724	global	\N	en	str_lang_label	Magyarul
725	global	\N	hu	str_lang_label	in English
726	global	\N	ro	str_lang_label	română
727	global	\N	en	str_last_10_rows	last 10 rows
728	global	\N	hu	str_last_10_rows	legutóbbi 10 sor
729	global	\N	ro	str_last_10_rows	ultimele 10 rânduri
730	global	\N	en	str_last_modify	last modification
731	global	\N	hu	str_last_modify	utolsó módosítás
732	global	\N	ro	str_last_modify	ultima modificare
733	global	\N	en	str_last_upload	last upload
734	global	\N	hu	str_last_upload	utolsó feltöltés
735	global	\N	ro	str_last_upload	ultimele încărcări
736	global	\N	en	str_latitude	latitude (Y)
737	global	\N	hu	str_latitude	szélesség (Y)
738	global	\N	ro	str_latitude	
739	global	\N	en	str_layer	layer
740	global	\N	hu	str_layer	réteg
741	global	\N	ro	str_layer	strat
742	global	\N	en	str_layer_def	layer definition
743	global	\N	hu	str_layer_def	réteg definíció
744	global	\N	ro	str_layer_def	definiția stratului
745	global	\N	en	str_layer_definitions	web map layers
746	global	\N	hu	str_layer_definitions	webes térképi rétegek
747	global	\N	ro	str_layer_definitions	map layers
748	global	\N	en	str_layers	layers
749	global	\N	hu	str_layers	rétegek
750	global	\N	ro	str_layers	straturi
751	global	\N	en	str_left	left
752	global	\N	hu	str_left	bal
753	global	\N	ro	str_left	stâng
754	global	\N	en	str_legend	notation
755	global	\N	hu	str_legend	jelmagyarázat
756	global	\N	ro	str_legend	legendă
757	global	\N	en	str_length	length
758	global	\N	hu	str_length	hossz
759	global	\N	ro	str_length	lungime
760	global	\N	en	str_level	level
761	global	\N	hu	str_level	szint
762	global	\N	ro	str_level	nivel
763	global	\N	en	str_lightblue	lightblue
764	global	\N	hu	str_lightblue	világoskék
765	global	\N	ro	str_lightblue	albastru deschis
766	global	\N	en	str_lightgreen	lightgreen
767	global	\N	hu	str_lightgreen	világoszöld
768	global	\N	ro	str_lightgreen	verde deschis
769	global	\N	en	str_like	I like!
770	global	\N	hu	str_like	ez jó!
771	global	\N	ro	str_like	îmi place!
772	global	\N	en	str_line	line
773	global	\N	hu	str_line	vonal
774	global	\N	ro	str_line	linie
775	global	\N	en	str_list	list
776	global	\N	hu	str_list	lista
777	global	\N	ro	str_list	listă
778	global	\N	en	str_listed	listed
779	global	\N	hu	str_listed	listázva
780	global	\N	ro	str_listed	listat
781	global	\N	en	str_load	load
782	global	\N	hu	str_load	betölt
783	global	\N	ro	str_load	încarcă
784	global	\N	en	str_load_imports	interrupted imports
785	global	\N	hu	str_load_imports	felfüggesztett adatfeltöltések
786	global	\N	ro	str_load_imports	încărcări întrerupte
787	global	\N	en	str_load_orcid_profile	Load ORCID profile data
788	global	\N	hu	str_load_orcid_profile	ORCID profil adatok betöltése
789	global	\N	ro	str_load_orcid_profile	încărcarea datelor de profil ORCID
790	global	\N	en	str_load_selections	spatial query
791	global	\N	hu	str_load_selections	térbeli lekérdezés
792	global	\N	ro	str_load_selections	interogare spațială
793	global	\N	en	str_local_language_definitions	local language defintions
794	global	\N	hu	str_local_language_definitions	helyi nyelvi definíciók
795	global	\N	ro	str_local_language_definitions	definiții de limbă locale
796	global	\N	en	str_local_translations	local translations
797	global	\N	hu	str_local_translations	helyi fordítások
798	global	\N	en	str_loggedin	logged in!
799	global	\N	hu	str_loggedin	be van jelentkezve!
800	global	\N	ro	str_loggedin	Sunteți conectat!
801	global	\N	en	str_login	Log in
802	global	\N	hu	str_login	Belépés
803	global	\N	ro	str_login	intrare
804	global	\N	en	str_logined_users	logined users
805	global	\N	hu	str_logined_users	bejelentkezett felhasználók
806	global	\N	ro	str_logined_users	utilizatori înregistrați
807	global	\N	en	str_login_error	LogIn error
808	global	\N	hu	str_login_error	Bejelentkezési hiba
809	global	\N	ro	str_login_error	Eroare login
810	global	\N	en	str_login_orcid	Sing in with ORCID iD
811	global	\N	hu	str_login_orcid	Bejelentkezés ORCID iD-val
812	global	\N	ro	str_login_orcid	intrare cu date de identificare ORCID
813	global	\N	en	str_logout	log out
814	global	\N	hu	str_logout	kilép
815	global	\N	ro	str_logout	ieșire
816	global	\N	en	str_longitude	longitude (X)
817	global	\N	hu	str_longitude	hosszúság (X)
818	global	\N	ro	str_longitude	
819	global	\N	en	str_long_text	long text
820	global	\N	hu	str_long_text	hosszú szöveg
821	global	\N	ro	str_long_text	
822	global	\N	en	str_lostpasswd	lost password
823	global	\N	hu	str_lostpasswd	elfelejtett jelszó
824	global	\N	ro	str_lostpasswd	parolă uitată
825	global	\N	en	str_mainteners	mainteners
826	global	\N	hu	str_mainteners	üzemeltetők
827	global	\N	ro	str_mainteners	administratori
828	global	\N	en	str_map	map
829	global	\N	hu	str_map	térkép
830	global	\N	ro	str_map	hartă
831	global	\N	en	str_map_access	map access
832	global	\N	hu	str_map_access	térkép elérés
833	global	\N	ro	str_map_access	acces hartă
834	global	\N	en	str_map_definitions	mapserver settings
835	global	\N	hu	str_map_definitions	mapszerver beállítások
836	global	\N	ro	str_map_definitions	definiții mapserver
837	global	\N	en	str_map_inf	map information
838	global	\N	hu	str_map_inf	térképi információk
839	global	\N	ro	str_map_inf	informații hartă
840	global	\N	en	str_map_usage	The sample points (green) are displayed on the map. More observations can belong to the same sample point. Clicking on the points will produce an information box below with sample details included.
841	global	\N	hu	str_map_usage	A térképen láthatóak a mintavételi pontok (zöld pöttyök). Egy mintavételi ponthoz több faj adat tartozik. Egy adott pontra kattintva a térkép alatt megjelenő információ sávban olvashatók az adott a mintavételi pont adatai.
842	global	\N	ro	str_map_usage	Pe hartă sunt vizibile punctele de observație colorate în verde. Unor puncte pot fi atribuite mai multe specii observate. Accesând un punct, în subsolul hărții se pot viziona toate observațiile de pe punctul respectiv.
843	global	\N	en	str_max	maximum
844	global	\N	hu	str_max	maximum
845	global	\N	ro	str_max	maxim
846	global	\N	en	str_mean	mean
847	global	\N	hu	str_mean	átlag
848	global	\N	ro	str_mean	medie
849	global	\N	en	str_members	members
850	global	\N	hu	str_members	tagok
851	global	\N	ro	str_members	membrii
852	global	\N	en	str_message	message
853	global	\N	hu	str_message	üzenet
855	global	\N	en	str_messages	messages
856	global	\N	hu	str_messages	üzenetek
857	global	\N	ro	str_messages	mesaje
858	global	\N	en	str_message_sent	Message sent
859	global	\N	hu	str_message_sent	Üzenet elküldve
860	global	\N	ro	str_message_sent	Mesaj trimis
861	global	\N	en	str_metal	metal
862	global	\N	hu	str_metal	fém
863	global	\N	ro	str_metal	metal
864	global	\N	en	str_meter	meter
865	global	\N	hu	str_meter	méter
866	global	\N	ro	str_meter	metru
867	global	\N	en	str_metrics	metrics
868	global	\N	hu	str_metrics	metrika
869	global	\N	ro	str_metrics	statistici
870	global	\N	en	str_min	minimum
871	global	\N	hu	str_min	minimum
872	global	\N	ro	str_min	minim
873	global	\N	en	str_mire	to
874	global	\N	hu	str_mire	mire
875	global	\N	ro	str_mire	la
876	global	\N	en	str_mispelled_name	mispellation
877	global	\N	hu	str_mispelled_name	elírás
878	global	\N	ro	str_mispelled_name	denumire scrisă eronat
879	global	\N	en	str_missing_mandatory_field	missing mandatory field
880	global	\N	hu	str_missing_mandatory_field	hiányzó kötelező mező
881	global	\N	ro	str_missing_mandatory_field	lipsă câmp obligatoriu
882	global	\N	en	str_missing_shpfile	incomplete shape (shp) file
883	global	\N	hu	str_missing_shpfile	hiányos shp fájl
884	global	\N	ro	str_missing_shpfile	fișier shape (.shp) incomplet
885	global	\N	en	str_mit	what
886	global	\N	hu	str_mit	mit
887	global	\N	ro	str_mit	ce
888	global	\N	en	str_modcount	modification count
889	global	\N	hu	str_modcount	módosítások száma
890	global	\N	ro	str_modcount	număr modificări
891	global	\N	en	str_modify	modify
892	global	\N	hu	str_modify	módosítás
893	global	\N	ro	str_modify	modificare
894	global	\N	en	str_modifyit	modify it
895	global	\N	hu	str_modifyit	módosít
896	global	\N	ro	str_modifyit	modifică
897	global	\N	en	str_module_include_error	module including error
898	global	\N	hu	str_module_include_error	modul beolvasás hiba
899	global	\N	ro	str_module_include_error	module care includ erori
900	global	\N	en	str_modules	modules
901	global	\N	hu	str_modules	modulok
902	global	\N	ro	str_modules	module
903	global	\N	en	str_moreinfo_wms	For more information about the wms/wfs protocols follow these links
904	global	\N	hu	str_moreinfo_wms	További információkért a WMS/WFS protokollokról látogassa ezeket az oldalakat
905	global	\N	ro	str_moreinfo_wms	pentru informații suplimentare referitoare la protocoalele WMS/WFS, vă rugăm accesați linkurile următoare
906	global	\N	en	str_morenews	minden hír
907	global	\N	hu	str_morenews	minden hír
908	global	\N	ro	str_morenews	toate știrile
909	global	\N	en	str_more_options	more options
910	global	\N	hu	str_more_options	további opciók
911	global	\N	ro	str_more_options	mai multe opțiuni
912	global	\N	en	str_more_uploadform_options	Choose from the uploading facilities
913	global	\N	hu	str_more_uploadform_options	Válasszon a feltöltési lehetőségek közül
914	global	\N	ro	str_more_uploadform_options	Alege dintre opțiunile de încărcare date
915	global	\N	en	str_most_data	most data
916	global	\N	hu	str_most_data	legtöbb adat
917	global	\N	ro	str_most_data	cele mai multe observații
918	global	\N	en	str_most_frequent_species	most frequent specieses
919	global	\N	hu	str_most_frequent_species	leggyakoribb fajok
920	global	\N	ro	str_most_frequent_species	cele mai frecvente specii
921	global	\N	en	str_most_uploads	most uploads
922	global	\N	hu	str_most_uploads	legtöbb feltöltés
923	global	\N	ro	str_most_uploads	cele mai multe încărcări
924	global	\N	en	str_mouse_zoom	mouse wheel zoom
925	global	\N	hu	str_mouse_zoom	egér görgő nagyítás
926	global	\N	ro	str_mouse_zoom	zoom cu roata mouseului
927	global	\N	en	str_my_last_upload	my last upload
928	global	\N	hu	str_my_last_upload	utolsó saját feltöltésem
929	global	\N	ro	str_my_last_upload	my last upload
930	global	\N	en	str_name	name
931	global	\N	hu	str_name	név
932	global	\N	ro	str_name	nume
933	global	\N	en	str_name_dist	distance
934	global	\N	hu	str_name_dist	távolság
935	global	\N	ro	str_name_dist	distanță
936	global	\N	en	str_nameofproject	name of the database
937	global	\N	hu	str_nameofproject	adatbázis neve
938	global	\N	ro	str_nameofproject	nume proiect
939	global	\N	en	str_name_status	name status
940	global	\N	hu	str_name_status	név státusz
941	global	\N	ro	str_name_status	statut denumire
942	global	\N	en	str_navigation	Navigation
943	global	\N	hu	str_navigation	Navigáció
944	global	\N	ro	str_navigation	navigare
945	global	\N	en	str_new	new
946	global	\N	hu	str_new	új
947	global	\N	ro	str_new	nou
948	global	\N	en	str_newactivation	Enter here the email address you registered with and the system sends a link that can be followed to set a new password.
949	global	\N	hu	str_newactivation	Adja meg itt azt az email címét amivel regisztrált és a rendszer küld egy linket amit követve új jelszót tud megadni.
950	global	\N	ro	str_newactivation	Introduceți adresa de email cu care v-ați înregistrat și vom trimite un nou mesaj de activare.
951	global	\N	en	str_new_column	new column
952	global	\N	hu	str_new_column	új oszlop
953	global	\N	en	str_new_filter	New filter
954	global	\N	hu	str_new_filter	Új szűrés
955	global	\N	ro	str_new_filter	filtru nou
956	global	\N	en	str_new_project	Founding new project
957	global	\N	hu	str_new_project	Új adatbázis alapítása
958	global	\N	ro	str_new_project	proiect nou
959	global	\N	en	str_news_stream	news stream
960	global	\N	hu	str_news_stream	hír folyam
961	global	\N	ro	str_news_stream	știri
962	global	\N	en	str_nickname	nickname
963	global	\N	hu	str_nickname	becenév
964	global	\N	ro	str_nickname	nickname
965	global	\N	en	str_no	no
966	global	\N	hu	str_no	nem
967	global	\N	ro	str_no	nu
968	global	\N	en	str_no_forms_available	There are no upload methods at the current access level.
969	global	\N	hu	str_no_forms_available	Nincs elérhető feltöltési mód a jelenlegi hozzáférési szinten
970	global	\N	ro	str_no_forms_available	Nu există formulare accesibile la nivelul actual de acces
971	global	\N	en	str_no_inds	no. of individuals
972	global	\N	hu	str_no_inds	egyedek száma
973	global	\N	ro	str_no_inds	nr. de exemplare
974	global	\N	en	str_nonexcel_days	non-excel days
975	global	\N	hu	str_nonexcel_days	nem-excel napok
976	global	\N	ro	str_nonexcel_days	zile non-excel
977	global	\N	en	str_non_sci_names	non scientific names
978	global	\N	hu	str_non_sci_names	nem tudományos nevek
979	global	\N	ro	str_non_sci_names	denumiri populare
980	global	\N	en	str_non_sci_sim_names	non scientific similar names
981	global	\N	hu	str_non_sci_sim_names	nem tudományos alternatív nevek
982	global	\N	ro	str_non_sci_sim_names	denumiri populare alternative
983	global	\N	en	str_no_records	no. of data
984	global	\N	hu	str_no_records	adatok száma
985	global	\N	ro	str_no_records	nr. de observații
986	global	\N	en	str_no_results_for_the_query	No results for the query
987	global	\N	hu	str_no_results_for_the_query	Nincs eredménye a lekérdezésnek
988	global	\N	ro	str_no_results_for_the_query	
989	global	\N	en	str_no_such_mail_address	No such mail address
990	global	\N	hu	str_no_such_mail_address	Nincs ilyen email cím
991	global	\N	ro	str_no_such_mail_address	Adresa de email nu există
992	global	\N	en	str_no_such_user	No such user or mistyped password!
993	global	\N	hu	str_no_such_user	nem jó felhasználónév vagy jelszó
994	global	\N	ro	str_no_such_user	Nu există utilizatorul sau parola a fost scrisă greșit!
995	global	\N	en	str_no_thanks	No, thanks
996	global	\N	hu	str_no_thanks	Nem, köszönöm
997	global	\N	ro	str_no_thanks	No, thanks
998	global	\N	en	str_nothing_happend	nothing happend with
999	global	\N	hu	str_nothing_happend	semmi sem változott
1000	global	\N	ro	str_nothing_happend	nu s-a întâmplat nimic
1001	global	\N	en	str_not_valid	not validated yet
1002	global	\N	hu	str_not_valid	nincs validálva
1003	global	\N	ro	str_not_valid	nevalidat
1004	global	\N	en	str_np	National Park
1005	global	\N	hu	str_np	Nemzeti park
1006	global	\N	ro	str_np	parc național
1007	global	\N	en	str_numeric	numeric
1008	global	\N	hu	str_numeric	szám
1009	global	\N	ro	str_numeric	număr
1010	global	\N	en	str_num_of_rows	num. of row
1011	global	\N	hu	str_num_of_rows	sorok száma
1012	global	\N	ro	str_num_of_rows	nr. rânduri de date
1013	global	\N	en	str_obligatory	obligatory
1014	global	\N	hu	str_obligatory	kötelező
1015	global	\N	ro	str_obligatory	obligatoriu
1016	global	\N	en	str_observation_upload	upload observation
1017	global	\N	hu	str_observation_upload	megfigyelés feltöltése
1018	global	\N	ro	str_observation_upload	încărcare observație
1019	global	\N	en	str_off	off
1020	global	\N	hu	str_off	ki
1021	global	\N	ro	str_off	oprit
1022	global	\N	en	str_on	on
1023	global	\N	hu	str_on	be
1024	global	\N	ro	str_on	pornit
1025	global	\N	en	str_one_match	only for the selected name
1026	global	\N	hu	str_one_match	csak a kiválasztott névre
1027	global	\N	ro	str_one_match	doar pentru numele selectat
1028	global	\N	en	str_onthemap	on the map
1029	global	\N	hu	str_onthemap	a térképen
1030	global	\N	ro	str_onthemap	pe hartă
1031	global	\N	en	str_openbiomaps_consortium	OpenBioMaps Consortium
1032	global	\N	hu	str_openbiomaps_consortium	OpenBioMaps Konzorcium
1033	global	\N	ro	str_openbiomaps_consortium	Consorțiul OpenBioMaps
1034	global	\N	en	str_operations	operations
1035	global	\N	hu	str_operations	műveletek
1036	global	\N	ro	str_operations	operații
1037	global	\N	en	str_opinion_post_1	- You can post your opinion with positive (+) or negatvie (-) voting.<br>- You can post your opinion without voting (ok).
1038	global	\N	hu	str_opinion_post_1	- Elküldheted a véleményed pozitív (+) vagy negatív (-) jelzéssel.<br>- Elküldheted a véleményed semleges jelzéssel (ok).
1039	global	\N	ro	str_opinion_post_1	 - Poți exprima opinia votând în mod pozitiv (+) sau negativ (-).<br>- Poți exprima o opinie neutră (ok).
1040	global	\N	en	str_opinion_post_2	- The negative voting without explanation.<br>- The inconsistency between your comment and the voting.
1041	global	\N	hu	str_opinion_post_2	- A negatív véleménytő magyarázat nélkül.<br>- Az inkonzisztenciától a véleményed és a szavazatod között.
1042	global	\N	ro	str_opinion_post_2	 - opinia negativă fără explicații. <br> - inconsecvența dintre votul exprimat și comentariul.
1043	global	\N	en	str_opinions	opinions
1044	global	\N	hu	str_opinions	vélemények
1045	global	\N	ro	str_opinions	opinii
1046	global	\N	en	str_options	search options
1047	global	\N	hu	str_options	keresési opciók
1048	global	\N	ro	str_options	opțiuni
1049	global	\N	en	str_orange	orange
1050	global	\N	hu	str_orange	narancs
1051	global	\N	ro	str_orange	portocaliu
1052	global	\N	en	str_orcid_profile	ORCID profile
1053	global	\N	hu	str_orcid_profile	ORCID profil
1054	global	\N	ro	str_orcid_profile	profil ORCID
1055	global	\N	en	str_other_databases	other databases
1056	global	\N	hu	str_other_databases	más adatbázisok
1057	global	\N	ro	str_other_databases	alte baze de date
1058	global	\N	en	str_other_filters	other filters
1059	global	\N	hu	str_other_filters	egyéb szűrők
1060	global	\N	ro	str_other_filters	alte filtre
1061	global	\N	en	str_other_langs	this page is available in the following languages
1062	global	\N	hu	str_other_langs	az oldal az alábbi nyelveken érhető el
1063	global	\N	ro	str_other_langs	pagina este accesibilă în următoarele limbi
1064	global	\N	en	str_other_pubs	Other publications
1065	global	\N	hu	str_other_pubs	Egyéb publikációk
1066	global	\N	ro	str_other_pubs	alte publicații
1067	global	\N	en	str_own	own
1068	global	\N	hu	str_own	saját
1069	global	\N	ro	str_own	propriu
1070	global	\N	en	str_own_polygons	own geometries
1071	global	\N	hu	str_own_polygons	saját geometriák
1072	global	\N	ro	str_own_polygons	geometrii proprii
1073	global	\N	en	str_paddress	address
1074	global	\N	hu	str_paddress	cím
1075	global	\N	ro	str_paddress	adresă
1076	global	\N	en	str_page	page
1077	global	\N	hu	str_page	oldal
1078	global	\N	ro	str_page	pagină
1079	global	\N	en	str_password	password
1080	global	\N	hu	str_password	Jelszó
1081	global	\N	ro	str_password	parolă
1082	global	\N	en	str_password_expired	Password expired, please use the 'Lost password' function to recreate it.
1083	global	\N	hu	str_password_expired	A jelszó lejárt, kérjük használja az 'elfelejtett jelszó' funkciót a megújításhoz.
1084	global	\N	ro	str_password_expired	Parolă expirată, vă rugăm folosiți funcția de 'Parolă uitată' pentru resetare.
1085	global	\N	en	str_password_reset_request	[%PROJECTTABLE%] Password reset request
1086	global	\N	hu	str_password_reset_request	[%PROJECTTABLE%] jelszó csere kérés
1087	global	\N	ro	str_password_reset_request	[%PROJECTTABLE%] solicitare de resetare parolă
1088	global	\N	en	str_pending	pending
1089	global	\N	hu	str_pending	függő
1090	global	\N	ro	str_pending	în așteptare
1091	global	\N	en	str_photo_id	photo id
1092	global	\N	hu	str_photo_id	fotó azonosító
1093	global	\N	ro	str_photo_id	identificator fotografie
1094	global	\N	en	str_photos	photos
1095	global	\N	hu	str_photos	fotók
1096	global	\N	ro	str_photos	fotografii
1097	global	\N	en	str_pink	pink
1098	global	\N	hu	str_pink	rózsaszín
1099	global	\N	ro	str_pink	roz
1100	global	\N	en	str_plogin	Please Log in first!
1101	global	\N	hu	str_plogin	Előbb be kell jelentkezni!
1102	global	\N	ro	str_plogin	Vă rugăm să vă conectați prima dată!
1103	global	\N	en	str_point	point
1104	global	\N	hu	str_point	pont
1105	global	\N	ro	str_point	punct
1106	global	\N	en	str_policy_agree_info	
1107	global	\N	hu	str_policy_agree_info	
1108	global	\N	ro	str_policy_agree_info	 În vederea îmbunătățirii transparenței și operarea mai corectă a datelor cu caracter personal, OpenBirdMaps a schimbat <a class="termsText" href="" data-target="privacy">Politica de confidență</a> , <a class="termsText" href="" data-target="terms">Termenii și condițiile de utilizare</a> și<a class="termsText" href="" data-target="cookies">Politica de cookie-uri</a>.
1109	global	\N	en	str_polygon	polygon
1110	global	\N	hu	str_polygon	poligon
1111	global	\N	ro	str_polygon	poligon
1112	global	\N	en	str_polygon_settings	manage custom geometries
1113	global	\N	hu	str_polygon_settings	egyéni geometriák kezelése
1114	global	\N	ro	str_polygon_settings	manipulare geometrii preîncărcate
1115	global	\N	en	str_position	position
1116	global	\N	hu	str_position	pozició
1117	global	\N	ro	str_position	poziție
1118	global	\N	en	str_preparation_of_results	Preparing results
1119	global	\N	hu	str_preparation_of_results	Az eredmények előkészítése
1120	global	\N	ro	str_preparation_of_results	
1121	global	\N	en	str_privacy_policy	privacy
1122	global	\N	hu	str_privacy_policy	adatkezelés
1123	global	\N	ro	str_privacy_policy	politica de confidență
1124	global	\N	en	str_private	private
1125	global	\N	hu	str_private	privát
1126	global	\N	ro	str_private	privat
1127	global	\N	en	str_private_policy_info	Privacy policy and protecting
1128	global	\N	hu	str_private_policy_info	A személyes adatok kezelése és védelme
1129	global	\N	ro	str_private_policy_info	Politica de confidență, Termeni și condiții de utilizare, Politica de cookie-uri
1130	global	\N	en	str_processing_query	Processing the query
1131	global	\N	hu	str_processing_query	A lekérdezés feldolgozása
1132	global	\N	ro	str_processing_query	
1133	global	\N	en	str_profile	profile
1134	global	\N	hu	str_profile	profil
1135	global	\N	ro	str_profile	profil
1136	global	\N	en	str_proj_comment	comments
1137	global	\N	hu	str_proj_comment	kommentek
1138	global	\N	ro	str_proj_comment	comentarii proiect
1139	global	\N	en	str_proj_desc	description
1140	global	\N	hu	str_proj_desc	projekt leírása
1141	global	\N	ro	str_proj_desc	descrierea proiectului
1142	global	\N	en	str_project	database
1143	global	\N	hu	str_project	adatbázis
1144	global	\N	ro	str_project	proiect
1145	global	\N	en	str_project_administration	project administration
1146	global	\N	hu	str_project_administration	projekt adminisztráció
1147	global	\N	ro	str_project_administration	administrare proiect
1148	global	\N	en	str_project_aims	aims
1149	global	\N	hu	str_project_aims	célok
1150	global	\N	ro	str_project_aims	scopuri
1151	global	\N	en	str_projectdata	database informations
1152	global	\N	hu	str_projectdata	adatbázis információk
1153	global	\N	ro	str_projectdata	informații proiect
1154	global	\N	en	str_projection	projection
1155	global	\N	hu	str_projection	projekció
1156	global	\N	ro	str_projection	proiecție
1157	global	\N	en	str_project_statistics	project statistics
1158	global	\N	hu	str_project_statistics	projekt statisztika
1159	global	\N	ro	str_project_statistics	statistici proiect
1160	global	\N	en	str_project_team	The [%PROJECTTABLE%] team
1161	global	\N	hu	str_project_team	Az [%PROJECTTABLE%] csapat
1162	global	\N	ro	str_project_team	Echipa [%PROJECTTABLE%]
1163	global	\N	en	str_project_users	project users
1164	global	\N	hu	str_project_users	projekt felhasználók
1165	global	\N	ro	str_project_users	
1166	global	\N	en	str_proj_geom	project geometry
1167	global	\N	hu	str_proj_geom	projekt geometria
1168	global	\N	ro	str_proj_geom	geometrie proiect
1169	global	\N	en	str_proj_sdesc	short description
1170	global	\N	hu	str_proj_sdesc	projekt rövid leírás
1171	global	\N	ro	str_proj_sdesc	descrierea sumară a proiectului
1172	global	\N	en	str_pseudo_column	pseudo columns
1173	global	\N	hu	str_pseudo_column	pszeudo oszlopok
1174	global	\N	ro	str_pseudo_column	pseudo columns
1175	global	\N	en	str_public	public
1176	global	\N	hu	str_public	publikus
1177	global	\N	ro	str_public	public
1178	global	\N	en	str_public_accessible_records	publicly available data
1179	global	\N	hu	str_public_accessible_records	publikus adatok
1180	global	\N	ro	str_public_accessible_records	număr de rezultate publice găsite
1181	global	\N	en	str_publisher	publisher
1182	global	\N	hu	str_publisher	kiadó
1183	global	\N	ro	str_publisher	editura
1184	global	\N	en	str_purple	purple
1185	global	\N	hu	str_purple	lila
1186	global	\N	ro	str_purple	mov
1187	global	\N	en	str_pworkpl	workplace
1188	global	\N	hu	str_pworkpl	munkahely
1189	global	\N	ro	str_pworkpl	organizație/instituție
1190	global	\N	en	str_quantity	quantity
1191	global	\N	hu	str_quantity	mennyiség
1192	global	\N	ro	str_quantity	cantitate
1193	global	\N	en	str_query	Query
1194	global	\N	hu	str_query	Kérdés
1195	global	\N	ro	str_query	interogare
1196	global	\N	en	str_query_by_id	query by id
1197	global	\N	hu	str_query_by_id	Lekérdezés azonosító alapján
1198	global	\N	ro	str_query_by_id	query by id
1199	global	\N	en	str_query_definitions	SQL query settings
1200	global	\N	hu	str_query_definitions	SQL lekérdezés beállítások
1201	global	\N	ro	str_query_definitions	setări interogare sql
1202	global	\N	en	str_query_set	Database query set
1203	global	\N	hu	str_query_set	Adatbázis lekérdezés összeállítása
1204	global	\N	ro	str_query_set	creare interogare
1205	global	\N	en	str_query_setsave	Query set and save
1206	global	\N	hu	str_query_setsave	Lekérdezések összeállítása és tárolása
1207	global	\N	ro	str_query_setsave	creare și salvare interogare
1208	global	\N	en	str_query_table	QUERY TABLE
1209	global	\N	hu	str_query_table	LEKÉRDEZETT TÁBLA
1210	global	\N	ro	str_query_table	TABELUL INTEROGAT
1211	global	\N	en	str_quick_queries	quick queries
1212	global	\N	hu	str_quick_queries	gyors lekérdezések
1213	global	\N	ro	str_quick_queries	quick queries
1214	global	\N	en	str_range_period	date range (without year)
1215	global	\N	hu	str_range_period	dátum tartomány (év nélkül)
1216	global	\N	ro	str_range_period	perioadă (fără an)
1217	global	\N	en	str_rarest_species	rarest specieses
1218	global	\N	hu	str_rarest_species	legritkább fajok
1219	global	\N	ro	str_rarest_species	cele mai rare specii
1220	global	\N	en	str_read	read
1221	global	\N	hu	str_read	olvasás
1222	global	\N	ro	str_read	citire
1223	global	\N	en	str_recommended	recommended
1224	global	\N	hu	str_recommended	ajánlott
1225	global	\N	ro	str_recommended	recomandat
1226	global	\N	en	str_recordsfound	records found
1227	global	\N	hu	str_recordsfound	találatok száma
1228	global	\N	ro	str_recordsfound	număr de rezultate găsite
1229	global	\N	en	str_red	red
1230	global	\N	hu	str_red	piros
1231	global	\N	ro	str_red	roșu
1232	global	\N	en	str_references	references
1233	global	\N	hu	str_references	hivatkozások
1234	global	\N	ro	str_references	referințe
1235	global	\N	en	str_register	Register
1236	global	\N	hu	str_register	Regisztráció
1237	global	\N	ro	str_register	înregistrare
1238	global	\N	en	str_registration	Registration
1239	global	\N	hu	str_registration	Regisztráció
1240	global	\N	ro	str_registration	înregistrare
1241	global	\N	en	str_relation	relation with other column
1242	global	\N	hu	str_relation	kapcsolat más oszloppal
1243	global	\N	ro	str_relation	relație cu altă coloană
1244	global	\N	en	str_replace_all	replace all
1245	global	\N	hu	str_replace_all	összes cseréje
1246	global	\N	ro	str_replace_all	înlocuiește tot
1247	global	\N	en	str_replace_match	replace match
1248	global	\N	hu	str_replace_match	csere egyezésen
1249	global	\N	ro	str_replace_match	înlocuiește dacă se potrivește
1250	global	\N	en	str_replacvar	replace variable
1251	global	\N	hu	str_replacvar	csere változó
1252	global	\N	ro	str_replacvar	înlocuire variabilă
1253	global	\N	en	str_report	report
1254	global	\N	hu	str_report	összefoglaló
1255	global	\N	ro	str_report	raport
1256	global	\N	en	str_reset_password_link	To reset your password follow the link below. If you can't click on the link, copy and paste it into your browser.
1257	global	\N	hu	str_reset_password_link	A jelszó lecseréléshez kövesse a következő linket. Ha a link nem kattintható, másolja a böngészőbe.
1258	global	\N	ro	str_reset_password_link	Pentru resetarea parolei urmăriți linkul de mai jos. Dacă linkul nu este activ, copiați-l în browser.
1259	global	\N	en	str_restricted	restricted
1260	global	\N	hu	str_restricted	korlátozott
1261	global	\N	ro	str_restricted	restricționat
1262	global	\N	en	str_right	right
1263	global	\N	hu	str_right	jobb
1264	global	\N	ro	str_right	drept
1265	global	\N	en	str_rights	rights
1266	global	\N	hu	str_rights	jogok
1267	global	\N	ro	str_rights	drepturi
1268	global	\N	en	str_ring_label	ring label
1269	global	\N	hu	str_ring_label	gyűrű felirat
1270	global	\N	ro	str_ring_label	inscripție inel
1271	global	\N	en	str_romanian	Romanian
1272	global	\N	hu	str_romanian	Román
1273	global	\N	ro	str_romanian	română
1274	global	\N	en	str_rom_name	Romanian name
1275	global	\N	hu	str_rom_name	Román név
1276	global	\N	ro	str_rom_name	denumire românească
1277	global	\N	en	str_row	row
1278	global	\N	hu	str_row	sor
1279	global	\N	ro	str_row	rând
1280	global	\N	en	str_rowcount	data count
1281	global	\N	hu	str_rowcount	feltöltött adatok száma
1282	global	\N	ro	str_rowcount	număr de date încărcate
1283	global	\N	en	str_rowid	row id
1284	global	\N	hu	str_rowid	sor azonosító
1285	global	\N	ro	str_rowid	identificator rând
1286	global	\N	en	str_rows	rows
1287	global	\N	hu	str_rows	sorok
1288	global	\N	ro	str_rows	rânduri
1289	global	\N	en	str_rules_create	Create rules
1290	global	\N	hu	str_rules_create	Szabályok elkészítése
1291	global	\N	ro	str_rules_create	creează reguli
1292	global	\N	en	str_running_date	pubication date
1293	global	\N	hu	str_running_date	megjelenés dátuma
1294	global	\N	ro	str_running_date	data publicației
1295	global	\N	en	str_russian	Russian
1296	global	\N	hu	str_russian	Orosz
1297	global	\N	ro	str_russian	rusă
1298	global	\N	en	str_sample_match	similarity match
1299	global	\N	hu	str_sample_match	hasonlóság egyezés
1300	global	\N	ro	str_sample_match	potrivire pe bază de similaritate
1301	global	\N	en	str_save	save
1302	global	\N	hu	str_save	mentés
1303	global	\N	ro	str_save	salvare
1304	global	\N	en	str_savedqueries	stored queries
1305	global	\N	hu	str_savedqueries	eltárolt lekérdezések
1306	global	\N	ro	str_savedqueries	interogări salvate
1307	global	\N	en	str_savedresults	saved results
1308	global	\N	hu	str_savedresults	elmentett eredmények
1309	global	\N	ro	str_savedresults	rezultate salvate
1310	global	\N	en	str_saveit	save it
1311	global	\N	hu	str_saveit	mentsd el
1312	global	\N	ro	str_saveit	salvează
1313	global	\N	en	str_save_options	save options
1314	global	\N	hu	str_save_options	mentési opciók
1315	global	\N	ro	str_save_options	opțiuni de salvare
1316	global	\N	en	str_saveqlabel	label for saved query
1317	global	\N	hu	str_saveqlabel	cimke a lekérdezésnek
1318	global	\N	ro	str_saveqlabel	label for saved query
1319	global	\N	en	str_savequery	store query
1320	global	\N	hu	str_savequery	lekérdezés eltárolása
1321	global	\N	ro	str_savequery	stocare interogare
1322	global	\N	en	str_savequeryquestion	You have an unsaved query in the session. Are you saving it now?
1323	global	\N	hu	str_savequeryquestion	Mentetlen lekérdezése van a munkamenetben. Elmenti most?
1324	global	\N	ro	str_savequeryquestion	Aveți o căutare avansată nesalvată. Salvați acum?
1325	global	\N	en	str_savequery_results	save results
1326	global	\N	hu	str_savequery_results	eredmények eltárolása
1327	global	\N	ro	str_savequery_results	salvează rezultatele
1328	global	\N	en	str_saverlabel	label for saved results
1329	global	\N	hu	str_saverlabel	cimke az eredményeknek
1330	global	\N	ro	str_saverlabel	label for saved results
1331	global	\N	en	str_save_selection	save spatial selection
1332	global	\N	hu	str_save_selection	térbeli szelekció elmentése
1333	global	\N	ro	str_save_selection	salvare interogare spațială
1334	global	\N	en	str_save_seleted_items	save selected items
1335	global	\N	hu	str_save_seleted_items	kiválasztott elemek mentése
1336	global	\N	ro	str_save_seleted_items	salvează obiectele selectate
1337	global	\N	en	str_sci_name	Scientific name
1338	global	\N	hu	str_sci_name	tudományos név
1339	global	\N	ro	str_sci_name	denumire științifică
1340	global	\N	en	str_sci_names	scientific names
1341	global	\N	hu	str_sci_names	tudományos nevek
1342	global	\N	ro	str_sci_names	denumiri științifice
1343	global	\N	en	str_search	search
1344	global	\N	hu	str_search	keres
1345	global	\N	ro	str_search	caută
1346	global	\N	en	str_searchmulti	search for multiple species
1347	global	\N	hu	str_searchmulti	keresés több fajra
1348	global	\N	ro	str_searchmulti	căutare pentru mai multe specii
1349	global	\N	en	str_searchone	search for one species
1350	global	\N	hu	str_searchone	keresés egy fajra
1351	global	\N	ro	str_searchone	căutare pentru o singură specie
1352	global	\N	en	str_searchstring	search string
1353	global	\N	hu	str_searchstring	keresőszó
1354	global	\N	ro	str_searchstring	termen de căutare
1355	global	\N	en	str_select	selection
1356	global	\N	hu	str_select	szelekció
1357	global	\N	ro	str_select	selecție
1358	global	\N	en	str_select_buffer_size	select buffer size
1359	global	\N	hu	str_select_buffer_size	szelekció puffer mérete
1360	global	\N	ro	str_select_buffer_size	selectare mărime buffer
1361	global	\N	en	str_selected_pubs	Selected publications
1362	global	\N	hu	str_selected_pubs	Válogatott publikációk
1363	global	\N	ro	str_selected_pubs	publicații selectate
1364	global	\N	en	str_selection_name	selections' names
1365	global	\N	hu	str_selection_name	szelekciók neve
1366	global	\N	ro	str_selection_name	nume geometrie
1367	global	\N	en	str_send	send
1368	global	\N	hu	str_send	küld
1369	global	\N	ro	str_send	trimite
1370	global	\N	en	str_sensitive	sensitive
1371	global	\N	hu	str_sensitive	érzékeny
1372	global	\N	ro	str_sensitive	
1373	global	\N	en	str_session	session
1374	global	\N	hu	str_session	munkamenet
1375	global	\N	ro	str_session	sesiune
1376	global	\N	en	str_session_expired	session expired
1377	global	\N	hu	str_session_expired	a munkamenet lejárt
1378	global	\N	ro	str_session_expired	sesiune expirată
1379	global	\N	en	str_session_timeout	seconds until session timeout
1380	global	\N	hu	str_session_timeout	másodperc múlva a munkamenet lejár
1381	global	\N	ro	str_session_timeout	secunde până la sfârșitul sesiunii
1382	global	\N	en	str_set	Set
1383	global	\N	hu	str_set	beállít
1384	global	\N	ro	str_set	setează
1385	global	\N	en	str_set_coordinates_from	Do I set geometry based on image information
1386	global	\N	hu	str_set_coordinates_from	Beállítsam a geometriát a képi információk alapján
1387	global	\N	ro	str_set_coordinates_from	Do I set geometry based on image information
1388	global	\N	en	str_set_it	set_it
1389	global	\N	hu	str_set_it	állítsd be
1390	global	\N	ro	str_set_it	
1391	global	\N	en	str_set_max	maximum
1392	global	\N	hu	str_set_max	maximum
1393	global	\N	ro	str_set_max	
1394	global	\N	en	str_set_password	Please change your password
1395	global	\N	hu	str_set_password	kérjük állítsa be a jelszavát
1396	global	\N	ro	str_set_password	vă rugăm să vă setați parola!
1397	global	\N	hu	str_set_save_query	Elmenthető lekérdezés összeállítása
1398	global	\N	en	str_set_save_query	Assambling savable queries
1399	global	\N	ro	str_set_save_query	creare interogare salvabilă
1400	global	\N	en	str_settings	settings
1401	global	\N	hu	str_settings	beállítások
1402	global	\N	ro	str_settings	setări
1403	global	\N	en	str_set_your_password	After you signed in, please set your new password.
1404	global	\N	hu	str_set_your_password	Miután bejelentkezett kérem adja meg a jelszavát.
1405	global	\N	ro	str_set_your_password	După ce v-ați logat, vă rugăm setați parola nouă.
1406	global	\N	en	str_shape_error	shape (.shp) file error
1407	global	\N	hu	str_shape_error	shape fájl hiba
1408	global	\N	ro	str_shape_error	eroare fișier shape (.shp)
1409	global	\N	en	str_share	share
1410	global	\N	hu	str_share	megosztás
1411	global	\N	ro	str_share	share
1412	global	\N	en	str_shared_polygons	shared geometries
1413	global	\N	hu	str_shared_polygons	megosztott geometriák
1414	global	\N	ro	str_shared_polygons	geometrii împărtășite
1415	global	\N	en	str_sheets	sheets
1416	global	\N	hu	str_sheets	lapok
1417	global	\N	ro	str_sheets	pagini
1418	global	\N	en	str_show_uploads	uploads
1419	global	\N	hu	str_show_uploads	feltöltések
1420	global	\N	ro	str_show_uploads	încărcări
1421	global	\N	en	str_shp_upload	geometry upload from SHP file
1422	global	\N	hu	str_shp_upload	geometria feltöltése SHP fájlból
1423	global	\N	ro	str_shp_upload	încărcare geometrie din fișier SHP
1424	global	\N	en	str_sigcont_explain	those users whose uploading contribution exceeds 5% of the whole database
1425	global	\N	hu	str_sigcont_explain	azon felhasználók akiknek a feltöltési hozzájárulása meghaladja a teljes adatbázis több mint 5%-át
1426	global	\N	ro	str_sigcont_explain	acei utilizatori a căror contribuție la totalul datelor încărcate depășește 5%
1427	global	\N	en	str_significant_contributors	significant contributors
1428	global	\N	hu	str_significant_contributors	jelentős hozzájárulók
1429	global	\N	ro	str_significant_contributors	contributori principali
1430	global	\N	en	str_signin	Sign in
1431	global	\N	hu	str_signin	Bejelentkezés
1432	global	\N	ro	str_signin	autentificare
1433	global	\N	en	str_silver	silver
1434	global	\N	hu	str_silver	ezüst
1435	global	\N	ro	str_silver	argintiu
1436	global	\N	en	str_sim_names	similar scientific names
1437	global	\N	hu	str_sim_names	hasonló tudományos nevek
1438	global	\N	ro	str_sim_names	denumiri științifice similare
1439	global	\N	en	str_skip_marked_rows	There are skip marked lines in the table!
1440	global	\N	hu	str_skip_marked_rows	Kihagyásra jelölt sorok vannak a táblázatban!
1441	global	\N	ro	str_skip_marked_rows	Există rânduri marcate pentru a fi omise din încărcare!
1442	global	\N	en	str_smpl_p	Sample points
1443	global	\N	hu	str_smpl_p	Mintavételi hely
1444	global	\N	ro	str_smpl_p	puncte eșantion
1445	global	\N	en	str_soft_error	soft error
1446	global	\N	hu	str_soft_error	puha hiba
1447	global	\N	ro	str_soft_error	soft error
1448	global	\N	en	str_some_errors	Some errors occured! Ommit the affected lines or repair the errors.
1449	global	\N	hu	str_some_errors	Néhány hiba van! Hagyd ki az érintett sorokat, vagy javítsd a hibákat.
1654	global	\N	en	str_validation	validation
1450	global	\N	ro	str_some_errors	Eroare! Completează/corectează erorile sau omite liniile afectate.
1451	global	\N	en	str_some_warnings	Attention! Repair or click on IMPORT again to disregard the warnings!
1452	global	\N	hu	str_some_warnings	Figyelem! Javítsd, vagy klikkelj megint az IMPORT gombon a figyelmeztetések figyelmen kívül hagyásához!
1453	global	\N	ro	str_some_warnings	Atenție! Corectează sau click încă o dată pe butonul ÎNCĂRCARE DATE pentru a ignora avertismentul!
1454	global	\N	en	str_spatial_geometry	spatial geometry
1455	global	\N	hu	str_spatial_geometry	térbeli geomatria
1456	global	\N	en	str_spatial_relationship	spatial realtionship
1457	global	\N	hu	str_spatial_relationship	térbeli kapcsolat
1458	global	\N	ro	str_spatial_relationship	relație spațială
1459	global	\N	en	str_species	species
1460	global	\N	hu	str_species	faj
1461	global	\N	ro	str_species	specie
1462	global	\N	en	str_speciescount	species
1463	global	\N	hu	str_speciescount	faj
1464	global	\N	ro	str_speciescount	specii
1465	global	\N	en	str_specieses	species name database
1466	global	\N	hu	str_specieses	fajnév adatbázis
1467	global	\N	ro	str_specieses	baza de date cu numele speciilor
1468	global	\N	en	str_specieslist	the list of species and summary of the query
1469	global	\N	hu	str_specieslist	a lekérdezésben előforduló fajok listája és adatok összesítése
1470	global	\N	ro	str_specieslist	lista speciilor și sumarul interogării
1471	global	\N	en	str_speciesplus	add more species to the filter
1472	global	\N	hu	str_speciesplus	további fajok hozzáadása a szűréshez
1473	global	\N	ro	str_speciesplus	adăugare specii noi la filtrare
1474	global	\N	en	str_species_stat	species statistics
1475	global	\N	hu	str_species_stat	faj statisztika
1476	global	\N	ro	str_species_stat	statistici specii
1477	global	\N	en	str_specname	species name
1478	global	\N	hu	str_specname	fajnév
1479	global	\N	ro	str_specname	nume specie
1480	global	\N	en	str_sqdesc	By clicking the links below, saved queries can be loaded. You can save the links from the right click menu to save them to the clipboard.
1481	global	\N	hu	str_sqdesc	Az alábbi hivatkozásokra kattintva az elmentett lekérdezések visszatölthetőek. A hivatkozásokat a jobb egérgombos menüből kiválasztva a vágólapra mentheti.
1482	global	\N	ro	str_sqdesc	Accesând acest link puteți continua o interogare salvată. Puteți salva linkul alegând opțiunea salvează din meniul click dreapta.
1483	global	\N	en	str_srdesc	
1484	global	\N	hu	str_srdesc	
1485	global	\N	ro	str_srdesc	
1486	global	\N	en	str_state	state
1487	global	\N	hu	str_state	állapot
1488	global	\N	ro	str_state	stare
1489	global	\N	en	str_status	status
1490	global	\N	hu	str_status	státusz
1491	global	\N	ro	str_status	statut
1492	global	\N	en	str_steps	steps
1493	global	\N	hu	str_steps	lépés
1494	global	\N	ro	str_steps	pași
1495	global	\N	en	str_success	success
1496	global	\N	hu	str_success	sikeres
1497	global	\N	ro	str_success	reușit
1498	global	\N	en	str_sum	sum
1499	global	\N	hu	str_sum	összeg
1500	global	\N	ro	str_sum	sumă
1501	global	\N	en	str_sumall	in all
1502	global	\N	hu	str_sumall	összesen
1503	global	\N	ro	str_sumall	din totalul de
1504	global	\N	en	str_summary	summary
1505	global	\N	hu	str_summary	összefoglaló
1506	global	\N	ro	str_summary	sumar
1507	global	\N	en	str_synonym_name	synonym name
1508	global	\N	hu	str_synonym_name	szinoním név
1509	global	\N	ro	str_synonym_name	sinonimă
1510	global	\N	en	str_table	table
1511	global	\N	hu	str_table	tábla
1512	global	\N	ro	str_table	tabel
1513	global	\N	en	str_table_comment	table comment
1514	global	\N	hu	str_table_comment	tábla megjegyzés
1515	global	\N	ro	str_table_comment	comentariu tabel
1516	global	\N	en	str_table_name	table name
1517	global	\N	hu	str_table_name	tábla neve
1518	global	\N	ro	str_table_name	nume tabel
1519	global	\N	en	str_taxon_filter	taxon filters
1520	global	\N	hu	str_taxon_filter	taxon név szűrés
1521	global	\N	ro	str_taxon_filter	filtru nume specie
1522	global	\N	en	str_taxon_name	species name
1523	global	\N	hu	str_taxon_name	faj név
1524	global	\N	ro	str_taxon_name	nume specie
1525	global	\N	en	str_taxon_names	species names
1526	global	\N	hu	str_taxon_names	faj nevek
1527	global	\N	ro	str_taxon_names	denumire specii
1528	global	\N	en	str_tc	Terms & Conditions
1529	global	\N	hu	str_tc	Szabályok és feltételek
1530	global	\N	ro	str_tc	reguli și condiții
1531	global	\N	en	str_technical_info	technical information
1532	global	\N	hu	str_technical_info	technical information
1533	global	\N	ro	str_technical_info	informație tehnică
1534	global	\N	en	str_template	template
1535	global	\N	hu	str_template	sablon
1536	global	\N	ro	str_template	template
1537	global	\N	en	str_text	text
1538	global	\N	hu	str_text	szöveg
1539	global	\N	ro	str_text	text
1540	global	\N	en	str_text_filters	text filters
1541	global	\N	hu	str_text_filters	szöveges szűrők
1542	global	\N	ro	str_text_filters	filtre de text
1543	global	\N	en	str_thanks_for	Thank you for your contribution!
1544	global	\N	hu	str_thanks_for	Köszönet a hozzájárulásodért!
1545	global	\N	ro	str_thanks_for	mulțumim că ați contribuit cu opinia
1546	global	\N	en	str_the	the
1547	global	\N	hu	str_the	a
1548	global	\N	ro	str_the	
1549	global	\N	en	str_thx_for_opinion	thanks for your contribution to the evaluation
1550	global	\N	hu	str_thx_for_opinion	köszönjük, hogy véleményével hozzájárult az értékeléshez
1551	global	\N	ro	str_thx_for_opinion	mulțumim că ați contribuit cu opinia dumneavoastră la evaluare
1552	global	\N	en	str_time	time
1553	global	\N	hu	str_time	idő
1554	global	\N	ro	str_time	timp
1555	global	\N	en	str_time_interval	time interval
1556	global	\N	hu	str_time_interval	idő intervallum
1557	global	\N	ro	str_time_interval	interval de timp
1558	global	\N	en	str_time_to_minutes	time to minutes
1559	global	\N	hu	str_time_to_minutes	idő percekben
1560	global	\N	ro	str_time_to_minutes	timp până la minute
1561	global	\N	en	str_to	to
1562	global	\N	hu	str_to	-ig
1563	global	\N	ro	str_to	până la
1564	global	\N	en	str_top_ten	Top 10
1565	global	\N	hu	str_top_ten	10 leggyakoribb
1566	global	\N	ro	str_top_ten	top 10
1567	global	\N	en	str_translation	translation
1568	global	\N	hu	str_translation	fordítás
1569	global	\N	ro	str_translation	
1570	global	\N	en	str_true	true
1571	global	\N	hu	str_true	igen
1572	global	\N	ro	str_true	da
1573	global	\N	en	str_turn_off	turn off
1574	global	\N	hu	str_turn_off	kikapcsol
1575	global	\N	ro	str_turn_off	oprește
1576	global	\N	en	str_turn_on	turn on
1577	global	\N	hu	str_turn_on	bekapcsol
1578	global	\N	ro	str_turn_on	pornește
1579	global	\N	en	str_type	type
1580	global	\N	hu	str_type	típus
1581	global	\N	ro	str_type	tip
1582	global	\N	en	str_type_to_listing_values	type to listing values
1583	global	\N	hu	str_type_to_listing_values	kezdjen el gépelni a lista megjelenítéséhez
1584	global	\N	ro	str_type_to_listing_values	
1585	global	\N	en	str_undefined	undefined
1586	global	\N	hu	str_undefined	definiálatlan
1587	global	\N	ro	str_undefined	nedefinit
1588	global	\N	en	str_update	update
1589	global	\N	hu	str_update	frissít
1590	global	\N	ro	str_update	actualizează
1591	global	\N	en	str_uphist	Uploading history
1592	global	\N	hu	str_uphist	Adatfeltöltés története
1593	global	\N	ro	str_uphist	istoric încărcare de date
1594	global	\N	en	str_upldesc	uploading list
1595	global	\N	hu	str_upldesc	feltöltés listája
1596	global	\N	ro	str_upldesc	acesta este lista încărcărilor utilizatorului
1597	global	\N	en	str_upload	upload
1598	global	\N	hu	str_upload	feltöltés
1599	global	\N	ro	str_upload	încărcare
1600	global	\N	en	str_uploadcount	upload count
1601	global	\N	hu	str_uploadcount	feltöltések száma
1602	global	\N	ro	str_uploadcount	număr încărcări
1603	global	\N	en	str_upload_date	upload date
1604	global	\N	hu	str_upload_date	feltöltés dátuma
1605	global	\N	ro	str_upload_date	încărcat în data de
1606	global	\N	en	str_uploaded_data	uploaded data
1607	global	\N	hu	str_uploaded_data	feltöltött adat
1608	global	\N	ro	str_uploaded_data	date încărcate
1609	global	\N	en	str_uploader	uploader
1610	global	\N	hu	str_uploader	feltöltő
1611	global	\N	ro	str_uploader	încărcat de
1612	global	\N	en	str_upload_extent	Uploaded geometry extent
1613	global	\N	hu	str_upload_extent	Feltöltött adatok térbeli kiterjedése
1614	global	\N	ro	str_upload_extent	întinderea geometriilor încărcate
1615	global	\N	en	str_upload_forms	upload forms
1616	global	\N	hu	str_upload_forms	feltöltő formok
1617	global	\N	ro	str_upload_forms	creare formulare
1618	global	\N	en	str_upload_from_file	upload from file
1619	global	\N	hu	str_upload_from_file	feltöltés fájlból
1620	global	\N	ro	str_upload_from_file	încărcare din fișier
1621	global	\N	en	str_upload_from_web	upload using web form
1622	global	\N	hu	str_upload_from_web	feltöltés űrlappal
1623	global	\N	ro	str_upload_from_web	încărcare din formular web
1624	global	\N	en	str_uploading_evaluation	uploading evaluation
1625	global	\N	hu	str_uploading_evaluation	feltöltés véleményezés
1626	global	\N	ro	str_uploading_evaluation	evaluarea încărcării
1627	global	\N	en	str_upload_length	Uploaded row count
1628	global	\N	hu	str_upload_length	Feltöltött adatok száma
1629	global	\N	ro	str_upload_length	numărul de rânduri încărcate
1630	global	\N	en	str_uploads	uploads
1631	global	\N	hu	str_uploads	feltöltések
1632	global	\N	ro	str_uploads	încărcări
1633	global	\N	en	str_url	URL
1634	global	\N	hu	str_url	URL
1635	global	\N	ro	str_url	URL
1636	global	\N	en	str_user	user
1637	global	\N	hu	str_user	felhasználó
1638	global	\N	ro	str_user	utilizator
1639	global	\N	en	str_user_agreement	User Agreement
1640	global	\N	hu	str_user_agreement	User Agreement
1641	global	\N	ro	str_user_agreement	reguli și condiții de utilizare (pdf)
1642	global	\N	en	str_userdata	user data
1643	global	\N	hu	str_userdata	felhasználói adatok
1644	global	\N	ro	str_userdata	date utilizator
1645	global	\N	en	str_user_evaluation	user evaluation
1646	global	\N	hu	str_user_evaluation	felhasználó véleményezés
1647	global	\N	ro	str_user_evaluation	evaluarea utilizatorului
1648	global	\N	en	str_userinfo	user information
1649	global	\N	hu	str_userinfo	felhasználói információk
1650	global	\N	ro	str_userinfo	informații utilizator
1651	global	\N	en	str_user_statistics	user statistics
1652	global	\N	hu	str_user_statistics	felhasználó statisztika
1653	global	\N	ro	str_user_statistics	statistici utilizatori
1655	global	\N	hu	str_validation	validitás
1656	global	\N	ro	str_validation	validare
1657	global	\N	en	str_validhist	Evaluation history
1658	global	\N	hu	str_validhist	Értékelés történet
1659	global	\N	ro	str_validhist	istoric validare
1660	global	\N	en	str_valuation	evaluation
1661	global	\N	hu	str_valuation	értékelés
1662	global	\N	ro	str_valuation	evaluare
1663	global	\N	en	str_valuations	valuations
1664	global	\N	hu	str_valuations	értékelések
1665	global	\N	ro	str_valuations	evaluări
1666	global	\N	en	str_value	value
1667	global	\N	hu	str_value	érték
1668	global	\N	ro	str_value	valoare
1669	global	\N	en	str_view	view
1670	global	\N	hu	str_view	megnéz
1671	global	\N	ro	str_view	vizualizează
1672	global	\N	en	str_view_options	display options
1673	global	\N	hu	str_view_options	megjelenítési opciók
1674	global	\N	ro	str_view_options	opțiuni de vizualizare
1675	global	\N	en	str_views	views
1676	global	\N	hu	str_views	megtekintések
1677	global	\N	ro	str_views	vizualizări
1678	global	\N	en	str_violet	violet
1679	global	\N	hu	str_violet	ibolya
1680	global	\N	ro	str_violet	violet
1681	global	\N	en	str_visible	visible
1682	global	\N	hu	str_visible	látható
1683	global	\N	ro	str_visible	vizibil
1684	global	\N	en	str_web_form	web form
1685	global	\N	hu	str_web_form	webes űrlap
1686	global	\N	ro	str_web_form	formular web
1687	global	\N	en	str_welcome_to	Welcome to %PROJECTTABLE%
1688	global	\N	hu	str_welcome_to	Üdvözöljük az %PROJECTTABLE%-ben
1689	global	\N	ro	str_welcome_to	Bine ați venit în %PROJECTTABLE%
1690	global	\N	en	str_welcome_to_project_in_domain	Welcome to `%PROJECTTABLE%` project in %DOMAIN%!
1691	global	\N	hu	str_welcome_to_project_in_domain	Üdvözöljük az `%PROJECTTABLE%` projektben a %DOMAIN%!
1692	global	\N	ro	str_welcome_to_project_in_domain	Bine ați venit în proiectul `%PROJECTTABLE%` din %DOMAIN%!
1693	global	\N	en	str_what_are_apikeys	What are the „API keys”?
1694	global	\N	hu	str_what_are_apikeys	Mik azok az „API kulcsok”?
1695	global	\N	ro	str_what_are_apikeys	Ce este „API keys”?
1696	global	\N	en	str_what_are_custom_geometries	What are the custom geometries?
1697	global	\N	hu	str_what_are_custom_geometries	Mik azok az egyéni geometriák?
1698	global	\N	ro	str_what_are_custom_geometries	Ce sunt geometriile preîncărcate?
1699	global	\N	en	str_what_are_load_imports	What are the interrupted imports?
1700	global	\N	hu	str_what_are_load_imports	Mik azok a felfüggesztett importok?
1701	global	\N	ro	str_what_are_load_imports	Ce sunt încărcările întrerupte?
1702	global	\N	en	str_what_are_savedqueries	What are the stored queries?
1703	global	\N	hu	str_what_are_savedqueries	Mik azok az eltárolt lekérdezések?
1704	global	\N	ro	str_what_are_savedqueries	Ce sunt interogările salvate?
1705	global	\N	en	str_what_are_savedresults	What are the saved results?
1706	global	\N	hu	str_what_are_savedresults	Mik azok az elmentett eredmények?
1707	global	\N	ro	str_what_are_savedresults	Ce sunt rezultatele salvate?
1708	global	\N	en	str_when	when
1709	global	\N	hu	str_when	mikor
1710	global	\N	ro	str_when	când
1711	global	\N	en	str_white	white
1712	global	\N	hu	str_white	fehér
1713	global	\N	ro	str_white	alb
1714	global	\N	en	str_who	who
1715	global	\N	hu	str_who	kinek
1716	global	\N	ro	str_who	cine
1717	global	\N	en	str_wkt	WKT
1718	global	\N	hu	str_wkt	WKT
1719	global	\N	ro	str_wkt	WKT
1720	global	\N	en	str_wmswfs_access	Public Web Services
1721	global	\N	hu	str_wmswfs_access	nyílvános web szolgáltatások
1722	global	\N	ro	str_wmswfs_access	servicii publice web
1723	global	\N	en	str_write	write
1724	global	\N	hu	str_write	írás
1725	global	\N	ro	str_write	scriere
1726	global	\N	en	str_yellow	yellow
1727	global	\N	hu	str_yellow	sárga
1728	global	\N	ro	str_yellow	galben
1729	global	\N	en	str_yes	yes
1730	global	\N	hu	str_yes	igen
1731	global	\N	ro	str_yes	da
1732	global	\N	en	str_yes_i_want	Yes, I want
1733	global	\N	hu	str_yes_i_want	Igen, szeretném
1734	global	\N	ro	str_yes_i_want	Yes, I want
1735	global	\N	en	str_you_have_connected_to_a_stranger_database	you have connected to a stranger database
1736	global	\N	hu	str_you_have_connected_to_a_stranger_database	egy idegen adatbázishoz csatlakoztál
1737	global	\N	ro	str_you_have_connected_to_a_stranger_database	ai accesat o bază de date străină
1738	global	\N	en	str_your_invitations_accepted	Your invitations accepted
1739	global	\N	hu	str_your_invitations_accepted	A meghívóját elfogadták
1740	global	\N	ro	str_your_invitations_accepted	Invitația Dumneavoastră a fost acceptată
1741	global	\N	en	str_your_opinion	your opinion
1742	global	\N	hu	str_your_opinion	az ön véleménye
1743	global	\N	ro	str_your_opinion	opinia ta
1744	global	\N	en	str_new_project_welcome	It is easy to founding a new database project!<br> <br><br>
1745	global	\N	hu	str_new_project_welcome	Új adatbázis projektet bárki alapíthat egyszerűen!<br>A lépések a következők:<ol><li>Egyedi nevet kell adnia a projektnek, ami az adatbázis tábláink neve is lesz egyúttal.</li><li>Szükséges egy rövid leíró nevet is adnia a projektjének.</li><li>Szükséges egy részletes leírást is írnia arról, hogy mi ez a projekt, mi a célja.</li><li>Meg kell határoznia az adatok elérésének és módosítának a lehetőségeit.</li><li>Az űrlap kitöltése és elküldése után a projekt kísérleti stádiumba kerül.</li><li>Az ebben a projektben érvényes felhasználó nevével és jelszavával tud belépni az újonnan létrejött projektbe, ahol a további beállításokat kell majd elvégeznie.</li></ol><br><br>
1746	global	\N	ro	str_new_project_welcome	Puteți înființa o bază de date nouă simplu!<br> Înființarea unui proiect nou constă din definirea bazei de date și înființarea unui grup de lucru.<br> Pentru crearea bazei de date completați formularul de mai jos. Baza de date trebuie să primească o denumire unică, care în același timp va fi și numele proiectului. Trebuie definit gradul de accesibilitate publică a datelor etc. Îndrumarul pentru completarea câmpurilor se găsește sub formular.  Dacă aveți nevoie de ajutor adițional, contactați administratorul bazei de date, în care vă aflați.<br>  După trimiterea formularului, trebuie contactat unul dintre administratorii OpenBioMaps pentru activarea proiectului. Datele de contact al administratorilor poate fi găsit în documentație..<br> Mulțumim pentru contribuția acordată comunității Openbiomaps și pentru mărirea numărului de date neprelucrate publice disponibile!<br><br>
1747	global	\N	en	str_access_help	Project level access settings.
1748	global	\N	hu	str_access_help	Projekt szintű hozzáférési szabályozás.
1749	global	\N	ro	str_access_help	Drepturi de accesibilitate pe nivel de proiect.
1750	global	\N	en	str_apikeys	API keys
1751	global	\N	hu	str_apikeys	API kulcsok
1752	global	\N	ro	str_apikeys	
1753	global	\N	en	str_assistant	assistant
1754	global	\N	hu	str_assistant	segítő
1755	global	\N	ro	str_assistant	asistent
1756	global	\N	en	str_best_wishes	best wishes
1757	global	\N	hu	str_best_wishes	üdvözlettel
1758	global	\N	ro	str_best_wishes	toate cele bune
1759	global	\N	en	str_choose_log	choose log file
1760	global	\N	hu	str_choose_log	válasszon log fájlt
1761	global	\N	ro	str_choose_log	alege log file
1762	global	\N	en	str_custom_check	custom check
1763	global	\N	hu	str_custom_check	egyéni ellenőrzés
1764	global	\N	ro	str_custom_check	verificare particularizată
1765	global	\N	en	str_data_assign	data assign
1766	global	\N	hu	str_data_assign	adat összerendelés
1767	global	\N	ro	str_data_assign	atribuire dată
1768	global	\N	en	str_data_extent	Data Extent
1769	global	\N	hu	str_data_extent	Adatok térbeli kiterjedése
1770	global	\N	ro	str_data_extent	Data Extent
1771	global	\N	en	str_data_mod_acc	This project data modifiable by
1772	global	\N	hu	str_data_mod_acc	A projekt adatainak módosítási hozzáférése
1773	global	\N	ro	str_data_mod_acc	Datele proiectului pot fi modificate de
1774	global	\N	en	str_data_read_acc	This project data accessible for
1775	global	\N	hu	str_data_read_acc	A projekt adatainak olvasási hozzáférése
1776	global	\N	ro	str_data_read_acc	Datele proiectului sunt accesibile pentru
1777	global	\N	en	str_data_srid	Data SRID (PostGIS)
1778	global	\N	hu	str_data_srid	Adatok SRID-ja (PostGIS)
1779	global	\N	ro	str_data_srid	Data SRID (PostGIS)
1780	global	\N	en	str_db_cols_help	Az oszlop típusának megadása határozza meg, hogy hogyan kezelje az OpenBioMaps az adott oszlopot. A 'ADAT' típus bármilyen oszlop lehet amit az OpenBioMaps egyéb adatként kezel a továbbiakban. A többi típusból általában egyet kell definiálni (pl. EGYEDSZÁM, TUDOMÁNYOS NÉV), de néha lehet többet is mint pl. a HIVATKOZÁS és a DÁTUM. Az összerendelés nélküli oszlopokat egyáltalán nem kezeli az OpenBioMaps!
1781	global	\N	hu	str_db_cols_help	Az oszlop típusának megadása határozza meg, hogy hogyan kezelje az OpenBioMaps az adott oszlopot. A 'DATA' típus bármilyen oszlop lehet amit az OpenBioMaps egyéb adatként kezel a továbbiakban. A többi típusból (pl. NUMBER OF INDIVIDUALS, SCIENTIFIC SPECIES NAME) egyet kell definiálni kivéve a 'CITE' és 'DATE' amiből lehet többet is. Az összerendelés nélküli oszlopokat egyáltalán nem kezeli az OpenBioMaps!
1782	global	\N	ro	str_db_cols_help	Setarea tipului coloanei va defini modul în care OpenBioMaps va folosi coloana respectivă.  Tipul 'common data' poate fi orice coloană, care va fi tratat de OpenBioMaps ca alte tipuri de date. Din restul tipurilor (timp, loc, specie) trebuie definit câte unul, cu excepția datei și a persoanei citate, din care pot fi definite mai multe. Coloanele, la care nu este setat tipul, nu vor fi luate în considerare de OpenBioMaps!
1783	global	\N	en	str_db_error_photo_id_column	Database error: Has the photo_id column configured properly?
1784	global	\N	hu	str_db_error_photo_id_column	Adatbázis hiba: a photo_id oszlop be van állítva rendesen?
1785	global	\N	ro	str_db_error_photo_id_column	Eroare bază de date: Coloana photo_id a fost configurat corect?
1786	global	\N	en	str_def_columns_names	columns's name (one word, no accentuated letters)
1787	global	\N	hu	str_def_columns_names	oszlop neve (egy szó ékezetek nélkül)
1788	global	\N	ro	str_def_columns_names	denumire coloană (un singur cuvânt, fără diacritice)
1789	global	\N	en	str_desc_desc	long description
1790	global	\N	hu	str_desc_desc	magyarázó leírás
1791	global	\N	ro	str_desc_desc	descriere lungă
1792	global	\N	en	str_history_create	History auto create
1793	global	\N	hu	str_history_create	History auto create
1794	global	\N	ro	str_history_create	Istorie auto create
1795	global	\N	en	str_invitationsend	Send invitation
1796	global	\N	hu	str_invitationsend	meghívó küldése
1797	global	\N	ro	str_invitationsend	trimite invitație
1798	global	\N	en	str_invited_email	e-mail address of the invited person
1799	global	\N	hu	str_invited_email	meghívott e-mail címe
1800	global	\N	ro	str_invited_email	adresa de e-mail invitat
1801	global	\N	en	str_invited_name	name of invited person
1802	global	\N	hu	str_invited_name	meghívott neve
1803	global	\N	ro	str_invited_name	nume invitat
1804	global	\N	en	str_layers_help	<h3>Must be defined:</h3><ul><li><b>Data layer:</b>, called <i>layer_data_*</i> and its attributes are (e.g.) (layer definition):<br>layers:'map-file-layer-name', isBaseLayer:'false', visibility:'true', opacity:'1.0', format:'image/png', transparent:'true', numZoomLevels:'20'<br>Its type is <i>WMS</i>. The <i>URL</i> and <i>Map</i> parameters can't be empty. In the default case their value is '<i>default</i>'<br>More data layers can be defined!</li></ul>
1869	global	\N	ro	str_psql_function_help	Aici pot fi mainpulate funcțiile plpgsql din baza de date.
1870	global	\N	en	str_saved_import_session_by	Saved import session by
1871	global	\N	hu	str_saved_import_session_by	Elmentett munkamenet
1805	global	\N	hu	str_layers_help	<h3>A rendszer működéshez szükséges definiálni:</h3><ul><li><b>Adat réteg:</b>, aminek a neve <i>layer_data_*</i> és például ezek a tulajdonságai (réteg defínició):<br>layers:'map-file-réteg-név', isBaseLayer:'false', visibility:'true', opacity:'1.0', format:'image/png', transparent:'true', numZoomLevels:'20'<br>A típusa <i>WMS</i> legyen. Az <i>URL</i> és <i>Térkép</i> paraméterek ne maradjanak üresen, alapesetben legyen az értékük '<i>default</i>'<br>Több adatréteg is definiálható!</li></ul>
1806	global	\N	ro	str_layers_help	<h3>Must be defined:</h3><ul><li><b>Data layer:</b>, called <i>layer_data_*</i> and its attributes are (e.g.) (layer definition):<br>layers:'map-file-layer-name', isBaseLayer:'false', visibility:'true', opacity:'1.0', format:'image/png', transparent:'true', numZoomLevels:'20'<br>Its type is <i>WMS</i>. The <i>URL</i> and <i>Map</i> parameters can't be empty. In the default case their value is '<i>default</i>'<br>More data layers can be defined!</li></ul>
1807	global	\N	en	str_layers_searchfor_private	Search for PRIVATE mapfile layers (result depend on the mapfile's syntax)
1808	global	\N	hu	str_layers_searchfor_private	Megtalált rétegek a PRIVÁT mapfájlban (az eredmények a fájl szintaxisától függhetnek)
1809	global	\N	ro	str_layers_searchfor_private	Search for PRIVATE mapfile layers (result depend on the mapfile's syntax)
1810	global	\N	en	str_layers_searchfor_public	Search for PUBLIC mapfile layers (result depend on the mapfile's syntax)
1811	global	\N	hu	str_layers_searchfor_public	Megtalált rétegek a PUBLIKUS mapfájlban (az eredmények a fájl szintaxisától függhetnek)
1812	global	\N	ro	str_layers_searchfor_public	Search for PUBLIC mapfile layers (result depend on the mapfile's syntax)
1813	global	\N	en	str_load_import_help	List of saved data imports. Follow the links to load imports.
1814	global	\N	hu	str_load_import_help	Elmentett adat importolálások listája. A listában található adatbetöltéseket innen be lehet fejezni.
1815	global	\N	ro	str_load_import_help	Lista încărcărilor de date salvate. De aici pot fi continuate sau șterse încărcările de date salvate
1816	global	\N	en	str_maintainer_access_explanation	Administrative rights
1817	global	\N	hu	str_maintainer_access_explanation	Adminisztratív jogok
1818	global	\N	ro	str_maintainer_access_explanation	drepturi de administrare
1819	global	\N	en	str_map_check_result	Map file check results
1820	global	\N	hu	str_map_check_result	térkép fájl ellenőrzés eredménye
1821	global	\N	ro	str_map_check_result	Rezultate verificare map file
1822	global	\N	en	str_map_extent_text	Here is the data's spatial extent. This values should be in the map file.
1823	global	\N	hu	str_map_extent_text	A térképi adatok térbeli kiterjedése alább látható. Ennek a kiterjedésnek kell szerepelnie  a map fájlban.
1824	global	\N	ro	str_map_extent_text	Here is the data's spatial extent. This values should be in the map file.
1825	global	\N	en	str_mapserver_help_text	Herea are the Mapserver's map files (private and public). In these map files should be defined the layers which will called by the OpenLayers. Here you can read more about the map files: <a href='http://mapserver.org/mapfile/'>http://mapserver.org/mapfile/</a>
1826	global	\N	hu	str_mapserver_help_text	Itt vannak a Mapserver map fájlok a privát és publikus eléréshez. Ezekben a map fájlokban kell definiálni a rétegeket amiket az OpenLayers függvények meghívnak. A mapfájlok leírása itt található: <a href='http://mapserver.org/mapfile/'>http://mapserver.org/mapfile/</a>
1827	global	\N	ro	str_mapserver_help_text	Aici găsiți fișierere map de pe Mapserver (private și publice). În aceste fișiere map trebuie definite straturile (layers) care vor fi accesate de OpenLayers. Aici puteți citi mai mult despre fișierele map: <a href='http://mapserver.org/mapfile/'>http://mapserver.org/mapfile/</a>
1828	global	\N	en	str_mapserver_layer	mapserver layer
1829	global	\N	hu	str_mapserver_layer	mapszerver réteg
1830	global	\N	ro	str_mapserver_layer	mapserver layer
1831	global	\N	en	str_master	master
1832	global	\N	hu	str_master	üzemeltető
1833	global	\N	ro	str_master	master
1834	global	\N	en	str_no_access_to_this_function	You don't have access to this function
1835	global	\N	hu	str_no_access_to_this_function	Nincs hozzáférése ezekhez a funkciókhoz
1836	global	\N	ro	str_no_access_to_this_function	Nu aveți acces la această funcție
1837	global	\N	en	str_no_check	no check
1838	global	\N	hu	str_no_check	nincs ellenőrzés
1839	global	\N	ro	str_no_check	nu verifica
1840	global	\N	en	str_normal	normal
1841	global	\N	hu	str_normal	normál
1842	global	\N	ro	str_normal	normal
1843	global	\N	en	str_no_srid	Data has no SRID (PostGIS)!!!
1844	global	\N	hu	str_no_srid	Az adatoknak nincs SRID beállítása (PostGIS)!!!
1845	global	\N	ro	str_no_srid	Datele nu conțin setări SRID (PostGIS)!!!
1846	global	\N	en	str_operator	operator
1847	global	\N	hu	str_operator	kezelő
1848	global	\N	ro	str_operator	operator
1849	global	\N	en	str_order	order
1850	global	\N	hu	str_order	sorrend
1851	global	\N	ro	str_order	ordine
1852	global	\N	en	str_parked	parked
1853	global	\N	hu	str_parked	felfüggesztett
1854	global	\N	ro	str_parked	suspendat
1855	global	\N	en	str_pending_invites	pending invites
1856	global	\N	hu	str_pending_invites	függő meghívások
1857	global	\N	ro	str_pending_invites	învitații în așteptare
1858	global	\N	en	str_personal_message	personal message
1859	global	\N	hu	str_personal_message	személyes üzenet
1860	global	\N	ro	str_personal_message	note personale
1861	global	\N	en	str_photos_dir_not_exists	photos directory not exists or not accessible!
1862	global	\N	hu	str_photos_dir_not_exists	photos könyvtár nem létezik vagy nem elérhető!
1863	global	\N	ro	str_photos_dir_not_exists	
1864	global	\N	en	str_project_srid	Project SRID (PostGIS)
1865	global	\N	hu	str_project_srid	Projekt SRID-ja (PostGIS)
1866	global	\N	ro	str_project_srid	Project SRID (PostGIS)
1867	global	\N	en	str_psql_function_help	Here are the database related basic plpgsql functions and triggers.
1868	global	\N	hu	str_psql_function_help	Itt lehet az adatbázishoz tartozó plpgsql függvényeket kezelni.
1872	global	\N	ro	str_saved_import_session_by	Sesiune de încărcare salvată de
1873	global	\N	en	str_server_logs	server logs
1874	global	\N	hu	str_server_logs	szerver logok
1875	global	\N	ro	str_server_logs	server logs
1876	global	\N	en	str_short_desc	short description (one-two words)
1877	global	\N	hu	str_short_desc	rövid leírás (1-2 szó)
1878	global	\N	ro	str_short_desc	descriere scurtă (una-două cuvinte)
1879	global	\N	en	str_spatial	spatial
1880	global	\N	hu	str_spatial	térbeli
1881	global	\N	ro	str_spatial	spațial
1882	global	\N	en	str_taxon_list_update	Taxon lista auto update
1883	global	\N	hu	str_taxon_list_update	Taxon lista auto update
1884	global	\N	ro	str_taxon_list_update	Auto update listă taxon
1885	global	\N	en	str_taxon_name_update	Taxonname auto update
1886	global	\N	hu	str_taxon_name_update	Taxonname auto update
1887	global	\N	ro	str_taxon_name_update	Auto update denumire taxon
1888	global	\N	en	str_visible_name	visible name
1889	global	\N	hu	str_visible_name	látható név
1890	global	\N	ro	str_visible_name	nume vizibilă
1891	global	\N	en	str_webmap_layer	web map layer
1892	global	\N	hu	str_webmap_layer	webes térképi réteg
1893	global	\N	ro	str_webmap_layer	web map layer
1894	global	\N	en	str_zero_access_explanation	No access of data - such like non-logined users
1895	global	\N	hu	str_zero_access_explanation	Nincs adathozzáférés - mint a nem bejelentkezett felhasználóknak
1896	global	\N	ro	str_zero_access_explanation	Fără acces la date – ca utilizatorii nelogați
1897	global	\N	en	str_search_filter	search filter
1898	global	\N	hu	str_search_filter	kereső szűrő
1899	global	\N	ro	str_search_filter	search filter
1900	global	\N	en	str_selected	selected
1901	global	\N	hu	str_selected	kiválasztva
1902	global	\N	ro	str_selected	selected
1903	global	\N	en	str_all_selected	all selected
1904	global	\N	hu	str_all_selected	mind kiválasztva
1905	global	\N	ro	str_all_selected	all selected
1906	global	\N	en	str_display_data	display data
1907	global	\N	hu	str_display_data	adatok ábrázolása
1908	global	\N	ro	str_display_data	display data
1909	global	\N	en	str_deselect_all_skips	Deselect all skip marks
1910	global	\N	hu	str_deselect_all_skips	Minden kihagyás jelölés megszüntetése
1911	global	\N	ro	str_deselect_all_skips	Deselect all skip marks
1912	global	\N	en	str_reversing_skips	Reverse set skip marks
1913	global	\N	hu	str_reversing_skips	Kihagyás jelölések megfordítása
1914	global	\N	ro	str_reversing_skips	Reverse set skip marks
1915	global	\N	en	str_newform_help_1	Without whitespaces, lowercase letters
1916	global	\N	hu	str_newform_help_1	Szóköz nélkül, csupa kisbetű, ékezetes karakterek nélkül.
1917	global	\N	ro	str_newform_help_1	fără spații, cu litere mici, fără diacritice.
1918	global	\N	en	str_newform_help_2	Sort description, headline
1919	global	\N	hu	str_newform_help_2	Rövid leírás, címsor.
1920	global	\N	ro	str_newform_help_2	scurtă descriere
1921	global	\N	en	str_newform_help_3	Description.
1922	global	\N	hu	str_newform_help_3	Tetszőleges hosszú leírás.
1923	global	\N	ro	str_newform_help_3	descriere
1924	global	\N	en	str_newform_help_4	Nonpublic database's comments.
1925	global	\N	hu	str_newform_help_4	Nem publikus adatbázis kommentek.
1926	global	\N	ro	str_newform_help_4	comentarii bază de date cu acces restricționat
1927	global	\N	en	str_non_obligatory_settings	non obligatory settings
1928	global	\N	hu	str_non_obligatory_settings	nem kötelező beállítások
1929	global	\N	ro	str_non_obligatory_settings	non obligatory settings
1930	global	\N	en	str_recent_uploads	Recent uploads
1931	global	\N	hu	str_recent_uploads	Legutóbbi feltöltések
1932	global	\N	ro	str_recent_uploads	Recent uploads
\.

--
-- Data for Name: user_groups; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

--COPY public.user_groups (user_id, group_id) FROM stdin;
--1	1
--\.


--
-- Data for Name: admin_pages; Type: TABLE DATA; Schema: public; Owner: biomapsadmin
--

COPY public.admin_pages (name, label, "group") FROM stdin;
dbcols	str_db_cols	project_admin
groups	str_groups	project_admin
upload_forms	str_upload_forms	project_admin
functions	str_functions	project_admin
taxon	str_taxon_names	project_admin
access	str_access	project_admin
languages	str_lang_definitions	project_admin
modules	str_modules	project_admin
imports	str_load_imports	project_admin
files	str_file_manager	project_admin
query_def	str_query_definitions	project_admin
openlayes_def	str_layer_definitions	project_admin
members	str_members	project_admin
mapserv	str_map_definitions	project_admin
server_logs	str_server_logs	project_admin
r_server	rshinyserver	project_admin
create_table	str_create_table	project_admin
serverinfo	str_serverinfo	project_admin
privileges	str_group_privileges	project_admin
invites	str_invites	invites
newsread	str_messages	messages
new_project_form	str_new_project	new_project
projekt_nyilvantarto	Projekt nyilvántartó	projekt_nyilvantarto
\.


--
-- Name: biomaps_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.biomaps_events_id_seq', 32, true);


--
-- Name: custom_reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.custom_reports_id_seq', 7, true);


--
-- Name: evaluations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.evaluations_id_seq', 79, true);


--
-- Name: form_training_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.form_training_id_seq', 4, true);


--
-- Name: form_training_questions_qid_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.form_training_questions_qid_seq', 13, true);


--
-- Name: groups_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.groups_group_id_seq', 142, true);


--
-- Name: invites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.invites_id_seq', 247, true);


--
-- Name: modules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.modules_id_seq', 369, true);


--
-- Name: pds_upload_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.pds_upload_test_id_seq', 1, true);


--
-- Name: preojects_news_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.preojects_news_stream_id_seq', 1, true);


--
-- Name: project_forms_form_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_forms_form_id_seq', 2, true);


--
-- Name: project_layers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_layers_id_seq', 52, true);


--
-- Name: project_queries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_queries_id_seq', 1, true);


--
-- Name: project_roles_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_roles_role_id_seq', 1, true);


--
-- Name: project_visitors_visitor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_visitors_visitor_id_seq', 1, true);


--
-- Name: queries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.queries_id_seq', 1, true);


--
-- Name: statusz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.statusz_id_seq', 1, false);


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: biomapsadmin
--

SELECT pg_catalog.setval('public.translations_id_seq', 1712, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 7, true);


