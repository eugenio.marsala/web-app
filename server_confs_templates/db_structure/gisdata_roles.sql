--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE biomapsadmin;
ALTER ROLE biomapsadmin WITH SUPERUSER NOINHERIT CREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5931990ef0e9da8e10b71cb5ae96793d4' VALID UNTIL 'infinity';
CREATE ROLE sablon_admin;
ALTER ROLE sablon_admin WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD '12345' VALID UNTIL 'infinity';
CREATE ROLE sablon_user;
ALTER ROLE sablon_user WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE gisadmin;
ALTER ROLE gisadmin WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md579c5a2e5c927587baa5072a932209b18';
CREATE ROLE project_admin;
ALTER ROLE project_admin WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION PASSWORD 'md52a987f8af4211e99846c77376a42ae43' VALID UNTIL 'infinity';
ALTER ROLE sablon_admin SET search_path TO public, system, pg_catalog;
ALTER ROLE project_admin SET search_path TO public, system, pg_catalog;


--
-- Role memberships
--

GRANT sablon_admin TO biomapsadmin WITH ADMIN OPTION GRANTED BY gisadmin;
GRANT project_admin TO sablon_admin GRANTED BY gisadmin;


--
-- PostgreSQL database cluster dump complete
--

