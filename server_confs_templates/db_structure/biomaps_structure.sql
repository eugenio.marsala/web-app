--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.10
-- Dumped by pg_dump version 10.5 (Debian 10.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--CREATE DATABASE biomaps WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';

--\c biomaps;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: delete_project(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.delete_project() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   BEGIN
      IF TG_OP = 'DELETE' THEN
         DELETE FROM  project_users
            WHERE project_table = OLD.project_table;
         DELETE FROM  groups
            WHERE project_table = OLD.project_table;
         DELETE FROM  invites
            WHERE project_table = OLD.project_table;
         DELETE FROM  header_names
            WHERE f_table_name = OLD.project_table;
         DELETE FROM  project_layers
            WHERE project_table = OLD.project_table;
         DELETE FROM  project_descriptions
            WHERE projecttable = OLD.project_table;
      END IF;
      RETURN OLD;
   END;
$$;


ALTER FUNCTION public.delete_project() OWNER TO biomapsadmin;

--
-- Name: dms2dd(character varying); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.dms2dd(strdegminsec character varying) RETURNS numeric
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    DECLARE
       i               numeric;
       intDmsLen       numeric;          -- Length of original string
       strCompassPoint Char(1);
       strNorm         varchar(16) = ''; -- Will contain normalized string
       strDegMinSecB   varchar(100);
       blnGotSeparator integer;          -- Keeps track of separator sequences
       arrDegMinSec    varchar[];        -- TYPE stringarray is table of varchar(2048) ;
       dDeg            numeric := 0;
       dMin            numeric := 0;
       dSec            numeric := 0;
       strChr          Char(1);
    BEGIN
       -- Remove leading and trailing spaces
       strDegMinSecB := REPLACE(strDegMinSec,' ','');
       -- assume no leading and trailing spaces?
       intDmsLen := Length(strDegMinSecB);

       blnGotSeparator := 0; -- Not in separator sequence right now

       -- Loop over string, replacing anything that is not a digit or a
       -- decimal separator with
       -- a single blank
       FOR i in 1..intDmsLen LOOP
          -- Get current character
          strChr := SubStr(strDegMinSecB, i, 1);
          -- either add character to normalized string or replace
          -- separator sequence with single blank         
          If strpos('0123456789,.', strChr) > 0 Then
             -- add character but replace comma with point
             If (strChr <> ',') Then
                strNorm := strNorm || strChr;
             Else
                strNorm := strNorm || '.';
             End If;
             blnGotSeparator := 0;
          ElsIf strpos('neswNESW',strChr) > 0 Then -- Extract Compass Point if present
            strCompassPoint := strChr;
          Else
             -- ensure only one separator is replaced with a blank -
             -- suppress the rest
             If blnGotSeparator = 0 Then
                strNorm := strNorm || ' ';
                blnGotSeparator := 0;
             End If;
          End If;
       End Loop;

       -- Split normalized string into array of max 3 components
       arrDegMinSec := string_to_array(strNorm, ' ');

       --convert specified components to double
       i := array_upper(arrDegMinSec,1);
       If i >= 1 Then
          dDeg := CAST(arrDegMinSec[1] AS numeric);
       End If;
       If i >= 2 Then
          dMin := CAST(arrDegMinSec[2] AS numeric);
       End If;
       If i >= 3 Then
          dSec := CAST(arrDegMinSec[3] AS numeric);
       End If;

       -- convert components to value
       return (CASE WHEN UPPER(strCompassPoint) IN ('S','W') 
                    THEN -1 
                    ELSE 1 
                END 
               *
               (dDeg + dMin / 60 + dSec / 3600));
    End 
$$;


ALTER FUNCTION public.dms2dd(strdegminsec character varying) OWNER TO biomapsadmin;

--
-- Name: drop_user(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.drop_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
      IF TG_OP = 'DELETE' THEN
         DELETE FROM project_users WHERE user_id = OLD.id;
         DELETE FROM project_roles WHERE user_id = OLD.id;
         DELETE FROM invites WHERE user_id = OLD.id;
      END IF;
      RETURN OLD;
END;
$$;


ALTER FUNCTION public.drop_user() OWNER TO biomapsadmin;

--
-- Name: form_last_mod(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.form_last_mod() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    NEW.last_mod = now();
    RETURN NEW;	
END;$$;


ALTER FUNCTION public.form_last_mod() OWNER TO biomapsadmin;

--
-- Name: idx(anyarray, anyelement); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.idx(anyarray, anyelement) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
  SELECT i FROM (
     SELECT generate_series(array_lower($1,1),array_upper($1,1))
  ) g(i)
  WHERE $1[i] = $2
  LIMIT 1;
$_$;


ALTER FUNCTION public.idx(anyarray, anyelement) OWNER TO biomapsadmin;

--
-- Name: obm_useridhash(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.obm_useridhash() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    SELECT md5(encrypt(NEW.email::bytea,'obm','des')) INTO new."user";
    RETURN NEW;
END$$;


ALTER FUNCTION public.obm_useridhash() OWNER TO biomapsadmin;

--
-- Name: role_itself(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.role_itself() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF TG_OP = 'INSERT' THEN
        new.container = ARRAY[new.role_id];
    END IF;
    RETURN new;
END;$$;


ALTER FUNCTION public.role_itself() OWNER TO biomapsadmin;

--
-- Name: update_user_vote(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.update_user_vote() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    BEGIN
      IF NEW."table"='users' AND NEW.valuation!=0 THEN
         UPDATE users SET validation=(SELECT avg(valuation) FROM evaluations
         WHERE NEW."table"='users' AND row=NEW.row AND valuation!='0') WHERE id=NEW.row;
      END IF;
      RETURN NEW;
   END;
$$;


ALTER FUNCTION public.update_user_vote() OWNER TO biomapsadmin;

--
-- Name: update_role_description(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.update_role_description () RETURNS trigger 
LANGUAGE plpgsql 
AS $$
BEGIN
    IF TG_OP = 'UPDATE' AND NEW.username != OLD.username THEN
        UPDATE project_roles SET description =NEW.username WHERE user_id = NEW.id;
    END IF;
    RETURN new;
END;
$$;

ALTER FUNCTION public.update_role_description() OWNER TO biomapsadmin;

--
-- Name: user_role(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION public.user_role() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO project_roles (project_table,description,user_id) SELECT project_table,username,user_id FROM project_users LEFT JOIN users ON (id=user_id) WHERE id=new.user_id AND project_table=new.project_table;
    END IF;
    RETURN new;
END;
$$;


ALTER FUNCTION public.user_role() OWNER TO biomapsadmin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_log; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.access_log (
    login character varying(32),
    date character varying(100),
    request character varying(255),
    ip_address character varying(15),
    password character varying(32)
);


ALTER TABLE public.access_log OWNER TO biomapsadmin;

--
-- Name: admin_pages; Type: TABLE; Schema: public; Owner: biomapsadmin; Tablespace: 
--

CREATE TABLE public.admin_pages (
    name character varying NOT NULL,
    label character varying NOT NULL,
    "group" character varying NOT NULL
);


ALTER TABLE public.admin_pages OWNER TO biomapsadmin;

--
-- Name: admin_pages_name; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin; Tablespace: 
--

ALTER TABLE ONLY public.admin_pages
    ADD CONSTRAINT admin_pages_name PRIMARY KEY (name);


--
-- Name: biomaps_events; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.biomaps_events (
    id integer NOT NULL,
    tipus character varying,
    leiras text,
    hely character varying,
    idopont timestamp without time zone,
    idopont_vege timestamp without time zone
);


ALTER TABLE public.biomaps_events OWNER TO biomapsadmin;

--
-- Name: biomaps_events_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.biomaps_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.biomaps_events_id_seq OWNER TO biomapsadmin;

--
-- Name: biomaps_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.biomaps_events_id_seq OWNED BY public.biomaps_events.id;


--
-- Name: custom_reports; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.custom_reports (
    id integer NOT NULL,
    command text,
    user_id integer NOT NULL,
    key character varying(32),
    project character varying(64),
    query_string text,
    project_table character varying(64)
);


ALTER TABLE public.custom_reports OWNER TO biomapsadmin;

--
-- Name: custom_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.custom_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.custom_reports_id_seq OWNER TO biomapsadmin;

--
-- Name: custom_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.custom_reports_id_seq OWNED BY public.custom_reports.id;


--
-- Name: evaluations; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.evaluations (
    "table" character varying(64) NOT NULL,
    "row" integer NOT NULL,
    user_name character varying(128) NOT NULL,
    valuation integer,
    id integer NOT NULL,
    datum timestamp without time zone DEFAULT now(),
    user_id integer,
    comments text,
    related_id integer
);


ALTER TABLE public.evaluations OWNER TO biomapsadmin;

--
-- Name: evaluations_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.evaluations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evaluations_id_seq OWNER TO biomapsadmin;

--
-- Name: evaluations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.evaluations_id_seq OWNED BY public.evaluations.id;


--
-- Name: form_training; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.form_training (
    id integer NOT NULL,
    form_id integer,
    html text,
    task_description text,
    enabled boolean,
    title character varying(32),
    qorder smallint,
    project_table character varying(64)
);


ALTER TABLE public.form_training OWNER TO biomapsadmin;

--
-- Name: COLUMN form_training.qorder; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.form_training.qorder IS 'question''s order';


--
-- Name: form_training_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.form_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.form_training_id_seq OWNER TO biomapsadmin;

--
-- Name: form_training_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.form_training_id_seq OWNED BY public.form_training.id;


--
-- Name: form_training_questions; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.form_training_questions (
    qid integer NOT NULL,
    training_id integer NOT NULL,
    caption text,
    answers json,
    qtype character varying(12) DEFAULT 'singleselect'::character varying
);


ALTER TABLE public.form_training_questions OWNER TO biomapsadmin;

--
-- Name: form_training_questions_qid_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.form_training_questions_qid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.form_training_questions_qid_seq OWNER TO biomapsadmin;

--
-- Name: form_training_questions_qid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.form_training_questions_qid_seq OWNED BY public.form_training_questions.qid;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.groups (
    group_id integer NOT NULL,
    project_table character varying(32) NOT NULL,
    description character varying(255) NOT NULL,
    roles integer[]
);


ALTER TABLE public.groups OWNER TO biomapsadmin;

--
-- Name: groups_group_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.groups_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_group_id_seq OWNER TO biomapsadmin;

--
-- Name: groups_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.groups_group_id_seq OWNED BY public.groups.group_id;


--
-- Name: handshakes; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.handshakes (
    key character varying(32) NOT NULL,
    project_table character varying(64) NOT NULL,
    user_id integer,
    datum timestamp with time zone DEFAULT now(),
    form_id integer,
    hs character varying(16) NOT NULL
);


ALTER TABLE public.handshakes OWNER TO biomapsadmin;

--
-- Name: header_names; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.header_names (
    f_table_schema character varying(256) NOT NULL,
    f_table_name character varying(256) NOT NULL,
    f_species_column character varying(256),
    f_date_columns character varying[],
    f_quantity_column character varying(256),
    f_order_columns text,
    f_id_column character varying,
    f_cite_person_columns character varying[],
    f_x_column character varying(32),
    f_y_column character varying(32),
    f_srid character varying(64),
    f_geom_column character varying(16),
    f_alter_speciesname_columns character varying[],
    f_file_id_columns character varying[],
    f_restrict_column character varying(32),
    f_main_table character varying(32),
    f_utmzone_column character varying(16)
);


ALTER TABLE public.header_names OWNER TO biomapsadmin;

--
-- Name: COLUMN header_names.f_order_columns; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.header_names.f_order_columns IS 'comma separated list of columns (JSON array)';


--
-- Name: COLUMN header_names.f_x_column; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.header_names.f_x_column IS 'X coordinate''s column';


--
-- Name: COLUMN header_names.f_y_column; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.header_names.f_y_column IS 'Y coordinate''s column';


--
-- Name: COLUMN header_names.f_alter_speciesname_columns; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.header_names.f_alter_speciesname_columns IS 'column => label';


--
-- Name: COLUMN header_names.f_file_id_columns; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.header_names.f_file_id_columns IS 'file reference';


--
-- Name: invites; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.invites (
    id integer NOT NULL,
    user_id integer NOT NULL,
    code character varying(256) NOT NULL,
    name character varying(64) NOT NULL,
    mail character varying(64) NOT NULL,
    created timestamp without time zone NOT NULL,
    project_table character varying(32),
    "group" integer
);


ALTER TABLE public.invites OWNER TO biomapsadmin;

--
-- Name: invites_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.invites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invites_id_seq OWNER TO biomapsadmin;

--
-- Name: invites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.invites_id_seq OWNED BY public.invites.id;


--
-- Name: modules; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.modules (
    project_table character varying(64) NOT NULL,
    module_name character varying(64),
    file character varying,
    function character varying(64),
    enabled boolean DEFAULT true,
    id integer NOT NULL,
    params character varying[],
    module_access integer DEFAULT 0,
    main_table character varying(64),
    group_access integer[] DEFAULT '{0}'::integer[]
);


ALTER TABLE public.modules OWNER TO biomapsadmin;

--
-- Name: modules_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.modules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modules_id_seq OWNER TO biomapsadmin;

--
-- Name: modules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_access_tokens (
    access_token character varying(40) NOT NULL,
    client_id character varying(80) NOT NULL,
    user_id character varying(255),
    expires timestamp without time zone NOT NULL,
    scope character varying(2000)
);


ALTER TABLE public.oauth_access_tokens OWNER TO biomapsadmin;

--
-- Name: oauth_authorization_codes; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_authorization_codes (
    authorization_code character varying(40) NOT NULL,
    client_id character varying(80) NOT NULL,
    user_id character varying(255),
    redirect_uri character varying(2000),
    expires timestamp without time zone NOT NULL,
    scope character varying(2000)
);


ALTER TABLE public.oauth_authorization_codes OWNER TO biomapsadmin;

--
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_clients (
    client_id character varying(80) NOT NULL,
    client_secret character varying(80),
    redirect_uri character varying(2000) NOT NULL,
    grant_types character varying(80),
    scope character varying(100),
    user_id character varying(80)
);


ALTER TABLE public.oauth_clients OWNER TO biomapsadmin;

--
-- Name: oauth_jwt; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_jwt (
    client_id character varying(80) NOT NULL,
    subject character varying(80),
    public_key character varying(2000)
);


ALTER TABLE public.oauth_jwt OWNER TO biomapsadmin;

--
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_refresh_tokens (
    refresh_token character varying(40) NOT NULL,
    client_id character varying(80) NOT NULL,
    user_id character varying(255),
    expires timestamp without time zone NOT NULL,
    scope character varying(2000)
);


ALTER TABLE public.oauth_refresh_tokens OWNER TO biomapsadmin;

--
-- Name: oauth_scopes; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_scopes (
    scope text,
    is_default boolean
);


ALTER TABLE public.oauth_scopes OWNER TO biomapsadmin;

--
-- Name: oauth_users; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.oauth_users (
    username character varying(255) NOT NULL,
    password character varying(2000),
    first_name character varying(255),
    last_name character varying(255)
);


ALTER TABLE public.oauth_users OWNER TO biomapsadmin;

--
-- Name: pds_upload_data; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.pds_upload_data (
    id integer NOT NULL,
    datum timestamp with time zone DEFAULT now(),
    comment text,
    geometry public.geometry,
    error character varying(256),
    files character varying[],
    user_id integer,
    project_table character varying(64),
    form_id integer,
    status character varying(1)
);


ALTER TABLE public.pds_upload_data OWNER TO biomapsadmin;

--
-- Name: TABLE pds_upload_data; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON TABLE public.pds_upload_data IS 'Uploaded data using PDS API';


--
-- Name: pds_upload_test_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.pds_upload_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pds_upload_test_id_seq OWNER TO biomapsadmin;

--
-- Name: pds_upload_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.pds_upload_test_id_seq OWNED BY public.pds_upload_data.id;


--
-- Name: project_news_stream; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_news_stream (
    id integer NOT NULL,
    datum timestamp with time zone,
    news text,
    priority integer,
    uploader integer,
    project_table character varying(32),
    level character varying(12),
    receiver integer,
    readed boolean DEFAULT false
);


ALTER TABLE public.project_news_stream OWNER TO biomapsadmin;

--
-- Name: preojects_news_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.preojects_news_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.preojects_news_stream_id_seq OWNER TO biomapsadmin;

--
-- Name: preojects_news_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.preojects_news_stream_id_seq OWNED BY public.project_news_stream.id;


--
-- Name: project_descriptions; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_descriptions (
    projecttable character varying(32),
    language character varying(3),
    short character varying(255),
    long text
);


ALTER TABLE public.project_descriptions OWNER TO biomapsadmin;

--
-- Name: project_forms; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_forms (
    form_id integer NOT NULL,
    project_table character varying(64),
    form_name character varying(128),
    form_type character varying(16)[],
    form_access numeric DEFAULT 1 NOT NULL,
    user_id numeric,
    active numeric(1,0) DEFAULT 1 NOT NULL,
    groups integer[],
    description text,
    destination_table character varying(64),
    srid character varying[],
    data_owners integer[],
    data_groups integer[],
    last_mod timestamp without time zone DEFAULT now(),
    ext_string character varying(128)
);


ALTER TABLE public.project_forms OWNER TO biomapsadmin;

--
-- Name: TABLE project_forms; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON TABLE public.project_forms IS 'Data upload forms';


--
-- Name: COLUMN project_forms.form_type; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms.form_type IS 'web,file,api';


--
-- Name: COLUMN project_forms.form_access; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms.form_access IS '0,1,2';


--
-- Name: COLUMN project_forms.active; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms.active IS 'disable a form by set 0';


--
-- Name: COLUMN project_forms.groups; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms.groups IS 'Groups'' rigths';


--
-- Name: COLUMN project_forms.ext_string; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms.ext_string IS 'c:utf16,s:;h:yes';


--
-- Name: project_forms_data; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_forms_data (
    form_id integer NOT NULL,
    "column" character varying(255) NOT NULL,
    description text,
    type character varying(64),
    control character varying(16),
    count numeric[],
    list character varying[],
    obl numeric,
    fullist numeric(1,0) DEFAULT 0 NOT NULL,
    default_value character varying(255),
    genlist character varying(128),
    api_params json,
    relation character varying(255),
    regexp character varying(128),
    spatial public.geometry,
    pseudo_columns character varying(64),
    custom_function character varying(255),
    list_definition json,
    position_order smallint
);


ALTER TABLE public.project_forms_data OWNER TO biomapsadmin;

--
-- Name: COLUMN project_forms_data.type; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms_data.type IS 'field type';


--
-- Name: COLUMN project_forms_data.control; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms_data.control IS 'control type';


--
-- Name: COLUMN project_forms_data.fullist; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms_data.fullist IS 'full list? (0,1)';


--
-- Name: COLUMN project_forms_data.regexp; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms_data.regexp IS 'a regular expression';


--
-- Name: COLUMN project_forms_data.spatial; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_forms_data.spatial IS 'spatial control';


--
-- Name: project_forms_form_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.project_forms_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_forms_form_id_seq OWNER TO biomapsadmin;

--
-- Name: project_forms_form_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.project_forms_form_id_seq OWNED BY public.project_forms.form_id;


--
-- Name: project_forms_groups; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_forms_groups (
    name character varying(128) NOT NULL,
    form_id smallint[],
    c_order smallint,
    project_table character varying(64) NOT NULL
);


ALTER TABLE public.project_forms_groups OWNER TO biomapsadmin;

--
-- Name: project_interconnects; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_interconnects (
    project_table character varying(64) NOT NULL,
    inter_table character varying(64) NOT NULL,
    pt_id_col character varying(64) NOT NULL,
    it_id_col character varying(64) NOT NULL
);


ALTER TABLE public.project_interconnects OWNER TO biomapsadmin;

--
-- Name: project_layers; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_layers (
    project_table character varying(64) DEFAULT NULL::character varying,
    layer_name character varying(64) NOT NULL,
    layer_def text,
    tipus character varying(24),
    url text,
    map text,
    name character varying(128),
    enabled boolean DEFAULT true,
    id integer NOT NULL,
    layer_order integer,
    ms_layer character varying(32),
    singletile boolean DEFAULT false,
    legend boolean DEFAULT false
);


ALTER TABLE public.project_layers OWNER TO biomapsadmin;

--
-- Name: TABLE project_layers; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON TABLE public.project_layers IS 'Openlayers definitions for mapserver';


--
-- Name: project_layers_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.project_layers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_layers_id_seq OWNER TO biomapsadmin;

--
-- Name: project_layers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.project_layers_id_seq OWNED BY public.project_layers.id;

--
-- Name: project_mapserver_layers; Type: TABLE; Schema: public; Owner: biomapsadmin; Tablespace: 
--

CREATE TABLE public.project_mapserver_layers (
    mapserv_layer_name character varying,
    project_table character varying,
    geometry_type character varying
);


ALTER TABLE public.project_mapserver_layers OWNER TO biomapsadmin;

--
-- Name: project_mapserver_layers_ukey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin; Tablespace: 
--

ALTER TABLE ONLY public.project_mapserver_layers
    ADD CONSTRAINT project_mapserver_layers_ukey UNIQUE (mapserv_layer_name, project_table, geometry_type);

--
-- Name: project_metaname; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_metaname (
    project_table character varying(64) NOT NULL,
    column_name character varying(64) NOT NULL,
    description text,
    short_name character varying(128) NOT NULL,
    rights numeric[],
    "order" integer
);


ALTER TABLE public.project_metaname OWNER TO biomapsadmin;

--
-- Name: TABLE project_metaname; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON TABLE public.project_metaname IS 'projects metadata table';


--
-- Name: COLUMN project_metaname.description; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_metaname.description IS 'Meta Description of column. E.g.: methods description, data types';


--
-- Name: COLUMN project_metaname.short_name; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_metaname.short_name IS 'viseible name of the column';


--
-- Name: COLUMN project_metaname.rights; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_metaname.rights IS 'access rights';


--
-- Name: project_queries_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.project_queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_queries_id_seq OWNER TO biomapsadmin;

--
-- Name: project_queries; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_queries (
    project_table character varying(64) DEFAULT NULL::character varying,
    layer_query text,
    enabled boolean DEFAULT true,
    id integer DEFAULT nextval('public.project_queries_id_seq'::regclass) NOT NULL,
    layer_type character varying(8),
    rst integer,
    layer_cname character varying(32),
    main_table character varying(64)
);


ALTER TABLE public.project_queries OWNER TO biomapsadmin;

--
-- Name: COLUMN project_queries.rst; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_queries.rst IS 'basic access control';


--
-- Name: project_roles_role_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.project_roles_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_roles_role_id_seq OWNER TO biomapsadmin;

--
-- Name: project_roles; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_roles (
    role_id integer DEFAULT nextval('public.project_roles_role_id_seq'::regclass),
    container integer[],
    project_table character varying(32),
    description character varying(32),
    user_id integer
);


ALTER TABLE public.project_roles OWNER TO biomapsadmin;

--
-- Name: project_users; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_users (
    project_table character varying(32) NOT NULL,
    user_id integer NOT NULL,
    user_status character varying(10) DEFAULT 'normal'::character varying NOT NULL,
    join_date timestamp with time zone DEFAULT now(),
    receive_mails smallint DEFAULT 1,
    visible_mail smallint DEFAULT 1
);


ALTER TABLE public.project_users OWNER TO biomapsadmin;

--
-- Name: COLUMN project_users.user_status; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_users.user_status IS '1: user, 2: project manager';


--
-- Name: project_variables; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_variables (
    project_table character varying(64) NOT NULL,
    map_center public.geometry,
    map_zoom smallint DEFAULT 7,
    click_buffer smallint DEFAULT 0,
    zoom_wheel boolean DEFAULT true,
    default_results_view character varying(12) DEFAULT 'list'::character varying,
    default_layer character varying(12) DEFAULT 'layer_osm'::character varying,
    fixheader boolean DEFAULT false,
    turn_off_layers character varying(128),
    subfilters boolean DEFAULT false,
    langs json,
    legend boolean DEFAULT false,
    training boolean DEFAULT false,
    rserver boolean DEFAULT false
);


ALTER TABLE public.project_variables OWNER TO biomapsadmin;

--
-- Name: COLUMN project_variables.langs; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_variables.langs IS 'for wikispecies URL creating';


--
-- Name: COLUMN project_variables.legend; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.project_variables.legend IS 'show map legend';


--
-- Name: project_visitors; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.project_visitors (
    visitor_id integer NOT NULL,
    date timestamp with time zone,
    ip_addr character varying(64),
    page character varying,
    action text,
    project_table character varying(64)
);


ALTER TABLE public.project_visitors OWNER TO biomapsadmin;

--
-- Name: project_visitors_visitor_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.project_visitors_visitor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_visitors_visitor_id_seq OWNER TO biomapsadmin;

--
-- Name: project_visitors_visitor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.project_visitors_visitor_id_seq OWNED BY public.project_visitors.visitor_id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.projects (
    comment text,
    admin_uid integer NOT NULL,
    project_table character varying(32) NOT NULL,
    creation_date date,
    email character varying(32),
    validation numeric,
    domain character varying(64),
    "Creator" character varying(128),
    stage character varying(16),
    doi character varying(128),
    running_date date,
    licence character varying(255),
    rum character varying(3),
    licence_uri character varying(255),
    alternate_id character varying[],
    collection_dates daterange,
    project_hash character varying(12),
    subjects character varying[],
    geolocation character varying(255),
    local_project boolean DEFAULT true,
    "protocol" character varying(5) NOT NULL DEFAULT 'http'
);


ALTER TABLE public.projects OWNER TO biomapsadmin;

--
-- Name: COLUMN projects.comment; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.projects.comment IS 'non public description of this database';


--
-- Name: COLUMN projects.licence; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.projects.licence IS 'OpenDataCommons: ODbL, ODC-By, PDDL';


--
-- Name: COLUMN projects.rum; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.projects.rum IS 'PUBLIC Read+- Upload +- Modify +-';


--
-- Name: queries; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.queries (
    id integer NOT NULL,
    user_id integer NOT NULL,
    comment character varying(255),
    query text NOT NULL,
    result text,
    datetime timestamp without time zone,
    type character varying(16),
    sessionid character varying(32),
    name character varying(255),
    selection public.geometry,
    doi character varying(64),
    licence text,
    authors character varying(128),
    abstract text,
    licence_uri character varying(200),
    subjects character varying[],
    formats character varying(32),
    data_collectors text,
    date_range daterange,
    publisher character varying(255)
);


ALTER TABLE public.queries OWNER TO biomapsadmin;

--
-- Name: COLUMN queries.name; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.queries.name IS 'short description of query';


--
-- Name: queries_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queries_id_seq OWNER TO biomapsadmin;

--
-- Name: queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: biomapsadmin
--

ALTER SEQUENCE public.queries_id_seq OWNED BY public.queries.id;


--
-- Name: settings_import; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.settings_import (
    user_id integer NOT NULL,
    project_table character varying(128) NOT NULL,
    slist text,
    olist text,
    name character varying(32) NOT NULL,
    sheet character varying(64),
    assignments json
);


ALTER TABLE public.settings_import OWNER TO biomapsadmin;

--
-- Name: COLUMN settings_import.name; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.settings_import.name IS 'template''s name';


--
-- Name: COLUMN settings_import.sheet; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.settings_import.sheet IS 'sheet name';


--
-- Name: statusz_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.statusz_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.statusz_id_seq OWNER TO biomapsadmin;


--
-- Name: supervisor; Type: TABLE; Schema: public; Owner: biomapsadmin; Tablespace: 
--

CREATE TABLE public.supervisor (
    update_id integer NOT NULL,
    status boolean DEFAULT false NOT NULL,
    sqlid integer,
    datum timestamp without time zone DEFAULT now() NOT NULL,
    revision text NOT NULL,
    database character varying(24),
    sqlcmd text,
    project character varying(32)
);


ALTER TABLE public.supervisor OWNER TO biomapsadmin;

--
-- Name: supervisor_update_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.supervisor_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.supervisor_update_id_seq OWNER TO biomapsadmin;

--
-- Name: update_id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.supervisor ALTER COLUMN update_id SET DEFAULT nextval('public.supervisor_update_id_seq'::regclass);


--
-- Name: track_citations; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.track_citations (
    project_table character varying(32),
    id integer NOT NULL,
    source text[],
    data_table character varying
);


ALTER TABLE public.track_citations OWNER TO biomapsadmin;

--
-- Name: track_data_views; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.track_data_views (
    project_table character varying(32),
    id integer NOT NULL,
    counter integer,
    session_id character varying(64)
);


ALTER TABLE public.track_data_views OWNER TO biomapsadmin;

--
-- Name: track_downloads; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.track_downloads (
    project_table character varying(32),
    id integer NOT NULL,
    counter integer,
    session_id character varying(64)
);


ALTER TABLE public.track_downloads OWNER TO biomapsadmin;

--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.translations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.translations_id_seq OWNER TO biomapsadmin;

--
-- Name: translations; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.translations (
    id integer DEFAULT nextval('public.translations_id_seq'::regclass) NOT NULL,
    scope character varying(32) NOT NULL,
    project character varying(32),
    lang character(2) NOT NULL,
    const character varying(128) NOT NULL,
    translation text NOT NULL
);


ALTER TABLE public.translations OWNER TO biomapsadmin;

--
-- Name: user_groups; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.user_groups (
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.user_groups OWNER TO biomapsadmin;

CREATE TABLE public.interconnects_master (
    local_project character varying(32),
    remote_project character varying,
    request_key character varying(32),
    accept_key character varying(32),
    pending boolean DEFAULT true
);

CREATE TABLE public.interconnects_slave (
    local_project character varying(32),
    remote_project character varying,
    accept_key character varying(32),
    pending boolean DEFAULT true
);

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO biomapsadmin;

--
-- Name: users; Type: TABLE; Schema: public; Owner: biomapsadmin
--

CREATE TABLE public.users (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    "user" character varying(255),
    password character varying(255) NOT NULL,
    status numeric(2,0) DEFAULT 0 NOT NULL,
    username character varying(100),
    institute character varying(255),
    address character varying(255),
    email character varying(40),
    inviter integer,
    validation numeric,
    reactivate character varying(16),
    orcid character varying(20),
    "references" character varying[],
    givenname character varying[],
    familyname character varying[],
    pwstate boolean DEFAULT false NOT NULL,
    terms_agree smallint DEFAULT 0
);


ALTER TABLE public.users OWNER TO biomapsadmin;

--
-- Name: COLUMN users."user"; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.users."user" IS 'crypt(id, email)';


--
-- Name: COLUMN users.username; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.users.username IS 'A felhasználó teljes neve';


--
-- Name: COLUMN users.email; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.users.email IS 'azonosító név';


--
-- Name: COLUMN users.orcid; Type: COMMENT; Schema: public; Owner: biomapsadmin
--

COMMENT ON COLUMN public.users.orcid IS 'ORCID id';


--
-- Name: biomaps_events id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.biomaps_events ALTER COLUMN id SET DEFAULT nextval('public.biomaps_events_id_seq'::regclass);


--
-- Name: custom_reports id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.custom_reports ALTER COLUMN id SET DEFAULT nextval('public.custom_reports_id_seq'::regclass);


--
-- Name: evaluations id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.evaluations ALTER COLUMN id SET DEFAULT nextval('public.evaluations_id_seq'::regclass);


--
-- Name: form_training id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.form_training ALTER COLUMN id SET DEFAULT nextval('public.form_training_id_seq'::regclass);


--
-- Name: form_training_questions qid; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.form_training_questions ALTER COLUMN qid SET DEFAULT nextval('public.form_training_questions_qid_seq'::regclass);


--
-- Name: groups group_id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.groups ALTER COLUMN group_id SET DEFAULT nextval('public.groups_group_id_seq'::regclass);


--
-- Name: invites id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.invites ALTER COLUMN id SET DEFAULT nextval('public.invites_id_seq'::regclass);


--
-- Name: modules id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);


--
-- Name: pds_upload_data id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.pds_upload_data ALTER COLUMN id SET DEFAULT nextval('public.pds_upload_test_id_seq'::regclass);


--
-- Name: project_forms form_id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms ALTER COLUMN form_id SET DEFAULT nextval('public.project_forms_form_id_seq'::regclass);


--
-- Name: project_layers id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_layers ALTER COLUMN id SET DEFAULT nextval('public.project_layers_id_seq'::regclass);


--
-- Name: project_news_stream id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_news_stream ALTER COLUMN id SET DEFAULT nextval('public.preojects_news_stream_id_seq'::regclass);


--
-- Name: project_visitors visitor_id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_visitors ALTER COLUMN visitor_id SET DEFAULT nextval('public.project_visitors_visitor_id_seq'::regclass);


--
-- Name: queries id; Type: DEFAULT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.queries ALTER COLUMN id SET DEFAULT nextval('public.queries_id_seq'::regclass);


--
-- Name: oauth_access_tokens access_token_pk; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT access_token_pk PRIMARY KEY (access_token);


--
-- Name: oauth_authorization_codes auth_code_pk; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.oauth_authorization_codes
    ADD CONSTRAINT auth_code_pk PRIMARY KEY (authorization_code);


--
-- Name: biomaps_events biomaps_events_id_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.biomaps_events
    ADD CONSTRAINT biomaps_events_id_key UNIQUE (id);


--
-- Name: oauth_clients clients_client_id_pk; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT clients_client_id_pk PRIMARY KEY (client_id);


--
-- Name: custom_reports custom_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.custom_reports
    ADD CONSTRAINT custom_reports_pkey PRIMARY KEY (id);


--
-- Name: form_training form_training_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.form_training
    ADD CONSTRAINT form_training_pkey PRIMARY KEY (id);


--
-- Name: form_training_questions form_training_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.form_training_questions
    ADD CONSTRAINT form_training_questions_pkey PRIMARY KEY (qid);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (group_id);


--
-- Name: groups groups_project_table_description_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_project_table_description_key UNIQUE (project_table, description);


--
-- Name: handshakes handshakes_key_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.handshakes
    ADD CONSTRAINT handshakes_key_pkey PRIMARY KEY (key);


--
-- Name: header_names i_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.header_names
    ADD CONSTRAINT i_key UNIQUE (f_table_name, f_main_table);


--
-- Name: invites invites_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.invites
    ADD CONSTRAINT invites_pkey PRIMARY KEY (id);


--
-- Name: oauth_jwt jwt_client_id_pk; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.oauth_jwt
    ADD CONSTRAINT jwt_client_id_pk PRIMARY KEY (client_id);


--
-- Name: pds_upload_data pds_upload_test_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.pds_upload_data
    ADD CONSTRAINT pds_upload_test_pkey PRIMARY KEY (id);


--
-- Name: project_news_stream preojects_news_stream_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_news_stream
    ADD CONSTRAINT preojects_news_stream_pkey PRIMARY KEY (id);


--
-- Name: project_descriptions project_descriptions_projecttable_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_descriptions
    ADD CONSTRAINT project_descriptions_projecttable_key UNIQUE (projecttable, language);


--
-- Name: project_forms_data project_forms_data_ukey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms_data
    ADD CONSTRAINT project_forms_data_ukey UNIQUE (form_id, "column");


--
-- Name: project_forms project_forms_form_name_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms
    ADD CONSTRAINT project_forms_form_name_key UNIQUE (form_name);


--
-- Name: project_forms_groups project_forms_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms_groups
    ADD CONSTRAINT project_forms_groups_pkey PRIMARY KEY (project_table, name);


--
-- Name: project_forms project_forms_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms
    ADD CONSTRAINT project_forms_pkey PRIMARY KEY (form_id);


--
-- Name: project_forms project_forms_tablename_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms
    ADD CONSTRAINT project_forms_tablename_key UNIQUE (project_table, form_name);


--
-- Name: project_interconnects project_interconnects_ukey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_interconnects
    ADD CONSTRAINT project_interconnects_ukey UNIQUE (project_table, inter_table);


--
-- Name: project_layers project_layer_ukey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_layers
    ADD CONSTRAINT project_layer_ukey UNIQUE (layer_name, layer_def);


--
-- Name: project_metaname project_metaname_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_metaname
    ADD CONSTRAINT project_metaname_pkey PRIMARY KEY (project_table, column_name);


--
-- Name: modules project_module_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT project_module_pkey PRIMARY KEY (id);


--
-- Name: project_queries project_queries_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_queries
    ADD CONSTRAINT project_queries_pkey PRIMARY KEY (id);


--
-- Name: projects project_table_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT project_table_key PRIMARY KEY (project_table);


--
-- Name: project_users project_users_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_users
    ADD CONSTRAINT project_users_pkey PRIMARY KEY (project_table, user_id);


--
-- Name: project_variables project_variables_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_variables
    ADD CONSTRAINT project_variables_pkey PRIMARY KEY (project_table);


--
-- Name: queries queries_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_pkey PRIMARY KEY (id);


--
-- Name: queries query_doi; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT query_doi UNIQUE (doi);


--
-- Name: oauth_refresh_tokens refresh_token_pk; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.oauth_refresh_tokens
    ADD CONSTRAINT refresh_token_pk PRIMARY KEY (refresh_token);


--
-- Name: project_roles role_ukey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_roles
    ADD CONSTRAINT role_ukey UNIQUE (role_id, project_table);


--
-- Name: settings_import setting_import-primary_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.settings_import
    ADD CONSTRAINT "setting_import-primary_key" UNIQUE (user_id, name);


--
-- Name: update_id_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin; Tablespace: 
--

ALTER TABLE ONLY public.supervisor
    ADD CONSTRAINT update_id_pkey PRIMARY KEY (update_id);


--
-- Name: track_citations track_citations_project_table_id_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.track_citations
    ADD CONSTRAINT track_citations_project_table_id_key UNIQUE (project_table, id);


--
-- Name: track_data_views track_data_views_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.track_data_views
    ADD CONSTRAINT track_data_views_key UNIQUE (project_table, id);


--
-- Name: track_downloads track_downloads_project_table_id_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.track_downloads
    ADD CONSTRAINT track_downloads_project_table_id_key UNIQUE (project_table, id);


--
-- Name: translations translations_id; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_id PRIMARY KEY (id);


--
-- Name: translations translations_scope_project_lang_const; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_scope_project_lang_const UNIQUE (scope, project, lang, const);


--
-- Name: invites ump_ukey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.invites
    ADD CONSTRAINT ump_ukey UNIQUE (user_id, mail, project_table);


--
-- Name: user_groups user_groups_primary_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.user_groups
    ADD CONSTRAINT user_groups_primary_key PRIMARY KEY (user_id, group_id);


--
-- Name: oauth_users username_pk; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.oauth_users
    ADD CONSTRAINT username_pk PRIMARY KEY (username);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: evaluations_key; Type: INDEX; Schema: public; Owner: biomapsadmin
--

CREATE UNIQUE INDEX evaluations_key ON public.evaluations USING btree (id);


--
-- Name: modules_project_module_idx; Type: INDEX; Schema: public; Owner: biomapsadmin
--

CREATE INDEX modules_project_module_idx ON public.modules USING btree (module_name, project_table);


--
-- Name: roles_user_id_idx; Type: INDEX; Schema: public; Owner: biomapsadmin
--

CREATE INDEX roles_user_id_idx ON public.project_roles USING btree (user_id);


--
-- Name: users update_role_description_trg; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER update_role_description_trg AFTER UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.update_role_description();

--
-- Name: project_users insert_role; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER insert_role AFTER INSERT ON public.project_users FOR EACH ROW EXECUTE PROCEDURE public.user_role();


--
-- Name: projects project_delete; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER project_delete BEFORE DELETE ON public.projects FOR EACH ROW EXECUTE PROCEDURE public.delete_project();


--
-- Name: project_forms project_forms_last_mod; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER project_forms_last_mod BEFORE UPDATE ON public.project_forms FOR EACH ROW EXECUTE PROCEDURE public.form_last_mod();


--
-- Name: project_roles role_itself; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER role_itself BEFORE INSERT ON public.project_roles FOR EACH ROW EXECUTE PROCEDURE public.role_itself();


--
-- Name: users user_idhash 	; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER "user_idhash 	" BEFORE INSERT OR UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.obm_useridhash();


--
-- Name: evaluations user_vote; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER user_vote AFTER INSERT ON public.evaluations FOR EACH ROW EXECUTE PROCEDURE public.update_user_vote();


--
-- Name: users users_delete; Type: TRIGGER; Schema: public; Owner: biomapsadmin
--

CREATE TRIGGER users_delete BEFORE DELETE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.drop_user();


--
-- Name: custom_reports custom_report_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.custom_reports
    ADD CONSTRAINT custom_report_project_fkey FOREIGN KEY (project) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: form_training_questions form_training_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.form_training_questions
    ADD CONSTRAINT form_training_fkey FOREIGN KEY (training_id) REFERENCES public.form_training(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_groups groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.user_groups
    ADD CONSTRAINT groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_forms_data project_forms_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms_data
    ADD CONSTRAINT project_forms_fkey FOREIGN KEY (form_id) REFERENCES public.project_forms(form_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: handshakes project_forms_form_id; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.handshakes
    ADD CONSTRAINT project_forms_form_id FOREIGN KEY (form_id) REFERENCES public.project_forms(form_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_forms_groups project_forms_groups_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms_groups
    ADD CONSTRAINT project_forms_groups_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_variables project_variables.fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_variables
    ADD CONSTRAINT "project_variables.fkey" FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: handshakes projects_project_table; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.handshakes
    ADD CONSTRAINT projects_project_table FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: header_names projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.header_names
    ADD CONSTRAINT header_names_projects_project_table_fkey FOREIGN KEY (f_table_name) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_queries projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_queries
    ADD CONSTRAINT project_queries_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_users projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_users
    ADD CONSTRAINT project_users_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_layers projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_layers
    ADD CONSTRAINT project_layers_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_interconnects projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_interconnects
    ADD CONSTRAINT project_interconnects_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_forms projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_forms
    ADD CONSTRAINT project_forms_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_descriptions projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_descriptions
    ADD CONSTRAINT project_descriptions_projects_project_table_fkey FOREIGN KEY (projecttable) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: modules projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: groups projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: invites projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.invites
    ADD CONSTRAINT invites_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: settings_import projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.settings_import
    ADD CONSTRAINT settings_import_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_news_stream projects_project_table_key; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_news_stream
    ADD CONSTRAINT project_news_stream_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: supervisor projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.supervisor
    ADD CONSTRAINT supervisor_projects_project_table_fkey FOREIGN KEY (project) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: project_roles projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_roles
    ADD CONSTRAINT project_roles_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: project_mapserver_layers projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_mapserver_layers
    ADD CONSTRAINT project_mapserver_layers_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: project_queries projects_project_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_projects_project_table_fkey FOREIGN KEY (comment) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: project_roles roles_user_id; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_roles
    ADD CONSTRAINT roles_user_id FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: project_users user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.project_users
    ADD CONSTRAINT user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: custom_reports user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.custom_reports
    ADD CONSTRAINT user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: projects users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (admin_uid) REFERENCES public.users(id);


--
-- Name: user_groups users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.user_groups
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: evaluations users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.evaluations
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: handshakes users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.handshakes
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: settings_import users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: biomapsadmin
--

ALTER TABLE ONLY public.settings_import
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: biomapsadmin
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT USAGE ON SCHEMA public TO biomapsadmin;
GRANT USAGE ON SCHEMA public TO mainpage_admin;


--
-- Name: TABLE access_log; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.access_log FROM PUBLIC;
REVOKE ALL ON TABLE public.access_log FROM biomapsadmin;
GRANT ALL ON TABLE public.access_log TO biomapsadmin;


--
-- Name: TABLE custom_reports; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.custom_reports FROM PUBLIC;
REVOKE ALL ON TABLE public.custom_reports FROM biomapsadmin;
GRANT ALL ON TABLE public.custom_reports TO biomapsadmin;


--
-- Name: SEQUENCE custom_reports_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.custom_reports_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.custom_reports_id_seq FROM biomapsadmin;
GRANT ALL ON SEQUENCE public.custom_reports_id_seq TO biomapsadmin;


--
-- Name: TABLE evaluations; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.evaluations FROM PUBLIC;
REVOKE ALL ON TABLE public.evaluations FROM biomapsadmin;
GRANT ALL ON TABLE public.evaluations TO biomapsadmin;


--
-- Name: TABLE groups; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.groups FROM PUBLIC;
REVOKE ALL ON TABLE public.groups FROM biomapsadmin;
GRANT ALL ON TABLE public.groups TO biomapsadmin;


--
-- Name: TABLE handshakes; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.handshakes FROM PUBLIC;
REVOKE ALL ON TABLE public.handshakes FROM biomapsadmin;
GRANT ALL ON TABLE public.handshakes TO biomapsadmin;


--
-- Name: TABLE header_names; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.header_names FROM PUBLIC;
REVOKE ALL ON TABLE public.header_names FROM biomapsadmin;
GRANT ALL ON TABLE public.header_names TO biomapsadmin;
GRANT SELECT ON TABLE public.header_names TO mainpage_admin;


--
-- Name: TABLE invites; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.invites FROM PUBLIC;
REVOKE ALL ON TABLE public.invites FROM biomapsadmin;
GRANT ALL ON TABLE public.invites TO biomapsadmin;


--
-- Name: TABLE modules; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.modules FROM PUBLIC;
REVOKE ALL ON TABLE public.modules FROM biomapsadmin;
GRANT ALL ON TABLE public.modules TO biomapsadmin;


--
-- Name: SEQUENCE modules_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.modules_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.modules_id_seq FROM biomapsadmin;
GRANT ALL ON SEQUENCE public.modules_id_seq TO biomapsadmin;


--
-- Name: TABLE oauth_access_tokens; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_access_tokens FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_access_tokens FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_access_tokens TO biomapsadmin;


--
-- Name: TABLE oauth_authorization_codes; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_authorization_codes FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_authorization_codes FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_authorization_codes TO biomapsadmin;


--
-- Name: TABLE oauth_clients; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_clients FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_clients FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_clients TO biomapsadmin;


--
-- Name: TABLE oauth_jwt; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_jwt FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_jwt FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_jwt TO biomapsadmin;


--
-- Name: TABLE oauth_refresh_tokens; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_refresh_tokens FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_refresh_tokens FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_refresh_tokens TO biomapsadmin;


--
-- Name: TABLE oauth_scopes; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_scopes FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_scopes FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_scopes TO biomapsadmin;


--
-- Name: TABLE oauth_users; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.oauth_users FROM PUBLIC;
REVOKE ALL ON TABLE public.oauth_users FROM biomapsadmin;
GRANT ALL ON TABLE public.oauth_users TO biomapsadmin;


--
-- Name: TABLE pds_upload_data; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.pds_upload_data FROM PUBLIC;
REVOKE ALL ON TABLE public.pds_upload_data FROM biomapsadmin;
GRANT ALL ON TABLE public.pds_upload_data TO biomapsadmin;


--
-- Name: TABLE project_news_stream; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_news_stream FROM PUBLIC;
REVOKE ALL ON TABLE public.project_news_stream FROM biomapsadmin;
GRANT ALL ON TABLE public.project_news_stream TO biomapsadmin;


--
-- Name: SEQUENCE preojects_news_stream_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.preojects_news_stream_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.preojects_news_stream_id_seq FROM biomapsadmin;
GRANT ALL ON SEQUENCE public.preojects_news_stream_id_seq TO biomapsadmin;


--
-- Name: TABLE project_descriptions; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_descriptions FROM PUBLIC;
REVOKE ALL ON TABLE public.project_descriptions FROM biomapsadmin;
GRANT ALL ON TABLE public.project_descriptions TO biomapsadmin;
GRANT SELECT ON TABLE public.project_descriptions TO mainpage_admin;


--
-- Name: TABLE project_forms; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_forms FROM PUBLIC;
REVOKE ALL ON TABLE public.project_forms FROM biomapsadmin;
GRANT ALL ON TABLE public.project_forms TO biomapsadmin;


--
-- Name: TABLE project_forms_data; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_forms_data FROM PUBLIC;
REVOKE ALL ON TABLE public.project_forms_data FROM biomapsadmin;
GRANT ALL ON TABLE public.project_forms_data TO biomapsadmin;


--
-- Name: TABLE project_interconnects; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_interconnects FROM PUBLIC;
REVOKE ALL ON TABLE public.project_interconnects FROM biomapsadmin;
GRANT ALL ON TABLE public.project_interconnects TO biomapsadmin;


--
-- Name: TABLE project_layers; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_layers FROM PUBLIC;
REVOKE ALL ON TABLE public.project_layers FROM biomapsadmin;
GRANT ALL ON TABLE public.project_layers TO biomapsadmin;


--
-- Name: TABLE project_metaname; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_metaname FROM PUBLIC;
REVOKE ALL ON TABLE public.project_metaname FROM biomapsadmin;
GRANT ALL ON TABLE public.project_metaname TO biomapsadmin;


--
-- Name: SEQUENCE project_queries_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.project_queries_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.project_queries_id_seq FROM biomapsadmin;


--
-- Name: TABLE project_queries; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_queries FROM PUBLIC;
REVOKE ALL ON TABLE public.project_queries FROM biomapsadmin;
GRANT ALL ON TABLE public.project_queries TO biomapsadmin;


--
-- Name: SEQUENCE project_roles_role_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.project_roles_role_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.project_roles_role_id_seq FROM biomapsadmin;
GRANT ALL ON SEQUENCE public.project_roles_role_id_seq TO biomapsadmin;


--
-- Name: TABLE project_roles; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_roles FROM PUBLIC;
REVOKE ALL ON TABLE public.project_roles FROM biomapsadmin;
GRANT ALL ON TABLE public.project_roles TO biomapsadmin;


--
-- Name: TABLE project_users; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_users FROM PUBLIC;
REVOKE ALL ON TABLE public.project_users FROM biomapsadmin;
GRANT ALL ON TABLE public.project_users TO biomapsadmin;


--
-- Name: TABLE project_variables; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_variables FROM PUBLIC;
REVOKE ALL ON TABLE public.project_variables FROM biomapsadmin;
GRANT ALL ON TABLE public.project_variables TO biomapsadmin;


--
-- Name: TABLE project_visitors; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.project_visitors FROM PUBLIC;
REVOKE ALL ON TABLE public.project_visitors FROM biomapsadmin;
GRANT ALL ON TABLE public.project_visitors TO biomapsadmin;


--
-- Name: SEQUENCE project_visitors_visitor_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.project_visitors_visitor_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.project_visitors_visitor_id_seq FROM biomapsadmin;
GRANT ALL ON SEQUENCE public.project_visitors_visitor_id_seq TO biomapsadmin;


--
-- Name: TABLE projects; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.projects FROM PUBLIC;
REVOKE ALL ON TABLE public.projects FROM biomapsadmin;
GRANT ALL ON TABLE public.projects TO biomapsadmin;
GRANT SELECT ON TABLE public.projects TO mainpage_admin;


--
-- Name: TABLE queries; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.queries FROM PUBLIC;
REVOKE ALL ON TABLE public.queries FROM biomapsadmin;
GRANT ALL ON TABLE public.queries TO biomapsadmin;


--
-- Name: SEQUENCE queries_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE public.queries_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.queries_id_seq FROM biomapsadmin;
GRANT ALL ON SEQUENCE public.queries_id_seq TO biomapsadmin;


--
-- Name: TABLE settings_import; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.settings_import FROM PUBLIC;
REVOKE ALL ON TABLE public.settings_import FROM biomapsadmin;
GRANT ALL ON TABLE public.settings_import TO biomapsadmin;


--
-- Name: TABLE track_citations; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.track_citations FROM PUBLIC;
REVOKE ALL ON TABLE public.track_citations FROM biomapsadmin;
GRANT ALL ON TABLE public.track_citations TO biomapsadmin;


--
-- Name: TABLE track_data_views; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.track_data_views FROM PUBLIC;
REVOKE ALL ON TABLE public.track_data_views FROM biomapsadmin;
GRANT ALL ON TABLE public.track_data_views TO biomapsadmin;


--
-- Name: TABLE track_downloads; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.track_downloads FROM PUBLIC;
REVOKE ALL ON TABLE public.track_downloads FROM biomapsadmin;
GRANT ALL ON TABLE public.track_downloads TO biomapsadmin;


--
-- Name: TABLE user_groups; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.user_groups FROM PUBLIC;
REVOKE ALL ON TABLE public.user_groups FROM biomapsadmin;
GRANT ALL ON TABLE public.user_groups TO biomapsadmin;


--
-- Name: TABLE users; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON TABLE public.users FROM PUBLIC;
REVOKE ALL ON TABLE public.users FROM biomapsadmin;
GRANT ALL ON TABLE public.users TO biomapsadmin;


--
-- PostgreSQL database dump complete
--

