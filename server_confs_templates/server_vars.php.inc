<?php
# Server app's global variables
#
# Server app's default language
define('LANG','hu');

# Institution running the server.
$institute_short_name = "OpenBioMaps Gekko";
$institute_logo = "Juvenile-leopard-gecko.jpg";

# Access port
$port = isset($_SERVER['HTTP_X_FORWARDED_PORT']) ? $_SERVER['HTTP_X_FORWARDED_PORT'] : $_SERVER['SERVER_PORT'];
$host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];
if ($port != 80 and $port != 443)
    $HOST = $host.":".$port.'/biomaps';
else
    $HOST = $host.'/biomaps';

?>
