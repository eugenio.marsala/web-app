<?php
# obm app settings
define("OB_DOMAIN","localhost/biomaps");
define("OB_TMP","/var/lib/openbiomaps/tmp/");
define("OB_ROOT","/var/www/html/biomaps/root-site/");
define("OB_ROOT_SITE","/var/www/html/biomaps/root-site/");
define("OB_RESOURCES","/var/www/html/biomaps/resources/");
#define("OB_SYSDIR","/mnt/data/"); if it is in a non-default place; default is /var/lib/openbiomaps/maps/

# Available LANGUAGES
define('LANGUAGES','en,hu,ro');

# Biomaps db access
define("POSTGRES_PORT","5432");
define("GISDB_HOST",'gisdata');
define("MAPSERVER_HOST",'mapserver');
define('biomapsdb_user','biomapsadmin');
define('biomapsdb_pass','abcd1234');
define('biomapsdb_name','biomaps');
define('biomapsdb_host','biomaps'); // instead of localhost using docker style standalone biomaps host
define('mainpage_user','mainpage_admin');
define('mainpage_pass','NoosheiXej3paepe');

# PostGIS version
define('POSTGIS_V','2.1');

# sendmail  or smtp
# If smtp, use SMTP_ variables in local_vars.php.inc files to enable per project smtp authentication
define('SENDMAIL','smtp');

# obm cache method
define('CACHE','memcache');

# R-Shiny Server ports
define('RSERVER_PORT_sablon',7982);
?>
