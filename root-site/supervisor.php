<?php
$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
require_once('server_vars.php.inc');

?>

<!DOCTYPE html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="Pragma" content="no-cache">
    <title>openbiomaps.org - supervisor</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <!--<link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo $HOST ?>/js/ui/jquery-ui.css" type="text/css" />-->
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo  $HOST ?>/css/pure-min.css" type="text/css" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/jquery.min.js"></script>
    <!--<script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/ui/jquery-ui.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo $HOST ?>/js/datetimepicker/jquery.datetimepicker.css"/ >
    <script src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
    <script>
      $( function() {
        jQuery('#day').datetimepicker({ timepicker:false,format:'Y/m/d'});
        jQuery('#from').datetimepicker({ datepicker:false,format:'H:i'});
        //$( "#day" ).datepicker( {"showAnim": "clip","dateFormat":"yy/mm/dd" });
      } );
    </script>
    <style type="text/css">
        body {
            margin-left:30px;
        }
        h1,h2 {
            margin-left:-30px !important;
        }
       .num {
            float: left;
            color: gray;
            font-size: 13px;   
            font-family: monospace;
            text-align: right;
            margin-right: 6pt;
            padding-right: 6pt;
            border-right: 1px solid gray;}
        code {
            white-space:nowrap;
        }
        .diff td {
            vertical-align : top;
            white-space    : pre;
            white-space    : pre-wrap;
            font-family    : monospace;
        }
        .diffDeleted {
            border: 1px solid rgb(255,192,192);
            background: rgb(255,224,224);
        }
        .diffInserted {
            border: 1px solid rgb(192,255,192);
            background: rgb(224,255,224);
        }
        .tooltip {
            position: relative;
            display: inline-block;
        }
        .tooltip .tooltiptext {
            visibility: hidden;
            width: 520px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -260px;
            opacity: 0;
            transition: opacity 0.3s;
            font-size:85%;
        }
        .tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }
        .tooltip:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }
    </style>
</head>
<body style='padding:10px'>

<?php
# supervisor
# 
#
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once('./common_functions.php');


if (!function_exists('error_clear_last')) {
    function error_clear_last() {
        set_error_handler('var_dump', 0);
        @$undef_var;
        restore_error_handler();
    }
}

function recurse_copy($src,$dst) {
    $done = true;
    $dir = opendir($src); 
    @mkdir($dst); 

    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                $done = recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                #if (!$done)
                #    echo "<span style='background-color:orange'>Copy $file failed.</span> ";
            } 
            else { 
                if (file_exists($src . '/' . $file)) {
                    $done = copy($src . '/' . $file,$dst . '/' . $file); 
                    if (!$done)
                        echo "<span style='background-color:orange'>Copy $dst/$file failed.</span> ";
                }
            } 
        } 
    } 
    closedir($dir);

    if ($done)
        return true;
    else
        return false;
}
function recurse_unlink($src) { 
    
    if (basename($src) == 'private') {
        echo "<span style='background-color:orange'>It contains a private subdirecory.</span> ";
        return false;
    }
    
    $dir = opendir($src);
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                $done = recurse_unlink($src . '/' . $file); 
                #if (!$done)
                #    echo "<span style='background-color:orange'>Delete directory $src/$file failed.</span> ";
            } 
            else { 
                $done = unlink($src . '/' . $file); 
                #if (!$done)
                #    echo "<span style='background-color:orange'>Unlink $src/$file failed.</span> ";
            } 
        } 
    } 
    closedir($dir); 
    $done = true;
    if (is_file($src))
        $done = unlink($src); 
    elseif(is_dir($src))
        $done = rmdir($src);


    if (!$done) {
        echo "<span style='background-color:red'>Unlink $src failed.</span> ";
        return false;
    } else
        return true;
}
function preview_file($file)
{
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    if (in_array($ext,array('map','php','js','txt','inc','css','json'))) {
        $lines = implode(range(1, count(file($file))), '<br />');
        $content = highlight_file($file, true);    
        echo "<table><tr><td class=\"num\" style='font-size:11px'>\n$lines\n</td><td><div style='font-size:11px;'>\n$content\n</td></tr></table>";
    }
    elseif (in_array($ext,array('png','jpg'))) {
        $imageData = base64_encode(file_get_contents($file));
        echo '<img src="data:image/jpeg;base64,'.$imageData.'">';
    } else {
        echo "No preview available<br>";
    }
}


if (!$BID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

#$warned_files = array('mainpage_sidebar1.php','mainpage_sidebar2.php','mainpage_sidebar3.php','mainpage_content1.php','mainpage_content2.php','mainpage_footer.php','mainpage_header.php');
$protected_files = array('private.map','public.map','local_vars.php.inc');
$excluded_local_dirs = array('/attached_files','/includes/private','/includes/modules/private','/uploads');
$protected_dirs = array('','/private','/public','/includes','/includes/modules','/includes/modules/private','/shiny','/attached_files','/templates','/uploads','/libs'); // protected against replace dir

if (isset($_POST['submitSchedule'])) {

    require_once('/etc/openbiomaps/system_vars.php.inc');
    $mfile = OB_ROOT.'projects/maintenance.conf';

    if (isset($_POST['m_project']))
        $mfile = OB_ROOT.'projects/'.$_POST['m_project'].'/maintenance.conf';

    $h = fopen($mfile,"w");
    if ($h) {
        fwrite($h,"# maintenance scheduler\n\n");
        fwrite($h,"## maintenance address\n");
        fwrite($h,$_POST['m_ip']."\n\n");
        fwrite($h,"## start day\n");
        fwrite($h,$_POST['m_day']."\n\n");
        fwrite($h,"## from\n");
        fwrite($h,$_POST['m_from']."\n\n");
        fwrite($h,"## length\n");
        fwrite($h,$_POST['m_length']." ".$_POST['m_length-unit']."\n");
        fclose($h);
    }
}

if (isset($_GET['translation_update'])) {
    $t_url = 'https://raw.githubusercontent.com/OpenBioMaps/translations/master/global_project_translations.csv';
    $data = file_get_contents($t_url);
    $rows = explode("\n",$data);
    $cmd = "BEGIN;DELETE FROM translations WHERE scope='global'";
    pg_query($BID,$cmd);
    $cmd = "UPDATE translations SET id=1000000+nextval('translations_id_seq');ALTER SEQUENCE translations_id_seq RESTART;UPDATE translations SET id = DEFAULT;";
    pg_query($BID,$cmd);

    $ee = "";
    $lines = 0;
    foreach($rows as $row) {
        $s = str_getcsv($row);
        if ($s[0]=='global' and preg_match('/^str_/',$s[2])) {
            if ($s[3]=='')
                $cmd = sprintf('INSERT INTO translations ("scope","lang","const","translation") VALUES (%s,%s,%s,\'\')',quote($s[0]),quote($s[1]),quote($s[2]));
            else
                $cmd = sprintf('INSERT INTO translations ("scope","lang","const","translation") VALUES (%s,%s,%s,%s)',quote($s[0]),quote($s[1]),quote($s[2]),quote($s[3]));

            $res = pg_query($BID,$cmd);
            $e = pg_last_error($BID);
            if ($e) {
                $ee .= 1;
            } else {
                $lines++;
            }
        }
    }
    if (!$ee and $lines) {
        pg_query($BID,"COMMIT");
        echo "<div style='padding:10px;background-color:green;color:white'>Translations updated.</div>";
    } else {
        pg_query($BID,"ROLLBACK");
        echo "<div style='padding:10px;background-color:orange;color:white'>Translations update failed.</div>";
    }
}

if (isset($_POST['RUNSQL']) and isset($_POST['SQLC']) and !isset($_POST['saveSQL'])) {
    $sql_done = array();

    $sqlid = $_POST['SQLC'];
    $pdir = "";
    
    if (isset($_POST['DATABASE']) and $_POST['DATABASE']=='biomaps') 
        $con = $BID;
    elseif (isset($_POST['DATABASE']) and $_POST['DATABASE']=='gisdata') { 

        if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
            echo "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to GIS database with biomaps.</div>";

        $con = $GID;
    }
    elseif (isset($_POST['DATABASE']) and $_POST['DATABASE']=='project') { 


        if (isset($_POST['PROJECT']) and $_POST['PROJECT']!='') {
            if ($_POST['PROJECT']=='*') 
                $pdir = $_POST['project'];
            else
                $pdir = $_POST['PROJECT'];
            include_once(OB_ROOT.'projects/'.$pdir.'/local_vars.php.inc');
            #if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
            #    echo "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to GIS database with gisdata user.</div>";
            #$con = $ID;

            # it is better to use superuser...
            if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
                echo "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to GIS database with biomaps.</div>";
            $con = $GID;
        } 
    }

    $res = pg_query($con,$_POST['RUNSQL']);
    $e = pg_last_error($con);
    if ($e) {
        echo "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
        $status = 'false'; 
    } elseif ($e===false) {
        echo "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
        $status = 'false'; 
    } else {
        $status = 'true';
        echo "<div style='padding:10px;background-color:green;color:white'>sql done.</div>";
    }

    $cmd = sprintf("INSERT INTO supervisor (status,datum,database,revision,sqlcmd,sqlid,project) VALUES ('%s',now(),%s,%s,%s,%d,%s)",$status,quote($_POST['DATABASE']),quote(trim($_POST['REVISION'])),quote(trim($_POST['RUNSQL'])),$sqlid,quote($pdir));
    $execute_status = pg_query($BID,$cmd);
    $e = pg_last_error($BID);

    if ($e) {
        echo "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    }

}
elseif (isset($_POST['RUNSQL']) and isset($_POST['SQLC']) and isset($_POST['saveSQL'])) {
    $sqlid = $_POST['SQLC'];
    $pdir = "";

    if (isset($_POST['DATABASE']) and $_POST['DATABASE']=='project') {
        if (isset($_POST['PROJECT']) and $_POST['PROJECT']!='') {
            if ($_POST['PROJECT']=='*') 
                $pdir = $_POST['project'];
            else
                $pdir = $_POST['PROJECT'];
        }
    }

    if ($pdir!='')
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",quote($_POST['DATABASE']),quote(trim($_POST['REVISION'])),$sqlid,quote($pdir));
    else
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",quote($_POST['DATABASE']),quote(trim($_POST['REVISION'])),$sqlid);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        if ($pdir!='')
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",quote($_POST['DATABASE']),quote(trim($_POST['REVISION'])),$sqlid,quote($pdir));
        else
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",quote($_POST['DATABASE']),quote(trim($_POST['REVISION'])),$sqlid,quote($pdir));
    } else {
        $cmd = sprintf("INSERT INTO supervisor (status,datum,database,revision,sqlcmd,sqlid,project) VALUES ('%s',now(),%s,%s,%s,%d,%s)",'true',quote($_POST['DATABASE']),quote(trim($_POST['REVISION'])),quote(trim($_POST['RUNSQL'])),$sqlid,quote($pdir));
    }

    $mark_as_read_status = pg_query($BID,$cmd);
    $e = pg_last_error($BID);

    if ($e) {
        echo "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    } else {
        echo "<div style='padding:10px;background-color:green;color:white'>done</div>";
    
    }

}

if (isset($_GET['show_diff'])) {

    chdir(OB_ROOT.'projects');
    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> <a href='projects/{$_GET['project']}'>visit {$_GET['project']}</a></h2>";

    $ts = array();
    foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
        $style = '';
        if ($dirname == $_GET['project'])
            $style = 'font-weight:bold';
        $ts[] = "<a class='pure-button' href='?project=$dirname' style='$style'>$dirname</a>";
    }
    echo implode(" ",$ts);
    echo "<hr>";


    //chdir(OB_ROOT.'libs');

   if (!isset($_GET['directory'])) {
        $_GET['directory'] = '';
    }

    $dirs =  array(
        'includes'=>'libs',
    );

    $chunks = explode('/', $_GET['directory']);
    $s_directory = array(OB_RESOURCES);
    $p_directory = array(OB_ROOT.'projects/'.$_GET['project']);
    $up = "";

    if (count($chunks)<=1 and preg_match('/^\/*$/',$chunks[0]))
        $s_directory[] = 'pages';

    //$chunks[0] = preg_replace('/^\/*/','/',$chunks[0]);

    foreach ($chunks as $i => $chunk) {
        if ($chunk=='') continue;
        if ($chunk == '..') {
            $up = 1;
            continue;
        }

        if (isset($dirs[$chunk])) {
            $s_directory[] = $dirs[$chunk];
            $p_directory[] = $chunk;
        } else {
            $s_directory[] = $chunk;
            $p_directory[] = $chunk;
        }
    }
    if ($up) {
        if (count($s_directory) > 1) {
            array_pop($s_directory);
            array_pop($p_directory);
        }
    }
    $s_directory = implode('/',$s_directory);
    $p_directory = implode('/',$p_directory);
    
    $chdir_to_p_dir = chdir($p_directory);

    // http://code.iamkate.com/php/diff-implementation/
    require_once OB_RESOURCES.'libs/class.Diff.php';

    echo "<div style='background-color:lightgray;font-family:monospace'>".$s_directory.'/'.$_GET['show_diff']." &nbsp; ";
    echo $p_directory.'/'.$_GET['show_diff']."</div>";
    // compare two files line by line
    echo Diff::toTable(Diff::compareFiles($s_directory.'/'.$_GET['show_diff'], $p_directory.'/'.$_GET['show_diff']));


} elseif (isset($_GET['check_file'])) {

    chdir(OB_ROOT.'projects');
    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> <a href='projects/{$_GET['project']}'>visit {$_GET['project']}</a></h2>";

    $ts = array();
    foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
        $style = '';
        if ($dirname == $_GET['project'])
            $style = 'font-weight:bold';
        $ts[] = "<a class='pure-button' href='?project=$dirname' style='$style'>$dirname</a>";
    }
    echo implode(" ",$ts);
    echo "<hr>";


    //chdir(OB_ROOT.'libs');

   if (!isset($_GET['directory'])) {
        $_GET['directory'] = '';
    }

    $dirs =  array(
        'includes'=>'libs',
    );

    $chunks = explode('/', $_GET['directory']);
    $s_directory = array(OB_RESOURCES);
    $p_directory = array(OB_ROOT.'projects/'.$_GET['project']);
    $up = "";

    if (count($chunks)<=1 and preg_match('/^\/*$/',$chunks[0]))
        $s_directory[] = 'pages';


    //$chunks[0] = preg_replace('/^\/*/','/',$chunks[0]);

    foreach ($chunks as $i => $chunk) {
        if ($chunk=='') continue;
        if ($chunk == '..') {
            $up = 1;
            continue;
        }

        if (isset($dirs[$chunk])) {
            $s_directory[] = $dirs[$chunk];
            $p_directory[] = $chunk;
        } else {
            $s_directory[] = $chunk;
            $p_directory[] = $chunk;
        }
    }
    if ($up) {
        if (count($s_directory) > 1) {
            array_pop($s_directory);
            array_pop($p_directory);
        }
    }
    $s_directory = implode('/',$s_directory);
    $p_directory = implode('/',$p_directory);
    
    $chdir_to_p_dir = chdir($p_directory);


    echo "<div style='background-color:lightgray;font-family:monospace'>".$p_directory.'/'.$_GET['check_file']."</div>";
    preview_file($p_directory.'/'.$_GET['check_file']);
}
elseif (isset($_GET['VIEWSQL']) and isset($_GET['SQLC'])) {
    $sql_done = array();

    $sqlid = $_GET['SQLC'];

    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a><hr></h2>";
    if (isset($_GET['project'])) {

        chdir(OB_ROOT.'projects');
        $ts = array();
        foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
            $style = '';
            if ($dirname == $_GET['project'])
                $style = 'font-weight:bold';
            $ts[] = "<a class='pure-button' href='?project=$dirname' style='$style'>$dirname</a>";
        }
        echo implode(" ",$ts);
        echo "<hr>";
    }
    echo "<script>
    function getContent(){
        document.getElementById('RUNSQL').value = document.getElementById('HighLightSQL').innerText;
    }
    </script>";

    echo "<form method='post' class='pure-form pure-form-aligned' onsubmit='return getContent()'>";
    echo " <fieldset>
        <legend>SQL edit/execute form</legend>";
    echo "<div class='pure-control-group'>";
        echo "<label for='REVISION'>Revision</label>";
        echo "<input id='REVISION' name='REVISION' readonly value='".$_GET['REVISION']."'>";
    echo "</div>";
    echo "<div class='pure-control-group'>";
        echo "<label for='DATABASE'>Database</label>";
        echo "<input id='DATABASE' name='DATABASE' readonly value='".$_GET['DATABASE']."'>";
    echo "</div>";
    echo "<div class='pure-control-group'>";
        echo "<label for='SQLC'>Command ID</label>";
        echo "<input id='SQLC' name='SQLC' readonly value='".$_GET['SQLC']."'>";
    echo "</div>";
    echo "<div class='pure-control-group'>";
        echo "<label for='PROJECT'>Project</label>";
        echo "<input id='PROJECT' name='PROJECT' readonly value='".$_GET['PROJECT']."'>";
    echo "</div>";
    if (isset($_GET['project'])) {
        echo "<div class='pure-control-group'>";
            echo "<label for='project'></label>";
            echo "<input id='project' name='project' readonly value='".$_GET['project']."'>";
        echo "</div>";
    }

    require_once(OB_RESOURCES.'/libs/vendor/autoload.php');
    $highlighted_sql = SqlFormatter::format(base64_decode($_GET['VIEWSQL']));

    echo "<div class='pure-control-group'>";
        echo "<label for='RUNSQL'>SQL command</label>";
        echo "<textarea id='RUNSQL' name='RUNSQL' style='display:none'>".base64_decode($_GET['VIEWSQL'])."</textarea>";
        echo "<div id='HighLightSQL' class='pure-u-1-2' contenteditable='TRUE' style='border:1px dotted #999'>".$highlighted_sql."</div>";
    echo "</div>";
    echo "<div class='pure-controls'>";
        echo "<button type='submit' name='submitSQL' class='pure-button pure-button-primary'>execute</button> ";
        echo "<button type='submit' name='saveSQL' class='pure-button'>mark as ready</button>";
    echo "</div>";
    echo "</fieldset></form>";
}
elseif (!isset($_GET['project'])) {
    #               #
    #   root page   #
    #               #

    echo "<h1 style='background-color:#0078e7;color:white;padding:15px;margin:0'> Supervisor </h1>";
    echo "<h2>Projects<hr></h2>";

    chdir(OB_ROOT.'projects');
    echo "<ul>";
    foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
        echo "<li><a href='?project=$dirname'>$dirname</a></li>";
    }
    echo "</ul>";

    # Traslations
    echo "<br><h2>Global Translation updates<hr></h2>";
    echo "<a href='?translation_update' class='pure-button pure-button-primary'>update global translations from GitHub</a><br>";
    

    # Maintenance
    echo "<br><h2>Global maintenance schedule<hr></h2>";

    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>0);
    $address = $_SERVER['REMOTE_ADDR'];
    $schedule_day = "";
    $from = "";
    $length = "";
    $mconf = 'maintenance.conf';

    if (file_exists($mconf)) {
        $contents = file($mconf); 
        
        foreach ($contents as $line) {

            if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $schedule_day = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $from = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $length = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $address = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            
            if (preg_match('/^## start day/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['day'] = 1;
            }
            elseif (preg_match('/^## from/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['from'] = 1;
            }
            elseif (preg_match('/^## length/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['length'] = 1;
            }
            elseif (preg_match('/^## maintenance address/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['address'] = 1;
            }
        }
    }

    echo "<form method='post' class='pure-form'>";
    echo " <fieldset>
        <legend>Set maintenance time window</legend>";
        echo " <input id='ip' name='m_ip' placeholder=\"maintener's IP address\" value='$address'>";
        echo " <input id='day' name='m_day' placeholder='day' value='$schedule_day'>";
        echo " <input id='from' name='m_from' placeholder='time start' value='$from'>";
        $int = array(1,2,3,4,5,6,7,8,9,10,11,12,15,24,30,36,45,48);
        $time_options = "";
        $m = array();
        preg_match("/^(\d+)(\s+)?(\w+)$/",$length,$m);
        foreach($int as $i) {
            if (isset($m[1]) and $i == $m[1])
                $time_options .= "<option selected>$i</option>";
            else
                $time_options .= "<option>$i</option>";

        }
        echo " <select id='length' name='m_length' placeholder='length'>$time_options</select>";
        $min_sel = "";
        if (isset($m[3]) and $m[3]=='minutes') $min_sel='selected';
        echo " <select id='length-unit' name='m_length-unit' placeholder='length-unit'><option>hours</option><option $min_sel>minutes</option></select>";

        echo " <button type='submit' name='submitSchedule' class='pure-button pure-button-primary'>set</button>";
    echo "</fieldset></form>";

    # SQL updates
    echo "<br><h2>Global SQL updates<hr></h2>";
    
    chdir(OB_ROOT_SITE);

    #include('Parsedown.php');
    #$Parsedown = new Parsedown();
    #$contents = file_get_contents('supervisor.md'); 
    #echo $Parsedown->text($contents);
    $contents = file('supervisor.md'); 
    $sql_cmds = array('revisions'=>array());
    $revision = '';
    $database = '';
    $projects = '';
    foreach ($contents as $line) {
        $m = array();
        if (preg_match('/^# (.+)/',$line,$m)) {
            $counter = '';
            $revision = $m[1];
            if (!isset($sql_cmds['revisions'][$revision]['databases']))
                $sql_cmds['revisions'][$revision] = array('databases'=>array());
            
        } elseif (preg_match('/^## (.+)/',$line,$m)) {
            $counter = '';
            if (preg_match('/^database: (.+)/',$m[1],$mm)) { 
                $database = $mm[1];
                if (!isset( $sql_cmds['revisions'][$revision]['databases'][$database]['projects']))
                    $sql_cmds['revisions'][$revision]['databases'][$database] = array('projects'=>array());
            }

        } elseif (preg_match('/^### (.+)/',$line,$m)) {
            $counter = '';
            $projects = '';
            if (preg_match('/^project: (.+)/',$m[1],$mm)) { 
                $projects = $mm[1];
            }
            if (!isset($sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds']))
                $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects] = array('cmds'=>array());
            if ($projects == '') {
                $counter = $m[1];
                $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds'][$counter] = "";
            }

        } elseif (preg_match('/^#### (.+)/',$line,$m)) {
            $counter = $m[1];
            $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds'][$counter] = "";
        } elseif ($counter!='' and !preg_match('/^#/',$line)) {

            $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds'][$counter] .= " ".$line;
        }
    }
    //echo json_encode($sql_cmds);
    foreach ($sql_cmds as $key=>$value) {
        if ($key == 'revisions') {
            foreach($value as $rev_key => $rev_value) {
                $revision_list = "";
                $revision_list_active_element = 0;
                $revision_list .= "<h3>$rev_key</h3>";
                foreach($rev_value as $databases) {
                    //databases
                    foreach($databases as $db_key => $db_value) {
                        $revision_list .= "<h4>$db_key</h4>";
                        foreach($db_value as $projects) {
                            //projects
                            foreach($projects as $project_key => $project_value) {
                                if ($project_key!='') continue;
                                $revision_list .= "<h5>$project_key</h5>";
                                foreach($project_value as $sql_key => $sql_value) {

                                    $revision_list .= "<ul style='list-style-type:none;font-size:80%;line-height:200%'>";
                                    foreach ($sql_value as $sqlline_key=>$sqlline_val) {
                                        $cmd = sprintf("SELECT status FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%s",quote(trim($db_key)),quote(trim($rev_key)),quote($sqlline_key));
                                        $res = pg_query($BID,$cmd);
                                        if (pg_num_rows($res)) {
                                            $row = pg_fetch_assoc($res);
                                            if ($row['status']=='f') {
                                                $revision_list .= sprintf("<li><span style='color:red'>Failed:</span> <a href='?REVISION=$rev_key&PROJECT=$project_key&DATABASE=$db_key&SQLC=$sqlline_key&VIEWSQL=%s'>$sqlline_val</a></li>",base64_encode($sqlline_val));
                                                $revision_list_active_element++;
                                            }
                                        } else {
                                            $revision_list .= sprintf("<li>$sqlline_key. <a href='?REVISION=$rev_key&PROJECT=$project_key&DATABASE=$db_key&SQLC=$sqlline_key&VIEWSQL=%s'>$sqlline_val</a></li>",base64_encode($sqlline_val));
                                            $revision_list_active_element++;
                                        }
                                    }
                                    $revision_list .= "</ul>";
                                }
                            }
                        }
                    }
                }
                if (!$revision_list_active_element)
                    $revision_list = "";
                else
                    echo $revision_list;
                $revision_list_active_element = 0;

            }
        }
    }

}
elseif (isset($_GET['project'])) {
    #                   #
    #    Project page   #
    #                   #

    chdir(OB_ROOT.'projects');


    touch($_GET['project'].'/supervisor_time_log_file');
        
    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>0);
    $address = $_SERVER['REMOTE_ADDR'];
    $schedule_day = "";
    $from = "";
    $length = "";

    $mconf = $_GET['project']."/maintenance.conf";
    
    if (file_exists($mconf)) {
        $contents = file($mconf);
        foreach ($contents as $line) {

            if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $schedule_day = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $from = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $length = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $address = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            
            if (preg_match('/^## start day/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['day'] = 1;
            }
            elseif (preg_match('/^## from/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['from'] = 1;
            }
            elseif (preg_match('/^## length/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['length'] = 1;
            }
            elseif (preg_match('/^## maintenance address/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['address'] = 1;
            }
        }
    }

    $mset  = "<form method='post' class='pure-form'><input type='hidden' name='m_project' value='{$_GET['project']}'>";

    $mset .= " <fieldset>
        <legend>Set maintenance time window</legend>";
        $mset .= " <input id='ip' name='m_ip' placeholder=\"maintener's IP address\" value='$address'>";
        $mset .= " <input id='day' name='m_day' placeholder='day' value='$schedule_day'>";
        $mset .= " <input id='from' name='m_from' placeholder='time start' value='$from'>";
        $int = array(1,2,3,4,5,6,7,8,9,10,11,12,15,24,30,36,45,48);
        $time_options = "";
        $m = array();
        preg_match("/^(\d+)(\s+)?(\w+)$/",$length,$m);
        foreach($int as $i) {
            if (isset($m[1]) and $i == $m[1])
                $time_options .= "<option selected>$i</option>";
            else
                $time_options .= "<option>$i</option>";

        }
        $mset .= " <select id='length' name='m_length' placeholder='length'>$time_options</select>";
        $min_sel = "";
        if (isset($m[3]) and $m[3]=='minutes') $min_sel='selected';
        $mset .= " <select id='length-unit' name='m_length-unit' placeholder='length-unit'><option>hours</option><option $min_sel>minutes</option></select>";

        $mset .= " <button type='submit' name='submitSchedule' class='pure-button pure-button-primary'>set</button>";
    $mset .= "</fieldset></form>";


    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> <a href='projects/{$_GET['project']}'>visit {$_GET['project']}</a></h2><div style='margin-left:-30px'>";

    $ts = array();
    foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
        $style = '';
        if ($dirname == $_GET['project'])
            $style = 'font-weight:bold';
        $ts[] = "<a class='pure-button' href='?project=$dirname' style='$style'>$dirname</a>";
    }
    echo implode(" ",$ts);
    echo "<hr></div>";


    //chdir(OB_ROOT.'libs');
    
    // default direcotry
    if (!isset($_GET['directory'])) {
        $_GET['directory'] = '';
    }

    $dirs =  array(
        'includes'=>'libs',
    );

    $chunks = explode('/', $_GET['directory']);
    $s_directory = array(OB_RESOURCES);
    $p_directory = array(OB_ROOT.'projects/'.$_GET['project']);
    $up = "";

    //if (count($chunks)<=1 and preg_match('/^\/*$/',$chunks[0])) 
    //    $s_directory[] = 'pages';

    //$chunks[0] = preg_replace('/^\/*/','/',$chunks[0]);

    foreach ($chunks as $i => $chunk) {
        if ($chunk=='') continue;
        if ($chunk == '..') {
            $up = 1;
            continue;
        }

        if (isset($dirs[$chunk])) {
            $s_directory[] = $dirs[$chunk];
            $p_directory[] = $chunk;
        } else {
            $s_directory[] = $chunk;
            $p_directory[] = $chunk;
        }
    }
    if ($up) {
        if (count($s_directory) > 1) {
            array_pop($s_directory);
            array_pop($p_directory);
        }
    }
    $s_directory = implode('/',$s_directory);
    $p_directory = implode('/',$p_directory);
    
    $chdir_to_p_dir = false;
    if (file_exists($p_directory) and is_dir($p_directory))
        $chdir_to_p_dir = chdir($p_directory);

    $updir = explode('/',$_GET['directory']);
    array_pop($updir);
    $updir = implode('/',$updir);
    
    $ts_reg = array();
    $ts = array("<a class='pure-button' href='?project={$_GET['project']}&directory={$_GET['directory']}'>.</a>","<a class='pure-button' href='?project={$_GET['project']}&directory=$updir'>..</a>");
    //foreach (glob("*",GLOB_ONLYDIR ) as $dirname) {
    if ($chdir_to_p_dir) {
        foreach (glob("*" ) as $dirname) {
            $ok = 0;
            if (is_link($dirname)) $ok = 1;
            elseif (is_dir($dirname)) $ok = 1;
            
            if (!$ok) continue;

            $style = '';
            if (isset($_GET['directory']) and $dirname == $_GET['directory'])
                $style = 'font-weight:bold';
            if (!file_exists($dirname))
                $style = "background-color:red";
            
            if (!in_array($_GET['directory']."/$dirname",$excluded_local_dirs)) {
                $c_directory = preg_replace('/\/pages/','',$s_directory);
                $odirname = $dirname;

                if (OB_RESOURCES == $c_directory and $dirname == 'includes')
                    $odirname = 'libs';

                if (!is_dir("$c_directory/$odirname"))
                    $ts[] = $dirname;
                else {
                    $output = shell_exec( "diff -q $c_directory/$odirname $p_directory/$dirname" );
                    $diff = 0;
                    $expl = array();
                    foreach(preg_split("/((\r?\n)|(\r\n?))/", $output) as $line){
                         if (preg_match('/^Only in/',$line)) {
                             if (preg_match('/examples$/',$line)) continue;
                             elseif (preg_match('/local_main_javascripts.php$/',$line)) continue;
                             elseif (preg_match('/\.swp$/',$line)) continue;
                             elseif (preg_match('/\.swo$/',$line)) continue;
                             elseif (preg_match('/private$/',$line)) continue;
                             elseif ($dirname=='private' and preg_match('/cache.php$/',$line)) continue;
                             elseif ($dirname=='private' and preg_match('/proxy.php$/',$line)) continue;
                             elseif ($dirname=='public' and preg_match('/cache.php$/',$line)) continue;
                             elseif ($dirname=='public' and preg_match('/proxy.php$/',$line)) continue;
                             $diff++;
                             $expl[] = $line;
                         }
                         elseif (preg_match('/^Files /',$line)) {
                             if (preg_match('/(public|private)\.map differ$/',$line)) continue;
                             elseif (preg_match('/\.swp differ$/',$line)) continue;
                             elseif (preg_match('/\.swo differ$/',$line)) continue;
                             elseif (preg_match('/r-server\.log differ$/',$line)) continue;
                             elseif (preg_match('/\.htaccess differ$/',$line)) continue;
                             elseif (preg_match('/mainpage_grid.php differ$/',$line)) continue;
                             elseif (preg_match('/mainpage_sidebar.\.php differ$/',$line)) continue;
                             elseif (preg_match('/mainpage_content.\.php differ$/',$line)) continue;
                             $diff++;
                             $expl[] = $line;
                         }
                    }
                    $ts_reg[] =  $dirname;
                    if ($diff) {
                        $output = implode("<br>", $expl);
                        $ts[] = "<div class='tooltip'><a class='pure-button' style='background-color:violet' href='?project={$_GET['project']}&directory={$_GET['directory']}/$dirname' style='$style'>$dirname</a><span class='tooltiptext'>$output</span></div>";
                    } else
                        $ts[] = "<a class='pure-button' href='?project={$_GET['project']}&directory={$_GET['directory']}/$dirname' style='$style'>$dirname</a>";
                }
            }
        }
    }

    // search for missing local directories

    $chdir_to_s_dir = chdir($s_directory);
    if ($chdir_to_s_dir) {

        foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
        //foreach (glob("*" ) as $dirname) {
            if (is_dir($dirname) and !in_array($dirname, $ts_reg) and !in_array("{$_GET['directory']}/$dirname",$protected_dirs))
                $ts[] = "<a class='pure-button' href='?project={$_GET['project']}&directory={$_GET['directory']}/$dirname&replace_directory' style='background-color:aliceblue'>$dirname</a>";
        }
    }

    $chdir_to_p_dir = false;
    if (file_exists($p_directory) and is_dir($p_directory))
        $chdir_to_p_dir = chdir($p_directory);

    if (isset($_GET['replace_broken_link'])) {
        if (is_link($p_directory) and !file_exists($p_directory)) {
            if (is_dir($s_directory)) {
                $done = unlink($p_directory) ? $done = recurse_copy($s_directory,$p_directory) : $done = false;
            }
            if (!$done)
                echo "<span style='background-color:orange'>Replace link failed!</span>";
        }
    } 
    elseif (isset($_GET['replace_directory']) and !in_array($_GET['directory'],$protected_dirs)) {
        if (is_dir($p_directory)) {
            //unlink($p_directory) ? mkdir($p_directory) : $done = false;
            $done = recurse_unlink($p_directory);
            $done = recurse_copy($s_directory,$p_directory);
            if (!$done)
                echo "<div style='background-color:orange'>Copy directory failed!</div>";
        } elseif (!is_file($p_directory)) {
            $done = recurse_copy($s_directory,$p_directory);
            if (!$done)
                echo "<div style='background-color:orange'>Copy directory failed!</div>";
            else
                echo "<div style='background-color:aliceblue'>Copy directory done!</div>";
        }
    } 


    $all_action = "";
    if (!is_dir($p_directory)) {
        if (is_link($p_directory) and !file_exists($p_directory))
            echo "<span style='color:red'>$p_directory is a broken link</span> <a href='?project={$_GET['project']}&directory={$_GET['directory']}&replace_broken_link'>[replace it]</a><br>";
        else {
            echo "$p_directory directory is not exists<br>";
        }
    } elseif (!is_writable($p_directory)) {
        echo "$p_directory directory is not writable<br>";
    } else
        $all_action = "[<a href='?project={$_GET['project']}&directory={$_GET['directory']}&update_all_files'>update all files</a>]";

    echo implode(" ",$ts);
    echo "<hr>";
    
    if (is_dir($s_directory)) {
        // remove this dangeorus link for those folders which countains private/custom files
        if (!in_array($_GET['directory'],$protected_dirs))
            echo "<i>$s_directory</i> [<a href='?project={$_GET['project']}&directory={$_GET['directory']}&replace_directory'>replace entire local directory</a>]<br>";
    }


    //debug
    //echo $s_directory;

    if (is_dir($s_directory) and chdir($s_directory)) {
        echo "<table class='pure-table'>";
        echo "<thead><tr><th>source file</th><th>project file</th><th>action $all_action</th></tr></thead></tbody>";

        //$dir = opendir($s_directory); 
        //while(false !== ( $filename = readdir($dir)) ) {
            //if (!is_file($filename) and !is_link($filename)) continue;
            //if (( $filename == '.' ) || ( $filename == '..' )) continue;

        // list files
        $files_array = glob("*.{map,inc,php,css,js,png,jpg,jpeg,svg,json}",GLOB_BRACE);
        usort($files_array, create_function('$b,$a', 'return filemtime($a) - filemtime($b);'));
        error_clear_last();
        foreach ($files_array as $filename) {
            if (in_array($filename,$protected_files)) {
                echo "<tr><td></td><td>$filename</td><td>protected file</td></tr>";
                continue;
            }
            echo "<tr>";
            echo "<td><a href='?project={$_GET['project']}&directory={$_GET['directory']}&check_file=$filename'>$filename</a> ";
            // stat 
            if (isset($_GET['stat']) and $_GET['stat']=='size')   
                echo "(" . filesize($filename) . ")";
            elseif (isset($_GET['stat']) and $_GET['stat']=='md5')   
                echo "(" . md5_file($filename) . ")";
            
            echo "</td>";

            if (is_dir($p_directory) and is_writable($p_directory)) {

                $project_file = "$p_directory/$filename";
                $copy_done = 2;
                $errors = array();
                if (isset($_GET['update_file']) and $filename == $_GET['update_file'] and $_GET['action']=='replace') {
                    unlink($project_file) ? $copy_done = copy($filename,$project_file) : $copy_done = false;
                    
                } elseif (isset($_GET['update_file']) and $filename == $_GET['update_file'] and $_GET['action']=='copy') {
                    $copy_done = copy($filename,$project_file);
                
                } elseif (isset($_GET['update_all_files'])) {
                    if (is_link($project_file)) { 
                        unlink($project_file) ? $copy_done = copy($filename,$project_file) : $copy_done = false;
                        $errors = error_get_last();
                        if ($errors and count($errors) and $errors['message']!='Undefined variable: undef_var') {
                            echo "<br />COPY ERROR: ".$errors['type'];
                            echo "<br />".$errors['message'];
                        }
                    }
                    else {
                        $copy_done = copy($filename,$project_file);
                        $errors = error_get_last();
                        if ($errors and count($errors) and $errors['message']!='Undefined variable: undef_var') {
                            echo "<br />COPY ERROR: ".$errors['type'];
                            echo "<br />".$errors['message'];
                        }
                    }
                }
                error_clear_last();

                if (is_file($project_file)) {
                    // local file exists
                    if (is_link($project_file)) {
                        echo "<td><span style='color:violet'>IT IS A SYMLINK!</span></td>";
                        if ($copy_done == 2)
                            echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=replace&directory={$_GET['directory']}'>replace file</a>]</td>";
                        elseif ($copy_done===false)
                            echo "<td><span style='color:red'>replace symlink failed</span></td>";
                        else
                            echo "<td>replace done</td>";
                        
                    } else {
                        if (md5_file($filename) != md5_file($project_file)) {
                            echo "<td><span style='color:orange'>The files are <a href='?project={$_GET['project']}&show_diff=$filename&directory={$_GET['directory']}'>DIFFERENT</a>!</span></td>";
                            if (!is_writable($project_file))
                                echo "<td>file is not writable</td>"; 
                            else
                                echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=copy&directory={$_GET['directory']}'>replace file</a>]</td>";

                        } else {
                            if ($copy_done===false)
                                echo "<td><span style='color:red'>action failed</span></td><td></td>";
                            else
                                echo "<td><span style='color:green'>up to date</span></td><td></td>";
                        }
                    }
                } else {
                    // broken link
                    if (is_link($project_file) ) {
                        echo "<td><span style='color:violet'>IT IS A BROKEN SYMLINK!</span></td>";

                        if ($copy_done===false)
                            echo "<td><span style='color:red'>copy failed</span></td>";
                        elseif ($copy_done == 2) {
                            if (is_writable($project_file))
                                echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=copy&directory={$_GET['directory']}'>copy file</a>]</td>";
                            else
                                echo "<td>not writable</td>";
                        }
                        else
                            echo "<td><span style='color:green'>copy done</span></td>";
                    } else {
                        //missing files
                        echo "<td><span style='color:violet'>Local file is missing!</span></td>";
                        if ($copy_done===false)
                            echo "<td><span style='color:red'>copy failed</span></td>";
                        elseif ($copy_done == 2) {
                            echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=copy&directory={$_GET['directory']}'>copy file</a>]</td>";
                        }
                        else
                            echo "<td><span style='color:green'>copy done</span></td>";

                    }
                }
            } else {
                echo "<td>$p_directory not writable</td><td></td>";
            
            }
            echo "</tr>";
        }
        echo "</tbody></table>";
    } else {
        echo "$s_directory directory does not exists or not readable.";
    }

    echo "<h2><br>Local maintenance schedule<hr></h2>";
    echo $mset;

    echo "<h2><br>Local SQL updates<hr></h2>";
    chdir(OB_ROOT_SITE);

    #include('Parsedown.php');
    #$Parsedown = new Parsedown();
    #$contents = file_get_contents('supervisor.md'); 
    #echo $Parsedown->text($contents);
    $contents = file('supervisor.md'); 
    $sql_cmds = array('revisions'=>array());
    $revision = '';
    $database = '';
    $projects = '';
    foreach ($contents as $line) {
        $m = array();
        if (preg_match('/^# (.+)/',$line,$m)) {
            $counter = '';
            $revision = $m[1];
            if(!isset($sql_cmds['revisions'][$revision]['databases']))
                $sql_cmds['revisions'][$revision] = array('databases'=>array());
            
        } elseif (preg_match('/^## (.+)/',$line,$m)) {
            $counter = '';
            if (preg_match('/^database: (.+)/',$m[1],$mm)) { 
                $database = $mm[1];
                if(!isset($sql_cmds['revisions'][$revision]['databases'][$database]['projects']))
                    $sql_cmds['revisions'][$revision]['databases'][$database] = array('projects'=>array());
            }

        } elseif (preg_match('/^### (.+)/',$line,$m)) {
            $counter = '';
            $projects = '';
            if (preg_match('/^project: (.+)/',$m[1],$mm)) { 
                $projects = $mm[1];
            }
            if(!isset($sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds']))
                $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects] = array('cmds'=>array());
            if ($projects == '') {
                $counter = $m[1];
                $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds'][$counter] = "";
            }

        } elseif (preg_match('/^#### (.+)/',$line,$m)) {
            $counter = $m[1];
            $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds'][$counter] = "";
        } elseif ($counter!='' and !preg_match('/^#/',$line)) {
            $sql_cmds['revisions'][$revision]['databases'][$database]['projects'][$projects]['cmds'][$counter] .= $line;
        }
    }
    //echo json_encode($sql_cmds);
    foreach ($sql_cmds as $key=>$value) {
        if ($key == 'revisions') {
            foreach($value as $rev_key => $rev_value) {
                echo "<h3>$rev_key</h3>";
                foreach($rev_value as $databases) {
                    //databases
                    foreach($databases as $db_key => $db_value) {
                        echo "<h4>$db_key</h4>";
                        foreach($db_value as $projects) {
                            //projects
                            foreach($projects as $project_key => $project_value) {
                                if ($project_key=='') continue;
                                if ($project_key != '*') echo "<h5>$project_key</h5>";
                                foreach($project_value as $sql_key => $sql_value) {

                                    echo "<ul style='list-style-type:none;font-size:80%;line-height:200%'>";
                                    foreach ($sql_value as $sqlline_key=>$sqlline_val) {
                                        // project version of general command
                                        $proj_cmd = preg_replace('/%project%/',$_GET['project'],$sqlline_val);

                                        $cmd = sprintf("SELECT status FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%s AND project=%s",quote(trim($db_key)),quote(trim($rev_key)),quote($sqlline_key),quote($_GET['project']));
                                        $res = pg_query($BID,$cmd);
                                        if (pg_num_rows($res)) {
                                            $row = pg_fetch_assoc($res);
                                            if ($row['status']=='f')
                                                echo "<li><span style='color:red'>Failed:</span> <a href='?REVISION=$rev_key&project={$_GET['project']}&PROJECT=$project_key&DATABASE=$db_key&SQLC=$sqlline_key&VIEWSQL=".base64_encode($proj_cmd)."'>$proj_cmd</a></li>";
                                        } else
                                            echo "<li>$sqlline_key. <a href='?REVISION=$rev_key&project={$_GET['project']}&PROJECT=$project_key&DATABASE=$db_key&SQLC=$sqlline_key&VIEWSQL=".base64_encode($proj_cmd)."'>$proj_cmd</a></li>";

                                    }    
                                    echo "</ul>";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

/*
            $files = scandir(getenv("OB_LIB_DIR").'../templates/initial/images/');
            foreach($files as $f) {
                if (is_file(getenv("OB_LIB_DIR")."../templates/initial/images/$f")) {
                    copy(getenv("OB_LIB_DIR")."../templates/initial/images/$f","$new_project_dir/images/$f") ? $e+=1 : print "<span class='err'>$f err</span><br>";
                }
            }
 */
?>
</body>
</html>
