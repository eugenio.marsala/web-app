# DROP user_groups table
## database: biomaps
### 1
DROP TABLE IF EXISTS user_groups;

### 2
DROP TABLE IF EXISTS groups;


# Rename f_restrict_column
## database: biomaps
### 1
ALTER TABLE "header_names" RENAME COLUMN f_restrict_column TO f_use_rules;

# form publish
## database: biomaps
### 1
ALTER TABLE "public"."project_forms" ADD COLUMN "published" timestamp without time zone;

### 2
ALTER TABLE "public"."project_forms" ADD COLUMN "published_form_id" integer;

### 3
ALTER TABLE ONLY "public"."project_forms" DROP CONSTRAINT project_forms_tablename_key;

### 4
ALTER TABLE ONLY "public"."project_forms" ADD CONSTRAINT project_forms_ukey UNIQUE (project_table, form_name, published);

# uniq labels for fileds in each form
## database: biomaps
### 1
ALTER TABLE "project_forms_data" ADD COLUMN "column_label" character varying(255);

# replace str_load_imports to str_intr_uploads
## database: biomaps
### 1
UPDATE "public"."admin_pages" SET label='str_intr_uploads' WHERE label='str_load_imports';

# replace str_access to str_data_access
## database: biomaps
### 1
UPDATE "public"."admin_pages" SET label='str_data_access' WHERE label='str_access';

# Add foreign key for projects table
## database: biomaps
### 1
ALTER TABLE ONLY public.supervisor
    ADD CONSTRAINT supervisor_projects_project_table_fkey FOREIGN KEY (project) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

### 2
ALTER TABLE ONLY public.project_roles
    ADD CONSTRAINT project_roles_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

### 3
ALTER TABLE ONLY public.project_mapserver_layers
    ADD CONSTRAINT project_mapserver_layers_projects_project_table_fkey FOREIGN KEY (project_table) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

### 4
ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_projects_project_table_fkey FOREIGN KEY (comment) REFERENCES public.projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE;

# project protocol
## database: biomaps
### 1
ALTER TABLE "projects" ADD COLUMN "protocol" character varying(5) NOT NULL DEFAULT 'http';

# utmzone column
## database: biomaps
### 1
ALTER TABLE "header_names" ADD COLUMN f_utmzone_column character varying (16);

# project_options table
## database: biomaps
### 1
CREATE SEQUENCE project_options_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

### 2
CREATE TABLE "public"."project_options" ( "id" integer DEFAULT nextval('project_options_id_seq') NOT NULL, "project_table" character varying NOT NULL, "role_id" integer, "option_name" character varying NOT NULL, "option_value" character varying NOT NULL, CONSTRAINT "project_options_id" PRIMARY KEY ("id"), CONSTRAINT "project_options_project_table_fkey" FOREIGN KEY (project_table) REFERENCES projects(project_table) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE, CONSTRAINT "project_options_role_id_fkey" FOREIGN KEY (role_id, project_table) REFERENCES project_roles(role_id, project_table) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE) WITH (oids = false);

# project column for project_metaname table
## database: biomaps
### 1
ALTER TABLE project_metaname ADD COLUMN project character varying; -- populate it manually!!

### 2
ALTER TABLE "project_metaname" ADD FOREIGN KEY ("project") REFERENCES "projects" ("project_table") ON DELETE CASCADE ON UPDATE CASCADE

# user_status table
## database: biomaps
### 1
CREATE TABLE "public"."admin_pages" ( "name" character varying NOT NULL, "label" character varying NOT NULL, "group" character varying NOT NULL, CONSTRAINT "admin_pages_name" PRIMARY KEY ("name")) WITH (oids = false);

### 2
INSERT INTO "admin_pages" ("name", "label", "group") VALUES ('dbcols','str_db_cols','project_admin'), ('groups','str_groups','project_admin'), ('upload_forms','str_upload_forms','project_admin'), ('functions','str_functions','project_admin'), ('taxon','str_taxon_names','project_admin'), ('access','str_access','project_admin'), ('languages','str_lang_definitions','project_admin'), ('modules','str_modules','project_admin'), ('imports','str_load_imports','project_admin'), ('files','str_file_manager','project_admin'), ('query_def','str_query_definitions','project_admin'), ('openlayes_def','str_layer_definitions','project_admin'), ('members','str_members','project_admin'), ('mapserv','str_map_definitions','project_admin'), ('server_logs','str_server_logs','project_admin'), ('r_server','rshinyserver','project_admin'), ('create_table','str_create_table','project_admin'), ('serverinfo','str_serverinfo','project_admin'), ('privileges','str_group_privileges','project_admin'), ('invites','str_invites','invites'), ('newsread','str_messages','messages'), ('new_project_form','str_new_project','new_project');

### 3
CREATE TABLE "public"."group_privileges" ( "project" character varying NOT NULL, "action" character varying NOT NULL, "role_id" integer NOT NULL, CONSTRAINT "group_privileges_project_action_role_id" UNIQUE ("project", "action", "role_id")) WITH (oids = false);

# news - personal message read state
## database: biomaps
### 1
ALTER TABLE project_news_stream ADD COLUMN readed boolean DEFAULT false;

# metarecords field
## database: project
### project: *
#### 1
ALTER TABLE system.uploadings ADD COLUMN metadata json;

# massive edit restore from save
## database: project
### project: *
#### 1
ALTER TABLE system.imports ADD COLUMN massive_edit json;

# column assign template updated
## database: biomaps
### 1
ALTER TABLE settings_import ADD COLUMN assignments json;

# form level column ordering
## database: biomaps
### 1
ALTER TABLE project_forms_data ADD COLUMN position_order smallint;

# citations per tables
## database: biomaps
### 1
ALTER TABLE track_citations ADD COLUMN data_table character varying;

# shared geom improvements
## database: project
### project: *
#### 1
ALTER TABLE system.shared_polygons ADD COLUMN original_name character varying;

#### 2
UPDATE system.shared_polygons SET original_name=name;

# interconnect communication state
## database: project
### project: *
#### 1
ALTER TABLE system.temp_index ADD COLUMN interconnect boolean DEFAULT false;

# receive_emails updates
## database: biomaps
### 1
--If you run this updates you should update php codes in all projects!!!
ALTER TABLE project_users ADD COLUMN receive_mails smallint DEFAULT 0;
ALTER TABLE project_users ADD COLUMN visible_mail smallint DEFAULT 1;

### 2
--If you run this updates you should update php codes in all projects!!!
UPDATE project_users SET visible_mail=foo.visible_mail,receive_mails=foo.receive_mails FROM (SELECT visible_mail,receive_mails,id FROM users) AS foo WHERE foo.id=user_id;

### 3
--If you run this updates you should update php codes in all projects!!!
ALTER TABLE users DROP COLUMN receive_mails;
ALTER TABLE users DROP COLUMN visible_mail;


# Server/Project interconnects
## database: biomaps
### 1
CREATE TABLE interconnects_master (
    local_project character varying(32),
    remote_project character varying,
    request_key character varying(32),
    accept_key character varying(32),
    pending boolean DEFAULT true
);

### 2
CREATE TABLE interconnects_slave (
    local_project character varying(32),
    remote_project character varying,
    accept_key character varying(32),
    pending boolean DEFAULT true
);

# SlideShow
## database: project
### project: *
#### 1
ALTER TABLE system.files ADD COLUMN slideshow boolean DEFAULT false;

# update project_layers
## database: biomaps
### 1
ALTER TABLE project_layers ADD COLUMN singletile boolean DEFAULT false;

### 2
ALTER TABLE project_layers ADD COLUMN legend boolean DEFAULT false;

# qgrids
## database: biomaps
### 1
CREATE TABLE project_mapserver_layers (
    mapserv_layer_name character varying,
    project_table character varying,
    geometry_type character varying
);

### 2
ALTER TABLE ONLY project_mapserver_layers
    ADD CONSTRAINT project_mapserver_layers_ukey UNIQUE (mapserv_layer_name, project_table, geometry_type);

### 3
INSERT INTO project_mapserver_layers (mapserv_layer_name,project_table,geometry_type)
     SELECT layer_name,project_table,geom_type
     FROM project_queries
     GROUP BY layer_name,project_table,geom_type

### 4
ALTER TABLE project_layers ADD COLUMN ms_layer character varying(32);

### 5
UPDATE project_layers SET ms_layer=foo.layer FROM (SELECT id,array_to_string(regexp_matches(layer_def, 'layers:.(\w+)'),'') as layer FROM project_layers) AS foo WHERE project_layers.id=foo.id

## database: project
### project: *
#### 1
CREATE TABLE %project%_qgrids (
        row_id integer NOT NULL,
        original geometry
);

#### 2
ALTER TABLE ONLY %project%_qgrids
        ADD CONSTRAINT %project%_qgrids_pkey PRIMARY KEY (row_id);

#### 3
UPDATE %project%_qgrids SET utm_10 = st_transform(foo.geom,4326)
FROM (
   SELECT d.obm_id,k.geom FROM %project% d LEFT JOIN shared."utm10_wgs84" k ON (st_within(d.obm_geometry,st_transform(k.geom,4326)))
) as foo
WHERE row_id=foo.obm_id;

# general search: taxon+observer
## database: project
### project: *
#### 1
CREATE SEQUENCE %project%_search_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START %d CACHE 1;

#### 2
CREATE SEQUENCE %project%_search_connect_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

#### 3
CREATE TABLE %project%_search (
    search_id integer DEFAULT nextval('%project%_search_id_seq') NOT NULL,
    data_table character varying(64) NOT NULL,
    subject character varying(128) NOT NULL,
    meta character varying(128) NOT NULL,
    word character varying(128) NOT NULL,
    institute character varying(128),
    status character varying(128),
    role_id integer,
    frequency integer,
    taxon_db integer DEFAULT 0,
    CONSTRAINT %project%_search_meta_word_key UNIQUE ("meta","word","subject")) WITH (oids = false);

#### 4
CREATE TABLE %project%_search_connect (
    data_table varchar(64),
    row_id integer,
    species_ids integer[],
    observer_ids integer[],
    CONSTRAINT %project%_search_connect_table_row_key UNIQUE ("data_table","row_id")) WITH (oids = false);

#### 5
INSERT INTO %project%_search (meta,word,subject,search_id,status,frequency)
    SELECT meta, word, lang, taxon_id, status, frequency FROM %project%_taxon;

#### 6
INSERT INTO %project%_search (meta,word,subject,frequency)
    SELECT lower(regexp_replace(names,'\s+','')),names,\'observer\', COUNT(names)
    FROM (SELECT unnest(regexp_split_to_array(concat_ws(',',%CITE_COL%),E\'[,;]\\\\s*\')) as names FROM %1$s) foo GROUP BY names ORDER BY count DESC;

#### 7
INSERT INTO %project%_search_connect (data_table, row_id, observer_ids)
    SELECT '%project%', obm_id, array_agg(search_id)
    FROM (SELECT obm_id, unnest(regexp_split_to_array(concat_ws(',',%CITE_COL%),E\'[,;]\\\\s*\')) as names FROM %1$s) as foo LEFT JOIN %project%_search o ON names = word GROUP by obm_id;

#### 8
UPDATE %project%_search_connect sc SET species_ids = ARRAY[taxon_id]
    FROM (SELECT taxon_id, obm_id FROM %1$s b LEFT JOIN %project%_taxon tx ON b.%SPECIES_COL% = tx.word AND b.%SPECIES_COL% IS NOT NULL) as t WHERE sc.row_id = t.obm_id;

# custom filetype
## database: project
### project: *
#### 1
CREATE SEQUENCE %project%_custom_filetype_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 821 CACHE 1;

#### 2
CREATE TABLE "public"."%project%_custom_filetype" (
    "id" integer DEFAULT nextval('%project%_custom_filetype_id_seq') NOT NULL,
    "file_type" character varying(32) NOT NULL,
    "from_column" character varying(32) NOT NULL,
    "action" character varying(32) NOT NULL,
    "to_column" character varying(32),
    "from_value" character varying(128),
    "to_value" character varying(128),
    "form" integer,
    "action_order" integer,
    "comments" character varying,
    "variant" character varying(32),
    CONSTRAINT "%project%_custom_filetype_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
