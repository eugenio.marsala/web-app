<?php

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once('./server_vars.php.inc');
require_once('./common_functions.php');
require_once('./init.php');

// Database connect ID
if (!$BID = PGconnectSQL(mainpage_user,mainpage_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

?>
<!DOCTYPE html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title><?php echo $institute_short_name ?></title>
    <link rel="shortcut icon" href="<?php echo $protocol.'://'.$HOST?>/img/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="<?php echo $protocol.'://'.$HOST?>/img/favicon.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-16.png">
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-57.png">
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-114.png">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-72.png">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-144.png">
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-60.png">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-120.png">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-76.png">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-152.png">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echo $protocol.'://'.$HOST?>/img/favicon-180.png">
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/dropit.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo $protocol.'://'.$HOST?>/css/pure-min.css" type="text/css" />
    <script type="text/javascript" src="<?php echo $protocol.'://'.$HOST?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $protocol.'://'.$HOST?>/js/dropit.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#menu1').dropit();
        $('#menu2').dropit();
        $('#cookie-ok').click(function(){
            $.post('index.php', {'cookies_accepted':'1' },
                function(data){
                    $("#cookie_div").hide();
            }); 
        });
    });
    </script>
</head>
<body>
<div id='holder'>
<div id='header'>
    <div style='width:1000px;margin-left:auto;margin-right:auto'>
        <div style='float:left'>
        <ul id='menu1' class='menu'>
                <li><a href="<?php echo $protocol."://".$HOST ?>/index.php"><?php echo str_home ?></a></li>
                <li><a href="http://openbiomaps.org/documents/"><?php echo str_documentation ?></a></li>
                <?php
                    if (file_exists('obm.readme'))
                        echo "<li><a href=\"$protocol://".$HOST."/obm.readme\">".str_readme."</a></li>";
                    if (file_exists('obm.release'))
                        echo "<li><a href=\"$protocol://".$HOST."/obm.release\">".str_release."</a></li>";
                ?>
        </ul>
        </div>
        <div style='float:right'>
        <ul id='menu2' class="menu">
            <li><a href="#"><?php echo str_languages ?></a><ul>
                    <li></li>
                    <li><a href="?lang=en">in english</a></li>
                    <li><a href="?lang=hu">magyarul</a></li>
                </ul></li>
        </ul>
        </div><!--/languages-->
    </div>
</div><!--/header-->
<div id='body'><div style='width: 800px;
  display: block;
  margin-left: auto;
  margin-right: auto;
  padding-top:60px;'>

<?php
if ($load_mainpage) {
?>


<div style='font-size:6em;font-weight:bold;font-family:courier new;position:relative'>
<span style='position:relative;z-index:-1;color: transparent;
background: #666666;
-webkit-background-clip: text;
-moz-background-clip: text;
background-clip: text;
text-shadow: 0px 3px 3px rgba(255,255,255,0.5);'><?php echo $institute_short_name; ?></span>
    <img src='<?php echo $protocol.'://'.$HOST ?>/img/<?php echo $institute_logo ?>' style='position:absolute;z-index:-2;top:75px;right:10px'>
</div>
<div class='helptext'>
<?php

    $cmd = "SELECT project_table FROM projects ORDER BY project_table";
    $res = pg_query($BID,$cmd);
    
    $stable = array();
    $testing = array();
    $experimental = array();
    $other = array();
    while ($e = pg_fetch_assoc($res)) {

        $cmd = "SELECT project_table,f_species_column,domain,stage,short,long,language,doi,creation_date,\"Creator\" as g,running_date,licence,rum FROM projects LEFT JOIN header_names ON (f_table_name=project_table) LEFT JOIN project_descriptions ON (project_table=projecttable) WHERE project_table='{$e['project_table']}' AND language='{$_SESSION['LANG']}'";
        $res2 = pg_query($BID,$cmd);
        if (!pg_num_rows($res2)) {
            $cmd = "SELECT project_table,f_species_column,domain,stage,short,long,language,doi,creation_date,\"Creator\" as g,running_date,licence,rum FROM projects LEFT JOIN header_names ON (f_table_name=project_table) LEFT JOIN project_descriptions ON (project_table=projecttable) WHERE project_table='{$e['project_table']}' AND language='en'";
            $res2 = pg_query($BID,$cmd);
            if (!pg_num_rows($res2)) {
                $cmd = "SELECT project_table,f_species_column,domain,stage,short,long,language,doi,creation_date,\"Creator\" as g,running_date,licence,rum FROM projects LEFT JOIN header_names ON (f_table_name=project_table) LEFT JOIN project_descriptions ON (project_table=projecttable) WHERE project_table='{$e['project_table']}' LIMIT 1";
                $res2 = pg_query($BID,$cmd);
            }
        }
        $row = pg_fetch_assoc($res2);

        $licence = 'n/a';
        if ($row['licence']!='') {
            $licence = $row['licence'];
            if($licence=='ODbL') $licence = "<a href='http://opendatacommons.org/licenses/odbl/1.0/' target='_blank'>ODbL</a>";
        }

        $rum = 'n/a';
        if ($row['rum']!='') {
            $rum = "";
            preg_match('/([0+-])([0+-])([0+-])/',$row['rum'],$m);
            if($m[1]=='+')
                $rum .= '<span style="color:green;font-weight:bold">R</span>';
            elseif($m[1]=='0')
                $rum .= '<span style="color:red;font-weight:bold">R</span>';
            else
                $rum .= '<span style="color:black;font-weight:bold">R</span>';
            if($m[2]=='+')
                $rum .= '<span style="color:green;font-weight:bold">U</span>';
            elseif($m[2]=='0')
                $rum .= '<span style="color:red;font-weight:bold">U</span>';
            else
                $rum .= '<span style="color:black;font-weight:bold">U</span>';
            if($m[3]=='+')
                $rum .= '<span style="color:green;font-weight:bold">M</span>';
            elseif($m[3]=='0')
                $rum .= '<span style="color:red;font-weight:bold">M</span>';
            else
                $rum .= '<span style="color:black;font-weight:bold">M</span>';
        }
        $rum = "<div style='padding:0 2px 0 2px;border:1px solid gray;font-family:Courier New;display:inline'><a href='http://openbiomaps.org/documents/en/faq.html#what-is-the-rum' target='_blank'>$rum</a></div>";


        if ($row['stage']=='stable') {
            if ($row['domain']=='')
                $domain = $protocol.'://'.$HOST.'/projects/'.$row['project_table'];
            else
                $domain = $protocol.'://'.$row['domain'];
             $stable[] = sprintf('
                <li style="padding-bottom:10px">
                    <a href="%1$s" class="project-name">%2$s</a>
                    <p style="padding-left:10px">%3$s</p>
                    <p style="padding-left:10px">DOI: <a href="%1$s/doi/">%4$s</a></p>
                    <div style="padding-left:10px">%5$s: %6$s<br>
                                %7$s: %8$s - %9$s: %10$s <br>
                                %11$s: %13$s<br>
                                %12$s: %14$s</div></li>',
            $domain,$row['short'],
            $row['long'],
            $row['doi'].'----',
            str_creator,$row['g'],
            str_creation_date,$row['creation_date'],str_running_date,$row['running_date'],
            str_open_content_level,str_licence,$rum,$licence);
        } else if ($row['stage']=='testing') {
            if ($row['domain']=='')
                $domain = $protocol.'://'.$HOST.'/projects/'.$row['project_table'];
            else
                $domain = $protocol.'://'.$row['domain'];
            $testing[] = sprintf('<li style="padding-bottom:10px"><a href="%1$s" class="project-name">%2$s</a><p style="padding-left:10px">%3$s</p><p style="padding-left:10px">%4$s: %5$s</li>',$domain,$row['short'],$row['long'],str_creator,$row['g']);
        } else if ($row['stage']=='experimental') {
            if ($row['domain']=='')
                $domain = $protocol.'://'.$HOST.'/projects/'.$row['project_table'];
            else
                $domain = $protocol.'://'.$row['domain'];
            $experimental[] = sprintf("<li style='padding-bottom:10px'><a href='%s' class='project-name'>%s</a><p style='padding-left:10px'>%s</p></li>",$domain,$row['short'],$row['long']);
        } else if ($row['stage']=='other') {
            if ($row['domain']=='')
                $domain = $protocol.'://'.$HOST.'/projects/'.$row['project_table'];
            else
                $domain = $protocol.'://'.$row['domain'];
            $other[] = sprintf("<li style='padding-bottom:10px'><a href='%s' class='project-name'>%s</a><p style='padding-left:10px'>%s</p></li>",$domain,$row['short'],$row['long']);
        } else {
            if ($row['domain']=='')
                $domain = $protocol.'://'.$HOST.'/projects/'.$row['project_table'];
            else
                $domain = $protocol.'://'.$row['domain'];
            $other[] = sprintf("<li style='padding-bottom:10px'><a href='%s' class='project-name'>%s</a><p style='padding-left:10px'>%s</p></li>",$domain,$row['short'],$row['long']);
        }
    }
    echo '<h3>'.str_stable.':</h3>';
    echo '<ul style="list-style-type:none">';
    print implode('',$stable);
    echo '</ul>';
    echo  '<h3>'.str_testing.":</h3>";
    echo '<ul style="list-style-type:none">';
    print implode('',$testing);
    echo '</ul>';
    echo  '<h3>'.str_experimental.":</h3>";
    echo '<ul style="list-style-type:none">';
    print implode('',$experimental);
    echo '</ul>';
    echo  '<h3>'.str_other.":</h3>";
    echo '<ul style="list-style-type:none">';
    print implode('',$other);
    echo '</ul>';
}    
    
elseif ($load_about) { ?>
    
    <br><br> 
    <h2><?php echo about; ?></h2>
    <div  class='helptext'>
    <?php

        if (file_exists('about_'.$_SESSION['LANG'].'.html'))
            include('about_'.$_SESSION['LANG'].'.html');
        else 
            include('about.html');
    ?>
    </div>
<?php 
}

elseif ($load_disclaimer) { ?>

    <br><br> 
    <h2><?php echo disclaimer ?> </h2>
    <div  class='helptext'>
    <?php
        if (file_exists('disclaimer_'.$_SESSION['LANG'].'.html'))
            include('disclaimer_'.$_SESSION['LANG'].'.html');
        else 
            include('disclaimer.html');
    ?>
    </div>
<?php 
}
elseif ($load_privacy) { ?>

    <br><br> 
    <h2><?php echo t(str_privacy_policy) ?></h2>
    <!--<h2>Privacy and Data Protection Policy</h2>-->
    <div class='helptext'>
        <?php 
        if (file_exists('at_'.$_SESSION['LANG'].'.html'))
            include('./at_'.$_SESSION['LANG'].'.html');
        else
            include('./at.html');
        ?>    
    </div>
<?php 
}
elseif ($load_useragreement) { ?>

    <br><br> 
    <h2><?php echo str_user_agreement ?></h2>
    <!--<h2>General Agreement</h2>-->
    <div class='helptext' style='text-align:left;width:700px;white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */'>
    <?php 
        if (file_exists('aszf_'.$_SESSION['LANG'].'.html'))
            include('./aszf_'.$_SESSION['LANG'].'.html');
        else
            include('./aszf.html');
    ?>  
    </div>
<?php 
}
elseif ($load_technical) { ?>

    <br><br> 
    <h2><?php echo str_technical_info ?></h2>
    <div class='helptext'>
        <?php 
        if (file_exists('server_'.$_SESSION['LANG'].'.html'))
            include('./server_'.$_SESSION['LANG'].'.html');
        else
            include('./server.html');
        ?>    
    </div>
<?php 
}
/* Using Cookies */
elseif ($load_cookies) { ?>
    <h2><?php echo str_cookies ?></h2>
    <div class='helptext'>
        <?php 
        if (file_exists('cookies_'.$_SESSION['LANG'].'.html'))
            include('./cookies_'.$_SESSION['LANG'].'.html');
        else
            include('./cookies.html');
        ?>
    </div>
<?php
}
?>



</div>
<img src='<?php echo $protocol.'://'.$HOST?>/img/OpenBioMaps.png' style='width:200px;position:absolute;bottom:245px;right:30px'>
</div>
<!--/body-->
<div id='footer'>
    <div class='grass'></div>
    <div class='footer'>
        <div style='font-size:75%;'>
            <ul style='list-style-type:none;padding:0'>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/terms/'><?php echo t(str_terms) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/privacy/'><?php echo t(str_privacy_policy) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/disclaimer/'><?php echo t(str_disclaimer) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/about/'><?php echo t( str_about_openbiomaps) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/technical/'><?php echo t(str_technical_info) ?></a></li>
            <li style='display:inline;padding-right:20px'><a href='<?php echo $protocol.'://'.$HOST?>/cookies/'><?php echo t(str_cookies) ?></a></li>
            </ul>
        </div>
    </div>
    <div style='padding:10px 0px 0px 0px;
    text-align:center;
    margin:0;
    position:relative;
    background:transparent;
    display:table;
    width:100%;'>
        <div style='display:table-cell;color:#777;font-size:75%'>
            <p style="font-weight:bold">Openbiomaps Contributors 2018</p>
            <br>
            <br>
        </div>
    </div>
    <?php
    if (!isset($_SESSION['cookies_accepted'])) {
        echo "<div id='cookie_div' style='text-align:center;position:fixed;z-index:1001;bottom:0;padding:20px 50px 20px 50px;background-color:black;color:#acacac'>";
        if ($_SESSION['LANG']!='hu') {
    ?>
        This site uses cookies to deliver our services and to offer you a better browsing experience. By using our site, you acknowledge that you have read and understand our <a href='<?php echo $protocol.'://'.$HOST?>/cookies/'>Cookie Policy</a>, <a href='<?php echo $protocol.'://'.$HOST?>/privacy/'>Privacy Policy</a>, and our <a href='<?php echo $protocol.'://'.$HOST?>/terms/'>Terms of Services</a>. Your use of OpenBioMaps’s Products and Services, including the OpenBioMaps Network, is subject to these policies and terms. <button class='pure-button' id='cookie-ok'>Accept</button>
    <?php
        } else {
    ?>
        Ez a weboldal Sütiket használ az oldal működésének biztosításához és a jobb felhasználói élmény biztosítása érdekében. A Sütik weboldalon történő használatával kapcsolatos részletes tájékoztatás az <a href='<?php echo $protocol.'://'.$HOST?>/privacy/'>Adatkezelési Tájékoztatóban</a> és a <a href='<?php echo $protocol.'://'.$HOST?>/cookies/'>Süti Tájékoztatóban</a> és a <a href='<?php echo $protocol.'://'.$HOST?>/terms/'>Felhasználói szabályzatban</a> található. <button class='pure-button' id='cookie-ok'>Rendben</button>
    <?php
        }
        echo "</div>";
    }
    ?>
</div><!--/footer-->
</div><!--/holder-->
</body>
</html>
