FROM  php:7.1-apache

RUN apt-get update -y && apt-get install -y \
        gdal-bin \
        gpsbabel

RUN apt-get update -y && apt-get install -y \
        libapache2-mod-auth-pgsql \
        libpq-dev \
        libmcrypt-dev \
        libzip-dev \
        vim \
    && docker-php-ext-install exif \
    && docker-php-ext-install pgsql \
    && docker-php-ext-install pdo_pgsql \
    && docker-php-ext-install mcrypt \
    && docker-php-ext-install -j$(nproc) zip

RUN apt-get install -y libmemcached-dev zlib1g-dev \
    && pecl install memcached \
    && docker-php-ext-enable memcached

RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

#RUN pecl install xdebug-2.5.5 \
#    && docker-php-ext-enable xdebug

RUN a2enmod rewrite && a2enmod headers

# create openbiomaps log file 
RUN touch /var/log/openbiomaps.log && chown www-data:www-data /var/log/openbiomaps.log

RUN sed -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/biomaps\/root-site/' /etc/apache2/sites-enabled/000-default.conf

# Add PHP composer to image
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./server_confs_templates/etc/apache2/conf-enabled/openbiomaps.conf /etc/apache2/conf-enabled/openbiomaps.conf
COPY ./server_confs_templates/etc/openbiomaps /etc/openbiomaps
COPY ./server_confs_templates/var/lib/openbiomaps /var/lib/openbiomaps
COPY ./root-site /var/www/html/biomaps/root-site
COPY ./projects /var/www/html/biomaps/root-site/projects
COPY ./resources /var/www/html/biomaps/resources
COPY ./server_confs_templates/server_vars.php.inc /var/www/html/biomaps/root-site/server_vars.php.inc
COPY ./composer.json /var/www/html/biomaps/
COPY ./composer.lock /var/www/html/biomaps/

# install dependencies
RUN (cd /var/www/html/biomaps && composer install --no-dev)
# ??Should it be a volume??
#VOLUME /var/lib/openbiomaps/biomaps/resources/vendor

VOLUME /var/www/html/biomaps/root-site/projects
VOLUME /var/lib/openbiomaps

