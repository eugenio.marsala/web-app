<?php
# API_VERSION 2.0

class pds {
    //
    public $valid_requests = array('service','table','header','project','user','get_data_rows','species','history','header','get_profile','track_data','track_download','track_citation','get_specieslist','LQ','type','upload','upload_test','m_comment','m_datum','m_geometry','get_form_list','get_form_data','put_data','form_id','debug','set_rules','get_report','put_header','access_token','soft_error','get_table_list','get_project_list','ignore_warns','get_project_vars','get_trainings','get_training_questions','training_results','training_toplist','get_mydata_rows','ic_post_data','ic_accept_key','ic_request_key','ic_slave_project','ic_slave_server','pg_user','get_tile_list','get_tile_zip');
    // subset of valid_requests which allowed by direct pds request

    #if ($API_VERSION==1.0)
    #    public $allowed_on_direct_access = array('get_api_form','get_api_forms','put_api_form','upload','table');

    public $project = '';
    public $table = '';
    public $access_token = '';
    public $dto = array();
    public $user = array();
    public $request_error = '';
    public $request_warnings = array();
    public $rights = array();
    public $results = array();
    public $method = '';
    public $debug = false;
    public $foo = '';
    public $pfs_put_data = false;
    public $API_VERSION = 1.0;

    // constructor
    public function __construct($API_POST,$API_VERSION) {
        $BID = $GLOBALS['BID'];
        $ID = $GLOBALS['ID'];
        $this->API_VERSION = $API_VERSION;

        foreach ($API_POST as $key=>$val) {

            // is the 'service' key exists and it is in the valid_request array return with "method"
            if (in_array($key,$this->valid_requests)) {
                if ($key == 'service') {
                    // ez így töbszörös service listát is feldolgoz
                    // az utolsó marad benne...
                    $this->method = $val;
                } else if ($key == 'debug') {
                    $this->debug = true;
                } else if ($key == 'put_data') {
                    $this->pfs_put_data = true;
                } else if ($key == 'table') {
                    $this->table = $val;
                } else if ($key == 'access_token') {
                    $this->access_token = $val;
                } else {
                    // egyéb request kulcsok a request tömbbe...
                    $this->request[$key]=$val;
                }
            } else {
                http_response_code(400);
                $this->request_error="$key is not a valid request!";
            }
        }

        //it is defined in the per project pds.php
        if (defined('CALL')) {

            //$this->project = PROJECT_DIR;
            //$this->project = basename(getenv('PROJECT_DIR'));
            $this->project = PROJECTTABLE;
            // ha már nem lett volna behívva a db_funcs-ban!
            //require_once(CALL."/projects/".PROJECT_DIR."/local_vars.php.inc");
            require_once(getenv('OB_LIB_DIR').'languages.php');

        } else {
            #API_VERSION <= 1.0
            # Ez a mobil fejlesztés miatt meghagyott kivétel!
            # ki kell majd szedni
            /*require_once("direct_access_vars.inc");
            if($this->method == 'PFS') {
                $this->project = $this->request['table'];
                foreach($this->request as $key=>$val) {
                    if (!in_array($key,$this->allowed_on_direct_access)) {
                        $this->request_warnings[] = "Request $key not allowed by using direct access, use the /projects/__foo__/pds.php";
                    }
                }
                return;
            }
            $this->request_error = "Only some PFS request allowed by using direct access, use the project path: /projects/__foo__/pds.php";
             */
            http_response_code(400);
            //$this->request_error = "No project defined!";
            return;
        }

        /* Oauth compatibilty for general SESSION variables
            * */
        if (defined('FAKE_PROJECT')) {
            $cmd = sprintf("SELECT id,username,pu.project_table
                FROM users
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email)
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s GROUP BY users.id,pu.project_table",quote($this->access_token));
            $res = pg_query($BID,$cmd);
            while ($row = pg_fetch_assoc($res)) {
                $_SESSION['Tid'] = $row['id'];
                $_SESSION['Trole_id'] = '';
                $_SESSION['Tname'] = '';
                $_SESSION['Tmail'] = '';
                $m = new modules(false);
                $_SESSION['modules'] = $m->set_modules();
            }
        } else {
            $cmd = sprintf("SELECT id,username,email
                FROM users
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email)
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table='%s' GROUP BY users.id",quote($this->access_token),$this->project);
            $res = pg_query($BID,$cmd);

            $_SESSION['Tid'] = 0;
            $_SESSION['Tmail'] = 'non-logined-user';
            $_SESSION['Tname'] = 'non-logined-user';
            $_SESSION['Tgroups'] = '';
            $_SESSION['Trole_id'] = 0;

            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $_SESSION['Tid'] = $row['id'];
                $_SESSION['Tmail'] = $row['email'];
                $_SESSION['Tname'] = $row['username'];

                read_groups($row['id']);
            }
            $m = new modules(false);
            $_SESSION['modules'] = $m->set_modules();
            st_col($this->table,'session');
        }

        // check project requests
        if ($this->project!='') {
            if (!isset($this->table) or $this->table=='')
                $this->table = $this->project;

            if ($this->table != $this->project)
                $this->table = $this->project.'_'.$this->table;

            $_SESSION['current_query_table'] = $this->table;

            $cmd = sprintf('SELECT * FROM projects LEFT JOIN header_names ON (f_table_name=project_table) WHERE project_table=%s AND f_main_table=%s',quote($this->project),quote($this->table));
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                http_response_code(400);
                $this->request_error = $this->table." table does not exists!";
                return;
            } else {
                $row = pg_fetch_assoc($res);
                #
                # other project params
            }

            if (defined('FAKE_PROJECT')) {
                $this->project = '';
                $this->table = "";
            }
        } else {
            http_response_code(400);
            $this->request_error = "No project defined!";
            return;
        }
    }

    /* SERVICE: PRS
     * query data point or history-data point based on its "id"
     * _GET request
     * E.G.: http://openbiomaps.org/pds/service.php?service=PRS&table=danubefish&data=(3699,4677) */
    public function getData() {

        if(!isset($this->project) or $this->project=='') {
            http_response_code(400);
            return;
        }
        if(!isset($this->table) or $this->table=='') {
            http_response_code(400);
            return;
        }
        $ID = $GLOBALS['ID'];

        # prepare a query and
        # return the result as json object
        $request_table = '';
        $request_value = '';
        $request_column = '';
        foreach($this->request as $key=>$val)
        {
            if ($key == 'get_data_rows') {
                $request_table = $this->table;
                // defaults
                $request_column = '--id--';
                $request_value = $val;
                // custom
                // e.g. species="Motacilla flave"
                if (preg_match('/([a-z0-9_]+)=(.+)/i',$val,$m)) {
                    $request_column = $m[1];
                    $request_value = $m[2];
                } elseif ($val == '*') {
                    $request_column = 1;
                    $request_value = 1;
                }
            }
            elseif ($key == 'get_history_rows') {
                $request_table = $this->table.'_history';
                $request_column = '--history--';
                $request_value = $val;
            }
        }

        // adat lekérdezés
        if (!isset($this->request['type']))
            $this->request['type'] = 'json';

        $response = dataQuery($request_table,$request_value,$request_column,$this->request['type']);
        $j = json_decode($response);

        if ($j->{'status'}=='error') {
            http_response_code(400);
            echo $response;
            return;
        } else if ($j->{'status'}=='fail') {
            http_response_code(401);
            echo $response;
            return;
        } else {
            $cmd = $j->{'data'};
            $result = pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                $errorID = uniqid();
                echo common_message('fail',str_sql_error,$errorID);
                log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                return;
            }
            if (!pg_num_rows($result)) {
                echo common_message('error','Data query has no data');
                return;
            }

            if ($this->request['type']=='xml') {
                // collapse with large arrays!!!
                $this->results = pg_fetch_all($result);
                // creating object of SimpleXMLElement
                $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><OBM_data></OBM_data>");
                // function call to convert array to xml
                array_to_xml($this->results,$xml);
                // print out XML
                header('Content-type: text/xml');
                print $xml->asXML();
            } else {
                header('Content-type: application/json');
                //defult request type is JSON
                //simulating common_message() function
                echo '{"status":"success","data":[';
                //echo '["';
                $p = pg_num_rows($result);
                $n = 0;
                //tricky way to fetch large respones
                while ($row = pg_fetch_row($result)) {
                    $n++;
                    echo $row[0];
                    if($n<$p) {
                        echo ",";
                    }
                }
                echo "]}";
                //the following way not works with large objects!
                //$this->results = pg_fetch_all($result);
                //print array2json($this->results);
            }
        }
    }

    // SERVICE: PFS
    // project feature servise
    // It is a function
    // $this->request['table'] should be defined and valid
    //
    public function getFunction($key,$value) {

        $BID = $GLOBALS['BID'];
        $ID = $GLOBALS['ID'];
        $cache = XCache::getInstance();

        $modules = new modules();

        // Get Function result
        if ($key == 'history') {
            # Ha a value tömb: (id1,id2,...)...
            #if(!is_array($value)) {
            #    if (preg_match("/^\((.+)\)$/",$value,$m)) {
            #        $value = preg_split("/,/",$m[1]);
            #    }
            #}

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }

            if (!is_array($value)) $value = array($value);
            foreach ($value as $var) {
                $result = histHeader($this->table,$var);
                if (pg_num_rows($result))
                    $r1 = pg_fetch_all($result);
                else
                    $r1 = array();
                $result = histRows($this->table,$var);
                if (pg_num_rows($result))
                    $r2 = pg_fetch_all($result);
                else
                    $r2 = array();
                if (count($r1) and count($r2) and count($r1)==count($r2)) {
                    $c = array_combine((array_merge(array_keys($r1[0]),array_keys($r2[0]))),array_merge(array_values($r1[0]),array_values($r2[0])));
                    $this->results[] = $c;
                }
            }
            $this->$key = array2json($this->results);
        } elseif ($key == 'track_data') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }

            if (!is_array($value)) {
                if (isset($cache->{'track_data_'.$this->table.'_'.$value})) {
                    print $cache->{'track_data_'.$this->table.'_'.$value};
                    return;
                }
            } else {
                http_response_code(400);
                //not handling array
                return;
            }

            $cmd = sprintf("SELECT counter FROM track_data_views WHERE project_table=%s AND id=%d",quote($this->project),preg_replace('/(\d+)[_]?(\d+)?/','$1',$value));
            $res = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($res);
            $this->$key = array2json(array($row['counter']));
            $cache->set('track_data_'.$this->project.'_'.$value,$this->$key,60);

        } elseif ($key == 'track_download') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }

            if (!is_array($value)) {
                if (isset($cache->{'track_download_'.$this->project.'_'.$value})) {
                    print $cache->{'track_download_'.$this->project.'_'.$value};
                    return;
                }
            } else {
                http_response_code(400);
                //not handling array
                return;
            }

            $cmd = sprintf("SELECT counter FROM track_downloads WHERE project_table=%s AND id=%d",quote($this->project),$value);
            $res = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($res);
            $this->$key = array2json(array($row['counter']));
            $cache->set('track_download_'.$this->table.'_'.$value,$this->$key,60);

        } elseif ($key == 'track_citation') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }

            if (!is_array($value)) {
                if (isset($cache->{'track_citation_'.$this->project.'_'.$value})) {
                    print $cache->{'track_citation_'.$this->project.'_'.$value};
                    return;
                }
            } else {
                http_response_code(400);
                //not handling array
                return;
            }

            $cmd = sprintf("SELECT unnest(source) as source FROM track_citations WHERE project_table=%s AND id=%d",quote($this->project),$value);
            $res = pg_query($BID,$cmd);
            $cit = array();
            while($row = pg_fetch_assoc($res)){
                $cit[] = $row['source'];
            }
            $this->$key = array2json($cit);
            $cache->set('track_citation_'.$this->project.'_'.$value,$this->$key,60);

        } elseif ($key == 'set_rules') {
            $this->$key = json_encode('a');
            print (json_encode('1'));

        } elseif ($key == 'get_profile') {

            if(!isset($this->project)) {
                http_response_code(400);
                return;
            }

            $result = pds_profile($this->project,$value);
            if (!$result) {
                echo common_message('error',"get profile data failed for user: $value");
                return;
            }

            $row = pg_fetch_assoc($result);
            echo common_message('ok',$row);
            return;

        } elseif ($key == 'get_specieslist') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }

            # fajlista lekérdezése
            $species_array = $_SESSION['st_col']['ALTERN_C'];
            $sp = implode(",",$species_array);
            $cmd = "SELECT * FROM \"".$this->project."_taxon\" ORDER BY taxon_id";
            $result = pg_query($ID,$cmd);
            if (!$result) {
                echo common_message('error',"get specieslist failed for project: ".$this->project);
                return;
            }

            if (pg_num_rows($result)) {
                echo common_message('ok',pg_fetch_all($result));
            }
            return;

        } elseif ($key == 'LQ') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }
            /* Previously saved query load
             * */
            $response = $this->loadq($value);
            $j = json_decode($response);

            if ($j->{'status'}=='error') {
                echo $response;
            } else if ($j->{'status'}=='fail') {
                echo $response;
            } else {
                $data = $j->{'data'};
                // adat lekérdezés
                $response = dataQuery($data[0],$data[1],'id','JSON');
                $j = json_decode($response);

                if ($j->{'status'}=='error') {
                    echo $response;
                } else if ($j->{'status'}=='fail') {
                    echo $response;
                } else {
                    $cmd = $j->{'data'};
                    $result = pg_query($ID,$cmd);
                    if (pg_last_error($ID)) {
                        $errorID = uniqid();
                        echo common_message('fail',str_sql_error,$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                        return;
                    }
                    if (!pg_num_rows($result)) {
                        echo common_message('error','Data query has no data');
                        return;
                    }
                    // heavy memory load warning!
                    echo common_message('ok',pg_fetch_all($result));
                }
            }

        } elseif ($key == 'get_mydata_rows') {
            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }
            $cmd = sprintf("SELECT id FROM system.uploadings WHERE project='%s' AND project_table='%s' AND uploader_id=%d",$this->project,$this->table,$_SESSION['Trole_id']);
            $res1 = pg_query($ID,$cmd);

            $data = array();
            $limit = 0;
            if ($value!='')
                $limit = $value;

            $counter = 0;
            while($row = pg_fetch_assoc($res1)) {
                $response = dataQuery($this->table,$row['id'],'obm_uploading_id','JSON');
                $j = json_decode($response);

                if ($j->{'status'}=='error') {
                    echo $response;
                } else if ($j->{'status'}=='fail') {
                    echo $response;
                } else {
                    $cmd = $j->{'data'};
                    $result = pg_query($ID,$cmd);
                    if (pg_last_error($ID)) {
                        $errorID = uniqid();
                        echo common_message('fail',str_sql_error,$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                        return;
                    }
                    //if (!pg_num_rows($result)) {
                    //    echo common_message('error','Data query has no data: '.$cmd);
                    //    return;
                    //}
                    $odata = array();
                    while ($row_data = pg_fetch_assoc($result)) {
                        if ($limit and $counter == $limit)
                            break;
                        $odata[] = json_decode($row_data['row_to_json'],true);
                        $counter++;
                    }
                    if (count($odata))
                        $data[] = $odata;
                }
            }

            echo common_message('ok',$data);

        } elseif ($key == 'get_form_list') {

            # list of available forms
            if(!isset($this->table) or $this->table=='') {
                http_response_code(400);
                return;
            }

            if (isset($_SESSION['Tgroups']))
                $tgroups = $_SESSION['Tgroups'];
            else
                $tgroups = '';

            if (isset($_SESSION['Tid']))
                $user = $_SESSION['Tid'];
            else
                $user = 0;

            $form_list = array();
            $form_ids = pds_form_choose_list($this->table,$user,$tgroups);
            foreach($form_ids as $id) {
                $row = pds_form_element($id,$this->table);
                $translated = $row['form_name'];
                if (defined($row['form_name'])) $translated = constant($row['form_name']);
                // backward compatibility
                // id, visibility, , [{“form_id”:”93”,”form_name”:”lepke űrlap”}
                $form_list[] = array('id'=>$id,'visibility'=>$translated,'form_id'=>$id,'form_name'=>$translated,'last_mod'=>$row['last_mod']);
                //$list[] = array('form_id'=>$id,'form_name'=>$translated);
            }
            if (!count($form_list)) {
                echo common_message('error','No data returned');
            } else {
                echo common_message('ok',$form_list);
            }
            return;

        } elseif ($key == 'get_form_data') {

            # fields and settings of a selected form
            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                return;
            }

            if (isset($_SESSION['Tgroups']))
                $tgroups = $_SESSION['Tgroups'];
            else
                $tgroups = '';

            if (isset($_SESSION['Tid']))
                $user = $_SESSION['Tid'];
            else
                $user = 0;

            if ( pds_form_access_check($value,$this->project,$user,$tgroups)!==1 ) {
                http_response_code(401);
                return;
            }

            $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%s AND project_table=%s",quote($value),quote($this->project));
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                echo common_message('error','Wrong form ID. Empty results returned.');
                return;
            }
            $row = pg_fetch_assoc($res);
            $st_col = st_col($row['destination_table'],'array');
            $destination_table = $row['destination_table'];

            if (isset($st_col['ORDER']))
                $order_col = json_decode($st_col['ORDER'],true);
            else
                $order_col = array();

            $ordered_values = array();
            if (isset($order_col[$destination_table]))
                foreach($order_col[$destination_table] as $key=>$val) {
                    if ($val!='')
                        $ordered_values[$val] = $key;
                }
            $order = "'".implode("','",array_values($ordered_values))."'";

            $cmd = sprintf("SELECT \"column\",position_order FROM project_forms_data WHERE form_id=%s ORDER BY position_order",quote($value));
            $respo = pg_query($BID,$cmd);
            $column_positions = pg_fetch_all($respo);
            $columns = array_column($column_positions, 'column');
            $positions = array_column($column_positions, 'position_order');

            if (array_filter($positions))
                $order = "'".implode("','",$columns)."'";

            #  	form_id 	column 	description 	type 	length 	count 	list 	obl 	fullist 	default_value 	genlist 	api_params
            $cmd = sprintf("SELECT d.description,default_value,\"column\",short_name,array_to_string(list,',') as list,control,count,
                type,genlist,obl,api_params,st_asText(spatial) AS spatial_limit, list_definition,custom_function,column_label
                FROM project_forms_data d
                LEFT JOIN project_forms f ON (d.form_id=f.form_id)
                LEFT JOIN project_metaname m ON (column_name=\"column\" AND m.project_table=f.destination_table)
                WHERE d.form_id=%s AND f.project_table=%s AND 'api'=ANY(form_type)
                ORDER BY idx(Array[%s],\"column\"::text)",quote($value),quote($this->project),$order);

            $form_data = array();
            $form_header = array("login_name"=>$_SESSION['Tname'],"login_email"=>$_SESSION['Tmail']);

            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                echo common_message('error','Wrong form ID or empty form. Empty results returned.');
                return;
            }

            //define permanent_sample_plots any way
            //$form_header['permanent_sample_plots'] = array();
            if ($modules->is_enabled('box_load_selection') and $this->API_VERSION > 2.0) {
                if (isset($_SESSION['Tid']))
                    $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE select_view IN (\'2\',\'3\',\'only-upload\',\'select-upload\') AND p.user_id=%d
                        ORDER BY name',$_SESSION['Tid']);
                else
                    $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                            FROM system.shared_polygons
                            WHERE access IN (\'4\',\'public\') OR (project_table=\'%s\' AND access IN (\'3\',\'project\'))
                            ORDER BY name',$this->project);

                $res3 = pg_query($ID,$cmd);
                $gopt = array();
                while($row3=pg_fetch_assoc($res3)) {
                    $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name'],"geometry"=>$row3['geom']);
                }
                if (count($gopt)) {
                    $form_header['permanent_sample_plots'] = $gopt;
                }
            }

            $form_header['boldyellow'] = array();
            // bold-yellow columns for uploaded data list columns
            if ($modules->is_enabled('bold_yellow') and $this->API_VERSION > 2.0) {
                $params = $modules->get_params('bold_yellow',$this->table);
                if ($params != '')
                    $form_header['boldyellow'] = preg_split('/;/',$params);
            }

            $target_col_list = [];
            $trigger_targets = array(); //target columns of triggered functions
            while($row = pg_fetch_assoc($res)) {

                if ($this->API_VERSION<2.1) {
                    if ($row['api_params']!='') {
                        $row['api_params'] = json_decode($row['api_params']);
                    }
                } elseif ($this->API_VERSION>2.0) {
                    $api_params = array('sticky'=>'off','hidden'=>'off','readonly'=>'off','list_elements_as_buttons'=>'off','once'=>'off');
                    if ($row['api_params']!='') {
                        $ap = json_decode($row['api_params']);
                        foreach ($ap as $ape) {
                            if ($ape=='sticky') $api_params['sticky'] = 'on';
                            elseif ($ape=='hidden') $api_params['hidden'] = 'on';
                            elseif ($ape=='readonly') $api_params['readonly'] = 'on';
                            elseif(preg_match('/^list_elements_as_buttons:?(.+)?/',$ape,$m)) {
                                if (isset($m[1]))
                                    $api_params['list_elements_as_buttons'] = $m[1];
                                else
                                    $api_params['list_elements_as_buttons'] = 'on';
                            }
                            elseif ($ape=='once') $api_params['once'] = 'on';
                        }
                    }
                    $row['api_params'] = $api_params;
                }

                $json_genlist = '';
                if ($row['genlist']!='') {
                    $genlist_def = preg_split('/:/',$row['genlist']);
                    if (count($genlist_def)==2 and $genlist_def[0] == 'SELECT') {
                        //SELECT:table.column
                        if (preg_match('/SELECT:(\w+)\.(\w+):(\w+)/',$row['genlist'],$m)) {
                            //SELECT:table.column:label
                            $bar = sprintf("%s AS bar",$m[2]);
                            $baro = $m[2];
                            if (isset($m[3]) and $m[3]!='') {
                                $bar = sprintf("%s AS bar",$m[3]);
                                $baro = $m[3];
                            }
                            $cmd = sprintf('SELECT DISTINCT %1$s foo, %3$s FROM public.%2$s ORDER BY %4$s LIMIT 5000',$m[2],$m[1],$bar,$baro);
                            $res3 = pg_query($ID,$cmd);
                            $out = array();
                            while ($row3 = pg_fetch_assoc($res3)) {
                                $out[$row3['foo']] = $row3['bar']; # label not handled!!!
                            }
                            $json_genlist = $out;

                        } else {
                            list($table,$column)=explode(".",$genlist_def[1]);
                            $cmd = sprintf('SELECT DISTINCT "%1$s" "%1$s" FROM public."%2$s" ORDER BY %1$s LIMIT 5000',$column,$table);
                            $res3 = pg_query($ID,$cmd);
                            if (pg_num_rows($res3))
                                $x = pg_fetch_all($res3);
                            $json_genlist = array_column($x,$column);
                        }
                    } elseif (count($genlist_def)>1) {
                        //id:label
                        $genlist_elements = preg_split('/,/',$row['genlist']);
                        $out = array();
                        foreach($genlist_elements as $ge) {
                            $ps = preg_split('/:/',$ge);
                            if (count($ps)==2)
                                $out[$ps[0]] = $ps[1];
                        }
                        $json_genlist = $out;

                    } elseif (preg_match('/\./',$row['genlist'])) {
                        // backward compatibility
                        // table.column
                        list($table,$column) = explode(".",$row['genlist']);
                        $cmd = sprintf('SELECT DISTINCT "%1$s" "%1$s" FROM public."%2$s" ORDER BY %1$s LIMIT 5000',$column,$table);
                        $res3 = pg_query($ID,$cmd);
                        if (pg_num_rows($res3))
                            $x = pg_fetch_all($res3);
                        $json_genlist = array_column($x,$column);

                    } else {
                        // normal list
                        $e = preg_split('/,/',$row['genlist']);
                        $result = preg_grep('~' . $_POST['term'] . '~', $e);
                        $json_genlist = $result;
                    }
                    $row['genlist'] = $json_genlist;

                }
                elseif ($row['list_definition']=='' and $row['type']=='list') {
                    $sel = new upload_select_list($row);
                    $prelist = $sel->get_options('array');
                    $json_genlist = array();

                    foreach ($prelist as $pe) {
                        if ($this->API_VERSION < 2.1)
                            $json_genlist[$pe['label'][0]] = $pe['value'];
                        else
                            // prepend whitespace for numeric values, to keep associative array keys
                            if (is_integer($pe['value']))
                                $json_genlist[ " {$pe['value']}" ] = $pe['label'][0];
                            else
                                $json_genlist[$pe['value']] = $pe['label'][0];
                    }

                    $row['list'] = $json_genlist;

                } elseif ($row['list_definition']!='' and $row['type']=='list') {
                    /* Kapcsolt lista példa
                     * {
                            "optionsSchema": "shared",
                            "optionsTable": "lista1",
                            "valueColumn": "lista1_id",
                            "labelColumn": "value",
                            "Function": "select_list"
                            "triggerTargetColumn": [
                                "objectid"
                            ],
                        }
                        {
                            "optionsSchema": "shared",
                            "optionsTable": "lista2",
                            "valueColumn": "value",
                            "filterColumn": "lista1_id",      <- előző listából jön
                            "Function": "select_list",
                            "triggerTargetColumn": [
                                ""
                            ]
                        }
                     */

                    $list_definition = json_decode($row['list_definition'],true);
                    $has_filterColumn = (isset($list_definition['filterColumn']));

                    // normal new list method
                    $sel = new upload_select_list($row, '', $has_filterColumn);

                    //$target_col_list = []; // miért csak egy elemű ez a tömb???
                    if ($has_filterColumn) {
                        $target_col_list[$row['column']] = $sel;
                    }

                    if (isset($list_definition['triggerTargetColumn']) && $list_definition['triggerTargetColumn'][0]!='') {
                        $trigger_targets[$row['column']] = $list_definition['triggerTargetColumn'][0];
                    }

                    $prelist = (!$has_filterColumn) ? $sel->get_options('array') : [];

                    $json_genlist = array();

                    foreach ($prelist as $pe) {
                        if ($this->API_VERSION < 2.1)
                            $json_genlist[$pe['label'][0]] = $pe['value'];
                        else
                            // prepend whitespace for numeric values, to keep associative array keys
                            if (is_integer($pe['value']))
                                $json_genlist[ " {$pe['value']}" ] = $pe['label'][0];
                            else
                                $json_genlist[$pe['value']] = $pe['label'][0];
                    }

                    $triggerTargetColumn = $list_definition['triggerTargetColumn'] ?? [];
                    $Function = $list_definition['Function'] ?? '';
                    $row['list_definition'] = array('multiselect'=>$sel->multiselect,'selected'=>$sel->selected,'triggerTargetColumn'=>$triggerTargetColumn,'Function'=>$Function);
                    $row['list'] = $json_genlist;
                }
                elseif ($row['list_definition']!='' and $row['type']=='autocomplete') {
                    $sel = new upload_select_list($row);
                    $prelist = $sel->get_options('array');
                    $row['genlist'] = array_column($prelist,'value');
                    $row['list_definition'] = null;
                }
                elseif ($row['list_definition']!='' and $row['type']=='autocompletelist') {
                    $sel = new upload_select_list($row);
                    $prelist = $sel->get_options('array');
                    $row['genlist'] = array_column($prelist,'value');
                    $row['list_definition'] = null;
                }


                // shared polygons as permanent_sample_plots
                // "permanent_sample_plots":"[{
                //	"geometry": "POINT(-72.6945975075 42.34444296)",
                //	"name": "Obs. G43,12h"
                //}, { ...

                if ($modules->is_enabled('box_load_selection') and $this->API_VERSION < 2.1) {
                    if ($row['type']=='point' or $row['type']=='polygon' or $row['type']=='line' or $row['type']=='wkt') {
                        if (isset($_SESSION['Tid']))
                            $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                                FROM system.shared_polygons
                                LEFT JOIN system.polygon_users p ON polygon_id=id
                                WHERE select_view IN (\'2\',\'3\',\'only-upload\',\'select-upload\') AND p.user_id=%d
                                ORDER BY name',$_SESSION['Tid']);
                        else
                            $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                                    FROM system.shared_polygons
                                    WHERE access IN (\'4\',\'public\') OR (project_table=\'%s\' AND access IN (\'3\',\'project\'))
                                    ORDER BY name',$this->project);

                        $res3 = pg_query($ID,$cmd);
                        $gopt = array();
                        while($row3=pg_fetch_assoc($res3)) {
                            $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name'],"geometry"=>$row3['geom']);
                        }
                        if (count($gopt)) {
                            $row['permanent_sample_plots'] = $gopt;
                        }
                    }
                }

                // column label
                if ($row['column_label']!='') {
                    $row['short_name'] = $row['column_label'];
                }
                if ($row['short_name']=='')
                    $row['short_name'] = $row['column'];
                else {
                    $translated = $row['short_name'];
                    if (defined($row['short_name'])) $translated = constant($row['short_name']);

                    $row['short_name'] = $translated;
                }

                // default user email
                if ($row['default_value'] == '_email') {
                    if (isset($_SESSION['Tid'])) {
                        $row['default_value'] = $_SESSION['Tmail'];
                    } else{
                        $row['default_value'] = "_input";
                    }
                } elseif ($row['default_value'] == '_login_name') {
                    if (isset($_SESSION['Tid'])) {
                        $row['default_value'] = $_SESSION['Tname'];
                    } else{
                        $row['default_value'] = "_input";
                    }
                }

                if ($this->API_VERSION>2.0) {

                    // magic worlds replaced to null, because Mobile client does not process it just transform it to simple input field
                    //if (in_array($row['default_value'],array('_input','_list','_email','_login_name','_geometry','_datum','_none','_attachment','_autocomplete','_boolean')) and $row['default_value']!='') {
                    if (in_array($row['default_value'],array('_input','_list','_email','_login_name','_geometry','_datum','_none','_attachment','_autocomplete','_boolean'))) {
                        //if ($row['default_value'] != '') {
                            // that means, it has a default value
                            //$row['api_params']['once']="on";
                        //}
                        $row['default_value'] = null;
                    }
                    elseif ($row['default_value'] == '_auto_geometry') {
                        //$row['api_params']['once']="on";
                        $row['default_value'] = null;
                        $row['api_params']['auto_geometry'] = "on";
                    }
                    //elseif ($row['default_value'] != '') {
                    //    $row['api_params']['once'] = "on";
                    //}
                }
                /*
                {
                    "column": "altema",
                    "short_name": "altema",
                    "list": {
                      "1": "abc",
                      "2": "abcd
                    },
                    ...
                */

                $form_data[] = $row;
            }
            $form_cols = array_column($form_data,'column');

            foreach($target_col_list as $tv => $sel) {
                $idx = array_search($tv,$form_cols);
                $row = $form_data[$idx];
                $prelist = $sel->get_options('array');

                $row['filtering_list'] = new stdClass();
                //$row['filtering_list']->list_fkeys = array();
                $row['filtering_list']->list_labels = array_column(array_column($prelist,'label'),0);
                $row['filtering_list']->list_values = array_column($prelist,'value');
                $row['filtering_list']->filter_values = array_column($prelist,'filter');
                $row['filtering_list']->filter_selector = array(array_search($tv,$trigger_targets));

                $form_data[$idx] = $row;
            }

            if ($this->API_VERSION>2.0) {
                echo common_message('ok',array("form_header"=>$form_header,"form_data"=>$form_data));
            } else {
                echo common_message('ok',$form_data);
            }
            return;

        } elseif ($key == 'upload_test' and $this->API_VERSION=='1.0') {
            // NOT USED ANY MORE

            // print out the uploaded data
            // TEST!!!
            $cmd = "SELECT id,datum,comment,project_table,ST_AsText(geometry) as geom,files,error FROM pds_upload_data ORDER BY id DESC";
            $res = pg_query($BID,$cmd);
            echo "datum - comment - project-table - geometry - files - errors<br>";
            while ($row = pg_fetch_assoc($res)) {
                echo $row['id'].'.&nbsp; &nbsp; '.$row['datum'].' - '.$row['comment'].' - '.$row['project_table'].' - '.$row['geom'].' - '.$row['files'].' - '.$row['error']."<br>";
            }
            return;

        } elseif ($key == 'upload' and $this->API_VERSION<1.3) {

            if(!isset($this->project) or $this->project=='') return;

            /* _POST upload into temporary table using API
             * It was the old API upload method, but can be used with some modification and with a new `temp upload` control parameter
             * */
            $response = array("error"=>array(),"result"=>array());

           ##     $user_id = $this->hs_req[0];
           ##     $project_table = $this->hs_req[1];
           ##     $form_id = $this->hs_req[2];

            //Processing uploaded data somehow....

            if ($value == 'mapp') {
                $geoms = preg_replace('/[^POINTLEYG0-9.() ]/','',$_POST['m_geometry']);
                $dates = preg_replace('/[^0-9.-\/ ]/','',$_POST['m_datum']);
                $comms = $_POST['m_comment'];
            }

            if ($value == 'mapp') {
                $file = 'm_file0';
            }

            // nincs ilyen most!
            if ( $value == 'japp') {
                $j=json_decode($value);
                if (!is_object($j)) {
                    $response['error'][] = "$value is not a valid json object!";
                }
            }

            if(!is_array($geoms)) $geoms = (array) $geoms;
            if(!is_array($dates)) $dates = (array) $dates;
            if(!is_array($comms)) $comms = (array) $comms;
            $good_data = 0;
            if ($geoms[0]=='')
                $response['error'][] = "empty geometry!";

            for($i=0;$i<count($geoms);$i++) {
                $file_dest = OB_TMP.$this->hs_req[1]."/".$this->hs_req[0]."/".$this->hs_req[2];
                if (isset($_FILES['m_file0']['tmp_name'][$i]) and $_FILES['m_file0']['tmp_name'][$i]!='')
                    move_upl_files($_FILES['m_file0'],$file_dest);

                if($value=='japp') {
                    $file = "m_file$i";
                }

                $g = $geoms[$i];
                $d = $dates[$i];
                $c = $comms[$i];
                if (!is_wkt($g)){
                    $response['error'][] = "$g is not a valid WKT string";
                    $g = '';
                }
                $files = '{'.join(',',$_FILES[$file]['name']).'}';
                //swap wkt x,y: ST_FlipCoordinates
                //should be optional
                $cmd = sprintf("INSERT INTO pds_upload_data (datum,comment,geometry,error,files,user_id,project_table,form_id) VALUES (%s,%s, ST_FlipCoordinates(ST_GeomFromText(%s, 4326)),%s,%s,%d,'%s',%d)",quote($d),quote($c),quote($g),quote(implode($response['error'])),quote($files),$user_id,$this->project,$form_id);
                $res = pg_query($BID,$cmd);
                if (pg_affected_rows($res)) {
                    $good_data++;
                    $response['result'][] = "Successfully uploaded";
                } else {
                    $response['error'][] = pg_last_error($BID);
                }
            }
            /*if (count($response['error'])) {
                echo implode("\n",$response['error']);
            }
            echo implode("\n",$response['result']);*/
            echo json_encode($response);
            return;

        } elseif ($key == 'get_table_list') {
            /* API_VERSION > 1.3 */
            /* Get list of tables of a project */

            if(!isset($this->project) or $this->project=='') return;

            $cmd = sprintf('SELECT f_main_table as mt FROM header_names WHERE f_table_name=%s',quote($this->project));
            $res = pg_query($BID,$cmd);
            /*$mt = array();
            while ($row = pg_fetch_assoc($res)) {
                $mt[] = preg_replace('/'.$this->project.'_/','',$row['mt']);
            }*/
            echo '{"status":"success","data":';
            echo "[";
            $p = pg_num_rows($res);
            $n = 0;
            //tricky way to fetch large respones
            while ($row = pg_fetch_assoc($res)) {
                $n++;
                echo '"'.preg_replace('/'.$this->project.'_/','',$row['mt']).'"';
                if($n<$p) echo ",";
            }
            echo "]}";
            return;

        } elseif ($key == 'get_report') {
            /* Perform custom query */

            if(!isset($this->project) or $this->project=='') return;
            // lekérdezési eredmény típusa
            if (!isset($this->request['type']))
                $this->request['type'] = 'json';

            $response = reportQuery($value,$this->project);

            $j = json_decode($response);

            if ($j->{'status'}=='error') {
                http_response_code(400);
                echo $response;
                return;
            } else if ($j->{'status'}=='fail') {
                http_response_code(401);
                echo $response;
                return;
            } else {
                $cmd = $j->{'data'};
                $result = pg_query($ID,$cmd);
                if (pg_last_error($ID)) {
                    $errorID = uniqid();
                    echo common_message('fail',str_sql_error,$errorID);
                    log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);

                    return;
                }
                if (!pg_num_rows($result)) {
                    echo common_message('error','Report query has no data');
                    return;
                }

                if ($this->request['type']=='xml') {
                    // collapse with large arrays!!!
                    $this->results = pg_fetch_all($result);
                    // creating object of SimpleXMLElement
                    $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><OBM_data></OBM_data>");
                    // function call to convert array to xml
                    array_to_xml($this->results,$xml);
                    // print out XML
                    header('Content-type: text/xml');
                    print $xml->asXML();
                    return;
                } else {
                    //defult request type is JSON
                    //common message simulation
                    echo '{"status":"success","data":';
                    echo "[";
                    $p = pg_num_rows($result);
                    $n = 0;
                    //tricky way to fetch large respones
                    while ($row = pg_fetch_row($result)) {
                        $n++;
                        echo $row[0];
                        if($n<$p) echo ",";
                    }
                    echo "]}";
                    return;
                }
            }

        } elseif ($key == 'get_project_list') {

            /* Get list of active local projects
             *
             * Listing those project where user is member or has upload rights!!
             * scope=get_project_list&value=2
             *
             * */
            if ($value['project']=='all-projects')
                $a = "";
            elseif ($value['project']!='')
                $a = " AND p.project_table=".quote($value['project']);
            else
                $a = ""; //all-projects

            if ($value['access_options']=='all') {
                // query all projecs regardless if them accessible or not
                // this is the default from 2019.06.25
                $cmd = sprintf("
                    SELECT p.project_table,creation_date,\"Creator\",email,stage,doi,running_date,licence,rum,collection_dates,subjects,project_hash,
                        protocol || '://' || domain || '/' AS project_url,pd.short AS project_description,'-' AS public_mapserv,training,rserver,language
                    FROM projects p
                    LEFT JOIN project_descriptions pd ON (p.project_table = pd.projecttable)
                    LEFT JOIN project_variables pv ON (pv.project_table = p.project_table)
                    WHERE local_project=TRUE AND project_hash!='' AND project_hash IS NOT NULL $a
                    GROUP BY p.project_table,pd.short,training,rserver,language
                    ORDER BY p.project_table");

            } else {
                // $value['access_options']=='accessible'

                if (!isset($_SESSION['Tid'])) {

                    $cmd = sprintf("
                            SELECT p.project_table,creation_date,\"Creator\",email,stage,doi,running_date,licence,rum,collection_dates,subjects,project_hash,
                                '$protocol://'||domain||'/' AS project_url,pd.short AS project_description,'-' AS public_mapserv,training,rserver,language
                            FROM projects p
                            LEFT JOIN project_forms pf ON (p.project_table = pf.project_table)
                            LEFT JOIN project_descriptions pd ON (p.project_table = pd.projecttable)
                            LEFT JOIN project_variables pv ON (pv.project_table = p.project_table)
                            WHERE local_project=TRUE AND project_hash!='' AND project_hash IS NOT NULL AND form_access=0 $a
                            GROUP BY p.project_table,pd.short,training,rserver,language
                            ORDER BY p.project_table");
                } else {
                    //query those projects which are accessible for the given user and those which have public forms

                    $cmd = sprintf("SELECT p.project_table,creation_date,\"Creator\",email,stage,doi,running_date,licence,rum,collection_dates,subjects,project_hash,
                                '$protocol://'||domain||'/' AS project_url,pd.short AS project_description,'-' AS public_mapserv,training,rserver,language
                            FROM projects p
                            LEFT JOIN project_users pu ON (p.project_table = pu.project_table)
                            LEFT JOIN project_forms pf ON (p.project_table = pf.project_table)
                            LEFT JOIN project_descriptions pd ON (p.project_table = pd.projecttable)
                            LEFT JOIN project_variables pv ON (pv.project_table = p.project_table)
                            WHERE local_project=TRUE AND project_hash!='' AND project_hash IS NOT NULL AND (pu.user_id=%d OR form_access=0) $a
                            GROUP BY p.project_table,pd.short,training,rserver,language
                            ORDER BY p.project_table",$_SESSION['Tid']);
                }
            }

            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                echo common_message('error','No local projects found');
                return;
            }

            echo '{"status":"success","data":';
            echo "[";

            $rows = array();
            $projects = array();
            while ($row = pg_fetch_assoc($res)) {
                if (!isset($projects[$row['project_table']]))
                    $projects[$row['project_table']] = $row;
                else {
                    if ($row['language']==$_SESSION['LANG'])
                        $projects[$row['project_table']] = $row;
                }
            }

            $p = count($projects);
            $n = 0;
            foreach ($projects as $row) {
                $n++;
                // API 2.0 compatibility
                if ($row['training'] == 't') $row['game'] = 'on';
                else $row['game'] = 'off';

                if ($row['rserver']== 't') {
                    if (defined('RSERVER_PORT_'.$this->project))
                        $row['rserver_port'] = constant('RSERVER_PORT_'.$this->project);
                    else
                        $row['rserver_port'] = 0;
                } else {
                    $row['rserver_port'] = 0;
                }

                echo json_encode($row,JSON_UNESCAPED_SLASHES);
                if($n<$p) echo ",";
            }
            echo "]}";
            return;
        } elseif ($key == 'get_project_vars') {

            if(!isset($this->project) or $this->project=='') return;

            if (defined('PUBLIC_MAPSERV'))
                $public_mapserv = PUBLIC_MAPSERV;

            // local_vars.php query
            $cmd = sprintf("SELECT st_asText(map_center) AS map_center,map_zoom,click_buffer,zoom_wheel,default_results_view,'$public_mapserv' AS public_mapserv,
                                    default_layer,fixheader,turn_off_layers,subfilters,langs,legend,training,rserver
                            FROM project_variables
                            WHERE project_table='%s'",$this->project);
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                echo common_message('error','No local projects found');
                return;
            }

            echo '{"status":"success","data":';
            echo "[";
                $p = pg_num_rows($res);
                $n = 0;
                while ($row = pg_fetch_assoc($res)) {
                    if ($row['training'] == 't') $row['game'] = 'on';
                    else $row['game'] = 'off';

                    if ($row['rserver']== 't') {
                        $row['rserver_port'] = constant('RSERVER_PORT_'.$this->project);
                    } else {
                        $row['rserver_port'] = 0;
                    }

                    $n++;
                    echo json_encode($row,JSON_UNESCAPED_SLASHES);
                    if($n<$p) echo ",";
                }
            echo "]}";
            return;
        } elseif ($key == 'get_trainings') {

            if(!isset($this->project) or $this->project=='') return;

            $cmd = sprintf("SELECT  id,form_id,html,task_description,enabled,title,qorder,project_table FROM form_training WHERE project_table='%s' ORDER BY qorder",$this->project);
            $res = pg_query($BID,$cmd);
            echo '{"status":"success","data":';
            echo json_encode(pg_fetch_all($res),JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            echo "}";
            return;
        } elseif ($key == 'get_training_questions') {

            if(!isset($this->project) or $this->project=='') return;

            $cmd = sprintf("SELECT qid,training_id,caption,answers,qtype FROM form_training_questions WHERE training_id=%d ORDER BY qid",$value);
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)){
                echo common_message('error','no rows returned');
                return;
            }

            echo '{"status":"success","data":';
            $b = array();
            while ($row = pg_fetch_assoc($res)) {
                $b[] = array("qid"=>$row['qid'],"training_id"=>$row['training_id'],"caption"=>'<h4>'.$row['caption'].'</h4>',"answers"=>json_decode($row['answers'],true),"qtype"=>$row['qtype']);
            }
            echo json_encode($b,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            echo "}";

            return;
        } elseif ($key == 'training_results') {

            if(!isset($this->project) or $this->project=='') return;

            // check user passed the trainings?
            $cmd = sprintf("SELECT form_id FROM form_training WHERE project_table='%s' ORDER BY qorder",$this->project);
            $res = pg_query($BID,$cmd);
            $training_forms = array();
            while ($row = pg_fetch_assoc($res)) {
                $training_forms[$row['form_id']] = -1; //untreated
                $cmd = sprintf("SELECT validation FROM system.uploadings WHERE form_id=%d AND project_table='%s' AND uploader_id=%d",$row['form_id'],$this->project,$_SESSION['Trole_id']);
                $res2 = pg_query($ID,$cmd);
                while ($row2 = pg_fetch_assoc($res2)) {
                    if ($row2['validation']>0.6)
                        $training_forms[$row['form_id']] = 1; //done
                    else
                        $training_forms[$row['form_id']] = 0; //failed
                }
            }
            echo common_message('ok',$training_forms);

            return;
        } elseif ($key == 'training_toplist') {
            if(!isset($this->project) or $this->project=='') return;

            // toplist....
            $cmd = sprintf("SELECT string_agg(form_id::varchar,',') AS form_id FROM form_training WHERE project_table='%s'",$this->project);
            $res = pg_query($BID,$cmd);
            $training_top = array();
            $form_id_list = "";
            while ($row = pg_fetch_assoc($res)) {
                $form_id_list = $row['form_id'];
            }

            if ($value=='nonames') {
                foreach(explode(',',$form_id_list) as $form_id ) {
                    $cmd = sprintf("SELECT sum(validation)/count(validation) AS mean,count(id) AS count,max(validation) AS max FROM system.uploadings WHERE form_id = %d AND project_table='%s'",$form_id,$this->project);
                    $res2 = pg_query($ID,$cmd);
                    while ($row2 = pg_fetch_assoc($res2)) {
                        $training_top[$form_id] = array("mean"=>$row2['mean']*100,"count"=>$row2['count'],"max"=>$row2['max']*100);
                    }
                }
            } else {
                $cmd = sprintf("SELECT sum(validation)/count(validation) AS mean,count(id) AS count,max(validation) AS max,uploader_name FROM system.uploadings WHERE form_id IN (%s) AND project_table='%s' GROUP BY uploader_name",$form_id_list,$this->project);
                $res2 = pg_query($ID,$cmd);
                while ($row2 = pg_fetch_assoc($res2)) {
                        $training_top[$row2['uploader_name']] = array("mean"=>$row2['mean']*100,"count"=>$row2['count'],"max"=>$row2['max']*100);
                }

            }
            echo common_message('ok',$training_top);

            return;
        } elseif ($key == 'pg_user') {

            // create or query pg_user

            if ($modules->is_enabled('create_pg_user') and $this->API_VERSION > 1.0) {
                $username = preg_replace('/[@.-]/','_',$_SESSION['Tmail']);

                $res = pg_query($BID,sprintf('SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = \'%s\'',$username));
                if (pg_num_rows($res)) {
                    echo common_message('ok',array("username"=>$username));
                } else {
                    echo $modules->_include('create_pg_user','create_pg_user');
                }
            } else {
                echo common_message('error',"pg_user module is not enabled");
            }

            return;

        } elseif ($key == 'get_tile_list') {
            # Mit szólsz ha egy api hívással elérhetővé teszem a zip-ek listáját, méretét, dátumát és cimkéjét? Ebből tudnál egy listát csinálni a telefonon amit tud a felhasználó használni....
            #

            if ($value=='') {
                // getlist
                $list = array();

                $tile_services = json_decode(file_get_contents('https://openbiomaps.org/projects/openbiomaps_network/?qtable=openbiomaps_network_tileservers&query_api={%22available%22:%22true%22}&output=json&filename='),true);


                foreach ($tile_services as $serv) {
                    $list[] = $serv['regio'];
                }

                $local_tiles = array();

                foreach ($local_tiles as $serv) {
                    $list[] = $serv['regio'];
                }

                echo json_encode($list);

            } elseif (isset($_POST['list'])) {

                $regios[$new_regio_label] = $regio_extent;

                //store_available_regios($regios);
            }

        } elseif ($key == 'get_tile_zip') {

            $regio = $value;

            # 47.0,22.0_46.0,23.0_12:13_hungary.zip
            # 47.0,22.0_46.0,23.0_11:12_hungary.zip
            #
            #
            $filename = "";
            foreach (glob("zipfiles/*_$value.zip") as $zip) {
                $filename = $zip;
            }

            if (file_exists($filename)) {
                header('Content-Type: application/zip');
                header('Content-Disposition: attachment; filename="'.basename($filename).'"');
                header('Content-Length: ' . filesize($filename));

                flush();
                readfile($filename);
            }
        }

        // return from get functions
        return;
    }
    // SERVICE: PFS
    // project feature servise
    // It is a function
    // $this->request['table'] should be defined and valid
    //
    public function putFunction($API_POST) {
        global $ID,$BID;

        /* New API upload data method
         *
         * */
        #require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

        if (isset($this->request['ic_post_data'])) {
            if ($_FILES['interconnect_post_file']['error'] == UPLOAD_ERR_OK               //checks for errors
                && is_uploaded_file($_FILES['interconnect_post_file']['tmp_name'])) {     //checks that file is uploaded
                //$file = file_get_contents($_FILES['interconnect_post_file']['tmp_name']);
                $file = readCSV($_FILES['interconnect_post_file']['tmp_name']);
                $data_count = count($file);

                // unique upload hash with 'nk' (no-key) flag
                if ($this->request['ic_accept_key']=='' and $this->request['ic_request_key']!='')
                    $hash = "nk_".$this->request['ic_request_key'];
                else if ($this->request['ic_accept_key']!='')
                    $hash = "wk_".$this->request['ic_accept_key'];
                else return;

                // append : false
                // interconnect : true
                if (create_upload_temp(readCSV($_FILES['interconnect_post_file']['tmp_name']),false,true)) {

                    $cmd = sprintf('UPDATE system.temp_index SET table_name=\'upload_%1$s_%2$s_%3$s\' WHERE interconnect=\'t\' AND table_name=\'upload_%1$s_%2$s\'',PROJECTTABLE,session_id(),$hash);

                    if (pg_query($ID,$cmd)) {

                        $cmd = sprintf('ALTER TABLE temporary_tables.upload_%1$s_%2$s RENAME TO "upload_%1$s_%2$s_%3$s"',PROJECTTABLE,session_id(),$hash);
                        $res = pg_query($ID,$cmd);

                        #$cmd = sprintf('INSERT INTO interconnects_register ...');
                        #$res = pg_query($BID,$cmd);
                        // send email alert about new files arrived
                        if (defined("OB_PROJECT_DOMAIN")) {
                            $domain = constant("OB_PROJECT_DOMAIN");
                            $server_email = PROJECTTABLE."@".parse_url("$protocol://".$domain,PHP_URL_HOST);
                            $reply_email = "noreply@".parse_url("$protocol://".$domain,PHP_URL_HOST);
                        } else {
                            $domain = $_SERVER["SERVER_NAME"];
                            $server_email = PROJECTTABLE."@".$domain;
                            $reply_email = "noreply@".$domain;
                        }

                        $cmd = sprintf("SELECT u.email AS email,p.email AS aemail, u.username AS name FROM projects p
                            LEFT JOIN project_users pu ON (pu.project_table=p.project_table)
                            LEFT JOIN users u ON (user_id=u.id)
                            WHERE p.project_table='%s' AND user_status::varchar IN ('2','master') AND receive_mails!=0",PROJECTTABLE);
                        $res = pg_query($BID,$cmd);
                        $emails = array();
                        $names = array();
                        while ( $row = pg_fetch_assoc($res)) {
                            $names[] = $row['name'];
                            $emails[] = $row['email'];
                        }

                        $emails = array_unique($emails);

                        $Subject = sprintf("Shared data from %s / %s database to %s",$this->request['ic_slave_server'],$this->request['ic_slave_project'],PROJECTTABLE);
                        $Title = 'Hi '.implode(', ',$names).'!';

                        $Message = sprintf("There are %d record from %s database to import<br>",$data_count,$this->request['ic_slave_project']);
                        $Message .= sprintf("Go to <a href='https://%s/upload/'>upload page</a> and choose a file upload form to add these data to your database.",URL);

                        $m = mail_to($emails,$Subject,"$server_email","$reply_email",$Title,$Message,"multipart");

                    }

                    //debug('interconnect link: '.PROJECTTABLE.'_'.session_id(),__FILE__,__LINE__);
                    echo common_message('ok','processed');
                } else {
                    echo common_message('error','upload processing failed');
                }
            }

            echo common_message('error','not processed');
            return;
        }


        if(!isset($this->request['form_id']) or $this->request['form_id']=='') return;


        if (isset($metadata) and isset($metadata['form_version']) ) {
            $last_modification_of_form = 0;
            $cmd = sprintf("SELECT round(extract(epoch from last_mod)) as last_mod FROM project_forms WHERE form_id=%s AND project_table=%s",quote($this->request['form_id']),quote($this->project));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $last_modification_of_form = $row['last_mod'];
            }
            if ($metadata['form_version'] < $last_modification_of_form) {
                echo common_message('error','This form is out of date, if you get an error, the upload may be completed on the server.');
            }
        }

        require_once(getenv('OB_LIB_DIR').'modules_class.php');
        require_once(getenv('OB_LIB_DIR').'upload_funcs.php');

        $_SESSION['api_warnings'] = array();
        $up = new upload_process();
        $destination_table = $up->access_check($this->request['form_id'],'api');

        if ( !$destination_table ) {
            http_response_code(401);
            echo common_message('error',str_access_denied);
            return;
        }
        $_SESSION['current_query_table'] = $destination_table;

        # facebook like bulk upload
        if (isset($_POST['batch'])) {
            $batch = json_decode($_POST['batch'],TRUE);
            $bacth_files = array();
            $sheetData = array();
            foreach ($batch as $line) {
                $sheetData[] = $line['data'][0];

                //$spf = preg_split('/,/',$line['attached_files']);
                $batch_files[] = $line['attached_files'];
            }
            if(!create_upload_temp($sheetData)) {
                echo common_message('error','Read file error, couldn\'t create temporary table for upload form.');
                return;
            }
        }

        // itt csak az attached fileok feldolgozása kell!!!
        // attached_files
        if (isset($_FILES) and count($_FILES)) {

            # only attachment uploading
            # returning saved files names
            if (isset($_FILES['attached_files'])) {
                // R client compatibility
                $r = move_upl_files($_FILES['attached_files'],getenv('PROJECT_DIR').'/attached_files');
                $rr = json_decode($r);
                $rr->{'file_names'} = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});
                //echo json_encode($rr);
                //returning with uploaded file reference name
                echo common_message('ok',$rr->{'file_names'});
                return;

            # data file uploading
            # create sheetData from file content
            } elseif (isset($_FILES['file'])) {

                $fp = new file_process();

                $log = $fp->fprocess('','');
                if (is_array($log) and isset($log['comment'])) {
                    //$upload_comment = $log['comment'];
                    if (create_upload_temp($fp->sheetData))
                        $_SESSION['theader'] = $fp->theader;
                    else {
                        log_action('Read file error, couldn\'t create temporary table for upload form.',__FILE__,__LINE__);
                        echo common_message('error','Read file error, couldn\'t create temporary table for upload form.');
                        return;
                    }
                } else {
                    echo common_message('error',"File upload failed: $log");
                    unset($_SESSION['theader']);
                    return;
                }
            # packed file: data + attachments (multilines)
            # superpack
            } elseif (isset($_FILES['multipacked_lines'])) {
                # unpack super pack
                #
                $zipdir = sprintf('%s/%s/zipdir/',OB_TMP,session_id());
                if (!unpack_zip($_FILES['multipacked_lines']['tmp_name'],$zipdir)) {
                    echo common_message('error','Failed unpacking files');
                    return;
                }

                $list = glob("$zipdir/*.zip");

                if (isset($_POST['header'])) $_SESSION['theader'] = json_decode($_POST['header'],true);
                $append = false;
                foreach ($list as $zip) {
                    $F = array('tmp_name'=>$zip,'name'=>basename($zip),'type'=>'','error'=>UPLOAD_ERR_OK);
                    if (packed_data_line_process($F,$append) != 'done') {
                        echo common_message('error','Failed to processing packed data line.');
                        return;
                    }
                    $append = true;
                }
            # packed file: data + attachments (only one data line)
            # single pack
            } elseif (isset($_FILES['packed_line'])) {
                // processing one line zipped data+attachments

                //header should be ordered as the file content processed if no other information in the file:
                //obm_gemetry,obm_files_id,species,datum,...?
                if (isset($_POST['header'])) $_SESSION['theader'] = json_decode($_POST['header'],true);
                if (packed_data_line_process($_FILES['packed_line']) != 'done') {
                    echo common_message('error','Failed to processing packed data line.');
                    return;
                }

            } elseif (isset($batch_files) and count($batch_files)) {
                $batch_line = 1;
                foreach ($batch_files as $fu) {
                    $fuua = preg_split ('/,/',$fu);
                    $uploaded_files = array();
                    foreach($fuua as $fuuae) {
                        if (isset($_FILES[$fuuae])) {
                            $r = move_upl_files($_FILES[$fuuae],getenv('PROJECT_DIR').'/attached_files');
                            $rr = json_decode($r);
                            $e = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});
                            // egyszerre it csak 1 fájl jön
                            $uploaded_files[] = $e[0];
                        }
                    }
                    $row = get_sheet_row($batch_line);
                    $jdata = json_decode($row['data'],true);
                    $jdata['obm_files_id'] = implode(',',$uploaded_files);
                    $res = update_sheet_row($batch_line,$jdata);

                    $batch_line++;
                }
                if ($API_POST['put_header']!='') {
                    $header_names = json_decode($API_POST['put_header'],TRUE);
                    if (!in_array('obm_files_id',$header_names)) {
                        $header_names[] = 'obm_files_id';
                        $API_POST['put_header'] = json_encode($header_names);
                    }
                }

            # unexpected files
            } else {
                http_response_code(422);
                echo common_message('error','Non handled file settings');
                return;
            }

        }

        // no files, only data
        if (isset($API_POST['put_data']) and (!isset($_FILES) or count($_FILES)==0)) {
            if (!create_upload_temp(json_decode($API_POST['put_data'],TRUE))) {
                #debug(session_id(),__FILE__,__LINE__);
                #debug($_POST,__FILE__,__LINE__);
                http_response_code(411);
                log_action('Read file error, couldn\'t create temporary table for upload form.',__FILE__,__LINE__);
                echo common_message('error','Read file error, couldn\'t create temporary table for upload form.');
                return;
            }

            # read multiple lines:
            # [{
            #	"species": "Tringa totanus",
            #	"brood_id": "512",
            #	"ring_code": "AWBO"
            # }, {
            #	"species": "Tringa totanus",
            #	"brood_id": "513",
            #	"ring_code": "BYWY"
            # }]
            #
            #
            #Some check of data structure??
        }

        #
        # description
        # Adat felvitel fájlból: 'teszt2.csv'

        # form_page_id
        # 58

        # form_page_type
        # file

        # srid
        # 4326

        # upload_table_post
        # {"id":["faj","the_geom","szamossag","hely","egyedszam","gyujto","adatkozlo"],
        #  "header":["faj","the_geom","szamossag",null,null,null,null,null,null,null,"hely","egyedszam","gyujto","adatkozlo"],
        #  "default_data":["úttest","","pompom","miki"],
        #  "rows":[[1,2]]}
        #
        #  Az "id" az adatbázis oszlopok amikbe beszúrunk adatokat,
        #  A "header" a feltöltött adattábla oszlopai sorban a kiválasztott/hozzárándelt oszlop nevekkel

        # Honnan jöjjön a header?
        # A "value" a 'condition' értéke vagy külön küldhető legyen, hogy ne kelljen mindig elküldeni pl?
        # OBM_get('api_form_headers')
        # OBM_set('api_form_header','koszti')
        # OBM_put('put_data','header',form-id,file)
        #
        #$j = $value = array('header'=>array('faj','hely','egyedszam'),'default_data'=>array(),'rows'=>array());


        // session theader is the column names of uploaded data
        // the database column association can be given from the put_header array
        $_SESSION['theader'] = get_sheet_header();

        if ($API_POST['put_header']!='') {
            $header = json_decode($API_POST['put_header'],TRUE);

            if (isset($_POST['metadata']))
                $metadata = json_decode($_POST['metadata'],TRUE);

            //empty header names given
            if (!count($header)) {
                if ( isset($metadata) and isset($metadata['observation_list_start']) and isset($metadata['observation_list_end']) and
                        $metadata['observation_list_start']!='' and $metadata['observation_list_end']!='') {
                    $header = array();
                } else {
                    log_action('Header is empty or not well formed',__FILE__,__LINE__);
                    echo common_message('error','Header is empty or not well formed');
                    return;
                }
            }
            
            if (isset($metadata) and isset($metadata['observation_list_id'])) {
                $temp_table = obslist_table($destination_table,'_obm_obsl');
                $up->temp_destination = 'temporary_tables.'.$temp_table;
                if (!$temp_table) {
                    log_action('Failed to create .._obm_obsl table',__FILE__,__LINE__);
                    echo common_message('error','Failed to create .._obm_obsl table');
                    return;        
                }
            }
        } else {
            // database column names equal to the sent columns' names and therefore additional header names not given
            $header = $_SESSION['theader'];
        }

        //get default data JSON
        if (isset($_POST['default_values'])) {
            $default_data = array_values(json_decode($_POST['default_values'],TRUE));
            $header = array_merge($header,array_keys(json_decode($_POST['default_values'],TRUE)));
        } else
            $default_data = array();

        // ez a tömb a webes felülettel való kompatibilitás miatt kell
        $j = array('header'=>$header,'default_data'=>$default_data,'rows'=>array());
        $j = json_encode($j);

        $_POST['upload_table_post'] = $j;
        $_POST['srid'] = 4326;
        $_POST['description'] = 'API upload';

        $response = $up->new_upload($destination_table);        // true | false
        $j = json_decode($up->messages[0]);                     // processing first message

        if ($j->{'status'}=='fail') {
            http_response_code(400);
            echo $up->messages[0];
            return;
        }

        // if failed to upload check form version, create a temp upload
        if ($response===false) {
            // In this case
            // $j->{'status'} == error

            $last_modification_of_form = 0;
            $cmd = sprintf("SELECT round(extract(epoch from last_mod)) as last_mod FROM project_forms WHERE form_id=%s AND project_table=%s",quote($this->request['form_id']),quote($this->project));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $last_modification_of_form = $row['last_mod'];
            }

            $metadata = json_decode($_POST['metadata'],TRUE);

            $hash = crc32(session_id().$this->request['form_id'].'api'.microtime());
            if (isset($metadata) and isset($metadata['form_version']) and $metadata['form_version'] < $last_modification_of_form) {
                
                
                $temp_table = obslist_table($destination_table,'_obm_upl');
                if (!$temp_table) {
                    log_action('Failed to create .._obm_upl table',__FILE__,__LINE__);
                    echo common_message('error','Failed to create .._obm_upl table');
                    return;        
                }

                $up->temp_destination = 'temporary_tables.'.$temp_table;
                $up->skip_checks = true;
                $up->errors = array();
                $up->soft_errors = array();
                $up->messages = array();
                $response = $up->new_upload($destination_table);        // true | false
                $j = json_decode($up->messages[0]);                     // processing first message

                if ($j->{'status'}=='fail') {
                    http_response_code(400);
                    echo $up->messages[0];
                    return;
                }
                elseif ($j->{'status'}=='error') {
                    http_response_code(422);
                    //debug($up->messages,__FILE__,__LINE__);
                    echo $up->messages[0];
                    return;
                } 
                elseif ($j->{'status'}=='success') {
                    // successful upload
                    http_response_code(200);
                    echo $up->messages[0];
                } 
                else {
                    // no status message - big problem
                    http_response_code(400);
                    echo common_message('error','Server error');
                }

                /* NULLFORM SOLUTION
                 * $header = '';
                $data = '';
                # nullform (id:???) should be created to use this imports
                # nullform should contains all columns!
                # - accessible only for admins
                #

                $nullform_id = getNullFormId($destination_table,false);
                if ($nullform_id) {
                    $cmd = sprintf("INSERT INTO system.imports (project_table,user_id,ref,datum,header,data,form_type,form_id,file)
                        VALUES ('%s','%d','%s',NOW(),'%s','%s','api',%d,'%s')",
                        PROJECTTABLE,$_SESSION['Tid'],$hash,$header,$data,$nullform_id,"upload_".PROJECTTABLE."_".session_id());
                    $res = pg_query($ID,$cmd);
                    if (pg_affected_rows($res)) {

                        $cmd = sprintf('UPDATE system.temp_index SET table_name=\'upload_%1$s_%2$s_%3$s\' WHERE table_name=\'upload_%1$s_%2$s\'',PROJECTTABLE,session_id(),$hash);
                        if (pg_query($ID,$cmd)) {
                            $cmd = sprintf('ALTER TABLE temporary_tables.upload_%1$s_%2$s RENAME TO "upload_%1$s_%2$s_%3$s"',PROJECTTABLE,session_id(),$hash);
                            $res = pg_query($ID,$cmd);
                            if (!pg_last_error($ID)) {
                                // Az adatok feltöltődtek, de ```talán``` az űrlap verziójának eltérése miatt nem fejezhető be. 
                                // Próbálja meg befejezni a szerveren a „megszakított importálás” funkció használatával a nullform űrlappal.
                                http_response_code(200);
                                echo common_message('ok','Data uploaded, but cannot be finished due to form version mismatch. Try to finish it on the server using "interrupted imports" function with the nullform.');
                            } else {
                                // nem sikerült átnevezni a temp táblát emiatt n
                                http_response_code(422);
                                log_action(sprintf('Failed to rename temporary_tables.upload_%1$s_%2$s',PROJECTTABLE,session_id()),__FILE__,__LINE__);
                                echo $up->messages[0];
                            }
                        } else {
                            // faild to create import: sending error message
                            http_response_code(422);
                            echo $up->messages[0];
                        }
                    } else {

                        http_response_code(422);
                        echo $up->messages[0];
                    }
                } else {

                    http_response_code(422);
                    log_action('Could not create nullform.',__FILE__,__LINE__);
                    echo $up->messages[0];
                } */
            } else {
                // no metadata or no form-version or form_version is up-to-date: sending error message
                http_response_code(422);
                echo $up->messages[0];
            }
        } 
        elseif ($j->{'status'}=='success') {
            // successful upload
            http_response_code(200);
            echo $up->messages[0];
        }
        else {
            // no status message - big problem
            http_response_code(400);
            echo common_message('error','Server error');
        }
        return;
    }
}

?>
