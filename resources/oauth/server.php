<?php
require_once('/etc/openbiomaps/system_vars.php.inc');

/* Create a sylog message
 * log_action function
 * */
function auth_debug($message,$file=NULL,$line=NULL) {
    if(is_array($message)) {
        $message = json_encode($message);
    }
    elseif(is_object($message)) {
        $message = json_encode($message);
    }
    $custom_log = "/var/log/openbiomaps.log";

    $pointer = "";
    if($file!=NULL or $line!=NULL)
        $pointer = " $file $line";

    $path_parts = pathinfo(__FILE__);
    if (is_writable($custom_log)) {
        $date = date('M j h:i:s'); 
        error_log("[OBM_".$path_parts['dirname']."] $date$pointer: $message\n", 3, "/var/log/openbiomaps.log");
    } else {
        openlog("[OBM_".$path_parts['dirname']."]", 0, LOG_LOCAL0);
        syslog(LOG_WARNING,$pointer.$message);
        closelog();
    }
}

$dsn      = 'pgsql:dbname='.biomapsdb_name.';host='.biomapsdb_host.';port='.POSTGRES_PORT.'';
$username = biomapsdb_user;
$password = biomapsdb_pass;

// error reporting (this is a demo, after all!)
//ini_set('display_errors',1);error_reporting(E_ALL);

// Autoloading (composer is preferred, but for this example let's just do this)
require_once('oauth2-server-php/src/OAuth2/Autoloader.php');
OAuth2\Autoloader::register();

// $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
$storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));

// Pass a storage object or array of storage objects to the OAuth2 server class
$server = new OAuth2\Server($storage, array('allow_implicit' => true));

// Add the "Client Credentials" grant type (it is the simplest of the grant types)
// $server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage , array('allow_credentials_in_request_body' => true)));

// Add the "Authorization Code" grant type (this is where the oauth magic happens)
$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

$server->addGrantType(new OAuth2\GrantType\UserCredentials($storage));

$server->addGrantType(new OAuth2\GrantType\RefreshToken($storage,array('always_issue_new_refresh_token' => true, 'refresh_token_lifetime' => 2419200)));

if (isset($_REQUEST['username'])) {
    $_REQUEST['username'] = strtolower(trim($_REQUEST['username'])); // as no email with spaces we can safely remove exrta spaces which is useful for mobil auth....
    $_POST['username'] = strtolower(trim($_REQUEST['username'])); // as no email with spaces we can safely remove exrta spaces which is useful for mobil auth....

    // add default scope when login if it wan not added
    if (isset($_REQUEST['scope']) and !preg_match('/get_project_list/',$_REQUEST['scope'])) {
        $_REQUEST['scope'] .= " get_project_list";
        $_POST['scope'] .= " get_project_list";
    }
}
    

//static scopes - sorry, doctrine is too comlicated...
#$defaultScope = 'forms';
$defaultScope = array(
    'get_project_list'    
);
$supportedScopes = array(
    'get_project_list',
    'get_specieslist',
    'get_form_list',
    'get_form_data',
    'get_profile',
    'get_data',
    'get_history',
    'get_report',
    'set_rules',
    'put_data',
    'webprofile',
    'get_tables',
    'get_trainings',
    'get_training_questions',
    'training_results',
    'training_toplist',
    'get_mydata_rows',
    'pg_user'
);
$memory = new OAuth2\Storage\Memory(array(
  'default_scope' => $defaultScope,
  'supported_scopes' => $supportedScopes
));
$scopeUtil = new OAuth2\Scope($memory);
$server->setScopeUtil($scopeUtil);

//$server->setConfig('enforce_state', false);
?>
