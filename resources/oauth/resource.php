<?php

// include our OAuth2 Server object
require_once __DIR__.'/server.php';

if (isset($_REQUEST['scope']))
    $scopeRequired = $_REQUEST['scope'];
else
    $scopeRequired = ''; //default scope

$value = '';
$table = '';
$form_id = '';
$put_data = '';
$put_header = '';
$soft_error = array();
$ignore_warns = 0;

if (isset($_REQUEST['value']))
    $value = $_REQUEST['value'];

if (isset($_REQUEST['table']))
    $table = $_REQUEST['table'];

if (isset($_REQUEST['project']))
    $project = $_REQUEST['project'];

if (isset($_REQUEST['form_id']))
    $form_id = $_REQUEST['form_id'];

if (isset($_REQUEST['data']))
    $put_data = $_REQUEST['data'];

if (isset($_REQUEST['header']))
    $put_header = $_REQUEST['header'];

if (isset($_REQUEST['soft_error']))
    $soft_error = json_decode($_REQUEST['soft_error'],true);

if (isset($_REQUEST['ignore_warnings']))
    $ignore_warns = 1;

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
    
    // non authenticated zone .......................................................
    // .............................................................................>
    if ($scopeRequired == 'get_project_list') {

        if (isset($_REQUEST['only-project']))
            $project = $_REQUEST['only-project'];
        else
            $project = 'all-projects';

        if (isset($_REQUEST['accessible']))
            $access_options = $_REQUEST['accessible'];
        else
            $access_options = 'all';

        $API_PARAMS = array('service'=>'PFS','get_project_list'=>array("project"=>$project,'access_options'=>$access_options));
        return;
    
    } elseif ($scopeRequired == 'get_project_vars') {
        $API_PARAMS = array('service'=>'PFS','get_project_vars'=>1,'project'=>$project);
        return;
    
    } elseif ($scopeRequired == 'get_form_list') {
        $API_PARAMS = array('service'=>'PFS','get_form_list'=>1,'table'=>$table);
        return;

    } elseif ($scopeRequired == 'get_form_data') {
        $API_PARAMS = array('service'=>'PFS','get_form_data'=>$value,'table'=>$table);
        return;

    } elseif ($scopeRequired == 'put_data') {
        $API_PARAMS = array('service'=>'PFS','ignore_warns'=>$ignore_warns,'form_id'=>$form_id,'put_header'=>$put_header,'put_data'=>$put_data,'soft_error'=>$soft_error,'table'=>$table);
        return;
        
    } elseif ($scopeRequired == 'interconnect_post_file_with_key') {
        $API_PARAMS = array('service'=>'PFS','put_data'=>1,'ic_post_data'=>1,'ic_accept_key'=>$_REQUEST['accept_key'],'ic_request_key'=>$_REQUEST['request_key'],'ic_slave_server'=>$_REQUEST['slave_server'],'ic_slave_project'=>$_REQUEST['slave_project']);
        return;
    } else {
        $server->getResponse()->send();
        die;
    }

}
// protected zone ...............................................................
// .............................................................................>

if ($scopeRequired == 'webprofile') {
    // web application request
    exit;
}

// PDS request
// PROJECT_DIR is defined in pds.php
if (!defined('CALL')) {
    header('Content-type:application/json;charset=utf-8');
    exit( "{\"status\":\"error\",\"message\":\"Use pds api\"}" );
}

// access token
$code = $server->getAccessTokenData($request, $response);

// scope based function parameters
if ($scopeRequired == 'get_profile') {

    $API_PARAMS = array('service'=>'PFS','get_profile'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_form_list') {

    $API_PARAMS = array('service'=>'PFS','get_form_list'=>1,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_form_data') {

    $API_PARAMS = array('service'=>'PFS','get_form_data'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_report') {
    
    $API_PARAMS = array('service'=>'PFS','get_report'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_tables') {
    
    $API_PARAMS = array('service'=>'PFS','get_table_list'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_specieslist') {
    
    $API_PARAMS = array('service'=>'PFS','get_specieslist'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_data') {

    $API_PARAMS = array('service'=>'PRS','get_data_rows'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_history') {

    $API_PARAMS = array('service'=>'PRS','get_history_rows'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'set_rules') {

    $API_PARAMS = array('service'=>'PFS','set_rules'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'put_data') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'ignore_warns'=>$ignore_warns,'form_id'=>$form_id,'put_header'=>$put_header,'put_data'=>$put_data,'soft_error'=>$soft_error,'table'=>$table);

} elseif ($scopeRequired == 'get_trainings') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_trainings'=>1);

} elseif ($scopeRequired == 'get_training_questions') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_training_questions'=>$value);

} elseif ($scopeRequired == 'training_results') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'training_results'=>$value);

} elseif ($scopeRequired == 'training_toplist') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'training_toplist'=>$value);

} elseif ($scopeRequired == 'get_project_list') {

    if (isset($_REQUEST['only-project']))
        $project = $_REQUEST['only-project'];
    else
        $project = 'all-projects';

    if (isset($_REQUEST['accessible']))
        $access_options = $_REQUEST['accessible'];
    else
        $access_options = 'all';

    $API_PARAMS = array('service'=>'PFS','get_project_list'=>array("project"=>$project,'access_options'=>$access_options),'access_token'=>$code['access_token']);
} elseif ($scopeRequired == 'get_mydata_rows') {
    
    $API_PARAMS = array('service'=>'PFS','get_mydata_rows'=>$value,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'pg_user') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'pg_user'=>$value);

} elseif ($scopeRequired == 'get_tile_list') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_tile_list'=>$value);

} elseif ($scopeRequired == 'get_tile_zip') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_tile_zip'=>$value);

}

?>
