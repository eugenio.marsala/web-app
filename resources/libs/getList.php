<?php
/* AJAX CALL for jquery UI autocomplete function
 * Parameters: type, term, (output)
 *      type: taxon - others not implemented yet
 *      term: string
 *      output: if specified not specified output will formatted for jquery UI autocomplete function, otherwise something else...
 *
 * It read letters and
 * returning taxon names as a JSON array
 *      [
 *              {"meta":"Tringaflavipes","label":"Tringa flavipes","value":"Tringa flavipes","id":"1087"},
 *              {"meta":"Tringatotanus","label":"Tringa totanus","value":"Tringa totanus","id":"1088"}
 *      ]
 *
 *  It can works as a standalone listing module for others if output is defines if output is definedd
 *
 * */

session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");


if (!isset($_REQUEST['type'])) exit;

if (in_array($_REQUEST['type'],['taxon','search']) && isset($_REQUEST['term'])) {

    $term = preg_replace('/\s+/','',$_REQUEST['term']);

    print trgm_search($_REQUEST['type'],$term);

}

function trgm_search($type, $string, $limit = 20, $dist = 0.9, $reorderLevel = 4) {
    global $ID;

    # pg_trgm search with limit
    $subject = ($type == 'taxon') ? '' : ',subject';
    $misspelled_excluded = ($type == 'taxon') ? '' : sprintf(" WHERE data_table = %s AND status != 'misspelled'", quote($_SESSION['current_query_table']));
    $cmd = sprintf('SELECT *
    FROM (
        SELECT DISTINCT ON (word%6$s) meta,word as name, word <->\'%1$s\' AS dist,%2$s_id,frequency%6$s
        FROM %3$s_%2$s %7$s
    ) p
    WHERE dist < %4$s
    ORDER BY dist,name
    LIMIT %5$s;',$string, $type, PROJECTTABLE, $dist, $limit,$subject,$misspelled_excluded);

    $result = pg_query($ID,$cmd);
    if (!pg_num_rows($result)) {
        return json_encode(array());
        exit;
    }

    $j = array();

    $all_rows = pg_fetch_all($result);

    if ($reorderLevel >= 1) {
        # ADVANCED REORDERING
        # Stage 1:
        #   szó eleji pontos egyezések kiszedése az eredmény listából
        $matches = preg_grep("/^$string/i", array_column($all_rows,'name'));
        $s = array();
        foreach($matches as $m) {
            $k = array_search($m, array_column($all_rows,'name'));
            $s[] = array_splice($all_rows,$k,1);
        }
        $begin_match = array();
        foreach($s as $se) {
            $begin_match[] = $se[0];
        }
        array_multisort(array_column($begin_match,'name'), SORT_ASC, $begin_match);
        $all_rows_reordered = $begin_match;

    }
    if ($reorderLevel >= 2) {
        # Stage 2:
        #   szó közbeni egyezések kiszedése az eredmény listából
        $matches = preg_grep("/$string/i", array_column($all_rows,'name'));
        $s = array();
        foreach($matches as $m) {
            $k = array_search($m, array_column($all_rows,'name'));
            $s[] = array_splice($all_rows,$k,1);
        }
        $middle_match = array();
        foreach($s as $se) {
            $middle_match[] = $se[0];
        }
        array_multisort(array_column($middle_match,'name'), SORT_ASC, $middle_match);

        # create all_rows_array again
        $all_rows_reordered = array_merge($begin_match,$middle_match,$all_rows);

    }
    if ($reorderLevel >= 3) {
        # Stage 3:
        #   Search for maximum frequency, pick up this element, put at the first position
        $frequency = array_column($all_rows_reordered,'frequency');
        $max = max($frequency);
        if ($max > 0) {
            $mf_idx = array_search($max,$frequency);
            $spliced_element = array_splice($all_rows_reordered,$mf_idx,1);
            // put at the first position of the list
            array_unshift($all_rows_reordered,$spliced_element[0]);
        }

        //$k = array_search($string, array_column($row, 'name'));
        //$frow = array_filter($row, function($el) use ($string) {
        //    return ( strpos($el['text'], $string) !== false );
        //});

    }

    for ($i = 0, $l = count($all_rows_reordered);  $i < $l; $i++) {
        $all_rows_reordered[$i]['idx'] = $i;
    }

    if ($subject)
        array_multisort(array_column($all_rows_reordered,'subject'), SORT_DESC, array_column($all_rows_reordered,'idx'), SORT_ASC, $all_rows_reordered);


    # renaming assoc array according to the needs of autocomplete js
    foreach ($all_rows_reordered as $row) {
        $x = array();
        $x['meta'] = $row['meta'];
        $x['label']= $row['name'];
        $x['value']= $row['name'];
        if ($subject)
            $x['category'] = $row['subject'];
        $x['id']   = $row["{$type}_id"];
        $j[] = $x;
    }
    if (isset($_REQUEST['output'])) {
        # non jqueryUI autocomplete output
        return json_encode($all_rows_reordered);

    } else {
        //standard output for jquery autocomplete js
        return json_encode($j);
    }
}

function category_sort($a, $b) {
    return ($a['subject'] <= $b['subject']) ? -1 : 1;
}

/*
elseif ($_GET['type'] === 'observer') {

    // status: 0 - nothing, 1 - accepted, 2 - misspelled
    $term = quote("{$_GET['term']}%");
    $cmd = sprintf('SELECT concat_ws(\' - \',word,institute) as value, observer_id as id FROM %s_observers WHERE word LIKE %s AND status IN (0,1) ORDER BY frequency DESC;',PROJECTTABLE,$term);


    if (pg_send_query($ID, $cmd)) {
        $res = pg_get_result($ID);
        $state = pg_result_error($res);
        if ($state != '') {
            log_action($state,__FILE__,__LINE__);
            return;
        }
        $observers = [];
        while ($row = pg_fetch_assoc($res)) {
            $observers[] = $row;
        }
        echo json_encode($observers);
    }
}
// elsif ($_REQUEST['type'] == '...') {}


exit;
 */
/*
Catalogue of Life query

WITH RECURSIVE q AS
        (
        SELECT  h, 1 AS level, ARRAY[taxon_id] AS breadcrumb
        FROM    taxon h
        WHERE   parent_id = 22
        UNION ALL
        SELECT  hi, q.level + 1 AS level, breadcrumb || taxon_id
        FROM    q
        JOIN    taxon hi
        ON      hi.parent_id = (q.h).taxon_id
        )
SELECT  REPEAT('  ', level) || (q.h).taxon_id,
        (q.h).parent_id,
        (q.h).taxon_id,
        level,
        breadcrumb::VARCHAR AS path
FROM    q
ORDER BY
        breadcrumb

 * */
?>
