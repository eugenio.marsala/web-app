<?php
/* Ajax function
 * IMPROVEMENTS NEEDED!!!
 *
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

track_visitors('evaluation');

pg_query($ID,'SET search_path TO system,public');
pg_query($GID,'SET search_path TO system,public');

#$_SESSION['allow-validation'] = 1;
#if (!isset($_SESSION['allow-validation'])) return;

// ez a target megoldás hekkelhető...
$target         = $_POST['target'];
$post_id        = $_POST['id'];
$value          = $_POST['value'];
$post_comment   = $_POST['comment'];
$rel_id         = "NULL";

!isset($_SESSION['Tid'])  ? $mfid = "NULL":  $mfid = $_SESSION['Tid'];
!isset($_SESSION['Tname'])? $mfname = "NULL":$mfname = $_SESSION['Tname'];

// Eval types
$acce = array('+','-','ok','accept','refuse','m','z','p');

$url = URL;
/*$cmd = sprintf("SELECT domain FROM projects WHERE project_table='%s'",PROJECTTABLE);
$res = pg_query($BID,$cmd);
if (pg_num_rows($res)) {
    $row = pg_fetch_assoc($res);
    $url = $row['domain'];
}*/

$eval_table = PROJECTTABLE;
if (isset($_SESSION['current_query_table']))
    $eval_table = $_SESSION['current_query_table'];

// Evaluation of data
if (in_array($value,$acce)) {
    $evaluation_type = "";
    if ($target=='data') {
        $db = $GID;
        $table = $eval_table;
        //unset($_SESSION["slide"][slide."$post_id"]);
        $cmd = sprintf("SELECT uploader_id FROM %s LEFT JOIN system.uploadings ON (obm_uploading_id=id) WHERE obm_id=%d",$eval_table,preg_replace('/[^0-9]+/','',$post_id));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $userdata = new userdata($row['uploader_id'],'userid');
            $user_id = $userdata->get_userid();
            $mail_text = "A <a href='http://$url/index.php?data&id=$post_id&table=$eval_table'>data</a> you have uploaded in ".$eval_table." has been evaluated.";
        }
        $evaluation_type = t(str_data_evaluation);
    } elseif ($target == 'upload') {
        // mode levelben kell lenni a data vote-hoz
        //if (!rst('mod')) exit;
        $db = $GID;
        $table = 'uploadings';

        $cmd = sprintf("SELECT uploader_id FROM system.uploadings WHERE id=%d",preg_replace('/[^0-9]+/','',$post_id));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $userdata = new userdata($row['uploader_id'],'userid');
            $user_id = $userdata->get_userid();
            $mail_text = "An <a href='http://$url/index.php?history=upload&id=$post_id'>upload</a> to ".$eval_table." has been evaluated.";
        }
        $evaluation_type = t(str_uploading_evaluation);
    } elseif ($target == 'user') {
        if(!isset($_SESSION['Tid'])) exit;
        $db = $BID;
        $table = 'users';

        $cmd = sprintf("SELECT user FROM users WHERE id=%d",preg_replace('/[^0-9]+/','',$post_id));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $uid = $row['user'];
        }
        $mail_text = "Your contribution to <a href='http://$url/index.php?profile=$uid'>".PROJECTTABLE."</a> has been evaluated.";
        $user_id = $uid;

        $evaluation_type = t(str_user_evaluation);
    } else {
        $user_id = '';
        log_action('Eval what?',__FILE__,__LINE__);
    }      

    if ($value == 'ok') $val = 0;
    elseif($value=='accept') $val = '0';
    elseif($value=='refuse') $val = '0';
    elseif($value=='-') $val = '-1';
    elseif($value=='+') $val = '+1';
    elseif($value=='m') $val = '-1';
    elseif($value=='z') $val = '0';
    elseif($value=='p') $val = '+1';

    if ($value=='accept') {
            $post_comment .= '<div class=\'accepted\'>accepted</div>';
    }
    elseif ($value=='refuse') {
            $post_comment .= '<div class=\'refused\'>refused</div>';
    }

    if (isset($_SESSION['evaluates'])) {
        if(in_array("$mfid-$table-$post_id",$_SESSION['evaluates'])) {
            echo common_message('error',t(str_itisalready_evaluated)."!");
            exit;
        }
    } else { 
        $_SESSION['evaluates'] = array();
    }

    $cmd = sprintf("INSERT INTO system.evaluations (\"table\",row,user_id,user_name,valuation,comments,related_id) VALUES ('%s',%d,%d,'%s','%d',%s,%s)",$table,$post_id,$mfid,$mfname,$val,quote($post_comment),quote($rel_id));
    $res = pg_query($db,$cmd);    
    if ($row = pg_affected_rows($res)) {
        $_SESSION["evaluates"][] = "$mfid-$table-$post_id";
        echo common_message('ok',t(str_thx_for_opinion).'!');

        if ($table == 'uploadings')
            insertNews(t(str_uploading_evaluation)." ($mfname): {$_POST['comment']}","project");
        elseif ($table == 'users')
            insertNews(t(str_user_evaluation)." ($mfname): {$_POST['comment']}","project");
        else
            insertNews("$table ".str_data_evaluation." ($mfname): {$_POST['comment']}","project");

        if (defined("OB_PROJECT_DOMAIN")) {
            $domain = constant("OB_PROJECT_DOMAIN");
            $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
            $reply_email = "noreply@".parse_url('http://'.$domain,PHP_URL_HOST);
        } elseif (defined("OB_DOMAIN")) {
            $domain = constant("OB_DOMAIN");
            $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
            $reply_email = "noreply@".parse_url('http://'.$domain,PHP_URL_HOST);
        } else {
            $domain = $_SERVER["SERVER_NAME"];
            $server_email = PROJECTTABLE."@".$domain; 
             $reply_email = "noreply@".$domain;
        }

        $cmd = sprintf("SELECT email,pu.receive_mails FROM users u LEFT JOIN project_users pu ON (user_id=u.id) WHERE id=%d AND project_table=%s",$user_id,quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $user_email = $row['email'];
            if ($row['receive_mails'] != 0)
                $m = mail_to("$user_email",$evaluation_type,"$server_email","$reply_email",$evaluation_type.":",$mail_text."<br><br>".$post_comment,'multipart');
            else
                insertNews("Personal message: $mail_text:<br>$post_comment","personal",$mfid,$user_id);
        }

    } else {
        echo common_message('fail','SQL error');
    }
        
    /*if ($target=='data') {
            
            $ID_COL=$_SESSION['st_col']['ID_C'];

            if (!isset($_SESSION['Tname'])) $name = 'unknown';
            else $name =  $_SESSION['Tname'];

            $cmd = sprintf("UPDATE %s SET validation_=array_append(validation_,'%s'), modifier_id='%d',comments_=array_append(comments_,'$val evalulation [$name]') 
                WHERE %s='%d'",PROJECTTABLE,"$val",$mfid,$ID_COL,$post_id);
            if ( PGsqlcmd($ID,$cmd) ) {
                unset($_SESSION["slide$post_id"]);
                print 'ok';
                return;
            } else {
                print 'SQL err';
                return;
            }

    } elseif ($target == 'upload') {
            //only for logined users
            if(!isset($_SESSION['Tid'])) {
                include('logout.php');
                exit;
            }

            $cmd = sprintf("UPDATE system.uploadings SET validation=array_append(validation,'%s') 
                WHERE id='%d'","$value"."1",$post_id);
            if ( PGsqlcmd($ID,$cmd) ) {
                print 'ok';
                return;
            } else {
                print 'SQL error';
                return;
            }
    }*/
    exit;
}
echo common_message('fail',"Invalid request");
exit;
?>
