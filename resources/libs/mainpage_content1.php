<?php

// content_function : map slideshow upload-table
// SESSION variable -> can be used/defined somewhere
//
if (defined('MAINPAGE_FUNCTION_CONTENT1'))
    $content_function = MAINPAGE_FUNCTION_CONTENT1;
else
    $content_function = 'map';

# map view
if ($content_function == 'map') {

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $smallmap = '
        <link rel="stylesheet" href="'.$protocol.'://'.URL.'/css/mapsdata.css?rev='.rev('css/mapsdata.css').'" type="text/css" />
        <script type="text/javascript" src="'.$protocol.'://'.URL.'/js/OpenLayers.js?rev='.rev('js/OpenLayers.js').'"></script>';
    
    if (file_exists('includes/local_maps_functions.js.php')) {
        ob_start();
        include_once('includes/local_maps_functions.js.php');
        $smallmap .= ob_get_contents();
        ob_end_clean();
    } else {
        $smallmap .= '<script type="text/javascript" src="'.$protocol.'://'.URL.'/js/maps_functions.js?rev='.rev('js/maps_functions.js').'"></script>';
    }

    if (file_exists('includes/local_main_javascripts.php')) {
        ob_start();
        include_once('includes/local_main_javascripts.php');
        $smallmap .= ob_get_contents();
        ob_end_clean();
    } else {
        //$smallmap .= "<script src='http://maps.google.com/maps/api/js?v=3.5'></script>";
    }

    $smallmap .= "<div style='position:relative'><div id='map' class='smallmap'></div><span id='mouse-position' style='position:absolute;top:5px;z-index:750;right:10px'></span></div>";
    echo  $smallmap;
}
# table view
else if ($content_function == 'upload-table') {
?>
<style>
.pure-table thead {
    background-color: #808080;
}
.pure-table-odd td, .pure-table-striped tr:nth-child(2n-1) td {
    background-color: #4d4d4d;
}
</style>

<?php
/*
 * last uploads data table width links */
    $mf = new mainpage_functions();
    $mf->table = PROJECTTABLE;
    $mf->columns = array('obm_id',$_SESSION['st_col']['DATE_C'][0],$_SESSION['st_col']['SPECIES_C'],$_SESSION['st_col']['NUM_IND_C'],$_SESSION['st_col']['CITE_C'][0]);
    $mf->headers = array('obm_id',str_date,str_species,str_no_inds,str_cite_person);
    echo "<h2>".str_recent_uploads."</h2>";
    echo "<div style='overflow-y:auto;height:400px'>";
    echo  $mf->returnFunc('data_table','');
    echo "</div>";
}
# slideshow
else if ($content_function == 'slideshow') {
?>
<style>
.mySlides {
    display: none;
}
.mySlides img {
    vertical-align: middle;
    max-height:500px;
    max-width:800px;
    height:auto;
    position:absolute;
    left:0;
    right:0;
    margin:0 auto;
    box-shadow: 0 0 20px rgba(0,0,0,0.4);
}
.slideshow-container {
  height: 500px;
  position: relative;
}
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  width: 100%;
  text-align: center;
  bottom:8px;
}
.text-label-bg {
    background-color:black;
    opacity:0.5;
    color:black;
    position:absolute;
    padding:10px;
    transform: translate(-50%, 0);
    bottom:8px;
    border-radius:3px;
}
.text-label {
    position:absolute;
    padding:10px;
    transform: translate(-50%, 0);
    bottom:8px;
}
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}
.active, .dot:hover {
  background-color: #717171;
}
/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>
<script>
var slideIndex = 1;
var slideing;
var timeOut = 4000;
function plusSlides(n) {
    clearTimeout(slideing);
    timeOut = 8000;
    showSlides(slideIndex += n);
}
function currentSlide(n) {
    clearTimeout(slideing);
    timeOut = 8000;
    showSlides(slideIndex = n);
}
function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    $('.mySlides').fadeOut(1000);
    if (typeof n==='undefined') {
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}    
        timeOut = 4000;
    } else {
        if (n > slides.length) {slideIndex = 1}    
        if (n < 1) {slideIndex = slides.length}
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    $('.mySlides:eq('+eval(slideIndex-1)+')').fadeIn(1000);
    dots[slideIndex-1].className += " active";
    slideing = setTimeout(showSlides, timeOut);
}
</script>
<?php
    $img_dir = 'attached_files';
    
    $files_arr = obm_cache('get',"files_arr");
    if ($files_arr===FALSE) {
        $files_arr = array();
        if ($dh = opendir($img_dir))
        {
            while (false !== ($file = readdir($dh)))
            {
                if (preg_match('/^.*\.(jpg|jpeg|gif|png)$/i', $file))
                {
                    $files_arr[] = $file;
                }
            }
            closedir($dh);
        }
    }
    obm_cache('set','files_arr',$files_arr,3600,FALSE);
    shuffle($files_arr); // randomize order of array items
    $array_length = 5;
    $files_length = count($files_arr);
    if ($files_length<$array_length)
        $array_length = $files_length;
    $sliced_arr = array_slice($files_arr, 0, $array_length);
    
    echo '<div class="slideshow-container">';

    $n = 1;
    echo "<div id='slideshow'>";
    global $ID,$BID;
    foreach ($sliced_arr as $s) {
        $cmd = sprintf("SELECT id,user_id,reference,comment,mimetype,exif,slideshow FROM system.files WHERE reference=%s",quote($s));
        $res = pg_query($ID,$cmd);
        $caption_text = '';
        $comment = '';
        if ($row = pg_fetch_assoc($res)) {
            if ($row['slideshow']=='f') continue;
            $caption_text = "<i>".$row['comment']."</i>";
            $comment = $row['comment'];
        }
        $cmd = sprintf("SELECT username FROM users WHERE id=%d",$row['user_id']);
        $res = pg_query($BID,$cmd);
        if ($row = pg_fetch_assoc($res)) {
            if ($comment!='')
                $caption_text .= "<br>";
            $caption_text .= "Foto: ".$row['username'];
        }
        list($width,$height) = @getimagesize("$img_dir/$s");

        if ($width>800) {
            if ($thumb = mkThumb($s,800,0)) {
                if (!is_dir("$img_dir/thumbnails")) mkdir("$img_dir/thumbnails");
                $s = "/thumbnails/$thumb";
                list($width,$height) = @getimagesize("$img_dir/$s");
            }
        }
        if ($height>500) {
            $s = preg_replace('/^\/thumbnails\/\d+x\d+_/','',$s);
            if ($thumb = mkThumb($s,800,500)) {
                if (!is_dir("$img_dir/thumbnails")) mkdir("$img_dir/thumbnails");
                $s = "/thumbnails/$thumb";
            }
        }

        echo "<div class='mySlides'>
          <!--<div class='numbertext'>$n / $array_length</div>-->
          <img src='$img_dir/$s'>
          <div class='text'><span class='text-label-bg'>$caption_text</span><span class='text-label'>$caption_text</span></div>
        </div>";
        $n++;
    }
    echo '</div>';
    echo '
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>
    <div style="text-align:center">';
    for ($i=0;$i<$array_length;$i++) {
        echo '<span class="dot" onclick="currentSlide('.sprintf($i+1).')"></span> ';
    }
    echo '</div>';
?>
<script>
showSlides(slideIndex);
</script>
<?php
}
?>
