<?php

/* Postgre SQL Connect function
 *
 * */

/*function PGconnectSQL($db_user,$db_pass,$db_name,$db_host,$conn_type='new') {
    if ($db_host=='' or $db_user=='' or $db_pass=='' or $db_name=='') return;
    if ($conn_type == 'async') {
        $conn=pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=10",PGSQL_CONNECT_ASYNC);
    } else    
        $conn=pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    pg_set_client_encoding( $conn, 'UTF8' );
    return $conn;
}*/

function PGPconnectSQL($db_user,$db_pass,$db_name,$db_host) {
    if ($db_host=='' or $db_user=='' or $db_pass=='' or $db_name=='') return;
    //$conn = pg_pconnect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    $conn = pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    if (!is_resource($conn)) {
        $custom_log = "/var/log/openbiomaps.log";
        if (is_writable($custom_log)) {
            $date = date('M j h:i:s'); 
            error_log("[OBM_".PROJECTTABLE."] $date: 'Postgres server is not available'\n", 3, $custom_log);
        } else {
            openlog("[OBM_".PROJECTTABLE."]", 0, LOG_LOCAL0);
            syslog(LOG_WARNING,'Postgres server is not available');
            closelog();
        }
        return false;
    } else {
        pg_set_client_encoding( $conn, 'UTF8' );
        return $conn;
    }
}

/* Postgre SQL Query function
 * depreceted
 * */
function PGquery($ID,$qstr) {
  $res = pg_query($ID,$qstr);
  # Logfájlba: 1 hiba történt itt:
  #if(pg_last_error($ID)) {
  #  echo $qstr;
  #  echo pg_last_error($ID);
  #}
  #print $qstr.'<br>'; 
  return $res;
}

/* Postgre SQL Query function
 * depreceted
 * */
function PGsqlcmd($ID,$qstr) {
   $res = pg_query($ID,$qstr);
   $nur = pg_affected_rows($res);
   return $nur;
}

?>
