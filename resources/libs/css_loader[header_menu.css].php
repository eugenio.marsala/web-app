<?php
function setExpires($expires) {
header(
'Expires: '.gmdate('D, d M Y H:i:s', time()+$expires).'GMT');
}
setExpires(300);

header("Content-type: text/css", true);

if (file_exists('../css/private/header_menu.css')) {

    include('../css/private/header_menu.css');

} elseif (file_exists('../css/header_menu.css')) {
    
    include('../css/header_menu.css');

}
?>
