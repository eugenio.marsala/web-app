<?php
function setExpires($expires) {
header(
'Expires: '.gmdate('D, d M Y H:i:s', time()+$expires).'GMT');
}
setExpires(300);

header("Content-type: text/css", true);

if (file_exists('../css/private/body.css')) {

    include('../css/private/body.css');

} elseif (file_exists('../css/body.css')) {
    
    include('../css/body.css');

}
?>
