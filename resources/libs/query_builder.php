<?php
## Olyan lekérdezések térbeli megjelenítésére szolgál WMS segítségével, amiket nem térbeli lehatárolással kérdeztünk le,
#  vagy bonyolult SQL lekérdezésekből származhatnak.
#
#  Itt rakjuk össze a mapfájl számára a postgis DATA query stringet.
#  A lekérdezés már bele van írva a map fájlba, de egyes részei a WMS lekérdezés során átadott extra argumentumként
#  változóként mennek.
#  Így lehet megvalósítani egyedileg specifikált WMS lekérdezéseket.
#  
#  Működés:
#  1. Térbeli lekérdezés továbbküldése WMS queryként. A sendQuery azonosítójú gombbon kattintva az ott megadott formok elemeinek (hely, taxon)  
#  értekeiből készül el egy SQL parancs. 
/* A query map layer-ben a qstr értéket rakja össze, ami egy wsf/wms kérdésként kerül lekérdezésre
 *
 *
 * */
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

if(!isset($_SESSION['Tproject'])) require_once(getenv('OB_LIB_DIR').'prepare_vars.php');

require_once(getenv('OB_LIB_DIR').'languages.php');

$debug_query = 0;

/* Update Login vars - refresh auth tokens */
if (isset($_COOKIE['access_token'])) {
    $cookie = json_decode( $_COOKIE[ "access_token" ] );

    $res = true;

    if (time() > $cookie->expiry) {
        // expired
        $res = refresh_token();
        $cookie = json_decode( $_COOKIE[ "access_token" ] );
    }

    if ($res) {
        // JUST check token valisity by a resource request
        $result = oauth_webprofile_request($cookie->data->access_token);

        if ($result === FALSE) { 
            if (!isset($_SESSION['Tid'])) {
                if(!oauth_login($cookie->data->access_token)) {
                    setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
                    unset($_COOKIE['access_token']);
                }
            }
        }
    }
} elseif (isset($_COOKIE['refresh_token'])) {
    $cookie = json_decode( $_COOKIE[ "refresh_token" ] );

    if (time() < $cookie->expiry) {
        $res = refresh_token();
        if ($res!==false) {
            $cookie = json_decode( $_COOKIE[ "access_token" ] );
            oauth_login($cookie->data->access_token);
        }
    }   
}

/* MODULE INCLUDES HERE */
$modules = new modules();

if (!isset($_SESSION['st_col']))
    init_session();


/* Initial variables */
$qstring = '';
$select = ""; # Statikusn a map fájlban....     "SELECT the_geom,gid,dt_to AS name,dinpi.taxon_id ";
$from = ""; # Statikusan a mapfájlban....       "FROM dinpi ";
$where = array(); # Statikusan a mapfájlban.... array('the_geom IS NOT NULL'); 
$group = ""; # Statikusan a mapfáljban....      "GROUP BY the_geom,name,dinpi.taxon_id,gid";
$save = '';

$_SESSION['taxon_join_filter'] = 0;
$_SESSION['search_join_filter'] = 0;
$_SESSION['uploading_join_filter'] = 0;
$_SESSION['current_query_keys'] = array();
$_SESSION['ignoreJoins'] = 0;

if (isset($_REQUEST['ignoreJoins']) and $_REQUEST['ignoreJoins'])
    $_SESSION['ignoreJoins'] = 1;

if(!isset($_SESSION['filter_type']))
    $_SESSION['filter_type'] = array();


if (isset($_SESSION['morefilter']) and $_SESSION['morefilter']=='off')
    $_SESSION['morefilter']='clear';

//loadQueryMap js call
if (isset($_POST['morefilter']) and $_POST['morefilter'] == 2) {
    $_SESSION['morefilter'] = 'off';
    
    $cmd = sprintf("CREATE UNLOGGED TABLE IF NOT EXISTS temporary_tables.temp_filter_%s_%s ( rowid INTEGER )",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    $cmd = sprintf("GRANT ALL ON temporary_tables.temp_filter_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
    pg_query($ID,$cmd);

    update_temp_table_index(PROJECTTABLE.'_'.session_id());

    $cmd = sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
}


if(!isset($_SESSION['morefilter']) or $_SESSION['morefilter']=='clear')
    $_SESSION['filter_type'] = array();

$st_col = st_col($_SESSION['current_query_table'],'array');
$geom_col = $st_col['GEOM_C'];
$id_col = "obm_id";

$sqla = sql_aliases(1);
$geom_col = $sqla['geom_col'];
$id_col = $sqla['id_col'];

if ($modules->is_enabled('grid_view')) {
    if (isset($_SESSION['display_grid_for_map'])) {
        $grid_geom_col = $_SESSION['display_grid_for_map'];
        $geom_col = 'qgrids.'.$grid_geom_col;
    } else {
        $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
        $geom_col = 'qgrids.'.$grid_geom_col;
    }
}
//log_action($geom_col,__FILE__,__LINE__);

$custom_table = '';
if (isset($_POST['custom_table']) and $_POST['custom_table']!='') {
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
    $custom_table = openssl_decrypt(base64_decode($_POST['custom_table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
}


# Ezt az egészet be lehet vinni session változók mögé, és akkor nem kell ajaxból hívni 
foreach ($_REQUEST as $post_key=>$post_val) {
    $m = array();
    if (preg_match('/^qids_(.+)/',$post_key,$m)) {
        if (preg_match('/obm_custom_report_(\d+)/',$m[1],$ma)) {
            $_REQUEST['report'] = $ma[1].'@'; 
        } else {

            $mcolumn = $m[1]; // matched query column's name
            # text_filter module

            if ($modules->is_enabled('text_filter')) {
                $w = $modules->_include('text_filter','assemble_where',array($mcolumn,$post_val,$custom_table));
                if($w!='') {
                    $_SESSION['current_query_keys'][] = $mcolumn;
                    $where[] = $w;
                }
            } elseif ($modules->is_enabled('text_filter2')) {
                $w = $modules->_include('text_filter2','assemble_where',array($mcolumn,$post_val,$custom_table));
                if($w!='') {
                    $_SESSION['current_query_keys'][] = $mcolumn;
                    $where[] = $w;
                }
            }
            else {
                if (!isset($_SESSION['Tid']))
                    echo "Text filter module is not enabled, maybe you have to log in to use this function.";
                else
                    echo "No text filter modules enabled.";
                log_action('text_filter module not enabled!',__FILE__,__LINE__);
            }

            // set filter type
            if($post_val!='[""]')
                $_SESSION['filter_type'][] = 'text: '.$post_val;
        }
    }
}

if (isset($_REQUEST['geom_selection']) and $_REQUEST['geom_selection']!='') {
    ## User specified geometry query type??
    ##
    # http://postgis.net/docs/manual-1.4/ch07.html#Spatial_Relationships_Measurements
    # $spatial_relations = array('ST_Contains','ST_Crosses','ST_Intersects','ST_Equals','ST_Overlaps','ST_DWithin','ST_Touches','ST_Disjoint','=');
    #
    # http://postgis.net/docs/manual-1.4/ch07.html#Operators
    # $bbox_relations = array('=','&&','~','~=');

    $spatial_relations = array('ST_Contains','ST_Crosses','ST_Intersects');
    // Returns TRUE if A's bounding box is the same as B's.
    $bbox_relations = array('=');

    /* apply transform geometry module
     * change geom_col name to st_snap_to_grid(.. */
    /*$module_name = array_values(array_intersect(array('snap_to_grid','transform_geometries','transform_geometry'), array_column($modules, 'module_name')));
    $mki = (count($module_name)) ? array_search($module_name[0], array_column($modules, 'module_name')) : false;
    if ($mki!==false) {
        $dmodule = new defModules();
        $geom_col = $dmodule->snap_to_grid('geom_column',$modules[$mki]['p'],$geom_col);
    }*/

    $rule = '';
    if ($_SESSION['st_col']['USE_RULES']) {
        $rule = sprintf('LEFT JOIN "%s_rules" ON (%s=row_id)',PROJECTTABLE,$id_col);
    }

    if ($_REQUEST['geom_selection']=='session' and isset($_SESSION['selection_geometry']) and $_SESSION['selection_geometry']!='') {

        if($_REQUEST['neighbour']==1) {
            /* POINT Click query
             *
             * */
            # ce {"left":18.69383628181169,"bottom":47.203916549986616,"right":19.74165732658823,"top":47.604053822625815 ,"centerLonLat":null}
            $ce = (array) json_decode($_REQUEST['ce']);
            # log_action($ce);
            $cmd = sprintf("SELECT St_Distance(St_GeomFromText('POINT(%f %f)'),St_GeomFromText('POINT(%f %f)'))",$ce['left'],$ce['bottom'],$ce['right'],$ce['top']);
            $res = pg_query($ID,$cmd);
            $row = pg_fetch_assoc($res);
            # log_action($cmd);
            $distance = $row['st_distance']/100;
            # log_action($distance);

            $spatials = sprintf('%5$s = ANY(SELECT %5$s FROM %4$s %7$s WHERE geometrytype(%1$s)=\'@@geom_type@@\' AND ST_DWithin(%1$s, ST_GeomFromText(%2$s,%3$d), %6$f) ORDER BY ST_Distance(%1$s,ST_GeomFromText(%2$s,%3$d)) )',
                                $geom_col,quote($_SESSION['selection_geometry']),$st_col['SRID_C'],PROJECTTABLE,$id_col,$distance,$rule);
            //log_action($spatials);
            array_push($where,$spatials);
            $_SESSION['filter_type'][] = 'geometry: '.$_SESSION['selection_geometry'] . " + " ."$distance buffer";
        } else {
            /* Normal spatial query
             *
             * */
            $spatials = array();
            foreach ($spatial_relations as $spr) {
                $spatials[] = sprintf('%4$s(st_Transform(ST_GeomFromText(%1$s,4326),%2$d),%3$s)',quote($_SESSION['selection_geometry']),$st_col['SRID_C'],$geom_col,$spr);
            }
            //foreach ($bbox_relations as $bbr) {
            //    $spatials[] = sprintf('ST_GeomFromText(%1$s,%2$d) %4$s %3$s',quote($_SESSION['selection_geometry']),$st_col['SRID_C'],$geom_col,$bbr);
            //}
            array_push($where,"(".join(' OR ',$spatials).")");

            $_SESSION['filter_type'][] = 'geometry: '.$_SESSION['selection_geometry'];
        }
        $_SESSION['current_query_keys'][] = 'obm_geometry';

        #array_push($where,sprintf('ST_Contains(ST_GeomFromText(%1$s,%2$d),%3$s) OR ST_Crosses(ST_GeomFromText(%1$s,%2$d),%3$s) OR ST_GeomFromText(%1$s,%2$d) = %3$s',quote( $_SESSION['selection_geometry']),$_SESSION['st_col']['SRID_C'],$_SESSION['st_col']['GEOM_C']));
        // set filter type
    }
    elseif ($_REQUEST['geom_selection']=='shared_polygons' and isset($_SESSION['selection_geometry_id']) and $_SESSION['selection_geometry_id']!='') {
        $spatials = array();
        if ( !isset($_REQUEST['spatial_function']) or !is_array($_REQUEST['spatial_function']) or !count($_REQUEST['spatial_function']) )
            $_REQUEST['spatial_function'] = array('contains');

        foreach($_REQUEST['spatial_function'] as $sf) {
            if ($sf == 'contains' )
                $spatials[] = sprintf('%3$s((SELECT geometry FROM system.shared_polygons WHERE id=%1$d),%2$s)',$_SESSION['selection_geometry_id'],$geom_col,'ST_Contains');
            if ($sf == 'intersects' )
                $spatials[] = sprintf('%3$s((SELECT geometry FROM system.shared_polygons WHERE id=%1$d),%2$s)',$_SESSION['selection_geometry_id'],$geom_col,'ST_Intersects');
            if ($sf == 'crosses' )
                $spatials[] = sprintf('%3$s((SELECT geometry FROM system.shared_polygons WHERE id=%1$d),%2$s)',$_SESSION['selection_geometry_id'],$geom_col,'ST_Crosses');
            if ($sf == 'disjoint' )
                $spatials[] = sprintf('%3$s((SELECT geometry FROM system.shared_polygons WHERE id=%1$d),%2$s)',$_SESSION['selection_geometry_id'],$geom_col,'ST_Disjoint');
        }
        //actually eliminated, ...
        //foreach ($bbox_relations as $bbr) {
        //    $spatials[] = sprintf('(SELECT geometry FROM system.shared_polygons WHERE id=%1$d) %3$s %2$s',$_SESSION['selection_geometry_id'],$geom_col,$bbr);
        //}
        array_push($where,"(".join(' OR ',$spatials).")");
        
        #array_push($where,sprintf('ST_Contains((SELECT geometry FROM system.shared_polygons WHERE id=%1$d),%2$s) OR ST_Crosses((SELECT geometry FROM system.shared_polygons WHERE id=%1$d),%2$s) OR (SELECT geometry FROM system.shared_polygons WHERE id=%1$d) = %2$s',$_SESSION['selection_geometry_id'],$_SESSION['st_col']['GEOM_C']));
        // set filter type
        $_SESSION['filter_type'][] = 'geometry-id: '.$_SESSION['selection_geometry_id'];
        $_SESSION['current_query_keys'][] = 'obm_geometry';
    }
    elseif (isset($_POST['geom_selection']) and $_POST['geom_selection']=='custom_polygons') {
        $spatials = array();
        
        $table_reference = '';
        if (isset($_POST['custom_table']) and isset($_SESSION['private_key'])) {
            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
            $table_reference = openssl_decrypt(base64_decode($_POST['custom_table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        }
        $sp = preg_split('/\./',$table_reference);
        if (count($sp) == 2) {
            $table_schema = $sp[0];
            $table = $sp[1];
        }
        $stcol = st_col($table_reference,'array');
    
        // should be come from header_names table with a unique id (which not exists in header_names currently!!!).
        // schema
        // table
        // geometry_column
        if ($table_reference!='') {
            $spatials[] = sprintf('%3$s(%2$s,(SELECT ST_Transform(ST_Collect(ARRAY_AGG(%5$s)),4326) FROM %4$s WHERE %1$s))',implode(' AND ',$where),$geom_col,'ST_Within',$table_reference,$stcol['GEOM_C']);
            $where = array();
            array_push($where,"(".join(' OR ',$spatials).")");
            $_SESSION['current_query_keys'][] = 'obm_geometry';
        }
    }
}

//Ennek nem tudom, hol kéne a helye legyen.
if (isset($_SESSION['match'])) unset($_SESSION['match']);

// faj név kereső
if (isset($_REQUEST['trgm_string']) and $_REQUEST['trgm_string']!='') {

    $_SESSION['taxon_join_filter'] = 1;

    $j = json_decode($_REQUEST['trgm_string'],true);
    $a_name = array();
    $a_id = array();
    foreach ($j as $je) {
        $split = preg_split('/#/',$je);
        $a_name[] = $split[1];
        $a_id[] = $split[0];
        // update taxon name query frequency
        // there is no postgres way? unfortunelly there is no select trigger...

        // SHOULD BE OPTIONAL!!!!
        
        //$cmd = sprintf('UPDATE %1$s_taxon SET frequency = frequency+1 WHERE meta = %2$s',PROJECTTABLE,quote($split[1]));
        //pg_query($ID,$cmd);
    }

    if ($_REQUEST['allmatch'] and count($a_id)) {
        array_push($where,sprintf("taxon_id IN (%s)",implode(',',$a_id)));
        $_SESSION['match'] = 'all';
    } else {
        array_push($where,sprintf("meta IN (%s)",implode(',',array_map('quote',$a_name))));
        $_SESSION['match'] = 'one';
    }
    // set filter type
    $_SESSION['filter_type'][] = 'taxon: '.implode(',',$a_name);
    $_SESSION['current_query_keys'][] = $st_col['SPECIES_C'];
}

//if (isset($_REQUEST['qids_obm_search']) && ($_REQUEST['qids_obm_search'] !== '')) {
    $_SESSION['search_join_filter'] = 1;
//}

//
if (isset($_REQUEST['report']) and $_REQUEST['report']!='') {
    $where = array();
    $_SESSION['current_query_keys'] = array();
    $_SESSION['filter_type'] = array();
    $_SESSION['taxon_join_filter'] = 1;

    list($id,$key) = preg_split("/@/",$_REQUEST['report']);
    if ($key!='') {
        $u = "key";
        $t = quote($key);
    } else {
        $u = "user_id";
        $t = quote($_SESSION['Tid']);
    }
    $cmd = sprintf("SELECT command,query_string FROM custom_reports WHERE project='%s' AND id=%s AND %s=%s",PROJECTTABLE,quote($id),$u,$t);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        #$where[] = json_decode($row['query_string'],true);
        $where[] = $row['query_string'];
    }
}

// Custom shp Layer feltöltés
// deprecated!!
// másik módszerben nem kell psql táblát létrehozni!!!
/*if (isset($_POST['custom'])) {
    $tbl = "upltmp_".session_id();
    $shp = "/tmp/".$tbl.".shp";
    $dbf = "/tmp/".$tbl.".dbf";
    $shx = "/tmp/".$tbl.".shx";
    //$sql = "/tmp/".$tbl.".sql";
    if (file_exists($shp)) {
        $cmd = "DROP TABLE IF EXISTS biomaps.$tbl";
        pg_query($ID,$cmd);
        //echo $cmd;
        // talán INVALID shp fájllal parancsot lehet injektálni!!!!
        $p = popen("shp2pgsql -I -s 4326 -W LATIN1 $shp biomaps.$tbl","r");
        $cmd = "";
        while ($l = fgets($p)){ $cmd .= $l; }
        pclose($p);

        //$n1 = exec("shp2pgsql -I -s 4326 -W LATIN1 $shp biomaps.$tbl > '$sql'");
        //$cmd = file_get_contents($sql, true);
        $n = PGsqlcmd($ID,$cmd);
        if ($n==0) {
            $cmd = "SELECT type FROM geometry_columns WHERE f_table_name='$tbl'";
            $res = PGquery($ID,$cmd);
            $row = pg_fetch_assoc($res);
            echo $row['type'].';';
        }
        unlink($shp);
        //if (file_exists($sql)) unlink($sql);
        if (file_exists($dbf)) unlink($dbf);
        if (file_exists($shx)) unlink($shx);
        //nem kellenek ha van shx állomány
        //$cmd = "UPDATE geometry_columns SET srid = 4326 WHERE f_table_name = '$tbl'";
        //$cmd = "SELECT UpdateGeometrySRID('$tbl', 'the_geom', 4326)";

        $where[] = session_id();
    }
}*/

// Nincs használva (fajnév kereső)
if (isset($_REQUEST['taxon']) and trim($_REQUEST['taxon'])!='') {
    mb_internal_encoding('utf-8');
    $taxon = mb_ereg_replace("[^a-zA-Z0-9.,-_()#\/\\\]",'',$_REQUEST['taxon']);
    $id = '';
    $lang = '';
    //mb_internal_encoding('utf-8');
    $ilt = preg_split('/#/',$taxon);
    $id = $ilt[0];
    $lang = $ilt[1];
    $taxon = $ilt[2];
    #print $taxon;
    //print $taxon;
    if (!in_array($lang,$_SESSION['st_col']['ALTERN_C'])) return;
    $col =  array_search($lang,$_SESSION['st_col']['ALTERN_C']);

    if ($col) {
        if ($_REQUEST['allmatch'] and $id) {
            array_push($where,sprintf("taxon_id='%d'",$id));
        }
        elseif ($_REQUEST['with_similar']) {
            $limit = preg_replace('/[^0-9]/','',$_REQUEST['with_similar']);
            if ($limit>10) $limit=10;
            elseif ($limit<1) $limit=1;
            $cmd = sprintf("SELECT * FROM (SELECT word <->%s AS dist,taxon_id FROM %s_taxon) p ORDER BY dist LIMIT %d",quote($taxon),PROJECTTABLE,$limit);
            $result = pg_query($ID,$cmd);
            //pg_close($ID);
            $qv = array();
            while ($row=pg_fetch_assoc($result)) {
                $qv[] = $row['taxon_id'];
            }
            $list = implode(',',$qv);
            array_push($where,sprintf("taxon_id IN (%s)",$list));
        } else {
            array_push($where,sprintf("replace($col,' ','') ILIKE %s",quote($taxon)));
        }
            
    }
    else return;

/* Faj név keresés COL adatbázisban
 * WITH RECURSIVE q AS
        (
            SELECT  h, 1 AS level, ARRAY[taxon_id] AS breadcrumb,taxon_category,taxon_description
            FROM    taxon h
            WHERE   parent_id = 9031529 or taxon_id = 9031529
            UNION ALL
            SELECT  hi, q.level + 1 AS level, breadcrumb || taxon_id,hi.taxon_category,hi.taxon_description
            FROM    q
            JOIN    taxon hi
            ON      hi.parent_id = (q.h).taxon_id
        ) SELECT wkb_geometry,ogc_fid,date AS name,fishdb.taxon_id  FROM fishdb LEFT JOIN q ON (q.h).taxon_id=fishdb.taxon_id  WHERE wkb_geometry IS NOT NULL AND taxon_category like '%species%' GROUP BY wkb_geometry,name,fishdb.taxon_id,ogc_fid*/

    /*$cmd = "WITH RECURSIVE q AS
        (
            SELECT  h, 1 AS level, ARRAY[taxon_id] AS breadcrumb,taxon_category,taxon_description
            FROM    taxon h
            WHERE   parent_id = $taxon or taxon_id = $taxon
            UNION ALL
            SELECT  hi, q.level + 1 AS level, breadcrumb || taxon_id,hi.taxon_category,hi.taxon_description
            FROM    q
            JOIN    taxon hi
            ON      hi.parent_id = (q.h).taxon_id
        ) ";
    $cmd .= 'SELECT gid FROM dinpi LEFT JOIN q ON (q.h).taxon_id=dinpi.taxon_id ';
    $cmd .= "WHERE taxon_category like '%species%' ";
    $cmd .= "GROUP BY gid";
    
    $result = PGquery($ID,$cmd);
    $qv = array();
    while ($row=pg_fetch_row($result)) {
           $qv[]=$row[0];
    }
    array_push($where,sprintf("gid IN (%s)",implode(',',$qv)));*/
    
    //$put = array('method=list');
    //$id = st_col('ID_C');
    //while($row=pg_fetch_assoc($result)) {
        #array_push($put,"$id=".$row[$id]);
    //    array_push($put,"id=".$row[$id]);
    //}
    #$_POST['result']=$put;
    //$_SESSION['json_post'] = json_encode($put);
    
    //SaveQueryTemp($save,'','table');
    ## Should replace url call like:
    # include('http://$url/results_builder.php?json_post=$json_obj');
    //include('results_builder.php');
}

$r_c = 0;
$where = array_filter($where);
if (count($where)) $r_c = 1;

$w = join(' AND ',$where);
if (trim($w) != '') $qstring = $w;

# debugging
if ($debug_query)
    log_action("debugging qstring: ".$qstring,__FILE__,__LINE__);

if ($r_c) {
    # This is a function to hide passed query string
    #$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    #unset($_SESSION['query_build_string']);
    //$random = substr( str_shuffle( $chars ), 0, 16 );
    //$_SESSION['query_build_string'][$random] = $qstring;
    //print $random;
    $_SESSION['query_build_string'] = $qstring;

    // $_GET['report'] & csv output request - do not print output
    if (isset($_SESSION['show_results']['type']) and $_SESSION['show_results']['type'] == 'csv')
        return;

    elseif (isset($_SESSION['show_results']['type']) and $_SESSION['show_results']['type'] == 'json')
        return;

    // ajax answer, 
    echo 1;
} else {
    echo "\nEmpty filter options.";
    //no ajax answer
    return false;
    //if (isset($_SESSION['query_build_string']))
    //    $random = array_pop(array_keys($_SESSION['query_build_string']));
}
?>
