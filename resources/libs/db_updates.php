<?php
class db_updates {

    var $revision;
    var $error = '';

    function __construct($revision) {
        $this->revision = $revision; 
        $this->update();
        if ($this->error) 
            log_action($this->error,__FILE__,__LINE__);
    }

    private function update() {
        global $BID,$ID,$GID;

        if ($this->revision == 'search table') {
            /********************
             * 2018. 09. 26. - search table 
             * *********************/

            $st = false;//obm_cache('get',preg_replace('/ /','',$this->revision),'','',FALSE);
            if ($st !== false) {
                return;
            }
            $cmd = sprintf("SELECT status FROM db_updates WHERE revision=%s",quote($this->revision));
            $res = $this->query($BID,$cmd);
            $result = pg_fetch_assoc($res[0]);
            if ($result['status'] == 't') {
                obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,FALSE);
                return;
            }

            $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE  table_schema = 'public' AND table_name = '%s_search');",PROJECTTABLE);
            $res = $this->query($ID,$cmd);
            $result = pg_fetch_assoc($res[0]);

            if ($result['exists'] == 'f') {

                if (!isset($_SESSION['st_col']['SPECIES_C']) || !isset($_SESSION['st_col']['ALTERN_C']) || !isset($_SESSION['st_col']['CITE_C'])) {
                    $this->error = 'st_cols not set';
                    return;
                } 

                $res = $this->query($ID, sprintf('SELECT max(taxon_id) c FROM %1$s_taxon;', PROJECTTABLE));
                $taxon_count = pg_fetch_assoc($res[0]);

                    //sprintf('ALTER SEQUENCE %1$s_search_id_seq MINVALUE %2$d;', PROJECTTABLE, $taxon_count['c']),


                $cmd = [ 
                    sprintf("CREATE SEQUENCE %s_search_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START %d CACHE 1;",PROJECTTABLE, $taxon_count['c']),
                    sprintf("CREATE SEQUENCE %s_search_connect_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;",PROJECTTABLE),
                    sprintf('CREATE TABLE %1$s_search ( search_id integer DEFAULT nextval(\'%1$s_search_id_seq\') NOT NULL, subject character varying(128) NOT NULL, meta character varying(128) NOT NULL, word character varying(128) NOT NULL, institute character varying(128), status integer DEFAULT 0, role_id integer, frequency integer, CONSTRAINT %1$s_search_meta_word_key UNIQUE ("meta","word","subject")) WITH (oids = false);',PROJECTTABLE),
                    sprintf('CREATE TABLE %1$s_search_connect (data_table varchar(64), row_id integer, species_ids integer[], observer_ids integer[], CONSTRAINT %1$s_search_connect_table_row_key UNIQUE ("data_table","row_id")) WITH (oids = false);',PROJECTTABLE),
                    sprintf('INSERT INTO %1$s_search (meta,word,subject,search_id,status,frequency) SELECT meta, word, lang, taxon_id, status, frequency FROM %1$s_taxon;',PROJECTTABLE),
                    sprintf('INSERT INTO %1$s_search (meta,word,subject,frequency) SELECT lower(regexp_replace(names,\'\s+\',\' \')),names,\'observer\', COUNT(names) FROM (SELECT unnest(regexp_split_to_array(concat_ws(\',\',%2$s),E\'[,;]\\\\s*\')) as names FROM %1$s) foo GROUP BY names ORDER BY count DESC;',PROJECTTABLE,implode(',',$_SESSION['st_col']['CITE_C'])),
                    sprintf('INSERT INTO %1$s_search_connect (data_table, row_id, observer_ids) SELECT \'%1$s\', obm_id, array_agg(search_id) FROM (SELECT obm_id, unnest(regexp_split_to_array(concat_ws(\',\',%2$s),E\'[,;]\\\\s*\')) as names FROM %1$s) as foo LEFT JOIN %1$s_search o ON names = word GROUP by obm_id;',PROJECTTABLE,implode(',',$_SESSION['st_col']['CITE_C'])),
                    sprintf('UPDATE %1$s_search_connect sc SET species_ids = ARRAY[taxon_id] FROM (SELECT taxon_id, obm_id FROM %1$s b LEFT JOIN %1$s_taxon tx ON b.%2$s = tx.word AND b.%2$s IS NOT NULL) as t WHERE sc.row_id = t.obm_id;', PROJECTTABLE, $_SESSION['st_col']['SPECIES_C'])
                ];
                if ($this->query($ID,$cmd)) {
                    $this->query($BID,sprintf('INSERT INTO db_updates (status,datum,revision) VALUES (true,now(),\'%s\')',$this->revision));
                    obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,FALSE);
                    return;
                }
            }
        }

        if ($this->revision == 'access & select_view to varchar') {
            /* 2018 szept, last magic numbers??
             *
             * */
            $st = obm_cache('get',preg_replace('/ /','',$this->revision),'','',TRUE);
            if ($st !== false ) 
                return;

            pg_query($GID,sprintf('ALTER TABLE system.polygon_users ALTER select_view TYPE character varying(13)'));
            pg_query($GID,sprintf('ALTER TABLE system.shared_polygons ALTER access TYPE character varying(7)'));
            pg_query($GID,sprintf('ALTER TABLE system.shared_polygons ALTER access SET DEFAULT \'private\'::character varying'));

            obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,TRUE);
            return;
        }

        if ($this->revision == 'project user status to varchar') {
            /* 2018 szept, eliminates magic numbers...
             *
             * */
            $st = obm_cache('get',preg_replace('/ /','',$this->revision),'','',FALSE);
            if ($st !== false ) 
                return;

            $cmd = sprintf("SELECT status FROM db_updates WHERE revision=%s",quote($this->revision));
            $res = pg_query($BID,$cmd);
            $result = pg_fetch_assoc($res);
            if ($result['status'] == 't') {
                obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,FALSE);
                return;
            } 

            pg_query($BID,sprintf('INSERT INTO db_updates (status,datum,revision) VALUES (true,now(),\'%s\')',$this->revision));
            pg_query($BID,sprintf('ALTER TABLE project_users ALTER user_status TYPE character varying(10)'));
            pg_query($BID,sprintf('ALTER TABLE project_users ALTER user_status SET DEFAULT \'normal\'::character varying'));

            obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,FALSE);
            return;
        }

        if ($this->revision == 'language files to db') {
            /********************
             * 2018 augusztus - áttérés a fájl alapú nyelv kezelésről az adatbázis translation táblára
             * *********************/

            $st = obm_cache('get',preg_replace('/ /','',$this->revision),'','',FALSE);
            if ($st !== false ) 
                return;

            $cmd = sprintf("SELECT status FROM db_updates WHERE revision=%s",quote($this->revision));
            $res = pg_query($BID,$cmd);
            $result = pg_fetch_assoc($res);
            if ($result['status'] == 't') {
                obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,FALSE);
                return;
            }

            $LANGS = explode(',',LANGUAGES); 

            $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM   information_schema.tables WHERE  table_schema = 'public' AND table_name = 'translations');");
            $res = pg_query($BID,$cmd);
            $result = pg_fetch_assoc($res);


            // importing translations into the database
            if ($result['exists'] == 'f') {

                // create db_updates record
                pg_query($BID,sprintf('INSERT INTO db_updates (status,datum,revision) VALUES (true,now(),\'%s\')',$this->revision));

                $cmd1 = "CREATE SEQUENCE translations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;";
                if (pg_send_query($BID, $cmd1)) {
                    $res = pg_get_result($BID);
                    $state = pg_result_error($res);
                    if ($state != '') {
                        log_action($state,__FILE__,__LINE__);
                    }
                    else {
                        $cmd2 = "CREATE TABLE \"public\".\"translations\" ( \"id\" integer DEFAULT nextval('translations_id_seq') NOT NULL, \"scope\" character varying(32) NOT NULL, \"project\" character varying(32), \"lang\" character(2) NOT NULL, \"const\" character varying(128) NOT NULL, \"translation\" text NOT NULL, CONSTRAINT \"translations_id\" PRIMARY KEY (\"id\"), CONSTRAINT \"translations_scope_project_lang_const\" UNIQUE (\"scope\", \"project\", \"lang\", \"const\")) WITH (oids = false); ";
                        if (pg_send_query($BID, $cmd2)) {
                            $res = pg_get_result($BID);
                            $state = pg_result_error($res);
                            if ($state != '') {
                                log_action($state,__FILE__,__LINE__);
                            }
                            else {
                                foreach ($LANGS as $l) {

                                    //import global laguage files
                                    $file = sprintf("%slanguages/%2s.php",getenv('OB_LIB_DIR'),$l);
                                    if (file_exists($file)) {
                                        $this->import_language_file($file,$l,'global','',$domain);
                                    }

                                    //import admin laguage files
                                    $file = sprintf("%slanguages/admin-%2s.php",getenv('OB_LIB_DIR'),$l);
                                    if (file_exists($file)) {
                                        $this->import_language_file($file,$l,'global','',$domain);
                                    }

                                    // import local language files
                                    $file = sprintf("%slanguages/local_%2s.php",getenv('PROJECT_DIR'),$l);
                                    if (file_exists($file)) {
                                        $this->import_language_file($file,$l,'local',PROJECTTABLE);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                $cmd = sprintf("SELECT count(*) as c FROM translations WHERE scope = 'local' AND project = '%s';", PROJECTTABLE);
                $res = pg_query($BID,$cmd);
                $result = pg_fetch_assoc($res);

                if ($result['c'] == 0) {
                    foreach ($LANGS as $l) {
                        $file = sprintf("%slanguages/local_%2s.php",getenv('PROJECT_DIR'),$l);
                        if (file_exists($file)) {
                            $this->import_language_file($file,$l,'local',PROJECTTABLE);
                        }
                    }
                }
            }
            obm_cache('set',preg_replace('/ /','',$this->revision),'t',3600*24,FALSE);
            return;
        }

    }
    // internal functions
    // import language files
    private function import_language_file($file,$lang,$scope,$project = '',$domain = '') {
        global $BID;

        pg_query($BID,"COPY translations (scope, project, lang, const, translation) FROM STDIN");

        $content = file($file);
        foreach($content as $row) {
            $m = array();
            preg_match("/define\(['\"](\w+)['\"],['\"](.*)['\"]\);/",$row,$m);
            if (isset($m[1]) && isset($m[2])) {
                $m[2] = str_replace("'.PROJECTTABLE.'",'%PROJECTTABLE%', $m[2]);
                $m[2] = str_replace("'.\$domain.'",'%DOMAIN%', $m[2]);
                pg_put_line($BID,"$scope\t$project\t$lang\t{$m[1]}\t{$m[2]}\n");
            }
        }
        pg_put_line($BID, "\\.\n");
        pg_end_copy($BID);

        return true;
    }
    private function query ($db,$cmd) {
        if (gettype($cmd) === "string")
            $cmd = [$cmd];

        pg_query($db, "BEGIN;");
        $cmdStr = implode('',$cmd);
        if (pg_send_query($db, $cmdStr)) {
            $results = [];
            for ($i = 0, $l = count($cmd); $i < $l; $i++) {
                $res = pg_get_result($db);
                $results[] = $res;
                $state = pg_result_error($res);
                if ($state != '') {
                    $this->error = $state;
                    pg_query($db,'ROLLBACK;');
                    return false;
                }
            }
            pg_query($db,'COMMIT;');
            return $results;
        }
        else {
            $this->error = "pg_send_query failed!";
            return false;
        }
    }
}
?>
