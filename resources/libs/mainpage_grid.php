<?php
class mgrid {
    // `elements`.php should be exists in local includes dir:
    // e.g. includes/sidebar1.php ...
    var $elements = array('header','sidebar1','sidebar2','sidebar3','content1','content2','footer');
    
    // default layout - will be overdefined width grids
    var $definition = '
    <div class="wrapper">
      <div class="box header">#header</div>
      <div class="box sidebar1">#sidebar1</div>
      <div class="box sidebar2">#sidebar2</div>
      <div class="box sidebar3">#sidebar3</div>
      <div class="box content1">#content1</div>
      <div class="box content2">#content2</div>
      <div class="box footer">#footer</div>
    </div>';
 
   // minimal width layout
   var $areas = 'header
     sidebar1
     content1
     sidebar2
     sidebar3
     content2
     footer';
   var $gap = "1em";

   // min 600px width layout
   var $gap600 = '5px';
   var $columns600 = '50% auto';
   var $areas600 = 'header   header
                    content1 content1
                    sidebar1 sidebar2
                    content2 sidebar3
                    content2 content3
                    footer   footer'; 

   // min 900px width layout
   var $gap900 = '10px';
   var $columns900 = '34% 33% auto';
   var $areas900 = 'header   header   header
                    content1 content1 sidebar1
                    sidebar2 sidebar2 sidebar1
                    sidebar2 sidebar2 sidebar3
                    content2 content2 sidebar3
                    content2 content2 sidebar3
                    content2 content2 footer'; 

   // min 1400 px width layout
   var $gap1400 = '20px';
   var $columns1400 = 'auto 350px 200px auto';
   var $areas1400 = 'header  header  header header
                     content1 sidebar2 sidebar3 content2
                     content1 sidebar2 sidebar3 content2
                     sidebar1 sidebar2 sidebar3 content2
                     sidebar1 footer  footer content2'; 

}
?>

