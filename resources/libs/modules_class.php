<?php

class modules {

    private $modules = array();
    public $main_table = "";

    /*  */
    public function __construct($read_session = true) {
        if ($read_session and isset($_SESSION['modules'])) {
            $this->modules = $_SESSION['modules'];
        }
        else {
            $this->modules = array();
        }
    }

    public function set_modules() {
        global $BID;
        
        $content = file(getenv('OB_LIB_DIR').'default_modules.php');
        $m_name = 0;
        $meth = 0;
        $examp = 0;
        $desc = 0;
        $general = 0;
        $methods = array();
        $examples = array();
        $descriptions = array();
        $generals = array();

        foreach ($content as $rows) {
            if (preg_match('/# Module/',$rows)) {
                $m_name = 1;
                $general = $meth = $examp = $desc = 0;
            } elseif (preg_match('/# Methods/',$rows)) {
                $meth = 1;
                $desc = $examp = $general = $m_name = 0;
            } elseif (preg_match('/# Description/',$rows)) {
                $desc = 1;
                $examp = $meth = $general = $m_name = 0;
            } elseif (preg_match('/# Example/',$rows)) {
                $examp = 1;
                $desc = $meth = $general = $m_name = 0;
            } elseif (preg_match('/# General module/',$rows)) {
                $general = 1;
                $examp = $desc = $meth = $m_name = 0;
            }

            $m = array();
            if ($m_name and preg_match('/## (.+)/',$rows,$m)) {
                $m_name = 0;
                $methods[trim($m[1])] = '';
                $descriptions[trim($m[1])] = '';
                $examples[trim($m[1])] = '';
                $last_module = trim($m[1]);
                $generals[trim($m[1])] = '';

            } elseif ($meth and preg_match('/## (.+)/',$rows,$m)) {
                $meth = 0;
                $methods[$last_module] = array_map('trim',explode(',',$m[1]));

            } elseif ($desc and preg_match('/## (.+)/',$rows,$m)) {
                $descriptions[$last_module] .= $m[1]."\n";

            } elseif ($examp and preg_match('/## (.+)/',$rows,$m)) {
                $examples[$last_module] .= $m[1]."\n";

            } elseif ($general) {
                $generals[$last_module] = 1;
            }
        }

        if (isset($_SESSION['Tid'])) {
            $a = "0,1";
            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') {
                $g = $_SESSION['Tgroups'].',0';

                // inherited privileges from other groups
                $cmd = sprintf('SELECT role_id FROM project_roles WHERE container && \'{%1$s}\'::int[] AND role_id NOT IN (%1$s)',$_SESSION['Tgroups']);
                $res = pg_query($BID,$cmd);
                $ig = array();
                while ($row = pg_fetch_assoc($res)) {
                    $ig[] = $row['role_id'];
                }
                $igi = implode(',',$ig);
                if ($igi!='')
                    $g .= ",$igi";

            } else
                $g = "0";
        } else {
            $a = "0";
            $g = "0";
        }

        $cmd = sprintf("SELECT \"function\" as f,module_name,array_to_string(params,';') as p, enabled, main_table 
            FROM modules 
            WHERE project_table='%s' AND module_access IN ($a) AND (group_access && ARRAY[$g] OR array_length(group_access, 1) IS NULL) 
            ORDER BY module_name,module_access DESC,group_access DESC", PROJECTTABLE);

        $res = pg_query($BID,$cmd);
        $modules = array();
        if(pg_num_rows($res)) {
            while ($row = pg_fetch_assoc($res)) {
                $row['methods'] = isset($methods[$row['module_name']]) ? $methods[$row['module_name']] : [];
                $row['examples'] = isset($examples[$row['module_name']]) ? $examples[$row['module_name']] : '';
                // general modules: remove main_table value
                if (isset($generals[$row['module_name']]) and $generals[$row['module_name']]==1) $row['main_table'] = '';
                $modules[] = $row;
            }
        }
        $this->modules = $modules;
        return $this->modules;
    }

    public function get_modules() {
        return $this->modules;
    }

    public function is_enabled($name, $main_table = '') {
        return count($this->filter_modules($name, $main_table));
    }

    public function get_params($name, $main_table = '') {
        if (isset($this->filter_modules($name, $main_table)[0])) {
            return $this->filter_modules($name, $main_table)[0]['p'];
        }
        return "";
    }

    /***
     *  If the main_table argument is missing, the method gives back results of modules 
     *  from every table of the project 
     *  */
    public function which_has_method($method, $main_table = '') {

        return array_column(array_filter($this->modules, function ($m) use ($method, $main_table) {

            if (!$m['methods']) {
                return false;
            }
            // general modules without main_table
            if ($m['main_table'] == '' && in_array($method, $m['methods']) && $m['enabled'] === 't') return true;

            return ($main_table != '') 
                ? ($m['main_table'] === $main_table && in_array($method, $m['methods']) && $m['enabled'] === 't')
                : (in_array($method, $m['methods']) && $m['enabled'] === 't');

        }),'module_name');
    }

    public function get_example($name) {
        if (isset($_SESSION['current_query_table']))
            $main_table = $_SESSION['current_query_table'];
        else
            $main_table = PROJECTTABLE;

        foreach($this->modules as $m) {
            if ($m['examples'] != '' && $m['main_table']==$main_table && $m['module_name']==$name)
                return $m['examples'];
        }
        return "";

    }

    public function set_main_table($main_table) {
        $this->main_table = $main_table;
    }

    private function filter_modules($name, $main_table = '') {
        if (!$main_table) 
            $main_table = $_SESSION['current_query_table'];

        $this->main_table = $main_table;

        //debug($name);
        //debug($main_table);
        //debug($this->modules);
        return array_values(array_filter($this->modules, function ($m) use ($name, $main_table) {
            // FORCE use modules - if a module use an other 
            if (isset($_SESSION['force_use']) and isset($_SESSION['force_use'][$name]) and $_SESSION['force_use'][$name] == $name) return 1;

            // general modules without main_table
            if ($m['main_table'] == '' && $m['module_name'] === $name && $m['enabled'] === 't') return true;

            return ($m['main_table'] === $main_table && $m['module_name'] === $name && $m['enabled'] === 't');
        }));
    }

    public function _include($module, $method, $pa = array(), $skip_evaluation = FALSE, $debug = FALSE) {

        require_once(getenv('OB_LIB_DIR').'default_modules.php');
        //if (!$skip_evaluation)
        
        if ($debug) {
            debug($this->is_enabled($module,$this->main_table),__FILE__,__LINE__);
            debug($module, __FILE__, __LINE__);
            debug($method,__FILE__,__LINE__);
            debug($pa,__FILE__,__LINE__);
        }
        if ($skip_evaluation || $this->is_enabled($module,$this->main_table)) {
            $dmodule = new defModules($module);
            $retval = $dmodule->$module($method,$this->get_params($module,$this->main_table), $pa);
            if ($debug)
                debug($retval,__FILE__,__LINE__);
            return $retval;
        }
        else {
            return;
        }
    }

    public function list_modules() {
        global $BID;
        
        $content = file(getenv('OB_LIB_DIR').'default_modules.php');
        $m_name = 0;
        $names = array();

        foreach ($content as $rows) {
            if (preg_match('/# Module/',$rows)) {
                $m_name = 1;
            }

            $m = array();
            if ($m_name and preg_match('/## (.+)/',$rows,$m)) {
                $m_name = 0;
                $names[] = trim($m[1]);
            } 
        }
        return $names;
    }
}
?>
