<?php
/* ***************************************************************************  /
    
GENERAL and GLOBAL FUNCTIONS for
profile and data interfaces

Do not change them
Use the local_funcs.php if you need specific functions
Damn... is not true, sorry

****************************************************************************** */
/* index functions 
 * summary page 
 * */
function load_database_summary() {
    global $ID,$BID,$OB_project_description,$OB_contact_email;
    require_once(getenv('OB_LIB_DIR').'database-summary-page.php');

    return $out;
}

/* index functions 
 * doimetadata page
 * */
function load_database_doimetadata() {
    global $ID,$BID;
    require_once(getenv('OB_LIB_DIR').'doi-metadata-page.php');

    if(isset($_GET['json'])) return json_encode($json, JSON_PRETTY_PRINT);
    elseif(isset($_GET['xml'])) {
        #$xml = new SimpleXMLElement('<root/>');
        #array_walk_recursive($json, array ($xml, 'addChild'));
        #return $xml->asXML();
        
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
        array_to_xml($json,$xml_data);
        #$xml_data->formatOutput = true;
        return sprintf("<pre>%s</pre>",$xml_data->asXML() );
    }
    else
        return $out;

}

/* index functions 
 * map page
 * */
function load_map() {
    global $BID;
    global $ID;
    
    require_once(getenv('OB_LIB_DIR').'map-page.php');
    
    return $out;
}

/* index functions 
 * map page
 * navigator
 * */
function load_navigator() {
    require_once(getenv('OB_LIB_DIR').'navigator.php');
    return $out;
}

/* index function
 * upload history page
 * */
function upload_history() {
    global $ID;
    $qtable=PROJECTTABLE;
    $_SESSION['evaluates'] = array();
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $ACC_LEVEL = ACC_LEVEL;
    if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
        return;
    }

    #if (isset($get_id) and $get_id>0) {
    #    $cmd = "SELECT id,uploader_name,uploading_date,validation,description,collectors FROM system.uploadings WHERE id='$get_id'";
    #} elseif (isset($_SESSION['Tid']) and $_SESSION['Tid']>0) {
    #}
    #$cmd = "SELECT id,uploading_date,uploader_id,uploader_name,array_avg(validation) as valuation,collectors,description FROM system.uploadings WHERE id={$_SESSION['getid']}";
    $cmd = sprintf("SELECT id,uploading_date,uploader_id,uploader_name,validation as data_eval,collectors,description,project_table FROM system.uploadings WHERE project=%s AND id=%s",quote(PROJECTTABLE),quote($_SESSION['getid']));
    
    //$cmd = "SELECT id,uploading_date,uploader_id,uploader_name,validation as data_eval,collectors,description,project_table FROM system.uploadings LEFT JOIN ".PROJECTTABLE." WHERE project='".PROJECTTABLE."' AND id={$_SESSION['getid']}";

    $res = pg_query($ID,$cmd);
    
    if (!pg_num_rows($res)) {
        echo "<div style='margin-left:50px;font-size:150%'>This data identifier does not exist or is not available.</div>";
        return;
    }
        
    $out = '<div style="line-height:200%">';
    while ($row=pg_fetch_assoc($res)) {
        $get_id = $row['id'];
        $out .= "<b>".t(str_dataupid).":</b> ".$row['id'].'<br>';
        $out .= "<b>".t(str_description).":</b> ".$row['description'].'<br>';
        $out .= "<b>".t(str_datauploader).":</b> ".$row['uploader_name'].'<br>';
        $out .= "<b>".t(str_datauptime).":</b> ".$row['uploading_date'].'<br>';
        $out .= '<b>'.t(str_data_providers).':</b> ';
	$CITE_C = $_SESSION['st_col']['CITE_C'];
        $names = array();
        foreach ($CITE_C as $sl) {
            if($sl!='') {
                //$sl = quote($sl);
            }
            else continue;

            $cmd = sprintf("SELECT DISTINCT %s as fa FROM %s WHERE obm_uploading_id='%d' AND %s IS NOT NULL",$sl,$qtable,$_SESSION['getid'],$sl);
            $cres = pg_query($ID,$cmd);
            while ($crow=pg_fetch_assoc($cres)) {
                array_push($names,$crow['fa']);
            }
        }
        $names = array_unique($names);
        asort($names);
        $out .= '<div style="max-width:700px;padding-left:25px">'.implode(', ',$names).'</div><br>';
        
        $cmd = sprintf("SELECT count(*) as c FROM ".$row['project_table']." WHERE obm_uploading_id='%d'",$row['id']);
        $cres = pg_query($ID,$cmd);
        if ($crow=pg_fetch_assoc($cres)) {
            $out .= '<b>'.t(str_num_of_rows).':</b> '.$crow['c'] ."</a><br>";
        }
        
        $out .= sprintf('<b>'.t(str_view).':</b> [<a href="'.$protocol.'://'.URL.'/?query=obm_uploading_id:'.$row['id'].'&qtable='.$row['project_table'].'" data-table="'.$row['project_table'].'" id="new-map-window">'.str_map.'</a>] &nbsp; [<a href="'.$protocol.'://'.URL.'/includes/modules/results_asTable.php?show&query=obm_uploading_id:'.$row['id'].'&qtable='.$row['project_table'].'" data-table="'.$row['project_table'].'" id="new-map-window">'.str_table.'</a>]<input type="hidden" id="obm_uploading_id" class="qf" value="'.$row['id'].'"><br>');
        $out .= sprintf("<b>%s:</b><div style='max-width:700px' id='dp_%s'>%s</div>",t(str_comments),$row['id'],comments($row['id'],PROJECTTABLE,'upload'));

        $out .= sprintf('</div><h3>%3$s</h3>
        <div style="width:600px">
        <div class=\'opinioning\'><div class=\'bubby\'><h3>'.str_thanks_for.'!</h3>'.str_opinion_post_1.'<br><br><span style=\'font-variant:small-caps\'>'.str_avoid.'…</span><br>'.str_opinion_post_2.'</div>
        <textarea class=\'expandTextarea yo\' id=\'evdt-%2$s\'></textarea></div>
        <div><b>%1$s:</b> (<a href="'.$protocol.'://'.URL.'/?history=validation&id=%2$s&blt=%4$s" target="_blank">%5$.2f%6$s</a>)
        <button class=\'button-error pure-button pm\' id=\'valm-%2$s\' rel=\'upload\' title=\''.str_dnlike.'\'><i class=\'fa-hand-o-down fa\'></i></button> 
        <button class=\'button-secondary pure-button pm\' id=\'valz-%2$s\' rel=\'upload\' title=\'ok\'>ok</button>
        <button class=\'button-success pure-button pm\' id=\'valp-%2$s\' rel=\'upload\' title=\''.str_like.'\'><i class=\'fa-hand-o-up fa\'></i></button></div>',
        t(str_valuation),$row['id'],t(str_your_opinion),'uploadings',$row['data_eval']*100,'%');
    }

    return $out;
}
/* index functions 
 * evaluation history page
 * */
function valid_history() {
    global $ID,$load_validhistory;

    $ACC_LEVEL = ACC_LEVEL;
    if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
        return;
    }

    $cmd = "SELECT user_id,user_name,valuation,comments,to_char(datum,'YYYY-MM-DD HH24:MI') as datum FROM system.evaluations WHERE \"table\"='$load_validhistory' AND row='".$_SESSION['getid']."'";
    $res = pg_query($ID,$cmd);

    $table = new createTable();
    $table->def(['tid'=>'mytable','tclass'=>'resultstable']);
    $table->tformat(['format_col'=>'1','format_string'=>'<a href=\'?profile=COL-0\'>COL-1</a>']);
    while ($row=pg_fetch_assoc($res)) {
        if ($row['user_name']=='NULL')
            $row['user_name'] = '-';
        $table->addRows($row);
    }
    $table->addHeader(array(-1,str_nickname,str_valuation,str_comments,str_date));
    return $table->printOut();
}

/* index functions */
function taxonlist() {
    /* Taxon list
     * Called from index.php
     * */
    global $ID; 
    
    $species_array = array_filter(array_unique(array_merge($_SESSION['st_col']['ALTERN_C'],array($_SESSION['st_col']['SPECIES_C']))));
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $o = $species_array;
    //$o = implode(",",$species_array);

    /*$join = array();
    foreach($species_array as $column) {
        $join[] = "t.word=$column";
    }
    $join_s = implode(" OR ",$join);

    $cmd = "SELECT $o,t.taxon_id as id FROM ".PROJECTTABLE." LEFT JOIN ".PROJECTTABLE."_taxon t ON ($join_s) GROUP BY $o,id ORDER BY $o";*/

    /*
    SELECT 
sci.word as latin,sci.taxon_id,sci.lang,
hun.word as magyar,hun.taxon_id,hun.lang
FROM dinpi_taxon sci LEFT JOIN dinpi_taxon hun ON (sci.taxon_id=hun.taxon_id AND sci.lang='faj' AND hun.lang='magyar')
WHERE sci.lang='faj' OR hun.lang='magyar'
ORDER BY sci.word,hun.word
             */
    $main = array_pop($species_array);
    $join = array(sprintf("%s_taxon $main",PROJECTTABLE));
    $c = array(sprintf('%1$s.word as %1$s,%1$s.taxon_id taxon_id_%1$s,%1$s.lang as lang_%1$s',$main));
    $where = array("$main.lang='$main'");

    foreach($species_array as $column) {
        $c[] = sprintf('%1$s.word as %1$s,%1$s.taxon_id as taxon_id_%1$s,%1$s.lang as lang_%1$s',$column);
        $join[] = sprintf("LEFT JOIN %s_taxon $column ON ($column.taxon_id=$main.taxon_id AND $main.lang='$main' AND $column.lang='$column')",PROJECTTABLE);
        $where[] = "$column.lang='$column'";
    }
    $cmd = sprintf("SELECT %s FROM %s WHERE %s ORDER BY %s",implode(',',$c),implode(' ',$join),implode(' OR ',$where),"$main.word");

    $result = pg_query($ID,$cmd);
    $t = array();
    while ($row=pg_fetch_assoc($result)) {
        $n = "";
        $n .= "<tr>";
        foreach ($o as $s) {
            if ($row["taxon_id_$s"])
                $n .= sprintf("<td><a href='$protocol://".URL."/?metaname&id={$row["taxon_id_$s"]}' target='_blank'>%s</a></td>",$row[$s]);
            else
                $n .= sprintf("<td>%s</td>",$row[$s]);
        }
        $n .= "</tr>";
        $t[] = $n;
    }
    $out = "<table id='specieslist' class='resultstable'>";
    foreach ($t as $row) $out .= $row;
    $out .= "</table>";
    return $out;
}
// species per user
function usertaxonlist($user) {
    global $ID,$BID;

    $table = PROJECTTABLE;
    $species_c = $_SESSION['st_col']['SPECIES_C'];
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    if (!isset($_SESSION['Tid'])) return;
    
    $cmd = sprintf("SELECT f_main_table,f_species_column FROM header_names WHERE f_table_name='%s' AND f_species_column IS NOT NULL",PROJECTTABLE);
    $result = pg_query($BID,$cmd);
    if (!pg_num_rows($result))
        return;
    else {
        while ($row = pg_fetch_assoc($result)) {
            $table = $row['f_main_table'];
            $species_c = $row['f_species_column'];
        }
    }

    $userdata = new userdata($user,'hash');
    $username = $userdata->get_username();
    $role_id = $userdata->get_roleid();
    $userhash = $userdata->hash;
    
    $out = "<h2>$username - ".str_species_stat."</h2>";

    $taxon_table = 0;
    $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='taxon_update_%s'",$table);
    $result = pg_query($ID,$trigger_enabled);
    if ( pg_num_rows($result) ) {
        $row = pg_fetch_assoc($result);
        if ($row['tgenabled'] == 'O') {
            $taxon_table = 1;
        }
    }

    if ($taxon_table)
        // with taxon table
        $cmd = sprintf('SELECT word as f,SUM(count) as count,t.taxon_id FROM (
          SELECT t.status,t.taxon_id,count(t.word)
          FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id LEFT JOIN %4$s_taxon t ON (word=%1$s) 
          WHERE uploader_id=%3$d 
          GROUP BY t.status,t.taxon_id) as foo
        LEFT JOIN %4$s_taxon t ON (foo.taxon_id=t.taxon_id)
        WHERE t.status IN (0,1) AND lang=\'%1$s\'
        GROUP by foo.taxon_id,word,t.taxon_id
        ORDER BY count DESC,word',$species_c,$table,$role_id,PROJECTTABLE);
    else
        // no taxon table
        $cmd = sprintf('SELECT %1$s as f,count(%1$s) FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id WHERE uploader_id=%3$d GROUP BY %1$s ORDER BY count DESC, %1$s',$species_c,$table,$role_id);

    $result = pg_query($ID,$cmd);
    $list = array();
    
    $nrows = pg_num_rows($result);
    if ($nrows < 10) {
        $out .= "<div style='font-size:130%'>";
        $out .= sprintf("%d %s.",$nrows,str_uploaded_species);
    } elseif ($nrows < 30) {
        $out .= "<div style='font-size:150%'>";
        for ($i = 0;$i<1;$i++) {
            $out .= sprintf('<i class="fa fa-trophy" style="color:#%1$s"></i>',rand_color());
        }
        $out .= sprintf("<br>%d %s: %s!",$nrows,str_uploaded_species,str_species_master_1);
    } elseif ($nrows < 60) {
        $out .= "<div style='font-size:170%'>";
        for ($i = 0;$i<2;$i++) {
            $out .= sprintf('<i class="fa fa-trophy" style="color:#%1$s"></i>',rand_color());
        }
        $out .= sprintf("<br>%d %s: %s!!",$nrows,str_uploaded_species,str_species_master_2);
    } elseif ($nrows < 100) {
        $out .= "<div style='font-size:190%'>";
        for ($i = 0;$i<3;$i++) {
            $out .= sprintf('<i class="fa fa-trophy" style="color:#%1$s"></i>',rand_color());
        }
        $out .= sprintf("<br>%d %s: %s!!!",$nrows,str_uploaded_species,str_species_master_3);
    } elseif ($nrows < 150) {
        $out .= "<div style='font-size:250%'>";
        for ($i = 0;$i<4;$i++) {
            $out .= sprintf('<i class="fa fa-trophy" style="color:#%1$s"></i>',rand_color());
        }
        $out .= sprintf("<br>%d %s: %s!!!!",$nrows,str_uploaded_species,str_species_master_4);
    } else {
        $out .= "<div style='font-size:250%'>";
        for ($i = 0;$i<5;$i++) {
            $out .= sprintf('<i class="fa fa-trophy" style="color:#%1$s"></i>',rand_color());
        }
        $out .= sprintf("<br>%d %s: %s!!!!!",$nrows,str_uploaded_species,str_species_master_5);
    }
    $out .= "</div>";
        
    while($row=pg_fetch_assoc($result)) {

        if ($taxon_table)
            $link = sprintf("<a href='$protocol://".URL."/?metaname&id={$row["taxon_id"]}' target='_blank'>%s</a></td>",$row['f']);
        else
            $link = $row['f'];

        $datalink = sprintf('<a href="'.$protocol.'://'.URL.'/?query=%1$s:'.$row['f'].';obm_uploader_user=%2$s" id="new-map-window" target="_blank">%3$d</a>',$species_c,$userhash,$row['count']);

        $list[] = "<div class='tbl-cell'>$link</div><div class='tbl-cell'>$datalink</div>";
    }
    $table = "<div class='tbl resultstable' style='margin:20px 0 0 30px'><div class='tbl-row'><div class='tbl-h'>".str_specname."</div><div class='tbl-h'>".str_rowcount."</div></div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";

    return $out.$table;
}

function file_get_contents_utf8($fn) {
     $content = file_get_contents($fn);
      return mb_convert_encoding($content, 'UTF-8',
          mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
}
/* index functions */
require_once('metaname.php');
/* profile page tabs
 * params:
 *      user id
 *      own profile control indicator
 *
 * */
function tabs($userid,$own_profile_functions='') {

    $admin_pages_elements = get_admin_pages_list('project_admin');

    ksort($admin_pages_elements,SORT_NATURAL|SORT_FLAG_CASE);

    $out = "<div id='tabs'><ul class='topnav' id='prodTabs'>
      <li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=profile&userid=$userid&get_own_profile=1&$own_profile_functions'> ".t(str_profile)."</a></li>";
    #$out .= (has_access('invites')) ? "<li class='profile_menu'><a href='#tab_basic' id='invites' data-url='includes/profile.php?options=invitations'>".t(str_invites)."</a></li>" : "";
    # OBM Alapvetés, nem lehet by default letiltani!!!
    $out .= "<li class='profile_menu'><a href='#tab_basic' id='invites' data-url='includes/profile.php?options=invitations'>".t(str_invites)."</a></li>";

    $project_admin_menu = "<li class='profile_menu'><a href='#' class='to' id='admin'>".t(str_project_administration)." <i class='fa fa-caret-down'></i></a>
        <ul class='subnav'>";
    $pa = 0;
    foreach ($admin_pages_elements as $key=>$value) {
        if (has_access($value)) {
            $project_admin_menu .= "<li><a href='#tab_basic' data-url='includes/project_admin.php?options=$value'>".mb_ereg_replace(' ','&nbsp;',t($key))."</a></li>";
            $pa++;
        }
    }
    $project_admin_menu .= "</ul></li>";
    if ($pa > 0) 
        $out .= $project_admin_menu;

    # OBM Alapvetés, nem lehet by default letiltani!!!
    #$out .= (has_access("newsread")) ? "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=newsread'>".t(str_messages)."</a></li>" : "";
    #$out .= (has_access("new_project_form")) ? "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=new_project_form'>".t(str_new_project)."</a></li>" : "";
    $out .= "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=newsread'>".t(str_messages)."</a></li>";
    $out .= "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=new_project_form'>".t(str_new_project)."</a></li>";
    $out .= "</ul></div>";
    
    ## SQL Console
    $out .= "
        <div id='sql-console' class='modal'>
            <div class='modal-draggable-handle modal-header'>SQL Console<i id='sql-console-close' class='fa fa-close fa-2x' style='cursor:pointer;position:absolute;top:0px;right:4px;color:white'></i></div>
            <div class='modal-body'>
                <div id='sql-answer'></div>
                <div id='sql-cmd' contenteditable='true'></div>
            </div>
            <div class='modal-footer'><button id='send-sql-cmd'>send</button></div>
        </div>";


    return $out;
}
/* Profile main page
 *
 * get user profile for editing
 * return a html table
 * userid: users/user <- $_SESSION['Tcrypt']
 * od: orcid profile data
 * */
function profile($userid,$od=0) {
    global $BID;
    $get_own_profile = 0;

    // Profile informations for non logined users:
    if (!isset($_SESSION['Tid'])) {
        echo "<div style='padding:30px 0px 0px 30px' id='tab_basic'>";
        echo "<h2>".str_userdata."</h2>";
        $cmd = sprintf("SELECT username,institute,email,visible_mail FROM \"users\" WHERE \"user\"=%s",quote($userid));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            echo "<div><span style='font-size:110%'>{$row['username']}</span></div>";
            echo "<div><span style='font-size:110%'>{$row['institute']}</span></div>";
            if ($row['visible_mail']==2) {
                echo "<div><span style='font-size:110%'>{$row['email']}</span></div>";
            }
        }
        echo "</div>";
        return;
    }

    $cmd = sprintf("SELECT \"user\" FROM users WHERE id=%d",$_SESSION['Tid']);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($userid == 1 or $userid=='')
            $userid = $row['user'];
        if ($row['user']==$userid)
            $get_own_profile = 1;
    } else {
        // error logging
        return;
    }

    // here, we read the profile page
    ob_start();
    $inc = 1;
    include(getenv('OB_LIB_DIR').'profile.php');
    $contents = ob_get_contents();
    ob_end_clean();

    $output = "";
    /*if (isset($_SESSION['Tid'])) {
        $def_timeout = ini_get('session.gc_maxlifetime');
        $output .= '<script language="javascript">var timeoutHandle = null;
    function startTimer(timeoutCount) {
        if (timeoutCount > 0) {
            document.getElementById("sessionTimer").innerHTML = (timeoutCount * 1000)/1000 + " '.str_session_timeout.'";
            timeoutHandle = setTimeout(function () { startTimer(timeoutCount-1);}, "1000");
        } else if (timeoutCount == 0) {
            document.getElementById("sessionTimer").innerHTML = "'.str_session_expired.'" ;
        }
    }
    function refreshTimer() {
        clearTimeout(timeoutHandle);
        startTimer(3600);
    }
    $(document).ready(function() {
        startTimer('.$def_timeout.');
    });
    </script><div style="position:absolute;right:10px;top:90px;color:#888"><span id="sessionTimer"></span></div>';
    }*/
    // own profile functionalities
    if ($get_own_profile==1) {
        if($od) $ol = "sopd=1";
        else $ol = "";
        // administrative tabs for logined users
        //$output = "<div id='tabs'><ul class='nav nav-pills' id='prodTabs'>
        $output .= tabs($userid,$ol);
        $output .= "<div style='padding:30px 0px 0px 20px' id='tab_basic'>$contents</div>";
    } else {
        $output .= $contents;
    }
    return $output;
}

// split ::: separated string
// the second one the default language
// improvment required!!!
// 1: more language support
// 2: generalized language set
// DEPRECATED - do not use!!!
function sepLangText($text) {
    if (preg_match('/:::/',$text)) {
       list($text_1,$text_2) = preg_split('/:::/',$text);
       if ($_SESSION['LANG'] == 'hu') $text = $text_1;
       else $text = $text_2;
    }
    return $text;
}
// Show saved queries in user profile
// Returns a html table
// shared: owner (any) - not owner (2)
function ShowSharedGeoms($user_id,$shared) {
    global $ID,$BID;

    //$em = "";
    $W = "";
    //    if($od) $ol = "&sopd=1";
    //    else $ol = "";
        // administrative tabs for logined users
        //$output = "<div id='tabs'><ul class='nav nav-pills' id='prodTabs'>

    if ($shared == 1) {
        $em = "<h2>".t(str_own_polygons);
        if (isset($_GET['showowngeoms'])) {
            $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
        }
        $em .= "</h2><div class='shortdesc'>".str_geomdesc."</div>";
    } elseif ($shared == 2) {
        $em = "<h2>".t(str_shared_polygons);
        if (isset($_GET['showsharedgeoms'])) {
            $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
        }
        $em .= "</h2><div class='shortdesc'>".str_geomdesc."</div>";
    }
    
    if ($shared==2) {
        $ec = "shared";
        $cmd = "SELECT id,name,access,p.select_view,p.user_id,s.user_id as owner
                FROM system.shared_polygons s 
                LEFT JOIN system.polygon_users p ON (id=polygon_id) 
                WHERE s.user_id!='$user_id' AND p.user_id='$user_id' AND (access IN ('4','public') OR (\"project_table\"='".PROJECTTABLE."' AND access IN ('2','3','login','project'))) 
                ORDER BY name,id";
    } elseif($shared==1) {
        $ec = "private";
        $cmd = "SELECT id,name,access,select_view,p.user_id,s.user_id as owner 
                FROM system.shared_polygons s 
                LEFT JOIN system.polygon_users p ON (id=polygon_id)
                WHERE s.user_id='$user_id' AND (p.user_id IS NULL or p.user_id='$user_id') 
                ORDER BY name,id";
    }
    
    $share_options = $save_b = $del_b = "" ;

    $res = pg_query($ID,$cmd);

    $table = "";
    while ( $row = pg_fetch_assoc($res) ) {
        
        $own = 0;
        if (isset($_SESSION['Tid']) and $_SESSION['Tid']==$row['owner']) $own = 1;

        $s1 = $s2 = $s3 = $s4 = '';
        if ($row['user_id']!=$user_id) $row['select_view']='none';

        // kicsit komplikált, mert egy értéken tárolok két beállítást
        if ($row['select_view']=='0' or $row['select_view']=='none') {
            if ($row['select_view']=='0') $row['select_view']='none';
            $view_button = '<button class="pure-button button-error button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
            $viewu_button ='<button class="pure-button button-error button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
        } elseif ($row['select_view']=='1' or $row['select_view']=='only-select') {
            if ($row['select_view']=='1') $row['select_view']='only-select';
            $view_button = '<button class="pure-button button-secondary button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-eye fa-2x"></i></button>';
            $viewu_button ='<button class="pure-button button-error button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
        } elseif ($row['select_view']=='2' or $row['select_view']=='only-upload') {
            if ($row['select_view']=='2') $row['select_view']='only-upload';
            $view_button = '<button class="pure-button button-error button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
            $viewu_button = '<button class="pure-button button-secondary button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-eye fa-2x"></i></button>';
        } elseif ($row['select_view']=='3' or $row['select_view']=='select-upload') {
            if ($row['select_view']=='3') $row['select_view']='select-upload';
            $view_button = '<button class="pure-button button-secondary button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-eye fa-2x"></i></button>';
            $viewu_button = '<button class="pure-button button-secondary button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-eye fa-2x"></i></button>';
        }

        $access_array = array(''=>'private','1'=>'private','2'=>'login','3'=>'project','4'=>'public','private'=>'private','login'=>'login','project'=>'project','public'=>'public');
        $access = $access_array[$row['access']];
        $options = selected_option(array(sprintf('%s::private',str_private),sprintf('%s::login',str_logined_users),sprintf('%s::project',str_project_users),sprintf('%s::public',str_public)),$access);

        if ($ec=='private') {
            $share_options = '<select class="polygon_access" id="ope-access_%2$d">'.$options.'</select>';
        }

        if ($own) {
            $del_b = '<button class="pure-button button-warning button-small droppolygon '.$ec.'" id="opd_%2$d"><i class="fa fa-trash-o fa-2x"></i></button>';
            $save_b = '<button class="pure-button button-gray button-small polygon_name_save" id="ope_%2$d"><i class="fa fa-floppy-o fa-2x"></i></button>';
        }

        $cmd = sprintf("SELECT user_id FROM system.polygon_users WHERE polygon_id=%d",$row['id']);
        $res3 = pg_query($ID,$cmd);

        // disable delete option if polygon is used by others
        if (pg_num_rows($res3) > 1)
            $del_b = pg_num_rows($res3);

        $table .= sprintf('<tr><td style="text-align:center">'.$del_b.'</td>
            <td><input value="%s" style="width:300px" class="pure-input polygon_name" id="ope-name_%2$d"> '.$save_b.'</td>
            <td style="text-align:center">'.$view_button.'</td><td style="text-align:center">'.$viewu_button.'</td>
            <td>'.$share_options.'</td></tr>',$row['name'],$row['id']);
    }
    $em .= "<table class='resultstable'>";
    $em .= "<tr><th>".t(str_delete)."</th><th>".t(str_selection_name)."</th><th>".t(str_select)."</th><th>".t(str_upload)."</th><th>".t(str_available_for)."</th></tr>";
    $em .= $table;
    $em .= "</table>";

    echo $em;
}
// Show saved queries in user profile
// Returns a html table
function ShowSavedQueries() {
    global $BID;
    if (!isset($_SESSION['Tid'])) return;
    
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_savedqueries);
    if (isset($_GET['showsavedqueries'])) {
        $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_sqdesc."</div>";

    $cmd = "SELECT id,command,user_id,key,query_string,project_table FROM custom_reports cr WHERE user_id='{$_SESSION['Tid']}' AND project='".PROJECTTABLE."' ORDER BY key"; 
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) 
    {    
        $em .= '<table class="resultstable">';
        $em .= "<tr><th>".t(str_actions)."</th><th>".t(str_label)."</th><th>".t('Link')."</th></tr>";
        while($row=pg_fetch_assoc($result)) {
            $link = "$protocol://".URL."/?report={$row['id']}@{$row['key']}&qtable=".preg_replace('/^'.PROJECTTABLE.'_/','',$row['project_table']);
            $em .= "<tr><td><input type='checkbox' name='dt[]' class='dropsq' value='{$row['id']}'></td><td>{$row['key']}</td><td><a href='$link' target='_blank'>$link</a></td></tr>";
        }
        $em .= "</table><button class='button-warning pure-button' id='dropsq'><i class='fa fa-trash'></i> ".str_drop_seleted_items."</button>";
    }
    echo $em;
}

// Show saved queries in user profile
// Returns a html table
function ShowSavedResults() {
    global $BID;
    if (!isset($_SESSION['Tid'])) return;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_savedresults);
    if (isset($_GET['showsavedresults'])) {
        $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_srdesc."</div>";

    $cmd = "SELECT name,datetime,query,to_char(datetime,'YYYY-MM-DD HH:MI') as date,\"type\",id,sessionid FROM \"queries\" LEFT JOIN header_names ON (queries.comment=f_main_table) WHERE f_table_name='".PROJECTTABLE."' AND user_id='{$_SESSION['Tid']}' ORDER BY datetime,name DESC"; 
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) 
    {    
        $em .= '<table class="resultstable">';
        $em .= "<tr><th>".t(str_actions)."</th><th>".t(str_label)."</th><th>".t(str_datetime)."</th><th>".t('Link')."</th></tr>";
        while($row=pg_fetch_assoc($result)) {
            $link = "$protocol://".URL."/?LQ={$row['id']}@{$row['sessionid']}";
            $em .= "<tr><td><input type='checkbox' name='dt[]' class='droprq' value='{$row['datetime']}'></td><td>{$row['name']}</td><td>{$row['date']}</td><td><a href='$link' target='_blank'>$link</a></td></tr>";
        }
        $em .= "</table><button class='button-warning pure-button' id='droprq'><i class='fa fa-trash'></i> ".str_drop_seleted_items."</button>";
    }
    echo $em;
}
// Show interrupted uplods in user profile
// Returns a html table
function ShowIUPS($user_id) {
    global $ID, $BID;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_intr_uploads);
    if (isset($_GET['showiups'])) {
        $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_continue_interrupted_imports."</div>";

    $cmd = sprintf("SELECT ref,to_char(datum, 'YYYY-MM-DD HH24:MI:SS') as datum,form_type,form_id, template_name,file FROM system.imports WHERE project_table='".PROJECTTABLE."' AND user_id=%d ORDER BY datum",$user_id);
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result)) 
    {    
        $em .= '<table class="resultstable">';
        $em .= "<tr><th>".t(str_actions)."</th><th>".t('Link')."</th><th>".t(str_formtype)."</th><th>".t(str_formname)."</th><th>".str_template."</th></tr>";
        while($row=pg_fetch_assoc($result)) {
            $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id='%s'",$row['form_id']);
            $res2 = pg_query($BID,$cmd);
            $row2 = pg_fetch_assoc($res2);

            $cmd = sprintf("SELECT count(*) as c FROM temporary_tables.%s_%s",$row['file'],$row['ref']);
            $rs3 = pg_query($ID,$cmd);
            $r3 = pg_fetch_assoc($rs3);

            $form_name = (defined($row2['form_name'])) ? constant($row2['form_name']) : $row2['form_name'];
            $em .= sprintf('<tr><td><input type="checkbox" name="dt[]" class="dropiups" value="%2$s"> <a href="includes/ajax?exportimport=%2$s" class="pure-button button-href button-success">'.str_export.'</a></td><td><a href="'.$protocol.'://%1$s/upload/?load=%2$s" target="_blank">%3$s (%7$d %8$s)</a></td><td>%4$s</td><td>%5$s</td><td><input id="name_%2$s" value="%6$s"></td></tr>',URL,$row['ref'],$row['datum'],$row['form_type'],$form_name,$row['template_name'],$r3['c'],str_rows);
        }
        $em .= "</table>";
        $em .= "<button class='button-warning pure-button' id='dropiups'><i class='fa fa-trash'></i> ".str_drop_seleted_items."</button> ";
        $em .= "<button class='button-success pure-button' id='saveiups'><i class='fa fa-save'></i> ".str_save_seleted_items."</button>";
    }
    echo $em;
}

// Show API keys 
// Returns a html table
function ShowAPIKeys($user_id) {
    global $BID;
    $cmd = "SELECT access_token,client_id,expires,scope,CASE WHEN expires>now() THEN 1 ELSE 0 END as exp_state FROM oauth_access_tokens oa LEFT JOIN users u ON oa.user_id=u.email
        WHERE u.id='$user_id' ORDER BY expires DESC";

    $res = pg_query($BID,$cmd);

    $em = "<h2>Access tokens: ".t(str_apikeys);
    if (isset($_GET['showapikeys'])) {
        $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'></div>";

    $em .= '<table class="resultstable">';
    $em .= "<tr><th>access token</th><th>client id</th><th>expires</th><th>scope</th><th>action</th></tr>";
    
    while ($row=pg_fetch_assoc($res)) {
            /*if ($row['expire']=='expired') {$color='#B03434';$label='Expired';}
            elseif ($row['expire']=='short') {$color='#C46712';$label='Created at '.$row['datum'];}
            else {$color='#3BA3C4';$label='Created at '.$row['datum'];}*/

        if ($row['exp_state']==1) $button_color = 'button-error';
        else $button_color = 'button-warning';

        $em .= sprintf('<tr>
                    <td>%2$s</td>
                    <td>%3$s</td>
                    <td>%4$s</td>
                    <td>%5$s</td>
                    <td><button class="pure-button %6$s dropapikey" data-tokentype="access" id="dropat_%2$s"><i class="fa fa-trash-o"></i> %1$s</button></td></tr>',
                    str_delete,
                    $row['access_token'],
                    $row['client_id'],
                    $row['expires'],
                    $row['scope'],$button_color);
    }
    $em .= "</table>";
    echo $em;

    $cmd = "SELECT refresh_token,client_id,expires,scope,CASE WHEN expires>now() THEN 1 ELSE 0 END as exp_state FROM oauth_refresh_tokens oa LEFT JOIN users u ON oa.user_id=u.email
        WHERE u.id='$user_id' ORDER BY expires DESC";

    $res = pg_query($BID,$cmd);

    $em = "<h2>Refresh tokens: ".t(str_apikeys)."</h2><div class='shortdesc'></div>";

    $em .= '<table class="resultstable">';
    $em .= "<tr><th>refresh token</th><th>client id</th><th>expires</th><th>scope</th><th>action</th></tr>";
    
    while ($row=pg_fetch_assoc($res)) {
            /*if ($row['expire']=='expired') {$color='#B03434';$label='Expired';}
            elseif ($row['expire']=='short') {$color='#C46712';$label='Created at '.$row['datum'];}
            else {$color='#3BA3C4';$label='Created at '.$row['datum'];}*/

        if ($row['exp_state']==1) $button_color = 'button-error';
        else $button_color = 'button-warning';

        $em .= sprintf('<tr>
                    <td>%2$s</td>
                    <td>%3$s</td>
                    <td>%4$s</td>
                    <td>%5$s</td>
                    <td><button class="pure-button %6$s dropapikey" data-tokentype="refresh" id="droprt_%2$s"><i class="fa fa-trash-o"></i> %1$s</button></td></tr>',
                    str_delete,
                    $row['refresh_token'],
                    $row['client_id'],
                    $row['expires'],
                    $row['scope'],$button_color);
    }
    $em .= "</table>";
    echo $em;


}


// Show user's uploads 
// Returns a html table
function ShowUploads($user) {
    global $ID,$BID;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_show_uploads);
    if (isset($_GET['showuploads'])) {
        $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_upldesc."</div>";

    $userdata = new userdata($user,'hash');
    $role_id = $userdata->get_roleid();

    //if (isset($_GET['showuploads'])) {
    $cmd = sprintf("SELECT * FROM system.uploadings WHERE uploader_id=%d AND project=%s ORDER BY uploading_date",$role_id,quote(PROJECTTABLE)); 
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result)) 
    {    
            $em .= '<table class="resultstable"><tr><th>Uploading date-time</th><th>Validation</th><th>Uploading ID</th><th>Details</th></tr>';
            while($row=pg_fetch_assoc($result)) {
                if ($row['description']=='') $row['description'] = "no description available";
                if ($row['validation']=='') $row['validation'] = 0;

                if (defined('SHINYURL') and constant("SHINYURL"))
                    $eurl = sprintf($protocol.'://'.URL.'/uplinf/%d/',$row['id']);
                else
                    $eurl = sprintf($protocol.'://'.URL.'/index.php?history=upload&id=%d',$row['id']);

                $em .= "<tr><td>{$row['uploading_date']}</td><td>{$row['validation']}</td><td>{$row['id']}</td><td><a href='$eurl'>{$row['description']}</a></td></tr>";
            }
            $em .= "</table>";
    }
    //}
    echo $em;
}
// A function to decode MIME message header extensions and get the text
function decode_imap_text($str){
    $result = '';
    $decode_header = imap_mime_header_decode($str);
    foreach ($decode_header AS $obj) {
        $result .= htmlspecialchars(rtrim($obj->text, "\t"));
    }
    return $result;
}

function server_logs() {
    if (!has_access($_GET['options'])) {
        return str_no_access_to_this_function."!";;
    }

    $source = 'syslog';
    $search = '';
    if (isset($_GET['source'])) $source = $_GET['source'];
    if (isset($_GET['search'])) $search = $_GET['search'];

    $output = str_choose_log.": <select id='syslog_source'>".selected_option(array('syslog::syslog','mapserver log::mapserv'),$source)."</select> <button id='syslog_refresh'><i class='fa fa-refresh'></i></button><br>";

    if ($source=='mapserv') {
        $syslog = "/tmp/".PROJECTTABLE."_private_ms_error.txt";
        if (is_readable($syslog)) {
            $log = exec('tail -n 2000 '.$syslog.'|tac|sed ":a;N;\$!ba;s/\n/\r/g"');
            $la = preg_split("/\r/",$log);
            $logtext = array();
            foreach ($la as $logline) {
                $m = array();
                //[Mon Oct 2 18:13:17 2017].434773
                if (preg_match('/^(\[.+?\])(\.\d+)(.+)$/',$logline,$m)) {
                    $logline = "<span class='loglineid'>$m[1]</span> $m[3]";
                }
                $logtext[] = $logline;
            }
            $log = implode('<br>',$logtext);

        } else {
            $log = "$syslog is not readable!";
        }
    }
    else {
        $syslog = "/var/log/openbiomaps.log";
        if (is_readable($syslog)) {
            //$log = exec('grep -A 10 OBM_'.PROJECTTABLE.': '.$syslog.'|tac|sed ":a;N;\$!ba;s/\n/\r/g"');
            $log = exec('grep -A 10 "\[OBM_'.PROJECTTABLE.'\]" '.$syslog.'|tail -n 200|sed ":a;N;\$!ba;s/\n/\r/g"');
            $la = preg_split("/\r/",$log);
            $logtext = array();
            $i = 0;
            foreach ($la as $logline) {
                $m = array();
                $newline = 1;
                if (preg_match('/^\[OBM_(\w+)\]/',$logline,$m)) {
                    if ($m[1]!=PROJECTTABLE) {
                        // non priviliged log line
                        continue;
                    }
                    $m = array();
                    // [OBM_transdiptera] Oct 3 04:28:12 /var/www/libs/results_builder.php 880 :
                    if (preg_match('/^\[OBM_'.PROJECTTABLE.'\] (\w+ \d+ \d{2}:\d{2}:\d{2})(.*?): (.+)$/',$logline,$m)) {
                        $logline = "<span class='loglineid'>$m[1]$m[2]</span> $m[3]";
                    }
                } else {
                    // not new log line, but multiline logs
                    $newline = 0;
                }

                $logline = preg_replace('/SELECT[\n\r\s]/i',' <b>SELECT</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?FROM[\n\r\s]/i',' <b>FROM</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?WHERE[\n\r\s]/i',' <b>WHERE</b> ',$logline);
                $logline = preg_replace('/DELETE[\n\r\s]/i',' <b>DELETE</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?LEFT[\n\r\s]/i',' <b>LEFT</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?RIGHT[\n\r\s]/i',' <b>RIGHT</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?JOIN[\n\r\s]/i',' <b>JOIN</b> ',$logline);
                $logline = preg_replace('/DROP[\n\r\s]/i',' <b>DROP</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?AND[\n\r\s]/i',' <span class="logline-control">AND</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?OR[\n\r\s]/i',' <span class="logline-control">OR</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?CASE[\n\r\s]/i',' <span class="logline-control">CASE</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?IF[\n\r\s]/i',' <span class="logline-control">IF</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?ELSE[\n\r\s]/i',' <span class="logline-control">ELSE</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?ORDER BY[\n\r\s]/i',' <span class="logline-control">ORDER BY</span> ',$logline);
                $logline = preg_replace('/[\n\r\s=]?ANY/i',' <span class="logline-control">ANY</span>',$logline);
                $logline = preg_replace('/[\n\r\s]ON[\n\r\s(]/',' <span class="logline-control">ON</span>',$logline);

                $logline = preg_replace('/[\n\r\s=]?TRUE/',' <span class="logline-statement">TRUE</span>',$logline);
                $logline = preg_replace('/[\n\r\s=]?FALSE/',' <span class="logline-statement">FALSE</span>',$logline);
                $logline = preg_replace('/[\n\r\s]ASC/',' <span class="logline-statement">ASC</span>',$logline);
                $logline = preg_replace('/[\n\r\s]DESC/',' <span class="logline-statement">DESC</span>',$logline);
                $logline = preg_replace("/$search/","<span class='logline-highlight'>$search</span>",$logline);

                if ($newline) {
                    $logtext[] = $logline;
                    $i++;
                } else
                    $logtext[$i-1] .= " ".$logline;
            }
            $log = implode('<br>',array_reverse($logtext));
        } else {
            $log = "$syslog is not readable!";
        }
    }
    echo $output."<div id='logconsole'>$log</div>";

}
?>
