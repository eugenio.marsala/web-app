<?php

/* session level if true - variable cached in private level - the includes the session name
 *
 * */
function obm_cache($action='',$key='',$value='',$time=30,$session_level=TRUE) {

    
    if (!defined('CACHE')) {
        log_action('no CACHE method defined! Define CACHE as xcache or memcache in local_vars.php.inc',__FILE__,__LINE__);
    }

    if (isset($_SESSION['Tid']))
        $tid = $_SESSION['Tid'];
    else
        $tid = 0;

    if ($session_level)
        $session_id = session_id();
    else {
        // general cache for the whole project
        $session_id = 0;
        $tid = 0;
    }

    if ($action=='set') {
        if (defined('CACHE') and constant('CACHE') == 'memcache') {
            $mcache = new Memcached();
            $mcache->addServer('localhost', 11211);

            // 0 - not compressed
            $mcache->set(PROJECTTABLE.$session_id.$tid.$key, $value, $time);
        } elseif (defined('CACHE') and constant('CACHE') == 'xcache') {
            $cache = XCache::getInstance();
            $cache->set(PROJECTTABLE.$session_id.$tid.$key, $value, $time); 
        }
        // cache debug
        //log_action("CACHE [$action]: ".PROJECTTABLE."$session_id$tid$key: $value ($time)",__FILE__,__LINE__);
        return true;
        
    } elseif ($action=='get') {
        if (defined('CACHE') and constant('CACHE') == 'memcache') {
            $mcache = new Memcached();
            $mcache->addServer('localhost', 11211);
            $value = $mcache->get(PROJECTTABLE.$session_id.$tid.$key);

        } elseif (defined('CACHE') and constant('CACHE') == 'xcache') {
            $cache = XCache::getInstance();
            if (isset($cache->{PROJECTTABLE.$session_id.$tid.$key}))
                $value = $cache->{PROJECTTABLE.$session_id.$tid.$key};
            else
                $value = FALSE;
        }
        // cache debug
        //log_action("CACHE [$action]: ".PROJECTTABLE."$session_id$tid$key: $value",__FILE__,__LINE__);
        return $value;
    } elseif ($action=='delete') {
        if (defined('CACHE') and constant('CACHE') == 'memcache') {
            $mcache = new Memcached();
            $mcache->addServer('localhost', 11211);
            $value = $mcache->delete(PROJECTTABLE.$session_id.$tid.$key);

        } elseif (defined('CACHE') and constant('CACHE') == 'xcache') {
            #$cache = XCache::getInstance();
            #if (isset($cache->{PROJECTTABLE.$session_id.$tid.$key}))
            #    $value = $cache->{PROJECTTABLE.$session_id.$tid.$key};
            #else
            #    $value = FALSE;
        }
        return;
    }
    log_action('invalid cache action',__FILE__,__LINE__);
    return false;
}

/* xcache construct class */
class XCache {   
    private static $xcobj;
    private static $xcache_exists;
    private function __construct()
    {
    }
    public final function __clone()
    {
        throw new BadMethodCallException("Clone is not allowed");
    } 
    /**
     * getInstance 
     * 
     * @static
     * @access public
     * @return object XCache instance
     */
    public static function getInstance() 
    {
        if (!(self::$xcobj instanceof XCache)) {
            self::$xcobj = new XCache;
            //favágó módszer, hogy ellenőrízzük a modul létezését
            //egy betöltéskor 1x kérdezzük meg
            if (!function_exists('xcache_set')) self::$xcache_exists=0;
            else self::$xcache_exists=1;
        }
        return self::$xcobj; 
    }
    /**
     * __set 
     * 
     * @param mixed $name 
     * @param mixed $value 
     * @access public
     * @return void
     */
    public function __set($name, $value)
    {
        if (!self::$xcache_exists) return;
        xcache_set($name, $value);
    }
    /* set with time
     * */
    public function set($name, $value, $time)
    {
        if (!self::$xcache_exists) return;
        xcache_set($name, $value, $time);
    }
    /**
     * __get 
     * 
     * @param mixed $name 
     * @access public
     * @return void
     */
    public function __get($name)
    {
        if (!self::$xcache_exists) return;
        return xcache_get($name);
    }
    /**
     * __isset 
     * 
     * @param mixed $name 
     * @access public
     * @return bool
     */
    public function __isset($name)
    {
        if (!self::$xcache_exists) return;
        return xcache_isset($name);
    }
    /**
     * __unset 
     * 
     * @param mixed $name 
     * @access public
     * @return void
     */
    public function __unset($name)
    {
        if (!self::$xcache_exists) return;
        xcache_unset($name);
    }
}

?>
