<?php
/* this file is included while uploading data file
 * these are not functions just a part of the code
 * would be better as functions??
 *
 *
 * */
class read_uploaded_file {
    public $theader = array();
    public $extra_file_arguments = '';
    public $feed_header = array(); // excel first line processing

    function read_upload_file($fileclass,$filetype,$filename,$append=0) {
        global $BID;

        // extra field arguments
        if (isset($_SESSION['field_separator'])) unset($_SESSION['field_separator']);
        if (isset($_SESSION['character_encoding'])) unset($_SESSION['character_encoding']);
        if (isset($_SESSION['feed_header_line'])) unset($_SESSION['feed_header_line']);
        if (isset($_SESSION['feed_header_line'])) unset($_SESSION['feed_header_line']);
        if (isset($_SESSION['decimal'])) unset($_SESSION['decimal']);

        /* xml class:
         *              - kml
         *              - gpx
         **/
        if ($fileclass=='xml') {
            $_SESSION['upload_file'] = array(); 
            if (!file_exists($filename)) {
                return "Failed to open $filename.";
            }

            if ($filetype=='KML') {
                //$xml = simplexml_load_file($filename);
                $xml = file_get_contents($filename);
                if (create_upload_temp(readKML($xml,'obj'),$append)) {
                    $this->theader = array("name","wkt");
                    return true;
                } else {
                    return "Read file error, couldn't create temporary table for upload form.";
                }
            }
            elseif ($filetype=='GPX') {
                /** Include path **/
                //set_include_path(get_include_path() . PATH_SEPARATOR . '../../libs/');
                /** Simple html dom parser */
                //include('simple_html_dom.php');
                $xml = simplexml_load_file($filename);
                $wpt = array();
                $trkpt = array();
                $theader = array();
                $addheader = 1;
                foreach( $xml->children() AS $child ) {
                    $name = $child->getName();
                    if ($name == 'wpt') {
                        $row = array();
                        $row['lat'] = (string)$child['lat'];
                        $row['lon'] = (string)$child['lon'];
                        $row['wkt'] = "POINT({$row['lon']} {$row['lat']})";
                        if ($addheader) {
                            $theader[] = 'lat';
                            $theader[] = 'lon';
                            $theader[] = 'wkt';
                        }
                        // lon, lat, ele ...
                        foreach($child->children() as $attr) {
                            $name = $attr->getName();
                            $row[$name] = (string)$attr;
                            if ($addheader) $theader[] = $name;
                        }
                        $addheader = 0;
                        // unset those child which have children
                        unset($row['extensions']);
                        $wpt[] = $row;
                    }
                    /*if ($name == 'trk') {
                    foreach( $child->children() AS $grandchild ) {
                        $grandname = $grandchild->getName();
                        if ($grandname == 'name') {
                            echo $grandchild.'<br/>';
                        }
                        if ($grandname == 'trkseg') {
                            foreach( $grandchild->children() AS $greatgrandchild ) {
                                $greatgrandname = $greatgrandchild->getName();
                                print_r($greatgrandname.'  ');
                                //echo '<br/>';
                                if ($greatgrandname == 'trkpt') {
                                    echo $greatgrandchild['lat'].' '.$greatgrandchild['lon'];
                                    foreach( $greatgrandchild->children() AS $elegreatgrandchild ) {
                                        echo $elegreatgrandchild.'<br/>';
                                    }
                                }
                                if ($greatgrandname == 'ele') {
                                    print_r($greatgrandchild);
                                }   
                            }
                        }
                    }
                   }*/ 
                }
                $this->theader = $theader;

                /* GPX Structure check */
                $new_wpt = array();
                foreach($wpt as $lines) {
                    $pos = 0;
                    $new_line = array();
                    foreach ($theader as $h) {
                        if (!array_key_exists($h,$lines)) {
                            $new_line = insertAt($new_line, [$h => ''], $pos);
                        } else {
                            $new_line[$h] = $lines[$h];
                        }
                        $pos++;
                    }
                    $new_wpt[] = $new_line;
                }

                if (create_upload_temp($new_wpt,$append)) 
                    return true;
                else
                    return "Read file error, couldn't create temporary table for upload form.";
            }
        /* txt class:
         *              -csv
         *              -fasta
         *
         * */
        } elseif ($fileclass=='txt') {
            $_SESSION['upload_file'] = array(); 

            // ezeket be lehetne vinni rendes változónak!!!!
            // CSV újraolvcasáskor szerintem elvesznek az extra argumentumok, mert itt felülíródnak semmivel...
            $efa = preg_split('/ /',$this->extra_file_arguments);
            foreach($efa as $e) {
                if (preg_match('/s:(.)/',$e,$m))
                    $_SESSION['field_separator'] = $m[1];
                elseif (preg_match('/c:(\w+)/',$e,$m))
                    $_SESSION['character_encoding'] = $m[1];
                elseif (preg_match('/h:(\w+)/',$e,$m)){
                    if ($m[1]=='yes')
                        $_SESSION['feed_header_line'] = 'yes';
                    elseif ($m[1]=='no')
                        $_SESSION['feed_header_line'] = 'no';
                } elseif (preg_match('/d:(.)/',$e,$m)) {
                    $_SESSION['decimal'] = $m[1];
                }
            }

            if ($filetype=='CSV') {
                
                if (create_upload_temp(readCSV($filename),$append)) {
                    $this->theader = get_sheet_header();
                    return true;
                } else 
                    return "Read file error, couldn't create temporary table for upload form.";

            } elseif ($filetype=='FASTA') {
                if (isset($_SESSION['field_separator']) and $_SESSION['field_separator']!='')
                    $sep = $_SESSION['field_separator'];
                else
                    $sep = '_';

                if (create_upload_temp(readFASTA($filename,$sep),$append)) {
                    $this->theader = get_sheet_header();
                    return true;
                }
                else
                    return "Read file error, couldn't create temporary table for upload form.";
            } elseif($filetype=='JSON') {
                if (create_upload_temp(readJSON($filename),$append)) {
                    $this->theader = get_sheet_header();
                    return true;
                }
                else
                    return "Read file error, couldn't create temporary table for upload form.";
            } else {

                $modules = new modules();
                if($modules->is_enabled('custom_filetype')) {
                    if (create_upload_temp($modules->_include('custom_filetype','custom_read',array(readCSV($filename,'nobom'),$filetype)),$append)) {
                        $this->theader = get_sheet_header();
                        return true;
                    }
                    else
                        return "Read file error, couldn't create temporary table for upload form.";
                }
            }
        /* write file class
         *              - csv
         *              not used yet/not works...
         * */
        } elseif ($fileclass=='writefile') {
            $_SESSION['upload_file'] = array(); 
            if (filetype=='CSV') {
                /** Include path **/
                set_include_path(get_include_path() . PATH_SEPARATOR . '../../libs/');

                /** parsecsv https://code.google.com/p/parsecsv-for-php */
                //include_once 'parsecsv.lib.php';
                //require 'parsecsv-for-php-master/parsecsv.lib.php';
                require_once('vendor/autoload.php');

                if (file_exists($filename) and filesize($filename)) {
                    //$csv = new parseCSV();
                    $csv = new ParseCsv\Csv();
                    $csv->auto($filename);

                    $this->sheetData = $csv->data;
                    $this->theader = array_keys($this->sheetData[0]);
                    return true;
                } else {
                    return "Failed to open $filename.";
                }
            }
        /* spreadsheet class
         *
         * */
        } elseif ($fileclass=='spreadsheet') {
            /** Include path **/
            //set_include_path(get_include_path() . PATH_SEPARATOR . '../../libs/PHPExcel/Classes/');
            require_once('vendor/autoload.php');
            
            if (!isset($_SESSION['upload_file']))
                $_SESSION['upload_file'] = array(); 

            $efa = preg_split('/ /',$this->extra_file_arguments);
            foreach($efa as $e) {
                if (preg_match('/s:(.)/',$e,$m))
                    $_SESSION['field_separator'] = $m[1];
                elseif (preg_match('/c:(\w+)/',$e,$m))
                    $_SESSION['character_encoding'] = $m[1];
                elseif (preg_match('/h:(\w+)/',$e,$m)){
                    if ($m[1]=='yes')
                        $_SESSION['feed_header_line'] = 'yes';
                    elseif ($m[1]=='no')
                        $_SESSION['feed_header_line'] = 'no';
                } elseif (preg_match('/d:(.)/',$e,$m)) {
                    $_SESSION['decimal'] = $m[1];
                }
            }

            //  $filetype = DEFINE IT!!! ...
            //  $filetype = 'Excel5';
            //	$filetype = 'Excel2007';
            //	$filetype = 'Excel2003XML';
            //	$filetype = 'OOCalc';
            //	$filetype = 'Gnumeric';
            //  $filename = DEFINE IT!!! ...
            //  $sheetname = 'Data Sheet #3';
            // $filterSubset = new MyReadFilter(9,15,range('G','K'));
            //echo 'Loading file ',pathinfo($filename,PATHINFO_BASENAME),' using IOFactory with a defined reader type of ',$filetype,'<br />';
            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($filetype);
            
            $_SESSION['upload_file']['sheetList'] = $objReader->listWorksheetNames($filename);
            
            if (isset($_SESSION['upload_file']['activeSheet']))
                $objReader->setLoadSheetsOnly($_SESSION['upload_file']['activeSheet']);

            //echo 'Loading Sheet "',$sheetname,'" only<br />';
            //$objReader->setLoadSheetsOnly($sheetname);
            //echo 'Loading Sheet using configurable filter<br />';
            //$objReader->setReadFilter($filterSubset);
            //$objPHPExcel = $objReader->load($filename);
            //

            $objReader->setReadDataOnly(true);
            try {
                /** Load $inputFileName to a PHPExcel Object  **/
                $objPHPExcel = $objReader->load($filename);


                if (!isset($_SESSION['upload_file']['activeSheet'])) {
                    $name = $objPHPExcel->getSheetNames();
                    $objPHPExcel->setActiveSheetIndexByName($name[0]);
                    $_SESSION['upload_file']['activeSheet'] = $name[0];
                }

                // sheet choice
                $sheetData = array_values($objPHPExcel->getActiveSheet()->toArray('',true,true,true));
                
                $theader = array();
                
                //if ($_SESSION['feed_header_line'] == 'yes') {

                    /* túl költséges header line processing
                     * foreach($sheetData as $line) {
                        foreach($line as $key => $value)
                        {
                            replaceKey($line, $key, $theader[$key]);
                        }
                    }
                    $this->theader = array_values(array_shift($sheetData));*/
                    $this->feed_header = $sheetData[0];
                //}
                if (count($this->feed_header) > 1024) {
                    return "It is not possible to import a sheet with more than 1024 columns!<br>Repair the sheet or update your structure!";
                }

                $count = 0;
                $letter = 'A';
                while ($count < count($sheetData[0])) 
                {
                    $theader[] = $letter;
                    ++$letter;
                    $count++;
                }
                $this->theader = $theader;

                if (create_upload_temp($sheetData,$append))
                    return true;
                else
                    return "Read file error, couldn't create temporary table for upload form.";
            } catch(PHPExcel_Reader_Exception $e) {
                // ez még nem rendes hiba kezelés!
                $this->theader = array();
                return $e->getMessage();
            }

	//SpatiaLite importálás	    
        } elseif ($fileclass == 'sqlite') {
            $selectedLayer = $this->extra_file_arguments;
            $selectedLayer = $_SESSION['upload_file']['activeSheet'];
            $_SESSION['upload_file'] = array(); 
            $_SESSION['upload_file']['activeSheet'] = $selectedLayer;

            /* read posted file
             * */
            // a fájlokat elpakoljuk hogy egy néven legyenek a beolvasáshoz
            $r = move_upl_files($_FILES["file"]);
            $rr = json_decode($r);
            $sqlite_name = $rr->{'file_names'}[0];
    
            $theader = array();
            if ($sqlite_name and $rr->{'tmp'}) {
                /* common_pg_funcs read .sqlite content into ...csv */
                readSQLITE($rr->{'tmp'}.$sqlite_name,'csv',$rr->{'tmp'}.$sqlite_name.".csv",false,$selectedLayer);

                if (create_upload_temp(readCSV($rr->{'tmp'}.$sqlite_name.".csv"),$append)) {
                    $this->theader = get_sheet_header();
                    exec("rm -rf {$rr->{'tmp'}}");
                    return true;
                } else {
                    exec("rm -rf {$rr->{'tmp'}}");
                    return "Read file error, couldn't create temporary table for upload form.";
                }
            }
            return "File upload failed.";

        /* shape class
         *
         * */
        } elseif ($fileclass == 'shape') {
            $_SESSION['upload_file'] = array(); 
            /* read posted files
             * searching for .shp
             * */
            // a shp fájlokat elpakoljuk hogy egy néven legyenek a beolvasáshoz
            $r = move_upl_files($_FILES["file"]);
            $rr = json_decode($r);
            $matches  = preg_grep ('/\.shp$/i', $rr->{'file_names'});
            $shp_name = array_pop($matches);
    
            #magic determination test?
            #if (mime..."$t/$shp_name"=='application/x-esri-shape')

            $theader = array();
            if ($shp_name and $rr->{'tmp'}) {
                /* common_pg_funcs read .shp content into ...csv */
                $ret = readSHP($rr->{'tmp'}.$shp_name,'csv',$rr->{'tmp'}.$shp_name.".csv",true,$this->extra_file_arguments);
                if (!is_array($ret) or !count($ret)) {
                    // Failed to read data
                    return false;
                }
                
                if (create_upload_temp(readCSV($rr->{'tmp'}.$shp_name.".csv"),$append)) {
                    $this->theader = get_sheet_header();
                    exec("rm -rf {$rr->{'tmp'}}");
                    return true;
                } else {
                    // Failed to read data
                    exec("rm -rf {$rr->{'tmp'}}");
                    return "Read file error, couldn't create temporary table for upload form.";
                }
        
                // remove temp dir width uploaded files
                // save files somewhere...?
            }
            return "File upload failed.";
        } elseif ($fileclass == 'image') {
            if (in_array(exif_imagetype($filename),array(IMAGETYPE_JPEG ,IMAGETYPE_TIFF_II , IMAGETYPE_TIFF_MM))) {
                $exif_read = exif_read_data($filename, 0, false);
                if (json_encode($exif_read))
                    $exif = $exif_read;
                else
                    $exif = array($exif_read['FileName'],$exif_read['FileDateTime'],$exif_read['FileSize'],$exif_read['FileType'],$exif_read['MimeType'],$exif_read['Model'],'corrupted exif...');
            } else {
                $a = exec("mediainfo $filename",$output);
                $n = array();
                foreach ($output as $o) {
                    $on = preg_split("/\s+: /",$o);
                    if(count($on)>1) {
                        if($on[0]=='Complete name') {
                            $on[1] = basename($on[1]);
                        }
                        $n[$on[0]] = $on[1];
                    } 
                }
                $exif = $n;
            }

            if (create_upload_temp(array($exif),$append)) {
                $this->theader = array_keys($exif);
                return true;
            } else 
                return "Read file error, couldn't create temporary table for upload form.";
        }
        return false;
    }
}
/** PHPExcel_IOFactory */
/*include 'PHPExcel/Classes/PHPExcel/IOFactory.php';
class MyReadFilter implements PHPExcel_Reader_IReadFilter {
    private $_startRow = 0;
    private $_endRow = 0;
    private $_columns = array();
    public function __construct($startRow, $endRow, $columns) {
        $this->_startRow	= $startRow;
        $this->_endRow		= $endRow;
        $this->_columns		= $columns;
    }
    public function readCell($column, $row, $worksheetName = '') {
        if ($row >= $this->_startRow && $row <= $this->_endRow) {
            if (in_array($column,$this->_columns)) {
                return true;
            }
        }
        return false;
    }
}*/
?>
