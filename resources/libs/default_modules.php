<?php
/*results_builder.php*/
class module {
    public function print_js($params) {
        return '';
    }
    public function print_css($params) {
        return '';
    }
    public function module_params_interface() {
        return false;
    }
    public function init($params, $pa) {
        return;
    }
    public function destroy($params, $pa) {
        return true;
    }
    static function query($db,$cmd) {
        if (gettype($cmd) === "string")
            $cmd = [$cmd];

        pg_query($db, "BEGIN;");
        $cmdStr = implode('',$cmd);
        if (pg_send_query($db, $cmdStr)) {
            $results = [];
            for ($i = 0, $l = count($cmd); $i < $l; $i++) {
                $res = pg_get_result($db);
                $results[] = $res;
                $state = pg_result_error($res);
                if ($state != '') {
                    log_action("ERROR: $state",__FILE__,__LINE__);
                    pg_query($db,'ROLLBACK;');
                    return false;
                }
            }
            pg_query($db,'COMMIT;');
            return $results;
        }
        else {
            log_action("ERROR: pg_send_query failed!",__FILE__,__LINE__);
            return false;
        }
    }
}
class defModules {

    private $module;
    private $default_module;
    private $private_module;
    private $private_path;
    public $available_methods;

    function __construct($module) {
        
        $this->default_module = getenv('OB_LIB_DIR').'modules/' . $module . '.php';
        $this->private_module = getenv('OB_LIB_DIR').'modules/private/' . $module . '.php';
        $this->private_path = getenv('OB_LIB_DIR').'modules/private/';

        $wp = '';
        if (file_exists($this->private_module)) {
            $wp = 'private';
            include_once($this->private_module);
        } 
        elseif (file_exists($this->default_module)) {
            $wp = 'default';
            include_once($this->default_module);
        }
        else {
            log_action("$module module not found",__FILE__,__LINE__);
            return;
        }

        if (class_exists($module)) {
            $this->available_methods = get_class_methods($module);
            $this->module = $module;
        }
        else {
            log_action("class $module does not exists on $wp path!",__FILE__,__LINE__);
            return;
        }
    }

    public function has_method($method) {
        return (!empty($this->available_methods) && in_array($method,$this->available_methods));
    }

    public function init() {
        $module_name = $this->module;
        $module_name::init();
        // $this->module::init(); # php 7
    }

    # Module
    ## results_specieslist
    # File
    ## results_specieslist.php
    # Description
    ## 
    # Methods
    ## print_speciestable, print_citetable, print_js
    function results_specieslist($action,$params,$pa=array()) {
        $mf = new results_specieslist();
        if ($action=='print_speciestable') {
            return $mf->print_speciestable($params);
        }
        elseif ($action=='print_citetable') {
            return $mf->print_citetable($params);
        } 
        elseif ($action=='print_js') {
            return $mf->print_js($params);
        }
    }
    
    # Module
    ## results_summary
    # File
    ## results_summary.php
    # Description
    ## 
    # Methods
    ## print_table
    function results_summary($action,$params,$pa=array()) {
        $mf = new results_summary();
        if ($action=='print_table') {
            return $mf->print_table($params);
        }
    }

    # Module
    ## results_asHtmlTable
    # File
    ## results_asHtmlTable.php
    # Description
    ## 
    ## Create simple results table
    ## Max 2000 rows
    ## Not used anywhere
    # Methods
    ## print_table 
    function results_asHtmlTable($action,$params,$pa=array()) {
        $mf = new results_asHtmlTable();
        if ($action=='print_table') {
            return $mf->print_table($params,$pa[0],$pa[1]);
        }
    }

    # Module
    ## results_asTable
    # File
    ## results_asTable.php
    # Description
    ## full size roll table
    # Methods
    ## print_table_page, init_table
    function results_asTable($action,$params,$pa=array()) {
        $mf = new results_asTable();
        if ($action=='print_table_page') {
            $mf->print_table_page($params);
        }
        elseif ($action=='init_table') {
            return $mf->init_table($params,$pa[0]);
        }
    }

    # Module
    ## results_asList
    # File 
    ## results_asList.php
    # Description
    ## Create foldable slide reference list
    # Called
    ## results_builder()
    # Methods
    ## print_roll_list, print_slide, header_assemble, body_assemble
    function results_asList($action,$params,$pa=array()) {
        if ($action=='print_roll_list') {
            $mf = new results_asList();
            return $mf->print_roll_list($params,$pa[0]);
        }
        elseif ($action=='print_slide') {
            $mf = new slide();
            return $mf->print_slide($params,$pa[0],$pa[1],$pa[2]);
        }
    }

    # Module
    ## results_asGPX 
    # File 
    ## results_asGPX.php
    # Description
    ## 
    # Methods
    # print_data
    function results_asGPX($action,$params,$pa=array()) {
        $mf = new results_asGPX();
        if ($action=='print_data') {
            return $mf->print_data($params,$pa[0],$pa[1]);
        }
    }

    # Modules
    ## results_asCSV
    # File
    ## results_asCSV.php
    # Description
    ## 
    # Methods
    ## print_data
    # Example
    ## sep=;quote=\"
    function results_asCSV($action,$params,$pa=array()) {
        $mf = new results_asCSV();
        if ($action=='print_data') {
            return $mf->print_data($params,$pa[0],$pa[1],$pa[2],$pa[3]);
        }
    }

    # Module
    ## results_asJSON
    # File
    ## results_asJSON.php
    # Description
    ## 
    # Methods
    ## print_data
    function results_asJSON($action,$params,$pa=array()) {
        $mf = new results_asJSON();
        if ($action=='print_data') {
            return $mf->print_data($params,$pa[0],$pa[1]);
        }
    }

    # Module
    ## results_asSHP
    # File
    ## results_asSHP.php
    # Description
    ## pa = $filename='',$data
    ## 
    # Methods
    ## print_data
    function results_asSHP($action,$params,$pa=array()) {
        $mf = new results_asSHP();
        if ($action=='print_data') {
            return $mf->print_data($params,$pa[0],$pa[1]);
        }
    }

    # Module
    ## results_buttons
    # File
    ## results_buttons.php
    # Description
    ## 
    # Methods
    ## print_buttons
    function results_buttons ($action,$params,$pa=array()) {
        $mf = new results_buttons();
        if ($action=='print_buttons') {
            return $mf->print_buttons($params,$pa[0]);
        }
    }

    # Module
    ## photos
    # File
    ## photos.php
    # Description
    ## photo or other file attchements uploader extension for data upload
    # Methods
    ## init, upload_box, print_js, photo_viewer
    function photos($action,$params,$pa=array()) {
        $mf = new photos();
        if ($action=='upload_box') {
            return $mf->upload_box($params);
        }
        elseif ($action=='print_js') {
            return $mf->print_js($params);
        }
        elseif ($action=='photo_viewer') {
            return $mf->photo_viewer($params,$pa[0]);
        }
        elseif ($action=='init') {
            return $mf->init($params, $pa);
        }

    }
   
    # Module
    ## box_load_selection
    # File
    ## box_load_selection.php
    # Description
    ## Map Filter Functions
    ## These functions returns with a html table which displayed beside the map window
    ## These are optional boxes. Setting are in the biomaps db projects' table.
    ## Load prevously saved spatial queries' polygons
    # Methods
    ## print_box, adminPage, getMenuItem, profileItem, query_button, print_js
    function box_load_selection($action,$params,$pa=array()) {
        $mf = new box_load_selection();
        if ($action=='print_box') {
            return $mf->print_box($params);
        }
        elseif ($action=='getMenuItem') {
            return $mf->getMenuItem();
        }
        elseif ($action=='adminPage') {
            return $mf->adminPage();
        }
        elseif ($action=='profileItem') {
            return $mf->profileItem();
        }
        elseif ($action=='print_js') {
            return $mf->print_js($params);
        }

    }

    # Module
    ## box_load_coord
    # File
    ## box_load_coord.php
    # Description
    ## Show given coordinates position on the map
    # Methods
    ## print_box, print_js
    # Example
    ## wgs84:4326
    ## eov:23700
    function box_load_coord($action,$params,$pa=array()) {

        $mf = new box_load_coord();
        if ($action=='print_box') {
            return $mf->print_box($params);
        }
        elseif ($action=='print_js') {
            return $mf->print_js($params);
        }
    }

    # Module
    ## box_load_last_data
    # File
    ## box_load_last_data.php 
    # Description
    ## Query last data
    # Methods
    ## print_box, query_button, limits, ajax, print_js
    # Example
    ## 20
    function box_load_last_data($action,$params,$pa=array()) {

        $mf = new box_load_last_data();
        if ($action=='print_box') {
            return $mf->print_box();
        }
        elseif ($action=='limits') {
            return $mf->limits($params,$pa[0]);
        }
        elseif ($action=='ajax') {
            return $mf->ajax($params,$pa[0]);
        }
        elseif ($action=='print_js') {
            return $mf->print_js($params);
        }
    }
    
    # Module
    ## box_custom
    # File
    ## box_custom.php
    # Description
    ## Custom box - only user defined version exists 
    # Methods
    ## __construct, print_box, print_js, query_button
    function box_custom($action,$params,$pa=array()) {

        $mf = new box_custom($this->private_path);
        if ($action=='print_box') {
            return $mf->print_box($params);
        }
        elseif ($action=='print_js') {
            return $mf->print_js($params);
        }
    }

    # Module
    ## text_filter
    # File
    ## text_filter.php
    # Description
    ## Taxon and other text filters
    ## create boxes
    ## assemble WHERE part of query string
    ## pa = $mc=null,$post_val=null,$custom_table=null
    # Methods
    ## print_box, print_js, ajax, assemble_where, query_button
    # Example
    ## obm_taxon
    ## crings::colour_rings
    ## obm_datum
    ## obm_uploading_date
    ## obm_uploader_user
    ## d.szamossag:nested(d.egyedszam):autocomplete
    ## d.egyedszam:values():
    ## obm_files_id
    ## observer::autocomplete
    function text_filter($action,$params,$pa=array()) {

        if ($action=='print_box') {
            $mf = new text_filter();
            return $mf->print_box($params);
        }
        elseif ($action=='assemble_where') {
            $mf = new query_builder();
            return $mf->assemble_where($params,$pa[0],$pa[1],$pa[2]);
        }
        elseif ($action=='print_js') {
            $mf = new text_filter();
            return $mf->print_js($params);
        }
        elseif ($action=='ajax') {
            $mf = new text_filter();
            return $mf->ajax($params,$pa[0]);
        }
    }

    # Module
    ## transform_data
    # File
    ## transform_data.php
    # Description 
    ## Transform data
    ## In result list it can transform data as need
    ## E.g. geometry to wkt
    ## $pa = $text,$col,$data_id=''
    # Methods
    ## text_transform
    # Example
    ## obm_geometry:geom
    ## obm_uploading_id:uplid
    ## tema:mmm
    function transform_data($action,$params,$pa=array()) {
        $mf = new transform_data();
        if ($action=='text_transform') {
            return $mf->text_transform($params,$pa[0],$pa[1],$pa[2]);
        }
    }

    # Module
    ## results_asStable
    # File
    ## results_asStable.php
    # Description
    ## print_roll_table
    ## compact table Stable
    # Methods
    ## print_roll_table
    # Example
    ## faj
    ## magyar
    ## dt_to
    function results_asStable($action,$params,$pa=array()) {
        $mf = new results_asStable();
        if ($action=='print_roll_table') {
            return $mf->print_roll_table($params,$pa[0]);
        }
    }
    
    # Module
    ## allowed_columns
    # File
    ## allowed_columns.php
    # Description
    ## columns visible for users in different access level
    ## it was the no_login_columns
    # Methods
    ## return_columns, return_gcolumns, return_general_columns
    # Example
    ## for_sensitive_data:faj,obm_geometry,obm_id
    ## for_no-geom_data:faj,obm_geometry,obm_id,egyedszam
    ## for_general:faj
    function allowed_columns($action,$params,$pa=array()) {
        $mf = new allowed_columns();
        if ($action=='return_columns') {
            return $mf->return_columns($params,$pa[0]);
        }
        elseif ($action=='return_gcolumns') {
            return $mf->return_gcolumns($params,$pa[0]);
        }
        elseif ($action=='return_general_columns') {
            return $mf->return_general_columns($params,$pa[0]);
        }
    }

    # Module
    ## bold_yellow
    # File
    ## bold_yellow.php
    # Description
    ## bold yellow labels
    # Methods
    ## mark_text
    # Example
    ## faj
    ## egyedszam
    ## datum
    function bold_yellow($action,$params,$pa=array()) {
        $mf = new bold_yellow();
        if ($action=='mark_text') {
            return $mf->mark_text($params);
        }
    }

    # Module
    ## extra_params
    # File
    ## extra_params.php
    # Description
    ## 
    # Methods
    ## add_params, format_input
    function extra_params($action,$params,$pa=array()) {
        $mf = new extra_params();
        if ($action=='add_params') {
            return $mf->add_params($params,$pa[0]);
        }
        elseif ($action=='format_default_input') {
            return $mf->format_input($params,$pa[0],$pa[1]);
        }
    }

    # Module
    ## additional_columns
    # File
    ## additional_columns.php
    # Description
    ## additional columns
    ## use it together with the join_tables module
    ## return with an array:
    ## co [0] columns array
    ## c  [1] column name assoc array
    # Methods
    ## return_columns
    # Example
    ## meta_1
    ## dmi.egyedszam
    function additional_columns($action,$params,$pa=array()) {
        $mf = new additional_columns();
        if ($action=='return_columns') {
            return $mf->return_columns($params);
        }
    }

    // deprecated
    // join table to use additional columns
    // use it together with the additional_columns module
    // RETURN: join command and column list and visible names list
    // [0] column name , separated list
    // [1] prefixed column names array: all column which defined in the database columns
    // [2] visible names array of array by JOIN
    function join_tables($action,$params,$pa=array()) {
        $mf = new join_tables();
        if ($action=='return_joins') {
            return $mf->return_joins($params);
        }
    }

    /* user specified sanp to grid
     * deprecated
     * */
    function snap_to_grid($action,$params,$pa=array()) {
        $mf = new snap_to_grid();
        if ($action=='geom_column') {
            return $mf->geom_column($params,$pa[0]);
        } elseif ($action=='geom_column_join') {
            return $mf->geom_column_join($params);
        } elseif ($action=='rules_join') {
            return $mf->rules_join($params);
        }
    }

    # Module
    ## restricted_data
    # File
    ## restricted_data.php
    # Description
    ## Rule based data restriction
    ## Old Milvus app used something similar: restricted_data -> titkos_adat()
    ## Used in results_builder.php 
    # Methods
    ## rule_data, apply_rules
    function restricted_data ($action,$params,$pa=array()) {
        $mf = new restricted_data();
        if ($action=='rule_data') {
            return $mf->rule_data($params,$pa[0]);
        }
    }

    # Module
    ## identify_point
    # File
    ## identify_point.php
    # Description
    ## a map page extension
    # Methods
    ## return_data, print_button
    # Example
    ## faj
    ## dt_from
    ## dt_to
    function identify_point ($action,$params,$pa=array()) {
        $mf = new identify_point();
        if ($action=='return_data') {
            return $mf->return_data($params,$pa[0],$pa[1]);
        }
        elseif ($action=='print_button') {
            return $mf->print_button($params);
        }
    }

    # Module
    ## custom_notify
    # File
    ## custom_notify.php
    # Description
    ## very special module
    ## postgres listen functinality and email alerts
    # Methods
    ## unlisten, listen, notify, email
    function custom_notify ($action,$params,$pa=array()) {
        $mf = new notify($params);
        if ($action=='unlisten') {
            $mf->unlisten($params);
        } elseif ($action=='listen') {
            $mf->listen($params);
        } elseif ($action=='notify') {
            $mf->notify($params);
        } elseif ($action=='email') {
            $mf->email($params);
        }
        return;
    }

    # Module
    ## custom_data_check
    # File
    ## custom_data_check.php
    # Description
    ## Custom checks for upload data
    ## $function='',$data_pile='',$column='',$header=''
    # Methods
    ## list_checks, custom_check, colour_rings
    function custom_data_check ($action,$params,$pa=array()) {
        $mf = new custom_data_check();
        if ($action=='list') {
            return $mf->list_checks($params);
        } elseif ($action=='check') {
            return $mf->custom_check($params,$pa[0],$pa[1],$pa[2],$pa[3]);
        }
        return;
    }

    # Module
    ## custom_filetype
    # File
    ## custom_filetype.php
    # Description
    ## Custom file preparation. E.g. observado style CSV
    ## $pa =$file = '',$filetype = null
    # Methods
    ## option_list, custom_read 
    function custom_filetype ($action,$params,$pa=array()) {
        $mf = new custom_filetype();
        if ($action=='option_list') {
            return $mf->option_list($params);
        }
        elseif ($action == 'custom_read') {
            return $mf->custom_read($pa[0],$pa[1]);
        }
    }

    # Module
    ## create_pg_user
    # File
    ## create_pg_user.php
    # Description
    ## Create postgres users for the project 
    # Methods
    ## new_pg_user,  profileItem, print_js
    # General module
    function create_pg_user ($action,$params,$pa=array()) {
        $mf = new create_pg_user();
        if ($action=='create_pg_user') {
            return $mf->new_pg_user($params);
        } elseif ($action=='profileItem') {
            return $mf->profileItem();
        } elseif ($action=='print_js') {
            return $mf->print_js($params);
        }
    }

    # Module
    ## custom_admin_pages 
    # File
    ## custom_admin_pages.php
    # Description
    ## Custom admin pages module 
/*    function custom_admin_pages($action,$params,$pa=array()) {
        if (method_exists('custom_admin_pages',$action)) {
            $mf = new custom_admin_pages($action, $params);

            if ($mf->error) {
                $mf->displayError();
                return $mf->retval;;
            }
            else {
                return $mf->retval;
            }
        }
        else {
            return; 
        }

    }
 */

    # Module
    ## grid_view 
    # File
    ## grid_view.php
    # Description
    ## Grid type chooser for restricted data displaying
    # Methods
    ## print_box, default_grid_geom, get_grid_layer, filter_query
    # Example
    ## layer_options:kef_5 (dinpi_grid), utm_2.5 (dinpi_grid), utm_10 (dinpi_grid), utm_100 (dinpi_grid), original (dinpi_points,dinpi_grid), etrs(dinpi_grid)
    function grid_view($action,$params,$pa=array()) {
        $mf = new grid_view();
        if ($action=='print_box') {
            return $mf->print_box($params);
        }
        elseif ($action=='default_grid_geom') {
            return $mf->default_grid_geom($params);
        }
        elseif ($action=='get_grid_layer') {
            return $mf->get_grid_layer($params);
        }
        elseif ($action=='filter_query') {
            return $mf->filter_query($params);
        }
    }

    # Module
    ## text_filter2
    # File
    ## text_filter2.php
    # Description
    ## Taxon and other text filters
    ## create boxes
    ## assemble WHERE part of query string
    # Methods
    ## print_js, module_params_interface, print_box, ajax, assemble_where, query_button
    function text_filter2($action,$params,$pa=array()) {
        if (in_array($action, ['print_box','print_js','ajax','module_params_interface'])) {
            $mf = new text_filter2($action,$params,$pa);
            if ($mf->error) 
                $mf->displayError();
            return $mf->retval;
        }
        elseif ($action=='assemble_where') {
            $mf = new query_builder();
            return $mf->assemble_where($params,$pa[0],$pa[1],$pa[2]);
        }
    }

    # Module
    ## download_restricted
    # File
    ## download_restricted.php
    # Description
    ## Restrict download 
    ## create boxes
    ## assemble WHERE part of query string
    # Methods
    ## __construct, getMenuItem, profileItem, adminPage, downloadAccepted, newRequest, getMailText, displayError, ajax, print_js, init
    function download_restricted ($action,$params,$pa=array()) {
        $mf = new download_restricted($action,$params,$pa);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

    # Module
    ## ebp
    # File
    ## ebp.php
    # Description
    ## EuroBirdPortal API Communication Module
    # Methods
    ## getMenuItem, adminPage, profileItem, ajax, print_js
    function ebp ($action,$params,$pa=array()) {
        $mf = new ebp($action,$params,$pa);

        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

    # Module
    ## read_table
    # File
    ## read_table.php
    # Description
    ## Read a table or view into a dynimac html table and give an encrypted link to it
    # Methods
    ## check_table, list_links, adminPage, getMenuItem
    # Example
    ## public.dinpi:faj DESC,szamossag
    function read_table ($action,$params,$pa=array()) {
        $mf = new read_table();

        if ($action=='check_table') {
            return $mf->check_table($params,$pa);
        } elseif ($action == 'list_links') {
            return $mf->list_links($params);
        } elseif ($action == 'decode_link') {
            return $mf->decode_link($params,$pa);
        }
        elseif ($action=='getMenuItem') {
            return $mf->getMenuItem();
        }
        elseif ($action=='adminPage') {
            return $mf->adminPage();
        }
    }

    # Module
    ## massive_edit
    # File
    ## massive_edit.php
    # Description
    ## 
    # Methods
    ## 
    # Example
    ##
    function massive_edit ($action,$params,$pa=array()) {
        $mf = new massive_edit();

        if ($action=='print_button') {
            return $mf->print_button($params,$pa);
        }
    }

    # Module
    ## validation
    # File
    ## validation.php
    # Description
    ## Internal API for validation algorithms 
    # Methods
    ## __construct, getMenuItem, adminPage, getMailText, displayError, ajax, print_js, init
    # General module
    function validation ($action,$params,$pa=array()) {
        $mf = new validation($action,$params,$pa);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

    # Module
    ## move_project
    # File
    ## move_project.php
    # Description
    ## Export - import projects with users, settings, data
    # Methods
    ## __construct, getMenuItem, adminPage, ajax, print_js, read_biomaps_file, delete_tmp_dir
    # General module
    function move_project ($action,$params,$pa=array()) {
        $mf = new move_project($action,$params,$pa);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

    # Module
    ## simple_search
    # File
    ## simple_search.php
    # Description
    ## One search box module
    # Methods
    ## __construct, getMenuItem, profileItem, adminPage, getMailText, displayError, ajax, print_js, init
    function simple_search ($action,$params,$pa=array()) {
        $mf = new simple_search($action,$params,$pa);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

    # Module
    ## list_manager
    # File
    ## list_manager.php
    # Description
    ## Manage lists used for uploads and queries
    # Methods
    ## __construct, getMenuItem, adminPage, displayError, ajax, print_js, init, print_css
    function list_manager ($action,$params,$pa=array()) {
        $mf = new list_manager($action,$params,$pa);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

}
?>
