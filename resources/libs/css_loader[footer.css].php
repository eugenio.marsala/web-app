<?php
function setExpires($expires) {
header(
'Expires: '.gmdate('D, d M Y H:i:s', time()+$expires).'GMT');
}
setExpires(300);

header("Content-type: text/css", true);

if (file_exists('../css/private/footer.css')) {

    include('../css/private/footer.css');

} elseif (file_exists('../css/footer.css')) {
    
    include('../css/footer.css');

}
?>
