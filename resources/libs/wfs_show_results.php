<?php
/* This is an AJAX call responder
 * functions.js
 *      LoadResults
 *      WFSGet depreceted
 *
 * */

session_start();

$j = json_decode($_POST['response_control']);
if(isset($_SESSION['show_results']['type'])) {
    $method = $_SESSION['show_results']['type'];
} else {
    $method = $j->{'method'};
    $_SESSION['show_results']['type'] = $method;
}

// rolling output
if($method=='stable') {
    $_SESSION['show_results']['rowHeight'] = '30';
    $_SESSION['show_results']['cellWidth'] = '120';
    $_SESSION['show_results']['borderRight'] = '1px solid lightgray';
    $_SESSION['show_results']['borderBottom'] = '1px solid lightgray';
} else if ($method == 'list') {
    $_SESSION['show_results']['rowHeight'] = '60';
    $_SESSION['show_results']['cellWidth'] = '400';
    $_SESSION['show_results']['borderRight'] = 'none';
    $_SESSION['show_results']['borderBottom'] = 'none';
}

include_once(getenv('OB_LIB_DIR').'results_builder.php');
$r = new results_builder(['method'=>$method,'specieslist'=>$j->{'specieslist'},'buttons'=>$j->{'buttons'},'summary'=>$j->{'summary'}]);
//if ($r->id_queue(array("id"=>$_SESSION['wfs_array']['id'],"geom"=>$_SESSION['wfs_array']['geom'],"data"=>$_SESSION['wfs_array']['data']))) {
if( $r->results_query() ) {
    if ($r->rq['results_count']) {
        // print out table, list or something
        $r->printOut();
    } else {
        //echo json_encode(array('res'=>'','geom'=>'','error'=>$r->error));
        echo common_message('error',str_no_results_found);
    }
    exit;
}
echo common_message('error',$r->error);
//echo json_encode(array('res'=>'','geom'=>'','error'=>$r->error));
#unset($_SESSION['wfs_array']);
unset($_SESSION['roll_array']);
?>
