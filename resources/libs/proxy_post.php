<?php
# that is the mapserver wrapper!
# this env-var was set in .htaccess

# Options -Indexes
# RewriteEngine On
# RewriteCond %{QUERY_STRING} !MAP=PMAP [NC]
# RewriteRule (.*) /mapserv [R=301,L]
# SetEnvIf Request_URI "(.*)" ACCESS=public
# <Files *.map>
#    Order Deny,Allow
#    Deny from all
#    #Allow from 127.0.0.1
# </Files>

//ini_set("log_errors", 1);
//ini_set("error_log", "/tmp/php-error.log");

#xdebug_start_trace('/tmp/xdebug');
require_once(getenv('OB_LIB_DIR').'modules_class.php');
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

//önálló hívás lehetséges!
if(!isset($_SESSION['st_col'])) { 
    st_col(PROJECTTABLE);
}

$accessLevel = isset($_SERVER['ACCESS']) ? $_SERVER['ACCESS'] : '';

if ($accessLevel=='public') {
    # Set the map path 
    if (defined('PUBLIC_MAPFILE'))
        $MAP = PUBLIC_MAPFILE;
    else
        $MAP = realpath('./public.map');
}
elseif($accessLevel=='private') {
    # Set the map path 
    if (defined('PRIVATE_MAPFILE'))
        $MAP = PRIVATE_MAPFILE;
    else
        $MAP = realpath('./private.map');
} else {
    # Set the map NULL path 
   $MAP = ''; 
}

$XML_TYPES = array('GetCapabilities','DescribeFeatureType','GetFeature','GetFeatureInfo','GetSchemaExtension');
$IMG_TYPES = array('GetMap','GetLegendGraphic');

if (defined('MAPSERVER'))
    $MAPSERVER = MAPSERVER."?";
else
    $MAPSERVER = "http://localhost/cgi-bin/mapserv?";

$referer = '';

if ($MAP!=''){
    /* ez amiatt kell mert a mapfile extent nagyon le tud maradni és így röptében, 
     * szöveges lekérdezéshez tudunk kb jó kiterjedést adni 
     * viszont jó lenne nem megismételni ezt a lekérdezést itt 100x - cache vagy constans!!!
     */
    // USING XCACHE !!!
    $cache = XCache::getInstance();
    if (isset($cache->{PROJECTTABLE."_bbox"})) {
        $extent = $cache->{PROJECTTABLE."_bbox"};
    } else {
        if (defined('POSTGIS_V') and POSTGIS_V>'2.0')
            //PostGis 2.1
            $res = pg_query($ID,"select st_expand(ST_Estimated_Extent('public','".PROJECTTABLE."','{$_SESSION['st_col']['GEOM_C']}'),0.1) as extent");
        else
        //    //PostGis <2.1
            $res = pg_query($ID,"select ST_Extent({$_SESSION['st_col']['GEOM_C']}) as extent from ".PROJECTTABLE);
        pg_close($ID);
        $extent = "";
        if ($res) {
            $row = pg_fetch_assoc($res);
            $extent = preg_replace("/[^0-9., ]/",'',$row['extent']);
            $extent = preg_replace("/ /",",",$extent);
            /*  Jobb megoldás esetén kiszedendő */
            #if ($e!='') {
            #    $e = "BBOX=$e";
            #}
        }
        $cache->set(PROJECTTABLE."_bbox", $extent, 60); // save for 1 minutes 
    }
    $q = array();
    
    //access control
    $access_controls = array();
    if (isset($_SESSION['Tid']))
        $uid = $_SESSION['Tid'];
    else
        $uid = 0;
    $rel = "IN";
    $uid2 = $uid;
    if (has_access('master')) {
        //testing admin access level
        $rel = "NOT%20IN";
        $uid2 = 0;
    }
    $access_controls['uid'] = "$uid";
    $access_controls['uid2'] = "$uid2";
    $access_controls['rel'] = "$rel";

    parse_str($_SERVER['QUERY_STRING'], $HttpQuery);
    $HttpQuery['ACCESS']=$MAP;
    $HttpQuery['MAP'] = "PMAP";
    
    /* we don't print the output xml if has refered from the web apps... */
    if (isset($_SERVER['HTTP_REFERER']))
        $referer = implode("/",array_slice(preg_split("/\//",$_SERVER['HTTP_REFERER']), 2,-1));
 
    # unhide the query string from query_builder.php
    if (isset($HttpQuery['qstr'])) {
        //query set parameters
        if (isset($_SESSION['query_build_string'][$HttpQuery['qstr']])) {
            $HttpQuery['qstr']=$_SESSION['query_build_string'][$HttpQuery['qstr']];
            $HttpQuery['BBOX'] = "$extent";
        } else {
            $HttpQuery['qstr']='';
            $HttpQuery['SERVICE'] = "WFS";
            $HttpQuery['VERSION'] = "1.1.0";
            $HttpQuery['REQUEST']="GetCapabilities";
            $HttpQuery['BBOX'] = "$extent";
        }
    }
    elseif (isset($HttpQuery['qstr-inter'])) {
        //geometry parameters
        if (isset($_SESSION['query_build_string'][$HttpQuery['qstr-inter']])) {
            $HttpQuery['qstr']=$_SESSION['query_build_string'][$HttpQuery['qstr-inter']];
            unset($HttpQuery['qstr-inter']);
            $HttpQuery['BBOX'] = $extent;
        } else {
            $HttpQuery['qstr']='';
            unset($HttpQuery['qstr-inter']);
            $HttpQuery['SERVICE'] = "WFS";
            $HttpQuery['VERSION'] = "1.1.0";
            $HttpQuery['REQUEST']="GetCapabilities";
            $HttpQuery['BBOX'] = "$extent";
        }
    }
    elseif (isset($HttpQuery['repeat'])) {
        $_SESSION['wfs_rows_counter']=$HttpQuery['repeat'];
        //pages, requets from maps.js
        if(isset($_SESSION['repeat_url'])) {
            parse_str($_SESSION['repeat_url'],$HttpQuery);
        } else {
            exit;
        }
    }
    $_SESSION['repeat_url'] = http_build_query($HttpQuery);
    // Close session write
    session_write_close();

    /* show xml response in local query
     * This called from the maps.js->#customLayer".click function
     * when somebody load a custom shp file as a new Layer */
    if (isset($HttpQuery['showXML'])) {
        $referer = ''; 
    }

    if (isset($_SESSION['Tth'])) {
        $password = decode($_SESSION['Tth'],session_id());
        $auth = sprintf('Authorization: Basic %s',base64_encode("{$_SESSION['Tmail']}:$password") );
        $opts = array('http'=>array('method'=>'POST','header'=>"$auth; Content-type: application/x-www-form-urlencoded\r\n",'content'=>http_build_query($HttpQuery)));
        $context = stream_context_create($opts);
        $fp = fopen($MAPSERVER, 'r', false, $context);
    } else { 
        $opts = array('http'=>array('method'=>'POST','header'=>"Content-type: application/x-www-form-urlencoded\r\n",'content'=>http_build_query($HttpQuery)));
        $context = stream_context_create($opts);
        $fp = fopen($MAPSERVER, 'r', false, $context);
        log_action(json_encode($opts));
        log_action(stream_get_contents($fp));
    }
    
    if (isset($HttpQuery['REQUEST'])) {
        if (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $XML_TYPES))!==false) $type = 'xml';
        elseif (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $IMG_TYPES))!==false) $type = 'image';
        else $type = 'unknown';
    } else {
        # skip_processing: paging...
        $type = '';
    }

    if ($type=='xml') {
        # xml output
        $ses = session_id();
        
        # rewrite the wfs:FeatureCollection xsi:schemaLocation
        //$content = preg_replace("/http:\/\/localhost\/cgi-bin\/mapserv/ius","http://localhost{$_SERVER['PHP_SELF']}/",$content);
        $e = 0;

        $list = glob(OB_TMP."conts_$ses*.xml");
        natsort($list);
        $file = array_pop($list);
        $m = array();
        if (preg_match('/(\d)+\.xml/',$file,$m)) {
            $e = $m[1];
        }
        while (file_exists(OB_TMP."conts_$ses-$e.xml")) {
            $e++;
        }
        $filename=OB_TMP."conts_$ses-$e.xml";
        $dest = fopen($filename, 'w');
        if ($fp===false) {
            error_log("WFS XML READ ERROR");
            exit;
        }
        if ($dest===false) {
            error_log("WFS XML WRITE ERROR");
            exit;
        }

        if(!stream_copy_to_stream($fp,$dest)) {
            error_log( "ERROR WHILE STREAMING XML DATA TO FILENAME: $filename\n" );
            exit;
        }

        //file_put_contents("/mnt/data/tmp/conts_$ses-$e.xml", $content);
        /* we don't print the output xml if has refered from the web apps... 
         * if we don't want to draw with the setHTML() */
        if (URL!=$referer) {
            header('Content-type: text/xml');
            $handle = fopen($filename, "rb");
            echo fread($handle, filesize($filename));
            //echo $content;
        }
        exit;
    } elseif($type=='image') {
        if (isset($HttpQuery['FORMAT'])) $format = $HttpQuery['FORMAT'];
        else $format = 'image/png';
        header("Content-type: $format");
        echo stream_get_contents($fp);
        exit;
    } else {
        echo stream_get_contents($fp);
        //echo $content;
        exit;
    }
} else {
    print "No map defined.";
    exit;
}

#xdebug_stop_trace();


?>
