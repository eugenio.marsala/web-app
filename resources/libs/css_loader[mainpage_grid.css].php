<?php
function setExpires($expires) {
header(
'Expires: '.gmdate('D, d M Y H:i:s', time()+$expires).'GMT');
}
setExpires(300);

header("Content-type: text/css", true);

if (file_exists('../css/private/mainpage_grid.css')) {

    include('../css/private/mainpage_grid.css');

} elseif (file_exists('../css/mainpage_grid.css')) {
    
    include('../css/mainpage_grid.css');

}
?>
