<?php
/* Update given data field
 * arguments: POST: 
 * name:    column name
 * id:      row id
 * text:    text
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
if(!isset($_SESSION['Tproject'])) { 
    require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
require_once(getenv('OB_LIB_DIR').'languages.php');

if (!isset($_POST['name'])) return;
else $post_name=preg_replace("/[^0-9A-Za-z-_]/","",$_POST['name']);

if (!isset($_POST['id'])) return;
else {
    $post_id = preg_replace("/^$post_name/","",$_POST['id']);
    $post_id = preg_replace("/[^0-9]/","",$post_id); 
}
if ($post_id=='') return;

if (!isset($_POST['text'])) return;
else $post_text = $_POST['text'];


# módosítási jog tesztelése az adott adatsorra
if(!rst('mod',$post_id)) return;

$nq_text = strip_tags($post_text);

//nem biztos, hogy ez tartható!!
if(strlen($nq_text)>1024) return;

if ($post_name=='obm_comments') return; // $text = $text." [by {$_SESSION['Tname']}]";
if ($post_name=='obm_validation') return; // no way to modify the validation directly

$text = quote($nq_text);

# hozzáférés kezelés...    
if (!isset($_SESSION['Tid'])) 
    $mfid = '0';
else
    $mfid = $_SESSION['Tid'];

$COLUMNS = array_merge(array('obm_id'),dbcolist('columns',$_SESSION['current_query_table']));
 

#if ($post_name == 'comments_') {
#    $cmd = "UPDATE ".PROJECTTABLE." SET comments_=array_append(comments_,$text), modifier_id='$mfid' WHERE $ID_COL='$post_id'";
#} else 

$cmd = sprintf('SELECT 1 FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote($_POST['target']));
$res = pg_query($BID,$cmd);
if (pg_num_rows($res)) {
    $data_table = $_POST['target'];
} else
    $data_table = $_SESSION['current_query_table'];

if ($data_table != $_SESSION['current_query_table']) {
    $pattern=sprintf("%s_",$data_table);
    if (preg_match("/^$pattern([a-z0-9_]+)$/",$post_name,$m)) {
        $post_name = $m[1];
        $COLUMNS[] = $m[1];
    }
    echo common_message('error',"Sorry, joined table update not possible yet: $data_table:$post_name");
    return;
}

// final check
if (!in_array($post_name,$COLUMNS)) {
    echo common_message('error',"Invalid request: $post_name");
    return;
}

if ($post_name == $_SESSION['st_col']['GEOM_C'])
    $text = "ST_GeomFromText($text, {$_SESSION['st_col']['SRID_C']})";

elseif ($post_name == 'obm_files_id') {
    $references = explode(',',$nq_text);
    //lekérdezem azokat a fájl neveket, amik már megvannak a fájl táblában, elvileg csak olyan fájl nevek vannak a listában,
    //de mivel kézzel módosítható, ezért kell az ellenőrzés
    $cmd = sprintf('SELECT array_to_string(array(SELECT reference FROM system.files WHERE reference IN (%s) AND project_table=\'%s\'),\',\') AS p',implode(',',array_map('quote',$references)),PROJECTTABLE);
    $pres = pg_query($ID,$cmd);
    $conid = ""; 
    $file_id = "";
    if (pg_num_rows($pres)) {
        $prow = pg_fetch_assoc($pres);
        $conid = false;
        $cmd = sprintf('SELECT obm_files_id FROM %s WHERE obm_id=%d AND obm_files_id IS NOT NULL',$data_table,$post_id);
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            if ($row['obm_files_id']) {
                $conid = $row['obm_files_id'];
                #$cmd = sprintf('DELETE FROM system.file_connect WHERE conid=\'%s\' AND project_table=\'%s\'',$conid,PROJECTTABLE);
                $cmd = sprintf('DELETE FROM system.file_connect WHERE conid=\'%s\' RETURNING file_id',$conid);
                $res = pg_query($ID,$cmd);
                while ($row = pg_fetch_assoc($res)) {
                    $file_id = $row['file_id'];
                    if ($file_id!='' and $prow['p']!='' and isset($_POST['refresh_add']) and $_POST['refresh_add']=='add') {
                        // update files list
                        // refresh-add class in edit() button
                        $cmd = sprintf("SELECT reference FROM system.files WHERE id=%d AND project_table=%s",$file_id,quote(PROJECTTABLE));
                        $res3 = pg_query($ID,$cmd);
                        while ($row3 = pg_fetch_assoc($res3)) {
                            $prow['p'] .= ",{$row3["reference"]}";
                        }
                    }
                }
            }
        }
        // minden fotó referencia 
        //  ellenőrzése, hogy létezik a photos táblában?
        //  létezik és valós kép-e a fájl
        //  Ha igen
        //  1) Létrehozunk egy fotó referencia azonosítót: project_id_gid_'photo' ami bekerül a photo oszlopba majd
        //  2) Létrehozunk annyi sort ahány kép van a photo_connect táblában : |project_id_gid_'photo'|fotó id|
        $conid = file_connection_process(explode(',',$prow['p']),$conid,PROJECTTABLE,0,0);
        if (!$conid) {
            echo common_message("fail","Couldn't create file connection.");
        }
    }
                    
    if ($conid) $text = quote($conid);
    else $text = quote('');
}

$cmd = "UPDATE \"".$data_table."\" SET $post_name=$text,obm_modifier_id='$mfid' WHERE obm_id='$post_id'";
$res = pg_query($ID,$cmd);

if ($res and pg_affected_rows($res)) {
    echo common_message("ok","ok");
    unset($_SESSION["slide$post_id"]);
}
else {
    echo common_message("fail","SQL error");
    log_action(pg_last_error($ID),__FILE__,__LINE__);
}
//pg_close($ID);
//else
//    print $cmd;
    
?>
