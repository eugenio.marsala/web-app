<?php
/* Asynchron cml script
 *
 *
 * */
#$logfile = 'background_process_debug.log';
#file_put_contents($logfile, '5');

$session_id = $argv[1];
$vars1 = getenv("allvars_$session_id");
putenv("allvars_$session_id");
$vars = json_decode(base64_decode($vars1),true);

$_SERVER = $vars['SERVER'];

$local_lib_dir = __DIR__;
$local_lib_dir_path = explode('/',$local_lib_dir);
array_pop($local_lib_dir_path);
$local_lib_dir_path = implode('/',$local_lib_dir_path);

putenv("PROJECT_DIR=$local_lib_dir_path/");
putenv("OB_LIB_DIR=$local_lib_dir/");

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS databases.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

fill_stable_with_columns($vars,$session_id,0);

?>
