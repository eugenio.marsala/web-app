<?php

/* Return with a value between 0-3
 * 0 non logined
 * 1 --- 
 * 2 logined and project member
 * 3 logined and (founder or admin/master )
 * */
function prst() {
    global $BID;

    //non-logined
    $rst = 0;

    //logined
    if (isset($_SESSION['Tid']))
        $rst = 1;
    
    if ($rst==1) {
        $acc_cached = obm_cache('get',"prst");
        if ($acc_cached!==FALSE) {
            return $acc_cached;
        }

        // check user is founder or maintainer
        if (has_access('master')) {
            $rst = 3;
        }
        else {
            // check project member
            $cmd = sprintf("SELECT 1 as \"enabled\" FROM project_users WHERE project_table='%s' AND user_id=%d  AND user_status::varchar NOT IN ('0','parked','banned')",PROJECTTABLE,$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)){
                $row = pg_fetch_assoc($res);
                if($row['enabled']=='1') 
                    $rst = 2;
            }
        }
        obm_cache('set',"prst",$rst,300);
    }
    
    return $rst;
}

/* query current restrictions related with the project definitions and login state
 * Check relation to the Project's general rules
 * ACC_LEVEL and MOD_LEVEL defined in local_vars.php
 *
 * */
function rst($acc_method,$data_id='',$table=PROJECTTABLE) {
    global $ID,$BID;

    /*  acc:    read or write access check: 'acc' 'mod'
        c:      a data id
        table:  a project_table

        this function return TRUE if the current access level >= than the function' restriction level
    
        ACC and MOD LEVELS:
        0/public = Open project:              Accessible for Everybody
        1/login = Closed project:             Accessible for Logined Members
        2/group = Paranoid project:           Accessible for specified group members

    */

    $ACC_LEVEL = ACC_LEVEL;
    $MOD_LEVEL = MOD_LEVEL;

    // RIGHTS FOR READING DATA 
    if ($acc_method=='acc' and ( "$ACC_LEVEL" == '0' or "$ACC_LEVEL" == 'public' )) {
        //ACCESSIBLE DATA FOR EVERYBODY
        return TRUE;
    }
    elseif ($acc_method=='acc' and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == 'login' )) {
        //ACCESSIBLE DATA FOR LOGINED USERS
        if(!isset($_SESSION['Tid'])) return FALSE;

        //if(isset($_SESSION['Tid'])) 
        //    return TRUE;

        $acc_cached = obm_cache('get',"rst.$acc_method");
        if ($acc_cached!==FALSE) {
            if ($acc_cached == 'TRUE')
                return TRUE;
            else
                return FALSE;
        }

        //check founder, project_managers and maintainers
        if (has_access('master')) {
            return TRUE;
        }

        // meg kell őrízni a régi státusz szinteket míg ki nem kopnak...!
        $cmd = sprintf("SELECT 1 as \"enabled\" FROM project_users WHERE project_table='%s' AND user_status::varchar NOT IN ('0','parked','banned') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)){
            $row = pg_fetch_assoc($res);
            if($row['enabled']=='1') {
                obm_cache('set',"rst".$acc_method,'TRUE',300);
                return TRUE;
            }
        }
        
        obm_cache('set',"rst".$acc_method,'FALSE',90);
        return FALSE;
    }
    elseif ($acc_method=='acc' and ( "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group' )) {

        //ACCESSIBLE DATA FOR SPECIFIED GROUPS

        if (isset($_SESSION['Tid'])) {
            //check founder, project_managers and maintainers
            if (has_access('master'))
                return TRUE;
    
            $acc_cached = obm_cache('get',"rst$acc_method$table$data_id");
            if ($acc_cached!==FALSE) {
                if ($acc_cached == 'TRUE')
                    return TRUE;
                else
                    return FALSE;
            }

            // parked users get false allways
            $cmd = sprintf("SELECT 1 as \"banned\" FROM project_users WHERE project_table='%s' AND user_status::varchar IN ('0','banned') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if($row['banned']=='1') { 
                    obm_cache('set',"rst".$acc_method.$table.$data_id,'FALSE',300);
                    return FALSE;
                }
            }

            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='')
                $login_groups = $_SESSION['Tgroups'];
            else
                $login_groups = 0;

            
            if ($_SESSION['st_col']['USE_RULES'] and $data_id!='') {
                
                $cmd = sprintf('SELECT CASE WHEN (read && ARRAY[%s] OR sensitivity::varchar IN (\'0\',\'public\')) THEN 1 ELSE 0 END FROM %s_rules WHERE "row_id"=%d AND "data_table"=%s',
                    $login_groups,PROJECTTABLE,$data_id,quote($table));
                $res = pg_query($ID,$cmd);
                if (!pg_num_rows($res)) {
                    // no rules for data
                    obm_cache('set',"rst".$acc_method.$table.$data_id,'FALSE',300);
                    return false;
                }
                $data_acc_row = pg_fetch_assoc($res);
                
                if ($data_acc_row['case'] == 1) {
                    // ok
                    obm_cache('set',"rst".$acc_method.$table.$data_id,'TRUE',300);
                    return true;
                } 
            } elseif ($data_id!='') {
                    // if there is no rules for data lines lets see who is the uploader
                    // it is important because the base policy is FALSE, see below
                    $cmd = sprintf('SELECT uploader_id FROM system.uploadings LEFT JOIN %1$s ON (obm_uploading_id=uploadings.id) WHERE "%1$s".obm_id=\'%2$d\'',$table,$data_id);
                    $res = pg_query($ID,$cmd);
                    if ($row = pg_num_rows($res)) {
                        if ($_SESSION['Trole_id'] == $row['uploader_id']) {
                            obm_cache('set',"rst".$acc_method.$table.$data_id,'TRUE',300);
                            return true;
                        }
                    }
            }
        } else {
            $acc_cached = obm_cache('get',"rst$acc_method$table$data_id",'','',FALSE);
            if ($acc_cached!==FALSE) {
                if ($acc_cached == 'TRUE')
                    return TRUE;
                else
                    return FALSE;
            }

            // rules for non logined users
            if ($data_id!='' and $_SESSION['st_col']['USE_RULES'] ) {
                $cmd = sprintf('SELECT sensitivity FROM %s_rules WHERE row_id=%d AND "data_table"=%s',PROJECTTABLE,$data_id,quote($table));
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $data_acc_row = pg_fetch_assoc($res);
                    if ($data_acc_row['sensitivity'] == 0 or $data_acc_row['sensitivity'] == 'public') {
                        obm_cache('set',"rst".$acc_method.$table.$data_id,'TRUE',3600,FALSE);
                        return true;
                    }
                }
                // no rules for data - default action is not accessible
            }
        }
        //default non handled cases
        obm_cache('set',"rst".$acc_method.$table.$data_id,'FALSE',300);
        return false;
    }
    
    if ($acc_method=='mod' and ( "$MOD_LEVEL" == '2' or "$MOD_LEVEL" == 'group' )) {

        // It is not wikipedia, sorry! Only logged in users can modify data
        if (!isset($_SESSION['Tid'])) return FALSE;

        // This has no meaning without data row id
        if ($data_id == '') return FALSE;

        //MODIFIBLE DATA FOR SPECIFIED GROUPS
        //
        $mod_cached = obm_cache('get',"rst$acc_method$table$data_id");
        if ($mod_cached!==FALSE) {
            if ($mod_cached == 'TRUE')
                return TRUE;
            else
                return FALSE;
        }

        //check founder, project_managers and maintainers
        if (has_access('master'))
            return TRUE;

        if (isset($_SESSION['Tid'])) {

            // parked users get false allways
            $cmd = sprintf("SELECT 1 as \"banned\" FROM project_users WHERE project_table='%s' AND user_status::varchar IN ('0','banned') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if($row['banned']=='1') { 
                    obm_cache('set',"rst".$acc_method,'FALSE',900);
                    return FALSE;
                }
            }

            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups'] != '')
                $login_groups = $_SESSION['Tgroups'];
            else
                $login_groups = 0;

            
            if ($_SESSION['st_col']['USE_RULES']) {

                $cmd = sprintf('SELECT CASE WHEN (write && ARRAY[%s]) THEN 1 ELSE 0 END FROM %s_rules WHERE "row_id"=%d AND "data_table"=%s',$login_groups,PROJECTTABLE,$data_id,quote($table));
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $data_acc_row = pg_fetch_assoc($res);
                    if ($data_acc_row['case'] == 1) {
                        // ok
                        obm_cache('set',"rst".$acc_method.$table.$data_id,'TRUE',300);
                        return true;
                    } 
                }
            } 
            // a feltöltőnek van joga editálni az adatát, ha nincs rá írási joga, akkor is...
            if (!$_SESSION['st_col']['USE_RULES']) {
                // group level project without _rules table
                $cmd = sprintf('SELECT uploader_id FROM system.uploadings LEFT JOIN %1$s ON (obm_uploading_id=uploadings.id) WHERE "%1$s".obm_id=\'%2$d\'',$table,$data_id);
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    if ($_SESSION['Trole_id'] == $row['uploader_id']) {
                        obm_cache('set',"rst".$acc_method.$table.$data_id,'TRUE',300);
                        return true;
                    }
                }
            } 
        } 
        obm_cache('set',"rst".$acc_method.$table.$data_id,'FALSE',300);
 
    } elseif ($acc_method=='mod' and ( "$MOD_LEVEL" == '1' or "$MOD_LEVEL" == 'login') ) {
        // EDITABLE DATA FOR non disabled logined users
        if(!isset($_SESSION['Tid'])) return FALSE;

        //check founder, project_managers and maintainers
        if (has_access('master'))
            return TRUE;

        $cmd = sprintf("SELECT 1 as \"enabled\" FROM project_users WHERE project_table='%s' AND user_status::varchar NOT IN ('0','parked','banned') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)){
            $row = pg_fetch_assoc($res);
            if($row['enabled']=='1') 
                return TRUE;
        }

    }
    elseif ($acc_method=='mod' and ( "$MOD_LEVEL" == '0' or "$MOD_LEVEL" == 'public')) {
        // EDITABLE DATA FOR EVERYBODY
        return TRUE;
    }

    // default 
    return FALSE;
}

function has_access($action, $table = PROJECTTABLE) {
    global $BID;

    if (!isset($_SESSION['Tid'])) {
        return FALSE;
    }

    $status = obm_cache('get',"has_access".$action);
    if ($status !== FALSE ) {
        //return ($status == 'TRUE');
    }
    $user_class = $_SESSION['Tstatus'];
    $roles = $_SESSION['Tgroups'];

    if (is_founder($table)) {
        obm_cache("set","has_access".$action,'TRUE',300);
        return TRUE;
    }

    // master allways has access to everythings
    if ($user_class == 'master') {
        obm_cache("set","has_access".$action,'TRUE',300);
        return TRUE;
    }

    // master should be defined by user_status or founder NOT by actions!!!
    if ($action=='master') {
        obm_cache("set","has_access".$action,'FALSE',300);
        return FALSE;
    }

    $admin_pages = get_admin_pages_list();
    if (!in_array($action, array_column($admin_pages, 'page'))) {
        return false;
    }

    $has_access = get_privileges(compact('roles','action'));

    if (count($has_access)) {
        obm_cache("set","has_access".$action,'TRUE',300);
        return TRUE;
    }

    obm_cache("set","has_access".$action,'FALSE',300);
    return FALSE;

}


function is_founder($table = PROJECTTABLE) {
    global $BID;

    if (!isset($_SESSION['Tid'])) {
        return FALSE;
    }
    $cmd = sprintf("SELECT 1 FROM projects WHERE admin_uid='%d' AND project_table='%s'",$_SESSION['Tid'],$table);
    $res = pg_query($BID,$cmd);
    return (pg_num_rows($res));
}

function get_privileges($args = []) {
    global $BID;
    $def = ['roles' => '%','action' => '%'];
    foreach ($def as $k => $v) {
        $args[$k] = (isset($args[$k])) ? $args[$k] : $def[$k];
    }

    if ($args['action']=='%' and $args['roles']=='%')
        $cmd = sprintf("SELECT action, role_id FROM group_privileges WHERE project = '%s' AND action LIKE %s", PROJECTTABLE, quote($args['action']));
    else
        $cmd = sprintf("SELECT action, role_id FROM group_privileges WHERE project = '%s' AND action LIKE %s AND role_id=ANY('{%s}')", PROJECTTABLE, quote($args['action']), $args['roles']);

    if (!$res = pg_query($BID,$cmd)) 
        return [];

    $privileges = [];
    while ($row = pg_fetch_assoc($res)) {
        $privileges[$row['role_id']][] = $row['action'];
    }
    return $privileges;
}
?>
