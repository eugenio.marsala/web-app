<?php
/* upload related functions
 * called from afuncs.php
 * */

class upload_process {
    public $insert_allowed = 0;
    public $data_owners = '{}';
    public $data_groups = '{}';
    public $selected_form_type = '';
    public $selected_form_id = '';
    public $uploading_id;
    public $errors = array();
    public $soft_errors = array();
    public $messages = array();
    public $massiveedit = 0;
    public $massiveedit_ids = array();
    public $temp_destination = false;
    public $skip_checks = false;

    function __construct($param='') {
        if ($param != '') {
            $this->massiveedit = 1;
            // még nincs erre lecserélve a session változó...
            $this->massiveedit_ids = json_decode($param);
        }
    }

    function access_check($selected_form_id,$selected_form_type) {
        global $BID;
        /* ACCESS check
         * */
        $destination_table = '';
        $this->selected_form_id = $selected_form_id;
        $this->selected_form_type = $selected_form_type;

        if ($selected_form_id) {
            if ($selected_form_type == 'wap') $selected_form_type = 'api';
            //validate input form data
            $cmd = sprintf("SELECT destination_table,ARRAY_TO_STRING(data_owners,',') as data_owners,ARRAY_TO_STRING(data_groups,',') as data_groups FROM project_forms WHERE active=1 AND project_table='".PROJECTTABLE."' AND form_id='%d' AND %s=ANY(form_type)",$selected_form_id,quote($selected_form_type));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $this->insert_allowed = 1;
                $row = pg_fetch_assoc($res);
                $this->data_owners = $row['data_owners'];
                $this->data_groups = $row['data_groups'];
                $destination_table = $row['destination_table'];
            }
        } else {
            // nem volt definiált form, ergo az adott jogosultsági szinten nem is létezik form!
            $acc = 0;
            $group_filter='';
            if (isset($_SESSION['Tid'])) {
                $acc = 1;
                $group_filter = sprintf(' OR (form_access=2 AND \'{%s}\'&&groups)',$_SESSION['Tgroups']);
            }
            $cmd = "SELECT \"form_id\",destination_table FROM project_forms WHERE active=1 AND project_table='".PROJECTTABLE."' AND 'file'=ANY(form_type) AND (form_access<='$acc' $group_filter)";
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $this->insert_allowed = 1;
                $this->selected_form_id = $row['form_id'];
                $destination_table = $row['destination_table'];
            }
        }

        // Check and remove it has conflicts with access check!!!
        if ($destination_table=='')
            $destination_table = PROJECTTABLE;

        if ( $this->insert_allowed == 0 ) {
            return false;
        }

	return $destination_table;;
    }

    function new_upload($destination_table) {
        global $ID,$GID,$BID;

        pg_query($ID,'SET search_path TO system,public');
        pg_query($GID,'SET search_path TO system,public');

        $modules = new modules();

        $trans_geom = 0;
        // debugging upload
        $upload_debug = 0;

        if (isset($_SESSION['last_upload_id']))
            unset($_SESSION['last_upload_id']);

        // forcing 2d transform of geometry
        // should be settable!!!
        $force2d = 1;

        /* Itt lehet a destination_table alapján lekérdezni mi a geom_c és az x_c és y_c !!!
         * */
        $st_col = st_col($destination_table,'array');
        $grid_zones_s = array('c','d','e','f','g','h','j','k','l','m');
        $grid_zones_n = array('n','p','q','r','s','t','u','v','w','x');

        /* Check UTM zone type
         * NOT GRID ZONES !!!
         * 37N / 37S
         * */
        /* Do not use this type for UTM zones it is confusing and can cause chaos...
         * if (isset($_POST['srid']) and preg_match('/^(\d{1,2})([SN])/',$_POST['srid'],$m)) {
            $upload_srid='';
            if ($m[2] == 'S') {
                $upload_srid = sprintf('327%02d',$m[1]);
            } elseif ($m[2] == 'N') {
                $upload_srid = sprintf('326%02d',$m[1]);
            }

            if ($upload_srid!='' and $st_col['SRID_C'] != $upload_srid) {
                $trans_geom=1;
            } elseif ($st_col['SRID_C'] == $upload_srid) {
                $trans_geom = 0;
            }

        } else*/
        if (isset($_POST['srid']) and preg_match('/^(\d{1,2})([a-z])/i',$_POST['srid'],$m)) {
            // UTM grid zones
            // letters c-f, n-x
            if (in_array($m[2],$grid_zones_s)) {
                $upload_srid = sprintf('327%02d',$m[1]);
            } elseif (in_array($m[2],$grid_zones_n)) {
                $upload_srid = sprintf('326%02d',$m[1]);
            }
            if ($upload_srid!='' and $st_col['SRID_C'] != $upload_srid) {
                $trans_geom=1;
            } elseif ($st_col['SRID_C'] == $upload_srid) {
                $trans_geom = 0;
            }
        } elseif (isset($_POST['srid'])) {

            // set upload srid
            $upload_srid = preg_replace('/[^0-9]/i','',$_POST['srid']);
            if ($upload_srid!='' and $st_col['SRID_C'] != $upload_srid) {
                $trans_geom=1;
            } else {
                $trans_geom=0;
                $upload_srid = $st_col['SRID_C'];
            }
        } else {
            $trans_geom = 0;
            $upload_srid = $st_col['SRID_C'];
        }

        //This is the main data object - created from the uploaded data file
        $j = json_decode($_POST['upload_table_post']);
        # Example upload_table_post
        # {
        #   "id":["faj","the_geom","szamossag","hely","egyedszam","gyujto","adatkozlo"],
        #
        #   "header":["faj","the_geom","szamossag",null,null,null,null,null,null,null,"hely","egyedszam","gyujto","adatkozlo"],
        #
        #   "default_data":["úttest","","pompom","miki"],
        #
        #   "rows":[[1,2]]
        # }

        $cols = array();
        $jid = array();
        $values = array();
        $UTMZONE = '';
        $X = ''; # x koordináta oszlop sorszáma
        $Y = ''; # y coordinate column's number
        $G = ''; # geometria oszlop sorszáma
        // A geometria oszlopokat kiszedem az oszlop listából, mert azt külön adom hozzá
        // by cols
        //
        for($i=0;$i<count($j->{'header'});$i++) {
            if (trim($j->{'header'}[$i])!='') {
                // $i <- hányadik szlop
                // Y_C, X_C és GEOM_C oszlopokat kiszedjük, mivel ezek külön vannak kezelve alább.
                if ($j->{'header'}[$i]==$st_col['X_C'] and $st_col['X_C']!='') {
                    $cols[] = $st_col['X_C'];
                    $X = $j->{'header'}[$i];
                }
                elseif ($j->{'header'}[$i]==$st_col['Y_C'] and $st_col['Y_C']!='') {
                    $cols[] = $st_col['Y_C'];
                    $Y = $j->{'header'}[$i];
                }
                elseif ($j->{'header'}[$i]==$st_col['GEOM_C'] and $st_col['GEOM_C']!='') {
                    $cols[] = '';
                    $G = $j->{'header'}[$i];
                }
                elseif ($j->{'header'}[$i]=='obm_id' and !$this->massiveedit) {
                    $cols[] = '';
                }
                elseif ($j->{'header'}[$i]==$st_col['UTMZONE_C'] and $st_col['UTMZONE_C']!='') {
                    $cols[] = $st_col['UTMZONE_C'];
                    $UTMZONE = $j->{'header'}[$i];
                }
                else {
                    $cols[] = $j->{'header'}[$i];
                }

                // Every validated column names in the jid array
                //$jid[] = trim($j->{'header'}[$i]);
            }
        }
        $sheetData_header = get_sheet_header();
        if (is_array($sheetData_header))
            $sheetData_header_count = count($sheetData_header);
        else
            $sheetData_header_count = 0;

        // SET MISSING COLUMNS
        // massive-edit / editrecord - missing columns from form but existing in dataset
        if ($this->massiveedit) {
            for ($i=0;$i<$sheetData_header_count;$i++) {
                if (!in_array($sheetData_header[$i],$j->{'header'}))
                    unset($sheetData_header[$i]);
            }
            $sheetData_header = array_values($sheetData_header);
            $sheetData_header_count = count($sheetData_header);
        }
        # 1..38
        for ($i=0;$i<$sheetData_header_count;$i++) {
            if (isset($j->{'header'}[$i])) {
                if ($this->massiveedit) {
                    // massive-edit / editrecord - missing columns from form but existing in dataset
                    $idx = array_search($sheetData_header[$i],$j->{'header'});
                    if ($idx!==false)
                        $jid[$sheetData_header[$i]] = $j->{'header'}[$idx];
                    else
                        $jid[$sheetData_header[$i]] = '';
                } else
                    $jid[$sheetData_header[$i]] = $j->{'header'}[$i];
            } else {
                // file upload - not assigned columns
                $jid[$sheetData_header[$i]] = '';
            }
        }
        # file upload with default columns
        # 41 > 38
        if (count($j->{'header'}) > $sheetData_header_count) {
            $hdiff = count($j->{'header'}) - $sheetData_header_count;
            for ($i = count($j->{'header'})-$hdiff;$i<count($j->{'header'});$i++) {
                $jid[$j->{'header'}[$i]] = $j->{'header'}[$i];
            }
        }

        $Xkey = false;
        $Ykey = false;
        $Gkey = false;
        $UTMZONEkey = false;
        if ($X!='')
            $Xkey = array_search($X,$jid);
        if ($Y!='')
            $Ykey = array_search($Y,$jid);
        if ($G!='')
            $Gkey = array_search($G,$jid);
        if ($UTMZONE!='')
            $UTMZONEkey = array_search($UTMZONE,$jid);

        $cmd = "SELECT \"column\",description,type,control,count,list,obl,fullist,relation FROM project_forms_data WHERE form_id='$this->selected_form_id'";
        $res = pg_query($BID,$cmd);
        $obl_fields = array();
        $rel_fields = array();
        $rel_statement = array();
        $rel_value = array();
        $rel_type = array();

        $RELATION_CHECK_LOGGING = false;

        while($row = pg_fetch_assoc($res)) {
            if ($row['obl']==1)
                $obl_fields[] = $row['column'];

            if ($row['relation']!='') {

                // general debug variable for relation

                /* relation pseudolanguage definition
                 *
                 * ( rel_field = rel_statement ) { rel_type(rel_value);rel_type(rel_value);... } , ( rel_field = rel_statement ) { rel_type(rel_value);... } , ...
                 *
                 * IF an other cell value (rel_field) match to (rel_statement) THEN  this cell (rel_type) value should be (rel_value)
                 *
                 * rel_type is a function related with the field type
                 *      datum:          set             extraxt year component from a datum string
                 *      text,numeric:   minmax          minmax range check
                 *      any type:       obligatory      change obligatory setting
                 *                      inequality      check inequality with these symbols: <>= between index and current field. Causing error message.
                 *      text:           set             set/append/prepend specified value
                 * rel_statement can be a regexp based function. In this case statement should be start with !! and followed by a regexp e.g.  !!^(\d{2})$
                 *      If statement is regexp rel_value also can be a function
                 *      .       means replace current cell value with matched string from the matched string from the rel_field
                 *      .+      means append current cell value to matched string from the rel_field
                 *      +.      means append matched string from the rel_field to the current cell value
                 *
                 * rel_value:
                 *      IF rel_type is inequality according to php comparison operators
                 *              +<.
                 *              +<=.
                 *              +>=.
                 *              +=.
                 *              +<>.
                 *              WHERE + is the matched rel_field value and . is the current cell value
                 *      Else can be anything - may be ignored - depending on the used function
                 *
                 * WARNING: more than one function call: rel_type(rel_value) not implemented yet! Only the lastone will be processed
                 *
                 */
                //if (preg_match('/^(\w+)=(.+)?\|(\w+)=(.+)/',$row['relation'],$m)) {
                //split multi statements
                $relations = preg_replace('/(\})\s*,\s*(\()/','$1``$2',$row['relation']);
                $relations = preg_split('/``/',$relations);
                //debug($row['column'],__FILE__,__LINE__);
                //debug($relations,__FILE__,__LINE__);
                $z = 0;
                for ( $zi=0; $zi<count($relations); $zi++ ) {
                    //if (preg_match('/^\(\s?(\w+)\s?=\s?(.+)?\)\s?\{\s?(\w+)\s?\(\s?(.+)\s?\)\s?\}/',$relations[$z],$m)) {
                    $m = array();
                    // field(m1)  statement(m2) functions(m3)
                    if ($RELATION_CHECK_LOGGING)
                        log_action('/^\(\s?(\w+)\s?=\s?(.+)?\)\s?\{\s?(.+)?\s?\}/,'.$relations[$z],__FILE__,__LINE__);

                    if (preg_match('/^\(\s?(\w+)\s?=\s?(.+)?\)\s?\{\s?(.+)?\s?\}/',$relations[$z],$m)) {
                        $functions = preg_split('/;/',$m[3]);
                        // explode multiple functions
                        $functions = preg_replace("/(\));(\w+)?/", "$1``$2", $m[3]);
                        $functions = preg_split('/``/',$functions);

                        // WARNING!! only the last function will be processed!!!
                        foreach($functions as $fun) {
                            //debug($fun,__FILE__,__LINE__);
                            $fn = array();
                            //function_name(fn1) (function parameter(fn2))
                            preg_match('/^(\w+)\((.+?)\)$/',$fun,$fn);


                            // equal to
                            if (!isset($rel_fields[$row['column']])) $rel_fields[$row['column']] = array();

                            $rel_fields[$row['column']][$z] = array_search($m[1],$jid);

                            if ($rel_fields[$row['column']][$z]===false)
                                log_action('Column name: '.$m[1].' might be mispelled in the realtion statement or not assigned to data.',__FILE__,__LINE__);

                            if (isset($m[2])) {
                                if (!isset($rel_statement[$row['column']])) $rel_statement[$row['column']] = array();
                                $rel_statement[$row['column']][$z] = $m[2];
                            }
                            if (isset($fn[1])) {
                                if (!isset($rel_type[$row['column']])) $rel_type[$row['column']] = array();
                                $rel_type[$row['column']][$z] = $fn[1];
                            }
                            if (isset($fn[2])) {
                                if (!isset($rel_value[$row['column']])) $rel_value[$row['column']] = array();
                                $rel_value[$row['column']][$z] = $fn[2];
                            }
                            //debug($rel_fields[$row['column']][$z],__FILE__,__LINE__);
                            //debug($rel_statement[$row['column']][$z],__FILE__,__LINE__);
                            //debug($rel_type[$row['column']][$z],__FILE__,__LINE__);
                            //debug($rel_value[$row['column']][$z],__FILE__,__LINE__);
                            $z++;
                        }
                    } else {
                        log_action("Wrong relation function format: {$relations[$z]}",__FILE__,__LINE__);
                    }
                }
            }
        }

        // remove obligatory fields for temp destination
        if ($this->skip_checks)
            $obl_fields = array();

        //debug($rel_fields);
        //debug($rel_statement);
        //debug($rel_type);
        //debug($rel_value);

        # normal names of columns
        $colnames = dbcolist('array',$destination_table);
        $ori_colnames = $colnames;

        $warnings = array();
        $list_elements = array();

        // collecting default_data values for temp sheet database
        $header = $j->{'header'};
        $dd = count($j->{'default_data'});
        $default_data = array();
        for ($u=0;$u<$dd;$u++) {
            $l = array_pop($header);
            $d = array_pop($j->{'default_data'});
            $default_data[$l] = $d;
        }

        $sheetData_count = get_upload_count();
        if ($sheetData_count==0) {
            // it can happen if a web form only contains default columns: vertical style form with only one row!!!!
            if (count($default_data)) {
                $sheetData_count = 1;
                if (add_sheet_row(1,$default_data,false))
                    $default_data = array();
            }
        }

        //api import id list
        $import_api_ids = array();

        //massive edit validated ids
        $upload_id = array();
        $invalid_upload_id = array();

        //$j_data_width = count($j->{'data'}); // rotated array, data columns
        //$j_data_count = count($j->{'data'}[0]); // rows

        $ea = array_pad(array(), count($jid), $this->selected_form_id );
        $pseudo_data = array();

        $processed_geoms = 0;
        $ssc = array();
        $colsattr = array(); //super array for storing column attributes;

        // A j->data web form esetén nem tartalmazza az skippelt sorokat!
        # Loop for every rows
        for($i=0;$i<$sheetData_count;$i++) {
            $ii = $i+1;
            $row = get_sheet_row($ii);

            $jdata = json_decode($row['data'],true);

            if ($jdata=='') $jdata = array();

            $drop_marked = 0;
            // drop completly empty rows - only if now default rows
            if (implode('',array_values($jdata)) == '') $drop_marked=1;

            // append default data elements to row
            if (count($default_data)) {
                $jdata = $default_data+$jdata;
                if ($i==0)
                    $drop_marked=0;
            }

            if ($drop_marked) {
                update_sheet_skips($ii,true);
                $ssc[] = $i;
                continue;
            }

            // skipped row
            if (get_row_skip($ii)=='t') {
                $ssc[] = $i;
                continue;
            }

            //nem tökéletes kezelése a koordináta transzformáció és a the_geom oszlop jelenlét kezelésének...
            //mert minden sorra teljesülnie kell, azaz vegyes transzformáció nem megengedett
            $transformed_geom_from_coords = 0;
            $geom_obl = 0;
            $geom_check = 'nocheck';
            $v = array();
            //$pseudo_data[] = array();

            # Loop for every columns
            $sheetData_header = get_sheet_header();
            $kc = 0;
            //{"A":"","B":"","C":"","D":"","E":"","F":"","G":"","H":"","I":"egyedszam","J":"","K":"","L":"","M":"","N":"","O":"","P":"","Q":"","R":"","S":"","T":"","U":"","V":"","W":"","X":"","Y":"","Z":"","AA":"","AB":"","AC":"","AD":"","AE":"","AF":"","AG":"","AH":"","AI":"","AJ":"","AK":"","AL":"","faj":"faj","eov_y":"eov_y","him":"him"}
            /* 
             * API $_POST
             * {
                "access_token": "5d4cc6438ccc15f238f128ac542d3dbd66f12fff",
                "form_id": "113",
                "scope": "put_data",
                "header": "[\"tipus\",\"ellenorzott\",\"obm_datum\",\"time\",\"datum\",\"observer\",\"corpse\",\"species\"]",
                "data": "[{\"tipus\":\"Bird\",\"ellenorzott\":\"\",\"obm_datum\":\"2020-02-18 09:00:00\",\"time\":\"09:00:00\",\"datum\":\"2020-02-18\",\"observer\":\"B\u00e1n Mikl\u00f3s\",\"corpse\":false,\"species\":\"Fraxinus excelsior\"}]",
                "metadata": "{\"id\":\"58b53a12-9824-4285-8c51-b9bfd500fe6f\",\"app_version\":\"OBM_mobile-r4_v1.6-2\",\"form_version\":\"1579255692\",\"started_at\":1582012802406,\"finished_at\":1582012938854}",
                "upload_table_post": "{\"header\":[\"tipus\",\"ellenorzott\",\"obm_datum\",\"time\",\"datum\",\"observer\",\"corpse\",\"species\"],\"default_data\":[],\"rows\":[]}",
                "srid": 4326,
                "description": "API upload"
               }

               $jid
               {
                "tipus": "tipus",
                "ellenorzott": "ellenorzott",
                "obm_datum": "obm_datum",
                "time": "time",
                "datum": "datum",
                "observer": "observer",
                "corpse": "corpse",
                "species": "species"
               } */
            for ($cjid = 0; $cjid<count($jid); $cjid++) {
                #foreach ($jid as $k=>$jidv) {
                $k = array_keys($jid)[$cjid];
                $jidv = array_values($jid)[$cjid];
                // $k           sheetData_header        <- uploaded file's columns
                // $jidv        j->{'header'}           <- posted table columns

                $selected_form_id = $ea[$kc];

                // external forms
                $drop_values = 0;
                if ($selected_form_id != $this->selected_form_id) {
                    //log_action("external forms checkings",__FILE__,__LINE__);
                    $drop_values = 1;
                }
                else {
                    $colnames = $ori_colnames;
                }

                // fájl feltöltésnél a nem hozzárendelt oszlopok eldobása!!
                if ($this->selected_form_type=='file' and $jidv=='') {
                    continue;
                }

                // Reading column names
                $ksp = preg_split('/:/',$jidv);
                if (count($ksp) == 2 and trim($ksp[1])!='')
                    $ksp = $ksp[1];
                else
                    $ksp = $ksp[0];

                // if column from POST['header'] is not picked yet, check: is this exists in the form data? If yes add to INSERT (column-list) 
                if (!isset($colsattr[$ksp])) {
                    // get column attributes
                    $cmd = "SELECT \"column\",type,control,array_to_string(\"count\",':') as cnt,array_to_string(list,',') as list,
                                        obl,fullist,regexp,spatial,custom_function,pseudo_columns,list_definition
                            FROM project_forms_data
                            WHERE form_id='$selected_form_id' AND \"column\"='$ksp'";
                    $res = pg_query($BID,$cmd);
                    // uploaded data contains a column which not exists in the database
                    if (!pg_num_rows($res)) {

                        if (!$this->skip_checks) {
                            $errorID = uniqid();
                            $this->messages[] = common_message("error","Unknown column ($ksp) in the selected form. #$errorID#");

                            debug($_POST,__FILE__,__LINE__); // due to an unresolved error...
                            log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                            log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
                            return false;
                        }
                        $idx = array_search($ksp,$cols);
                        if ($idx!==false)
                            unset($cols[$idx]);
                        continue;
                    } 
                    $colsattr[$ksp] = pg_fetch_assoc($res);
                }
                $col = $colsattr[$ksp];

                if ($col['list_definition'] != '') {
                    $l = json_decode($col['list_definition']);
                    // ez mi??
                    if (isset($l->labelAsValue)) {
                    }
                }
                // change col SQL for the pseudocolumns
                // pseudo_columns:
                // {"meret_kb":{"columns":["meta_1","meta_2"],"values":[null,null],"destination_table":["dinpi_meta","dinpi_meta"]}}
                if ($col['pseudo_columns'] != '') {
                    $pseudo_data = array($jidv => array("columns"=>array(),"values"=>array(),"destination_table"=>array()));
                    //$pseudo_data[$jid[$i]] = array("columns"=>array(),"values"=>array(),"destination_table"=>array());
                    $sp = preg_split('/:/',$col['pseudo_columns']);

                    $cmd = sprintf('SELECT form_id,destination_table FROM project_forms WHERE form_name=%s',quote($sp[0]));
                    $pfid_res = pg_query($BID,$cmd);
                    $pfid_row = pg_fetch_assoc($pfid_res);
                    $p_form = $pfid_row['form_id'];
                    $p_table = $pfid_row['destination_table'];
                    $colnames = dbcolist('array',$p_table);

                    $p_cols = preg_split('/,/',$sp[1]);
                    $in = $kc+1;

                    $offset = array_search($col['column'], array_keys($jid))+1;
                    $pseudo_columns_counter = 0;

                    foreach($p_cols as $pc) {
                        $pseudo_columns_counter++;
                        // update jid array (column names)
                        $jid = array_merge (
                            array_slice($jid, 0, $offset),
                            array($pc => $col['column'].":".$pc),
                            array_slice($jid, $offset, null));

                        $pseudo_data[$jidv]['destination_table'][] = $p_table;
                        $pseudo_data[$jidv]['columns'][] = $pc;
                        $pseudo_data[$jidv]['values'][] = $jdata[$col['column']];
                        $jdata[$pc] = $jdata[$col['column']];
                        // update ea array (selected_form)
                        array_splice( $ea, $in, 0, $p_form );
                        array_splice( $cols, $in, 0, $pc );
                        $in++;
                    }
                }

                // if there are more than one spatial check column, only the last one will be handled!!!
                if ($col['control'] == 'spatial' and ($col['type']=='wkt' or $col['type']=='point' or $col['type']=='line' or $col['type']=='polygon')) {
                    $geom_check = 'spatial';
                    $spatial = $col['spatial'];
                }

                //photo_id transformation
                if ($col['type']=='file_id') { # and $i=='obm_files_id' - nem veszem figyelembe, hogy ez valóban photo-id típus-e
                    //DATA from obm_files_id in $k.row
                    //$j->{'data'}['obm_files_id'][$k]
                    $conid = false;
                    $references = array();
                    if ($this->massiveedit) {
                        $cmd = sprintf('SELECT reference,conid FROM system.file_connect LEFT JOIN system.files ON (file_id=id) WHERE conid = %s',quote($jdata[$k]));
                        $pres = pg_query($ID,$cmd);
                        if (pg_num_rows($pres)) {
                            while ($prow = pg_fetch_assoc($pres)) {
                                $conid = $prow['conid'];
                                $references[] = $prow['reference'];
                            }
                        } else {
                            $references = explode(',',$jdata[$k]);
                        }
                    } else {
                        $references = explode(',',$jdata[$k]);
                    }
                    $cmd = sprintf('SELECT array_to_string(array(SELECT reference FROM system.files WHERE reference IN (%s) AND data_table=\'%s\'),\',\') AS p',implode(',',array_map('quote',$references)),$destination_table);
                    $pres = pg_query($ID,$cmd);
                    $conid = false;
                    if (pg_num_rows($pres)) {
                        $prow = pg_fetch_assoc($pres);
                        // minden fotó referencia
                        //  ellenőrzése, hogy létezik a photos táblában?
                        //  létezik és valós kép-e a fájl
                        //  Ha igen
                        //  1) Létrehozunk egy fotó referencia azonosítót: project_id_gid_'photo' ami bekerül a photo oszlopba majd
                        //  2) Létrehozunk annyi sort ahány kép van a photo_connect táblában : |project_id_gid_'photo'|fotó id|
                        $conid = file_connection_process(explode(',',$prow['p']),false,PROJECTTABLE,session_id(),$ii);
                    }

                    if ($conid) $jdata[$k] = $conid;
                    else $jdata[$k] = '';
                }

                # list checking
                #if ($col['type']=='list' and $col['list']!='' and $k==$S) {
                if ($col['type']=='list' and $col['list']!='') {
                    #így csak faj listára
                    $list_elements[] = $jdata[$k];
                }

                $jdata[$k] = $this->relation_check('set',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$jdata[$k]);

                # OBLIGATORY magic numbers:
                # 0 no
                # 1 yes
                # 2 depend on
                # 3 soft error

                // change obligatory setting based on relation function
                // e.g. clutch_size=3|obligatory=1
                $col['obl'] = $this->relation_check('obligatory',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$col['obl']);

                // remove obligatory for temp uploads
                if ($this->skip_checks)
                    $col['obl'] = 2;

                // jump to page link text in error messages
                $jumplink = '';
                if ($this->selected_form_type=='file') {
                    $ecpage = floor(($ii+1)/$_SESSION['perpage'])*$_SESSION['perpage'];
                    //$ecpage = floor(($ii-1)/$_SESSION['perpage']);
                    $jumplink = " <a class='pagel' data-index='$k' target='{$col['column']}' data-target='{$col['column']}' href='$ecpage'>[".str_jump."]</a>";
                }

                if ($col['obl']!=2 and trim($jdata[$k])=='' and $jdata[$k]!==false and ($col['type']!='wkt' and $col['type']!='point' and $col['type']!='line' and $col['type']!='polygon' )) {
                    # obligatory fields empty -> error
                    $this->errors[] = warning($ii,$colnames[$col['column']],str_a_mandatory_field_is_empty.$jumplink,$col['column']);
                    if ($col['obl']==3) {
                        $this->soft_errors[] = 1;
                        if ($col['type']=='numeric')
                            $v[] = 'NULL';
                        else
                            $v[] = '$abc$$abc$';
                    } else
                        $this->soft_errors[] = 0;
                } else {

                    if ($col['obl']==1 and ($col['type']=='wkt' or $col['type']=='point' or $col['type']=='line' or $col['type']=='polygon')) {
                        //X Y cordináták miatt
                        $geom_obl = 1;
                    }
                    /* Type check
                     * * */
                    if ($col['type']=='point' and trim($jdata[$k])!='' and $jid[$k]!=$G) {

                        # point check and prepare
                        # it is here because of the multiple geometry forms...
                        $m = array();
                        if (preg_match('/^(\w+)\s?\(/i',$jdata[$k],$m)) {
                            # wkt format chceck
                            if(!preg_match('/^pointz?$/i',$m[1])) {
                                $this->errors[] = warning($ii,$st_col['GEOM_C'],'POINT type WKT expected'.$jumplink,$col['column']);
                            }
                        }
                        # extracting coordinates
                        $lon = "";
                        $lat = "";
                        $m = array();

                        if (preg_match("/(-?\d{1,8})[°.]\s?(\d+)['.]\s?([0-9.]+)?\"?\s?([NWES])?[ ,-]{1}(-?\d{1,8})[°.]\s?(\d+)['.]\s?([0-9.]+)?\"?\s?([NWES])?/iu",$jdata[$k],$mdeg)) {

                            $direction = $mdeg[4];
                            if ($mdeg[1]<0) {
                                $degree = abs($mdeg[1]);
                                $direction = 'W';
                            } else {
                                $degree = $mdeg[1];
                            }
                            #Decimal Degrees = Degrees + minutes/60 + seconds/3600
                            $x = $degree+($mdeg[2]/60)+$mdeg[3]/3600;
                            if ($direction=='S' || $direction == 'W')
                                $x = $x*-1;
                            $lon = $x;

                            $direction = $mdeg[8];
                            if ($mdeg[5]<0) {
                                $degree = abs($mdeg[5]);
                                $direction = 'W';
                            } else {
                                $degree = $mdeg[5];
                                $direction = 'E';
                            }
                            #Decimal Degrees = Degrees + minutes/60 + seconds/3600
                            $y = $mdeg[5]+($mdeg[6]/60)+$mdeg[7]/3600;
                            if ($direction=='S' || $direction == 'W')
                                $y = $y*-1;
                            $lat = $y;
                        } else
                            preg_match('/(-?\w{0,3}\d{1,2}\.\d+)[ ,-]{1}(-?\w{0,3}\d{1,2}\.\d+)/',$jdata[$k],$m);

                        if (count($m)<2 & $lon=='' and $lat=='') {
                            $this->errors[] = warning($ii,$st_col['GEOM_C'],str_geometry_verification_failed.$jumplink,$col['column']);
                        } elseif ($lon=='' and $lat=='') {
                            $lon = trim($m[1]);
                            $lat = trim($m[2]);
                            if (preg_match('/lon/i',$lon)) {
                               $lon = preg_replace('/lon/i','',$lon);
                               $lat = preg_replace('/lat/i','',$lat);
                            } elseif (preg_match('/lon/i',$lat)) {
                               $hosszusag = preg_replace('/lon/i','',$lat);
                               $szelesseg = preg_replace('/lat/i','',$lon);
                               $lon = $hosszusag;
                               $lat = $szelesseg;
                            }
                        }

                        $jdata[$k] = sprintf("ST_PointFromText('POINT(%f %f)', %d)",$lon,$lat,$upload_srid);
                        $v[] = $jdata[$k];
                    } elseif ($col['type']=='numeric' and trim($jdata[$k])!='') {

                        if(!is_numeric(trim($jdata[$k]))) {
                            if(is_numeric(trim(preg_replace('/,/','.',$jdata[$k])))) {
                                $jdata[$k] = preg_replace('/,/','.',$jdata[$k]);
                            }
                        }

                        if(!is_numeric(trim($jdata[$k]))) {
                            # Numeric check
                            if (trim($jdata[$k])=='true') {
                                //boolean true cast as numeric 1
                                $jdata[$k] = 1;
                                $v[] = 1;
                            }
                            elseif (trim($jdata[$k])=='false') {
                                //boolean false cast as numeric 0
                                $jdata[$k] = 0;
                                $v[] = 0;
                            }
                            else {
                                $this->errors[] = warning($ii,$col['column'],'numeric data expected'.$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            }
                        } else {
                            //minmax check
                            if ( $col['control']=='minmax' ) {
                                // relation on this column exists and relation type is minmax
                                $col['cnt'] = $this->relation_check('minmax',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$col['cnt']);

                                $minmax = preg_split('/:/',$col['cnt']);

                                if (count($minmax)==1) {
                                    $mm_min = 0;
                                    $mm_max = $minmax[0];
                                } else {
                                    $mm_min = $minmax[0];
                                    $mm_max = $minmax[1];
                                }

                                if ( $mm_min == $mm_max and  $jdata[$k] != $mm_min ) {
                                    $this->errors[] = warning($ii,$col['column'],'the input value is differ from the expected'.$jumplink,$col['column']);
                                    if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                    else  $this->soft_errors[] = 0;
                                }
                                elseif ( $mm_max == 0 and $jdata[$k] < $mm_min ) {
                                    $this->errors[] = warning($ii,$col['column'],'the input value is less than the expected'.$jumplink,$col['column']);
                                    if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                    else  $this->soft_errors[] = 0;
                                }
                                elseif ( $mm_min == 0 and $jdata[$k] > $mm_max ) {
                                    $this->errors[] = warning($ii,$col['column'],'the input value is greater than the expected'.$jumplink,$col['column']);
                                    if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                    else  $this->soft_errors[] = 0;
                                }
                                elseif ( $jdata[$k] > $mm_max or $jdata[$k] < $mm_min ) {
                                    $this->errors[] = warning($ii,$col['column'],'the input value is out of range'.$jumplink,$col['column']);
                                    if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                    else  $this->soft_errors[] = 0;
                                }
                            }
                            $v[] = $jdata[$k];
                        }
                    } elseif ($col['type']=='date' and trim($jdata[$k])!='') {
                        #gpx type
                        #19-05-14
                        #18-MAJ-14
                        #2014-04-15
                        #2015-09-23T08:07:07Z
                        #2016-08-08 16:38:31.504292
                        #Extract from full date-time format
                        #Sun May 13 08:52:51 CEST 2018
                        $parsed_date = date_parse($jdata[$k]);
                        $m = array();
                        if (preg_match("/([0-9]{1,4})[-.\/]([a-z]{3}|[0-9]{1,2})[-.\/]([0-9]{1,4})/i",trim($jdata[$k]),$m)) {
                            //expected order
                            $year = $m[1];
                            $month = $m[2];
                            $day = $m[3];
                            //nontrivial date format
                            if (strlen($m[1])==2) {
                                $day = $m[1];
                                $month = $m[2];
                                if (preg_match('/^[01][0-9]$/',$m[3])) {
                                    $year = "20".$m[3];
                                    $warnings[] = warning($ii,$col['column'],"Year autocorrection from $m[3] to 20$m[3]".$jumplink,$col['column']);
                                }
                                elseif (preg_match('/^[7-9][0-9]$/',$m[3])) {
                                    $year = "19".$m[3];
                                    $warnings[] = warning($ii,$col['column'],"Year autocorrection from $m[3] to 19$m[3]".$jumplink,$col['column']);
                                }
                                elseif (preg_match('/^[12][089][0-9][0-9]$/',$m[3])) {
                                    $year = $m[3];
                                } else {
                                    $warnings[] = warning($ii,$col['column'],"Ambiguous year format: $m[3]".$jumplink,$col['column']);
                                    $year = $m[3];
                                }
                            }
                            if (preg_match('/[a-z]{3}/i',$month)) {
                                //text representation of month
                                //hungarian gpx
                                $month = preg_replace('/maj/i','may',$month);
                                $month = preg_replace('/jún/i','jun',$month);
                                $month = preg_replace('/júl/i','jul',$month);
                                $month = preg_replace('/okt/i','oct',$month);
                                $month = preg_replace('/sze/i','sep',$month);
                            }

                            // year substitution
                            $year = $this->relation_check('set',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$year);
                            $jdata[$k] = sprintf("%d-%'.02d-%'.02d",$year,$month,$day);

                            // inequality check on date string
                            if ($this->relation_check('inequality',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,'')===false) {
                                $this->errors[] = warning($ii,$col['column'],'datum relation problem'.$jumplink,$col['column']);
                                if ($col['obl']==3) { $this->soft_errors[] = 1; }
                            }

                            $datestring = "$year-$month-$day";
                            if (strlen($month)==4) {
                                //if the year on the middle position give the whole string to postgres and hope it can understand it :)
                                $datestring = $m[0];
                            }
                            $v[] = sprintf("'%s'",$datestring);

                        } elseif (preg_match('/^[0-9]{3,8}$/',trim($jdata[$k]),$m)) {

                            // Koszti's date format
                            $md = array();
                            $ymd = array();
                            if (preg_match('/^(0?[1-9]|1[0-2])([0-3][0-9])$/',trim($jdata[$k]),$md)) {
                                // mmdd
                                // year come from relation field or default is current year
                                // field = statement | type = value
                                // year=!|year=!
                                // if type = year and if 'year" column's value = "!" then
                                // $year = ! which causing an error - it is OK
                                // year should come year column

                                $year = '';//date("Y");
                                $year = $this->relation_check('set',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$year);
                                $jdata[$k] = sprintf("%d-%'.02d-%'.02d",$year,$md[1],$md[2]);

                            #log_action($rel_fields,__FILE__,__LINE__);
                            #log_action($rel_statement,__FILE__,__LINE__);
                            #log_action($rel_type,__FILE__,__LINE__);
                            #log_action($rel_value,__FILE__,__LINE__);
                                if ($this->relation_check('inequality',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,'')===false) {
                                    $this->errors[] = warning($ii,$col['column'],'datum relation problem'.$jumplink,$col['column']);
                                    if ($col['obl']==3) { $this->soft_errors[] = 1; }
                                }

                                $datestring = sprintf("%d-%'.02d-%'.02d",$year,$md[1],$md[2]);

                            } elseif (preg_match('/^([12][890][0-9][0-9])([01][0-9])([0-3][0-9])$/',trim($jdata[$k]),$ymd)) {
                                // dates
                                // yymmdd

                                $ymd[1] = $this->relation_check('set',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$ymd[1]);
                                $jdata[$k] = sprintf("%d-%'.02d-%'.02d",$ymd[1],$ymd[2],$ymd[3]);

                                if ($this->relation_check('inequality',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,'')===false) {
                                    $this->errors[] = warning($ii,$col['column'],'datum relation problem'.$jumplink,$col['column']);
                                    if ($col['obl']==3) { $this->soft_errors[] = 1; }
                                }

                                $datestring = sprintf("%d-%'.02d-%'.02d",$ymd[1],$ymd[2],$ymd[3]);

                            } else {
                                // possible SQL error
                                $datestring = trim($jdata[$k]);
                            }
                            $v[] = sprintf("'%s'",$datestring);

                        } elseif ($parsed_date['error_count']==0) {
                            $datestring = sprintf("%d-%d-%d",$parsed_date['year'],$parsed_date['month'],$parsed_date['day']);
                            $v[] = sprintf("'%s'",$datestring);
                        } else {
                            // NO error for NA but create a NULL!!
                            // It is  trick for avoid datum warning.
                            //if ($col['obl'] != 1 and $jdata[$k] == 'NA') $jdata[$k] = 'NULL';
                            //else {
                                $this->errors[] = warning($ii,$col['column'],'datum format expected e.g. (<i>1970-03-25</i>) but '.$jdata[$k].$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            //}
                        }
                    } elseif ($col['type']=='datetime' and trim($jdata[$k])!='') {
                        #gpx type
                        #19-05-14 21:45:00
                        #18-MAJ-14 18:09:28
                        #2014-04-15 11:52:06.451000
                        #2015-09-23T08:07:07Z
                        #0=>2014-04-15 11:52:06
                        #1=>2014
                        #2=>04
                        #3=>15
                        #4=>11:52 <F2>:06
                        $m = array();
                        $parsed_date = date_parse($jdata[$k]);
                        if (preg_match("/([0-9]{1,4})[-.\/]([a-z]{3}|[0-9]{1,2})[-.\/]([0-9]{1,4})\.?[\s|T]([0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})/i",trim($jdata[$k]),$m)) {
                            //expected order
                            $year = $m[1];
                            $month = $m[2];
                            $day = $m[3];
                            //nontrivial date format
                            if (strlen($m[1])==2) {
                                $day = $m[1];
                                $month = $m[2];
                                if (preg_match('/^[01][0-9]$/',$m[3])) {
                                    $year = "20".$m[3];
                                    $warnings[] = warning($ii,$col['column'],"Year autocorrection from $m[3] to 20$m[3]".$jumplink,$col['column']);
                                }
                                elseif (preg_match('/^[7-9][0-9]$/',$m[3])) {
                                    $year = "19".$m[3];
                                    $warnings[] = warning($ii,$col['column'],"Year autocorrection from $m[3] to 19$m[3]".$jumplink,$col['column']);
                                } else {
                                    $this->errors[] = warning($ii,$col['column'],"Ambiguous year format: $m[3]".$jumplink,$col['column']);
                                    if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                    else  $this->soft_errors[] = 0;
                                }
                            }

                            if (preg_match('/[a-z]{3}/i',$month)) {
                                //text representation of month
                                //hungarian gpx
                                $month = preg_replace('/maj/i','may',$month);
                                $month = preg_replace('/jún/i','jun',$month);
                                $month = preg_replace('/júl/i','jul',$month);
                                $month = preg_replace('/okt/i','oct',$month);
                                $month = preg_replace('/sze/i','sep',$month);
                            }
                            $datestring = "$year-$month-$day $m[4]";
                            if (strlen($month)==4) {
                                //if the year on the middle position give the whole string to postgres and hope it can understand it :)
                                $datestring = $m[0];
                            }
                            $v[] = sprintf("'%s'",$datestring);

                        } elseif ($parsed_date['error_count']==0) {
                            $datestring = sprintf("%d-%d-%d %d:%d:%d",$parsed_date['year'],$parsed_date['month'],$parsed_date['day'],$parsed_date['hour'],$parsed_date['minute'],$parsed_date['second']);
                            $v[] = sprintf("'%s'",$datestring);

                        } else {
                            # Datum repair
                            if(preg_match("/^([0-9]{1,4})[-.\/]([A-Z]{3}|[0-9]{1,4})[-.\/]([0-9]{1,4})$/i",trim($jdata[$k]),$m) ){
                                # only datum, missing time
                                $v[] = sprintf("%s",quote($m[0]." 00:00:00"));
                                $warnings[] = warning($ii,$col['column'],"Missing time string, default time is 00:00:00".$jumplink,$col['column']);
                            }
                            elseif(preg_match("/^([0-9]{1,4})[-.\/]([A-Z]{3}|[0-9]{1,4})[-.\/]([0-9]{1,4})[\s|T]([0-9]{1,2}:[0-9]{1,2})$/i",trim($jdata[$k]),$m) ){
                                # missing seconds
                                $v[] = sprintf("%s",quote($m[1].'-'.$m[2].'-'.$m[3].' '.$m[4].':00'));
                                $warnings[] = warning($ii,$col['column'],"Scanty time string, default seconds is :00".$jumplink,$col['column']);
                            } else {
                                if (preg_match('/^[0-9]+$/',$jdata[$k]))
                                    $this->errors[] = warning($ii,$col['column'],'You need to convert excel days ('.$jdata[$k].') to true datum using [<i class="fa fa-fw fa-flask"></i>] functions '.$jumplink,$col['column']);
                                else
                                    $this->errors[] = warning($ii,$col['column'],'datum-time format expected (<i>1970-03-25 12:21:00</i>) but '.$jdata[$k].$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                                #$this->errors[] = array_pop($warnings);
                            }
                        }
                    } elseif ($col['type']=='time' and trim($jdata[$k])=='') {
                        $jdata[$k] = 'NULL';
                        $v[] = $jdata[$k];
                    } elseif ($col['type']=='time' and trim($jdata[$k])!='') {
                        if (!preg_match('/([0-2]{0,1}[0-9]{1}).(\d{1,2})?/',trim($jdata[$k]))) {
                            # 11:50
                            # Time check
                            $this->errors[] = warning($ii,$col['column'],'time format expected (e.g.: <i>02:20</i>)'.$jumplink,$col['column']);
                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                            else  $this->soft_errors[] = 0;
                        } else {
                            // Koszti's time format
                            // HHMM
                            // strict time check
                            if (preg_match('/([0-2]{0,1}[0-9]{1})([0-5]{1}[0-9]{1})/',$jdata[$k],$hm)) {
                                if ( $col['control']=='minmax' ) {

                                    $col['cnt'] = $this->relation_check('minmax',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$col['cnt']);

                                    $minmax = preg_split('/:/',$col['cnt']);

                                    if (count($minmax)==2) {
                                        $mm_min = $minmax[0];
                                        $mm_max = $minmax[1];

                                        if ($mm_min == $mm_max and  $hm[1] != $mm_min ) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is differ from the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        } elseif ($hm[1] < $mm_min) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is less than the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        } elseif ($hm[1] > $mm_max) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is greater than the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        } elseif ($hm[1] == $mm_max and $hm[2] > 0) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is out of the allowed range'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        }
                                    }
                                }
                                if (strlen($hm[1])=='1')
                                    $hm[1] = '0'.$hm[1];

                                $jdata[$k] = "$hm[1]:$hm[2]";

                            // 05-08-00
                            } else if (preg_match('/([0-2]{1}[0-9]{1}).([0-5]{1}[0-9]{1}).(\d{1,2})/',trim($jdata[$k]),$hm)) {
                                $jdata[$k] = "$hm[1]:$hm[2]:$hm[3]";

                            // 12:34 -> 12:34:00
                            // 1234
                            // 12 -> 12:00:00 ???
                            } else if (preg_match('/([0-2]{1}[0-9]{1}):?(\d{1,2})?/',trim($jdata[$k]),$hm)) {

                                if (preg_match('/^([0-2]{1}[0-9]{1})$/',trim($jdata[$k]))) {
                                    $jdata[$k] = $hm[0].":00";
                                    $warnings[] = warning($ii,$col['column'],"Scanty time string, default seconds is :00".$jumplink,$col['column']);
                                    $hm[2] = 0;
                                }

                                if ( $col['control']=='minmax' ) {

                                    $col['cnt'] = $this->relation_check('minmax',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$col['cnt']);

                                    $minmax = preg_split('/:/',$col['cnt']);

                                    if (count($minmax)==2) {
                                        $mm_min = $minmax[0];
                                        $mm_max = $minmax[1];

                                        if ($mm_min == $mm_max and  $hm[1] != $mm_min ) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is differ from the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        } elseif ($hm[1] < $mm_min) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is less than the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        } elseif ($hm[1] > $mm_max) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is greater than the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        } elseif ( $hm[1] == $mm_max and $hm[2]>0) {
                                            $this->errors[] = warning($ii,$col['column'],'the input value is greater than the expected'.$jumplink,$col['column']);
                                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                            else  $this->soft_errors[] = 0;
                                        }
                                    }
                                }
                            }
                        }
                        $v[] = quote($jdata[$k]);
                    } elseif ($col['type']=='timetominutes' and trim($jdata[$k])!='') {
                        # numeric column type, convert time to numeric value
                        if(!preg_match('/[0-9.]+:?(\d{1,2})?/',trim($jdata[$k]))) {
                            # 11:50
                            # Time check
                            $this->errors[] = warning($k,$col['column'],'time format expected (e.g.: <i>02:20</i>)'.$jumplink,$col['column']);
                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                            else  $this->soft_errors[] = 0;
                        } else {
                            $hm = array();
                            if (preg_match('/^(\d+)([a-zA-Z ]+)?$/',$jdata[$k],$hm)) {
                                $jdata[$k] = sprintf("%d",$hm[1]);
                            } elseif(preg_match('/(\d+):(\d+)/',$jdata[$k],$hm)) {
                                $jdata[$k] = sprintf("%d",$hm[1]*60+$hm[2]);
                            } elseif (preg_match('/(\d+\.\d)/',$jdata[$k],$hm)) {
                                $jdata[$k] = sprintf("%d",$hm[1]*60);
                            } else {
                                $warnings[] = warning($k,$col['column'],'time conversion failed'.$jumplink,$col['column']);
                            }
                            $v[] = $jdata[$k];
                        }
                    } elseif ($col['type']=='tinterval' and trim($jdata[$k])!='') {
                       if(!preg_match('/\d{1,4}.\d{1,2}|\s{3}.\d{1,4}.\d{1,2}:\d{1,2}(:\d{1,2}\.\d+)?\w? \d{1,4}.\d{1,2}|\s{3}.\d{1,4}.\d{1,2}:\d{1,2}(:\d{1,2}\.\d+)?\w?/',trim($jdata[$k]))) {
                            # Time interval check: from - to
                            $this->errors[] = warning($ii,$col['column'],'time interval format expected (<i>2014-02-25 12:00:00 2014-02-25 13:00:00</i>)'.$jumplink,$col['column']);
                            if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                            else  $this->soft_errors[] = 0;
                       }
                       $v[] = $jdata[$k];
                    } elseif ($col['type']=='boolean' and trim($jdata[$k])!='') {
                        if ($jdata[$k]=='true' or $jdata[$k]=='1') $jdata[$k] = 'TRUE';
                        else $jdata[$k] = 'FALSE';
                        $v[] = $jdata[$k];
                    } elseif ($col['type']=='array' and trim($jdata[$k])!='') {
                        //auto type determination
                        $ejk = explode(',',$jdata[$k]);
                        $ejk = array_map('trim', $ejk);
                        $elements = array();
                        $numeric_elements = 0;
                        $text_elements = 0;

                        foreach($ejk as $e) {
                            if ($e!='' and !is_numeric($e)) {
                                $elements[] = quote($e);
                                $text_elements++;
                            } elseif($e!='' and is_numeric($e)) {
                                $elements[] = $e;
                                $numeric_elements++;
                            }
                        }
                        if ($numeric_elements==0 and $text_elements==0 and $col['obl']==1)
                            $this->errors[] = warning($ii,$colnames[$col['column']],str_a_mandatory_field_is_empty.$jumplink,$col['column']);

                        //values('{1, 2, 3}')
                        //values('{"a","b","c"}')
                        $jdata[$k] = "'{".implode(',',$elements)."}'";

                        $v[] = $jdata[$k];
                    } elseif(trim($cols[$kc])!='') {
                        # We skip the geometry columns' data
                        # Control text input length
                        if ( $col['control']=='minmax' and trim($jdata[$k])!='') {

                            $col['cnt'] = $this->relation_check('minmax',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$col['cnt']);

                            $minmax = preg_split('/:/',$col['cnt']);
                            if (count($minmax)==1){
                                $mm_min = 0;
                                $mm_max = $minmax[0];
                            } else {
                                $mm_min = $minmax[0];
                                $mm_max = $minmax[1];
                            }

                            if ($mm_min == $mm_max and  strlen($jdata[$k]) != $mm_min ) {
                                $this->errors[] = warning($ii,$col['column'],'the input value is differ from the expected'.$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            } elseif ($mm_max == 0 and strlen($jdata[$k]) < $mm_min) {
                                $this->errors[] = warning($ii,$col['column'],'the input value is less than the expected'.$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            } elseif ($mm_min == 0 and strlen($jdata[$k]) > $mm_max) {
                                $this->errors[] = warning($ii,$col['column'],'the input value is greater than the expected'.$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            } elseif (strlen($jdata[$k]) > $mm_max or strlen($jdata[$k]) < $mm_min) {
                                $this->errors[] = warning($ii,$col['column'],'the input value is out of range'.$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            }
                        }
                        elseif ( $col['control']=='regexp' and trim($jdata[$k])!='') {

                            if(!preg_match("/".$col['regexp']."/",$jdata[$k])){
                                $this->errors[] = warning($ii,$col['column'],'does not match to pattern'.$jumplink,$col['column']);
                                if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                else  $this->soft_errors[] = 0;
                            }
                        }
                        elseif ( $col['control']=='custom_check') {
                            // custom control function
                            $custom_control_function = $col['custom_function'];

                            // Creating custom notify-listening event
                            if ($response = $modules->_include('custom_data_check','check',array($custom_control_function,$jdata,$k,$jid))) {
                                $this->errors[] = warning($ii,$col['column'],"$custom_control_function check failed $response".$jumplink,$col['column']);
                                if ($response !== false) {
                                    // allow soft error!
                                    if ($col['obl'] == 3) { $this->soft_errors[] = 1; }
                                    else  $this->soft_errors[] = 0;
                                }
                            }
                        }

                        // somtimes jdata[k] index is not defined. Why? Column names like I J K L not defined ....
                        if (!isset($jdata[$k])) {
                            $jdata[$k] = 'NULL';
                        }

                        //$jdata[$k] = $this->relation_check('set',$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$jdata[$k]);

                        $v[] = quote($jdata[$k]);
                        //nem lehet kiüríteni sajna, a geom X Y miatt...
                        //$jdata[$i][$k] = "";
                    }
                }


                // remove last value if it is a pseudo_column - we just checked the content
                if ($drop_values) {
                    array_pop($v);
                    // restore colnames for formatted warnings
                    //$colnames = dbcolist('array',$destination_table);
                    //$colnames = $ori_colnames;
                }
                $kc++;
            }

            //az eredeti koordinátából csináljuk a geometriát!
            $geom = '';
            // a nem fő táblákban a geometria alapból 1, hogy NULL geometriás adatok is felmehessenek.
            //if ($destination_table!=PROJECTTABLE) $geom = 1;

            //van értéke az x,y koordináta oszlopknak, azaz lehet, vagy ilyen adatunk, de lehet máshonnan is valid geometry
            if ($Xkey!==false and $Ykey!==false and $jdata[$Xkey]!='' and $jdata[$Ykey]!='' and  ($Gkey===false or $jdata[$Gkey]=='')) {
                # transform given X,Y coordinates to geometry
                $m = array();
                $x = trim($jdata[$Xkey]);
                $y = trim($jdata[$Ykey]);

                if ($UTMZONEkey!==false) {
                   $utmzone = $jdata[$UTMZONEkey];

                   $upload_srid='';

                   if (preg_match('/^(\d{1,2})([a-z])/i',$utmzone,$m)) {
                        // UTM grid zones
                        // letters c-f, n-x
                        if (in_array(strtolower($m[2]),$grid_zones_s)) {
                            $upload_srid = sprintf('327%02d',$m[1]);
                        } elseif (in_array(strtolower($m[2]),$grid_zones_n)) {
                            $upload_srid = sprintf('326%02d',$m[1]);
                        }
                        if ($upload_srid!='' and $st_col['SRID_C'] != $upload_srid) {
                            $trans_geom=1;
                        } elseif ($st_col['SRID_C'] == $upload_srid) {
                            $trans_geom = 0;
                        }
                    }
                }

                if (preg_match('/^(-?\d{1,8})([,.])(\d+)$/u',$jdata[$Xkey],$m)) {
                    $x = "$m[1].$m[3]";
                    $jdata[$Xkey] = $x;
                    #(-?\d{1,8})[°.]\s?(\d+)(['.])\s?([0-9.]+)?(["'])?\s?([NWES])?
                    #array(6
                    #   0=>017°30'069"N   019°16.296'
                    #   1=>017
                    #   2=>30
                    #   3=>'
                    #   4=>069
                    #   5=>"
                    # ? 6=>N
                    #)
                } elseif (preg_match("/^(-?\d{1,8})[°.]\s?(\d+)(['.])\s?([0-9.]+)?([\"'])?\s?([NWES])?/iu",$jdata[$Xkey],$m)) {
                    #36°57'9" N
                    #110°4'21" W -110.4.21
                    if (!isset($m[6]))
                        $direction = '';
                    else
                        $direction = $m[6];

                    if ($m[1]<0) {
                        $degree = abs($m[1]);
                        if ($direction == '')
                            $direction = 'W';
                    } else {
                        $degree = $m[1];
                        if ($direction == '')
                            $direction = 'E';
                    }
                    #Decimal Degrees = Degrees + minutes/60 + seconds/3600
                    if ($m[3] == "'")
                        $x = $degree+($m[2]/60)+$m[4]/3600;
                    else
                        $x = $degree+("$m[2].$m[4]"/60);

                    if ($direction=='S' || $direction == 'W')
                        $x = $x*-1;
                    $jdata[$Xkey] = $x;
                } else {
                    //$this->errors[] = warning($ii+1,'',"X ".str_coordinate_transformation_failed);
                }

                if (preg_match('/^(-?\d{1,8})([,.])(\d+)$/u',$jdata[$Ykey],$m)) {
                    $y = "$m[1].$m[3]";
                    $jdata[$Ykey] = $y;
                } elseif (preg_match("/^(-?\d{1,8})[°.]\s?(\d+)(['.])\s?([0-9.]+)?([\"'])?\s?([NWES])?/iu",$jdata[$Ykey],$m)) {
                    # -19.917500 Ferro correction
                    #
                    if (!isset($m[6]))
                        $direction = '';
                    else
                        $direction = $m[6];

                    if ($m[1]<0) {
                        $degree = abs($m[1]);
                        if ($direction == '')
                            $direction = 'W';
                    } else {
                        $degree = $m[1];
                        if ($direction == '')
                            $direction = 'E';
                    }
                    #Decimal Degrees = Degrees + minutes/60 + seconds/3600
                    if ($m[3] == "'")
                        $y = $m[1]+($m[2]/60)+$m[4]/3600;
                    else
                        $y = $degree+("$m[2].$m[4]"/60);

                    if ($direction=='S' || $direction == 'W')
                        $y = $y*-1;
                    $jdata[$Ykey] = $y;
                } else {
                    //$this->errors[] = warning($ii+1,'',"Y ".str_coordinate_transformation_failed);
                }

                # WGS84 check
                if ($upload_srid == 4326 and ($x < -180 or $x > 180)) {
                    $this->errors[] = warning($ii,$st_col['GEOM_C'],'WGS84 x range should be within -180 and 180'.$jumplink,$st_col['GEOM_C']);
                    $this->soft_errors[] = 0;
                }
                if ($upload_srid == 4326 and ($y < -90 or $y > 90)) {
                    $this->errors[] = warning($ii,$st_col['GEOM_C'],'WGS84 y range should be within -90 and 90'.$jumplink,$st_col['GEOM_C']);
                    $this->soft_errors[] = 0;
                }

                # EOV auto flip
                if ($upload_srid == 23700 and ($y > 400000 and $x < 400000)) {
                    $x_flip = $x;
                    $y_flip = $y;
                    $y = $x_flip;
                    $x = $y_flip;
                }

                $cmd = sprintf("SELECT ST_PointFromText('POINT(%.15f %.15f)', %d) AS geom",$x,$y,$upload_srid);
                // Beyond current transaction - BID should allow postgis!
                $res = pg_query($BID,$cmd);
                if (!pg_num_rows($res)) {
                    log_action("create geometry from xy coords failed: ".$cmd,__FILE__,__LINE__);
                    $this->errors[] = warning($ii,$st_col['GEOM_C'],str_coordinate_transformation_failed.$jumplink,$st_col['GEOM_C']);
                    $this->soft_errors[] = 0;
                } else {
                    $row = pg_fetch_assoc($res);
                    $geom = $row['geom'];
                    if ($geom=='') {
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],str_coordinate_transformation_failed.$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    } else {
                        $transformed_geom_from_coords = 1;
                    }
                }
            }

            if (trim($G)!='' and $geom=='' and $jdata[$Gkey]!='') {
                # transform given WKT to geometry
                ## point - polygon - line

                $cmd = "SELECT type FROM project_forms_data WHERE form_id='$this->selected_form_id' AND \"column\"='$G'";
                $res = pg_query($BID,$cmd);
                $row = pg_fetch_assoc($res);
                if($row['type']=='polygon') {
                    # json retransform
                    $polygon = json_decode($jdata[$Gkey]);
                    $poly = "";
                    if ($polygon=='') {
                        # not a json
                        $m = array();
                        if (preg_match('/^(\w+)\((.+)\)$/i',$jdata[$Gkey],$m)) {
                            if(!preg_match('/^polygon$/i',$m[1])) {
                                $this->errors[] = warning($ii,$st_col['GEOM_C'],'POLYGON type WKT expected'.$jumplink,$st_col['GEOM_C']);
                                $this->soft_errors[] = 0;
                            } else {
                                $poly = $m[2];
                            }
                        } else {
                            $this->errors[] = warning($ii,$st_col['GEOM_C'],'wrong polygon format'.$jumplink,$st_col['GEOM_C']);
                            $this->soft_errors[] = 0;
                        }
                    } else {
                        #JSON
                        $coo = array();
                        foreach($polygon as $c) {
                            $coo[] = $c[0].' '.$c[1];
                        }
                        $poly = '('.implode(',',$coo).')';
                    }
                    if ($poly!='') {
                        $cmd = "SELECT ST_GeomFromText('POLYGON($poly)',$upload_srid) AS geom";
                        // Beyond current transaction - BID should allow postgis!
                        $res = pg_query($BID,$cmd);
                        if (!pg_num_rows($res)) {
                            $this->errors[] = warning($ii,$st_col['GEOM_C'],'geometry transformation failed'.$jumplink,$st_col['GEOM_C']);
                            $this->soft_errors[] = 0;
                        } else {
                            $processed_geoms++;
                            $row = pg_fetch_assoc($res);
                            $geom = $row['geom'];
                        }
                    }
                } elseif($row['type']=='point') {
                    $m = array();
                    if (preg_match('/^(\w+)\s?\(/i',$jdata[$Gkey],$m)) {
                        # wkt format chceck
                        if(!preg_match('/^pointz?$/i',$m[1])) {
                            $this->errors[] = warning($ii,$st_col['GEOM_C'],'POINT type WKT expected'.$jumplink,$st_col['GEOM_C']);
                            $this->soft_errors[] = 0;
                        }
                    }
                    # extracting coordinates

                    $lon = "";
                    $lat = "";
                    $m = array();

                    # 017° 30' 069" E -017° 30' 069"N
                    if (preg_match("/(-?\d{1,8})[°.]\s?(\d+)['.]\s?([0-9.]+)?\"?\s?([NWES])?[ ,-]{1}(-?\d{1,8})[°.]\s?(\d+)['.]\s?([0-9.]+)?\"?\s?([NWES])?/iu",$jdata[$Gkey],$mdeg)) {

                        $direction = $mdeg[4];
                        if ($mdeg[1]<0) {
                            $degree = abs($mdeg[1]);
                            $direction = 'W';
                        } else {
                            $degree = $mdeg[1];
                        }
                        #Decimal Degrees = Degrees + minutes/60 + seconds/3600
                        $x = $degree+($mdeg[2]/60)+$mdeg[3]/3600;
                        if ($direction=='S' || $direction == 'W')
                            $x = $x*-1;
                        $lon = $x;

                        $direction = $mdeg[8];
                        if ($mdeg[5]<0) {
                            $degree = abs($mdeg[5]);
                            $direction = 'W';
                        } else {
                            $degree = $mdeg[5];
                            $direction = 'E';
                        }
                        #Decimal Degrees = Degrees + minutes/60 + seconds/3600
                        $y = $mdeg[5]+($mdeg[6]/60)+$mdeg[7]/3600;
                        if ($direction=='S' || $direction == 'W')
                            $y = $y*-1;
                        $lat = $y;
                    } else
                        preg_match('/(-?\w{0,3}\d{1,2}\.\d+)[ ,-]{1}(-?\w{0,3}\d{1,2}\.\d+)/',$jdata[$Gkey],$m);

                    if (count($m)<2 and $lat=='' and $lon=='') {
                        //log_action($jdata[$G],__FILE__,__LINE__);
                        //log_action($m,__FILE__,__LINE__);
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],str_geometry_verification_failed.$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    } elseif ($lat=='' and $lon=='') {
                        $lon = trim($m[1]);
                        $lat = trim($m[2]);
                        if (preg_match('/lon/i',$lon)) {
                           $lon = preg_replace('/lon/i','',$lon);
                           $lat = preg_replace('/lat/i','',$lat);
                        } elseif (preg_match('/lon/i',$lat)) {
                           $hosszusag = preg_replace('/lon/i','',$lat);
                           $szelesseg = preg_replace('/lat/i','',$lon);
                           $lon = $hosszusag;
                           $lat = $szelesseg;
                        }
                    }
                    $cmd = sprintf("SELECT ST_GeomFromText('POINT(%.15f %.15f)', %d) AS geom",$lon,$lat,$upload_srid);
                    // Beyond current transaction - BID should allow postgis!
                    $res = pg_query($BID,$cmd);
                    if (!pg_num_rows($res)) {
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],'geometry transformation failed'.$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    } else {
                        $processed_geoms++;
                        $row = pg_fetch_assoc($res);
                        $geom = $row['geom'];
                    }
                } elseif($row['type']=='line') {
                    # check?
                    $line = '';
                    $line_cmd = "";
                    $m = array();
                    if (preg_match('/^(\w+)\((.+)\)$/i',$jdata[$Gkey],$m)) {
                        if(!preg_match('/^linestring$/i',$m[1])) {
                            $this->errors[] = warning($ii,$st_col['GEOM_C'],'LINE type WKT expected'.$jumplink,$st_col['GEOM_C']);
                            $this->soft_errors[] = 0;
                        } else {
                            $line = $m[2];
                        }
                    } else if (preg_match('/^\d+\.\d+ \d+\.\d+ 0 0;/',$jdata[$Gkey])) {
                        // KML copy-paste line string format
                        // LON-LAT ORDER!!!!!
                        // 47.7132007 19.1597366 0 0;47.7132899 19.1597807 0 0;47.7133747 19.1597512 0 0;47.7135985 19.1597915 0 0;47.7137428 19.1597942 0 0;47.7138601 19.1597646 0 0;47.7139233 19.1597271 0 0;47.7140749 19.1596708 0 0;47.7143474 19.1596466 0 0;47.7145134 19.1595554 0 0;47.7146379 19.1595232 0 0;47.7147949 19.159416 0 0;47.7149267 19.1592389 0 0;47.714993500000006 19.1591236 0 0;47.7150256 19.1590571 0 0;47.7150945 19.1588286 0 0;47.7151884 19.1585818 0 0;47.7153724 19.158217 0 0;47.7154266 19.1581258 0 0;47.7155457 19.1579273 0 0;47.71557640000001 19.1578013 0 0;47.7156882 19.1575947 0 0;47.7157875 19.1573641 0 0;47.7160997 19.1568169 0 0;47.7161701 19.1566533 0 0;47.7162423 19.1564736 0 0;47.7162977 19.1563856 0 0;47.716987700000004 19.1575371 0 0;47.7172384 19.1576537 0 0;47.7173593 19.1576672 0 0;47.7174369 19.157632300000003 0 0;47.7182056 19.15723 0 0;47.7185395 19.1570637 0 0;47.7187488 19.1569912 0 0;47.71889130000001 19.1568947 0 0;47.7192161 19.1565809 0 0;47.719301 19.1565433 0 0;47.7193876 19.1565299 0 0;47.7195608 19.1563234 0 0;47.7196619 19.1562456 0 0;47.7200228 19.1560391 0 0;47.720258400000006 19.1559541 0 0;47.7203548 19.1558915 0 0;47.720528 19.1558379 0 0;47.72071210000001 19.1556475 0 0;47.7209845 19.1554195 0 0;47.7210332 19.1553444 0 0;47.7211217 19.1552961 0 0;47.72132010000001 19.155204900000005 0 0;47.7214015 19.1551414 0 0;47.7220302 19.154361100000003 0 0
                        //LINESTRING(20.460467663337763 47.51089758010562,20.468449917365053 47.511767187935234,20.469222393561154 47.50918730933827,20.47394308142682 47.509361237725244)
                        $line = preg_replace('/ 0 0/','',preg_replace('/;/',',',$jdata[$Gkey]));
                        // implement as upload function??
                        //$line_cmd = "ST_FlipCoordinates(ST_GeomFromText('LINESTRING($line)',$upload_srid)::geometry)"
                    } else if (preg_match('/^\d+\.\d+,\d+\.\d+,0\.0 /',$jdata[$Gkey])) {
                        // KML copy-paste line string format
                        //19.1597366,47.7132007,0.0 19.1597807,47.7132899,0.0 19.1597512,47.7133747,0.0 19.1597915,47.7135985,0.0 19.1597942,47.7137428,0.0 19.1597646,47.7138601,0.0 19.1597271,47.7139233,0.0 19.1596708,47.7140749,0.0 19.1596466,47.7143474,0.0 19.1595554,47.7145134,0.0 19.1595232,47.7146379,0.0 19.159416,47.7147949,0.0 19.1592389,47.7149267,0.0 19.1591236,47.714993500000006,0.0 19.1590571,47.7150256,0.0  19.1588286,47.7150945,0.0 19.1585818,47.7151884,0.0 19.158217,47.7153724,0.0 19.1581258,47.7154266,0.0 19.1579273,47.7155457,0.0 19.1578013,47.71557640000001,0.0 19.1575947,47.7156882,0.0 19.1573641,47.7157875,0.0 19.1568169,47.7160997,0.0 19.1566533,47.7161701,0.0 19.1564736,47.7162423,0.0 19.1563856,47.7162977,0.0 19.1575371,47.716987700000004,0.0 19.1576537,47.7172384,0.0 19.1576672,47.7173593,0.0 19.157632300000003,47.7174369,0.0 19.15723,47.7182056,0.0 19.1570637,47.7185395,0.0 19.1569912,47.7187488,0.0 19.1568947,47.71889130000001,0.0 19.1565809,47.7192161,0.0 19.1565433,47.719301,0.0 19.1565299,47.7193876,0.0 19.1563234,47.7195608,0.0 19.1562456,47.7196619,0.0 19.1560391,47.7200228,0.0 19.1559541,47.720258400000006,0.0 19.1558915,47.7203548,0.0 19.1558379,47.720528,0.0 19.1556475,47.72071210000001,0.0 19.1554195,47.7209845,0.0 19.1553444,47.7210332,0.0 19.1552961,47.7211217,0.0 19.155204900000005,47.72132010000001,0.0 19.1551414,47.7214015,0.0 19.154361100000003,47.7220302,0.0
                        //LINESTRING(20.460467663337763 47.51089758010562,20.468449917365053 47.511767187935234,20.469222393561154 47.50918730933827,20.47394308142682 47.509361237725244)
                        $line = preg_replace('/_/',' ',preg_replace('/_0\.0/','',preg_replace('/\s+/',',',preg_replace('/,/','_',$jdata[$Gkey]))));
                        // implement as upload function??
                        //$line_cmd = "ST_FlipCoordinates(ST_GeomFromText('LINESTRING($line)',$upload_srid)::geometry)"
                        //log_action($line);
                    } else {
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],'wrong line format'.$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    }

                    //LINESTRING(21.32059867158332 47.39018877473387,21.595256874669953 47.44222742331253,21.68314749965736 47.605442716673295)

                    $cmd = "SELECT ST_GeomFromText('LINESTRING($line)',$upload_srid) AS geom";
                    // Beyond current transaction - BID should allow postgis!
                    $res = pg_query($BID,$cmd);
                    if (!pg_num_rows($res)) {
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],'geometry transformation failed'.$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    } else {
                        $processed_geoms++;
                        $row = pg_fetch_assoc($res);
                        $geom = $row['geom'];
                    }

                } elseif($row['type']=='wkt') {
                    $m = array();
                    //PointZ (19.06300999999999846 47.6091690000000014 117.15599799999999675)
                    if (preg_match('/^(\w+)\s?\((.+)\)$/i',$jdata[$Gkey],$m)) {
                        $mm = array();
                        if(!preg_match('/^(linestring)$|^(point)z?$|^(polygon)$/i',$m[1],$mm)) {
                            $this->errors[] = warning($k,$st_col['GEOM_C'],'only point,linestring and polygon WKT string supported'.$jumplink,$st_col['GEOM_C']);
                            $this->soft_errors[] = 0;
                        } else {
                            $wkt = $m[2];
                            if ($m[1]=='polygon') {
                                if ($force2d)
                                    $cmd = sprintf("SELECT ST_Force2D(ST_GeomFromText('POLYGON(($wkt))',%d)) AS geom",$upload_srid);
                                else
                                    $cmd = sprintf("SELECT ST_GeomFromText('POLYGON(($wkt))',%d) AS geom",$upload_srid);
                            }
                            else {
                                /* remove height USE st_force2d() !
                                 * if (preg_match('/^pointz$/i',$m[1])) {
                                    $swkt = preg_split('/ /',$m[2]);
                                    if (count($swkt)==3) {
                                        $wkt = $swkt[0].' '.$swkt[1];
                                    }
                                }*/
                                if ($force2d)
                                    $cmd = sprintf("SELECT ST_Force2D(ST_GeomFromText('%s($wkt)',%d)) AS geom",array_pop($mm),$upload_srid);
                                else
                                    $cmd = sprintf("SELECT ST_GeomFromText('%s($wkt)',%d) AS geom",array_pop($mm),$upload_srid);
                            }
                            // Beyond current transaction - BID should allow postgis!
                            //log_action($cmd);
                            $res = pg_query($BID,$cmd);
                            if (!pg_num_rows($res)) {
                                $this->errors[] = warning($ii,$st_col['GEOM_C'],'geometry transformation failed'.$jumplink,$st_col['GEOM_C']);
                                $this->soft_errors[] = 0;
                            } else {
                                $processed_geoms++;
                                $row = pg_fetch_assoc($res);
                                $geom = $row['geom'];
                            }
                        }
                    } else {
                        $geom = '';
                    }
                }
            }

            /*
             *
             * */
            if ($geom != '') {
                if ($trans_geom) {
                    // SRID transformation! - POSTGIS VERSION!
                    $cmd = "SELECT 'ST_Transform(geometry, integer)'::regprocedure;";
                    $res = pg_query($BID,$cmd);
                    if (pg_num_rows($res)==1) $f='ST_Transform';
                    else $f='Transform';

                    $cmd = "SELECT $f('$geom'::geometry,{$st_col['SRID_C']}) AS geom";
                    // Beyond current transaction - BID should allow postgis! why???,
                    // if BID is an other server can be slover, if ID is an other server can be faster anyway it has no meaning to put it to BID
                    $res = pg_query($BID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $geom = $row['geom'];
                    if ($geom=='') {
                        # wrong SRID
                        $e = pg_last_error($ID);
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],"the geometry transformation failed ($e)".$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    }
                }
                // Spatial check of geometry data
                if ( $geom_check=='spatial' and $spatial != '') {
                    $cmd = "SELECT ST_Contains(
                                st_transform(
                                    st_setsrid('$spatial'::geometry,4326),$upload_srid),
                                st_transform('$geom'::geometry,$upload_srid)) as c";
                    $res = pg_query($BID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $c = $row['c'];

                    if ($c=='f') {
                        $this->errors[] = warning($ii,$st_col['GEOM_C'],"the geometry not within the allowed spatial limits".$jumplink,$st_col['GEOM_C']);
                        $this->soft_errors[] = 0;
                    }
                }
            }
            # if no geometry
            if ($geom == '' and $geom_obl) {
                //$this->errors[] = warning($j->{'rows'}[$i],'','- no valid geometry');
                $this->errors[] = warning($ii,$st_col['GEOM_C'],str_no_geometry,$st_col['GEOM_C']);
                $this->soft_errors[] = 0;
            }

            /*
             *
             * */
            if ($this->massiveedit) {
                $nv = array();
                if ($geom=='') $geom = 'NULL';
                else $geom = "'$geom'";
                $s = 0;
                foreach($jid as $jid_key=>$jid_val) {
                    if ($jid_val=='') continue;

                    if ($jid_key=='obm_id') {
                        //dequote quoted obm_id
                        $obm_id = $v[$s];
                        if (preg_match('/\$[a-z]+\$([0-9]+)\$[a-z]+\$/i',$v[$s],$m)) {
                            $obm_id = $m[1];
                        }
                        if (!in_array($obm_id,$this->massiveedit_ids)) {
                            $invalid_upload_id[] = $obm_id;
                        }

                        $upload_id[] = $obm_id;
                        $s++;
                        continue;
                    }
                    elseif ($jid_key==$st_col['GEOM_C']) {
                        continue;
                    }
                    $nv[] = sprintf('"%s"=%s',$jid_key,$v[$s]);
                    $s++;
                }
                $nv[] = sprintf('%s=%s',$st_col['GEOM_C'],$geom);
                $values[] = implode(",",$nv);
                $this->uploading_id = -99; // set an invalid uploading_id
            } else {
                if ($geom=='') $geom = 'NULL';
                else $geom = "'$geom'";

                // obm_uploading_id should be exists!!!
                if (count($v)) {
                    if ($st_col['GEOM_C'] != '')
                        $values[] = implode(",",$v).",#CURRENT_OBM_UPLOADING_ID#,$geom";
                    else
                        $values[] = implode(",",$v).",#CURRENT_OBM_UPLOADING_ID#";
                } else {
                    if ($st_col['GEOM_C'] != '')
                        $values[] = "#CURRENT_OBM_UPLOADING_ID#,$geom";
                }
            }

            if (isset($_SESSION['import_api_ids'])) {
                $import_api_ids[] = $_SESSION['import_api_ids'][$i];
            }
        }# end $i - data rows loop

        // memory free
        unset($j);

        # checking if species lists, all elements available
        # IT CAN BE MORE GENERAL!!!!
        if (count($list_elements)) {
            $cmd = "SELECT unnest(list) as list,fullist FROM project_forms_data WHERE form_id='$this->selected_form_id' AND \"column\"='{$st_col['SPECIES_C']}'";
            $res = pg_query($BID,$cmd);
            $mis = array();
            while ($row = pg_fetch_assoc($res)) {
                if ($row['list']!='' and $row['fullist']==1 and !in_array($row['list'],$list_elements)) {
                    #we check list content only if it is not an autogenerated list!
                    #SELECT:balkanherps_species.spcode:sp
                    if (!preg_match('/^SELECT:/',$row['list']))
                        $mis[] = $row['list'];
                }
            }
            if (count($mis)) {
                $this->errors[] = warning(1,$st_col['SPECIES_C'],"Missing elements from the species list: <i>%s</i>",implode($mis,', '));
                $this->soft_errors[] = 0;
            }
        }
        # checking all mandatory fields are avilable
        if (count($jid)) {
            foreach($obl_fields as $o) {
                if (!in_array($o,$jid)) {
                    if ($st_col['GEOM_C']==$o and $transformed_geom_from_coords==1) continue;
                    // only one error message for web interface
                    //$this->errors[] = str_missing_mandatory_field.": <i>$o</i>";
                    log_action("Missing mandatory field: $o - Is this form changed???",__FILE__,__LINE__);
                    $this->errors[] = warning(1,$o,str_missing_mandatory_field);
                    $this->soft_errors[] = 0;
                }
            }
        }

        if (count($ssc)) $warnings[] = str_skip_marked_rows." [".count($ssc)."]";

        // clean warnings if page was reloaded and warning count has not changed
        // It is not a perfect solution for warnings handling ...
        // For web warnings - in API import_warnings is allways empty!
        //
        if (isset($_SESSION['import_warnings']) and count($warnings) == $_SESSION['import_warnings'])
            $warnings = array();

        $iambeer = array();
        if (count($this->errors)) {
            $expl = "<span style='font-size:150%;color:red'>".str_some_errors."</span><br>";
            $e = array();
                //[0,1,0,1,0,1,0,1,0,1,0,1,0,1]
            for ($sk = 0; $sk<count($this->errors);$sk++) {
                $e[] = $this->errors[$sk]."<br>";
                //echo "$e<br>";
                //
                if (isset( $this->soft_errors[$sk] ) and $this->soft_errors[$sk]==1) {
                    if (isset($_POST['soft_error'][$sk]))
                        $skval = $_POST['soft_error'][$sk];
                    else
                        $skval = '';
                    #echo sprintf('&nbsp; &nbsp; &nbsp; &nbsp; Are you really sure what are you doing? (type: "'.t(str_yes).'") <input class="iambeer" value="%s"><br>',$skval);
                    $e[] = sprintf('&nbsp; &nbsp; &nbsp; &nbsp; Are you really sure what are you doing? (type: "'.t(str_yes).'") <input class="iambeer" value="%s"><br>',$skval);
                    $iambeer[] = sprintf('<input type="hidden" class="iambeer" value="%s"><br>',$skval);
                }
            }
            // egyelőre nem sok értelme van az api_warnings megkülönböztetésének, de megadja  a lehetőséget valami tagolt tömb készítésének, ami kezelehető html processing nélkül is az api kliensnek
            /*if (count($this->errors) and isset($_SESSION['api_warnings']) and count($_SESSION['api_warnings'])) {
                foreach ($_SESSION['api_warnings'] as $key=>$value) {
                    $e[] = "$key: ".implode("<br>",$_SESSION['api_warnings'][$key]);
                }
            }*/

            if (array_sum($this->soft_errors)>0) {
                // send question: Are you really want to try import data?
                // ... as warning
                if (isset($_POST['soft_error'])) {
                    for($sk=0;$sk<count($_POST['soft_error']);$sk++) {
                        if ($_POST['soft_error'][$sk] == t(str_yes)) {
                            //ok, no more warnings
                            unset($this->errors[$sk]);
                        }
                    }
                }

                //still have soft errors
                if (count($this->errors)) {
                    if ( isset($_SESSION['api_warnings']) and count($_SESSION['api_warnings']))
                        $this->messages[] = common_message('error',json_encode($_SESSION['api_warnings']));
                    else
                        $this->messages[] = common_message('error',$expl.implode($e));
                    return false;
                }
            } else {
                // There are errors, we exit here.
                if ( isset($_SESSION['api_warnings']) and count($_SESSION['api_warnings'])) {
                    /* handling API oauth login
                     * megkapjuk session-ben a pds_class-ból
                     **/
                    /*$cmd = sprintf("SELECT id,username
                            FROM users
                            LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email)
                            WHERE access_token=%s",quote($_REQUEST['access_token']))
                    $res = pg_query($BID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $login_id = $row['id'];
                    $login_name = $row['Tname'];*/

                    // Debugging API uploads
                    // save uploaded data into openbiomaps.log
                    if (file_exists('/etc/openbiomaps/debug_uploads')) {
                        $cmd = sprintf('SELECT user_id FROM oauth_access_tokens WHERE access_token=%s',quote($_REQUEST['access_token']));
                        $res = pg_query($BID,$cmd);
                        if (pg_num_rows($res)) {
                            $row = pg_fetch_assoc($res);
                            $lines = file('/etc/openbiomaps/debug_uploads');
                            foreach ($lines as $line) {
                                list($debug_user,$debug_mode,$debug_project,$debug_form) = preg_split('/ /',trim($line));

                                if (!isset($debug_project) or $debug_project=='')
                                    $debug_project = PROJECTTABLE;

                                if (!isset($debug_form) or $debug_form=='')
                                    $debug_form = $selected_form_id;

                                if ($row['user_id'] == $debug_user and $debug_project==PROJECTTABLE and $debug_form==$selected_form_id) {

                                        $debug_cols = array_filter($cols);
                                        $debug_cols = '"' . implode('","', $debug_cols) . '"';
                                        $debug_cmd = "INSERT INTO $destination_table ($debug_cols) VALUES ";
                                        $debug_cmd .= "(".implode("),(",$values).")";

                                        log_action($debug_cmd,'API upload debug',$_SESSION['Tmail']);
                                        if (isset($debug_mode) and $debug_mode='return_ok') {
                                            $this->messages[] = common_message("ok",array(str_upload_extent=>'',str_upload_length=>'0'));
                                            return true;;
                                        }
                                }
                            }
                        }
                    }

                    $this->messages[] = common_message('error',json_encode($_SESSION['api_warnings']));
                } else
                    $this->messages[] = common_message('error',$expl.implode($e));
                return false;
            }
        }

        if (count($warnings)) {
            if ($this->selected_form_type=='api') {
                if (isset($_SESSION['api_warnings']) and count($_SESSION['api_warnings'])) {
                    $n = 0;
                    foreach ($_SESSION['api_warnings'] as $key=>$value) {
                        $e[] = str_row.": {$warnings[$n]}, ".str_column.": $key: ".implode("<br>",$_SESSION['api_warnings'][$key]);
                        $n++;
                    }
                }

                // Az import_warnings nem működik!!!!
                //echo common_message('error',json_encode($e));
                $this->messages[] = common_message('error',json_encode($_SESSION['api_warnings']));
            } else {
                $expl = "<span style='font-size:150%;color:orange'>".str_some_warnings."</span><br>";
                $e = array();
                foreach ($warnings as $w) $e[] = "$w<br>";
                if (count($iambeer))
                    $this->messages[] = common_message('error',$expl.implode($e).'<br>'.implode($iambeer));
                else
                    $this->messages[] = common_message('error',$expl.implode($e));

            }
            $_SESSION['import_warnings'] = count($warnings);
            return false;
        }
        unset($_SESSION['import_warnings']);

        #if ($error) {
        #    echo "Some errors occured! Ommit the affected lines or repair the errors.";
        #    exit;
        #}

        //pg_query($ID,'ROLLBACK');
        //echo $cmd;exit;

        // Update MAP extent automatically!
        // FURTHER FEATURE
        /*$extent = PGquery($ID,"select ST_Extent(the_geom) as extent from ".PROJECTTABLE);
        $row = pg_fetch_assoc($extent);
        $e = preg_replace("/[^0-9., ]/",'',$row['extent']);
        $e = preg_replace("/,/"," ",$e);
        json ...
        POST['tpost']
        update_mapfile.php
         */

        /* UPDATE PROJECT_taxon table FROM TRIGGER
         * insert the alternative names
         * insert the scientific names
         * */

        #EOV to UTM
        #SELECT AsText(Transform(GeomFromText('POINT(600000 200000)',23700),32634));

        /*"INSERT INTO ".PROJECTTABLE." (\"gid\",\"objectid\",\"faj\",\"magyar\",\"eov_x\",\"eov_y\",\"hely\",\"szamossag\",\"meret_kb\",\"egyedszam\",\"him\",\"nosteny\",\"eloford_al\",\"gyujto\",\"gyujto2\",\"gyujto3\",\"hatarozo\",\"adatkozlo\",\"dt_from\",\"dt_to\",\"modsz\",\"elohely\",\"veszteny\",\"kapcs_dok\",\"megj\",\"rogz_dt\",\"rogz_modsz\",\"altema\",\"the_geom\",\"taxon_id\",\"uploading_id\",\"validation_\",\"comments_\",\"modifier_id\",\"natura2000\",\"vedettseg\",\"tema\",\"ob_datum\")
            VALUES
            (nextval('dinpi_gid_seq'::regclass),'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','$uploading_id','','','','','','','')";*/
        //echo $cmd.'<br>';
       // pg_query($ID,"ROLLBACK");

        /* debug uploads - not implemented, just a theoretical option */
        if ($upload_debug) {
            log_action($processed_geoms);
            //echo "Do not upload";
            return false;
            //exit;
        }

        /* INSERT uploading
        /*
        /* hogyan kezeljük a be nem jelentkezett felhasználókat?
        /* */
        if (!isset($_SESSION['Tid'])) $login_id = -1;
        else $login_id = $_SESSION['Trole_id'];

        if (!isset($_SESSION['Tname'])) $login_name = 'unknown user';
        else $login_name = $_SESSION['Tname'];

        /* handling API oauth login
         * megkapjuk session-ben a pds_class-ból
        if ($login_id == -1 and isset($_REQUEST['access_token'])) {
            $cmd = sprintf("SELECT id,username FROM users
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email)
                WHERE access_token=%s ",quote($_REQUEST['access_token']));
            $res = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($res);
            $login_id = $row['id'];
            $login_name = $row['Tname'];
        }*/


        /* Normal uploads */
        if (!$this->massiveedit) {
            $data_groups = ($this->data_groups == null) ? $login_id : $this->data_groups;

        // metadata processing
        /* metadata = {
                id,
                app_version,
                form_version,
                gps_accuracy,
                started_at,
                finished_at,
                observation_list_id,
                *observation_list_start,
                *observation_list_end,
               }*/
            if (isset($_POST['metadata'])) {
                $metadata = $_POST['metadata'];
                $md = json_decode($metadata,TRUE);
                if (isset($md['id']) and $md['id']!='') {
                    // API upload test
                    $cmd = sprintf("SELECT 1 FROM \"system\".\"uploadings\" WHERE metadata IS NOT NULL AND  metadata ->> 'id'='%s'",$md['id']);
                    $res = pg_query($GID,$cmd);
                    if (pg_num_rows($res)) {
                        //$this->messages[] = common_message('error',str_data_already_uploded);
                        $this->messages[] = common_message('error','Data already uploded!'); // Do not translate this, the mobile-app use this error string!!!
                        pg_query($GID,"ROLLBACK");
                        return false;
                    }
                }
            } else
                $metadata = NULL;

            // optional
            // background processing after the first 100 records?

            // replace destination table to temporary_tables.destination_table_obm_... if it is a temp upload
            if ($this->temp_destination) $destination_table = $this->temp_destination;

            pg_query($GID,'BEGIN');
            // SET LOCAL WORK_MEM !!!
            pg_query($GID,"SET local work_mem = '64MB'");
            $cmd = sprintf("INSERT INTO \"system\".\"uploadings\"
                    (\"id\",\"uploading_date\",\"uploader_id\",\"uploader_name\",\"validation\",\"collectors\",\"description\",\"owner\",\"group\",\"project_table\",\"project\",\"form_id\",\"metadata\")
                    VALUES (nextval('uploadings_id_seq'::regclass),NOW(),%d,%s,NULL,'',%s,'{%s}','{%s}',%s,%s,%d,%s) RETURNING id",
                    $login_id,quote($login_name),quote($_POST['description']),$this->data_owners,$data_groups,quote($destination_table),quote(PROJECTTABLE),$this->selected_form_id,quote($metadata));

            $res = pg_query($GID,$cmd);
            $row = pg_fetch_assoc($res);

            if (!count($row)) {
                $errorID = uniqid();
                if ($this->selected_form_type=='api') {
                    $this->messages[] = common_message('fail',str_insert_error,$errorID);
                }
                log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);

                pg_query($GID,"ROLLBACK");
                return false;
            }
            $this->uploading_id = $row['id'];
            $_SESSION['last_upload_id'] = $row['id'];
            /* it is a resurce intensive solution. Would be much easier to query uploading_id before assemble values
             * BUT the sequence generation is not handled by the transactions, therefore the uploading's ids increasing too fast
            /*
             * array map function */
            $values = array_map(array($this,'uploading_id_replace'),$values);
        }
        //removing pseudo_columns and its values from the SQL command
        //{"meret_kb":{"columns":["meta_1","meta_2"],"values":["1",""],"destination_table":["dinpi_meta","dinpi_meta"]}}
        if (count($pseudo_data)) {
            foreach($pseudo_data as $psk=>$psv) {
                foreach($psv['columns'] as $ps) {
                    $pck = array_search($ps,$cols);
                    array_splice($cols,$pck,1);
                }
            }
        }

        /* assemble INSERT command */
        $cols = array_filter($cols);
        if (count($cols)) {
            $cols = '"' . implode('","', $cols) . '"';

            // obm_uploading_id should be exists

            if ($st_col['GEOM_C'] != '')
                $cols .= ",\"obm_uploading_id\",\"{$st_col['GEOM_C']}\"";
            else
                $cols .= ",\"obm_uploading_id\"";
        } else {
            // no other columns, just geometry specified. very strange...

            if ($st_col['GEOM_C'] != '')
                $cols = "\"obm_uploading_id\",\"{$st_col['GEOM_C']}\"";
            else
                $cols = "\"obm_uploading_id\"";

        }

        if (implode("",$values)=='') {
            // API upload: mobile app sends metadata
            // a JSON array:
            /* metadata = {
                id,
                app_version,
                form_version,
                gps_accuracy,
                started_at,
                finished_at,
                observation_list_id,
                *observation_list_start,
                *observation_list_end,
               }*/
            if (isset($_POST['metadata'])) {
                $metadata = json_decode($_POST['metadata'],TRUE);
                if (isset($metadata['observation_list_start']) and isset($metadata['observation_list_end']) and
                    $metadata['observation_list_start']!='' and $metadata['observation_list_end']!='') {
                    $this->messages[] = common_message('ok','Observation list metadata uploaded'); // this message text is processed by the mobile app!!! Do not change it.
                    pg_query($GID,"COMMIT");
                    return true;
                }
            }
            // empty data table,
            $this->messages[] = common_message('error', str_blank_sheet);
            pg_query($GID,"ROLLBACK");
            return false;
        }

        $cmd = "INSERT INTO $destination_table ($cols) VALUES ";
        // obm_id should be exists in every table!!!
        $cmd .= "(".implode("),(",$values).") RETURNING obm_id";
        //$cmd .= "(".implode("),(",$values).")";
        //log_action($cmd,__FILE__,__LINE__);

        if ($modules->is_enabled('massive_edit') and $this->massiveedit) {
            $cmd = '';
            $count_values = count($values);

            if (!count($upload_id)){
                $this->messages[] = common_message('error','Edit ID column assigned to obm_id!');
                log_action("Edit ID column assigned to obm_id!",__FILE__,__LINE__);
                pg_query($GID,"ROLLBACK");
                return false;
            } else {
                for($o=0;$o<$count_values;$o++) {
                    $vava = $values[$o];
                    if (!isset($upload_id[$o]) or $upload_id[$o]=='') {
                        echo "Undefined EDID<br>";
                        continue;
                    }
                    $va_id = $upload_id[$o];
                    if (in_array($va_id,$invalid_upload_id)) {
                        echo "Invalid EDID: $va_id - skipped<br>";
                        log_action("invalid EDID: $va_id",__FILE__,__LINE__);
                        continue;
                    }
                    // dinamikus ID vagy pedig csak a main tábla szerkeszthető!!!
                    $cmd .= sprintf("UPDATE $destination_table SET %s WHERE obm_id=%d;",$vava,$va_id);
                }
            }
        }

        /* Execute insert command */
        //log_action($cmd);
        $res = pg_query($GID,$cmd);
        if(!$res or !pg_affected_rows($res)) {
            $errorID = uniqid();
            $this->messages[] = common_message('fail',str_insert_error." #$errorID#");
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            log_action("ErrorID#$errorID# "."IMPORT upload_error: $cmd",__FILE__,__LINE__);

            pg_query($GID,"ROLLBACK");
            return false;
        }
        else {
            // 3D table modell - insert into joined tables if there are such columns
            // It can be a big table!!!
            // These INSERT can be lot...
            while ($id_row = pg_fetch_assoc($res)) {

                //{"meret_kb":{"columns":["meta_1","meta_2"],"values":["65","65"],"destination_table":["dinpi_meta","dinpi_meta"]}}
                    foreach($pseudo_data as $key=>$rfc) {
                        $cols = '"' . implode('","', $pseudo_data[$key]['columns']) . '"';
                        $cols .= ",column_name,rowid,obm_uploading_id";
                        $values = implode(',',array_map('quote',$pseudo_data[$key]['values']));
                        $values .= ",".quote($key).",".$id_row['obm_id'].",".$this->uploading_id;
                        $cmd = sprintf("INSERT INTO %s (%s) VALUES(%s)",$pseudo_data[$key]['destination_table'][0],$cols,$values);
                        $res = pg_query($GID,$cmd);
                        if(!pg_affected_rows($res)) {
                            $errorID = uniqid();
                            $this->messages[] = common_message('fail',str_insert_error." #$errorID#");
                            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
                            log_action("ErrorID#$errorID# "."IMPORT upload_error: $cmd",__FILE__,__LINE__);
                            pg_query($GID,"ROLLBACK");
                            return false;
                        }
                    }
            }
            $n = pg_affected_rows($res);

            // Creating custom notify-listening event
            if ($mki = $modules->is_enabled('custom_notify'))
                $modules->_include('custom_notify','listen',[],true);

            pg_query($GID,"COMMIT");
            if (!isset($_SESSION['Tname'])) $name = 'Unknown user';
            else $name = $_SESSION['Tname'];

            $envelope = '';
            $uploaded_row_count = '';

            if (!$this->massiveedit) {
                // normal upload
                if ($geom!='' and !is_null($geom) and $geom!='NULL') {
                    $ecmd = "SELECT ST_asText(ST_Extent(obm_geometry)) as bb,count(*) FROM $destination_table WHERE obm_uploading_id=$this->uploading_id";
                    $eres = pg_query($GID,$ecmd);
                    if (pg_num_rows($eres)) {
                        $erow = pg_fetch_assoc($eres);
                        $envelope = $erow['bb'];
                        $uploaded_row_count = $erow['count'];
                    }
                } else {
                    $ecmd = "SELECT count(*) FROM $destination_table WHERE obm_uploading_id=$this->uploading_id";
                    $eres = pg_query($GID,$ecmd);
                    if (pg_num_rows($eres)) {
                        $erow = pg_fetch_assoc($eres);
                        $uploaded_row_count = $erow['count'];
                    }
                }
            } else {
                // massive edit
                $uploaded_row_count = count($upload_id);
            }


            // Createing automatic news event
            $uplnewslink = "<a href='?history=upload&id=$this->uploading_id'>$n</a>";
            insertNews("$name uploaded [$uplnewslink] new data to ".PROJECTTABLE." project.",'project');

            // Creating custom notify event
            if ($mki) {
                $modules->_include('custom_notify','notify',[],true);
            }

            if (isset($_SESSION['upload']['loadref'])) {
                $cmd = sprintf("DELETE FROM system.imports WHERE ref=%s AND template_name IS NULL",quote($_SESSION['upload']['loadref'],1));
                $res = pg_query($GID,$cmd);
                unset($_SESSION['upload']['loadref']);
                // regenerate id for saveref
                $_SESSION['upload']['saveref'] = crc32(session_id().$this->selected_form_id.$this->selected_form_type.microtime());
            }
            if (isset($_SESSION['upload']['saveref'])) {
                $cmd = sprintf("DELETE FROM system.imports WHERE ref=%s AND template_name IS NULL",quote($_SESSION['upload']['saveref'],1));
                $res = pg_query($GID,$cmd);

                $cmd = sprintf('DROP TABLE IF EXISTS temporary_tables.upload_%1$s_%2$s_%3$s',PROJECTTABLE,session_id(),$_SESSION['upload']['saveref']);
                $result = pg_query($ID,$cmd);
                unset($_SESSION['upload']['saveref']);
                // regenerate id for saveref
                $_SESSION['upload']['saveref'] = crc32(session_id().$this->selected_form_id.$this->selected_form_type.microtime());
            }


            if (isset($_SESSION['import_api_ids'])) {
                $dest = getenv('PROJECT_DIR').'/attached_files';
                foreach($import_api_ids as $iai) {
                    $cmd = sprintf("SELECT unnest(files) as file FROM pds_upload_data WHERE user_id='%d' AND project_table='%s' AND form_id='%d' AND id='%d'",$_SESSION['Tid'],PROJECTTABLE,$this->selected_form_id,$iai);
                    $res = pg_query($BID,$cmd);
                    //delete attached files
                    while ($row = pg_fetch_assoc($res)) {
                        $sum = sha1_file(OB_TMP.PROJECTTABLE."/".$_SESSION['Tid']."/".$this->selected_form_id."/".$row['file']);
                        //akárki akármilyen néven töltötte fel és még progress állapotú - az md5sum egyértelműen azonosítja!
                        $cmd = sprintf("SELECT id,reference FROM system.files WHERE status='progress' AND sum='%s'",$sum);
                        $res2 = pg_query($ID,$cmd);
                        $row2 = pg_fetch_assoc($res2);
                        //double check!
                        //delete files already copyed to attached_files
                        if (file_exists($dest."/".$row2['reference'])) {
                            pg_query($GID,"UPDATE system.files SET status='valid' WHERE id='{$row2['id']}'");
                            unlink(OB_TMP.PROJECTTABLE."/".$_SESSION['Tid']."/".$this->selected_form_id."/".$row['file']);
                        } else {
                        }
                    }
                    $cmd = sprintf("DELETE FROM pds_upload_data WHERE user_id='%d' AND project_table='%s' AND form_id='%d' AND id='%d'",$_SESSION['Tid'],PROJECTTABLE,$this->selected_form_id,$iai);
                    $res = pg_query($BID,$cmd);
                }
                unset($_SESSION['import_api_ids']);
            }

            if(isset($_SESSION['sheetDataPage'])) unset($_SESSION['sheetDataPage']);
            if(isset($_SESSION['sheetDataPage_counter'])) unset($_SESSION['sheetDataPage_counter']);
            //if(isset($_SESSION['sheetData'])) unset($_SESSION['sheetData']);
            //if(isset($_SESSION['theader'])) unset($_SESSION['theader']);
            if ($this->selected_form_type=='web')

                $this->messages[] = common_message("ok",sprintf("OK.w%s<br>%s: $uploaded_row_count<br>%s: $envelope",t(str_done),t(str_upload_length),t(str_upload_extent)));

            elseif ($this->selected_form_type=='file')

                $this->messages[] = common_message("ok",sprintf("OK.f%s<br>%s: $uploaded_row_count<br>%s: $envelope",t(str_done),t(str_upload_length),t(str_upload_extent)));

            else

                $this->messages[] = common_message("ok",array(str_upload_extent=>$envelope,str_upload_length=>$uploaded_row_count));

            // email alerts to admin
            $email_alert = true;
            if (defined('EMAIL_ALERTS_ON_UPLOADS') and (EMAIL_ALERTS_ON_UPLOADS=='off' or EMAIL_ALERTS_ON_UPLOADS=='0' or EMAIL_ALERTS_ON_UPLOADS==false)) {
                $email_alert = false;
            }

            if ($email_alert) {
                if (defined("OB_PROJECT_DOMAIN")) {
                    $domain = constant("OB_PROJECT_DOMAIN");
                    $server_email = PROJECTTABLE."@".parse_url("$protocol://".$domain,PHP_URL_HOST);
                    $reply_email = "noreply@".parse_url("$protocol://".$domain,PHP_URL_HOST);
                } else {
                    $domain = $_SERVER["SERVER_NAME"];
                    $server_email = PROJECTTABLE."@".$domain;
                    $reply_email = "noreply@".$domain;
                }

                $cmd = sprintf("SELECT u.email AS email,p.email AS aemail, u.username AS name FROM projects p
                    LEFT JOIN project_users pu ON (pu.project_table=p.project_table)
                    LEFT JOIN users u ON (user_id=u.id)
                    WHERE p.project_table='%s' AND user_status::varchar IN ('2','master') AND pu.receive_mails!=0",PROJECTTABLE);
                $res = pg_query($BID,$cmd);
                $emails = array();
                $names = array();
                while ( $row = pg_fetch_assoc($res)) {
                    $names[] = $row['name'];
                    $emails[] = $row['email'];
                    //$emails[] = $row['aemail'];
                }
                $emails = array_unique($emails);

                if (count($emails)) {
                    $Subject = "$uploaded_row_count records uploaded into ". PROJECTTABLE;

                    $Title = 'Hi '.implode(', ',$names).'!';
                    $Message = '';
                    $qtable = '';
                    if ($destination_table != PROJECTTABLE) {
                        $Message .= "$uploaded_row_count records uploaded into ". $destination_table;
                        $m = preg_split('/_/',$destination_table);
                        array_shift($m);
                        $qtable = "&qtable=".implode('_',$m);
                    }

                    $Message .= "Follow this link to check this upload on map:<br>";
                    $Message .= "&nbsp; &nbsp; "."<a href='http://".URL."/index.php?query=obm_uploading_id:$this->uploading_id".$qtable."'>http://".URL."/index.php?query=obm_uploading_id:$this->uploading_id".$qtable."</a><br><br>";
                    $Message .= "Follow this link to see upload details:<br>";
                    $Message .= "&nbsp; &nbsp; "."<a href='http://".URL."/index.php?history=upload&id=$this->uploading_id'>http://".URL."/index.php?history=upload&id=$this->uploading_id</a>";

                    $m = mail_to($emails,$Subject,"$server_email","$reply_email",$Title,$Message,"multipart");
                }
            }

            return true;
        }

	return false;
    }

    // array_map function
    function uploading_id_replace($ain) {
        $aot = preg_replace('/#CURRENT_OBM_UPLOADING_ID#/',$this->uploading_id,$ain);
        return $aot;
    }

    // relation_check('obligatory',$rel_fields,$rel_statement,$rel_value,$jid,$i)
    // magyar: (faj=!!(^.+$)){obligatory(1);set(.)}
    function relation_check ($rel_function,$rel_fields,$rel_statement,$rel_type,$rel_value,$jid,$jdata,$k,$return) {

        global $RELATION_CHECK_LOGGING;

        // pseudo columns not exists in jid, therefore cannot handle relation checks!!
        if (!isset($jid[$k]))
            return $return;

        /*
        debug($k,__FILE__,__LINE__);
        debug($rel_function,__FILE__,__LINE__);
        debug($rel_fields,__FILE__,__LINE__);
        debug($rel_statement,__FILE__,__LINE__);
        debug($rel_type,__FILE__,__LINE__);
        debug($rel_value,__FILE__,__LINE__);
        debug($jid,__FILE__,__LINE__);
         */

        // filed has relation check
        // jid[k] is the assigned name of the column
        if (isset($rel_fields[$jid[$k]])) {
            // TRY $rel_function on $jid[$k]
            $ret = $return;
            for($z=0;$z<count($rel_fields[$jid[$k]]);$z++) {
                // TEST $rel_type[$jid[$k]][$z] on $rel_function
                // field's function is the called function

                if ($rel_type[$jid[$k]][$z]==$rel_function) {
                    // referred column name
                    $index = $rel_fields[$jid[$k]][$z];

                    if ( preg_match('/^!!(.+)$/',$rel_statement[$jid[$k]][$z],$we) ) {
                        // regexp type condition
                        if ( isset($jdata[$index]) and preg_match( "/".$we[1]."/", $jdata[$index] ) ) {
                            $return = $this->statements($rel_value,$jid,$jdata,$k,$index,$z,$we,$rel_function);
                        }
                        if (!isset($jdata[$index])) {
                            log_action("The referred column name not exists among the available columns. Not assigned?",__FILE__,__LINE__);
                        }

                        if ($RELATION_CHECK_LOGGING) {
                            log_action('preg_match('."/".$we[1]."/,".$jdata[$index].')');
                        }
                    } elseif ( trim($jdata[$index]) == $rel_statement[$jid[$k]][$z]) {
                        // normal `equal to` condition
                        // (visszafogas=1) {obligatory(1)}
                        // 0 1 ''
                        $return = $this->statements($rel_value,$jid,$jdata,$k,$index,$z,$we,$rel_function);

                        if ($RELATION_CHECK_LOGGING) {
                            log_action($jdata[$index] .'=='. $rel_statement[$jid[$k]][$z]);
                        }
                    } else {
                        // The value of the indexed cell and the value of the current cell do not match
                        // Returns the value of the cell
                        if ($RELATION_CHECK_LOGGING) {
                            log_action('There is no match for the condition: '.$jdata[$index] . ' ?? ' . $rel_statement[$jid[$k]][$z]);
                        }
                    }
                    break;
                }
            }

        }
        return $return;
    }

    // relation pseudolanguage functions
    // ez sem nem elegáns, sem nem szép, de működik
    // ha nem szedem ki függvénybe, mocsok lesz a kód a statement_ blokkok ismételgetésétől...
    function statements($rel_value,$jid,$jdata,$k,$index,$z,$we,$rel_function) {
        $f = 'statement_'.$rel_function;
        //$return = call_user_func($this->$f(),$rel_value,$jid,$j,$k,$index,$z,$we);
        $return = call_user_func_array(array($this,$f), array($rel_value,$jid,$jdata,$k,$index,$z,$we));
        return  $return;
    }

    function statement_obligatory($rel_value,$jid,$jdata,$k,$index,$z,$we) {
        $r = $rel_value[$jid[$k]][$z];
        return $r;
    }

    function statement_minmax($rel_value,$jid,$jdata,$k,$index,$z,$we) {
        $r = $rel_value[$jid[$k]][$z];
        $minmax = preg_split('/:/',$r);

        if (count($minmax)==2) {
            if ($minmax[0]=='set(.)') {
                $minmax[0] = $jdata[$index];
            }
            elseif ($minmax[1]=='set(.)') {
                $minmax[1] = $jdata[$index];
            }
            $r = implode(':',$minmax);
        }

        return $r;
    }

    // set
    // return a set value
    function statement_set ($rel_value,$jid,$jdata,$k,$index,$z,$we) {
        if ($rel_value[$jid[$k]][$z] == '.') {
            # set val based on statement match - ignore current cell value
            //if (trim($jdata[$k]) != '' )
                $val = $jdata[$index];
            //else
            //    $val = "";
        } elseif ($rel_value[$jid[$k]][$z] == '.+') {
            # append current cell value to matched index cell value
            if (trim( $jdata[$k] )!='' )
                $val = $jdata[$index].$jdata[$k];
            else
                $val = "";
        } elseif ($rel_value[$jid[$k]][$z] == '+.') {
            //prepend ...
            if (trim( $jdata[$k] )!='' )
                $val = $jdata[$k].$jdata[$index];
            else
                $val = "";
        } else {
            $val = $rel_value[$jid[$k]][$z];
        }

        return $val;
    }

    /*function statement_year ($rel_value,$jid,$j,$k,$index,$z,$we) {
        $year = '';
        if ($rel_value[$jid[$k]][$z] == '.') {
            # set year based on statement match - ignore current cell value
            $year = $j->{'data'}[$index][$k];
        } elseif ($rel_value[$jid[$k]][$z] == '.+') {
            # append current cell value to matched index cell value
            $year = $j->{'data'}[$index][$k].$j->{'data'}[$k];
        } elseif ($rel_value[$jid[$k]][$z] == '+.') {
            $year = $j->{'data'}[$k].$j->{'data'}[$index][$k];
        }
        return $year;
    }*/

    //inequality_statements($rel_value,$jid,$i,$index,$z,$k)
    function statement_inequality($rel_value,$jid,$jdata,$k,$index,$z,$we) {
        # e.g. on end_date: (found_date=!!^(.+)$) {inequality(+>=.)}
        # if end_date is not empty, check end_date is greater than found_date? if yes, return true
        #
        # $we contains a regexp statemnet which should match the indexed cell's content
        #       ["!!^(.+)$","^(.+)$"]
        # $rel_value contains all columns' rel_values:
        #       {"found_date":["."],"laying_date":["."],"enddate":[".","+>=."],"length1":["1"],"breath1":["1"],"length2":["1"],"breath2":["1"],"length3":["1"],"breath3":["1"]}
        # e.g end_date condition is the following: (year=!!^(\d{4})$) {set(.)},(found_date=!!^(.+)$) {inequality(+>=.)}
        if ( preg_match( "/".$we[1]."/",$jdata[$index] ) ) {

            if ( preg_match("/(.+?)([<>=]{1,2})(.+)/",$rel_value[$jid[$k]][$z],$r) ) {

                $tzz = array('.'=>$jdata[$index],'+'=>$jdata[$k]);
                if ($r[2]=='>') {
                    $tzz = array('.'=>$jdata[$index],'+'=>$jdata[$k]);
                    if ( $tzz[$r[1]] > $tzz[$r[3]] ) {
                        return true;
                    }
                    return false;
                } elseif ($r[2]=='<') {
                    if ( $tzz[$r[1]] < $tzz[$r[3]] ) {
                        return true;
                    }
                    return false;
                } elseif ($r[2]=='=') {
                    if ( $tzz[$r[1]] == $tzz[$r[3]] ) {
                        return true;
                    }
                    return false;
                } elseif ($r[2]=='<=') {
                    if ( $tzz[$r[1]] <= $tzz[$r[3]] ) {
                        return true;
                    }
                    return false;
                } elseif ($r[2]=='>=') {
                    if ( $tzz[$r[1]] >= $tzz[$r[3]] ) {
                        return true;
                    }
                    return false;
                } elseif ($r[2]=='<>') {
                    if ( $tzz[$r[1]] <> $tzz[$r[3]] ) {
                        return true;
                    }
                    return false;
                }
            }
        }
        return true;
    }

// end class
}

/* Geometry processing
 * ez jelenleg nem működik a session változók kigyomlálása miatt
 *
 * */
class geom_process {
    public $error='';
    public $upload_comment='';
    public $show_geom_ref ='';
    public $theader = '';
    public $sheetData ='';


    function gprocess($show_geom_ref) {
        global $ID,$BID,$selected_form_id,$insert_allowed;
        /* Ez nagyon frankó egy api függvény lehetne
         * Nincs jogosultság rendezés, de ez lehetne az API hívás jogosultsághoz kötve
         * */

        $st_col = st_col($destination_table,'array');

        $selected_form_type = 'api';
        $acc = 0;
        $group_filter='';
        if (isset($_SESSION['Tid'])) {
            $acc = 1;
            $group_filter = sprintf(' OR (form_access=2 AND \'{%s}\'&&groups)',$_SESSION['Tgroups']);
        }
        $cmd = "SELECT \"form_id\" FROM project_forms WHERE active=1 AND project_table='".PROJECTTABLE."' AND 'api'=ANY(form_type) AND (form_access<='$acc' $group_filter)";
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $insert_allowed = $row['form_id'];
        } else {
            $insert_allowed = -1;
        }

        $m = array();
        preg_match("/^(\w+)\(([0-9,.]+)\)$/",$_GET['geom'],$m);
        $Geom_Type = $m[1];
        $Geom_Val= $m[2];
        $sheetData = array();
        if ($Geom_Type == 'POINT') {
            $e = explode(",",$Geom_Val);
            $cmd = "SELECT ST_PointFromText('POINT($e[0] $e[1])', {$st_col['SRID_C']}) AS geom";
            $res = pg_query($ID,$cmd);
            $row = pg_fetch_assoc($res);
            $sheetData[] = array('x'=>$e[0],'y'=>$e[1],'geom'=>$row['geom'],'date'=>date('Y-m-d H:i:s'),'quantity'=>1);
            $this->theader = array("geom-x","geom-y","geometry","date","quantity");
            $geom_ref=array();
            if ($show_geom_ref=='' and isset($e[0]) and isset($e[1])) {
                $geom_ref[]=$e[0].','.$e[1];
            }

            if ($show_geom_ref==''){
                $this->show_geom_ref = json_encode($geom_ref);
                //$this->'show_geom_type' = "point";
            }
        }
        $upload_comment = "Egyedi pont felvitel megadott koordináták alapján.";
        return array('comment'=>$upload_comment);
    }
}
/* PROCESSING UPLOADED FILE:
 *
 *
 *
 * */
//unset($_SESSION['sheetDataPage_counter']);
//unset($_SESSION['sheetDataPage']);
class file_process {
    public $error='';
    public $upload_comment=array();
    public $theader = '';
    public $inputFileName;
    public $inputFileType;
    public $show_file_form;
    public $feed_header = array();

    function fprocess($file_type_as,$extra_file_arguments) {
        $inputFileType = '';
        $data_dir = getenv('OB_LIB_DIR')."../uploads/datafiles/";  // Az eredeti adatfájl eltárolása!
        if (!is_writable($data_dir)) {
            log_action("$data_dir is not writable!",__FILE__,__LINE__);
        }

        $Error = file_upload($_FILES['file']);
        if ($Error != '')
            echo "<div class='error-message'>$Error</div>";


        if (!is_array($_FILES["file"]["tmp_name"])) {
            $_FILES["file"]["tmp_name"] = array($_FILES["file"]["tmp_name"]);
            $_FILES['file']['name'] = array($_FILES['file']['name']);
            $_FILES['file']['type'] = array($_FILES['file']['type']);
        }


        $upl_file_count = count($_FILES["file"]["tmp_name"]);

        if ($upl_file_count == 0 or $upl_file_count == "") {
            echo "<div class='error-message'>No files processed.</div>";
            return;
        }

        $sf = ''; // shape file counter

        $fileclass = "unknown";

        $_SESSION['file_ref'] = array();
        for($i=0;$i<$upl_file_count;$i++) {
            if (filesize($_FILES["file"]["tmp_name"][$i])==0) continue;
            // kipakoljuk a fájl tartalmát az uploads/datafile mappába ref# néven.
            // reload miatt kell
            //$save_file = 0;
            //if ($save_file==1) {
            $ref = md5(session_id().microtime());
            $_SESSION['file_ref'][] = $ref;
            file_put_contents(sprintf("%s/%s_%s",$data_dir,$ref,$_FILES['file']['name'][$i]), file_get_contents($_FILES["file"]["tmp_name"][$i], false));
            //}

            /*if (isset($_SESSION['Tid'])) {
                $cmd = "INSERT INTO system.imports (project_table,user_id,ref,datum,header,data) VALUES ('".PROJECTTABLE."','{$_SESSION['Tid']}','$ref',NOW(),'','')";
                $n = PGsqlcmd($ID,$cmd);
                if ($n<1) {
                    $Error = sprintf("%s",pg_last_error($ID));
                } else {
                    $_SESSION['imports'] = $ref;
                }
            }*/

            // more types?
            //
            //
            $finfo = new finfo(FILEINFO_MIME);
            $mimetype = $finfo->file($_FILES["file"]["tmp_name"][$i]);
            #"text/plain; charset=iso-8859-1"

            $file_extension = (false === $pos = strrpos($_FILES["file"]["name"][$i], '.')) ? '' : substr($_FILES["file"]["name"][$i], $pos);
            //mime debug
            //echo $_FILES["file"]["name"][$i].": ".$_FILES["file"]["type"][$i]."<br>";

            //windows7 miatt
            $arr = preg_split('/;/',$mimetype);

            if ( isset($arr[0]) and $arr[0]=='text/plain' and $file_extension=='.csv' ) {
                $fileclass = "txt"; # text/csv
                $inputFileType = 'CSV';
                /* text, csv
                $charset = '';
                if(isset($arr[1])) {
                    $brr = preg_split('/=/',$arr[1]);
                    if (isset($brr[1])) $charset = $brr[1];
                }

                if (strtolower($charset) != 'utf-8' and $charset!='') {
                    //$text = file_get_contents($_FILES["file"]["tmp_name"][$i]);
                    //setlocale(LC_ALL, 'hu_HU.UTF8');
                    //$ttext = iconv($charset, "UTF-8//IGNORE", $text);
                    system("iconv -f $charset -t utf-8 {$_FILES["file"]["tmp_name"][$i]} > {$_FILES["file"]["tmp_name"][$i]}-utf8");
                    rename("{$_FILES["file"]["tmp_name"][$i]}-utf8","{$_FILES["file"]["tmp_name"][$i]}");
                    #ini_set('mbstring.substitute_character', "none");
                    #$ttext= mb_convert_encoding($text, 'UTF-8',$charset);
                    #$ttext = mb_convert_encoding($text, "UTF-8", mb_detect_encoding($text, "UTF-8, ISO-8859-1, ISO-8859-15", true));
                    #file_put_contents($_FILES["file"]["tmp_name"][$i], $ttext);
                }*/

            } elseif(isset($arr[0]) and $arr[0]=='text/plain' and $file_extension=='.json') {
                $fileclass = "txt";
                $inputFileType = "JSON";
            } elseif(isset($arr[0]) and $arr[0]=='text/plain' and $file_extension=='.fasta') {
                $fileclass = "txt";
                $inputFileType = "FASTA";
            } elseif(isset($arr[0]) and $arr[0]=='text/plain' and $file_extension=='.txt') {
                $fileclass = "txt";
                $inputFileType = "TXT";
            } elseif ($_FILES["file"]["type"][$i]=="application/vnd.ms-excel") {
                $fileclass = "spreadsheet";
                $inputFileType = 'Xls'; #'Excel5';
            } elseif ($_FILES["file"]["type"][$i]=="application/vnd.oasis.opendocument.spreadsheet") {
                $fileclass = "spreadsheet";
                $inputFileType = 'Ods'; #'OOCalc';
            } elseif (in_array($_FILES["file"]["type"][$i], ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/wps-office.xlsx"])) {
                $fileclass = "spreadsheet";
                $inputFileType = 'Xlsx'; #'Excel2007';
            } elseif ($_FILES["file"]["type"][$i]=="application/x-sqlite3")  {
                if ($file_extension == '.sqlite') {
                    $fileclass = "sqlite";
                    $inputFileType = 'SpatiaLite';
                }
            } elseif ($_FILES["file"]["type"][$i]=="application/x-esri-shape") {
                if ($file_extension == '.shp') {
                    $fileclass = "shape";
                    $inputFileType = 'EsriShape';
                    $sf=$i;
                }
            } elseif ($_FILES["file"]["type"][$i]=="image/jpeg") {
                $fileclass = "image";
                $inputFileType = "JPEG";
            } else {
                // here we not considering the mime type just use the file extension
                if ($file_extension == '.xls') $fileclass = "spreadsheet"; # application/vnd.ms-excel
                elseif ($file_extension == '.xlsx') $fileclass = "spreadsheet"; # application/vnd.ms-excel
                elseif ($file_extension == '.csv') {
                    $fileclass = "txt"; # text/csv
                    $inputFileType = 'CSV';
                }
                elseif ($file_extension == '.gpx') {
                    $fileclass = "xml"; # application/octet-stream, ....
                    $inputFileType = 'GPX';
                }
                elseif ($file_extension == '.kml') {
                    $fileclass = "xml"; #
                    $inputFileType = 'KML';
                }
                elseif ($file_extension == '.sqlite') {
                    $fileclass = 'sqlite'; # application/octet-stream ....
                    $inputFileType = 'SpatiaLite';
                }
                elseif ($file_extension == '.shp') {
                    $fileclass = "shape"; # application/octet-stream, ....
                    $inputFileType = 'EsriShape';
                    $sf=$i;
                }
                elseif ($file_extension == '.shx') {
                    $fileclass = "shape"; # application/octet-stream, ....
                    $inputFileType = 'EsriShape';
                    $sf=$i;
                }
            }
            $upload_comment[] = str_data_upload_from_file.": '{$_FILES["file"]["name"][$i]}'";
        }
        //overwrite file type from the user choice of the drop-down menu (fasta,csv...)
        if ($file_type_as!='')
            $inputFileType = $file_type_as;

        // processing
        //include only one function!
        include_once("read_uploaded_file.php");
        $ruf = new read_uploaded_file();

        if ($fileclass == 'unknown' and $sf=='') {
            echo "<div class='error-message'>Unhandled file type: ".$_FILES["file"]["type"][$i]."</div>";
            return;
        } elseif($fileclass != 'unknown' and $sf=='') {
            // Normal file read
            //
            // concatenate more files
            $concate_tables = 0;
            if (count($_FILES["file"]["tmp_name"])>1) $concate_tables = 1;

            $inputFileName = array_shift($_FILES["file"]["tmp_name"]);
            array_unshift($_FILES['file']['tmp_name'],$inputFileName);

            $ruf->extra_file_arguments = $extra_file_arguments;
            $ruf_res = $ruf->read_upload_file($fileclass,$inputFileType,$inputFileName);
            if ($ruf_res===true) {
                $this->show_file_form = 1;
                $this->theader = $ruf->theader;
                $this->feed_header = $ruf->feed_header; // excel feed header
                if ($concate_tables) {
                    foreach($_FILES['file']['tmp_name'] as $inputFileName)
                        $ruf->read_upload_file($fileclass,$inputFileType,$inputFileName,1);
                }
            }
            else {
                echo "<div class='error-message'>".$ruf_res."</div>";
                return "File read error";
            }
        } elseif ($sf) {
            # shp file
            $fileclass = "shape";
            $inputFileName = $_FILES["file"]["tmp_name"][$sf];
            $ruf->extra_file_arguments = $extra_file_arguments;
            if ($ruf->read_upload_file($fileclass,$inputFileType,$inputFileName)) {
                $this->show_file_form = 1;
                $this->theader = $ruf->theader;
                if (isset($_SESSION['upload']['hd72-correction']) and $_SESSION['upload']['hd72-correction'] == 1) {
                    $upload_comment[] = "HD72 Correction applied";
                }
            } else {
                return "Shape read error";
            }
            //include("read_uploaded_file.php");
        }
        return array('comment'=>implode("\n",$upload_comment));
    }
}

/************** functions ******************
 * only called in upload.php
 * */

/* restore saved upload
 *
 * */
function clean_upload_temp() {
    global $ID;
    $cmd = sprintf("SELECT EXISTS (
            SELECT 1
            FROM   information_schema.tables
            WHERE  table_schema = 'temporary_tables' AND table_name = 'upload_%s_%s')",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_row($res);
        if ($row[0]=='t') {
            $cmd = sprintf("DELETE FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
            $res = pg_query($ID,$cmd);
            if (pg_affected_rows($res)) {
                return true;
            }
            if (pg_last_error($ID)) {
                log_action(pg_last_error($ID),__FILE__,__LINE__);
                return false;
            }
        } else {
            return true;
        }
    }
    return false;
}
/* restore saved upload
 *
 * */
function restore_upload_temp($name,$ref) {
    global $ID;

    $cmd = sprintf('DROP TABLE IF EXISTS temporary_tables.%1$s',$name);
    $res = pg_query($ID,$cmd);

    $cmd = sprintf('DROP TABLE IF EXISTS temporary_tables.upload_%1$s_%2$s',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    $cmd = sprintf('CREATE TABLE temporary_tables.upload_%2$s_%3$s AS SELECT * FROM temporary_tables."%1$s_%4$s"',$name,PROJECTTABLE,session_id(),$ref);
    $res = pg_query($ID,$cmd);

    if (pg_last_error($ID)) {
        log_action(pg_last_error($ID),__FILE__,__LINE__);
        return false;
    }
}
/* Form access check
 *
 * */
function form_access_check($form_id,$form_type) {
    global $BID;

    if ($form_id=='') return 0;

    if (isset($_GET['massiveedit'])) {
        $cmd = sprintf("SELECT 1 FROM project_forms_data WHERE form_id=%s AND \"column\"='obm_id'",quote($form_id));
        $res = pg_query($BID,$cmd);
        if (!pg_num_rows($res)) return 0;
    }

    // owner, admins can access all forms
    // master can access form types as other type
    if (has_access('master')) return 1;

    $f1 = $f2 = $ft= $fn=$ft_str='';
    $acc = 0; #0:public, 1:logined, 2:closed, 3:inactive
    $group_filter='';
    $types = array('file','web','api');
    if ($form_type!='') {
        $tq=sprintf("AND %s=ANY(form_type)",quote($form_type));
        $t=array_search($form_type,$types);
        $t="'{$types[$t]}'";
    } else {
        $tq="";
        $t="array_to_string(\"form_type\",'|')";
    }

    //access level - we create a filter for it
    if (isset($_SESSION['Tid'])) {
        $acc = 1;
        /* the form is available only for specified groups
         * and the logined user is member of these groups */
        $group_filter = sprintf(' OR (form_access=2 AND \'{%s}\'&&groups)',$_SESSION['Tgroups']);
    }

    // van-e olyan aktív form a projektben aminek input_form_id az azonosítója és (form típusa input_form_type) és a hozzáférés kisebb/egyenlő mint acc(0|1)
    // több ilyen form is lehet!!!
    $cmd = sprintf("SELECT form_id,$t as form_type FROM project_forms WHERE project_table='".PROJECTTABLE."' AND active=1 AND form_id=%s $tq AND (form_access<='%s' $group_filter)",quote($form_id),$acc);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($form_type!='') {
            $ft = explode('|',$row['form_type']);
            if(in_array($form_type,$ft))
                return 1;
            else
                return 0;
        } else {
            return 1;
        }
    }

    return 0;
}
/* Upload form list
 * returning: array of form ids
 * */
function form_choose_list() {
    #var_dump(debug_backtrace());
    global $BID;
    $list = array();

    if (isset($_GET['massiveedit'])) {
        // nullform készítés
        $nullform_id = getNullFormId($_SESSION['current_query_table'],true);
    } 
    $cmd = sprintf('(SELECT form_id, form_name
        FROM project_forms p1 
        WHERE project_table=\'%1$s\' AND published IS NULL AND form_access<3 AND active=1
        ORDER BY form_name)
        UNION
        (
        SELECT t1.form_id,t1.form_name FROM project_forms t1
        JOIN 
        (
           SELECT form_name,MAX(published) AS MAXDATE
           FROM project_forms
           WHERE project_table=\'%1$s\' AND form_access<3 AND active=1
           GROUP BY form_name
        ) t2
        ON t1.form_name = t2.form_name
        AND t1.published = t2.MAXDATE
        WHERE t1.project_table=\'%1$s\')
        ORDER BY form_name',PROJECTTABLE);


    $res = pg_query($BID,$cmd);
    $n = array();
    while ($row = pg_fetch_assoc($res)) {
        //hozzáférés ellenőrzése
        if (form_access_check($row['form_id'],'')===1) {
            if (!isset($_GET['massiveedit'])) {
                // remove edit forms from the list
                $cmd = sprintf("SELECT form_id FROM project_forms_data WHERE form_id=%d AND \"column\"='obm_id'",$row['form_id']);
                $res2 = pg_query($BID,$cmd);
                if (pg_num_rows($res2)) continue;
            }
            $n[] = $row['form_id'];
        }
    }
    return $n;
}
/*
 *
 * */
function form_element($form_id) {
    global $BID;
    $cmd = sprintf("SELECT form_id,form_name,array_to_string(\"form_type\",'|') as ft FROM project_forms WHERE project_table='".PROJECTTABLE."' AND form_id='%d' and active=1",$form_id);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_array($res);
    return $row;
}
/* upload.php
 * default available form list
 * cot: column-assign-template
 * */
function format_form_list($form_id,$form_name,$form_types,$cot) {

    $modules = new modules();

    #$icons = array('fa-file-o','fa-table','fa-caret-square-o-up');
    #$icon = '';
    $list = '';
    $f = array();
    $f[0] = '<span class="fa-stack fa-lg" style="color:#C4C4C4"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x"></i></span>';
    $f[1] = '<span class="fa-stack fa-lg" style="color:#C4C4C4"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x"></i></span>';
    $oa = 0; // only api counter
    foreach(explode('|',$form_types) as $ft) {

        if ($modules->is_enabled('massive_edit') and isset($_GET['massiveedit']) and $ft=='web') {

            $f[1] = '<span style="color:#acacac" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-table"></i></span>';
            continue;
        }

        $massiveedit = isset($_GET['massiveedit']) ? '&massiveedit' : '';

        if ($ft=='file') {
            $f[0] = sprintf('<a href="?form=%d&type=file%s%s" title="%s"><span style="color:#3BA3C4" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-file"></i></span></a>',$form_id,$cot,$massiveedit,str_file_upload);
            $oa++;
        } elseif ($ft=='web') {
            $f[1] = sprintf('<a href="?form=%d&type=web%s" title="%s"><span style="color:#3BA3C4" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-table"></i></span></a>',$form_id,$cot,str_web_form);
            $oa++;
        }

    }
    $translated = $form_name;
    if (defined($form_name)) $translated = constant($form_name);

    if ($oa)
        $list = sprintf('<span>%s</span> <span>%s</span>',implode(' ',$f),$translated);

    return $list;
}

function groupped_form_list($list) {
    global $BID;

    $g = array('ZZZZ'=>array());
    $c_order = array(0);

    foreach ($list as $form_id) {

        $row = form_element($form_id);

        if ($row['ft'] === 'api')
            continue;

        $l = format_form_list($row['form_id'],$row['form_name'],$row['ft'],'');

        $cmd = sprintf("SELECT name,c_order FROM project_forms_groups WHERE project_table='%s' AND $form_id = ANY(form_id)",PROJECTTABLE);
        $res = pg_query($BID,$cmd);
        if(pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            if (!isset($g[$r['name']])) {
                $g[$r['name']] = array();
                $c_order[] = $r['c_order'];
            }
            $g[$r['name']][] = $l;
        } else {
            $g['ZZZZ'][] = $l;
        }

    }
    $g_sort = array();
    $count = count($c_order);
    $g_keys = array_keys($g);
    for($n=0;$n<$count;$n++) {
        #0 2 4 3 1
        $sort = $c_order[$n];
        $g_sort[$sort] = $g_keys[$n];
    }
    ksort($g_sort);
    $gk = array();
    foreach($g_sort as $n) {
        $gk[$n] = $g[$n];
    }
    $g = $gk;
    $list = "<ul id='formMenu'>";
    foreach($g as $k=>$v) {
        $sor = "<li>";
        $label = $k;
        if (preg_match('/^str_/',$label))
            if (defined($label))
                $label = constant($label);

        if ($k!='ZZZZ') $sor .= "<h3><i class='fa fa-caret-right fa-fw'></i> $label</h3><ul class='noJS'>";
        foreach ($v as $ls) {
            if ($k!='ZZZZ')
                $sor .= "<li>$ls</li>";
            else
                $sor .= "<li style='padding-left:10px'>$ls</li>";
        }
        if ($k!='ZZZZ') $sor .= "</ul>";
        $sor .= "</li>";

        $list .= $sor;
    }
    $list .= "</ul>";

    return array($list);
}

/* Get Destionation table for form */

function getFormDestinationTable($form_id) {
    global $BID;

    $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%d  AND project_table='%s' AND active=1",$form_id,PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if ($row=pg_fetch_assoc($res)) {
        return $row['destination_table'];
    }
    return false;
}
/* Creates automatically an observation list table in temporary_tables schema
 * returning with table name
 * */
function obslist_table($origin_table,$tag) {
    global $ID;

    $table = $origin_table.$tag;

    $res = pg_query($ID,'SELECT version()');
    $ife = '';
    if ($row=pg_fetch_assoc($res)) {
        if (preg_match('/PostgreSQL (\d+)\.(\d+) /i',$row['version'],$m)) {
            if ($m[1] == 9 and $m[2]>4)
                $ife = 'IF NOT EXISTS';
            elseif ($m[1] > 9)
                $ife = 'IF NOT EXISTS';
        }
    }

    $cmd = sprintf('SELECT to_regclass(\'temporary_tables.%1$s\')',$table);
    $res = pg_query($ID,$cmd);
    if ($row=pg_fetch_assoc($res)) {
        if ($row['to_regclass'] != 'temporary_tables.'.$table) {
            pg_query($ID,'BEGIN');

            $cmd = sprintf('CREATE TABLE IF NOT EXISTS temporary_tables.%1$s (
                LIKE %2$s
                INCLUDING defaults
                INCLUDING constraints
                INCLUDING indexes);',$table,$origin_table);
            $res = pg_query($ID,$cmd);
            if (pg_last_error($ID)) { 
                pg_query($ID,"ROLLBACK");
                log_action($pg_last_error($ID),__FILE__,__LINE__);
                return false;
            }

            // IF NOT EXISTS implemented from postgres >= 9.5
            $cmd = sprintf('CREATE SEQUENCE %2$s temporary_tables.%1$s_seq
                START WITH 1
                INCREMENT BY 1
                NO MINVALUE
                NO MAXVALUE
                CACHE 1;',$table,$ife);
            $res = pg_query($ID,$cmd);
            if (pg_last_error($ID)) { 
                pg_query($ID,"ROLLBACK");
                log_action($pg_last_error($ID),__FILE__,__LINE__);
                return false;
            }

            $cmd = sprintf('ALTER TABLE temporary_tables.%1$s ALTER COLUMN obm_id SET DEFAULT nextval(\'temporary_tables.%1$s_seq\');',$table);
            //checkitout_regen_es_most_obm_upl_seq
            $res = pg_query($ID,$cmd);
            if (pg_last_error($ID)) { 
                pg_query($ID,"ROLLBACK");
                log_action($pg_last_error($ID),__FILE__,__LINE__);
                return false;
            }
            
            pg_query($ID,"COMMIT");
            return $table;
        } else {
            return $table;
        }
    }
    return false;
}
?>
