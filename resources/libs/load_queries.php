<?php
/*
 *
 *
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

list($id,$ses) = preg_split('/@/',$_GET['id']);
$id = preg_replace("/[^0-9]/","", $id);
$ses = preg_replace("/[^a-zA-Z0-9]/","", $ses);
if (strlen($ses)>30) return;

// ez így rettenetes!!!
$_SESSION['load_loadquery'] = "$id"."@"."$ses";

$cmd = "SELECT query,result,ST_AsText(selection) as q FROM queries WHERE id='$id' and sessionid='$ses'";
$result = pg_query($BID,$cmd);
//pg_close($BID);

if(isset($_SESSION['loaded_xml_related_geom'])) unset($_SESSION['loaded_xml_related_geom']);
if(isset($_SESSION['loaded_xml_content'])) unset($_SESSION['loaded_xml_content']);

if (pg_num_rows($result)) {
    $row=pg_fetch_assoc($result);
    $file = $row['result'];
    // ez nem tudom, hogy kerül majd még feldolgozásra....
    $_SESSION['loaded_xml_related_geom']=$row['q'];
    
    $_SESSION['filter_type'] = array($row['query']);


    /* we don't print the output xml if has refered from the web apps... 
     * it is needed when we would like to see the xml!!!
     * more GET options needed to handel it!
     * */
    if (isset($_GET['getXML'])) {
        //$referer = implode("/",array_slice(preg_split("/\//",$_SERVER['HTTP_REFERER']), 2,-1));
        //if (URL!=$referer) {
            header('Content-type: text/xml');
            //header('Content-Length: '.strlen($file)); 
            //header("Content-Disposition: attachment; filename=\"$id@$ses.xml\";");
            //header("Expires: -1");
            //header("Cache-Control: no-store, no-cache, must-revalidate");
            //header("Cache-Control: post-check=0, pre-check=0", false);
            printf("%s",$file);
        //}
    }
    else {
        //wfs_processing use it
        //$_SESSION['loaded_xml_content'] = $file;
        
        $_SESSION['loaded_geojson_content'] = $file;
    }
}
?>
