<?php
    header('Content-Type: text/javascript', true);
    require(getenv('OB_LIB_DIR').'db_funcs.php');
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS databases.");
    if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database with biomaps.");
    if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        die("Unsuccesful connect to UI database.");
    
    require(getenv('OB_LIB_DIR').'modules_class.php');
    require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    require(getenv('OB_LIB_DIR').'prepare_auth.php');
    require(getenv('OB_LIB_DIR').'prepare_session.php');
    require(getenv('OB_LIB_DIR').'languages.php');
    #if (isset($_SESSION['Tid']))
    #    require_once(getenv('OB_LIB_DIR').'languages-admin.php');
    setExpires(30);

    // list of modules which contains print_js functions - should be automatically loaded 
    $modules = new modules();

    foreach ($modules->which_has_method('print_js', $_SESSION['current_query_table']) as $m) { 
        echo $modules->_include($m,'print_js');
    }
    
?>
