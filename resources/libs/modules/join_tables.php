<?php
class join_tables {
    function return_joins($p) {
        global $BID;
        $c = array();  // join command
        $oc = array(); // joined columns
        $ocvn = array(); // joined columns visible names
        $jointypes = array('left join'=>'LEFT JOIN','right join'=>'RIGHT JOIN','inner join'=>'INNER JOIN','outer join'=>'OUTER JOIN','left'=>'LEFT JOIN','right'=>'RIGHT JOIN','join'=>'JOIN');
        $params = preg_split('/;/',$p);
        if (count($params)) {
            foreach ($params as $pm) {
                $j = preg_split('/:/',$pm);
                #e.g. j = "LEFT JOIN,ektf_botgarden_szarmazas,j0.id=p.szarmazas_id"
                if (array_key_exists(strtolower($j[0]),$jointypes)) {
                    // set join type
                    $JOIN = $jointypes[strtolower($j[0])];

                    //only own project tables allowed!!!
                    //check join table is valid in the project
                    //what about the genearal tables like weather???
                    $cmd = sprintf('SELECT 1 FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote($j[1]));
                    $result = pg_query($BID,$cmd);
                    if (pg_num_rows($result)) {
                        //joined table's columns
                        //if allowed, otherwise use the additional_columns module
                        if (isset($j[3]) and ($j[3]!=FALSE and $j[3]!=0 and $j[3]!='no')) {
                            $ofc = dbcolist('columns',$j[1]);
                            $ocvn[$j[1]] = dbcolist('array',$j[1]);
                            // species -> additional.species
                            array_walk($ofc, 'prefix', $j[1]);
                            $oc = array_merge($oc,$ofc);
                        }
                        $m = array();
                        //join condition split
                        //multiple conditions allowed - separated by ;
                        $conditions = preg_split('/&/',$j[2]);
                        $mcon = array();
                        foreach ($conditions as $con) {
                            if (preg_match("/$j[1]\.([a-z0-9_]+) ?([=<>]) ?([a-z0-9_]+)(\.)?([a-z0-9_]+)?/i",$con,$m)) {
                                // join string
                                if ($m[3] == 'p') {
                                    $con = preg_replace('/=(p)\./',"=".PROJECTTABLE.".",$con);
                                }
                                // Több feltétel JOINNál
                                //$mcon[] = sprintf("%s.%s %s %s%s%s",$j[1],$m[1],$m[2],$m[3]);
                                $mcon[] = $con;
                            }
                        }
                        $c[] = sprintf(" %s %s ON (%s) ",$JOIN,$j[1],implode(' AND ',$mcon));
                    }
                }
            }
        }
        # array(join cmd, prefixed columns)
        return(array(implode(' ',$c),$oc,$ocvn));
  
    }
}
?>
