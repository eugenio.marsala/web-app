<?php
class results_summary {
    function print_table($params) {
        global $ID;

        if (!has_access('master') and $_SESSION['st_col']['USE_RULES']) {
            if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
                $tgroups = 0;
            else
                $tgroups = $_SESSION['Tgroups'];

            // exclude sensitive data if rules allowed
            $cmd = sprintf('SELECT COUNT(DISTINCT obm_id) AS c FROM temporary_tables.temp_query_%1$s_%2$s
                    LEFT JOIN %1$s_rules r ON ("data_table"=\'%4$s\' AND obm_id=r.row_id) 
                    WHERE 
                    (sensitivity::varchar IN (\'1\',\'2\',\'3\',\'no-geom\',\'restricted\',\'only-own\') AND ARRAY[%3$s] && read) OR sensitivity::varchar IN (\'0\',\'public\') OR sensitivity IS NULL',PROJECTTABLE,session_id(),$tgroups,$_SESSION['current_query_table']);
            $f = t(str_public_accessible_records);
            //$cmd = sprintf('SELECT COUNT(DISTINCT obm_id) as c FROM temporary_tables.temp_query_%s_%s',PROJECTTABLE,session_id());

        } else {
            $f = t(str_recordsfound);
            //$cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_query_%s_%s',PROJECTTABLE,session_id());
            $cmd = sprintf('SELECT COUNT(DISTINCT obm_id) as c FROM temporary_tables.temp_query_%s_%s',PROJECTTABLE,session_id());
        }

        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        return sprintf('%s: %d',$f,$row['c']);
    }
}
?>
