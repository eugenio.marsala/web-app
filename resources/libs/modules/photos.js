    var photo_cell_id = '';
    var selected_photo_name = "";
    var selected_photo_id = "";

    $(document).ready(function() {

        $("#photo_upload_div").draggable({
            cursor: "move",
            zIndex: 10000,
        });

        $("body").on('change','#photo_upload-file',function(){
            $(this).removeClass('button-secondary');
            //$('#photo_upload').addClass('button-secondary');

            $.post("ajax",{'check_file_size':this.files[0].size},function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['data']);
                } else if (retval['status']=='success') {
                    $('#photo_upload').trigger('click');
                }
            });

        });
        /* Click on upload file
         * pseudo function, triggered in photos module
         * */
        $("body").on('click','#photo_upload',function(e) {
            e.stopPropagation();
            e.preventDefault();
            var id=$(this).attr('id');
            var files = document.getElementById(id+"-file").files;
            var u = new Uploader;
            u.button('photo_upload');
            u.complete('UplResponse');
            u.param('pict');
            u.after('$("#photo_anchor").addClass("button-secondary")');
            u.file_upload(files);
        });
        /* anchor filename 
         * write the uploaded file name as reference into the attachment data cell 
         * */
        $("#photo_upload_div").on('click','#photo_anchor',function() {
            if(photo_cell_id=='') return false;
            //js v1.7
            var spl=photo_cell_id.split('-');
            var target=spl[0];
            var pos=spl[1]; 
            var v;
            if(target=='default') {
                v = $("#default-"+pos).val().split(',');
            } else if (target=='in') {
                //interface - datasheet
                v = $("#dc-"+pos).val().split(',');
            } else if(target=='header') {
                //autofil
                //js v1.7
                //var [rowIndex,colIndex] = pos.split(',');
                var spl=pos.split(',');
                var rowIndex=spl[0];
                var colIndex=spl[1]; 
                v = $("#upload-data-table > .thead > .tr:eq("+rowIndex+") > .th:nth-child("+colIndex+")").find('input').val().split(',');
            }
            else {
                //normal
                //js v1.7
                //var [rowIndex,colIndex] = pos.split(',');
                var spl=pos.split(',');
                var rowIndex=spl[0];
                var colIndex=spl[1]; 
                v = $("#upload-data-table > .tbody > .tr:eq("+rowIndex+") > .td:nth-child("+colIndex+")").find('input').val().split(',');
                
                var this_autoskip = $("#upload-data-table > .tbody > .tr:eq("+rowIndex+")").find('.autoskip');
                if (this_autoskip.val()==1) {
                    this_autoskip.val(0);
                    this_autoskip.removeClass('autoskip');
                }
            }

            // names of attachments
            var list = $("#uresponse").find('img').each(function(){
                var alt = $(this).attr('alt');
                v.push(alt);

                var coords = '';
                if ($(this).attr('data-coords')) {
                    coords = $(this).data('coords');
                    if ($(this).data('satellites')!='') 
                        accuracy = str_accuracy+": "+$(this).data('satellites');
                    else
                        accuracy = '';
                }
                var spl=pos.split(',');
                var rowIndex=spl[0];
                if (coords != '') {

                    var geom_col_id = get_geometry_column_id();
                    var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );

                    $( "#dialog-confirm" ).dialog("option", "title", "GPS Coordinates")
                    $( "#dialog-confirm" ).dialog({"buttons":[
                        {
                            text: str_yes_i_want,
                            icons: { primary: "ui-icon-flag" },
                            click: function() {
                                $("#upload-data-table > .tbody > .tr:eq("+rowIndex+")").find('.td:eq('+geom_col_id+')').find('input').val(coords);
                                $( this ).dialog( "close" );
                            }
                        },
                        {
                            text: str_no_thanks,
                            icons: { primary: "ui-icon-cancel" },
                            click: function() {
                                $( this ).dialog( "close" );
                            }
                        }   
                    ], "height":200});
                    $( "#dialog-confirm" ).html( "<div style='font-size:150%'>"+str_set_coordinates_from+" ("+coords+")?<br>"+accuracy+"</div>" );
                }
            });
            v.sort();
            v = $.unique( v );
            v = v.filter(Boolean)
            
            if(target=='default')
                v = $("#default-"+pos).val(v.toString());
            else if(target=='header') {
                //autofil
                //var [rowIndex,colIndex] = pos.split(',');
                var spl=pos.split(',');
                var rowIndex=spl[0];
                var colIndex=spl[1]; 
                $("#upload-data-table > .thead > .tr:eq("+rowIndex+") > .th:nth-child("+colIndex+")").find('input').val(v.toString());
                var input = $("#upload-data-table > .thead > .tr:eq("+rowIndex+") > .th:nth-child("+colIndex+")").find('input');
                fillfunction(input,'fill');
            } else if (target=='in') {
                v = $("#dc-"+pos).val(v.toString());
            } else {
                //normal
                //var [rowIndex,colIndex] = pos.split(',');
                var spl=pos.split(',');
                var rowIndex=spl[0];
                var colIndex=spl[1]; 
                $("#upload-data-table > .tbody > .tr:eq("+rowIndex+") > .td:nth-child("+colIndex+")").find('input').val(v.toString());
            }

            //$("#photo_anchor").removeClass("button-secondary");
            //$("#photo_anchor").addClass("button-passive");
            $("#photo_upload_div").hide();
            // should be cleaned!!
            $("#uresponse").html('');
        });
        /* Delete photo from a connection
         * */
        $("#photodiv").on('click','.p-delete',function() {
            var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );

            var selected_photo_name = $("#photodiv-frame-img").data('name');
            var conid = $("#photodiv-frame-img").data('conid');

            $( "#dialog-confirm" ).dialog( "option", "buttons",   [        {
                text: "Unlink",
                icons: {
                    primary: "ui-icon-closethick"
                },
                click: function() {
                    $.post("ajax",{'photomanage':selected_photo_name,'conid':conid,'option':'delete'},function(data){
                        $("#photodiv").hide();
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            alert('Ok!');
                            if (selected_photo_id!='') {
                                $("#"+selected_photo_id).remove();
                            }
                        }
                    });
                    $( this ).dialog( "close" );
                }
            }]);
            $( "#dialog-confirm" ).text( "Unlink "+selected_photo_name+" file from data?" );
        });
        /* Save comment attached to a file
         * */
        $("#photodiv").on('click','.p-save',function() {
            var selected_photo_name = $("#photodiv-frame-img").data('name');
            var conid = $("#photodiv-frame-img").data('conid');
            var data_id = $("#photodiv-frame-img").data('id');
            var data_table = '';
            $.post("ajax",
                {'conid':conid,'photomanage':selected_photo_name,'data-id':data_id,'comment':$("#photodiv-comment").val(),'option':'save'},
                function(data){
                    var retval = jsendp(data);
                    if (retval['status']=='error') {
                        alert(retval['message']);
                    } else if (retval['status']=='fail') {
                        alert(retval['data']);
                    } else if (retval['status']=='success') {
                        alert('Ok!');
                        $(".p-save").removeClass('button-warning');
                        $(".p-save").addClass('button-gray');
                    }
            });

        });
        /* file connection update
         * */
        $("#tab_basic").on('click','.save_connection',function() {
            var id = $(this).attr('id');
            var widgetId=id.substring(id.indexOf('_')+1,id.length);
            //var oriconnlink = $(this).closest('li').find('.conn_link').attr('id');
            //oriconnlink = oriconnlink.substring(oriconnlink.indexOf('_')+1,oriconnlink.length);
            
            //var origids = $(this).closest('li').find('.data_link').attr('id');
            //origids = origids.substring(origids.indexOf('_')+1,origids.length);

            //var connlink = $(this).closest('li').find('.conn_link').val();
            var datalink = $(this).closest('li').find('.data_link').val();
            //$.post("ajax",{'photomanage':widgetId,'option':'updateconnection','oriconnlink':oriconnlink,'connlink':connlink,'oridatalink':origids,'datalink':datalink},function(data){
            $.post("ajax",{'photomanage':widgetId,'option':'updateconnection','datalink':datalink},function(data) {
                //$('#tab_basic').load(projecturl+'includes/project_admin.php?options=files');
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['data']);
                } else if (retval['status']=='success') {
                    alert('Ok!');
                }
            });
        });
        // admin functions 
        $("#tab_basic").on('click','.update_table',function() {
            var id = $(this).attr('id');
            var widgetId=id.substring(id.indexOf('_')+1,id.length);
            var tablelink = $(this).closest('li').find('.table_link').val();
            $.post("ajax",{'photomanage':widgetId,'option':'updatetable','tablelink':tablelink},function(data) {
                //$('#tab_basic').load(projecturl+'includes/project_admin.php?options=files');
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['data']);
                } else if (retval['status']=='success') {
                    alert('Ok!');
                }
            });
        });
        /*
         * */
        $("#tab_basic").on('click','.add_file',function() {
            var filename = $(this).attr("data-attr"); 
            var tablelink = $(this).closest('li').find('.table_link').val();

            $.post("ajax",{'photomanage':filename,'option':'addtable','tablelink':tablelink},function(data){
                //$('#tab_basic').load(projecturl+'includes/project_admin.php?options=files');
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['data']);
                } else if (retval['status']=='success') {
                    alert('Ok!');
                }
            });
        });
// both: editor and viewer
$("#photodiv").on('click','.p-exif',function() {
    $("#photodiv-exif").slideToggle(800);
    return false;
});
$("#photodiv").on('click','.p-expand',function() {
    var img = $("#photodiv-frame > img");
    img.attr("src", projecturl+'getphoto?ref='+selected_photo_name+'&getfullimage');
    $("#photodiv").css({'width':'100%'});
    $("#photodiv-carpet").css({'width':'100%'});
    $(this).find('i').removeClass('fa-arrows-alt');
    $(this).find('i').addClass('fa-compress');
    $(this).removeClass('p-expand');
    $(this).addClass('p-compress');
    $("#photodiv-frame").css({'overflow':'auto'});
    $("#photodiv-frame").css({'height':($(window).height()-15 + 'px')});
    $("#photodiv-comment").css({'position':'fixed'});
});
$("#photodiv").on('click','.p-compress',function() {
    $("#photodiv").css({'width':'50%'});
    $("#photodiv-carpet").css({'width':'50%'});
    $(this).find('i').removeClass('fa-compress');
    $(this).find('i').addClass('fa-arrows-alt');
    $(this).removeClass('p-compress');
    $(this).addClass('p-expand');
    $("#photodiv-comment").css({'position':'absolute'});
});
$("#photodiv").on('click','.p-close',function() {
    $("#photodiv-frame > img").remove();
    $("#photodiv").toggle();
    $("#photodiv").css({'width':'50%'});
    $("#photodiv-carpet").css({'width':'50%'});
    return false;
});
$("#photodiv").on('click','.p-download',function() {
    var w = window.open('getphoto?ref='+selected_photo_name+'&getfullimage','_blank','menubar=0,scrollbars=0,status=0,titlebar=0,toolbar=0,width=100,height=100');
    w.document.close();
});
$("#pud-close").click(function(e){
    $("#photo_upload_div").hide();
    $("#uresponse").html('');
});

$("body").on('change keyup paste','#photodiv-comment',function() {
    $(".p-save").removeClass('button-gray');
    $(".p-save").addClass('button-warning');
});
/* Photo things ----------------------------------------------------------> 
 * Viewer
 * */
$("body").on('click','.photolink',function(e) {
    e.stopPropagation();
    e.preventDefault();

    // Crearing the traces of the previously displayed image
    $("#photodiv-frame-img").remove();
    $("#photodiv-exif").html('');
    $("#photodiv-thumbnails").html('');
    $("#photodiv-comment").val('');
    

    var val = $(this).attr('href');
    var data_id = $(this).attr('id');
    selected_photo_id = $(this).attr('id');
    
    if (val.includes('ref')) {
        //http://localhost/biomaps/projects/dinpi/getphoto?ref=/DSCN9382.JPG"
        //what is "c" ?
        var params = val.match(/c=(.+)&ref=\/?(.+)$/);
        if (params == null) {
            params = new Array("","","");
            params[2] = val.match(/ref=\/?(.+)$/)[1];
        }
        // global variable 
        var connId = params[1];
        selected_photo_name = params[2];
        createPhotoDiv(selected_photo_name, connId,data_id);
    }
    else if (val.includes('connection')) {
        // stable photo-link
        var conn = val.match(/connection=\/?(.+)$/);

        var params = {connectionId:conn[1]};

        $.post('getphoto', params)
            .done(function(data, textStatus, jqXHR) {
                selected_photo_name = data.reference;
                createPhotoDiv(data.reference,conn[1],data_id);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown.toString());
            });
    }
});
/* upload table - upload photo icon
 * photos module !!!
 * */
$("body").on('click','.photo-here',function(){
    var target = $(this).data('target');
    if (typeof id != "undefined") {
        target = $(this).attr('target');
    }
    var v = '';
    var pos;
    if (target == 'data') {
        ri = $(this).closest('.tr').index();
        pos = ri+','+(1+(+$(this).closest('.td').index()));
    } else if (target=='header') {
        ri = $(this).closest('.tr').index();
        pos = ri+','+(1+(+$(this).closest('.th').index()));
    } else if (target=='in') {
        var id=$(this).attr('id');
        pos = id.substring(id.indexOf('-')+1,id.length);
    } else {
        //default - kitett mezők és önálló inputok
        var id=$(this).attr('id');
        if (id.match(/dc-(.+)/)) {
            target = 'in';
        } else { 
            target = 'default';
        }
        pos = id.substring(id.indexOf('-')+1,id.length);
    }
    photo_cell_id = target+"-"+pos;

    console.log(photo_cell_id);
    
    //reread current uploads to show them
    if (target == 'data') {
        v = $(this).closest('.td').find('input').val();
    } else if (target=='header') {
        v = $(this).closest('.th').find('input').val();
    } else if (target=='default') {
        v = $(this).val();
    } else if (target=='in') {
        v = $(this).val();
    }
    //else if (target=='header') {
    //    var v = $(this).closest('li').find('input').val();
    //}
    if (v != '') {
        var m = v.split(',');
        $("#uresponse").html('');
        for(k = 0; k < m.length; k++) {
            $.post("getphoto", {ref:m[k],getthumbnailimage:1,url:1}, function(imgurl){
                /*attachment processing response
                {
                    "url": [[
                            "http:\/\/dinpi.openbiomaps.org\/projects\/dinpi\/getphoto?ref=",
                            "thumbnails",
                            "EGLAF.jpg",
                            "5"
                        ]]
                    }
                */
                var j = jsendp(imgurl);
                if (j['status']=='error') {
                    console.log(j['message']);
                    return;
                } else if (j['status']=='fail') {
                    console.log($j['message']);
                    return;
                } else if (j['status']=='success') { 
                    j = j['data'];
                }

                var fp = j['file_properties'];
                for (f = 0; f < fp.length; f++) {
                    var upf = fp[f];
                    var img = document.createElement('img');
                    //set icon or thumbnail
                    img.src = 'http://openbiomaps.org/Images/32px_none.png';
                    $.ajax({
                        url: 'ajax',
                        data: {
                            'get_mime': 1,
                            'thumb': 'yes',
                            'file': upf['filename'],
                            'size': 32
                        },
                        type: 'POST'
                    }).done(function(r){
                          img.src = r;
                    }).fail(function(){
                          console.log('ajax mime get failed');
                    });
                    img.setAttribute('class', 'thumb');
                    // img alt is the unique name of the attached file given by the attachment upload process
                    img.setAttribute('alt', upf['filename']);
                    $("#uresponse").append("<a href='"+upf['url']+'/'+upf['filename']+"' class='photolink' id='gf_"+upf['id']+"'></a>");
                    $("#uresponse").find('a').last().html(img);
                    $("#uresponse").append(' ');
                    $("#uresponse").find('a').last().draggable({
                        revert: "invalid",
                        containment: "document",
                        helper: "clone",
                        cursor: "move"
                    });
                }
            });
        }
    }
    //$("#photo_upload").removeClass("button-secondary")
    $('#photo_anchor').removeClass('button-secondary');
    $('#photo_upload-file').addClass('button-secondary');
    $("#photo_upload-file").val('');

    $("#photo_upload_div").show();
});


    });
    
    /* photo div function
     *
     * */

function get_geometry_column_id() {
    var geom_col_id = '';
    $(".oListElem").each(function(){
        var colvalue=$(this).find('li').eq(0).text();
        if (colvalue!='') {
            var colIndex = $(this).closest('.th').index();
            if ($(this).find('li').eq(0).attr('id')==geom_col) {
                geom_col_id = colIndex;
            }
        }
    });
    //console.log(geom_col_id);
    return geom_col_id;
}

    function createPhotoDiv (selected_photo_name, connId, data_id) {

        var img = document.createElement('img');
        img.src = 'http://openbiomaps.org/Images/32px_none.png';
        $.ajax({
            url: 'ajax',
            data: {
                'get_mime': 1,
                'thumb': 'no',
                'file': selected_photo_name,
                'size': 512
            },
            type: 'POST'
        }).done(function(r){
            img.src = r;
        }).fail(function(){
            console.log('ajax mime get failed');
        });

        $("#photodiv-frame").append(img);
        img.setAttribute('id','photodiv-frame-img');
        $("#photodiv-frame-img").attr('data-name',selected_photo_name);
        $("#photodiv-frame-img").attr('data-conid',connId);
        $("#photodiv-frame-img").attr('data-id',data_id);

        $(".p-save").removeClass('button-warning');
        $(".p-save").addClass('button-gray');

        $("body").css('cursor','progress');
        $.post("getphoto", {description:selected_photo_name,getthumbnailimage:1,url:1,'connId': connId }, function(data){
            var j = {url:'',comment:'',exif:[],error:''};
            try {
                j = JSON.parse(data);
            }
            catch(e) { 
                $( "#dialog" ).text("Invalid JSON received. It might be caused by a corrupted image.");
                var isOpen = $( "#dialog" ).dialog( "isOpen" );
                if(!isOpen) $( "#dialog" ).dialog( "open" );
            }

            var connected_photos = j['file_properties'];
            $('#photodiv-comment').val(j['comment']);
            $("#photodiv-exif").empty();

            for (var key in j['exif']) {
                if (!j['exif'].hasOwnProperty(key)) continue;

                var value = j['exif'][key];
                if(value !== null && typeof value === 'object') {
                    //?
                    $("#photodiv-exif").append("<li><span style='font-weight:bold'>"+key+"</span></li>");
                    $("#photodiv-exif").append("<ul id='photo-exif-"+key+"'>");
                    for (var key_key in value) {
                        // Nested exif not works properly!!!
                        //console.log(key_key);
                        //console.log(value);
                        //console.log(typeof value[key_key]);
                        //if (typeof value[key_key]!=='undefined') {
                            var key_value = value[key_key];
                        //} else
                        //    var key_value = value.join();
                        $("#photodiv-exif-"+key).append("<li><span style='font-weight:bold'>"+key_key+":</span> "+key_value+"</li>");
                    }
                    $("#photodiv-exif").append("</ul>");
                } else {
                    //
                    $("#photodiv-exif").append("<li><span style='font-weight:bold'>"+key+":</span> "+value+"</li>");
                }
            }
            for (i = 0; i < connected_photos.length; i++) {
                var img = document.createElement('img');
                var upf = connected_photos[i];
                img.src = upf['url']+'/'+upf['type']+'/'+upf['filename'];
                img.setAttribute('class', 'thumb');
                img.setAttribute('alt', upf['filename']);
                $("#photodiv-thumbnails").append("<a href='"+upf['url']+upf['filename']+"' class='photolink' id='gf_"+upf['id']+"'></a>");
                $("#photodiv-thumbnails").find('a').last().html(img);
                $("#photodiv-thumbnails").append('<br>');
            }

            $("body").css('cursor','initial');
        });

        $("#photodiv").show("slow");
    }

