<?php
class identify_point {
    /* params:
     * point:
     * ce:
     * */
    function return_data ($params,$point,$ce) {
        global $ID,$BID;

        $ACC_LEVEL = ACC_LEVEL;

        $modules = new modules();

        //$geom_col = $_SESSION['st_col']['GEOM_C'];
        //$geom_col = $_SESSION['current_query_table'].'.'.$_SESSION['st_col']['GEOM_C'];

        /*$module_name = array_values(array_intersect(array('snap_to_grid','transform_geometries','transform_geometry'), array_column($modules, 'module_name')));
        $mki = (count($module_name)) ? array_search($module_name[0], array_column($modules, 'module_name')) : false;
        if ($mki!==false) {
            require_once(getenv('OB_LIB_DIR').'default_modules.php');
            $dmodule = new defModules();
            $dm_geom_col = $dmodule->snap_to_grid('geom_column',$modules[$mki]['p'],NULL);

            // EZT A MODULT NEM KÉNE ITT HASZNÁLNI!!!!!!!
            if ($geom_col!=$dm_geom_col)
                return;
        }*/

        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        $sqla = sql_aliases(1);
        $qtable_alias = $sqla['qtable_alias'];
        $FROM = $sqla['from'];
        $GEOM_C = $sqla['geom_col'];

        $ce = (array) json_decode($ce);

        if ($params == '') {
            // comma separated list of queried columns
            $columns = 'obm_id';
            $parameter_columns = array('obm_id');
        } else {
            $parameter_columns = preg_split('/;/',$params);
            // column array

            //a subset of the main cols, depending on user's access rights
            $allowed_cols = ($modules->is_enabled('allowed_columns'))
                ? $modules->_include('allowed_columns','return_columns',array($ce),true)
                : $parameter_columns;

            // comma separated list of queried columns
            $columns = implode(',',preg_filter('/^/', $qtable_alias.".", $parameter_columns));
        }

        $cmd = sprintf("SELECT St_Distance(St_GeomFromText('POINT(%f %f)'),St_GeomFromText('POINT(%f %f)'))",$ce['left'],$ce['bottom'],$ce['right'],$ce['top']);
        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $distance = $row['st_distance']/100;
        $rule = '';
        $rule_join = '';
        $grid_join = '';
        $GRST = has_access('master');

        if ($modules->is_enabled('grid_view')) {
            if (isset($_SESSION['display_grid_for_map'])) {
                $grid_geom_col = $_SESSION['display_grid_for_map'];
                $GEOM_C = 'qgrids.'.$grid_geom_col.'';
            } else {
                $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                $GEOM_C = 'qgrids.'.$grid_geom_col.'';
            }

            /* JOIN with grid table if module enabled */
            $grid_join = sprintf('LEFT JOIN %s_qgrids as qgrids ON ("%s".obm_id=qgrids.row_id)',$_SESSION['current_query_table'],$qtable_alias);
        }

        if ( !$GRST and $_SESSION['st_col']['USE_RULES']) {

            $rule = " AND ( (sensitivity::varchar IN ('3','only-owner') AND ARRAY[$tgroups] && read) ";
            $rule .= " OR (sensitivity::varchar IN ('1','no-geom') AND ARRAY[$tgroups] && read) ";
            $rule .= " OR sensitivity::varchar IN ('0','2','public','restricted') ";
            $rule .= " OR sensitivity IS NULL ) ";

            $rule_join = sprintf('LEFT JOIN %1$s_rules r ON (r.data_table=\'%2$s\' AND %2$s.obm_id=r.row_id)',PROJECTTABLE,$qtable_alias);
        }
        $subfilter = "";
        $res4 = pg_query($ID,sprintf("SELECT EXISTS (
           SELECT 1
           FROM   information_schema.tables
           WHERE  table_schema = 'temporary_tables'
           AND    table_name = 'temp_%s_%s');",PROJECTTABLE,session_id()));
        $row = pg_fetch_assoc($res4);
        if ($row['exists']=='t') {
            $res5 = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_%1$s_%2$s',PROJECTTABLE,session_id()));
            if (pg_num_rows($res5))
                $subfilter = sprintf('RIGHT JOIN temporary_tables.temp_%1$s_%2$s t ON (%3$s.obm_id=t.obm_id)',PROJECTTABLE,session_id(),$qtable_alias);
        }

        //$cmd = sprintf('SELECT %9$s.obm_id,CONCAT_WS(\'<br>\',%3$s) as short_content,%10$s count(*) OVER() AS full_count %1$s %7$s %5$s
        $cmd = sprintf('SELECT *, count(*) OVER() AS full_count %1$s %7$s %10$s %5$s
            WHERE ST_DWithin(%8$s, st_transform(st_geomfromtext(%2$s,900913),4326), %4$s) %6$s
            ORDER BY ST_Distance(%8$s,st_transform(st_geomfromtext(%2$s,900913),4326)) LIMIT 30',$FROM,$point,$columns,$distance,$rule_join,$rule,$subfilter,$GEOM_C,$qtable_alias,$grid_join);

        $res = pg_query($ID,$cmd);
        $n = pg_num_rows($res);
        $e = '';
        $fc = '';
        while ($row2 = pg_fetch_assoc($res)) {

            $cols = $parameter_columns;
            if (!$GRST and isset($row2['sensitivity']) and ($row2['sensitivity']=='2' or $row2['sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row2['read'])))) {
                    $cols = array_values(array_intersect($parameter_columns,$allowed_cols));
                }
            }

            $connid = 0;
            $values = [];
            foreach ($cols as $column) {
                if ($column === 'obm_files_id' && $row2[$column] !== '') {
                    $connid = $row2[$column];
                    continue;
                }
                if (preg_match("/^JSON:(.*)$/", $column, $o)) {
                    $j = json_decode(base64_decode($o[1]));
                    $err = "<div class='text-error'>%s</div>";
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        $values[] = sprintf($err,json_last_error_msg());
                        continue;
                    }
                    if (!isset($j->type)) {
                        $values[] = sprintf($err,"incomplete json: <a href='http://openbiomaps.org/documents/hu/modules.html' target='_blank'>read the documentation</a>");
                        continue;
                    }

                    if ($j->type === 'link') {
                        if (!isset($j->href) || !isset($j->label)) {
                            $values[] = sprintf($err,"incomplete json: <a href='http://openbiomaps.org/documents/hu/modules.html' target='_blank'>read the documentation</a>");
                            continue;
                        }
                        $label = (defined($j->label)) ? constant($j->label) : $j->label;
                        $params = (isset($j->params)) ? $j->params : [];
                        $class = (isset($j->class)) ? $j->class : '';
                        $target = (isset($j->target)) ? $j->target : '_blank';
                        $id = (isset($j->id)) ? $j->id : '';

                        for ($i = 0; $i < count($params); $i++) {
                            $j->href = preg_replace("/%" . ($i+1) . "%/",$row2[$params[$i]],$j->href);
                        }
                        $values[] = sprintf("<a href='%s' target='%s' id='%s' class='%s'>%s</a>",$j->href,$target,$id,$class,$label);
                    }


                }
                else
                    $values[] = $row2[$column];
            }

            if (defined('SHINYURL') and constant("SHINYURL"))
                $eurl = sprintf('http://'.URL.'/data/%d/',$row2['obm_id']);
            else
                $eurl = sprintf('http://'.URL.'/?data&id=%d',$row2['obm_id']);

            $title_value = array_shift($values);
            $short_content = '<br>'.implode('<br>',$values);
            $photolink = ($connid) ? "<a href='getphoto?connection=$connid' class='photolink'><i class='fa fa-camera'></i></a>" : "";
            $e .= "<div class='identify_point_record'><a href='$eurl' target='_blank'><span style='font-size:125%'>$title_value</span></a>$photolink $short_content</div>";
            $fc = $row2['full_count'];
        }
        return json_encode(array("records"=>$n,"all_records"=>$fc,"content"=>$e,"str_listed"=>str_listed,"str_sumall"=>str_sumall));

    }

    function print_button ($params) {
        # no params processing...
        $out = "<span>".mb_convert_case(str_identify, MB_CASE_UPPER, "UTF-8").":&nbsp;</span>";
        $out .= "<button class='pure-button button-passive button-small' id='identify_point'><i class='fa fa-lg fa-info'></i></button>";
        return "<div class='mcbox'> $out </div>";
    }
}
?>
