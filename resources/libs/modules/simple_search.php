<?php
/**
 * Simple search module
 *
 * 
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 * @version 1.0
 */

class simple_search extends module {
    var $error = '';
    var $retval;

    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        $this->params = explode(';',$params);

        if ($action)
            $this->retval = $this->$action($params,$pa);

    }

    public function getMenuItem() {
        return ['label' => 'Simple search', 'url' => 'simple_search' ];
    }

    public function profileItem() {
        
        $em = sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='#tab_basic' data-url='includes/project_admin.php?options=validation' class='profilelink'>".'str_validation_interface'."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink("user_interface.html#download-requests",str_what_is_validation));

        return [
            'label' => str_validation_interface,
            'fa' => 'fa-tasks',
            'item' => $em
        ];
    }


    public function adminPage($params) {
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        ob_start();

?>
SIMPLE SEARCH admin page
<?php
        $out = ob_get_clean();

        return $out;
    }
     
        
    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    public function ajax($params, $pa) {

        global $ID;

        $request = $pa[0];
        $term = preg_replace('/\s+/','',$request['term']);

        print $this->trgm_search($term);
        return;

    }

    public function print_js($request) {

        $js = (file_exists(getenv('OB_LIB_DIR').'modules/simple_search.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/simple_search.js') : '';
        return $js;

    }

    public function destroy($params, $pa) {
        debug('simple_search destroy() running',__FILE__,__LINE__);
    }

    public function init($params) {
        debug('simple_search init running', __FILE__,__LINE__);
        return;
    }

    public function print_box() {

        global $ID;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        ob_start();
        $t = new table_row_template();
        $simple_search_title = (defined('str_simple_search_title')) ? constant('str_simple_search_title') : 'Simple search';
        $t->cell(mb_convert_case($simple_search_title, MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        $res = pg_query($ID, sprintf('SELECT DISTINCT subject FROM %1$s_terms;',PROJECTTABLE));
        $subjects = pg_fetch_all($res);

        $def_hidden = '';

        foreach ($subjects as $sub) {
            $t->cell("<div id='obm_search_{$sub['subject']}' class='qf divInput'><button id='{$sub['subject']}_relation' style='float:right' class='search-relation pure-button pure-button-primary hidden'>OR</button></div>",2,'selections');
            $t->row("text-options_obm_search $def_hidden");
        }

        $t->row("text-options_obm_search $def_hidden");
        $t->cell("<input style='width:100%;font-size:150%' type='text' id='search' class='multi-autocomplete' value='' data-ids='' autocomplete='off'>",2,'content');
        $t->row("text-options_obm_search $def_hidden");

        $t->cell("<button class='show-advanced-filters'>" . ((defined('str_show_advanced_filters')) ? constant('str_show_advanced_filters') : 'Show advanced filters') . "</button>", 2, 'content');
        $t->row("text-options_obm_search $def_hidden");

?>
        <link rel="stylesheet" href="<?= $protocol; ?>://<?= URL ?>/includes/modules/simple_search.css" type="text/css" />
        <table class="mapfb simple-search"> <?= $t->printOut(); ?> </table> 
<?php
        $html = ob_get_clean();
        return $html;
    }

    public function wrap_filters($params, $pa) {
        return "<div class='advanced-filters'>{$pa[0]}</div>";
    }

    private function trgm_search($string, $limit = 20, $dist = 0.9, $reorderLevel = 4) {
        global $ID;

        # pg_trgm search with limit
        $cmd = sprintf("WITH t AS (SELECT lower(unaccent(%s)) as str) SELECT term_id, term, lower(unaccent(term)) <-> t.str as dist, subject FROM %s_terms, t WHERE lower(unaccent(term)) LIKE '%%' || t.str || '%%' ORDER BY dist LIMIT 20;",quote($string), PROJECTTABLE);

        $result = pg_query($ID,$cmd);
        if (!pg_num_rows($result)) {
            return json_encode(array());
            exit;
        }

        $j = array();

        $all_rows = pg_fetch_all($result);

        /*
        if ($reorderLevel >= 1) {
            # ADVANCED REORDERING
            # Stage 1:
            #   szó eleji pontos egyezések kiszedése az eredmény listából
            $matches = preg_grep("/^$string/i", array_column($all_rows,'name'));
            $s = array();
            foreach($matches as $m) {
                $k = array_search($m, array_column($all_rows,'name'));
                $s[] = array_splice($all_rows,$k,1);
            }
            $begin_match = array();
            foreach($s as $se) {
                $begin_match[] = $se[0];
            }
            array_multisort(array_column($begin_match,'name'), SORT_ASC, $begin_match);
            $all_rows_reordered = $begin_match;

        }
        if ($reorderLevel >= 2) {
            # Stage 2:
            #   szó közbeni egyezések kiszedése az eredmény listából
            $matches = preg_grep("/$string/i", array_column($all_rows,'name'));
            $s = array();
            foreach($matches as $m) {
                $k = array_search($m, array_column($all_rows,'name'));
                $s[] = array_splice($all_rows,$k,1);
            }
            $middle_match = array();
            foreach($s as $se) {
                $middle_match[] = $se[0];
            }
            array_multisort(array_column($middle_match,'name'), SORT_ASC, $middle_match);

            # create all_rows_array again  
            $all_rows_reordered = array_merge($begin_match,$middle_match,$all_rows);

        }
        if ($reorderLevel >= 3) {
            # Stage 3:
            #   Search for maximum frequency, pick up this element, put at the first position
            $frequency = array_column($all_rows_reordered,'frequency');
            $max = max($frequency);
            if ($max > 0) {
                $mf_idx = array_search($max,$frequency);
                $spliced_element = array_splice($all_rows_reordered,$mf_idx,1);
                // put at the first position of the list
                array_unshift($all_rows_reordered,$spliced_element[0]);
            }

            //$k = array_search($string, array_column($row, 'name'));
            //$frow = array_filter($row, function($el) use ($string) {
            //    return ( strpos($el['text'], $string) !== false );
            //});

        }
         */
        for ($i = 0, $l = count($all_rows);  $i < $l; $i++) {
            $all_rows[$i]['name'] = (substr($all_rows[$i]['term'],0,4) == 'str_' && defined($all_rows[$i]['term'])) ? constant($all_rows[$i]['term']) : $all_rows[$i]['term'];
            $all_rows[$i]['idx'] = $i;
        }

        array_multisort(array_column($all_rows,'subject'), SORT_DESC, array_column($all_rows,'idx'), SORT_ASC, $all_rows);


        # renaming assoc array according to the needs of autocomplete js
        foreach ($all_rows as $row) {
            $x = array();
            $x['meta'] = $row['term']; 
            $x['label']= $row['name'];
            $x['value']= $row['term'];
            $x['category'] = $row['subject'];
            $x['id']   = $row["term_id"];
            $j[] = $x;
        }

        return json_encode($j);
    }
}
?>
