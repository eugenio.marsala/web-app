<?php
/* parameters:
 *
 * */
class results_asJSON {
    function print_data($params,$filename='',$data) {
        global $ID,$BID;

        $ACC_LEVEL = ACC_LEVEL;
        $modules = new modules();
        $data_rows = array();
        // default encoding
        
        $track_id = array();

        $main_cols = dbcolist('columns',PROJECTTABLE);
        $main_cols[] = 'obm_id';
        $allowed_cols = $main_cols;
        $allowed_cols = array_merge($allowed_cols,array('obm_id','uploader_name','uploading_date','upid'));
        $allowed_gcols = $allowed_cols;
        $allowed_general_columns = array();

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            if (!isset($_SESSION['load_loadquery']))
                $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
                $allowed_gcols = $modules->_include('allowed_columns','return_gcolumns',array($main_cols),true);
                $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array($main_cols),true);
                $allowed_cols = array_merge($allowed_cols,array('obm_id','uploader_name','uploading_date','upid'));
                $allowed_gcols = array_merge($allowed_gcols,array('obm_id','uploader_name','uploading_date','upid'));
                $allowed_general_columns = array_merge($allowed_general_columns,array('obm_id','uploader_name','uploading_date','upid'));
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                // drop all column for non logined users if access level is higher
                $allowed_cols = array('obm_id','uploader_name','uploading_date','upid');
                $allowed_gcols = $allowed_cols;
            }
        }

        $general_restriction = 0;
        if (!has_access('master') and count($allowed_general_columns)) {
            $general_restriction = 1;
        }

        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY obm_id',PROJECTTABLE,session_id()));

        while($line = pg_fetch_assoc($res)) {
            //add meta columns
            $cell = array();
            // ski0pping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($line)==1) {
                if ($line[0]==$chk) continue;
                else $chk = $line[0];
            }
            
            $geometry_restriction = 0;
            $column_restriction = 0;
            if (!has_access('master') and isset($line['obm_sensitivity']) and ($line['obm_sensitivity']=='1' or $line['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$line['obm_read']))))
                    $geometry_restriction = 1;
            }
            elseif (!has_access('master') and isset($line['obm_sensitivity']) and ($line['obm_sensitivity']=='2' or $line['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$line['obm_read']))))
                    $column_restriction = 1;
            }

            // print cells
            foreach($data->csv_header as $k=>$v) {

                if ($column_restriction) {
                    if ( !in_array($k,$allowed_cols) ) {
                        $line[$k] = 'NA';
                    }
                }
                elseif ($geometry_restriction ) {
                    if ( $k == 'obm_geometry')
                        $line[$k] = 'NA';
                    if ( !in_array($k,$allowed_gcols) ) {
                        $line[$k] = 'NA';
                    }
                }
                //non sensitivity based restrictions 
                if ($general_restriction) {
                    if ( !in_array($k,$allowed_general_columns) ) {
                        $line[$k] = 'NA';
                    }
                }

                if (isset($line[$k]))
                    $cl = $line[$k];
                else
                    $cl = "";

                $cell[$k] = $cl;
            }
            // ez a tömb  túl nagyra nőhet - ki lehet kerülni folyamatos printteléssel
            $data_rows[] = $cell;
            // ez a tömb is túl nagyra nőhet!!
            $track_id[] = $line['obm_id'];
        }
        track_download($track_id);

        // direct print 
        if ($filename=='') {
            header('Content-type:application/json;charset=utf-8');
            print json_encode($data_rows);
        } else {
            // file print
            $filesize = mb_strlen(json_encode($data_rows), '8bit');

            # header definíció!!!
            header("Content-Type: application/json");
            header("Content-Length: $filesize"); 
            header("Content-Disposition: attachment; filename=\"$filename\";");
            header("Expires: -1");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            print json_encode($data_rows);
        }

        return;
    }
}
?>
