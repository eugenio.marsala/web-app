<?php
/**
 * List manager module
 *
 * 
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 * @version 1.0
 */

class list_manager extends module {
    var $error = '';
    var $retval;

    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        $this->params = explode(';',$params);

        if ($action)
            $this->retval = $this->$action($params,$pa);

    }

    public function getMenuItem() {
        return ['label' => 'List manager', 'url' => 'list_manager' ];
    }

    public function adminPage($params) {
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $out = '<button class="module_submenu button-success button-xlarge pure-button" data-url="includes/project_admin.php?options=list_manager"> <i class="fa fa-refresh"></i> </button>';


        return $out;
    }
    public function db_cols_header() {
        return 'Manage lists';
    }

    public function manage_list_button($params, $pa) {
        return "<button class='pure-button open-list-manager' data-table='{$pa['table']}' data-colname='{$pa['colname']}'><i class='fa fa-list' aria-hidden='true'></i></button>";
    }
        
    public function modal() {
        ob_start();
?>
        <div id='list-manager' class='modal'>
            <div class='modal-draggable-handle' style=''>List manager<i id='' class='fa fa-close fa-2x close-list-manager' style='cursor:pointer;position:absolute;top:0px;right:0px;color:white'></i></div>
            <div class='modal-body'> <textarea id="list-manager-textarea"> </textarea> </div> 
            <div class='modal-footer'> 
                <button class="pure-button button-success" id="save-list"> <i class="fa fa-floppy-o"> </i> <?= str_save ?> </button>
                <button class="pure-button button-warning" id="generate-list"> <i class="fa fa-gears"> </i> Generate list from data </button>
            </div> 
        </div>
<?php
        return ob_get_clean();
    }

    public function upload_form_builder_widget() {
        ob_start();
?>
        <form id="list-manager-widget"> 
            <div class="lm-buttons">
                <div> <input type='radio' name='lm-list-elements' value='all' class='lm-list-elements'> All defined elements </div> 
                <div> <input type='radio' name='lm-list-elements' value='select' class='lm-list-elements'> Select list elements </div> 
                <div> <input type='radio' name='lm-list-elements' value='standard-editor' class='lm-list-elements'> Use the standard editor </div> 
            </div>  
            <div id="lm-list"> </div> 
        </form>
<?php
        return ob_get_clean();
    }

    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    public function ajax($params, $pa) {

        $request = $pa[0];

        if (!isset($request['action'])) {
            echo 'ajax action is missing';
            exit;
        }

        switch ($request['action']) {
        case 'get_list':
            if (!isset($request['colname'])) {
                echo 'Column name not defined';
                exit;
            }

            if (!isset($request['table'])) {
                echo 'Table not defined';
                exit;
            }

            $list = new li5t($request['colname'], $request['table']);

            echo json_encode($list->get_list('term'));
            exit;

        case 'save_list':
            $l = explode("\n",$request['data']);
            $list = new li5t();
            $list->set_table($request['table']);
            $list->set_subject($request['colname']);
            $list->set_list($l);

            $list->save_list();

            echo 'saved';
            exit;

        case 'generate_list':
            
            if (!isset($request['colname'])) {
                echo 'Column name not defined';
                exit;
            }

            if (!isset($request['table'])) {
                echo 'Table not defined';
                exit;
            }

            $list = new li5t($request['colname'], $request['table'],true);

            echo json_encode($list->get_list('term'));
            exit;

        case 'build_upload_form_list':
            debug('asdf',__FILE__,__LINE__);
            exit;
        }

    }

    public function print_js($request) {

        return (file_exists(getenv('OB_LIB_DIR').'modules/list_manager.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/list_manager.js') : '';

    }

    public function print_css($request) {

        return (file_exists(getenv('OB_LIB_DIR').'modules/list_manager.css')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/list_manager.css') : '';

    }

    public function destroy($params, $pa) {
        global $ID;
        debug('list_manager destroy() running',__FILE__,__LINE__);
	return;
    }

    public function init($params, $pa) {
        debug('list_manager init running', __FILE__,__LINE__);

        global $ID;

        //checking terms table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_terms');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE SEQUENCE %1$s_term_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;',PROJECTTABLE);
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_terms" ( term_id integer DEFAULT nextval(\'%1$s_term_id_seq\') NOT NULL, data_table character varying(64) NOT NULL, subject character varying(128) NOT NULL, term character varying NOT NULL, description character varying, status character varying(128), taxon_db integer DEFAULT 0, CONSTRAINT %1$s_data_table_subject_term_key UNIQUE ("data_table","subject","term")) WITH (oids = false); ', PROJECTTABLE );

            if (!empty($cmd1) && !self::query($ID,$cmd1)) {
                $this->error = 'list_manager init failed: table creation error';
                return;
            }

            $cmd = sprintf('CREATE INDEX CONCURRENTLY index_%1$s_terms_on_term_trigram ON %1$s_terms USING gin (term gin_trgm_ops);', PROJECTTABLE);
            if (!$res = pg_query($ID, $cmd)) {
                $this->error = 'index creation error';
            }

        }
        //checking list_manager_results table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_terms_connect');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE SEQUENCE %1$s_terms_connect_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;',PROJECTTABLE);
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_terms_connect" ( tcid integer DEFAULT nextval(\'%1$s_terms_connect_id_seq\') NOT NULL, data_table character varying(64) NOT NULL, row_id integer NOT NULL, subject character varying(128) NOT NULL, term_id integer[] NOT NULL, CONSTRAINT %1$s_data_table_row_id_subject_key UNIQUE ("data_table","row_id","subject")) WITH (oids = false); ', PROJECTTABLE );
        }
        if (!empty($cmd1) && !self::query($ID,$cmd1)) {
            $this->error = 'list_manager init failed: table creation error';
            return;
        }
        log_action('list_manager init succeded',__FILE__,__LINE__);
        return;
    }

}

class li5t {
    var $list = [];
    var $subject = '';
    var $table = '';

    public function __construct($subject = null, $table = null, $generate = false ) {
        global $ID;

        if (is_null($subject)) 
            return;

        $this->subject = $subject;
        $this->table = (is_null($table)) 
            ? defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE
            : $table;

        if ($generate) 
            $this->generate_list();
        else
            $this->request_list();
    }

    private function request_list() {
        global $ID;

        $cmd = sprintf("SELECT * FROM %s_terms WHERE data_table = %s AND subject = %s;", PROJECTTABLE, quote($this->table), quote($this->subject));

        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return;
        }
        while ($row = pg_fetch_assoc($res)) {
            $this->list[] = new term($row);
        }

        return;
    }

    private function generate_list() {
        global $ID;

        $cmd = sprintf("SELECT DISTINCT %s FROM %s;", $this->subject,  $this->table);

        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return;
        }
        $results = pg_fetch_all($res);
        $this->set_list(array_column($results, $this->subject));

        return;

    }

    public function set_subject($subject) {
        if ($subject) 
            $this->subject = $subject;
    }
    public function set_table($table) {
        if ($table)
            $this->table = $table;
    }

    public function set_list($list) {
        $this->list = [];
        if (isset($this->table) && isset($this->subject)) {
            foreach ($list as $l) {
                if (is_null($l)) continue;
                $this->list[] = new term([
                    'data_table' => $this->table,
                    'subject' => $this->subject,
                    'term' => $l,
                ]);
            }
        }
    }

    public function save_list() {
        if (!$this->delete_list()) 
            return;

        foreach ($this->list as $l) {
            $l->save();
        }
    }

    public function delete_list() {
        global $ID;

        $cmd = sprintf('DELETE FROM %s_terms WHERE data_table = %s AND subject = %s;', PROJECTTABLE, quote($this->table), quote($this->subject));
        if (! $res = pg_query($ID, $cmd)) {
            log_action('delete list error');
            return false;
        }
        return true;
    }


    public function sortBy($by) {
    }

    public function count() {
        return count($this->list);
    }

    public function get_list($prop = null) {
        if (is_null($prop))
            return $this->list;
        elseif (!in_array($prop,['term','term_id']))
            return null;
        $ret = [];
        foreach ($this->list as $l) {
            $ret[] = $l->{"get_$prop"}();
        }
        return $ret;

    }

}

class term {
    var $props = ['data_table','subject','term','term_id','taxon_db','status'];

    public function __construct($t) {
        foreach ($this->props as $p) {
            if (isset($t[$p])) 
                $this->$p = $t[$p];
        }
    }

    public function get_term() {
        return $this->term;
    }
    
    public function get_taxon_db() {
        return $this->taxon_db;
    }
    
    public function get_term_id() {
        return $this->term_id;
    }

    public function delete() {
        global $ID;

        //TODO
    }

    public function save() {
        global $ID;

        if (isset($this->term_id)) 
            $cmd = sprintf('UPDATE %s_terms SET data_table = %s, subject = %s, term = %s WHERE term_id = %s;', PROJECTTABLE, quote($this->data_table), quote($this->subject), quote($this->term), quote($this->term_id));
        else
            $cmd = sprintf('INSERT INTO %s_terms (data_table, subject, term, status) VALUES (%s, %s, %s, 0);', PROJECTTABLE, quote($this->data_table), quote($this->subject), quote($this->term));

        if ($this->term === '') {
            return false;
        }

        if (! $res = pg_query($ID, $cmd)) {
            log_action(pg_last_error($ID),__FILE__,__LINE__);
            return false;
        }

        return true;
    }
}

?>
