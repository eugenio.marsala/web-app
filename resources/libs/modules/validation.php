<?php
/**
 * Validation API module
 *
 * 
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 * @version 1.0
 */

class validation extends module {
    var $error = '';
    var $retval;
    var $project_email;
    var $vms = [];
    var $validation_dir;
    var $mtable;

    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        $res = pg_query($BID,"SELECT email FROM projects WHERE project_table='".PROJECTTABLE."'");
        $row = pg_fetch_assoc($res);
        $this->project_email = $row['email'];

        $this->params = explode(';',$params);
        $this->validation_dir = getenv('OB_LIB_DIR') . 'modules/validation_modules/';

        if (isset($pa['mtable'])) {
            $this->mtable = $pa['mtable'];
        }
    
        $this->include_vms();

        if ($action)
            $this->retval = $this->$action($params,$pa);

    }

    public function getMenuItem() {
        return ['label' => 'str_validation_interface', 'url' => 'validation' ];
    }

    public function adminPage($params) {
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        ob_start();

?>
        <link rel="stylesheet" href="<?= $protocol; ?>://<?= URL ?>/includes/modules/validation.css" type="text/css" />
        <div id="validation_admin_page">
        <button class="module_submenu button-success button-xlarge pure-button" data-url="includes/project_admin.php?options=validation&amp;mtable=<?= $this->mtable ?>">
                <i class="fa fa-refresh"></i>
            </button>
        <div id="message-div" class="message"></div>
        <h3>Job management</h3>
<?php 
        foreach( $this->vms as $vm) { 
?>
        <form action="" method="post" id="<?= $vm->get_name(); ?>-form" class="pure-form">
            <input type="hidden" name="mtable" value="<?= $this->mtable ?>" >
            <div class="pure-g">
                <div class="pure-u-1-12"> <?= $vm->get_name(); ?> </div>
                <div class="pure-u-1-12"> <?= $vm->get_enabled_chkbox(); ?> </div>
                <div class="pure-u-1-3"> <?= $vm->get_times(); ?> </div>
                <div class="pure-u-1-3"> <?= $vm->get_params(); ?> </div>
                <div class="pure-u-1-6"> 
                    <button class="pure-button button-secondary save-job" name="submit" data-vm="<?= $vm->get_name(); ?>" type="submit"><i class="fa fa-floppy-o"> </i> Save</button> 
                </div>
            </div> 
        </form>
        <?php } ?>
        <h3> Results </h3>
        <?php foreach ( $this->vms as $vm ) {
        $results = $vm->get_results();
        if ($results) {

?>
            <h4> <?= $vm->get_name() ?> </h4>
            <?= $results ?>
        <?php }
        }?>
        </div>
<?php
        $out = ob_get_clean();

        return $out;
    }

    private function getMailText($const,$lang) { 
        global $BID;
        $cmd = sprintf("SELECT translation FROM translations WHERE project = '%s' AND lang = %s AND const = %s LIMIT 1", PROJECTTABLE, quote($lang), quote($const));
        $text = pg_fetch_assoc($this->query($BID, $cmd));
        return $text['translation'];
    }
        
    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    public function ajax($params, $pa) {

        global $ID;

        $request = $pa[0];

        if (isset($request['vm']) && in_array($request['vm'],array_keys($this->vms))) {
            if ( isset($request['action']) && $request['action'] == 'save-job') {
                $this->vms[$request['vm']]->saveJob($request);
            }

        }

    }

    public function print_js($request) {

        $js = (file_exists(getenv('OB_LIB_DIR').'modules/validation.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/validation.js') : '';
        return $js;

    }

    static function run() {

        debug('validation run running',__FILE__,__LINE__);

    }
    public function destroy($params, $pa) {
        debug('validation destroy() running',__FILE__,__LINE__);
        foreach ($this->vms as $vm) {
            //validation module initialization
            if ( method_exists($vm,'destroy') ) {
                if (!$vm->destroy($params, $pa)) {
                    $this->error = 'validation modul destroy() failed';
                    return;
                }
            }
        }
        debug('validation destroy() finished',__FILE__,__LINE__);
    }

    public function init($params,$pa) {
        debug('validation init running', __FILE__,__LINE__);

        global $ID;

        //checking validation_jobs table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_validation_jobs');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE SEQUENCE %1$s_validation_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;',PROJECTTABLE);
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_validation_jobs" ( "id" integer DEFAULT nextval(\'%1$s_validation_jobs_id_seq\') NOT NULL, data_table character varying NOT NULL, job character varying NOT NULL, parameters json NOT NULL, enabled smallint NOT NULL DEFAULT 0, created_at timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, CONSTRAINT "%1$s_validation_id" PRIMARY KEY ("id")); ', PROJECTTABLE );
        }
        if (!empty($cmd1) && !self::query($ID,$cmd1)) {
            $this->error = 'validation init failed: table creation error';
            return;
        }

        //checking validation_results table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_validation_results');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_validation_results" ( "row_id" integer NOT NULL, data_table character varying NOT NULL, test_name character varying NOT NULL, result character varying NOT NULL, PRIMARY KEY ("row_id", "data_table", "test_name", "result")); ', PROJECTTABLE );
        }
        if (!empty($cmd1) && !self::query($ID,$cmd1)) {
            $this->error = 'validation init failed: table creation error';
            return;
        }
        //checking validation_modules table
        if ( ! file_exists( $this->validation_dir ) ) {
            mkdir($this->validation_dir);
        }


        foreach ($this->vms as $vm) {
            //validation module initialization
            if ( method_exists($vm,'init') ) {
                if (!$vm->init()) {
                    $this->error = 'validation modul init failed';
                    return;
                }
            }
        }
        log_action('validation init succeded',__FILE__,__LINE__);
        return;
    }

    private function include_vms() {
        foreach ($this->params as $p) {
            if ($p !== '') {
                //checking validation module files
                $validation_file = $this->validation_dir . $p . '.php';
                if ( ! file_exists( $validation_file) ) {
                    $template_file = getenv('OB_LIB_DIR') . 'modules/validation_modules/vm_template.php';
                    if (!file_exists($template_file)) {
                        $this->error = "validation init failed: vm_template.php file missing";
                        return false;
                    }
                    $content = file_get_contents($template_file);
                    $content = preg_replace('/%%vm%%/', $p, $content);
                    if ( ! file_put_contents($validation_file, $content) ) {
                        $this->error = "validation init failed: vm_template.php copy failed";
                        return false;
                    }

                }


                require_once($validation_file);

                //checking the structure of the validation module
                if ( ! class_exists($p) ) {
                    $this->error = "validation init failed: $p class missing from $p.php";
                    return;
                }
                $this->vms[$p] = new $p($this->mtable);
            }
        }
    }

}

class validation_module extends module {
    var $p;
    var $mtable;
    var $params;
    var $job_file;

    public function __construct($p,$mtable) {
        $this->p = $p;
        $this->mtable = $mtable;
        $this->request_module_params();
        $this->job_file = getenv('PROJECT_DIR').'/jobs/'. $this->p . '.php';
    }
    
    public function get_name() {
        return $this->p;
    }

    public function get_times() {
        $h = new input_field([
            'type' => 'text', 
            'value' => (isset($this->params->time->hour)) ? $this->params->time->hour : '',
            'name' => 'hour',
            'pattern' => '[0-9*]{1,2}',
        ]);
        $m = new input_field([
            'type' => 'text', 
            'value' => (isset($this->params->time->minute)) ? $this->params->time->minute : '',
            'name' => 'minute',
            'pattern' => '[0-9*]{1,2}',
        ]);
        $d = new input_field([
            'type' => 'text', 
            'value' => (isset($this->params->time->day)) ? $this->params->time->day : '', 
            'name' => 'day',
            'pattern' => '[0-9*]{1,2}',
        ]);
        $out = "<div>" . $m->getElement() . $h->getElement() . $d->getElement() . "</div>";
        return $out;
    }

    public function get_enabled_chkbox() {
        $chk = new input_field([
            'type' => 'checkbox',
            'name' => 'enabled',
            'checked' => ($this->enabled) ? 'checked' : '',
        ]);
        return $chk->getElement();

    }

    public function get_params() {

        $parameters = (isset($this->params->parameters)) ? $this->params->parameters : '';

        $ta = new input_field([
            'element' => 'textarea',
            'value' => ($parameters) ? json_encode($parameters,JSON_PRETTY_PRINT) : '',
            'placeholder' => ($parameters) ? '' : 'parameters not set',
            'name' => 'parameters',
        ]);

        return $ta->getElement();
    }

    private function createJobFile() {
        if (!file_exists($this->job_file)) {
            $template_file = getenv('OB_LIB_DIR') . 'modules/validation_modules/job_template.php';
            if (!file_exists($template_file)) {
                return false;
            }
            $content = file_get_contents($template_file);
            $content = preg_replace('/%%vm%%/', $this->p, $content);
            file_put_contents($this->job_file, $content);
        }
        return true;
    }
    public function destroy($params,$pa) {
        global $ID;
        if (!isset($pa['mtable'])) {
            $this->error = 'mtable missing';
            return false;
        }
        
        $cmd = sprintf('UPDATE %1$s_validation_jobs SET enabled=0 WHERE data_table = %2$s;', PROJECTTABLE, quote($pa['mtable']));
        $res = pg_query($ID, $cmd);
        if (pg_affected_rows($res) == 0) {
            debug('query_failed',__FILE__,__LINE__);
            return false;
        }

        return $this->deleteJobFile();
    }

    private function deleteJobFile() {
        $jobs = read_jobs();
        unset($jobs[$this->p . '.php']);
        $write_result = write_jobs($jobs);

        if (!file_exists($this->job_file)) {
            debug('delete job file failed',__FILE__,__LINE__);
            return false;
        }
        return unlink($this->job_file);
    }
        
    public function get_results() {
        return '';
    }

    public function saveJob($request) {

        global $ID;

        parse_str($request['data'],$data);
        $vm_parameters = json_decode($data['parameters']);
        $mtable = $data['mtable'];
        $enabled = (isset($data['enabled'])) ? 1 : 0;

        $times = [
            'hour'=>$data['hour'],
            'minute' => $data['minute'],
            'day' => $data['day'],
        ];

        foreach ($times as $time) {

            if (!preg_match('/^(\d{1,2}|\*)$/',$time,$outpu)) {
                echo common_message('error',"Invalid time: $time");
                return;
            }
        }
        
        $jobs = read_jobs();
        if (isset($jobs['error'])) {
            echo common_message('error',$jobs['error']);
            return;
        }

        if ($enabled)  {
            if (!$this->createJobFile()) {
                echo common_message('error','Job file creation error');
                return;
            }
        }
        else {

            if (!$this->deleteJobFile()) {
                echo common_message('error','Job file deletion error');
                return;
            }
            
        }
        

        $parameters = json_encode([
            'time' => $times,
            'parameters' => $vm_parameters
        ]);

        $cmd = [
            sprintf('UPDATE %1$s_validation_jobs SET parameters=%2$s, enabled=%5$s WHERE data_table = %3$s AND job = %4$s;', PROJECTTABLE, quote($parameters), quote($mtable), quote($request['vm']), quote($enabled)),
                sprintf('INSERT INTO %1$s_validation_jobs (data_table, job, parameters, enabled) SELECT %3$s, %4$s, %2$s, %5$s WHERE NOT EXISTS (SELECT 1 FROM %1$s_validation_jobs WHERE data_table = %3$s AND job = %4$s);', PROJECTTABLE, quote($parameters), quote($mtable), quote($request['vm']), quote($enabled)),
            ];

        if (!$res = parent::query($ID, $cmd)) {
            echo common_message('error','Module parameters update error');
            return;
        }

        $aff_rows = 0;
        foreach ( $res as $r ) 
            $aff_rows += pg_affected_rows($r);
        
        if ($aff_rows == 0) {
            echo common_message('success','Module parameters not updated');
            return;
        }

        if ($enabled) 
            $jobs[$this->p . '.php'] = [$data['minute'],$data['hour'],$data['day']];


        $write_result = write_jobs($jobs);
        if (isset($write_result['error'])) {
            echo common_message('error',$write_result['error']);
            return;
        }
        if ( $write_result === false ) {
            echo common_message('error', 'File write error!');
            return;
        }

        echo common_message('success','Module parameters updated');
        return;

    }

    private function request_module_params() {
        global $ID;

        $cmd = sprintf('SELECT parameters, enabled FROM %1$s_validation_jobs WHERE data_table = %2$s AND job = %3$s;', PROJECTTABLE, quote($this->mtable), quote($this->p));
        $res = pg_query($ID, $cmd);
        $result = pg_fetch_assoc($res);

        $this->params = json_decode($result['parameters']);
        $this->enabled = $result['enabled'];
    }

    static function getJobParams($job) {
        global $ID;

        $table = PROJECTTABLE . '_validation_jobs';
        $res = pg_query_params($ID, "SELECT parameters FROM $table WHERE data_table = $1 AND job = $2;", [PROJECTTABLE, $job]);
        if (!$res) {
            job_log('Query failed');
            exit();
        }
        if ($results = pg_fetch_row($res)) {
            $results = json_decode($results[0]);
            return $results->parameters;
        }
        else { 
            return [];
        }
    }
}



?>
