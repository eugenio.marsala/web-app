<?php
/**
 * Move project module
 *
 *
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 */

class move_project extends module {
    var $error = '';
    var $retval;
    var $tmp_dir = '/tmp/project_import_tmp/';
    var $data = [];
    var $biomaps = [
        'projects' => ['project_table', 'BID'],
        'project_descriptions' => ['projecttable', 'BID'],
        'project_users' => ['project_table', 'BID'],
        'project_roles' => ['project_table', 'BID'],
        'group_privileges' => ['project', 'BID'],
        'header_names' => ['f_table_name', 'BID'],
        'invites' => ['project_table', 'BID'],
        'modules' => ['project_table', 'BID'],
        'project_forms' => ['project_table', 'BID'],
        'project_forms_groups' => ['project_table', 'BID'],
        'project_interconnects' => ['project_table', 'BID'],
        'project_layers' => ['project_table', 'BID'],
        'custom_reports' => ['project', 'BID'],
        'handshakes' => ['project_table', 'BID'],
        'project_layers' => ['project_table', 'BID'],
        'project_mapserver_layers' => ['project_table', 'BID'],
        'project_metaname' => ['project', 'BID'],
        'project_news_stream' => ['project_table', 'BID'],
        'project_queries' => ['project_table', 'BID'],
        'project_variables' => ['project_table', 'BID'],
        'queries' => ['comment', 'BID'],
        'settings_import' => ['project_table', 'BID'],
        'supervisor' => ['project', 'BID'],
        'track_citations' => ['project_table', 'BID'],
        'track_data_views' => ['project_table', 'BID'],
        'track_downloads' => ['project_table', 'BID'],
        'translations' => ['project', 'BID'],
    ];
    var $private_files = [
        'includes/private',
        'includes/modules/private',
        'js/private',
        'css/private',
        'jobs',
        'private',
    ];

    var $pg_dump;
    var $psql;

    var $gisdata = [];

    var $shared = [];
    var $system = [
      'uploadings' => ['project', 'ID'],
      'shared_polygons' => ['project_table', 'ID'],
      'query_buff' => ['table', 'ID'],
      'files' => ['project_table', 'ID'],
      'evaluations' => ['table', 'ID'],
      ];

    function __construct($action = null, $params = null,$pa = array()) {
        global $ID;
        $this->pg_dump = defined('PG_DUMP') ? constant('PG_DUMP') : 'pg_dump';
        $this->psql = defined('PSQL') ? constant('PSQL') : 'psql';
        $this->request_shared_tables();

        if ($action)
            $this->retval = $this->$action($params,$pa);

    }

    public function getMenuItem() {
        return ['label' => 'move_project', 'url' => 'move_project' ];
    }


    public function adminPage($params) {
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        ob_start();
?>
        <?php if (!has_access('master')) : ?>

            <div id="message-div" class="message message-error">You don't have access to this module!</div>

        <?php else : ?>

        <div>
            <input type='checkbox' name='include-visitors' id='include-visitors'>
            <label for='include-visitors'> Include visitors </label>
        </div>

        <p> Include tables from shared schema </p>
        <?php foreach ($this->shared as $table): ?>

        <div>
            <input type='checkbox' name='include-shared' value=<?= $table ?> id='include-<?= $table ?>'>
            <label for='include-<?= $table ?>'> <?= $table ?> </label>
        </div>

        <?php endforeach; ?>

        <div>
            <label for='new-project-name'> Rename project </label>
            <input type="text" name="new-project-name" id="new-project-name">
        </div>
            <button class="button-success button-xlarge pure-button" id='export_project' data-url="">
                    <i class="fa fa-refresh"></i> Export project
                </button>
            <div id="message-div" class="message"></div>

        <?php endif; ?>

<?php
        $out = ob_get_clean();

        return $out;
    }

    private function request_shared_tables() {
      global $ID;

      $cmd = sprintf("SELECT table_name FROM information_schema.tables WHERE table_schema = 'shared';");
      if (! $res = pg_query($ID, $cmd)) {
        log_action(pg_last_error($res));
        return false;
      }
      $results = pg_fetch_all($res);
      if ($results)
        $this->shared = array_column($results, 'table_name');
    }

    public function upload_project_file_field() {
        $str = defined("str_upload_your_project_file") ? str_upload_your_project_file : "Upload your project file";
        $out = "<tr><td>". $str ."</td><td><input type='file' name='project-file'></td></tr>";
        return $out;
    }

    private function extract_project_file() {
        if (!isset($_FILES['project-file'])) {
            return false;
        }

        mkdir($this->tmp_dir);
        move_uploaded_file($_FILES['project-file']['tmp_name'], $this->tmp_dir.'project_import.tar.gz');
        exec("tar -xzf {$this->tmp_dir}project_import.tar.gz -C {$this->tmp_dir}");


        return true;
    }

    public function delete_tmp_dir() {
        exec("rm -rf {$this->tmp_dir}");
        return;
    }

    public function read_biomaps_file($params, $pa) {
        if (!$this->extract_project_file()) {
            return ['status'=>'error', 'message' => 'project file not uploaded'];
        }

        $pf = new project_file();

        if (!$pf) {
            return [ 'status' => 'error', 'message' => 'json decode error'];
        }

        return ['status' => 'success','data' => $pf];
    }

    public function ajax($params, $pa) {

        global $BID, $ID;

        $request = $pa[0];

        if (!has_access('master')) {
            echo "You don't have access to this!";
            return;
        }

        if (isset($request['action']) && $request['action'] == 'export_project') {
            $this->data = [];

            if ($request['include_visitors'] === 'true')
                $this->biomaps['project_visitors'] = 'project_table';
            //
            array_walk($this->biomaps, [$this, 'append_data']);
            array_walk($this->system, [$this, 'append_data']);

            //special cases, when not the project name is the foreign key:
            $table = 'users';
            $cmd = sprintf("SELECT row_to_json(foo) FROM (SELECT u.* FROM project_users pu LEFT JOIN users u ON u.id = pu.user_id where pu.project_table = '%s') as foo;", PROJECTTABLE);
            $this->data[$table] = $this->get_table_data($BID, $cmd);

            $table = 'project_forms_data';
            $cmd = sprintf("SELECT row_to_json(foo) FROM (SELECT pfd.* FROM project_forms_data pfd LEFT JOIN project_forms pf ON pfd.form_id = pf.form_id WHERE pf.project_table = '%s') as foo;", PROJECTTABLE);
            $this->data[$table] = $this->get_table_data($BID, $cmd);

            $table = 'polygon_users';
            $cmd = sprintf("SELECT row_to_json(foo) FROM (SELECT poly.* FROM system.polygon_users poly LEFT JOIN shared_polygons sp ON poly.polygon_id = sp.id WHERE sp.project_table = '%s') as foo;", PROJECTTABLE);
            $this->data[$table] = $this->get_table_data($ID, $cmd);

            $table = 'file_connect';
            $cmd = sprintf("SELECT row_to_json(foo) FROM (SELECT fc.* FROM system.file_connect fc LEFT JOIN system.files f ON fc.file_id = f.id WHERE f.project_table = '%s') as foo;", PROJECTTABLE);
            $this->data[$table] = $this->get_table_data($ID, $cmd);

            //appending some project constants to the exported data
            $this->append_local_vars();

            //creating a tmpdir
            $export_name = PROJECTTABLE . '_export_'.date('YmdHis');
            $tmpdir = '/tmp/' . $export_name;
            if (! mkdir($tmpdir)) {
                echo common_message('error', 'tmpdir creation error');
                return;
            }

            $tarcmd = ['biomaps.json','gisdata','shared','system'];
            // copying the custom files of the project
            foreach ($this->private_files as $pf) {
                $src_dir = getenv('PROJECT_DIR') . $pf;
                $dst_dir = "$tmpdir/$pf";
                if (is_dir($src_dir)) {
                    if (preg_match('/(\w+)/',$pf,$basedir)) {
                        $tarcmd[] = $basedir[1];
                    }
                    mkdir($dst_dir,0777,true);
                    $cmd = "cp -r $src_dir/. $dst_dir";
                    exec($cmd);
                }
            }

            // dumping the gisdata tables
            $cmd = sprintf("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_name LIKE '%s%%';", PROJECTTABLE);
            if (! $res = pg_query($ID, $cmd)) {
                //TODO
            }
            $results = pg_fetch_all($res);
            $this->gisdata = array_merge($this->gisdata,array_column($results, 'table_name'));

            mkdir("$tmpdir/gisdata",0777,true);
            foreach ($this->gisdata as $table) {
                $sed = "";
                $ntable = $table;
                if (isset($request['new_project_name']) && $request['new_project_name'] !== '') {
                    $sed = "| sed s/".PROJECTTABLE."/{$request['new_project_name']}/g ";
                    $ntable = str_replace(PROJECTTABLE,$request['new_project_name'],$table);
                }

                $cmd = "{$this->pg_dump} --dbname=postgresql://" . gisdb_user . ":".gisdb_pass."@".gisdb_host.":".POSTGRES_PORT."/".gisdb_name." -s -t $table $sed> $tmpdir/gisdata/$ntable.sql";
                exec($cmd);

                $this->append_gisdata_data($table);
            }
            // writing the biomaps settings to a json file
            $fp = fopen($tmpdir.'/biomaps.json', 'w');

            //!!!!!!!!!ezt így veszélyes - nem így kéne
            $biomaps = (isset($request['new_project_name']) && $request['new_project_name'] !== '')
                ? str_replace(PROJECTTABLE,$request['new_project_name'],json_encode($this->data, JSON_PRETTY_PRINT))
                : json_encode($this->data);

            fwrite($fp,$biomaps);
            fclose($fp);

            mkdir("$tmpdir/shared",0777,true);
            foreach ($request['include_shared'] as $shared_table) {
                $cmd = "{$this->pg_dump} --dbname=postgresql://" . gisdb_user . ":".gisdb_pass."@".gisdb_host.":".POSTGRES_PORT."/".gisdb_name." -t shared.$shared_table > $tmpdir/shared/$shared_table.sql";
                exec($cmd);
            }
            $cmd = sprintf("SELECT oid FROM pg_catalog.pg_proc WHERE proname LIKE '%%%s%%';", PROJECTTABLE);
            if (! $res = pg_query($ID, $cmd)) {
                //TODO
            }
            $results = pg_fetch_all($res);
            $function_ids = array_column($results, 'oid');

            foreach ($function_ids as $f) {
                $sed = "";
                if (isset($request['new_project_name']) && $request['new_project_name'] !== '') {
                    $sed = "| sed s/".PROJECTTABLE."/{$request['new_project_name']}/g ";
                }

                $cmd = "{$this->psql} --dbname=postgresql://" . gisdb_user . ":".gisdb_pass."@".gisdb_host.":".POSTGRES_PORT."/".gisdb_name." -t -P format=unaligned -c \"SELECT pg_get_functiondef($f)\" $sed  >> $tmpdir/gisdata/functions.sql && echo ';' >> $tmpdir/gisdata/001-functions.sql";
                exec($cmd);
            }

            $cmd = sprintf("SELECT sequence_name FROM information_schema.sequences WHERE sequence_name LIKE '%%%s%%';", PROJECTTABLE);
            if (! $res = pg_query($ID, $cmd)) {
                //TODO
            }
            $results = pg_fetch_all($res);
            $sequences = array_column($results, 'sequence_name');

            foreach ($sequences as $seq) {
                $sed = "";
                $nseq = $seq;
                if (isset($request['new_project_name']) && $request['new_project_name'] !== '') {
                    $sed = "| sed s/".PROJECTTABLE."/{$request['new_project_name']}/g ";
                    $nseq = str_replace(PROJECTTABLE,$request['new_project_name'],$seq);
                }

                $cmd = "{$this->pg_dump} --dbname=postgresql://" . gisdb_user . ":".gisdb_pass."@".gisdb_host.":".POSTGRES_PORT."/".gisdb_name." -t $seq $sed> $tmpdir/gisdata/00-$nseq.sql";
                exec($cmd);
            }

            $cmd = "tar -czf ".getenv('PROJECT_DIR')."$export_name.tar.gz -C $tmpdir/ " . implode(' ',$tarcmd);
            exec($cmd);
            exec("rm -rf $tmpdir");

            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';


            $msg = "Export ready: <a href='$protocol://".URL."/$export_name.tar.gz' download> download </a> </br>WARNING SQL functions are not exported </br>";
            if (isset($request['new_project_name']) && $request['new_project_name'] !== '')
                echo common_message('warning', "$msg WARNING: please review the project files, beacause the renaming was not safe for the DB content!!!");
            else
                echo common_message('warning', $msg);

            return;
        }

    }



    private function append_local_vars() {
        $lv = [
            'LANG' => LANG,
            'ACC_LEVEL' => ACC_LEVEL,
            'MOD_LEVEL' => MOD_LEVEL,
            'gisdb_user' => gisdb_user,
            'gisdb_pass' => gisdb_pass,
            'MAP'=> MAP,
            'PRIVATE_MAPFILE'=> PRIVATE_MAPFILE,
            'INVITATIONS'=> INVITATIONS,
            'SMTP_AUTH'=> SMTP_AUTH,
            'SMTP_HOST'=> SMTP_HOST,
            'SMTP_USERNAME'=> SMTP_USERNAME,
            'SMTP_PASSWORD'=> SMTP_PASSWORD,
            'SHINYURL'=> SHINYURL,
            'LOAD_INTROPAGE'=> LOAD_INTROPAGE,
            'MAP_OVER_MAINPAGE'=> MAP_OVER_MAINPAGE,
        ];
        $this->data['local_vars'] = $lv;
    }

    private function append_data($options, $table) {

      $project_column = $options[0];
      $db = $options[1];

      global $$db;

      $cmd = sprintf('SELECT row_to_json(%1$s) FROM %1$s WHERE "%3$s" = \'%2$s\';', $table, PROJECTTABLE, $project_column);
      $this->data[$table] = $this->get_table_data($$db, $cmd);
    }

    private function append_gisdata_data($table) {
        global $ID;
        $cmd = sprintf('SELECT row_to_json(%1$s) FROM %1$s;', $table);
        $this->data[$table] = $this->get_table_data($ID, $cmd);
    }

    private function get_table_data($db, $cmd) {

        if ($res = pg_query($db, $cmd)) {
            $data = [];
            while ($row = pg_fetch_row($res)) {
                $data[] = json_decode($row[0]);
            }
            return $data;
        }
        else {
            log_action("ERROR: $cmd",__FILE__,__LINE__);
            return false;
        }
    }

    public function print_js($request) {
        $js_file = getenv('OB_LIB_DIR').'modules/move_project.js';
        return (file_exists($js_file)) ? file_get_contents($js_file) : '';
    }

}

class project_file {
    var $biomaps;
    var $project_name;
    var $dir = '/tmp/project_import_tmp/';
    var $new_project_dir;
    var $psql;

    public function __construct() {
        $this->biomaps = json_decode(file_get_contents($this->dir.'biomaps.json'));
        $this->project_name = $this->get('projects',0,'project_table');
        $this->new_project_dir = getenv("PROJECT_DIR")."../{$this->project_name}";
        $this->psql = defined('PSQL') ? constant('PSQL') : 'psql';
    }



    // returns the speciefied table, all matching rows or all values from the column (not implemented)
    public function getAll($table, $row = null, $column = null) {

        if (is_null($row) && is_null($column))
            return $this->getTable($table);
        elseif (is_null($column))
            return $this->getRows($table, $row);
        elseif (is_null($row))
            return $this->getColumn($table, $column);

    }
    //returns the speciefied table, the first matching row or one cell of this
    public function get($table, $row = null, $column = null) {

        if (is_null($row))
            return $this->getTable($table);
        elseif (is_null($column))
            return $this->getFirstRow($table, $row);
        else
            return $this->getCell($table, $row, $column);

    }



    private function getTable($table) {
        return $this->biomaps->$table;
    }

    private function getRows($table, $row) {
        $t = $this->getTable($table);

        if (is_int($row) && (gettype($t) == 'array')) {
            $r = [$t[$row]];
        }
        elseif (is_string($row) && (gettype($t) == 'object')) {
            $r = [$t->$row];
        }
        elseif (is_array($row)) {
            $r = array_values(array_filter($t, function ($el) use ($row) {
                $key = $row[0];
                $value = $row[1];
                if (isset($row[2])) {
                    if ($row[2] == '!=')
                        return ($el->$key != $value);
                    elseif ($row[2] == '<')
                        return ($el->$key < $value);
                    elseif ($row[2] == '>')
                        return ($el->$key > $value);
                    else
                        return false;
                }
                else
                    return ($el->$key == $value);
            }));
        }
        return $r;
    }

    private function getColumn($table, $column) {
      $t = $this->getTable($table);
      return array_map(function($row) use ($column) {
        return $row->$column;
      }, $t);

    }

    private function getFirstRow($table, $row) {
        $r = $this->getRows($table, $row);
        return (isset($r[0])) ? $r[0] : new stdClass();
    }

    private function getCell ($table, $row, $column) {
        $r = $this->getFirstRow($table, $row);
        if (! isset($r->$column)) {

          debug("$table -> ".json_encode($row)." -> $column undefined",__FILE__,__LINE__);
        }
        return (gettype($r) === 'object' && isset($r->$column)) ? $r->$column : null;
    }


    private function gisdata_tables() {
      return array_filter(array_keys((array)$this->biomaps), function($tbl) {
        return preg_match("/{$this->project_name}\w?/",$tbl);
      });
    }


    public function set($table, $row, $column, $value) {
        if (!isset($this->biomaps->$table)) {
            return false;
        }
        if (is_int($row)) {
            $this->biomaps->$table[$row]->$column = $value;
            return true;
        }
        elseif (is_array($row)) {
            $matches = array_filter($this->biomaps->$table, function($el) use ($row) {
                return (isset($el->{$row[0]}) && $el->{$row[0]} == $row[1]);
            });
            if (count($matches) === 0) {
                return false;
            }
            foreach ($matches as $k => $match) {
                $this->biomaps->{$table}[$k]->$column = $value;
            }
            return true;
        }
    }

    public function get_short_desc() {
        $lang = $this->get('local_vars','LANG');
        return $this->get('project_descriptions',['language',$lang],'short');
    }

    public function get_long_desc() {
        $lang = $this->get('local_vars','LANG');
        return $this->get('project_descriptions',['language',$lang],'long');
    }

    public function header_names_cmd() {
        $header_names = $this->get('header_names');
        $cmd = '';
        foreach ($header_names as $hn) {
            $hn = (array)$hn;
            $cmd .= sprintf('INSERT INTO header_names (%s) VALUES (%s);',
                $this->prepare_columns_string(array_keys($hn)),
                $this->prepare_values_string(array_values($hn))
            );
        }

        return $cmd;
    }

    public function import_users() {
        global $BID;

        $users = $this->get('users');

        for ($i = 0, $l = count($users); $i < $l; $i++) {
            $user = (array)$users[$i];
            $old_id = $user['id'];
            unset($user['id']);

            $columns = $this->prepare_columns_string(array_keys($user));
            $values = $this->prepare_values_string(array_values($user));

            $cmd = sprintf('INSERT INTO users (%1$s) SELECT %2$s WHERE NOT EXISTS ( SELECT email FROM users WHERE email = %3$s); SELECT id FROM users WHERE email = %3$s;', $columns, $values, quote($user['email']));
            if (!$res = pg_query($BID, $cmd)) {
                echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
                return;
            }
            $results = pg_fetch_assoc($res);
            if (!$this->set('users',['email',$user['email']],'new_id',(int)$results['id'])) {
                print 'pf setter failed';
                return false;
            }
            if (!$this->set('project_roles',['user_id',$old_id],'new_id',(int)$results['id']))
                print 'pf setter failed';
        }
        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_users() {
        global $BID;
        $project_users = $this->get('project_users');
        $cmd = '';
        foreach ($project_users as $pu) {
            $pu = (array)$pu;
            $old_id = $pu['user_id'];
            $pu['user_id'] = $this->get('users',['id',$old_id],'new_id');

            $cmd .= sprintf('INSERT INTO project_users (%s) VALUES (%s);',
                $this->prepare_columns_string(array_keys($pu)),
                $this->prepare_values_string(array_values($pu))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return false;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_roles() {
        global $BID;
        $new_role_ids = $this->requestRoleIds();

        for ($i = 0, $l = count($new_role_ids); $i < $l; $i++) {
            $nri = $new_role_ids[$i];
            $this->set('project_roles',['new_id',$nri['user_id']],'new_role_id',(int)$nri['role_id']);
        }

        $groups = $this->getAll('project_roles',['user_id', null]);
        $cmd = '';
        foreach ($groups as $pg) {
            $cmd = sprintf('INSERT INTO project_roles ("project_table", "description") VALUES (%s, %s) RETURNING role_id;', quote($pg->project_table), quote($pg->description));
            if (!$res = pg_query($BID, $cmd)) {
                echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
                return;
            }
            $results = pg_fetch_assoc($res);
            $this->set('project_roles',['role_id',$pg->role_id],'new_role_id',(int)$results['role_id']);
        }

        //UPDATING role containers
        $groups = $this->get('project_roles');
        $cmd = '';
        foreach ($groups as $pg) {
            $pg = (array)$pg;
            unset($pg['role_id']);
            unset($pg['user_id']);
            $pg['container'] = $this->change_role_ids($pg['container']);
            $cmd .= sprintf('UPDATE project_roles SET container = %s WHERE role_id = %s;',
                $this->quote($pg['container']),
                quote($pg['new_role_id'])
            );

        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }
        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_group_privileges() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $row['role_id'] = $this->get('project_roles',['role_id',$row['role_id']],'new_role_id');
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            log_action($cmd,__FILE__,__LINE__);
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;

    }

    public function import_invites() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');

            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_modules() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $row['group_access'] = $this->change_role_ids($row['group_access']);
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_forms() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        foreach ($rows as $row) {
            $row = (array)$row;
            $old_form_id = $row['form_id'];
            unset($row['form_id']);

            $row['user_id'] = $this->get('users',['id', $row['user_id']], 'new_id');
            $row['groups'] = $this->change_role_ids($row['groups']);
            $row['data_groups'] = $this->change_role_ids($row['data_groups']);

            $cmd = sprintf('INSERT INTO %s (%s) VALUES (%s) RETURNING form_id;',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );

            if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
                echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
                return;
            }

            $results = pg_fetch_assoc($res);
            if (isset($results['form_id'])) {
                $this->set('project_forms',['form_id',$old_form_id],'new_form_id',(int)$results['form_id']);
                $this->import_project_forms_data($old_form_id);
            }

        }
        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    private function import_project_forms_data($form_id) {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->getAll($table,['form_id',$form_id]);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $row['form_id'] = $this->get('project_forms',['form_id',$form_id], 'new_form_id');
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;

    }

    public function import_project_forms_groups() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $row['form_id'] = array_map(function($el) {
                return $this->get('project_forms',['form_id',$el],'new_form_id');
            },$row['form_id']);
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;

    }

    public function import_project_layers() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_custom_reports() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_mapserver_layers() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_queries() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_variables() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_project_metaname() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_queries() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_supervisor() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['update_id']);
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_translations() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            unset($row['id']);
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_settings_import() {
        global $BID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        $cmd = '';
        foreach ($rows as $row) {
            $row = (array)$row;
            $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
            $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );
        }
        if (!empty($cmd) && !$res = pg_query($BID, $cmd)) {
            echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
            return;
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_uploadings() {
        global $ID;
        preg_match('/import_(\w+)/', __FUNCTION__, $t);
        $table = $t[1];

        $rows = $this->get($table);
        foreach ($rows as $row) {
            $row = (array)$row;
            $row['form_id'] = $this->get('project_forms',['form_id',$row['form_id']],'new_form_id');
            $row['uploader_id'] = $this->get('project_roles',['role_id',$row['uploader_id']],'new_role_id');
            $row['group'] = $this->change_role_ids($row['group']);
            $old_uplid = $row['id'];
            unset($row['id']);
            $cmd = sprintf('INSERT INTO system.%s (%s) VALUES (%s) RETURNING id;',
                $table,
                $this->prepare_columns_string(array_keys($row)),
                $this->prepare_values_string(array_values($row))
            );

            if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
                echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
                return;
            }

            $results = pg_fetch_assoc($res);
            if (isset($results['id'])) {
                $this->set('uploadings',['id',$old_uplid],'new_id',(int)$results['id']);
              }
        }

        print __METHOD__ . " <span class='done'>done</span>.<br>";
        return true;
    }

    public function import_gisdata() {

      global $ID;
      $tables = scandir($this->dir.'gisdata/');

      foreach ($tables as $table) {
        if (in_array($table,['.','..'])) {
          continue;
        }
        $file = $this->dir . 'gisdata/'. $table;
        $import_log = "{$this->new_project_dir}/import.log";
        $cmd = "echo '$table' >> $import_log && {$this->psql} postgresql://" . $this->get('local_vars','gisdb_user') . ":".$this->get('local_vars','gisdb_pass')."@".GISDB_HOST.":".POSTGRES_PORT."/gisdata -f $file >> $import_log 2>&1";
        exec($cmd);
      }
      $this->import_uploadings();
      $tables = $this->gisdata_tables();
      foreach ($tables as $table) {
        $this->import_gisdata_table($table);
      }

      $this->import_shared_polygons();
      $this->import_query_buff();
      $this->import_polygon_users();
      $this->import_files();
      $this->import_file_connect();
      $this->import_evaluations();

  }

    public function import_shared_polygons() {
      global $ID;

      preg_match('/import_(\w+)/', __FUNCTION__, $t);
      $table = $t[1];
      $rows = $this->get($table);
      foreach ($rows as $row) {
        $row = (array)$row;
        $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
        $old_id = $row['id'];
        unset($row['id']);
        $cmd = sprintf('INSERT INTO system.%s (%s) VALUES (%s) RETURNING (id);',
              $table,
              $this->prepare_columns_string(array_keys($row)),
              $this->prepare_values_string(array_values($row))
            );
        if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
          echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
          return;
        }

        $results = pg_fetch_assoc($res);
        if (isset($results['id'])) {
          $this->set('shared_polygons',['id',$old_id],'new_id',(int)$results['id']);
        }
      }
      print __METHOD__ . " <span class='done'>done</span>.<br>";
      return true;

    }

    public function import_query_buff() {
      global $ID;

      preg_match('/import_(\w+)/', __FUNCTION__, $t);
      $table = $t[1];
      $rows = $this->get($table);
      $cmd = '';
      foreach ($rows as $row) {
        $row = (array)$row;
        $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
        unset($row['id']);
        $cmd .= sprintf('INSERT INTO system.%s (%s) VALUES (%s);',
        $table,
        $this->prepare_columns_string(array_keys($row)),
        $this->prepare_values_string(array_values($row))
      );
    }
    if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
      echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
      return;
    }

    print __METHOD__ . " <span class='done'>done</span>.<br>";
    return true;

    }

    public function import_polygon_users() {
      global $ID;

      preg_match('/import_(\w+)/', __FUNCTION__, $t);
      $table = $t[1];
      $rows = $this->get($table);
      $cmd = '';
      foreach ($rows as $row) {
        $row = (array)$row;
        $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
        $row['polygon_id'] = $this->get('shared_polygons',['id',$row['polygon_id']],'new_id');
        $cmd .= sprintf('INSERT INTO system.%s (%s) VALUES (%s);',
        $table,
        $this->prepare_columns_string(array_keys($row)),
        $this->prepare_values_string(array_values($row))
      );
    }
    if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
      echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
      return;
    }

    print __METHOD__ . " <span class='done'>done</span>.<br>";
    return true;

  }

    public function import_files() {
      global $ID;

      preg_match('/import_(\w+)/', __FUNCTION__, $t);
      $table = $t[1];
      $rows = $this->get($table);
      foreach ($rows as $row) {
        $row = (array)$row;
        $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
        $old_id = $row['id'];
        unset($row['id']);
        $cmd = sprintf('INSERT INTO system.%s (%s) VALUES (%s) RETURNING (id);',
              $table,
              $this->prepare_columns_string(array_keys($row)),
              $this->prepare_values_string(array_values($row))
            );
        if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
          echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
          return;
        }

        $results = pg_fetch_assoc($res);
        if (isset($results['id'])) {
          $this->set($table,['id',$old_id],'new_id',(int)$results['id']);
        }
      }
      print __METHOD__ . " <span class='done'>done</span>.<br>";
      return true;

    }


    public function import_file_connect() {
      global $ID;

      preg_match('/import_(\w+)/', __FUNCTION__, $t);
      $table = $t[1];
      $rows = $this->get($table);
      $cmd = '';
      foreach ($rows as $row) {
        $row = (array)$row;
        $row['file_id'] = $this->get('files',['id',$row['file_id']],'new_id');

        $cmd .= sprintf('INSERT INTO system.%s (%s) VALUES (%s);',
        $table,
        $this->prepare_columns_string(array_keys($row)),
        $this->prepare_values_string(array_values($row))
      );
    }
    if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
      echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
      return;
    }

    print __METHOD__ . " <span class='done'>done</span>.<br>";
    return true;

  }

    public function import_evaluations() {
      global $ID;

      preg_match('/import_(\w+)/', __FUNCTION__, $t);
      $table = $t[1];
      $rows = $this->get($table);
      $cmd = '';
      foreach ($rows as $row) {
        $row = (array)$row;
        $row['user_id'] = $this->get('users',['id',$row['user_id']],'new_id');
        unset($row['id']);
        $cmd .= sprintf('INSERT INTO system.%s (%s) VALUES (%s);',
        $table,
        $this->prepare_columns_string(array_keys($row)),
        $this->prepare_values_string(array_values($row))
      );
    }
    if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
      echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
      return;
    }

    print __METHOD__ . " <span class='done'>done</span>.<br>";
    return true;

  }


    public function import_gisdata_table($table) {
      global $ID;

      $rows = $this->get($table);
      $cmd = '';
      foreach ($rows as $row) {
        $row = (array)$row;
        if (isset($row['obm_uploading_id'])) {
          $row['obm_uploading_id'] = $this->get('uploadings',['id',$row['obm_uploading_id']],'new_id');
        }
        $cmd .= sprintf('INSERT INTO %s (%s) VALUES (%s);',
        $table,
        $this->prepare_columns_string(array_keys($row)),
        $this->prepare_values_string(array_values($row))
      );
    }
    if (!empty($cmd) && !$res = pg_query($ID, $cmd)) {
      echo "<span class='err'>SQL error: ".__METHOD__."</span><br>";
      return;
    }

    print __METHOD__ . " <span class='done'>done</span>.<br>";
    return true;
  }

    public function copy_private_files($subdir) {
        if (is_dir($this->dir.$subdir)) {
            return copyr($this->dir.$subdir,$this->new_project_dir.'/'.$subdir);
        }
        return 0;
    }


    private function requestRoleIds() {
        global $BID;

        $cmd = sprintf("SELECT user_id, role_id FROM project_roles WHERE project_table = %s;", quote($this->project_name));
        if (! $res = pg_query($BID, $cmd)) {
            print 'requestRoleIds SQL error';
        }
        $results = pg_fetch_all($res);

        return $results;
    }

    private function change_role_ids($arr) {
        return array_map(function($el) {
            return ($el != 0)
                ? $this->get('project_roles',['role_id',$el],'new_role_id')
                : $el;
        },$arr);
    }

    private function prepare_columns_string($arr) {
        return implode(',',array_map(function($el) {
            return '"'.$el.'"';
        },$arr));
    }

    private function prepare_values_string($arr) {
        return implode(',',array_map([__CLASS__,'quote'],$arr));
    }

    static function quote($text,$force_quote=false,$debug=false) {
        //if ($text=='NULL' or trim($text)=='') return "NULL";
        if ($debug) {
            debug($text,__FILE__,__LINE__);
            debug(gettype($text),__FILE__,__LINE__);
        }

        if (is_null($text)) {
          return "NULL";
        }

        if ($text==='NULL') return "NULL";

        if (is_int($text)) {
            return $text;
        }

        if (is_bool($text)) {
            $text = ($text) ? 't' : 'f';
        }

        if (is_array($text)) {
            /*
            for ($i = 0, $l = count($text); $i < $l; $i++) {

                $text[$i] = (is_numeric($text[$i]))
                    ? $text[$i]
                    : pg_escape_literal($text[$i]); // ez nem jó
            }
             */
            $text = '{'.implode(',',$text).'}';
        }

        if (is_object($text)) {
            $text = json_encode($text);
        }

        if ($force_quote===false)
            if (gettype($text)=='integer') return $text;

        //if (trim($text)=='') return "NULL";

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $random_delimeter = substr( str_shuffle( $chars ), 0, 16 );
        $text = '$'.$random_delimeter.'$'.trim($text).'$'.$random_delimeter.'$';

        return $text;
    }
}

?>
