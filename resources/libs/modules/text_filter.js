/* text filter module
 *
 * */
    var species_plus = new Array();

    // taxon_filter function
    function setTaxon( message ) {
            var a = message.split('#');
            species_plus = species_plus.concat(a[0] + '#' + a[1]);
            $('<button class="pure-button button-href remove-species" value="' + a[0] + '#' + a[1] + '">' + a[2] + ' &nbsp; <i class="fa fa-close"></i></button>').prependTo( '#tsellist' );
            $('#taxon_sim').val('');
            $('#taxon_sim').html('');
            $('#taxon_sim').text('');
            $('#taxon_sim').focus();
    }

    function getParamsOf_text_filter() {
            // text filter variables processing
            var taxon_val = '';
            var trgm_string = '';
            var allmatch = 0;
            var onematch = 0;
            var qids = new Array();
            var qval = new Array();
            //cleaning taxon filter
            //$('#tsellist').find('button').remove();
            // a map_filter_box.php.inc-ben lévő
            // qf class input/select metők tartalmai
            // kerülnek a myQVar objectbe
            $('.qf').each(function(){
                var stri = '';
                var stra = new Array();
                var qid=$(this).attr('id');
                if( $(this).prop('type') == 'text' ) {
                    stra.push($(this).val());
                } else if ($(this).prop('type') == 'button' ) {
                    stra.push(($(this).find('i').hasClass('fa-toggle-on')) ? 'on' : '');
                }
                else if ($(this).hasClass('divInput')) {
                    const id = [];
                    $(this).children().each(function() {
                        if ($(this).hasClass('search-relation'))
                            id.push($(this).html());
                        else
                            id.push($(this).data('id'));
                    });
                    stra = id;
                } else {
                    $('.mapfb').find('#' + qid + ' option:selected').each(function () {
                        stra.push($(this).val());
                    });
                }
                if (stra.length){
                    qids.push(qid);
                    qval.push(JSON.stringify(stra));
                }
            });
            /* többszörös fajválasztó */
            if (species_plus.length){
               trgm_string = JSON.stringify(species_plus);
               //species_plus = new Array();
            }
            if ($('#allmatch').is(':checked')) {
                    allmatch = 1;
            }
            if ($('#onematch').is(':checked')) {
                    onematch = 1;
            }

            var myVar = { trgm_string:trgm_string,allmatch:allmatch,onematch:onematch, }

            // text filter variables
            for (var i=0; i < qids.length; i++) {
                myVar['qids_' + qids[i]] = qval[i];
            }
        return myVar;
    }

    $(document).ready(function() {

        text_filter_enabled = 1;

        $('#mapholder').on('click','#taxon-search-settings',function() {
            $('#taxon-search-settings-box').toggle();
        });

        $('#taxon_sim').autocomplete({
            source: function( request, response ) {
                $.post(projecturl+'includes/getList.php', {type:'taxon',term:$('#taxon_sim').val()}, function (data) {
                        response(JSON.parse(data));
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                setTaxon( ui.item.id + '#' + ui.item.meta + '#' + ui.item.value);
                $(this).val('');
                return false;
            }
        });
        /* remove species
         * */
        $('body').on('click','.remove-species',function() {
            for( var i = 0; i < species_plus.length; i++) {
                if ( species_plus[i] === $(this).val()) {
                    species_plus.splice(i, 1);
                }
            }
            $(this).remove();
        });

        $('#colourringdiv').appendTo('#body');
        $('#mapfilters').on('click','#filter-options-toggle',function(){
            $('#filter-options-box').toggle();
            $(this).find('i').toggleClass('fa-minus');
            $(this).find('i').toggleClass('fa-plus');
        });

        /* Clear incremntal filters
         *
         * */
        $('#mapfilters').on('click','#clear-filters',function(){
            species_plus = new Array();
            $('#tsellist').find('button').remove();
            $('.divInput').find('div').remove();
            $('.mapfb').find('input:text').val('');
            $('.mapfb').find('textarea').html('');
            $('.mapfb').find('button').each(function() {
                if ($(this).find('i').hasClass('fa-toggle-on')) {
                    $(this).find('i').toggleClass('fa-toggle-on');
                    $(this).find('i').toggleClass('fa-toggle-off');
                    $(this).removeClass('button-success');
                }
            });
            $('.mapfb').find('select').find('option').removeAttr('selected');
            geometry_query_neighbour = 0;
            geometry_query_session = 0;
            geometry_query_selection = 0;
            drawLayer.destroyFeatures();
            skip_loadQueryMap_customBox = 0;
        });

        // mapfilterbox query text filter autocomplete wrapper
        $('#mapfilters').on('input','.qfautocomplete',function() {
            var ref = $(this).attr('id');
            var thise = $(this);
            // query table
            var etr = $(this).data('etr');
            var nested_column = $(this).data('nested_column');
            var nested_element = $(this).data('nested_element');
            var all_options = $(this).data('all_options');
            $('#'+nested_element+' option').remove();

            // optional extension for speeding up query, if autocomplete table contains a column with clustered/indexed value
            var speedup_filter = 'off';
            var speedup = $(this).closest('.mapfb').find('.speedup_filter');
            if (typeof speedup != 'undefined') {
                if (speedup.val()=='on')
                    speedup_filter = 'on';
            }
            thise.css('background-color','white');


            $('.qfautocomplete').autocomplete({

                source: function( request, response ) {
                    thise.css('cursor','progress');
                    $.post('ajax', {'m':'text_filter','qflist':1,'id':ref,'etr':etr,'term':request.term,'speedup_filter':speedup_filter}, function (data) {
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            thise.css('cursor','default');
                            thise.css('background-color','red');

                            if (retval['message'] == 'Page expired') {
                                alert(retval['message']);
                                location.reload();
                            }
                        } else if (retval['status']=='fail') {
                            alert('autocompleting failed');
                        } else if (retval['status']=='success') {
                            response(JSON.parse(retval['data']));
                        }
                        thise.css('cursor','default');
                    });
                },
                select: function( event, ui ) {
                    // select element from list fireing the nested list creation
                    if (typeof nested_element != 'undefined') {
                        if (ui.item.value=='')
                            return;

                        thise.css('cursor','progress');
                        $.post('ajax', {'m':'text_filter','create_dynamic_menu':1,'filter_column':ref,'etr':etr,'filter_term':ui.item.value,'nested_column':nested_column,'nested_element':nested_element,'all_options':all_options},
                            function (data) {
                                $('#'+nested_element).append(data);
                                $('#'+nested_element).trigger('nested_data_appended');
                                thise.css('cursor','default');
                        });
                    }
                },
                minLength: 1,
            });
        });

        // nested menus ajax trigger call
        $('#mapfilters').on('change select','.nested',function() {
            var ref = $(this).attr('id');
            // query table
            var etr = $(this).data('etr');
            $('#'+nested_element+' option').remove();

            // if this list creates an other list depending on the chosen value
            var nested_column = $(this).data('nested_column');
            var nested_element = $(this).data('nested_element');
            var all_options = $(this).data('all_options');
            if ($('option:selected',this).text()=='')
                return;
            $.post('ajax', {'m':'text_filter','create_dynamic_menu':1,'filter_column':ref,'etr':etr,'filter_term':$('option:selected',this).text(),'nested_column':nested_column,'nested_element':nested_element,'all_options':all_options},
                function (data) {
                    $('#'+nested_element).append(data);
                    $('#'+nested_element).trigger('nested_data_appended');
            });
        });
        /* obm_datum
         * date query events
         * */
        $( "#obm_uploading_date" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat:'yy-mm-dd',
          firstDay:1,
          constrainInput: false,
          showAnim: "fold"
        });
        $( "#exact_date" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat:'yy-mm-dd',
          firstDay:1,
          constrainInput: false,
          showAnim: "fold"
        });
        $( "#exact_from" ).datepicker({
          defaultDate: "-1w",
          changeMonth: true,
          changeYear: true,
          dateFormat:'yy-mm-dd',
          firstDay:1,
          onClose: function( selectedDate ) {
            $( "#exact_to" ).datepicker( "option", "minDate", selectedDate );
          }
        });
        $( "#exact_to" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat:'yy-mm-dd',
          firstDay:1,
          onClose: function( selectedDate ) {
            $( "#exact_from" ).datepicker( "option", "maxDate", selectedDate );
          }
        });
        $( "#exact_mfrom" ).datepicker({
          changeMonth: true,
          dateFormat:'mm-dd',
          firstDay:1,
          onClose: function( selectedDate ) {
            $( "#exact_to" ).datepicker( "option", "minDate", selectedDate );
          },
          beforeShow: function (input, inst) {
            inst.dpDiv.addClass('NoYearDatePicker');
          },
          onClose: function(dateText, inst){
                    inst.dpDiv.removeClass('NoYearDatePicker');
          }
        });
        $( "#exact_mto" ).datepicker({
          changeMonth: true,
          dateFormat:'mm-dd',
          firstDay:1,
          onClose: function( selectedDate ) {
            $( "#exact_from" ).datepicker( "option", "maxDate", selectedDate );
          },
          beforeShow: function (input, inst) {
            inst.dpDiv.addClass('NoYearDatePicker');
          },
          onClose: function(dateText, inst){
                    inst.dpDiv.removeClass('NoYearDatePicker');
          }
        });

        $('body').on("change click",".filter-sh-advopt",function() {
        var val = $(this).val();
        if($(this).is(':checked')){
            $(this).closest('table').find('.text-options_'+$(this).val()).each(function(){
                 if($(this).hasClass('def-hidden')){
                     $(this).removeClass('def-hidden');
                 }
            });
        } else {
            $(this).closest('table').find('.text-options_'+$(this).val()).each(function(){
                if(!$(this).hasClass('def-hidden')){
                    $(this).addClass('def-hidden');
                    //clear search text if search item is closed
                    $("#" + val).val('');
                }
            });
        }
        });

    });
