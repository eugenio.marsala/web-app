<?php
/* This module will be an iterface for users to choose a grid (e.g. UTM )for displaying data
 * It is just the first few lines...
 * */
class grid_view {
    function print_box ($params) {
        global $ID;
        $p = preg_split('/;/',$params);
        $available_grids = array();
        foreach ($p as $pm) {
            $poptions = preg_split('/:/',$pm);
            if ($poptions[0] == 'layer_options') {
                $grids = preg_split('/,/',$poptions[1]);
                //utm_10 (dinpi_grid),utm_100 (dinpi_grid)
                $available_grids = array();
                foreach($grids as $gr) {
                    if (preg_match('/(\w+)/',$gr,$m)){
                        $available_grids[] = $m[1];
                    }
                }
            }
        }

        $js = "<script language='javascript'>$(document).ready(function() { ";
        $js .= "
        $(\"body\").on('click','#qgrids_chooser_send',function(){

            $( '#dialog' ).text('Waiting for the WMS response...');
            var isOpen = $( '#dialog' ).dialog( 'isOpen' );
            if (!isOpen) $( '#dialog' ).dialog( 'open' );

            $.post('ajax', {choose_display_grid_for_map:$('#qgrids_chooser').val()},
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    $( '#dialog' ).text(retval['message']);
                } else if (retval['status']=='fail') {
                    $( '#dialog' ).text('Invalid response received.');
                } else if (retval['status']=='success') {


                    for(var i=0;i<dataLayers.length;i++) {
                        dataLayers[i].visibility = true
                        dataLayers[i].redraw(true);
                    }
                    $( '#dialog' ).dialog( 'close' );
                    /*$.post('ajax',{'check_layer':dataLayers_ns},function(data) {
                        var retval = jsendp(data);
                        if (retval['status']=='success') {
                            var v = retval['data'];
                            var layers_state = JSON.parse(v);
                            for(var i=0;i<layers_state.length;i++) {
                                if (layers_state[i]=='drop') {
                                    //set visibility false prevent send query to this layer!
                                    dataLayers[i].visibility = false
                                } else {
                                    dataLayers[i].visibility = true
                                }
                            }
                        }
                        for(var i=0;i<dataLayers.length;i++) {
                            dataLayers[i].redraw(true);
                        }
                    });*/
                }
            });

        });";
        $js .= "});</script>";

        $t = new table_row_template();
    
        $cmd = sprintf("SELECT c.column_name, pgd.description FROM pg_catalog.pg_statio_all_tables as st inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid) 
            INNER JOIN information_schema.columns c on 
                (pgd.objsubid=c.ordinal_position and c.table_schema=st.schemaname and c.table_name=st.relname and c.table_name = '%s_qgrids' and c.table_schema = 'public')",$_SESSION['current_query_table']);
     
        $res = pg_query($ID,$cmd);
        $options = "<option value=''></option>";
        while($row=pg_fetch_assoc($res)) {
            $selected = '';
            if (in_array($row['column_name'],$available_grids)) {
                $label = $row['description'];
                if (isset($_SESSION['display_grid_for_map']) and $row['column_name'] == $_SESSION['display_grid_for_map'])
                    $selected = 'selected';
                $grid_text = str_grid;
                if ($row['column_name'] == 'original') {
                    $label = str_original;
                    $grid_text = "";
                }
                $options .= sprintf("<option value='%s' $selected>%s %s</option>",$row['column_name'],$label,$grid_text);
            }
        }
        
        $t->cell(mb_convert_case(str_display_data, MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        $t->cell("<select id='qgrids_chooser' style='width:100%'>$options</select>",2,'content');
        $t->row();
        $t->cell("<button id='qgrids_chooser_send' class='button-gray button-xlarge pure-button'><i class='fa-refresh fa'></i> ".str_update."</button>",2,'title');
        $t->row();

        return sprintf("%s<table class='mapfb'>%s</table>",$js,$t->printOut());
    }
    // first element of the list is the default!
    function default_grid_geom($params) {
        $p = preg_split('/;/',$params);
        foreach ($p as $pm) {
            $poptions = preg_split('/:/',$pm);
            if ($poptions[0] == 'layer_options') {
                $gr = preg_split('/,/',$poptions[1]);
                $default_grid = '';
                if (preg_match('/(\w+)/',$gr[0],$m)){
                    $default_grid = $m[1];
                }

                return $default_grid;
            }
        }
    }
    //overlay a layer
    function get_grid_layer($params) {
        $p = preg_split('/;/',$params);
        foreach ($p as $pm) {
            $poptions = preg_split('/:/',$pm);
            if ($poptions[0] == 'layer_options') {
                //utm_10 (dinpi_grid),utm_100 (dinpi_grid)
                $lg = preg_split('/,/',$poptions[1]);
                $layers = array();
                foreach($lg as $l) {
                    if (preg_match('/([\w.]+)\s*\((\w+)\)/',$l,$m)) {
                        $layers[$m[1]] = $m[2];
                    }
                }
                return $layers;
            }
        }
    }
    function filter_query($params) {
        $p = preg_split('/;/',$params);
        foreach ($p as $pm) {
            $poptions = preg_split('/:/',$pm);
            //"AND layer_cname='layer_data_biotika'";
            if ($poptions[0] == 'filter_query' and $poptions[1]!='') {
                return sprintf('AND layer_cname IN (%s)',implode(',',array_map('quote',preg_split('/,/',$poptions[1]))) );
            }
        }
    }
}
?>
