<?php
class create_pg_user {
    function new_pg_user($params) {
        global $ID,$GID;
        
        $username = preg_replace('/[@.-]/','_',$_SESSION['Tmail']);
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $password = substr( str_shuffle( $chars ), 0, 16 );
        $date_now = date_create(date('Y-m-d'));

        $date = date_add($date_now, date_interval_create_from_date_string('1 year'));
        $date = date_format($date, 'Y-m-d');

        

        // create PROJECTTABLE_user IF NOT EXISTS
        pg_query($GID,sprintf('DO
        $body$
        BEGIN
           IF NOT EXISTS (
              SELECT 1
              FROM   pg_catalog.pg_roles
              WHERE  rolname = \'%1$s_user\') THEN
              CREATE ROLE %1$s_user NOLOGIN NOINHERIT;
           END IF;
        END
        $body$
        ',PROJECTTABLE));

        //--GRANT usage ON SCHEMA public to %s;
        //GRANT SELECT ON ALL PROJECTABLE TO PROJECTABLE_user
        $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."'";
        $result = pg_query($GID,$cmd);
        while ($row = pg_fetch_assoc($result)) {
            pg_query($ID,sprintf('GRANT select on table %s to %s_user',$row['m'],PROJECTTABLE));
        }

        pg_query($GID,'BEGIN');
        #$cmd = sprintf('DROP ROLE IF EXISTS %1$s;CREATE ROLE %1$s WITH LOGIN PASSWORD \'%2$s\' VALID UNTIL \'%3$s\' CONNECTION LIMIT 2 INHERIT;GRANT %4$s_user TO %1$s;',$username,$password,$date,PROJECTTABLE);
        $cmd = sprintf("SELECT 1 FROM pg_roles WHERE rolname='%s'",$username);
        $res = pg_query($GID,$cmd);
        
        if (pg_num_rows($res)) {
            $cmd = sprintf('ALTER USER %1$s WITH PASSWORD \'%2$s\';ALTER USER %1$s VALID UNTIL \'%3$s\'',$username,$password,$date);
            
        } else {
            $cmd = sprintf('CREATE ROLE %1$s WITH LOGIN PASSWORD \'%2$s\' VALID UNTIL \'%3$s\' INHERIT;GRANT %4$s_user TO %1$s;',$username,$password,$date,PROJECTTABLE);
        }

        if (pg_query($GID,$cmd)) {

            pg_query($GID,'COMMIT');
            return common_message('ok',array('usern'=>$username,'passw'=>$password));
        } else {
            return common_message('error',pg_last_error($GID));
        }
    }

    function profileItem() {
        global $GID;
        $username = preg_replace('/[@.-]/','_',$_SESSION['Tmail']);
            
        $em = "<tr><td><button id='create_pg_user' class='button-large button-secondary pure-button pure-u-1-1'><i class='fa fa-user-circle-o'></i> ".t(str_create)."</button></td></tr>";

        $res = pg_query($GID,sprintf('SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = \'%s\'',$username));
        if (pg_num_rows($res)) {
            $em .= "<tr><td><span id='pg_user_name'>$username</span></td></tr>";
        }
        $em .= "<tr><td>".wikilink('modules.html#create-pg-user',str_what_is_postgres_user)."</td></tr>";

        return [
            'label' => str_create_pg_user,
            'fa' => 'fa-user-circle',
            'item' => $em
        ];

    }
    function print_js() {
        echo "
    // create pg user module
    $(document).ready(function() {
        // Create Postgres User module Call
        $('body').on('click','#create_pg_user',function() {
            $.post('ajax', {create_pg_user:1},
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    $( '#dialog' ).text(retval['message']);
                } else if (retval['status']=='fail') {
                    $( '#dialog' ).text('Invalid response received.');
                } else if (retval['status']=='success') {
                    v = retval['data'];
                    $( '#dialog' ).html('Use the follwing username and password to connect the postgres database:<br><br>'+v.usern+'<br>'+v.passw);
                    $( '#pg_user_name').text(v.usern);
                }

                var isOpen = $( '#dialog' ).dialog( 'isOpen' );
                if(!isOpen) $( '#dialog' ).dialog( 'open' );
            });
        });
    });";

    }
}
?>
