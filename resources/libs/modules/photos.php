<?php
class photos extends module {
    function print_js ($params) {

        $js = (file_exists(getenv('OB_LIB_DIR').'modules/photos.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/photos.js') : '';

        $js .= "
    var str_yes_i_want = '".str_yes_i_want."!';
    var str_no_thanks = '".str_no_thanks."'; 
    var str_set_coordinates_from = '".str_set_coordinates_from."';
    var str_accuracy = '".str_accuracy."';
    var geom_col = '{$_SESSION['st_col']['GEOM_C']}';
";
        return $js;
    }

    // attachment upload dialog
    function upload_box ($params) {
        /* upload file modal dialog:
         * - change color of buttons
         * - triggering upload event
         * */ 

        $div = "<div id='photo_upload_div' class='fix-modal' style='border:1px solid #666;border-bottom:4px solid #666'><h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:-10px -10px 10px -10px !important'>".t(str_upload)."</h2><button style='margin: -11px' class='pure-button button-passive fix-modal-close' id='pud-close'><i class='fa fa-close'></i></button><form method='post' enctype='multipart/form-data' class='pure-form'><input type='file' id='photo_upload-file' name='image_files[]' multiple='multiple' class='pure-button button-secondary'><br><br>
        <button id='photo_upload' style='display:none' class='button-xlarge pure-button'><i class='fa fa-upload'></i> ".t(str_upload)."</button>
        </form><div id='uresponse'></div>
        <button id='photo_anchor' style='margin-top:8px'  class='button-xlarge pure-button'><i class='fa fa-anchor'></i> ".t(str_set)."</button>
        </div>";
        return $div;
    }

    // photo/attachment viewer
    function photo_viewer($params,$def) {
        if ($def == 'upload') {
            $drop_btn = '<button class="pure-button button-error button-large p-delete" title="'.str_delete.'"><i class="fa fa-trash fa-lg"></i></button>';
            $download_btn = '';
        } else {
            $drop_btn = '<button class="pure-button button-error button-large p-delete" title="'.str_unlink_file_from_data.'"><i class="fa fa-unlink fa-lg"></i></button>';
            $download_btn = '<button class="pure-button button-success button-large p-download" title="'.str_download_full_size.'"><i class="fa fa-download fa-lg"></i></button>';
        }

        $div = '
<div id="photodiv">
    <div id="photodiv-carpet"></div>
    <div id="photodiv-buttons">
        '.$download_btn.'
        <button class="pure-button button-secondary button-large p-expand"><i class="fa fa-arrows-alt fa-lg"></i></button> 
        <button class="pure-button button-secondary button-large p-exif" >Info</button>
        <button class="pure-button button-gray button-large p-save" ><i class="fa fa-floppy-o fa-lg"></i></button>
        '.$drop_btn.'
        <button class="pure-button button-secondary button-large p-close"><i class="fa fa-times fa-lg"></i></button> 
    </div>
    <ul id="photodiv-exif"></ul>
    <div id="photodiv-frame">
        <textarea id="photodiv-comment"></textarea>
        <div style="position:absolute;right:0;top:0">
            <div id="photodiv-data"></div>
            <div id="photodiv-thumbnails"></div>
        </div>
    </div>
</div>';

        return $div;
    }

    public function init($params, $pa) {

        debug('photos init() running',__FILE__,__LINE__);

        global $ID;

        $table = (isset($pa['mtable'])) ? $pa['mtable'] : PROJECTTABLE;
        //checking trigger existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.triggers WHERE trigger_schema = 'public' AND trigger_name = 'file_connection' AND event_object_table = '%s');", $table);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE TRIGGER file_connection AFTER INSERT ON %1$s FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();',$table);
        }
        else {
            $cmd = sprintf("SELECT tgenabled FROM pg_catalog.pg_trigger WHERE NOT tgisinternal AND tgrelid = '%s'::regclass and tgname = 'file_connection';", $table);
            if( $res = parent::query($ID,$cmd)) {
                $result = pg_fetch_assoc($res[0]);
                if ($result['tgenabled'] == 'D') {
                    $cmd1[] = sprintf('ALTER TABLE %1$s ENABLE TRIGGER file_connection;',$table);
                }
            }
        }

        if (!empty($cmd1)) {

            return parent::query($ID,$cmd1);
        }

        return false;
    }
}
?>
