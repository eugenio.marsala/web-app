<?php
/* save query button
 * save results button
 * save spatial selection button
 * */
class results_buttons {
    function  print_buttons($params,$def) {
        global $BID;

        $modules = new modules();
        
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        
        $dlr = $modules->_include('download_restricted','downloadAccepted');
        
        # Hash generating
        $hash = genhash(6);
        $b = 0;

        $buttons = '<script>
        $(document).ready(function() {
            $("body").on("click","#get_obm_servers",function(){
                $.post("ajax",{"fetch_obm_servers":"servers"},
                    function(data) {
                        var j = JSON.parse(data);
                        var ddl = $("#share_obm_servers");
                        ddl.append("<option></option>"); 
                        for (k = 0; k < j.length; k++) {
                            if (j[k]["domain"]!="")
                                ddl.append("<option value=\'" + j[k][\'domain\']+ "\'>" + j[k][\'institute\'] + "</option>");
                        }
                }); 
            });
            $("body").on("change","#share_obm_servers",function(){

                var server = $("#share_obm_servers").val();
                if (server != "") { 
                    $.post("ajax",{"fetch_obm_projects":server},
                        function(data) {
                            var j = JSON.parse(data);
                            $("#share_obm_server_project").html("");
                            var ddl = $("#share_obm_server_project");   
                            for (k = 0; k < j.length; k++) {
                                if (j[k]["project_url"]!="")
                                    ddl.append("<option value=\'" + j[k][\'project_url\']+ "\'>" + j[k][\'project_table\'] + "</option>");
                            }
                            $("#share-data-send-request").show();
                    });
                }
            });
            $("body").on("click","#share-data-send-request",function(){
                $.post("ajax",{"shareQuery":$("#share_obm_server_project").val()},
                    function(data) {
                        var retval = jsendp(data);
                        if (retval["status"]=="error") {
                            alert(retval["message"]);
                        } else if (retval["status"]=="fail") {
                            alert(retval["data"]);
                        } else if (retval["status"]=="success") {
                            alert(retval["data"]);
                        }
                });
            });
        });
        
        </script>';

        $buttons .= "<div class='pure-form'><legend style='font-size:125%'><i class='fa fa-download'></i> ".t(str_download)."</legend><ul style='list-style-type:none' id='download-options'>";
        
        $url = $protocol."://".URL."/includes/";

        if ($modules->is_enabled('results_asCSV')) { 
            $file_name = sprintf("%s_%s_[%s].csv",PROJECTTABLE,date("Y-m-d_G:i"),$hash);
            $btn = ($dlr) 
                ? sprintf("<li style='padding-bottom:4px'><span style='font-family:monospace;border:1px solid gray;border-radius:3px;padding:0 2px 0 2px'>.CSV</span> <a href='%s%s' class='openDlrForm'>%s</a></li>",$url,"queries.php?csv=$file_name",str_requestCsvDownload)
                : sprintf("<li style='padding-bottom:4px'><span style='font-family:monospace;border:1px solid gray;border-radius:3px;padding:0 2px 0 2px'>.CSV</span> <a href='%s%s'>%s</a></li>",$url,"queries.php?csv=$file_name",str_downloadascsv);

            $buttons .= $btn;
            $b++;
        }

        if ($modules->is_enabled('results_asGPX')) { 
            $btnLabel = ($dlr) ? 'str_requestGpxDownload' : 'str_downloadasgpx' ;
            $file_name = sprintf("%s_%s_[%s].gpx",PROJECTTABLE,date("Y-m-d_G:i"),$hash);

            $btn = ($dlr) 
                ? sprintf("<li style='padding-bottom:4px'><span style='font-family:monospace;border:1px solid gray;border-radius:3px;padding:0 2px 0 2px'>.GPX</span> <a href='%s%s' class='openDlrForm'>%s</a></li>",$url,"queries.php?gpx=$file_name",str_requestGpxDownload)
                : sprintf("<li style='padding-bottom:4px'><span style='font-family:monospace;border:1px solid gray;border-radius:3px;padding:0 2px 0 2px'>.GPX</span> <a href='%s%s'>%s</a></li>",$url,"queries.php?gpx=$file_name",str_downloadasgpx);

            $buttons .= $btn; 
            $b++;
        }

        $buttons .= "</ul></div>";


        if ($dlr) {
            $buttons .= "<form id='dlr-message'><h5>".str_explain_your_request."</h5><textarea name='message' required></textarea><button type='submit'>Submit</button> </form>";
        }

        // if it is an LQ do not print save results, save query, save spatial options.
        if (isset($_SESSION['load_loadquery']))
            return $buttons;

        // Save Query Button for logined users
        if (isset($_SESSION['Tid'])) {
            if (isset($_SESSION['loaded_xml_content'])) unset($_SESSION['loaded_xml_content']);

            $buttons .= "<div class='pure-form'><legend style='font-size:125%'><i class='fa fa-bookmark'></i> ".t(str_bookmarks)."</legend><ul style='list-style-type:none' id='save-options'>";

            //ha meg vannak engedve a gombok és nem betöltött lekérdezésről van szó
            if ($def['buttons']!='off' and !isset($_SESSION['loaded_xml_content'])) {
                $buttons .= sprintf("<li style='padding-bottom:6px'><div>%s:<br><i class='fa fa-folder-o fa-lg'></i> <input id='saveqlabel-1' placeholder='%s'><button value='1' class='saveq button-success pure-button' style='vertical-align:baseline'>%s</button></div></li>",
                    t(str_savequery_results),str_saverlabel,str_save);
                $b++;
            }
            if ($def['buttons']!='off' and !isset($_SESSION['load_getreport'])) {
                $buttons .= sprintf("<li style='padding-bottom:6px'><div>%s:<br><i class='fa fa-search-plus fa-lg'></i> <input id='saveqlabel-2' placeholder='%s'><button value='2' class='saveq button-success pure-button' style='vertical-align:baseline'>%s</button></div></li>",
                    t(str_savequery),str_saveqlabel,str_save);
                $b++;
            }
            if (isset($_SESSION['selection_geometry_id']) and $modules->is_enabled('box_load_selection')) {
                $buttons .= sprintf("<li style='padding-bottom:0px'><div>%s:<br><i class='fa fa-map fa-lg'></i> <input id='saveqlabel-3' placeholder='%s'><button value='3' class='saveq button-success pure-button' style='vertical-align:baseline'>%s</button></div></li>",
                    t(str_save_selection),str_saveqlabel,str_save);
                $b++;
            }


            if (isset($_SESSION['load_getreport'])) unset($_SESSION['load_getreport']);
            
            $buttons .= "</ul></div>";

            if (has_access('master')) {
                // share options
                $buttons .= "<div class='pure-form'><legend style='font-size:125%'><i class='fa fa-share'></i> ".t(str_share)."</legend><ul style='list-style-type:none' id='save-options'>";
                $buttons .= sprintf('<li style="padding-bottom:6px"><div>%1$s:<br><button id="get_obm_servers" class="pure-button button-secondary">%4$s</button></li>
                    <li style="padding-bottom:6px"><i class="fa fa-database fa-lg"></i> <select id="share_obm_servers" placeholder="%2$s"></select></li>
                    <li style="padding-bottom:6px"><i class="fa fa-sitemap fa-lg"></i> <select id="share_obm_server_project" placeholder="%2$s"></select></li>
                    <li style="padding-bottom:6px"><button value="4" id="share-data-send-request" class="button-success pure-button" style="vertical-align:baseline;display:none">%3$s</button></div></li>',
                    t(str_share_data_with_other_openbiomaps_project),'',str_share_data_width_project,t(str_get_obm_servers_list));
                
                $buttons .= "</ul></div>";
            }

        } else {
            // Save Query Button for non logined users
            // ez mire való???
            // $buttons .= sprintf("<br><br><div id='sqform'><button id='saveq' class='button-success pure-button'><i class='fa fa-floppy-o'></i> %s</button> %s: <input id='saveqlabel' title='%s'></div>",t(str_savequery),str_label,str_saveqlabel);
        }

        if ($b) {
            return $buttons;
        }

        return;
    }
}
?>
