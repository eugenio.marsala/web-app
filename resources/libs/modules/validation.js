$(document).ready(function() {

    $(document).on('click','.save-job',async function(ev) {
        ev.preventDefault();
        const vm = $(this).data('vm');
        const parameters = {
            m: 'validation',
            vm: vm,
            action: 'save-job',
            data: $('#'+vm+'-form').serialize(),
        };

        const result = JSON.parse(await $.post('ajax',parameters));
        const message = $('#message-div');

        if (result.status == 'success') {
            message.html(result.data).addClass('message-success').show();
        }
        else if (result.status == 'error') {
            message.html(result.message).addClass('message-error').show();
        }

        
        setTimeout( function() {
            message.removeClass('message-error').removeClass('message-success').html('').hide();
        }, 3000);
          
    });
});



