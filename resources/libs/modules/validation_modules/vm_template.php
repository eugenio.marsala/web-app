<?php
class %%vm%% extends validation_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('%%vm%% initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        job_log("running %%vm%% module");
    }
}
?>
