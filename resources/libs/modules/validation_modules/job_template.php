<?php 
$path = $argv[1];
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/../local_vars.php.inc');
require_once($path.'/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

require_once($path.'/../includes/default_modules.php');
require_once($path.'/../includes/modules/validation.php');
require_once($path.'/../includes/modules/validation_modules/%%vm%%.php');

job_log('%%vm%% started');

if (!class_exists('%%vm%%')) {
    job_log("class %%vm%% does not exists\n");
    exit();
}
if (!method_exists('%%vm%%','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
call_user_func(['%%vm%%','run']);

?>
