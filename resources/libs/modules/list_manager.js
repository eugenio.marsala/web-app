$(document).ready(function() {
    $("body").on("click",".open-list-manager", function(e) {
        e.preventDefault();
        const lm = $("#list-manager");

        const tbl = $(this).data('table');
        const col = $(this).data('colname');

        lm.data('colname', col);
        lm.data('table', tbl);

        show_list('get_list');
    });

    $("body").on("click",".close-list-manager",function(e) {
        e.preventDefault();
        const lm = $("#list-manager");
        lm.hide();
    });

    $("body").on("click","#save-list", async function(e) {
        e.preventDefault();
        try {
            const lm = $("#list-manager");

            const col = lm.data('colname');
            const tbl = lm.data('table');

            const data = $('#list-manager-textarea').val();

            const resp = await $.post('ajax',{
                m: 'list_manager',
                action: 'save_list',
                data: data,
                colname: col,
                table: tbl
            });
        }
        catch (error) {
            console.log(error);
        }
    });

    $("body").on("click", "#generate-list", async function(e) {
        e.preventDefault();
        const lm = $("#list-manager");

        const tbl = lm.data('table');
        const col = lm.data('colname');

        show_list('generate_list');
    });


    $("body").on("click", ".lm-list-elements", async function() {

        try {
            $('#list-editor-help-box').hide();
            const wid = $('#list-editor').data('wid');
            const tbl = $('#form_table option:selected').val();

            const list_ul = $('#lm-list');
            list_ul.html('');

            if ($(this).val() == 'all') {
                clear_list();
                update_list('optionsTable',obj.project + '_terms');
                update_list('valueColumn','term');
                update_list('preFilterColumn',['data_table','subject']);
                update_list('preFilterValue',[tbl,wid]);
            }
            else if ($(this).val() == 'select') {

                const l = Object.keys(get_list('list'));
                clear_list();

                const list = await $.getJSON('ajax',{
                    m: 'list_manager',
                    action: 'get_list',
                    table: tbl,
                    colname: wid,
                });
                list_ul.append('<span><input type="checkbox" name="lm-list-element" class="lm-list-element" value=""> Empty line </span>');
                for (let k in list) {
                    if (!list.hasOwnProperty(k)) continue;
                    let checked = (l.indexOf(list[k]) >= 0) ? 'checked="true"' : '';
                    list_ul.append('<span><input type="checkbox" name="lm-list-element" class="lm-list-element" value="'+list[k]+'" '+ checked +'>'+list[k]+'</span>');
                };
                update_list('list',l);
            }
            else if ($(this).val() == 'standard-editor') {
                list_ul.html('');
                $('#list-editor-help-box').show();
            }
        }
        catch (error) {
            console.log(error);
        }
    });

    $("body").on("click",".lm-list-element", function () {
        let checked = $('#lm-list input:checked').map(function() {
            return this.value;
        }).get();
        update_list('list',checked);
    });

    $('body').on('list-editor-save-success list-editor-cancel',function() {
        $('#lm-list').html('');
        $('.lm-list-elements').prop('checked', false);
    });

    async function show_list(action) {
        try {
            const lm = $("#list-manager");
            const tbl = lm.data('table');
            const col = lm.data('colname');

            const data = await $.getJSON('ajax',{
                m: 'list_manager',
                action: action,
                colname: col,
                table: tbl
            });

            $('#list-manager-textarea').val(data.join("\n"));
            lm.show();
        }
        catch (error) {
            console.log(error);
        }
    }
});

