<?php
class text_filter {

    function print_box ($p) {
        global $ID,$BID;
        $t = new table_row_template();
        $s = new select_input_template();

        $qtable = $_SESSION['current_query_table'];

        $params = preg_split('/;/',$p);

        $pm_labels = array();

        if (isset($_SESSION['private_key'])) {
            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = openssl_random_pseudo_bytes($ivlen);
            $_SESSION['openssl_ivs']['text_filter_table'] = base64_encode($iv);
        }
        else {
            log_action('ERROR: _SESSION[private_key] not defined',__FILE__,__LINE__);
        }

        if (count($params)) {
            $t->cell(mb_convert_case(str_text_filters, MB_CASE_UPPER, "UTF-8"),2,'title center');
            $t->row();

            $first = 0;
            $options = array();
            // create check list of available filter options
            foreach ($params as $pm) {

                if (preg_match('/^_table_prefix=/',$pm)) {
                    continue;
                }

                if ($first==0) {
                    $first++;
                    continue;
                } else
                    $first++;

                $pm = preg_split('/:/',$pm);

                if ($pm[0]=='obm_uploader_name') $pm[0] = 'obm_uploader_user';

                $pm_labels[$pm[0]]=$pm[0];
                //log_action($pm_labels);
                $label = $pm[0];
                // should query the translation - automatism not work if the users give strange names
                $cmd = "SELECT ARRAY_TO_STRING(ARRAY_AGG(f_main_table),',') AS a FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
                #log_action($cmd);
                $res = pg_query($BID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    $tables = "'".implode("','", preg_split('/,/',$row['a']))."'";

                    $pm_column = $pm[0];
                    $pm_a = $pm[0];
                    $pm_split = preg_split('/\./',$pm[0]);
                    if (count($pm_split) > 1) {
                        $pm_column = $pm_split[1];
                        $pm_a = preg_replace('/\./','-',$pm[0]);
                    }

                    $cmd = sprintf("SELECT project_table,short_name FROM project_metaname WHERE project_table IN (%s) AND column_name='%s'",$tables,$pm_column);
                    $res = pg_query($BID,$cmd);
                    if (pg_num_rows($res)) {
                        $row = pg_fetch_assoc($res);
                        if (defined($row['short_name'])) {
                            $pm_labels[$pm_column] = constant($row['short_name']);
                            $label = constant($row['short_name']);
                        }
                        elseif (defined($pm_column)) {
                            // local languge definitions
                            $pm_labels[$pm_column] = constant($pm_column);
                            $label = constant($pm_column);
                        } else {
                            $pm_labels[$pm_column] = $pm_column;
                            $label = $row['short_name'];
                        }
                    } else {
                        if (defined($pm_column)) {

                            // local languge definitions
                            $pm_labels[$pm_column] = constant($pm_column);
                            $label = constant($pm_column);
                        } else {
                            if ($pm[0]=='obm_datum')
                                $label = str_datum_filter;
                            elseif ($pm[0]=='obm_taxon')
                                $label = str_taxon_filter;
                            elseif ($pm[0]=='obm_uploader_user')
                                $label = str_uploader;
                            elseif ($pm[0]=='obm_uploading_date')
                                $label = str_upload_date;
                            elseif ($pm[0]=='obm_observation_list')
                                $label = str_observation_list;

                            //log_action('No assignable column for...'.$pm[0],__FILE__,__LINE__);
                        }
                    }
                }

                $options[] = "<input type='checkbox' class='filter-sh-advopt' style='vertical-align:middle' value='$pm_a'> $label<br>";
            }
        }


        #$s->set_style("width:100%");
        #$s->new_menu('name_of_the_institute',institute,$ID);
        #$s->set_style("");
        #sprintf("<tr><td class='title' style='vertical-align:top'>%s: </td><td colspan=3>%s</td></tr>",$s->name,$s->menu);
        $_SESSION['qform'] = array();
        $first = 0;
        foreach($params as $pm) {
            //debug($pm);

            $pm = preg_split('/:/',$pm);

            if ($pm[0]=='obm_taxon') {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $t->cell(str_taxon_name.":",1,'title');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='taxon_sim' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_taxon $def_hidden");
                $t->cell("<div id='tsellist'>
                <span id='taxon-search-settings' class='pure-button button-href' style='margin-left:5px'><i class='fa fa-cog fa-lg'></i></span>
                <div style='position:relative'><div id='taxon-search-settings-box' style='display:none;border:1px solid #acacac;padding:2px 20px 2px 2px;margin:2px;position:absolute;left:2px;top:0px;background-color:white'>
                    <input type='radio' id='onematch' name='match' style='vertical-align:bottom'> ".str_one_match."<br>
                    <input type='radio' id='allmatch' checked name='match' style='vertical-align:bottom'> ".str_all_match."</div>
                </div>",2,'title');
                $t->row("text-options_obm_taxon $def_hidden");
                $first++;
            } elseif($pm[0]=='obm_datum') {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $t->cell(str_exact_date,1,'title');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_date' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_date_period,2,'title');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(t(str_from),1,'content');
                $t->cell(t(str_to),1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_from' class='qf' value='' autocomplete='off'>",1,'content');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_to' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_range_period,2,'title');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(t(str_from),1,'content');
                $t->cell(t(str_to),1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_mfrom' class='qf' value='' autocomplete='off'>",1,'content');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_mto' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
            } elseif ($pm[0]=='obm_uploading_date') {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $t->cell(str_upload_date,1,'title');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='obm_uploading_date' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_uploading_date $def_hidden");
            } elseif ($pm[0]=='obm_uploader_user' || $pm[0]=='obm_uploader_name') {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $upl_user_box = "<select id='obm_uploader_user' class='qf'><option></option>";
                $cmd = sprintf("SELECT uploader_id, string_agg(DISTINCT uploader_name, ', ') AS un FROM system.uploadings WHERE project_table = '%s' GROUP BY uploader_id;", $qtable);
                //$cmd = "SELECT DISTINCT uploader_id,uploader_name AS un FROM system.uploadings u LEFT JOIN ".PROJECTTABLE." d ON (d.obm_uploading_id=u.id) ORDER BY uploader_name";
                $res = pg_query($ID,$cmd);
                $uid = array();
                while($row = pg_fetch_assoc($res)) {
                    if (in_array($row['uploader_id'],$uid)) continue;
                    $uid[] = $row['uploader_id'];
                    $upl_user_box .= "<option value='{$row['uploader_id']}'>{$row['un']}</option>";
                }
                $upl_user_box .="</select>";

                //array_splice($params,$i,1);
                $t->cell(str_uploader,1,'title');
                $t->cell($upl_user_box,1,'content');
                $t->row("text-options_obm_uploader_user $def_hidden");
            } elseif ($pm[0]=='obm_files_id') {
                // Photo filter
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $pm_column = $pm[0];
                $pm_a = $pm[0];
                $pm_split = preg_split('/\./',$pm[0]);
                if (count($pm_split) > 1) {
                    $pm_column = $pm_split[1];
                    $pm_a = preg_replace('/\./','-',$pm_a);
                }

                $label = $pm[0];
                if (isset($pm_labels[$pm_column]))
                    $label = $pm_labels[$pm_column];
                $t->cell($label,1,'title');

                $t->cell('<button class="qf pure-button button-passive button-small" type="button" id="hasphoto"><i class="fa fa-lg fa-toggle-off"></i></button>',1,'content');

                $t->row("text-options_$pm_a $def_hidden");
            }
            elseif ($pm[0] == 'obm_search') {
                /* Multiple search: taxon & observers
                 *
                 * */

                $res = pg_query($ID, sprintf('SELECT DISTINCT subject FROM %1$s_search WHERE data_table = %2$s;',PROJECTTABLE, quote($qtable)));
                $subjects = pg_fetch_all($res);

                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $t->cell(str_multisearch_filter,2,'title center','background-color:azure');
                $t->row("text-options_obm_search $def_hidden");
                //$t->cell(str_searchstring,2,'title');
                foreach ($subjects as $sub) {
                    $t->cell("<div id='obm_search_{$sub['subject']}' class='qf divInput'><button id='{$sub['subject']}_relation' style='float:right' class='search-relation pure-button pure-button-primary hidden'>OR</button></div>",2,'content');
                    $t->row("text-options_obm_search $def_hidden");
                }

                $t->row("text-options_obm_search $def_hidden");
                $t->cell("<input style='width:100%;font-size:150%' type='text' id='search' class='multi-autocomplete' value='' data-ids='' autocomplete='off'>",2,'content');
                $t->row("text-options_obm_search $def_hidden");
/*                $t->row("text-options_obm_search $def_hidden");
                $t->cell("<input type='radio' id='and' name='obs_match' style='vertical-align:bottom'>AND
                    <input type='radio' id='or' checked name='obs_match' style='vertical-align:bottom'>OR",2,'content');*/
                $first++;

/*            } elseif($pm[0]=='obm_datum') {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $t->cell(str_datum_filter,2,'title center');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_exact_date,1,'title');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_date' class='qf' value=''>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_date_period,2,'title');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(t(str_from),1,'content');
                $t->cell(t(str_to),1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_from' class='qf' value=''>",1,'content');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_to' class='qf' value=''>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_range_period,2,'title');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(t(str_from),1,'content');
                $t->cell(t(str_to),1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_mfrom' class='qf' value=''>",1,'content');
                $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_mto' class='qf' value=''>",1,'content');
                $t->row("text-options_obm_datum $def_hidden"); */
            }
            elseif ($pm[0] == 'obm_observation_list') {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $_box = "<select id='obm_observation_list' class='qf'><option></option>";
 	
                $cmd = sprintf("SELECT id, oidl, uploading_id, obsstart, obsend, nulllist, started_at, finished_at, obsend-obsstart as duration FROM %s_observation_list WHERE obsstart IS NOT NULL ORDER BY obsstart", PROJECTTABLE);
                $res = pg_query($ID,$cmd);
                while($row = pg_fetch_assoc($res)) {
                    $_box .= "<option value='{$row['oidl']}'>{$row['obsstart']} {$row['duration']}</option>";
                }
                $_box .="</select>";

                //array_splice($params,$i,1);
                $t->cell(str_observation_list,1,'title');
                $t->cell($_box,1,'content');
                $t->row("text-options_obm_observation_list $def_hidden");

            }
            else {
                if ($first==0)
                    $def_hidden = '';
                else
                    $def_hidden = 'def-hidden';
                $first++;

                $format = '';

                //processing filters
                //d.egyedszam:_boolen
                //d.dt_to:nested
                $filter = '';
                if (isset($pm[1]) and $pm[1]!='') {
                    $filter = $pm[1];
                }

                // processing controls
                // d.szamossag::multi
                // default control values
                $s->multi = "multiple='multiple'";
                $s->autocomplete = false;
                $s->colour_rings = false;
                $s->colour_rings_options = '';
                $s->access_class = 'qf nested';
                if (isset($pm[2]) and $pm[2]!='') {
                    if ($pm[2]=='single')
                        $s->multi = '';
                    elseif ($pm[2] == 'autocomplete') {
                        $s->autocomplete = true;
                        $s->access_class = 'qf';
                    }
                    elseif ($pm[2] == 'colour_rings') {
                        $s->colour_rings = true;
                        $s->access_class = 'qf';
                        if (isset($pm[3]))
                            $s->colour_rings_options = $pm[3];
                    }
                }

                // processing column name
                $pm_column = $pm[0];
                $pm_a = $pm[0];
                $pm_split = preg_split('/\./',$pm[0]);
                if (count($pm_split) > 1) {
                    $pm_column = $pm_split[1];
                    $pm_a = preg_replace('/\./','-',$pm_a);
                }

                $label = $pm[0];
                $cmd = sprintf("SELECT project_table,short_name FROM project_metaname WHERE project_table IN (%s) AND column_name='%s'",$tables,$pm_column);
                $res = pg_query($BID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    if (defined($row['short_name'])) {
                        $label = constant($row['short_name']);
                    }
                    elseif (defined($pm_column)) {
                        $label = constant($pm_column);
                    } else {
                        $label = $row['short_name'];
                    }
                } elseif (defined($pm_column)) {
                        $label = constant($pm_column);
                } else {
                    if (isset($pm_labels[$pm_column]))
                        $label = $pm_labels[$pm_column];
                }

                //betöltött select mező cimkéje
                $t->cell($label,1,'title');

                //betöltött select mező
                if ($filter!='') {
                    if (preg_match('/nested\((.+)\)/',$filter,$m)) {
                        $element = preg_replace('/\./','-',$m[1]);
                        $s->tdata = array('nested_column'=>"$m[1]",'nested_element'=>"$element",'etr'=>'');
                    }
                }
                $s->new_menu($pm[0],$filter);

                //betöltött szelkció tartalma
                $t->cell($s->menu,1,'content');

                //a teljes sor létrehozása
                $t->row("text-options_$pm_a $def_hidden");
            }
        }
            if (count($options)) {
                $options = implode('',$options);
                $t->cell("<div><div id='filter-options-toggle' style='border-bottom:1px solid #cacaca;height:30px;margin-bottom:1px;cursor:pointer'><span style='display:inline-block;float:left;padding-top:6px;font-weight:bold'>".t(str_more_options).":</span><button class='pure-button button-secondary button-small' style='float:right;padding-left:20px;padding-right:20px'><i class='fa fa-lg fa-plus'></i></button></div><div id='filter-options-box' class='pure-u-1' style='height:100px;overflow-y:scroll;display:none'>$options</div></div>",2,'');
            }
            $t->row();

        $html = sprintf("<input type='hidden' id='taxon_id'>
            <table class='mapfb'>
            %s
            </table>
            <div style='font-size:0.8em' id='responseText'></div>",$t->printOut());
        return $html;

    }
    function print_js($params) {

        $js = (file_exists(getenv('OB_LIB_DIR').'modules/text_filter.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/text_filter.js') : '';
        return $js;
    }

    function ajax($params,$request) {
        global $ID;
        /* mapview, filterbox
         * get dynamic list of filtered elements from a table
         * return JSON array of filtered elements
         * */
        if (isset($request['qflist'])) {
            $term = preg_replace('/;/','',$request['term']);
            $id = preg_replace('/;/','',$request['id']);
            $schema = 'public';
            $column = '';
            $table = '';

            if (isset($request['etr']) and $request['etr']!='' and isset($_SESSION['private_key'])) {
                $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
                $table_reference = openssl_decrypt(base64_decode($request['etr']), "AES-128-CBC", $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
                if ($table_reference!='') {
                    list($schema,$table) = preg_split('/\./',$table_reference);
                    $column = $id;
                }
                else
                    // hogyan lehet elkapni a lejárt lapot már javascripttel?
                    // be kellene tenni egy encryptált stringet a main.js.php-val a lap tetejére, mint page_code és session
                    // változóként is benn tartani, ha nem egyezik, akkor kötelezően reloadolni a lapot??
                    echo common_message('error','Page expired');
            } else {
                //tképp ez is jobb lenne encryptálva...
                $sqla = sql_aliases(1);
                $qta = $sqla['qtable_alias'];
                $qt = $sqla['qtable'];
                $table = '';
                $column = '';

                if (preg_match('/-/',$id))
                    list($table,$column) = preg_split('/-/',$id);
                else
                    $table = $id;

                if ($column=='') {
                    $column = $table;
                    $table = PROJECTTABLE;
                }

                if ($table==$qta)
                    $table = $qt;
                //else
                //    log_action("table mismatch: $table $qta",__FILE__,__LINE__);
            }

            $speedup_filter = '';
            // It can be slow...
            // much more faster with and indexed first letter filter!!!!!
            // ALTER TABLE .... ADD COLUMN obm_first_letter_filter character(1);
            // UPDATE ... SET obm_first_letter_filter=lower(substring(---- from 1 for 1))
            // EXPLAIN ANALYZE SELECT DISTINCT "telepules" "telepules" FROM hrsz_terkepek."kuvet_bevet_2018" WHERE obm_first_letter_filter='g' AND telepules ILIKE 'gárd%' ORDER BY telepules LIMIT 20
            if (isset($request['speedup_filter']) and $request['speedup_filter']=='on'){
		mb_internal_encoding("UTF-8");
                $speedup_filter = sprintf('obm_first_letter_filter=%s AND ',quote(mb_strtolower(mb_substr($term,0,1))));
	    }

            // safe regexp characters in term
            $term = preg_replace('/\|/','\|',$term);
            $term = preg_replace('/\[/','\[',$term);
            $term = preg_replace('/\]/','\]',$term);

            $cmd = sprintf('SELECT DISTINCT "%1$s" "%1$s",COUNT(%1$s) AS count_ FROM %5$s."%2$s" WHERE %6$s (CAST("%1$s" AS TEXT) SIMILAR TO \'%3$s%4$s\') OR (LOWER(CAST("%1$s" AS TEXT)) SIMILAR TO \'%3$s%4$s\') GROUP BY "%1$s" ORDER BY %1$s LIMIT 15',$column,$table,$term,'%',$schema,$speedup_filter);
            $res = pg_query($ID,$cmd);
debug($cmd);

            if (pg_num_rows($res)) {
                $x = pg_fetch_all($res);
                $data = array_column($x,$column);
                $coun = array_column($x,'count_');
                $combined = array_map(function($a, $b) { return $a . ' (' . $b . ')'; }, $data, $coun);
                #[ { label: "Choice1", value: "value1" }, ... ]
                #$dc = array_combine($combined, $data);
                $combined = array_map(function($a, $b) { return array('label'=>$a,'value'=>$b); }, $combined, $data);

                echo common_message('ok',json_encode($combined));
                #echo common_message('ok',json_encode(array_column($x,$column)));
            } else {
                echo common_message('error','No matching string could be found.');
                if (pg_last_error($ID)) {
                    log_action(pg_last_error($ID),__FILE__,__LINE__);
                    log_action($cmd,__FILE__,__LINE__);
                }
            }

            exit;
        }
        /* general dynamic select/autocomplete menu
         * etr: Encrypted source table reference
         * id: filtered column name
         * term: filter term
         *
         * return a complete menu
         * */
        if (isset($request['create_dynamic_menu'])) {

            $table_reference = '';
            if (isset($request['etr']) and isset($_SESSION['private_key'])) {
                $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
                $table_reference = openssl_decrypt(base64_decode($request['etr']), "AES-128-CBC", $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            }

            $s = new select_input_template();
            if (isset($request['access_class']))
                $s->access_class = $request['access_class'];
            $s->destination_table = $table_reference;
            $s->filter_column = $request['filter_column'];
            $s->filter_value = array($request['filter_term']);

            if (isset($request['all_options']) and $request['all_options']=='on')
                $s->all_options = 'on'; // <select><options> query without DISTINCT

            $s->onlyoptions = 'on';
            if (isset($request['nested_element']))
                $s->id = preg_replace('/\./','-',$request['nested_element']);

            if (isset($request['nested_column'])) {
                $s->new_menu($request['nested_column']);
                echo $s->menu;
            }
            exit;
        }
    }
}

class query_builder {
    /*       params:
     *           mc: matched column, the column name, which will be used in the query string
     *     post_val:
     * custom_table:
     *
     * */
    function assemble_where($params,$mc,$post_val,$custom_table) {
        global $ID;
        $w = '';
        $specials = array('exact_from','exact_mfrom','exact_mto','exact_to','exact_date',
            'obm_uploading_date','obm_uploader_user','obm_uploader_name','obm_id','obm_uploading_id',
            'hasphoto','obm_uploading_id_mine','obm_observation_list');

        // ez drága így lekérdezni, ha nincs _search kérés!!
        // modulba kell tenni, hogy van-e egyáltalán _search tábla!
        /*$res = pg_query($ID, sprintf('SELECT DISTINCT subject FROM %1$s_search;',PROJECTTABLE));
        $subjects = pg_fetch_all($res);

        foreach ($subjects as $s) {
            $specials[] = "obm_search_{$s['subject']}";
        }*/

        $sqla = sql_aliases(1);

        if ($custom_table =='')
            $query_table = $_SESSION['current_query_table'];
        else
            $query_table = $custom_table;

        $st_col = st_col($query_table,'array');

        //deleting specials from dbcolist
        $dbcolist = dbcolist('array',$query_table);
        foreach ( $specials as $del ) {
            unset($dbcolist[$del]);
        }

        // table prefix module param processing
        $prefix = "";
        $pm_split = preg_split('/-/',$mc);
        if (count($pm_split) > 1) {
            $mc = $pm_split[1];
            $prefix = $pm_split[0].'.';
        }

        if (array_key_exists($mc,$dbcolist)) {
            $ja = json_decode($post_val,true);
            // if post_val not json, just single value
            if (!count($ja))
                $ja = $post_val;

            if (is_array( $ja ))
                $v = implode(',',array_map('quote',$ja));
            else
                $v = quote($ja);

            if($v=='NULL') return;

            $w = $prefix."\"$mc\" IN ($v)";

            return $w;
        }

        // special search for date ranges
        if (in_array($mc,$specials)) {
            /*qids_exact_date	["2016-01-13"]
              qids_exact_from	["2016-01-13"]
              qids_exact_mfrom ["01-19"]
              qids_exact_mto	 ["01-28"]
              qids_exact_to	["2016-01-22"] */

            //ez megint egy erőltett dolog és nincs dokumentálva, hogy csak az első oszlopra működik!!!
            //MEG KELL VÁLTOZTATNI!!!!
            $DATE_C = $st_col['DATE_C'][0];

            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$DATE_C,$m))
            //    $DATE_C = $m[2];


            if (is_object($post_val)) {
                $ja = json_decode($post_val,true);
            } else {
                if (preg_match('/^\[["\d]/',$post_val))
                    $ja = json_decode($post_val,true);
                else
                    $ja = $post_val;
            }

            if (is_array( $ja )) {
                $v = implode(',',array_map('quote',$ja));
                $test_string = implode(',',$ja);
                if ($test_string == '') $test_string = 'NULL';
            } else {
                $v = quote($ja);
                if ($ja == '') $test_string = 'NULL';
                else
                    $test_string = $ja;
            }

            if($v==NULL) {
                //??
                if (isset($_SESSION['exact_date_from']) and $mc == 'exact_to') $v = sprintf("'%s'",date("Y-m-d"));
                elseif (isset($_SESSION['exact_mdate_from']) and $mc == 'exact_mto') $v = sprintf("'%s'",date("m-d"));
                else
                    return;
            }
            if ($mc == 'exact_date' and $test_string!='NULL' and $DATE_C!='') {
                $w = "$v=date_trunc('day', \"$DATE_C\")";
            }
            elseif ($mc == 'exact_from' and $test_string!='NULL') {
                $_SESSION['exact_date_from'] = $v;
                #return with subquery
                #$w = "($DATE_C > $v)";
                #return $w;
                return;
            }
            elseif ($mc == 'exact_to' and isset($_SESSION['exact_date_from']) and $_SESSION['exact_date_from']!='NULL' and $DATE_C!='') {
                #overwrite subquery with full date query
                #... ???? ...
                #
                $ef = $_SESSION['exact_date_from'];
                unset($_SESSION['exact_date_from']);
                // set today as exact_date_to if it was not set
                if ($test_string=='NULL')
                    $v = quote(date("Y-m-d"));
                $w = "($DATE_C BETWEEN $ef AND $v)";
            }
            elseif ($mc == 'exact_mfrom' and $test_string!='NULL') {
                $_SESSION['exact_mdate_from'] = $v;
                return;
            }
            elseif ($mc == 'exact_mto' and $test_string!='NULL' and $DATE_C!='') {
                $start = $_SESSION['exact_mdate_from'];
                unset($_SESSION['exact_mdate_from']);
                $m = array();
                if(preg_match('/([0-9]{2})-([0-9]{2})/',$start,$m)) {
                    $start_month = $m[1];
                    $start_day = $m[2];
                    $m = array();
                    if (preg_match('/([0-9]{2})-([0-9]{2})/',$v,$m)){
                        $end_month = $m[1];
                        $end_day = $m[2];
                        $month = array();
                        if ($start_month<=$end_month and $start_day<=$end_day)
                            $range = range($start_month,$end_month);
                        else {
                            $range = array_merge(range($start_month,12),range(1,$end_month));
                        }

                        $nc = count($range);
                        $n = 0;
                        foreach($range as $i) {
                            $day_filter = '';
                            /*(extract(month from "datum_tol")=3 AND extract(day from "datum_tol")>2) OR
                            (extract(month from "datum_tol")=4) OR
                            (extract(month from "datum_tol")=5 AND extract(day from "datum_tol")<5)*/
                            if ($i==$start_month and $nc>1 and !$n) {
                                $day_filter = " AND extract(day from \"$DATE_C\")>=$start_day";
                            } elseif($i==$end_month and $nc>1 and $n==$nc-1) {
                                $day_filter = " AND extract(day from \"$DATE_C\")<=$end_day";
                            } elseif ($nc == 1 ) {
                                $day_filter = " AND extract(day from \"$DATE_C\")>=$start_day AND extract(day from \"$DATE_C\")<=$end_day";
                            }
                            $month[] = "(extract(month from \"$DATE_C\")=$i $day_filter)";
                            $n++;
                        }
                        $w = "(".implode(" OR ",$month).")";
                    }
                }
            } elseif ($mc == 'obm_uploading_date' and $test_string!='NULL') {
                //only works if in the map file and the query_layer's query the uploading is LEFT JOINED
                $w = "date_trunc('day',uploading_date)=$v";
                $_SESSION['uploading_join_filter'] = 1;
            } elseif (($mc == 'obm_uploader_user' or $mc == 'obm_uploader_name') and $test_string!='NULL') {
                //only works if in the map file and the query_layer's query the uploading is LEFT JOINED
                if (strlen($test_string)==32) {
                    $userdata = new userdata($test_string,'hash');
                    $v = quote($userdata->get_roleid());
                }

                $w = "uploader_id=$v";
                $_SESSION['uploading_join_filter'] = 1;
            } elseif ($mc == 'obm_uploading_id' and $test_string!='NULL') {

                if ($test_string == 'last_data') {
                    $cmd = sprintf("SELECT id FROM system.uploadings WHERE project_table=%s ORDER BY uploading_date DESC LIMIT 1",quote($_SESSION['current_query_table']));
                    $res = pg_query($ID,$cmd);
                    if (!pg_num_rows($res)) {
                        //log_action($cmd,__FILE__,__LINE__);
                        $v = 0;
                    } else {
                        $row = pg_fetch_assoc($res);
                        $v = $row['id'];
                    }
                } else {
                    //overwrite qtable_alias based on uploading_id's table
                    $cmd = sprintf("SELECT project_table FROM system.uploadings WHERE project='%s' AND id=%s",PROJECTTABLE,$v);
                    $res = pg_query($ID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $sqla = sql_aliases($row['project_table']);
                    $qtable_alias = $sqla['qtable_alias'];
                }
                $w = $sqla['qtable_alias'].".obm_uploading_id=$v";

            } elseif ($mc == 'obm_uploading_id_mine' and $test_string!='NULL') {
                if ($test_string == 'last_data') {
                    $cmd = sprintf("SELECT id FROM system.uploadings WHERE project_table=%s AND uploader_id=%d ORDER BY uploading_date DESC LIMIT 1",quote($_SESSION['current_query_table']),$_SESSION['Trole_id']);
                    $res = pg_query($ID,$cmd);
                    if (!pg_num_rows($res)) {
                        //log_action($cmd,__FILE__,__LINE__);
                        $v = 0;
                    } else {
                        $row = pg_fetch_assoc($res);
                        $v = $row['id'];
                    }
                }
                $w = $sqla['qtable_alias'].".obm_uploading_id=$v";
            } elseif ($mc == 'obm_id' and $test_string!='NULL') {

                if ($test_string == 'last_data') {

                    $m = new modules();
                    $n = 10;
                    $num = $m->get_params('box_load_last_data');
                    if ($num!='' and is_numeric($num)) $n = $num;

                    $table = $_SESSION['current_query_table'];
                    $cmd = "SELECT ARRAY_TO_STRING( array( SELECT obm_id FROM $table ORDER BY obm_id DESC LIMIT $n ),',') as id";
                    $res = pg_query($ID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $v = $row['id'];
                }
                $qtable_alias = $sqla['qtable_alias'];
                //obm_id query : map apply text query over spatial query
                $w = sprintf("$qtable_alias.\"obm_id\" IN (%s)",$v);

            } elseif ($mc == 'hasphoto' and $test_string!='NULL') {
                $w = 'obm_files_id IS NOT NULL';

            } elseif (substr($mc,0,10) == 'obm_search' and $test_string != 'NULL') {
                if (count($ja) == 1) {
                    $w = '';
                }
                else {
                    $relation = ($ja[0] == 'OR') ? '&&' : '@>' ;
                    $v = substr($v,strpos($v,',') + 1);
                    $subject = substr($mc,11);
                    // faj_ids DOES NOT EXISTS!!!!
                    $w = "{$subject}_ids::text[] $relation ARRAY[$v]";
                }
            } elseif ($mc == 'obm_observation_list' and $test_string!='NULL') {
                $w = $sqla['qtable_alias'].".obm_observation_list_id=$v";
                //$w = "oidl=$v";
            }


        }

        return $w;

    }
}

?>
