<?php
class box_load_coord {
    function print_box ($params='') {

        if ($params=='')
            $params = "WGS84 (epsg 4326)=4326";
        $p = preg_split('/;/',$params);
        $epsg_list = array();
        foreach ($p as $pm) {
            $list = preg_split('/[=:]/',$pm);
            if (isset($list[1]))
                $epsg_list[] = $list[0].'::'.$list[1];
            else
                $epsg_list[] = $list[0].'::'.$list[0];

        }
        
        $options = selected_option($epsg_list,'');
        $sout = "<select class='' id='epsg_set'>$options</select> ";
        $sout .= "Lon (x): <input type='text' size='6' style='vertical-align:middle;max-height:26px;border:1px solid lightgray;border-radius:3px;padding:4px;' id='coord_query_x'> Lat (y): <input type='text' size=6 style='vertical-align:middle;max-height:26px;border:1px solid lightgray;border-radius:3px;padding:4px;' id='coord_query_y'>";
        $sout .= "<button id='coord_query' class='button-gray button-small pure-button'><i class='fa-map-pin fa-lg fa'></i></button>";
        return sprintf("%s",$sout);
    }

    function print_js ($params) {
        echo '
$(document).ready(function() {
    $("#coord_query").click(function(){
        var X = $("#coord_query_x").val();var Y = $("#coord_query_y").val();
        //var p = new OpenLayers.Geometry.Point(X, Y); 
        var p = new OpenLayers.LonLat(X, Y);
        p.transform(new OpenLayers.Projection("EPSG:"+$("#epsg_set").val()), map.getProjectionObject());
        var pointFeature = new OpenLayers.Feature.Vector(p, null, highlight_style);
        var currentZoom = map.getZoom();

        // A buffer méretének 2x-re zoomolunk
        //if ($(\'#buffer\').length>0){b=$("#buffer").val();}else{b=obj.buffer;}
        //var reader = new jsts.io.WKTReader();
        //var input = reader.read(new String(p));
        //if (b==\'\'||b==0){b=500;}
        //var buffer = input.buffer(b*10);
        //var parser = new jsts.io.OpenLayersParser();
        //buffer = parser.write(buffer);
        //var Zoomfeature = new OpenLayers.Feature.Vector(buffer, null, null);
        //map.zoomToExtent(Zoomfeature.geometry.getBounds(), closest=false);
        //draw marker icon 
        markerLayer.clearMarkers();
        var size = new OpenLayers.Size(21,25);
        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
        var icon = new OpenLayers.Icon(\'js/img/marker.png\',size,offset);
        markerLayer.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(p.lon,p.lat),icon));
        map.setCenter(new OpenLayers.LonLat(p.lon, p.lat), currentZoom);
    });
    $("body").on("mousewheel DOMMouseScroll","#coord_query_y", function(event){
        //if (!$(this).is(":focus")) return;
        event.preventDefault();
        var n=+$(this).val();
        if ($("#epsg_set").val()=="4326") {
            var i=0.0001;
        }
        else {
            var i=1;
        }
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            // scroll up
            n=eval(n+i);
            if ($("#epsg_set").val()=="4326")  n=n.toFixed(4);
            $(this).val(n)
            $("#coord_query_y").val(n);
        }
        else {
            // scroll down
            n=eval(n-i)
            if ($("#epsg_set").val()=="4326")  n=n.toFixed(4);
            $(this).val(n)
            $("#coord_query_y").val(n);
        }
    });
    $("body").on("mousewheel DOMMouseScroll","#coord_query_x", function(event){
        //if (!$(this).is(":focus")) return;
        event.preventDefault();
        var n=+$(this).val();
        if ($("#epsg_set").val()=="4326") {
            var i=0.0001;
        }
        else {
            var i=1;
        }
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            // scroll up
            n=eval(n+i);
            if ($("#epsg_set").val()=="4326")  n=n.toFixed(4);
            $(this).val(n)
            $("#coord_query_x").val(n);
        }
        else {
            // scroll down
            n=eval(n-i)
            if ($("#epsg_set").val()=="4326")  n=n.toFixed(4);
            $(this).val(n)
            $("#coord_query_x").val(n);
        }
    });

});
';
    }
}
?>
