<?php
class box_custom {
    
    private $path;
    
    function __construct($private_path)
    {
        $this->path = $private_path;
    }
    function print_box ($p='') {
        $params = preg_split('/;/',$p);
        $pmi = array();
        $boxes = "";
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);

            if(file_exists($this->path."$c[0].php")) {
                $e = '';
                if ( php_check_syntax($this->path."$c[0].php",$e)) {
                    include_once($this->path."$c[0].php");
                    $classname=$c[0].'_Class';
                    $bc = new $classname();
                    $args = isset($c1) ? $c[1] : "";
                    $boxes .= $bc->print_box($args);
                }
                if (!preg_match('/^No syntax errors detected/',$e))
                    log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
            }
        }
        return $boxes;
    }
    function print_js ($p='') {
        $params = preg_split('/;/',$p);
        $pmi = array();
        $js = "";
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);
            if(file_exists($this->path."$c[0].php")) {
                $e = '';
                if ( php_check_syntax($this->path."$c[0].php",$e)) {
                    include_once($this->path."$c[0].php");
                    $classname=$c[0].'_Class';
                    $bc = new $classname();
                    $args = isset($c1) ? $c[1] : "";
                    $js .= $bc->print_js($args);
                }
                if (!preg_match('/^No syntax errors detected/',$e))
                    log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
            }
        }
        return $js;
    }
}
?>
