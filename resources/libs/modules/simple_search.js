$(document).ready(function() {
    $('.show-advanced-filters').on('click',function(ev) {
        console.log('click');
        $('.advanced-filters').show();
        $('.simple-search').hide();
    });


    $( function() {
        function split( val, regexp = /,\s*/ ) {
            return val.split( regexp );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( function() {
            $.widget( "custom.catcomplete", $.ui.autocomplete, {
                _create: function() {
                    this._super();
                    this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                },
                _renderMenu: function( ul, items ) {
                    var that = this,
                        currentCategory = "";
                    $.each( items, function( index, item ) {
                        var li;
                        if ( item.category != currentCategory ) {
                            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                            currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );
                        if ( item.category ) {
                            li.attr( "aria-label", item.category + " : " + item.label );
                        }
                    });
                }
            });

            $( ".multi-autocomplete" ).catcomplete({
                delay: 0,
                source: function( request, response ) {
                    $.getJSON( "ajax", {
                        m: "simple_search",
                        type: "search",
                        term: extractLast( request.term )
                    }, response );
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    var ids = split( $(this).data('ids'),/,/);
                    
                    // remove the current input
                    terms.pop();
                    
                    // add the selected item
                    terms.push( ui.item.value );
                    ids.push( ui.item.id );

                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );

                    var searchBox = $('#obm_search_' + ui.item.category);
                    searchBox.append('<div class="'+ui.item.category+' search-badge pure-button" data-id='+ui.item.id+'>'+ui.item.value+'<span>x</span></div>');
                    searchBox.find('.search-relation').removeClass('hidden');
                    this.value = '';
                    return false;
                }
            });
            $('.search-relation').click(function(){
                  $(this).toggleText('AND', 'OR');
            })
        } );
    } );
    // text-filter2 
    // is this called ever?
    $("#mapfilters").on("click",".search-badge span",function(ev) {
        $(this).parent().remove();
    });
});
