<?php
/**
 * Download restricted module
 *
 * This module replaces the CSV and GPX download buttons with a form, asking for the
 * purpose of the data download. It has an admin page, where the operators can approve
 * or reject the requests.
 *
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 * @version 1.0
 */
class download_restricted {
    var $error = '';
    var $retval;
    var $project_email;
    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        $res = pg_query($BID,"SELECT email FROM projects WHERE project_table='".PROJECTTABLE."'");
        $row = pg_fetch_assoc($res);
        $this->project_email = $row['email'];

        if ($action)
            $this->retval = $this->$action($params,$pa);

    }

    public function getMenuItem() {
        return ['label' => str_download_requests, 'url' => 'download_restricted' ];
    }

    public function profileItem() {
        
        $em = sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='#tab_basic' data-url='includes/project_admin.php?options=download_restricted' class='profilelink'>".str_download_requests."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink("user_interface.html#download-requests",str_what_are_download_requests));

        return [
            'label' => str_download_requests,
            'fa' => 'fa-download',
            'item' => $em
        ];
    }


    public function adminPage() {

        global $BID, $ID;

        $out = '<div id="download_requests">';
        $where = (!has_access('master')) ? sprintf(" AND user_id = '%s'",$_SESSION['Tid']) : '';

        $cmd = sprintf("SELECT id, filename, user_id, downloaded, status, message, valid_until::date as valid_until FROM download_requests r WHERE project = '%s'%s ORDER BY requested DESC;",PROJECTTABLE, $where);
        if ($res = $this->query($ID, $cmd)) {
            $requests_unfiltered = [];
            $requsters = [];
            while ($row = pg_fetch_assoc($res)) {
                $requests_unfiltered[] = $row;
                $requsters[] = $row['user_id'];
            }
        }


        $cmd = sprintf("SELECT id, username, email FROM users WHERE id IN (%s);",implode(',',$requsters));
        if ($res = $this->query($BID,$cmd)) {
            $users = [];
            while ($row = pg_fetch_assoc($res)) {
                $users[$row['id']] = $row;
            }
        }

        $requests = array_filter($requests_unfiltered, function($req) use ($users) {
            return (isset($users[$req['user_id']]));
        });




// creating download requests table
        
        $tbl = new createTable();
        $tbl->def(['tid'=>'pendingRequests','tclass'=>'resultstable']);
        $tbl->addHeader(array('id', str_filename, str_requested_by, str_message, str_actions));
        foreach ($requests as $r) {
            $userExists = (isset($users[$r['user_id']])) ;
            if ($r['status'] == 'pending') {
                if (!has_access('master')) {
                    $approveBtn = '';
                    $rejectBtn = '';
                    $fileLink = $r['filename'];
                }
                else {
                    $approveBtn = sprintf('<a class="button-href pure-button approve" data-request_id="%2$s"><i class="fa fa-smile"></i> Approve</a>',URL,$r['id']);
                    $rejectBtn = sprintf('<a class="button-href pure-button reject" data-request_id="%2$s"><i class="fa fa-frown"></i> Reject</a>',URL,$r['id']);
                    $fileLink = sprintf('<a href="http://%1$s/downloads/%2$s" target="_blank">%2$s</a>',URL,$r['filename']);
                }

                $tbl->addRows([
                    $r['id'],
                    $fileLink,
                    $users[$r['user_id']]['username'],
                    $r['message'],
                    $approveBtn . $rejectBtn

                ]);
            }
        }

        if (!empty($tbl->tr)) {
            $out .= '<h3><i class="fa fa-minus showHide" data-target="pendingRequests"></i> '. str_pending_requests .' </h3>';
            $out .= $tbl->printOut();
        }

        $tbl = new createTable();
        $tbl->def(['tid'=>'approveTable','tclass'=>'resultstable']);
        $tbl->addHeader(array('id', str_filename, str_requested_by, str_valid_until, str_downloaded, str_actions));
        foreach ($requests as $r) {
            if ($r['status'] == 'approved') {

                if (!has_access('master')) {
                    $rejectBtn = ($r['valid_until'] < date("Y-m-d")) ? 'expired' : sprintf('<a href="http://%1$s/ajax?m=download_restricted&action=download&id=%2$s" target="_blank" class="button-href pure-button" ><i class="fa fa-download"></i> Download</a>',URL,$r['id']);
                    $fileLink = $r['filename'];

                }
                else {
                    $rejectBtn = sprintf('<a class="button-href pure-button reject" data-request_id="%2$s" ><i class="fa fa-frown"></i> Reject</a>',URL,$r['id']);
                    $fileLink = sprintf('<a href="http://%1$s/downloads/%2$s" target="_blank">%2$s</a>',URL,$r['filename']);
                }


                $tbl->addRows([
                    $r['id'],
                    $fileLink,
                    $users[$r['user_id']]['username'],
                    $r['valid_until'],
                    $r['downloaded'],
                    $rejectBtn
                ]);
            }
        }

        if (!empty($tbl->tr)) {
            $out .= '<h3><i class="fa fa-minus showHide" data-target="approveTable"></i> '. str_approved_requests .' </h3>';
            $out .= $tbl->printOut();
        }

        $tbl = new createTable();
        $tbl->def(['tid'=>'rejectTable','tclass'=>'resultstable hidden']);
        $tbl->addHeader(array('id', str_filename, str_requested_by, str_actions));
        foreach ($requests as $r) {
            if ($r['status'] == 'rejected') {
                if (!has_access('master')) {
                    $approveBtn = '';
                    $fileLink = $r['filename'];
                }
                else {
                    $approveBtn = sprintf('<a class="button-href pure-button approve" data-request_id="%2$s"><i class="fa fa-smile"></i> Approve</a>',URL,$r['id']);
                    $fileLink = sprintf('<a href="http://%1$s/downloads/%2$s" target="_blank">%2$s</a>',URL,$r['filename']);
                }

                $tbl->addRows([
                    $r['id'],
                    $fileLink,
                    $users[$r['user_id']]['username'],
                    $approveBtn
                ]);
            }
        }

        if (!empty($tbl->tr)) {
            $out .= '<h3><i class="fa fa-plus showHide" data-target="rejectTable"></i> ' . str_rejected_requests . ' </h3>';
            $out .= $tbl->printOut();
        }
        $out .= '</div>';
        return $out;
    }

    public function downloadAccepted() {
        global $ID;
        if (!has_access('master')) {
            $cmd = sprintf('SELECT DISTINCT uploader_id FROM temporary_tables.temp_query_%1$s_%2$s',PROJECTTABLE,session_id());
            $res = $this->query($ID,$cmd);
            if (pg_num_rows($res) > 1) {
                return 1;
            }
            if ($row = pg_fetch_assoc($res)) {
                if ($row['uploader_id'] == $_SESSION['Trole_id'])
                    return 0;
                else 
                    return 1;
            }
        }
        else  
            return 0;
    }

    public function newRequest($params, $files) {
        global $ID, $BID;
        $tmpfile = $files['tmpfile'];
        $filename = $files['filename'];
        $reqMessage = $files['message'];

        if (!$files['tmpfile']) {
            $content = $files['content'];
            $f = fopen(getenv('PROJECT_DIR').'downloads/'.$filename,'w+');
            fwrite($f, $content);
            fclose($f);
        }
        else {
            //copying the file to project dir
            copy($tmpfile,getenv('PROJECT_DIR').'downloads/'.$filename);
            if (!file_exists(getenv('PROJECT_DIR').'downloads/'.$filename)) {
                $this->error = 'copy error!';
                return false;
            }
        }

        //insert request into the database
        $cmd = sprintf('INSERT INTO download_requests ("filename","project","user_id","message","lang") VALUES (%s,\'%s\',%s,%s,\'%s\') RETURNING "id";',quote($filename),PROJECTTABLE,$_SESSION['Tid'],quote($reqMessage),$_SESSION['LANG']);
        $res = $this->query($ID,$cmd);
        if (!$r = pg_fetch_assoc($res)) {
            $this->error = 'insert error';
            return false;
        }

        $id = $r['id'];

        preg_match('/(.*)%ST%(.*)%TB%(.*)/', str_download_mail_to_requester, $m);
        if (count($m) !== 4) 
            log_action('message not formed correctly',__FILE__,__LINE__);

        $subject = $m[1];
        $title = preg_replace('/%username%/',$_SESSION['Tname'],$m[2]);
        $message = $m[3];

        //sending mail to requester
        //function mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method) {
        //
        $err = mail_to("{$_SESSION['Tmail']}","$subject","$this->project_email","$this->project_email","$title","$message" ,'multipart');

        $message = preg_replace('/%username%/',$_SESSION['Tname'],str_download_mail_to_admin);
        $message = preg_replace('/%requestid%/',$id,$message);
        $message = preg_replace('/%request_message%/',$reqMessage,$message);

        //mail to project admin
        $err = mail_to("$this->project_email","download request from {$_SESSION['Tname']}","{$_SESSION['Tmail']}","$this->project_email","","$message" ,'multipart');

        echo common_message('ok');
        return;

    }

    private function getMailText($const,$lang) { 
        global $BID;
        $cmd = sprintf("SELECT translation FROM translations WHERE project = '%s' AND lang = %s AND const = %s LIMIT 1", PROJECTTABLE, quote($lang), quote($const));
        $text = pg_fetch_assoc($this->query($BID, $cmd));
        return $text['translation'];
    }
        
    private function approve($req_id) {
        global $ID;

        $req = $this->getRequest($req_id);
        switch ($req['status']) {
        case 'pending':  
        case 'rejected':
            $cmd = sprintf("UPDATE download_requests SET status = 'approved', valid_until = NOW() + interval '1 month'  WHERE id = %s", quote($req_id));

            if ($this->query($ID, $cmd)) {
                $str_download_mail_approved = $this->getMailText('str_download_mail_approved',$req['lang']);

                preg_match('/(.*)%ST%(.*)%TB%(.*)/',$str_download_mail_approved, $m);
                if (count($m) !== 4) 
                    log_action('message not formed correctly',__FILE__,__LINE__);

                $subject = $m[1];
                $title = preg_replace('/%username%/',$req['username'],$m[2]);
                $message = preg_replace('/%terms%/',"<a href='http://". OB_DOMAIN."/aszf_{$_SESSION['LANG']}.pdf' download>  </a>",$m[3]);

                $err = mail_to("{$req['email']}","$subject","$this->project_email","$this->project_email","$title","$message" ,'multipart');
                echo common_message('success',"download request #$req_id approved");
            }

            break;
        case 'approved':
            echo common_message("fail","download request #$req_id already approved");
            break;
        case 'not found':
            echo common_message("fail","download request #$req_id not found");
            break;
        }
    }


    private function reject($req_id) {
        global $ID;

        $req = $this->getRequest($req_id);
        switch ($req['status']) {
        case 'pending':  
        case 'approved':
            $cmd = sprintf("UPDATE download_requests SET status = 'rejected', valid_until = NOW() WHERE id = %s", quote($req_id));
            if ($this->query($ID, $cmd))
                echo common_message("success","download request #$req_id rejected");
            break;
        case 'rejected':
            echo common_message("fail","download request #$req_id already rejected");
            break;
        case 'not found':
            echo common_message("fail","download request #$req_id not found");
            break;
        }
    }

    private function download($req_id) {
        global $ID;

        $req = $this->getRequest($req_id);
        $f = getenv('PROJECT_DIR').'downloads/'.$req['filename'];
        if ($req['user_id'] !== $_SESSION['Tid']) {
            $this->error = 'ERROR: Not your file!';
            return;
        }
        if (!file_exists($f)) {
            $this->error = 'ERROR: Sorry your file does not exist!';
            return;
        }
        if ($req['valid_until'] < date("Y-m-d H:i:s")) {
            $this->error = 'ERROR: Your download is expired!';
            return;
        }
        if (headers_sent($file, $line)) {
            $this->error = "HTTP headers already sent at {$file}:{$line}";
            return;
        }
        $cmd = sprintf("UPDATE download_requests SET downloaded = %s WHERE id = %s", $req['downloaded'] + 1, $req['id']);
        $this->query($ID,$cmd);
        $filesize = filesize($f);
        switch (substr($req['filename'],-3)) {
        case 'csv':
            $ftype = 'csv';
            break;
        case 'gpx':
            $ftype = 'xml';
            break;
        default:
            $this->error = 'Unknown filetype';
            return;
        }

        header("Content-Type: text/$ftype");
        header("Content-Length: ". $filesize); 
        header("Content-Disposition: attachment; filename=\"{$req['filename']}\";");
        header("Expires: -1");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);

        $handle = fopen($f, "r");
        print(fread($handle, $filesize));
        fclose($handle);

        exit;

    }

    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    private function getRequest($id) {
        global $ID, $BID;

        if ($res = $this->query($ID,sprintf("SELECT * FROM download_requests WHERE id = %s;",$id))) {
            if ($request = pg_fetch_assoc($res)) {
                if ($res = $this->query($BID,sprintf("SELECT id, username, email FROM users WHERE id IN (%s);",$request['user_id']))) {
                    if ($row2 = pg_fetch_assoc($res)) {
                        $request['email'] = $row2['email'];
                        $request['username'] = $row2['username'];
                        return $request;
                    }
                }
            }
            else
                return 'not found';
        }
    }

    public function ajax($params, $pa) {

        $request = $pa[0];
        if (isset($request['action'])) {
            $id = (isset($request['id'])) ? preg_replace('/[^0-9,]/','',$request['id']) : '';

            if ($request['action'] == 'approve')
                $this->approve($id);
            if ($request['action'] == 'reject')
                $this->reject($id); 
            if ($request['action'] == 'download')
                $this->download($id);
        }
    }

    public function print_js($request) {
        //$script = '<script type="text/javascript" src="http://'.URL.'/modules/download_restricted.js"/> </script> ';
        $script = '
$(document).ready(function() {
    $("body").on("click",".openDlrForm",function(ev){
        ev.preventDefault();
        var f = $("#dlr-message");

        if (!f.length) {
            $( "#dialog" ).text("'.str_dowload_request_already_sent.'");
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog" ).dialog( "open" ); 
        }

        f.find("[type=\'submit\']").attr("href",$(this).attr("href"));
        f.show();
        $(this).attr("disabled","disabled");
    });
    $("body").on("submit","#dlr-message",function(ev) {
        ev.preventDefault();
        var href = $(this).find("[type=\'submit\']").attr("href");
        var message = $(this).find("[name=\'message\']").val();

        $.post(href,{"message":message},function(data) { 
            var retval = jsendp(data);
            let txt = "";
            if (retval["status"]=="error")
                txt = retval["message"];
            else if (retval["status"]=="fail")
                txt = "FAIL: "+retval["data"];
            else if (retval["status"]=="success") {
                txt = "' . str_download_request_sent . '";
                $("#dlr-message").hide();
            }
            $( "#dialog" ).text(txt);
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog" ).dialog( "open" ); 
        });
    });
    $("#tab_basic").on("click",".reject, .approve",async function(ev){
        ev.preventDefault();
        try {
            let action = "";
            if ($(this).hasClass("approve")) action = "approve";
            if ($(this).hasClass("reject")) action = "reject";
            if (action !== "") {
                const id = $(this).data("request_id");
                const data = await $.post("ajax", { "m": "download_restricted", "action": action, "id": id });
                   
                const retval = jsendp(data);
                let txt = "";
                if (retval["status"]=="error") 
                    txt = retval["message"];
                else if (retval["status"]=="fail")
                    txt = "FAIL: "+retval["data"];
                else if (retval["status"]=="success") {
                    const row = $(this).parent().parent();
                    row.remove();
                    txt = "SUCCESS: " + retval["data"];
                }
                $( "#dialog" ).text(txt);
                var isOpen = $( "#dialog" ).dialog( "isOpen" );
                if(!isOpen) $( "#dialog" ).dialog( "open" ); 
            }
        }
        catch (error) {
            console.log(error);
        }
    });
});';

        return $script;

    }

    static function init() {

        global $ID;
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'download_requests');");
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);


        // importing translations into the database
        if ($result['exists'] == 'f') {

            $cmd1 = sprintf('CREATE SEQUENCE download_requests_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1; 
            CREATE TABLE "public"."download_requests" ( 
                "id" integer DEFAULT nextval(\'download_requests_id_seq\') NOT NULL, 
                "filename" character varying(128) NOT NULL, 
                "project" character varying(32) NOT NULL, 
                "user_id" integer NOT NULL, 
                "lang" character varying(2) NOT NULL,
                "status" varchar(32) NOT NULL DEFAULT \'pending\',
                "message" text,
                "downloaded" integer DEFAULT 0, 
                "requested" timestamp DEFAULT NOW(), 
                "valid_until" timestamp,  
                CONSTRAINT "download_requests_filename" UNIQUE ("filename"), 
                CONSTRAINT "download_requests_id" PRIMARY KEY ("id")); ');
            if (pg_send_query($ID, $cmd1)) {
                for ($i = 0; $i < 2; $i++) {
                    $res = pg_get_result($ID);
                    $state = pg_result_error($res);
                    if ($state != '') {
                        log_action($state,__FILE__,__LINE__);
                        return false;
                    }
                }
            }
            else {
                log_action("pg_send_query failed!",__FILE__,__LINE__);
                return false;
            }
        }
        return true;

    }
    private function query($dbconn,$cmd) {
        if (pg_send_query($dbconn,$cmd)) {
            $res = pg_get_result($dbconn);
            $state = pg_result_error($res);
            if ($state != '') {
                $this->error = $state;
                return false;
            }
            return $res;
        }
        else {
            $this->error = "pg_send_query failed!";
            return false;
        }

    }
}

?>
