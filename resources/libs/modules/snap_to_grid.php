<?php
# alias: trasform_geometries
class snap_to_grid {
    function  geom_column($p,$geom_col) {

        if (is_null($geom_col)) {
            if (isset($_SESSION['st_col']['GEOM_C']) and isset($_SESSION['current_query_table']))
                // if obm_geometry defined in the query table!!!!
                $t = $_SESSION['current_query_table'].'.'.$_SESSION['st_col']['GEOM_C'];
            else
                 $t = "obm_geometry";
            
        } else {
            $t = $geom_col;
        }
        // This module function only used for non login users!!
        if (!isset($_SESSION['Tid']) and !rst('acc')) {
            $params = preg_split('/;/',$p);
            if (count($params)==2) {
                $t = sprintf("ST_AsText(ST_SnapToGrid(%s,%f,%f))",$t,$params[0],$params[1]);
            } else {
                $t = sprintf("ST_AsText(ST_SnapToGrid(%s,0.13,0.09))",$t);
            }
        }
        return $t;
    }
    function geom_column_join($p) {
        return $this->geom_column($p);
    }
    function rules_join($p) {
        return '';
    }
}
?>
