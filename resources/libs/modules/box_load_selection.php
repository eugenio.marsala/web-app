<?php
class box_load_selection {
    function print_box ($params) {
        global $ID;
        $spatial_relationships = ['contains','intersects','crosses','disjoint'];

        $p = array_filter(explode(';',$params), function ($p) use ($spatial_relationships) {
            return in_array($p,$spatial_relationships);
        });

        if (count($p)) 
            $spatial_relationships = $p;

        $t = new table_row_template();
    
        if (isset($_SESSION['Tid']))
            $cmd = sprintf("SELECT id,name,to_char(timestamp,'YYYY.MM.DD:HH24:MI:SS') as d 
            FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id 
            WHERE select_view IN ('1','3','only-select','select-upload') AND p.user_id=%d 
            ORDER BY name,d",$_SESSION['Tid']);
        else 
            $cmd = sprintf("SELECT id,name,to_char(timestamp,'YYYY.MM.DD:HH24:MI:SS') as d 
            FROM system.shared_polygons s  
            WHERE access IN ('4','public') OR (project_table='%s' AND access='3') 
            ORDER BY name,d",PROJECTTABLE);
     
        $res = pg_query($ID,$cmd);
        $options = "<option></option>";
        while($row=pg_fetch_assoc($res)) {
           $options .= sprintf("<option value='%d'>%s - %s</option>",$row['id'],$row['name'],$row['d']);
        }
        
        $t->cell(mb_convert_case(str_load_selections, MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        $box_html = "<select id='shared_geom_id' style='width:auto;vertical-align:top;max-width:400px'>$options</select><span id='shared-geom-settings' class='pure-button button-href' style='margin-left:10px;'><i class='fa fa-cog fa-lg'></i></span> <div style='position:relative'><div id='shared-geom-settings-box' style='display:none;border:1px solid #acacac;padding:2px 20px 2px 2px;margin:2px;position:absolute;right:5px;top:0px;background-color:white'> ".t(str_spatial_relationship).":<br><select id='spatial_function' name='spatial_function[]' size=3 style='width:auto' multiple>";

        for ($i = 0, $l = count($spatial_relationships); $i < $l; $i++) {
            $selected = ($i == 0) ? 'selected' : '';
            $sr = $spatial_relationships[$i];
            $box_html .= "<option $selected value='$sr'>".constant("str_$sr")."</option>";
        }

        $box_html .= "</select> </div></div> ";

        $t->cell($box_html,2,'content');

        $t->row();
        //$t->cell("<button id='select_with_shared_geom' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
        $t->row();
        return sprintf("<table class='mapfb'>%s</table>",$t->printOut());

    }
    function profileItem() {

        $em = sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='?profile&showowngeoms#tab_basic' data-url='/includes/profile.php?options=owngeoms' class='profilelink'>".str_own_polygons."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='?profile&showsharedgeoms#tab_basic' data-url='/includes/profile.php?options=sharedgeoms' class='profilelink'>".str_shared_polygons."</a>");
        $em .= sprintf("<tr><td>");

        $em .= str_shp_upload.":<br>KML, SHP (.shp,.shx,.dbf, .prj)<br>";
        $em .= "<form enctype='multipart/form-data' action='' method='post'><table>
            <tr><td><input type='file' id='shpfile' name='shpfile[]' multiple=multiple size='8' class='pure-button'></td></tr>
            <tr><td><div id='shpuplresp'></div></td></tr>
            <tr><td>
label column: <input id='shp_label'><br>
<button id='shp_upload' class='button-large button-secondary pure-button pure-u-1-1'><i class='fa fa-upload' id='shp_upload-i'></i> ".t(str_upload)."</button></td></tr>
            <tr><td>".wikilink('user_interface.html#custom-geometries',str_what_are_custom_geometries)."</td></tr>
            </table></form>";
        $em .= "</td></tr>";

        return [
            'label' => str_polygon_settings,
            'fa' => 'fa-map',
            'item' => $em
        ];
    }
    function adminPage() {

        if (!has_access('master')) return;

        global $ID,$BID;

        $W = "";

        $em = "<h2>".t(str_all_polygons)."</h2><div class='shortdesc'>".str_geomdesc."</div>";
        
        $cmd = "SELECT id, name, original_name, access, p.select_view, ARRAY_TO_STRING(ARRAY_AGG(p.user_id),',') AS user, s.user_id as owner,st_asText(geometry) as wkt
                    FROM system.shared_polygons s 
                    LEFT JOIN system.polygon_users p ON (id=polygon_id) 
                    GROUP BY s.id,p.select_view
                    ORDER BY name,id";

        
        $share_options = $admin_funcs = $save_b = $del_b = "" ;
        $view_button = '';
        $viewu_button = '';

        $res = pg_query($ID,$cmd);
        $n = 0;
        $defn = pg_num_rows($res) + 100;
        $table = "";
        $polygonid = '';
        while ( $row = pg_fetch_assoc($res) ) {

            $own = 0;
            if (isset($_SESSION['Tid']) and $_SESSION['Tid']==$row['owner']) $own = 1;

            $s1 = $s2 = $s3 = $s4 = '';
            #'<button class="pure-button button-gray button-small polygon_showforce_options sel" id="ops%3$d_%2$d" value="none"><i id="iops%3$d_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
            $def_view_button = '<span style="font-size:16px">+</span><input type="hidden" class="polygon_showforce_options sel" id="ops%3$d_%2$d" value="select-upload">';
            #'<button class="pure-button button-gray button-small polygon_showforce_options upl" id="opu%3$d_%2$d" value="none"><i id="iopu%3$d_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
            $def_upld_button = '<span style="font-size:16px">+</span><input type="hidden" class="polygon_showforce_options upl" id="opu%3$d_%2$d" value="select-upload">';

            // kicsit komplikált, mert egy értéken tárolok két beállítást
            if ($row['select_view']=='0' or $row['select_view']=='none') {
                // elméletileg ilyen eset nincs, mert törlődik 
                if ($row['select_view']=='0') $row['select_view']='none';
                $view_button = '<button class="pure-button button-error button-small polygon_showforce_options sel" id="ops%3$d_%2$d" value="'.$row['select_view'].'"><i id="iops%3$d_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
                $viewu_button ='<button class="pure-button button-error button-small polygon_showforce_options upl" id="opu%3$d_%2$d" value="'.$row['select_view'].'"><i id="iopu%3$d_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
            } elseif ($row['select_view']=='1' or $row['select_view']=='only-select') {
                if ($row['select_view']=='1') $row['select_view']='only-select';
                $view_button = '<button class="pure-button button-secondary button-small polygon_showforce_options sel" id="ops%3$d_%2$d" value="'.$row['select_view'].'"><i id="iops%3$d_%2$d" class="fa fa-eye fa-2x"></i></button>';
                $viewu_button ='<button class="pure-button button-error button-small polygon_showforce_options upl" id="opu%3$d_%2$d" value="'.$row['select_view'].'"><i id="iopu%3$d_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
            } elseif ($row['select_view']=='2' or $row['select_view']=='only-upload') {
                if ($row['select_view']=='2') $row['select_view']='only-upload';
                $view_button = '<button class="pure-button button-error button-small polygon_showforce_options sel" id="ops%3$d_%2$d" value="'.$row['select_view'].'"><i id="iops%3$d_%2$d" class="fa fa-eye-slash fa-2x"></i></button>';
                $viewu_button = '<button class="pure-button button-secondary button-small polygon_showforce_options upl" id="opu%3$d_%2$d" value="'.$row['select_view'].'"><i id="iopu%3$d_%2$d" class="fa fa-eye fa-2x"></i></button>';
            } elseif ($row['select_view']=='3' or $row['select_view']=='select-upload') {
                if ($row['select_view']=='3') $row['select_view']='select-upload';
                $view_button = '<button class="pure-button button-secondary button-small polygon_showforce_options sel" id="ops%3$d_%2$d" value="'.$row['select_view'].'"><i id="iops%3$d_%2$d" class="fa fa-eye fa-2x"></i></button>';
                $viewu_button = '<button class="pure-button button-secondary button-small polygon_showforce_options upl" id="opu%3$d_%2$d" value="'.$row['select_view'].'"><i id="iopu%3$d_%2$d" class="fa fa-eye fa-2x"></i></button>';
            }

            $access_array = array(''=>'private','1'=>'private','2'=>'login','3'=>'project','4'=>'public','private'=>'private','login'=>'login','project'=>'project','public'=>'public');
            $access = $access_array[$row['access']];
            $options = selected_option(array(sprintf('%s::private',str_private),sprintf('%s::login',str_logined_users),sprintf('%s::project',str_project_users),sprintf('%s::public',str_public)),$access);

            $cmd = sprintf("SELECT user_id FROM system.polygon_users WHERE polygon_id=%d",$row['id']);
            $res3 = pg_query($ID,$cmd);

            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

            $input = '<span style="font-size:8px">%4$s</span><br><input value="%1$s" style="width:300px" class="pure-input polygon_name" id="ope-name%3$d_%2$d">';
            $save_b = '<button class="pure-button button-gray button-small polygon_forcename_save" id="ope%3$d_%2$d"><i class="fa fa-floppy-o fa-2x"></i></button><br>
                
                <a href="#" onClick="MyWindow=window.open(\''.$protocol.'://'.URL.'/geomtest?wkt='.$row['wkt'].'&name='.$row['name'].'\',\'MyWindow\',\'width=600,height=300\'); return false;">Click here to show geometry</a>';
                
            $del_b = '<button class="pure-button button-warning button-small dropforcepolygon" id="opd%3$d_%2$d"><i class="fa fa-trash-o fa-2x"></i></button>';

            // disable delete option if polygon is used by others
            if (pg_num_rows($res3) > 0)
                $del_b .= " [".pg_num_rows($res3)."]";

            $share_options = '<select class="polygon_forceaccess" id="ope-access_%2$d">'.$options.'</select>';

            $cmd = "SELECT id,username,array_to_string(familyname,' ') as familyname,array_to_string(givenname,' ') as givenname
                FROM users u 
                LEFT JOIN project_users pu ON (pu.user_id=u.id)
                WHERE pu.project_table='".PROJECTTABLE."' 
                ORDER BY familyname,givenname,username";
            $res4 = pg_query($BID,$cmd);

            $options = array();
            $def_options = array();
            $admin_funcs = '';
            $def_admin_funcs = '';
            while ( $user_row = pg_fetch_assoc($res4) ) {
                $selected = '';
                if (in_array($user_row['id'],explode(',',$row['user'])))
                    $selected = "selected";

                if ($user_row['familyname'].$user_row['givenname']=='')
                    $name = $user_row['username'];
                else
                    $name = $user_row['familyname'].' '.$user_row['givenname'];
                $options[] = "<option value='{$user_row['id']}' $selected>$name</option>";
                $def_options[] = "<option value='{$user_row['id']}'>$name</option>";
            }
            
            $admin_funcs .= sprintf('<select multiple class="poly_users_force" id="poly-users-force%3$d_%1$d">%2$s</select>',$row['id'],implode("",$options),$n);
            $def_admin_funcs .= sprintf('<input type="hidden" class="admin-option" data-url="includes/project_admin.php?options=box_load_selection"><select multiple class="poly_users_force" id="poly-users-force%3$d_%1$d">%2$s</select>',$row['id'],implode("",$def_options),$defn);

            // ez a felső sora minden geometriának
            if ($row['id'] != $polygonid) {
                $table .= sprintf('<tr><td style="text-align:center">'.$del_b.'</td>
                <td>'.$input.' '.$save_b.'</td><td>'.$share_options.'</td>
                <td style="text-align:center">'.$def_view_button.'</td><td style="text-align:center">'.$def_upld_button.'</td>
                <td>'.$def_admin_funcs.'</td></tr>',$row['name'],$row['id'],$defn,$row['original_name']);
            }

            if ($view_button != '')
                $table .= sprintf('<tr><td></td><td><input hidden value="%1$s" class="pure-input" style="width:300px"></td><td></td>
                    <td style="text-align:center">'.$view_button.'</td><td style="text-align:center">'.$viewu_button.'</td>
                    <td>'.$admin_funcs.'</td></tr>',$row['name'],$row['id'],$n);

            $polygonid = $row['id'];
            
            $n++;
            $defn++;
        }

        $em .= "<table class='resultstable'>";
        $em .= "<tr><th>".t(str_delete)."</th><th>".t(str_selection_name)."</th><th>".t(str_available_for)."</th><th>".t(str_select)."</th><th>".t(str_upload)."</th><th>".t(str_administration)."</th></tr>";
        $em .= $table;
        $em .= "</table>";
        return $em;


    }
    function getMenuItem() {
        return ['label' => str_all_polygons, 'url' => 'box_load_selection' ];
    }

    function print_js($params) {
        echo "
    // box load selection module
    $(document).ready(function() {
        /* modules/box_load_selection.php
         * FILTERBOX
         * Szelekció betöltése 
         * can be multipolygon width several repeated WFS query, wich resulting various output
         *
         * */
        $('#mapfilters').on('change','#shared_geom_id',function(){
            drawLayer.destroyFeatures();
            geometry_query_selection = 0;
            if ($(this).val()!='') {
                $.post('ajax', {getWktGeometry:$('#shared_geom_id').val()},
                    function(data){
                        // draw polygon on map
                        drawPolygonFromWKT(data,1);
                        geometry_query_selection = 1;
                });
            }
        });
    });";
    }
}
?>
