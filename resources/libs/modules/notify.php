<?php
class notify {
    function listen($param) {
        global $GID;
        pg_query($GID, "LISTEN $param");
        return;
    }

    function unlisten($param) {
        global $GID;
        pg_query($GID, "UNLISTEN $param");
        return;
    }

    // it is a good place to create custom trigger function like sending mails after uploading data
    // using postgres LISTEN/NOTIFY
    function notify($param) {
        global $GID;
        $w = 0;
        while (1) {
            if ($w == 5) break;

            $notify = pg_get_notify($GID);
            if ($notify) {
                $payload = $notify['payload'];
                
                break;

            } else {
                //log_action('No notify message yet.');
            }

            $w++;
            sleep(1);
        }
        $this->unlisten($param);
        return;
    }

    function email($param) {
        return; 
    }

}

?>
