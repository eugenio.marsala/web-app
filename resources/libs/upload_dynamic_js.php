<?php
/* Dynamic JS includes for upload tables
 *
 * */
$X_C = "--";
$Y_C = "--";
if (isset($_SESSION['st_col']['X_C']) and $_SESSION['st_col']['X_C']!='')
    $X_C = $_SESSION['st_col']['X_C'];
if (isset($_SESSION['st_col']['Y_C']) and $_SESSION['st_col']['Y_C']!='')
    $Y_C = $_SESSION['st_col']['Y_C'];
$SPECIES_C = $_SESSION['st_col']['SPECIES_C'];
if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$SPECIES_C,$m))
    $SPECIES_C = $m[2];

$GEOM_C = $_SESSION['st_col']['GEOM_C'];

?>
<script type="text/javascript">
var sfid='<?php
    echo md5($_SESSION['openpage_key'].$insert_allowed.$selected_form_type); ?>';

function isNumeric(obj) {return obj - parseFloat(obj) >= 0;}
$(document).ready(function() {
    //fire methods for loading ui.shortable, after saved states loaded
    //Is it possible to save and load back the namespace as well?
    $('body').on('dblclick','ul.oList',function(){
        sortevent();
    });
    $('body').on('dblclick','ul.sList',function(){
        sortevent();
    });

    // Show points on the map CLICK function
    // Its prepare the url GET parameter for the geomtest.php
    $("#showon").click(function(event){
        event.preventDefault();
        var srid = $('#upl-srid').val();
        if (srid!='') var epsg = "EPSG:"+srid;
        var lon=new Array, lat=new Array, name=new Array, geom=new Array, linestring=new Array, point=new Array, gtype=new Array;
        
        //oszloponként
        $(".oListElem").each(function(){
            var colvalue=$(this).find('li').eq(0).text();
            if (colvalue!='') {
                var colIndex = $(this).closest('.th').index();
                // x coord column
                if ($(this).find('li').eq(0).attr('id')=='<?php echo $X_C; ?>') {
                    // all row
                    $("#upload-data-table > .tbody > .tr").each(function(){
                        // if not marked to skip
                        $(this).find('.td:eq('+colIndex+')').find('input').val()
                        if($(this).attr('class')!='skippedRow' && $(this).find('.td:eq('+colIndex+')').find('input').val()!='')
                            lon.push($(this).find('.td:eq('+colIndex+')').find('input').val());
                    });
                // y coord column
                } else if ($(this).find('li').eq(0).attr('id')=='<?php echo $Y_C; ?>') {
                    // all row
                    $("#upload-data-table > .tbody > .tr").each(function(){
                        // if not marked to skip
                        if($(this).attr('class')!='skippedRow' && $(this).find('.td:eq('+colIndex+')').find('input').val()!='')
                            lat.push($(this).find('.td:eq('+colIndex+')').find('input').val());
                    });
                // scientific name
                } else if ($(this).find('li').eq(0).attr('id')=='<?php echo $SPECIES_C ?>') {
                    // all row
                    $("#upload-data-table > .tbody > .tr").each(function(){
                        if($(this).attr('class')!='skippedRow' && $(this).find('.td:eq('+colIndex+')').find('input').val()!='')
                            name.push($(this).find('.td:eq('+colIndex+')').find('input').val());
                    });
                // geom column
                } else if ($(this).find('li').eq(0).attr('id')=='<?php echo $GEOM_C ?>') {
                    geom_col_id = colIndex;
                    // all row
                    $("#upload-data-table > .tbody > .tr").each(function(){
                        // if not marked to skip
                        if($(this).attr('class')!='skippedRow' && $(this).find('.td:eq('+colIndex+')').find('input').val()!='') {
                            var str = $(this).find('.td:eq('+colIndex+')').find('input').val();
                            var arr = new Array();
                            var a;
                            // ez a blokk kimehetne egy WKT2GeoJson osztályba
                            if (con = str.match(/POINT\s?\((.+)\)/i)){
                                coords = con[1].match(/([0-9. ]+)/g);
                                $.each(coords, function( i, val ) {
                                    arr.push(val.replace(/ /g,","));
                                });
                                a = arr.join();
                                try {
                                    a = JSON.parse("[" + a + "]");
                                } catch (e) {
                                    alert("Geometry syntax should be json: [[1,2],[2,3][1,3]] or WKT");
                                }
                                geom.push(a);
                                gtype.push('point');
                            } 
                            else if (con = str.match(/POLYGON\s?\((.+)\)/i)){
                                coords = con[1].match(/([0-9. ]+)/g);
                                $.each(coords, function( i, val ) {
                                    arr.push(val.replace(/ /g,","));
                                });
                                a = "[["+arr.join("],[")+"]]";
                                try {
                                    a = JSON.parse("[" + a + "]");
                                } catch (e) {
                                    alert("Geometry syntax should be json: [[1,2],[2,3][1,3]] or WKT");
                                }
                                geom.push(a);
                                gtype.push('polygon');
                            } 
                            else if (con = str.match(/LINESTRING\s?\((.+)\)/i)){
                                coords = con[1].match(/([0-9. ]+)/g);
                                $.each(coords, function( i, val ) {
                                    arr.push(val.replace(/ /g,","));
                                });
                                a = "["+arr.join("],[")+"]";
                                try {
                                    a = JSON.parse("[" + a + "]");
                                } catch (e) {
                                    alert("Geometry syntax should be json: [[1,2],[2,3][1,3]] or WKT");
                                }
                                geom.push(a);
                                gtype.push('line');
                            }
                        }
                    });
                }
            }
        });
        var x = new Array();
        if (lon.length) {
            for(var i=0;i<lon.length;i++){
                if (lon[i]!='') {
                    //match for 05°10.341 and 052.39.191, 052.39,191 as well
                    if (lon[i].match(/\d+[°.]\d+[.,']\d+/)) {

                        // radian degree
                        // php version in afuncs.php 1180
                        // preg_match("/(-?\d{1,3})°(\d+)'(\d+)/",$j->{'data'}[$X][$k],$m)
                        // Decimal Degrees = Degrees + minutes/60 + seconds/3600
                        // $x = $m[1]+($m[2]/60)+$m[3]/3600;
                        var parts = lon[i].split(/[^\w-]+/);
                        console.log(parts);
                        if (parts[2].match(/(\d+)([wesn])/i)) {
                            res = parts[2].match(/(\d+)([wesn])/i);
                            parts[2] = res[1];
                            parts[3] = res[2];
                        }
                        if (parts[0]<0) {
                            parts[0] = Math.abs(parts[0]);
                            parts[3] = 'W';
                        }
                        var lon_ = ConvertDMSToDD(parts[0], parts[1], parts[2], parts[3]);
                        var parts = lat[i].split(/[^\w-]+/);
                        if (parts[2].match(/(\d+)([wesn])/i)) {
                            res = parts[2].match(/(\d+)([wesn])/i);
                            parts[2] = res[1];
                            parts[3] = res[2];
                        }
                        if (parts[0]<0) {
                            parts[0] = Math.abs(parts[0]);
                            parts[3] = 'S';
                        }
                        var lat_ = ConvertDMSToDD(parts[0], parts[1], parts[2], parts[3]);
                        x.push({name:name[i],point:Array(parseFloat(lon_),parseFloat(lat_))});

                    } else {
                        // normal way: float numbers
                        x.push({name:name[i],point:Array(parseFloat(lon[i]),parseFloat(lat[i]))});
                    }
                }
            }
        }
        else if (geom.length) {
            for(var i=0;i<geom.length;i++){
                if (geom[i]!='') {
                    if (gtype[i]=='point') {
                        if (typeof name[i] === 'undefined' || name[i] === null) {
                            name[i] = 'name: '+i;
                        }
                        x.push({name:name[i],point:geom[i]});
                    } else if (gtype[i]=='line') {
                        if (typeof name[i] === 'undefined' || name[i] === null) {
                            name[i] = 'name: '+i;
                        }
                        x.push({name:name[i],line:geom[i]});
                    } else if (gtype[i]=='polygon') {
                        if (typeof name[i] === 'undefined' || name[i] === null) {
                            name[i] = 'name: '+i;
                        }
                        x.push({name:name[i],geom:geom[i]});
                    }
                }
            }
        }
        //geojson method
        /*$.getScript( "http://<?php echo URL ?>/js/geojson.min.js", function( data, textStatus, jqxhr ) {
          console.log( data ); // Data returned
          console.log( textStatus ); // Success
          console.log( jqxhr.status ); // 200
          console.log( "Load was performed." );
    });*/
        var g = GeoJSON.parse(x, {crs:epsg,Point:'point',Polygon:'geom',LineString:'line', include: ['name']});
        //later we should update like this:
        //g.crs.properties.name="urn:ogc:def:crs:OGC:1.3:CRS84";
        $.post("geomtest", { 'geojson_post':JSON.stringify(g),'srid':srid },
        function(data){
            var url = 'geomtest/?d='+data;
            window.open(url, '_blank');
        });

    });
<?php
if(isset($_SESSION['Tid']) and isset($_POST['column-assign-template'])) {
    # Bejelentkezett felhasználóknak template beállítások beolvasása
    # TEMPLATE session változók beállítása
    #

    if (!isset($_SESSION['upload_file']))
        $_SESSION['upload_file'] = array(); 
    $cmd = sprintf("SELECT slist,olist,name,sheet,assignments FROM settings_import WHERE user_id='{$_SESSION['Tid']}' AND project_table='".PROJECTTABLE."' AND name=%s",quote($_POST['column-assign-template']));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)){
        $row = pg_fetch_assoc($res);
        $upload_slist_template = json_decode($row['slist']);
        $upload_olist_template = json_decode($row['olist']);
        $_SESSION['upload_file']['activeSheet'] = $row['sheet'];
        $template_column_assign = $row['assignments'];
    }
}
/* elmentett állapot betöltése */
/* imports.header */
if (isset($jh)) {
    
    //template-ból töltöm be a bal oldali táblát, de ezt tképp nem kellene erőltetni,
    //habár így a két folyamathoz egy kód van!
    //$upload_slist_template=$jh->{'slist'};

    //ha be akarnám utólag tölteni a templatot, de nem akarom! 
    //$cot = 1;

    //left table
    $p = array();
    foreach($jh->{'olist'} as $i=>$k) { $p[]=preg_replace("/[^0-9]/","",$i); }
    printf("var cf=new Array(%s);\n",implode(',',$p));

    // sheetSkip létrehozása
    //if ($selected_form_type=='file') 
    //        $_SESSION['sheetSkip'] = $jd['data'];

    // row length of first data column; web form
    if ($selected_form_type=='web') {
        $sheetHeader = get_sheet_header();

        $column_width = count($sheetHeader);
        $data_length = get_upload_count();
    }
    //printf("var data=$.parseJSON('%s');\n",json_encode(get_sheet_page(0,-1))); 
    //imports.data
    /*
     * imports.data
        title:[],
        header:[],
        default:{
          id:[],
          data:[]
        }
     */
    printf("var jdata=$.parseJSON(%s);\n",$jda); 

    $validate_text = get_sheet_page(0,-1);
    $ee = array();
    foreach ($validate_text as $e) {
	$e = preg_replace('/"/','\"',$e);
	$ee[] = $e;
    }
    printf("var data=$.parseJSON('%s');\n",json_encode($ee,JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_AMP)); 

    $a = array('name'=>'','longitude'=>'','latitude'=>'','comment'=>'','description'=>'','symbol'=>'','time'=>'','elevation'=>'','ref'=>'','type'=>'');
            
        /*$count = 0;
        $letter = 'A';
        while ($count < ($column_width+1)) 
        {
            $theader[] = $letter;
            ++$letter;
            $count++;
        }*/
?>

<?php
        printf ("var skip=$.parseJSON('%s');\n",json_encode(get_sheet_skips(0,-1),true));
?>
        var data_ri = Array;
        var ri = -1;
        $('#upload-data-table > .tbody .td').each(function() {
            id = $(this).index();
            if (id==0) {
                ri++;
                if (skip[ri]['skip_marked'] == 't') {
                    $(this).find('input').trigger('click');
                }
                data_ri = $.map(data[ri], function(value, index) {
                    return [value];
                });

            }
            else {
<?php
    if ($selected_form_type=='web') {
?>
                // data columns
                var val = data_ri.shift();
                $(this).find('input').val(val)
                $(this).find('select').val(val)
<?php
    }
?>
            }
        });
        //set header
        $('#upload-data-table > .thead > .tr:eq(0)').find('.th').each(function(){
            id = $(this).index();
            if (typeof jdata['title'] !== 'undefined' && jdata['title'].length) {
                var val = jdata['title'].shift();
                //if (id==0) {
                //    $(this).html(val);
                //} else {
                    $(this).html(val);
                //}
            }
        });
        // itt rakom ki az elmentett fejléceket az oszlopok fölé
        // ezeket ki kell szedni az slist-ből ha esetleg benne maradtak volna! 
        $('#upload-data-table > .thead > .tr:eq(1)').find('.th').each(function(){
            if (typeof jdata['header'] !== 'undefined' && jdata['header'].length) {
                $(this).html(jdata['header'].shift());
                t = $(this).find('.field-title');
                if (t.length) {
                    ttid = t.html();
                    $("#leftTable").find( "#" + ttid ).remove();
                }
                $(this).find('.oList').trigger('dblclick');
            }
        });
        // set default
        if (typeof jdata['default'] !== 'undefined' && jdata['default'].length) {
            var j = $.parseJSON(jdata['default']);

            for (i=0;i<j['id'].length;i++) {
                $("#"+j['id'][i]).val(j['data'][i]);
            }
        }
    
        // load users' col-list template
<?php
} 


/* Template settings
 * Javascript from session variables
 * backward compatibility  -  js solution
 * DEPRECATED !!!
 * */
if (isset($upload_slist_template) and $upload_slist_template!='') {
    printf("    var slist=%s;\n",json_encode($upload_slist_template));
    echo "
    $('#sList').empty();
    $.each(slist, function(key, value) {
        $('#sList').append('<li id='+ key +'>'+ value +'</li>');
    });\n";
}
if (isset($upload_olist_template) and $upload_olist_template!='') {
    printf("    var olist=%s;\n",json_encode($upload_olist_template));
    echo "
    $('.oListElem').empty();
    $.each(olist, function(key, value) {
        $('.oListElem').each(function(){
            if ($(this).children().length==0 & $(this).closest('th').index()==key) {
                $(this).append(value);
            }
        });
    });";
}
?>
});
</script>
