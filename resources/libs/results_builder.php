<?php
/* This function set together the map query results
 * wfs_response is a json array including row ids and other wfs response elements
 *
 *
 * */
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

require_once(getenv('OB_LIB_DIR'). 'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
if(!isset($_SESSION['Tproject'])) {
    require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
require_once(getenv('OB_LIB_DIR').'languages.php');

/* create result slides, csv, gpx and table
 * it is handling restricted data as well
 * */
class results_builder {

    private $Method;
    private $Filename;
    private $Buttons = 'off';
    private $Slide;
    private $Specieslist = 'off';
    private $Print = 'on';
    private $Quote = "'";
    private $Sep = ",";
    private $Summary = "off";
    private $Reverse = 'off';
    private $Clear_current_rows = 'on';
    public $rq = array('columns','query','joins','joins_vnames','join_id','extent','results_count');
    public $response_id = array();
    public $error = array();
    public $modules = array();
    public $current_rows = array();
    public $current_rows_length;

    public function __construct (array $options = array()) {
        global $BID;
        $Method = (isset($options['method'])) ? $options['method'] : null;
        $Filename = (isset($options['filename'])) ? $options['filename'] : null;
        $Buttons = (isset($options['buttons'])) ? $options['buttons'] : null;
        $Slide = (isset($options['slide'])) ? $options['slide'] : null;
        $Specieslist = (isset($options['specieslist'])) ? $options['specieslist'] : null;
        $Print = (isset($options['print'])) ? $options['print'] : null;
        $Quote = (isset($options['quote'])) ? $options['quote'] : null;
        $Sep = (isset($options['sep'])) ? $options['sep'] : null;
        $Summary = (isset($options['summary'])) ? $options['summary'] : null;
        $Reverse = (isset($options['reverse'])) ? $options['reverse'] : null;
        $Clear_current_rows = (isset($options['clear_current_rows'])) ? $options['clear_current_rows'] : null;

        $this->method = $Method;
        $this->filename = $Filename;
        $this->buttons = $Buttons;
        $this->slide = $Slide;
        $this->specieslist = $Specieslist;
        $this->print = $Print;
        $this->sep = $Sep;
        $this->quote = $Quote;
        $this->summary = $Summary;
        $this->reverse = $Reverse;
        $this->clear_current_rows = $Clear_current_rows;

        //should be exists!
        //if (!isset($_SESSION['current_query_table'])) $_SESSION['current_query_table'] = PROJECTTABLE;
        /* MODULE INCLUDES HERE */

        $this->modules = new modules();
    }

    // create list of ids from WFS/WMS response
    /*function id_queue($wfs_response) {

        if (!is_array($wfs_response)) {
            $j = json_decode($wfs_response);
            if (is_object($j)) {
                $wfs_response = array();
                $wfs_response['data'] = $j->{'data'};
                $wfs_response['id']   = $j->{'id'};
                $wfs_response['geom'] = $j->{'geom'};
                if (isset($j->{'history'}))
                    $wfs_response['history'] = $j->{'history'};
            } else {
                //save to admin
                $this->error[] = "Invalid results object.";
                return false;
            }
        }

        //get the data rows' ids into response_id Array
        //this array will be processed in the results_query
        $n = 0;
        $ids = array();
        foreach ($wfs_response['id'] as $i) {

            preg_match("/[a-z]?+([0-9]+)/i", $i,$m);
            if (isset($m) and count($m)) {
                $ids[$n] = $m[1];
                $n++;
            }
        }
        //talán segítek vele a postgresnek...
        //vagy szivatom a httpd-t
        asort($ids);
        $this->response_id = $ids;
        return true;
    }*/

    //Kap egy json objectet ami control infókat és leékrdezendő id-kat tartalmaz
    //
    function printOut() {
        global $ID,$BID;
        //if (trim($wfs_response)=='') return "Empty WFS response";
        //

        $html_output = "";
        $ks = array();

        $st_col = st_col($_SESSION['current_query_table'],'array');

        // CLEAR morefilter table
        if (isset($_SESSION['morefilter']) and $_SESSION['morefilter']!='clear') {

            array_reverse(array_filter($_SESSION['filter_type']));
            $ft = implode(" <i class='fa fa-arrow-right fa-lg'></i><br> ",$_SESSION['filter_type']);

            if ($_SESSION['morefilter']=='off')
                $html_output .= str_filters_in_this_query.":<div style='color:#333;font-size:80%;font-weight:normal;margin:10px'>$ft</div><button class='pure-button button-passive button-small'>".str_new_filter."</button>";
            else
                $html_output .= str_filters_in_this_query.":<div style='color:#333;font-size:80%;font-weight:normal;margin:10px'>$ft</div><button class='pure-button button-warning button-small clear-morefilter'>".str_new_filter."</button>";
            $html_output .= "<br><br>";
        }

        //Too long wfs response handling...
        //its affects the wfs display and the slide output as well
        if (isset($_SESSION['wfs_rows'])) {
            $lp = ceil($_SESSION['wfs_rows']/2000);
            $m = "Only 2000 records loaded from the {$_SESSION['wfs_rows']}. Load more: ";
            $m .= pager(2000,$_SESSION['wfs_rows'],$_SESSION['wfs_rows_counter']-2000);
            $html_output .= $m."<br>";
        }

        /*
         * */
        $data = new formatData();
        $data->modules = $this->modules;
        //push column names to $data->csv_header
        $data->csv_head($this->rq['columns']);

        if ($this->modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
            $pos = array_search('obm_files_id', array_keys($data->csv_header));
            if ($pos > 0) {
                $pos++;
                $data->csv_header = array_slice($data->csv_header, 0, $pos, true) + ["filename" => str_filename] + array_slice($data->csv_header, $pos, count($data->csv_header)-$pos, true);
            }
        }
        // Summary
        // html output
        $summary_output = "";
        if ($this->summary=='on')
            $summary_output .= $this->modules->_include('results_summary','print_table');

        // Species list
        // html output
        if ($this->specieslist=='on') {

            if ($this->modules->is_enabled('results_specieslist')) {
                //$b= sprintf("%s",button("queries.php?splist=list.txt",str_download_specieslist,'fa fa-download'));
                $summary_output .= sprintf('<div id="specieslist" style="position:relative;padding-left:25px"><div style="position:absolute;top:0px;left:0px"><i id="specieslist-toggle-icon" class="fa fa-plus fa-lg specieslist-toggle" style="color:rgb(66, 184, 221)"></i></div><span class="specieslist-toggle">%s</span>: <div id="specieslist-content"></div></div>',t(str_specieslist));

                $summary_output .= sprintf('<div id="citelist"    style="margin-top:15px;position:relative;padding-left:25px"><div style="position:absolute;top:0px;left:0px"><i id="citelist-toggle-icon" class="fa fa-plus fa-lg citelist-toggle" style="color:rgb(66, 184, 221)"></i></div><span class="citelist-toggle">%s</span>: <div id="citelist-content" style="font-size:125%s;margin-top:10px"></div></div>',str_inthisquerythefollowing,'%');
            }
        }

        if ($summary_output != '')
            $html_output .= sprintf('<div><span class="mtitle"><a name="nav_summary">%s</a></span><div class="options-box">%s</div></div>',t(str_report),$summary_output);

        // Save options buttons
        // html output
        if ($this->buttons=='on') {

            $buttons = $this->modules->_include('results_buttons','print_buttons',array(['buttons'=>$this->buttons]));
            if ($buttons != '')
                $html_output .= "<div style='width:600px'><span class='mtitle'><a name='save_options'>".t(str_save_options)."</a></span><div class='options-box' style='display:none'>$buttons</div></div>";
        }

        //edit option
        if ($this->modules->is_enabled('massive_edit')) {

            $html_output .= $this->modules->_include('massive_edit','print_button');
        }

        // View option buttons
        $html_output .= "<div style='width:600px'><span class='mtitle'><a name='view_options'>".t(str_view_options)."</a></span></div>";

        if ($this->modules->is_enabled('results_asTable')) {
            $html_output .= sprintf("%s ",button('modules/results_asTable.php?show',t(str_as_full_table),'fa fa-eye'));
        }

        /*if (!isset($_SESSION['show_results'])) {
            $_SESSION['show_results'] = array();

            if(defined('DEFAULT_VIEW_RESULT_METHOD'))
                $_SESSION['show_results']['type'] = constant("DEFAULT_VIEW_RESULT_METHOD");
            else
                $_SESSION['show_results']['type'] = 'list';

            $_SESSION['show_results']['visible'] = 'on';
        }*/

        if ($this->modules->is_enabled('results_asList')) {
            $marked = "";
            if ($_SESSION['show_results']['type']=='list')
                $marked = "button-darkgray";
            $html_output .= sprintf("%s ",button('?',t(str_as_foldable_list),'fa fa-eye','',"button-href pure-button changeView $marked",'type=list'));
        }
        if ($this->modules->is_enabled('results_asStable')) {
            $marked = "";
            if ($_SESSION['show_results']['type']=='stable')
                $marked = "button-darkgray";
            $html_output .= sprintf("%s ",button('?',t(str_as_compact_table),'fa fa-eye','',"button-href pure-button changeView $marked",'type=stable'));
        }

        // Results outputs
        // Rolltable - big table
        // Set: SESSION['roll_array']
        if ($this->method=='rolltable') {

            if ($this->modules->is_enabled('results_asTable')) {
                //default itegrated module
                return $this->modules->_include('results_asTable','init_table',array($data));
            } else {
                echo common_message('error','Display results table module not enabled');
                log_action('results_asTable module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
            //pg_close($ID);
            return true;

        } elseif ($this->method=='html_table') {
            // create_csv()
            // Results in simple, limited html table
            //
            $data->csv_header['upid'] = t(str_dataupid);
            $data->csv_header['uploader_name'] = t(str_datauploader);

            if ($this->modules->is_enabled('results_asHtmlTable')) {
                $table = $this->modules->_include('results_asHtmlTable', 'print_table',array($data,$this->current_rows));
            }
            else {
                log_action('results_asHtmlTable module not enabled!',__FILE__,__LINE__);
                $dummy = new createTable();
                $dummy->def(['tid'=>'mytable','tclass'=>'resultstable']);
                $table = $dummy->printOut();
            }
            echo $table;

        } elseif ($this->method=='slide') {
            // create_csv()
            // Internal output. Called on using results_asList in "list" method
            // creates openbiomaps slides
            // These will be stored in _SESSION['slide'] variables and
            // printed out from read_results.php
            //$mki = array_search('results_listSlide', array_column($this->modules, 'module_name'));
            if ($this->modules->is_enabled('results_asList')) {
                //default itegrated module
                $slide = $this->modules->_include('results_asList','print_slide',array($data,$this->rq['columns'],['slide'=>$this->slide]));
                echo $slide;
            } else {
                echo common_message('error','Display results slide module not enabled');
                log_action('results_listSlide module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
            //pg_close($ID);
            return true;

        } elseif ($this->method=='csv') {
            //create data->csv_rows
            #$data->csv_header['upid'] = t(str_dataupid);
            #$data->csv_header['uploader_name'] = t(str_datauploader);
            #$data->csv_header['uploading_date'] = t(str_upload_date);

            $data->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'upid'=>str_dataupid),$data->csv_header);

            // Results as CSV
            // the called functions print out
            if ($this->modules->is_enabled('results_asCSV')) {
                //default itegrated module
                $this->modules->_include('results_asCSV','print_data',array($this->filename,$this->quote,$this->sep,$data));
            } else {
                echo common_message('error','CSV results module not enabled');
                log_action('results_asCSV module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
            //pg_close($ID);
            return true;

        } elseif ($this->method=='json') {

            $data->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'upid'=>str_dataupid),$data->csv_header);
            // module not exists yet!
            if ($this->modules->is_enabled('results_asJSON')) {
                //default itegrated module
                $this->modules->_include('results_asJSON','print_data',array($this->filename,$data));
            } else {
                echo common_message('error','JSON results module not enabled');
                log_action('results_asJSON module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }

            return true;
        } elseif ($this->method=='shape') {
            // module not exists yet!
            if ($this->modules->is_enabled('results_asSHP')) {
                //default itegrated module
                $this->modules->_include('results_asSHP','print_data',array($this->filename,$data));
            } else {
                echo common_message('error','SHP results module not enabled');
                log_action('results_asSHP module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }

            return true;
        } elseif ($this->method=='gpx') {
            // Results as GPX
            // the called functions print out
            if ($this->modules->is_enabled('results_asGPX')) {
                //default itegrated module
                $this->modules->_include('results_asGPX','print_data',array($this->filename,$data));
            } else {
                echo common_message('error','GPX results module not enabled');
                log_action('results_asGPX module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
            //pg_close($ID);
            return true;

        } elseif ($this->method=='list') {
            //$_SESSION['roll_array'] = array();
            if ($this->modules->is_enabled('results_asList')) {
                //default itegrated module
                $ks = $this->modules->_include('results_asList','print_roll_list',array($data));

                if ($this->rq['results_count'])
                    $html_output .= "<div id='slider' class='slider'></div>";
                else
                    $html_output = "";
            } else {
                echo common_message('error','list results module not enabled');
                log_action('results_asList module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
        } elseif ($this->method=='stable') {

            $data->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'upid'=>str_dataupid),$data->csv_header);

            // Resulting a html table
            if ($this->modules->is_enabled('results_asStable')) {
                //default itegrated module: roller table

                if ($this->rq['results_count']) {
                    $ks = $this->modules->_include('results_asStable','print_roll_table',array($data));
                    $html_output .= "<div id='slider' class='slider'></div>";
                } else
                    $html_output = "";

            } else {
                echo common_message('error','small table results module not enabled');
                log_action('results_asStable module not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
        } elseif ($this->method=='off') {
            /*
             * */
        } else {
            echo common_message('error','Undefined output method: '.$this->method);
            return false;
            # R object, plot, ...?
        }
        #if (isset($_SESSION['Tid'])) {
            // prepare_vars: SaveQuery()
            // Its works if the SaveQueryTemp called before

        session_write_close();

        //if ($this->method!='stable' and $this->method!='list')
        //    pg_close($ID);

        #$res = system("echo '$csv' | R --vanilla --slave \"a<-read.csv(pipe('cat /dev/stdin'),sep='#'); summary(a)\"");
        #print "<pre>$res</pre>";

        $l = array("res"=>$html_output,"geom"=>explode(',',$this->rq['extent']),'rowHeight'=>$_SESSION['show_results']['rowHeight'],'cellWidth'=>$_SESSION['show_results']['cellWidth'],'arrayType'=>$_SESSION['show_results']['type'],'borderRight'=>$_SESSION['show_results']['borderRight'],'borderBottom'=>$_SESSION['show_results']['borderBottom'],'scrollbar_header'=>json_encode($ks));
        echo common_message('ok',$l);

        return true;
    }

    function results_length() {
        return $this->rq['results_count'];
    }

    // slide function
    // fetch data from postgres result object
    // set: current_rows
    // set: current_rows_length
    // EZ még obm 1.0 xml processing volt
    function fetch_data() {
        global $ID,$BID;
        $new_query = 1;
        //reload page clear result variables
        //default state
        //When not used?
        //read_results.php -> read only one slide data
        //printGPX -> read current max 2000 records to export GPX
        if ($this->clear_current_rows=='on') {
            unset($_SESSION['wfs_rows']);
            unset($_SESSION['wfs_rows_counter']);
            $new_query = 1;
        }

        //prepare sql query and columns list

        // create a new current_rows array - default state
        if($new_query) {
            $error[] = $this->rq['query'];
            #log_action("query:".$this->rq['query'],__FILE__,__LINE__);
            if (isset($this->rq['query']) and $this->rq['query']!='') {
                //
                //<<<<<<<<<<<<<<<<<<< MAIN QUERY EXECUTION >>>>>>>>>>>>>>>>>>>
                //
                $result = pg_query($ID,$this->rq['query']);
            } else {
                //should be saved for admins
                $this->error[] = "An error occured while preparing the database query. Please contact the database maintainers.";
                return false;
                // error message: no query prepared
            }

            if(pg_last_error($ID)){
                // Wrong query
                $this->error[] = "Query error: ";
                $this->error[] = $this->rq['query'];
                return false;
                //should be saved for admin
            } elseif (pg_num_rows($result)==0) {
                /* Empty result
                 * Read the wfs response instead of database query.
                 */
                $this->error[] = "No database results in this query.<br>";
                log_action("results_builder: no-data:  {$this->rq['query']}",__FILE__,__LINE__);

                return false;
            } else {
                # Postgres query result
                $this->current_rows_length = pg_num_rows($result);
                //MEMORY!!!
                //while($row = pg_fetch_assoc($result)) {

                //}
                $this->current_rows = pg_fetch_all($result);
            }
        } else {
            //reread results_builder without
            //clear_current_rows == off
            //button list/stable, gpx

            if(isset($_SESSION['current_rows'])){
                $this->current_rows_length = count($_SESSION['current_rows']);
                $this->current_rows = $_SESSION['current_rows'];
            }
        }
        //reverse order
        if ($this->reverse=='on') {
            //log_action('reverse');
            $reverse_rows = array();
            for($i=0;$i<$this->current_rows_length;$i++) {
                $reverse_rows[] = array_pop($this->current_rows);
            }
            $this->current_rows = $reverse_rows;
            unset($reverse_rows);
        }

        // No rows error message
        if (!$this->current_rows_length) {
            $this->error[] = "Empty results object.";
            return false;
        }

        /* Data content restriction based on rules
         * It can remove some columns from the original table or change contents based on complex rules
         * There are other ACCESS LEVEL HANDLING mechanism for general access rule handlings!
         * Use this only for very special non standard cases! Maybe never...
         * It was very firs module for access rule handlings (for Milvus) and I keept it as special module.
         * */
        if($this->modules->is_enabled('restricted_data')) {
            $this->current_rows = $this->modules->_include('restricted_data','rule_data',array($this->current_rows));
        }

        return $this->current_rows_length;
    }
    /* Ez a függvény szedi össze a webes adatmegjelenítéshez az oszlop neveket és fűzi össze az SQL SELECT szöveget a lekérdezéshez.
     * Rafinált, mert az SQL lekérdezés már lefutott (results_query.php/proxy.php) és a visszaadott ID-k ott vannak a temp_--- táblában - itt már csak formázgatunk
     * viszont ennek a formázásnak összhangban kell lennie a mapfile formázásaival!!!
     *
     * Nem jelenít meg semmit, nem hajtja végre a lekérdezést!
     * USE user specific modules to call user functions to EXTEND the SQL query:
     * additional_columns:
     * join_extension:
     * column_names:
     * only_columns:
     * */
    function results_query($id_list = '',$do_not_create_temp_table = false) {
        global $BID,$ID;

        if (!isset($_SESSION['current_query_table']))
            $_SESSION['current_query_table'] = PROJECTTABLE;

        $st_col = st_col($_SESSION['current_query_table'],'array');

        /*
         * $this->rq['extent']
         * */
        if (isset($_SESSION['do-not-create-query'])) {
            unset($_SESSION['do-not-create-query']);
            $this->rq['columns'] = dbcolist('array',$_SESSION['current_query_table']);

            $cmd = sprintf("SELECT concat_ws(',',ST_XMIN(ST_Extent(obm_geometry)),ST_YMIN(ST_Extent(obm_geometry)),ST_XMAX(ST_Extent(obm_geometry)),ST_YMAX(ST_Extent(obm_geometry))) as extent
                        FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id());
            $res = pg_query($ID,$cmd);
            $row = pg_fetch_assoc($res);
            $this->rq['extent'] = $row['extent'];
            $this->rq['results_count'] = 1;

            return true;
        }

        if (isset($st_col['SPECIES_C']))
            $SPECIES_C = $st_col['SPECIES_C'];
        else
            $SPECIES_C = "";
        if (isset($st_col['GEOM_C']) and $st_col['GEOM_C']!='') {
            $GEOM_C = $st_col['GEOM_C'];
            $GEOM_NAME = $GEOM_C;
            $GEOM_C = $_SESSION['current_query_table'].".".$GEOM_C;
        } else {
            $GEOM_C = '';
        }

        $historder = "";
        //$histcol = "";
        $main_columns_visible_names = array();
        $additional_columns_visible_names = array();
        $joined_table_columns_visible_names = array();

        // flexible data table

        $sqla = sql_aliases(1);
        $qtable = $sqla['qtable'];
        $qtable_alias = $sqla['qtable_alias'];
        $FROM = $sqla['from'];
        $GEOM_C = $sqla['geom_col'];
        $geom_alias = $sqla['geom_alias'];
        $id_col = "$qtable_alias.obm_id";

        /* QUERY COLUMNS LIST
         * first:       obm_id (qtable=PROJECTTABLE) followed by the geometry (header_names!!)
         * secondly:    uploadings columns, modifier_id
         * thirdly:     dbcolist main column names || no_login_columns
         *
         * */
        $c = array();
        $query_cmd = array();
        $c[] = $id_col;

        // geometry data displayed as WKT text
        if ($GEOM_C != '') {

            if ($grid_mki = $this->modules->is_enabled('grid_view')) {
                if (isset($_SESSION['display_grid_for_map'])) {
                    $grid_geom_col = $_SESSION['display_grid_for_map'];
                    $geom_col = 'ST_AsText(qgrids.'.'"'.$grid_geom_col.'"'.')';
                } else {
                    $grid_geom_col = $this->modules->_include('grid_view','default_grid_geom');
                    $geom_col = 'ST_AsText(qgrids.'.'"'.$grid_geom_col.'"'.')';
                }

            } else
                $geom_col = "ST_AsText($GEOM_C)";
        }
        else
            $geom_col = '';

        /* module functions
         * TRANSFORM GEOMETRY: default is postgis St_SnapToGrid function for points location transformation
         *
         * */
        /*$module_name = array_values(array_intersect(array('snap_to_grid','transform_geometries','transform_geometry'), array_column($this->modules, 'module_name')));
        $mki = (count($module_name)) ? array_search($module_name[0], array_column($this->modules, 'module_name')) : false;
        if ($mki!==false) {
            //default itegrated module
            $dmodule = new defModules();
            if (!isset($_SESSION['load_loadquery'])) {
                $geom_col = $dmodule->snap_to_grid('geom_column',$this->modules[$mki]['p'],NULL);
            }
        }*/
        // what is this??
        if (preg_match("/$qtable\./",$geom_col))
            $geom_col = preg_replace("/$qtable\./","$qtable_alias.",$geom_col);


        $c[] = "$geom_col as obm_geometry";

        //$c[] = $histcol;
        $c[] = "uploadings.uploading_date";
        $c[] = "uploadings.uploader_name";
        $c[] = "uploadings.uploader_id";
        $c[] = "$qtable_alias.obm_validation as data_eval";
        $c[] = "uploadings.id as upid";
        $c[] = "$qtable_alias.obm_modifier_id";
        /* if rules table exists for the main table!!! */
        if ($st_col['USE_RULES']) {
            $c[] = sprintf("ARRAY_TO_STRING(data_rules.read,',') as obm_read");
            $c[] = sprintf("ARRAY_TO_STRING(data_rules.write,',') as obm_write");
            $c[] = sprintf("data_rules.sensitivity as obm_sensitivity");
        }

        //only column names
        $oc = dbcolist('columns',$qtable);

        //list of columns with visable names without prefix
        //it can be longer than the real column list....
        $main_columns_visible_names = dbcolist('array',$qtable);

        //kidobjuk a formázatlan geometriát
        if (isset($GEOM_NAME))
            unset($oc[array_search($GEOM_NAME,$oc)]);

        //table_name.column formatting
        array_walk($oc, 'prefix', $qtable_alias);
        $c = array_merge($c,$oc);

        /* module functions
         *
         * ADDITIONAL COLUMNS
         * The joined table's columns
         * */
        if ($this->modules->is_enabled('additional_columns')) {
            // send module params
            $ret = $this->modules->_include('additional_columns','return_columns');
            $c = array_merge($c,$ret[0]);
            $additional_columns_visible_names = $ret[1];
        }

        /* JOIN with UPLOADING table */
        $query_cmd[] = $FROM . join_table('uploadings', compact('qtable_alias') );

        /* JOIN with grid table if module enabled */
        if ($grid_mki) {
            $query_cmd[] = join_table('grid',compact('id_col','qtable') );
        }

        /* if rules table defined */
        /* JOIN with RULES table */
        if ($st_col['USE_RULES']) {
            $query_cmd[] = join_table('rules', compact('id_col','qtable') );
        }

        if ($this->modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
            $query_cmd[] = join_table('filename', compact('qtable'));
            $c[] = 'fj.filename';
        }

        /* On the fly JOIN tables IN the results query
         * not nice, complicated
         * module functions
         * DEPRECATED - will be removed the join_atble module?!
         *
         * JOIN_EXTENSION: join other tables to the main table
         * extend the the query command
         * Here we can extend our query result with data which not exists in the original query (project_queries)
         * */
        /*$this->rq['joins_vnames'] = '';
        $defined_visible_names_array = array();
        $mki = array_search('join_tables', array_column($this->modules, 'module_name'));
        if ($mki!==false) {
            $dmodule = new defModules();
            // send module params
            $ret = $dmodule->join_tables('return_joins',$this->modules[$mki]['p']);

            //this will be used for modifying joined table data on data_sheet_loader
            $query_cmd[] = $ret[0]; // appending the join_tables to the query's JOIN part
            $c = array_merge($c,$ret[1]); // table.column array
            $defined_visible_names_array = $ret[3]; // visible names array of array
            $this->rq['joins_vnames'] = $ret[2];
            $this->rq['joins'] = $ret[0];
        }*/

        /* Drop the non unique names!!!
         * which has a small chance thanx to the prefixes, however they can cause problems while creating the temporary table because the duplicated prefixed column names there will non-unique again!!!
         * */
        $cu = array_unique($c);
        if (count($c) != count($cu)) {
            log_action("The `where` non-unique column names in the query which automatically filtered: ".implode(',',array_diff_key($c,$cu)),__FILE__,__LINE__);
            $c = $cu;
        }


        /* sofisticated column name modification for joined tables if there are the same names in the different tables
         * the main problem is, that these names break the temporary table creation, therefore here I change the table.column style to
         * table_column
         * this should be triggered in the visible column names: table_column AS predefined names
         * */
        $cfk = array();
        foreach($c as $cf) {
            // split the columns array along the prefix separator
            $cfp = preg_split('/\./',$cf);
            if (count($cfp)==2) {
                $cfk[] = $cfp[1];
            } else
                $cfk[] = $cfp[0];

        }
        // if this condition is true
        // we should rename the non unique columns names and give AS name for them
        if (count($cfk) != count(array_unique($cfk))) {
            //we are here because there are some non unique names
            //difference array
            //$d[3] = 'foo'
            // Itt felvesszük azokat az oszlopokat amik azonos néven vannak a JOIN táblában mint a MAIN táblában
            $d = array_diff_key($cfk,array_unique($cfk));
            foreach(array_keys($d) as $k) {
                if(preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$c[$k],$m)) {
                    $l = sprintf("%s",preg_replace('/\./','_',$c[$k]));
                    $label = $m[2];
                    //if (isset($defined_visible_names_array[$m[1]])) {
                    //    $label = $defined_visible_names_array[$m[1]][$m[2]];
                    //}
                    $joined_table_columns_visible_names[$l] = $label;
                    $c[$k] = sprintf("%s AS %s",$c[$k],$l);
                }
            }
            // Itt felvesszük azokat amik eltérő néven vannak
            /*foreach($defined_visible_names_array as $k) {
                foreach($k as $key=>$value) {
                    $joined_table_columns_visible_names[$key] = $value;
                }
            }*/

        } /*else {
            foreach($defined_visible_names_array as $k) {
                foreach($k as $key=>$value) {
                    $joined_table_columns_visible_names[$key] = $value;
                }
            }
            }*/

        /* merge the three sources of columns
         * */
        $this->rq['columns'] = array_merge(array('hist_time'=>t(str_last_modify)),$main_columns_visible_names,$additional_columns_visible_names,$joined_table_columns_visible_names);
        //log_action($this->rq['columns'],__FILE__,__LINE__);

        //eliminates the empty elements if there are some!
        array_unshift($query_cmd,implode(',',array_filter($c)));
        array_unshift($query_cmd,"SELECT");

        /* ORDER BY
         *  used by modules, e.g. results_asStable
         *  */

        if (!isset($_SESSION['orderby'])) {
            $_SESSION['orderby'] = 'obm_id';
            $_SESSION['orderad'] = 1;
            $_SESSION['orderascd'] = '';
        }
        if (!isset($_SESSION['orderad']) or $_SESSION['orderad']==0) {
            $_SESSION['orderad'] = 1;
        }

        if ($this->reverse != '') {
            $_SESSION['orderad'] = $_SESSION['orderad']*-1;
            $OD = ($_SESSION['orderad']<0) ? 'ASC' : 'DESC';
            $_SESSION['orderby'] = $this->reverse;
            $_SESSION['orderascd'] = $OD;
        }
        elseif ($SPECIES_C != '') {
            if ($_SESSION['orderby'] == $this->reverse) {
                $_SESSION['orderad'] = $_SESSION['orderad']*-1;
                $OD = ($_SESSION['orderad']<0) ? 'ASC' : 'DESC';
            } else {
                $OD = 'ASC';
            }
            $_SESSION['orderby'] = $SPECIES_C;
            $_SESSION['orderascd'] = $OD;
        } else {
            $_SESSION['orderby'] = 'obm_id';
            $_SESSION['orderascd'] = '';
        }

        /* cmd CONDITION
         * */
        if ($id_list=='')
            $id_filter = sprintf("SELECT obm_id FROM temporary_tables.temp_%s_%s",PROJECTTABLE,session_id());
        else
            # only one row -> e.g. result_slide
            $id_filter = $id_list;

        // sensitive data handling in queries
        $sensitive = '';
        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        // visible columns restriction
        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $main_cols[] = 'obm_id';
        $main_cols[] = 'obm_uploading_id';
        $main_cols[] = 'filename';
        $allowed_cols = $main_cols;

        if ($this->modules->is_enabled('allowed_columns')) {
            //default itegrated module
            $allowed_cols = $this->modules->_include('allowed_columns','return_columns',array($main_cols));
        } else {
            $ACC_LEVEL = ACC_LEVEL;
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                // drop all column for non logined users if access level is higher
                $allowed_cols = array();
            }
        }

        /* drop lines if sensitivity problems meet with allowed columns restriction
         *
         * */
        $puy = 1;
        if (isset($_SESSION['current_query_keys'])) {
            $intersected_allowed_columns_count = count(array_intersect($_SESSION['current_query_keys'],$allowed_cols));
            $query_column_count = count($_SESSION['current_query_keys']);
            if ($intersected_allowed_columns_count != $query_column_count) {
                $puy = 0;
            }
        }

        if (!has_access('master') and $st_col['USE_RULES']) {
            $sensitive = " AND ( (sensitivity::varchar IN ('3','only-owner') AND ARRAY[$tgroups] && read) ";
            $sensitive .= "   OR (sensitivity IS NULL OR sensitivity::varchar IN ('0','public') ) ";
            $sensitive .= "   OR (sensitivity::varchar IN ('2','restricted') AND ARRAY[$tgroups] && read)";
            $sensitive .= "   OR (sensitivity::varchar IN ('1','no-geom'))";
            $sensitive .= "   OR (sensitivity::varchar IN ('2','restricted') AND 1=$puy) ) ";
        }

        //$query_cmd[] = sprintf('WHERE "%s".obm_id IN (%s) %s %s',$qtable_alias,$id_filter,$sensitive,$ORDERBY);
        $query_cmd[] = sprintf('WHERE "%s".obm_id IN (%s) %s',$qtable_alias,$id_filter,$sensitive);

        // implode final sql command
        $this->rq['query'] = implode(" ",$query_cmd);
        //log_action($this->rq['query'],__FILE__,__LINE__);

        // data sheet page call
        // skip creating temp table, because we do not fire mapserver action
        if ($do_not_create_temp_table)
            return true;

        // push selected result into temp_ table
        // for map query
        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
        // It is fast - ok!
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_query_%s_%s AS %s",PROJECTTABLE,session_id(),$this->rq['query']);
        //debugging query
        //log_action($cmd,__FILE__,__LINE__);
        $res = pg_query($ID,$cmd);
        if(!pg_affected_rows($res)) {
            if (pg_last_error($ID) or $res===false) {
                log_action(pg_last_error($ID),__FILE__,__LINE__);
                log_action($cmd,__FILE__,__LINE__);
            } else {
                if (isset($_SESSION['filter_type'])) {
                    unset($_SESSION['filter_type']);
                }

                // empty query
            }
            $this->rq['results_count'] = 0;
        } else {
            $this->rq['results_count'] = pg_affected_rows($res);
        }

        $cmd = sprintf("GRANT SELECT ON temporary_tables.temp_query_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
        pg_query($ID,$cmd);


        $cmd = sprintf("SELECT concat_ws(',',ST_XMIN(ST_Extent(obm_geometry)),ST_YMIN(ST_Extent(obm_geometry)),ST_XMAX(ST_Extent(obm_geometry)),ST_YMAX(ST_Extent(obm_geometry))) as extent
                        FROM temporary_tables.temp_query_%s_%s f",PROJECTTABLE,session_id());
        if (!has_access('master') and $st_col['USE_RULES']) {
            $cmd .= join_table('rules', compact( 'qtable', 'id_col'));
            $cmd .= " WHERE  (sensitivity::varchar IN ('1','3','no-geom','only-owner') AND ARRAY[$tgroups] && read) ";
            $cmd .= "   OR sensitivity IS NULL OR sensitivity::varchar IN ('0','2','public','restricted') ";
        }

        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $this->rq['extent'] = $row['extent'];

        update_temp_table_index('query_'.PROJECTTABLE.'_'.session_id());

        return true;
    }
}



# results list csv table printout class functions
class formatData {
    public $modules = array();
    public $def=array('r_id'=>'','u_id'=>'','u_date'=>'','upl_id'=>'','upl_name'=>'','r_spec'=>'');
    public $header_ref;
    public $body_ref;
    public $csv_rows=array(); // two dimensional associate array contains all queried data, the
    public $csv_header=array();
    //public $uspecies=array();
    public $cite=array();

    function def($arr) {
        foreach ($arr as $key=>$val) {
            if ($key == 'r_id') $this->def['r_id'] = $val;
            elseif ($key == 'u_id') $this->def['u_id'] = $val;
            elseif ($key == 'u_date') $this->def['u_date'] = $val;
            elseif ($key == 'upl_id') $this->def['upl_id'] = $val;
            elseif ($key == 'upl_name') $this->def['upl_name'] = $val;
            elseif ($key == 'r_spec') $this->def['r_spec'] = $val;
            elseif ($key == 'r_valid') $this->def['r_valid'] = $val;
        }
    }
    // assemble slide
    // called in resultsAsList module !!!!
    function list_ref_ass($slide_id,$slide_title) {

        // slide header reference
        if ($slide_title!='') $slide_title = " - $slide_title";

        $dh = sprintf('<p class="toggler" id="toggler-slide%1$s">%2$s (ID: %3$s%4$s)<span class="expandSlider"><i class="fa fa-folder-o"></i></span><span class="collapseSlider"><i class="fa fa-folder-open-o"></i></span></p>',$slide_id,str_dataheader,preg_replace("/^NULL$/","undefined",$slide_id),$slide_title);
        // slide body reference
        $t = 'b'.$slide_id;
        $dh .= sprintf('<p class=\'toggler\' id=\'toggler-slideb%1$s\'><span class="expandSlider"><i class="fa fa-folder-o"></i></span><span class="collapseSlider"><i class="fa fa-folder-open-o"></i></span>%2$s</p>',$slide_id,str_details.'...');
        return "<div class='data_block'>".$dh."</div>";
    }

    //cite persons
    //function cite_push($e) {
    //    $this->cite[]=$e;
    //}
    //data header columns
    function csv_head($e) {
        $this->csv_header=$e;
    }
    // data rows
    // put lon,lat and other fields in a row of a table
    function csv_push($geom,$e) {
        $n = count($this->csv_header);
        $csv = new SplFixedArray($n);//array();
        // correct header should be prepared previously
        // without it there will be no data in the csv table
        $i = 0;
        foreach($this->csv_header as $cn=>$cv) {
            if (isset($e[$cn]))
                //array_push($csv,$e[$cn]);
                $csv[$i] = $e[$cn];
            else
                //array_push($csv,'');
                $csv[$i] = '';
            $i++;
        }
        if($geom!='')
            $csv[0] = $geom;
        array_push($this->csv_rows,$csv->toArray());
    }
} // class end


?>
