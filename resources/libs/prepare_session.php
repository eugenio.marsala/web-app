<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

# set new array elements if new languges set in languages dir
# if php version is over php7 it can be moved to /etc/openbiomaps/system_vars.php.inc
$LANGUAGES = array('en'=>'in English','hu'=>'magyarul','ro'=>'română','ru'=>'русский');

if(getenv("PROJECT_DIR") !== false) {
    require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
} else {
    exit;
}

$token = array('projecttable'=>PROJECTTABLE,'session_id'=>session_id());

if (isset($_SESSION['token']) and $_SESSION['token']['projecttable']!=PROJECTTABLE) {
    // new database loaded
    $_GET['logout'] = 1;

} elseif (!isset($_SESSION['token'])) {
    $_SESSION['token'] = $token;
} elseif (isset($_SESSION['token']) and isset($_SESSION['token']['projecttable']) and $_SESSION['token']['projecttable']==PROJECTTABLE and $_SESSION['token']['session_id']!=session_id()) {
    #expired session_id
    $_SESSION['token'] = $token;
}

if (isset($_SESSION['current_query_table'])) {
    if (!preg_match('/^'.PROJECTTABLE.'/',$_SESSION['current_query_table'])) {
        $tables =getProjectTables();
        if (!in_array($_SESSION['current_query_table'],$tables))
            $_GET['logout'] = 1;
    }
}

if (!isset($_SESSION['current_query_table'])) { 
    if (defined('DEFAULT_TABLE'))
        $_SESSION['current_query_table'] = DEFAULT_TABLE;
    else
        $_SESSION['current_query_table'] = PROJECTTABLE;
}

if (!isset($_SESSION['modules'])) {
    $m = new modules(false);
    $_SESSION['modules'] = $m->set_modules();
}

//$_SESSION['LANG_SET_TIME'] = time();
?>
