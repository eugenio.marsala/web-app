<?php

class mainpage_functions {
    public $table;
    public $debug = FALSE;
    // data_table
    public $columns = array();
    public $headers = array();
    public $limit = 1000;
    public $orderby = 'obm_uploading_id';
    public $obm_id_order = ',obm_id DESC';
    public $order = 'DESC';
    // users' stat
    public $column = "";
    // default cache times in minutes
    public $count_members_cache = 60;
    public $count_data_cache = 10;
    public $count_uploads_cache = 10;
    public $count_species_cache = 30;
    public $stat_species_cache = 60;
    public $stat_species_user_cache = 2;
    public $most_active_users_cache = 60;
    public $force_update_cache = false;

    public function __construct() {
        $this->table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
    }

    function returnFunc($method,$args) {
        return call_user_func(array($this, $method), $args);
    }
    // a summary functions
    // a data table
    // default order is uploads
    function data_table($args) {

        if (!preg_match('/^'.PROJECTTABLE.'/',$this->table)) {
            return;
        }

        global $ID;
        $columns = implode(',',$this->columns);
        $cmd = sprintf("SELECT %s FROM %s ORDER BY %s %s %s LIMIT %s",$columns,$this->table,$this->orderby,$this->order,$this->obm_id_order,$this->limit);
        $res = pg_query($ID,$cmd);

        $table = new createTable();
        $table->def(['tid'=>'mytable','tclass'=>'pure pure-table pure-table-striped','tstyle'=>'width:100%']);

        // auto create link from obm_id column
        if ($this->columns[0] == 'obm_id' )
            $table->tformat(['format_col'=>'0','format_string'=>'<a href=\'?data&id=COL-0&table='.$this->table.'\' style="color:lightblue"><i class="fa fa-lg fa-newspaper-o"></i></a>']);

        while ($row=pg_fetch_assoc($res)) {
            $table->addRows($row);
        }

        $table->addHeader($this->headers);
        return $table->printOut();
    }
    // a summary functions
    // count members
    // Recommended CACHE: 60 minutes
    function count_members($project = PROJECTTABLE) {
        global $BID;

        $cnt = obm_cache('get',"count_members",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        $cmd = sprintf("SELECT count(*) as c FROM project_users WHERE project_table=%s AND user_status::varchar IN ('1','2','3','4','normal','master','operator','assistant')",quote($project));
        $result = pg_query($BID,$cmd);
        $row = pg_fetch_assoc($result);

        $time = $this->count_members_cache * 60;
        obm_cache('set',"count_members",$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count data
    // Recommended CACHE: 10 minutes
    function count_data() {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;

        $cnt = obm_cache('get',"count_data".$this->table,'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        $cmd = sprintf("SELECT count(obm_id) as c FROM \"%s\"",$this->table);
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        $time = $this->count_data_cache * 60;
        obm_cache('set',"count_data".$this->table,$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count uplods
    // Recommended CACHE: 10 minutes
    function count_uploads($project = PROJECTTABLE) {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$project)) return;

        $cnt = obm_cache('get',"count_uploads",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        $cmd = sprintf("SELECT count(id) as c FROM system.uploadings WHERE project=%s",quote($project));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        $time = $this->count_uploads_cache * 60;
        obm_cache('set',"count_uploads",$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count species
    // CACHE: 30 minutes
    public function count_species($use_taxon_db=FALSE) {
        global $ID;

        $st_col = st_col($this->table,'array');
        $SPECIES_C = $st_col['SPECIES_C'];

        //if ($table_ext!='')
        //    $table = PROJECTTABLE.'_'.$table_ext;

        if ($SPECIES_C!='') {
            $cnt = obm_cache('get',"count_species",'','',FALSE);
            if ($this->force_update_cache) $cnt = false;
            if ($cnt !== false )
                return $cnt;

            $sss = preg_replace('/[a-z0-9_]+\./','',$SPECIES_C);

            #count only valid names
            if ($use_taxon_db)
                $cmd = "SELECT COUNT(DISTINCT word) AS c FROM ".PROJECTTABLE."_taxon WHERE status IN (0,1,3,4) AND lang='".$SPECIES_C."' AND taxon_db>0";
            else
                $cmd = "SELECT COUNT($sss) AS c FROM (SELECT DISTINCT $SPECIES_C FROM ".$this->table." LEFT JOIN ".PROJECTTABLE."_taxon ON ($SPECIES_C=word) WHERE status IN (0,1,3,4)) AS temp";

            $result = pg_query($ID,$cmd);
            $row=pg_fetch_assoc($result);

            $time = $this->count_species_cache * 60;
            obm_cache('set',"count_species",$row['c'],$time,FALSE);
            return  $row['c'];
        }
    }
    // a summary functions
    // count species
    // CACHE: 60 minutes
    public function stat_species($use_taxon_db=FALSE,$stat_type=array('frequent'),$addlinks=true) {

        $table = obm_cache('get',"stat_species",'','',FALSE);
        if ($this->force_update_cache) $table = false;
        if ($table !== false )
            return $table;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        global $ID;
        $sum = "0";
        $count = "";
        $order = "cnt";
        $where = "";

        $st_col = st_col($this->table,'array');
        $qtable = "";
        if ($this->table!='PROJECTTABLE') {
            $t = preg_replace('/'.PROJECTTABLE.'_/','',$this->table);
            $qtable = "&qtable=$t";
        }
        $SPECIES_C = $st_col['SPECIES_C'];

        if ($SPECIES_C!='') {
            $count = "count($SPECIES_C)";
        }

        if ($st_col['NUM_IND_C']!='') {
            $NUM_IND_C = $st_col['NUM_IND_C'];
            $sum = "SUM($NUM_IND_C) AS cnt";
            $where = "WHERE $NUM_IND_C IS NOT NULL";
        } else {
            $count .= " AS cnt";
        }

        if ($SPECIES_C!='') {

            $table = "";

            if (in_array('rarest',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count, $sum FROM ".$table." $where GROUP BY $SPECIES_C ORDER BY $order LIMIT 3";
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small'>{$row['cnt']}</div>";
                }
                $table = "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_rarest_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }
            if (in_array('frequent',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count, $sum FROM ".$this->table." $where GROUP BY $SPECIES_C ORDER BY $order DESC LIMIT 6";
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small' style='text-align:right'>".number_format($row['cnt'])."</div>";
                }
                $table .= "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_most_frequent_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }

            $time = $this->stat_species_cache * 60;
            obm_cache('set',"stat_species",$table,$time,FALSE);
            return $table;
        }
    }
    // a summary functions
    // last upload date
    function last_upload($project = PROJECTTABLE) {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$project)) return;

        $cmd = sprintf("SELECT max(uploading_date) as d FROM system.uploadings WHERE project=%s",quote($project));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        return $row['d'];
    }
    // a summary functions
    // most frequent species per user
    // CACHE: 2 minutes
    function stat_species_user() {
        if (!isset($_SESSION['Tid']))
            return;

        $table = obm_cache('get',"stat_species_user");
        if ($this->force_update_cache) $table = false;
        if ($table !== false )
            return $table;

        global $ID,$BID;
        $st_col = st_col($this->table,'array');

        $table = $this->table;
        $species_c = $st_col['SPECIES_C'];

        // f_main_table????
        /*$cmd = sprintf("SELECT f_main_table,f_species_column FROM header_names WHERE f_table_name='%s' AND f_species_column IS NOT NULL",PROJECTTABLE);
        $result = pg_query($BID,$cmd);
        if (!pg_num_rows($result))
            return;
        else {
            while ($row = pg_fetch_assoc($result)) {
                $table = $row['f_main_table'];
                $species_c = $row['f_species_column'];
            }
        }*/

        $list = array();
        $cmd = sprintf('SELECT %1$s as f,count(%1$s) FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id WHERE uploader_id=%3$d GROUP BY %1$s ORDER BY count DESC LIMIT 5',$species_c,$table,$_SESSION['Trole_id']);
        $result = pg_query($ID,$cmd);
        while($row=pg_fetch_assoc($result)) {
            $list[] = "<div class='tbl-cell'>{$row['f']}</div><div class='tbl-cell' style='text-align:right'>{$row['count']}</div>";
        }
        $table = "<div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";

        $time = $this->stat_species_user_cache * 60;
        obm_cache('set',"stat_species_user",$table,$time);
        return $table;
    }
    // a summary functions
    // most active users: upload count
    // CACHE: 60 minutes
    function most_active_users($type) {
        global $ID;

        $rows = obm_cache('get',"most_active_users",'','',FALSE);
        if ($this->force_update_cache) $rows = false;
        if ($rows !== false )
            return $rows;

        if ($type == 'data') {
            if ($this->table == '') $this->table = PROJECTTABLE;
            if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;
            $cmd = "SELECT uploader_name,uploader_id,COUNT(*) as cn
                FROM system.uploadings u
                LEFT JOIN \"".$this->table."\" d ON (d.obm_uploading_id=u.id)
                WHERE uploader_id>0 AND obm_uploading_id IS NOT NULL
                GROUP BY uploader_name,uploader_id
                ORDER BY cn DESC LIMIT 100";
        }
        elseif ($type == 'uploads') {
            $cmd = sprintf("SELECT uploader_name,uploader_id,COUNT(*) as cn
                FROM system.uploadings u
                WHERE uploader_id>0 AND project=%s
                GROUP BY uploader_name,uploader_id
                HAVING COUNT(*)>1
                ORDER BY cn DESC LIMIT 100",quote(PROJECTTABLE));
        } elseif ($type == 'custom' and $this->column!='') {
            if ($this->table == '') $this->table = PROJECTTABLE;
            if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;
            $cmd = "SELECT COUNT(obm_id) as cn, \"".$this->column."\" AS uploader_name
                FROM \"".$this->table."\"
                GROUP BY \"".$this->column."\"
                ORDER BY cn DESC LIMIT 100";
            if ($this->debug)
                log_action($cmd);
        }

        $result = pg_query($ID,$cmd);
        # Mocskonyi Zsófia        12     912
        # Zágon András	         298     175
        # Selmeczi Kovács Ádám	 246      88
        # Bérces Sándor	           5      81
        # Mocskonyi Zsófia	 202      47

        $counter = array();
        while ($row = pg_fetch_assoc($result)) {
            if (!isset($counter[$row['uploader_name']]))
                $counter[$row['uploader_name']] = 0;
            $counter[$row['uploader_name']] += $row['cn'];

        }
        arsort($counter);
        $rows = array();
        foreach($counter as $name=>$cn) {
            $rows[] = array('uploader_name'=>$name,'uploader_id'=>0,'cn'=>$cn);
        }
        $rows = array_slice($rows, 0, 10);

        $time = $this->most_active_users_cache* 60;
        obm_cache('set',"most_active_users",$rows,$time);
        return $rows;
    }
    function view_table_links ($sep='<br>') {
        require_once(getenv('OB_LIB_DIR'). 'modules_class.php');

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $modules = new modules();
        $links = array();

        if ($modules->is_enabled('read_table',PROJECTTABLE)) {
            $l = $modules->_include('read_table','list_links');
            foreach ($l as $link) {
                //$links .= "<a href='$protocol://".URL."/includes/modules/results_asTable.php?view&table=$link'>$protocol://".URL."/includes/modules/results_asTable.php?view&table=$link</a>";
                //use short links!
                $links[] = "<a href='$protocol://".URL."/view-table/$link/' target='_blank'>$protocol://".URL."/view-table/$link/</a>";
            }
        }
        return implode($sep,$links);

    }
}
// include local mainpage functions
if (file_exists(getenv('PROJECT_DIR')."includes/private/mainpage_local_functions.php")) {

    require_once(getenv('PROJECT_DIR').'includes/private/mainpage_local_functions.php');

}

?>
