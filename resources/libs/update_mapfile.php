<?php
/*
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

// csak bejelentkezve és csak a saját profilunkat tudjuk szerkeszteni
if(!isset($_SESSION['Tid'])) {
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}
require_once(getenv('OB_LIB_DIR'). 'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

// Rights checking
if (!has_access('master')) exit;

$modules = new modules();
# mapfile updates
# openlayers updates


/* Mapfile Update
 * Ajax call: main.js
 * Effective call: project_admin.php
 */
if (isset($_POST['p'])) {
    $p = $_POST['p'];
    if ($p=='public' or $p=='private')
        $map = file(getenv('PROJECT_DIR')."$p/$p.map");

    if(!isset($map)) return;

    // replace @@variable@@ to something
    if (isset($_POST['tpost'])) {
        $j = json_decode($_POST['tpost']);
        $o = array();
        foreach ($map as $line) {
            for($i=0;$i<count($j->{'id'});$i++){
                $id =  $j->{'id'}[$i];
                $val = $j->{'val'}[$i];
                if($val=='') continue;
                $line = preg_replace("/$id/","$val",$line);
            }
            $o[] = $line;
        }
        $o = implode("",$o);
        #$fp=fopen(getenv('PROJECT_DIR')."/$p/$p.map",'w');
        #fwrite($fp,implode("",$o),strlen(implode("",$o)));
        #print implode("",$o);
    } elseif (isset($_POST['map_post'])) {
        // replace the complete map files
        $o = $_POST['map_post'];
    }

    if ($o!='') {
        $fp=fopen(getenv('PROJECT_DIR')."/$p/$p.map",'w');
        $writeres = fwrite($fp,$o,strlen($o));

        $cmd = "BEGIN;DELETE FROM project_mapserver_layers WHERE project_table='".PROJECTTABLE."'";
        pg_query($BID,$cmd);

        $cmds = '';
        for ($i=0;$i<count($_POST['mp']);$i++) {
            $cmds .= sprintf("INSERT INTO project_mapserver_layers (mapserv_layer_name,project_table,geometry_type) VALUES (%s,'".PROJECTTABLE."',%s);",quote($_POST['mp'][$i]),quote(strtoupper($_POST['mg'][$i])));
        }
        $res = pg_query($BID,$cmds);

        if (pg_affected_rows($res)) {
                pg_query($BID,'COMMIT');
                echo $o;
        } else {
                pg_query($BID,'ROLLBACK');
                echo common_message("error","Saving error");
        }
    }
    exit;
}
/* OpenLayers Definitions Update
 * Ajax call: main.js
 * Effective call: project_admin.php
 */
if (isset($_POST['op'])) {
    $drop_layer = 0;
    if ($_POST['def']=='')
        $drop_layer = 1;
    $id = preg_replace("/[^0-9new]/","",$_POST['op']);
    $name = preg_replace("/[^A-Za-z0-9-_]/","",$_POST['name']);
    $def = preg_split("/\n/",$_POST['def']);
    $def_array = $def;
    $def = array_map(function($a) { return preg_replace('/,$/','',$a); },$def);
    $def = quote(implode(',',$def));
    //$query = '';
    //if (preg_match('/FROM '.PROJECTTABLE.'/',$_POST['query'])) {
    //    $query = quote($_POST['query']);
    //}
    $order = preg_replace("/[^0-9]/","",$_POST['order']);
    if ($order == '') $order = 'NULL';
    $enab = preg_replace("/[^tf]/i","",$_POST['enab']);
    $enab = preg_replace("/t/i","TRUE",$enab);
    $enab = preg_replace("/f/i","FALSE",$enab);
    $url = quote($_POST['url']);
    $map = quote($_POST['map']);
    $type = preg_replace("/[^A-Za-z0-9-]/","",$_POST['type']);
    $desc = preg_replace("/[^A-Za-zöüóőúéáűíÖÜÓŐÚÉÁŰÍ0-9-_ ]/","",$_POST['desc']);
    //undefined index problem!!!
    $ms_layer = quote(preg_replace("/[^A-Za-z0-9-_]/","",$_POST['mslayer']));

    if ($id=='new') {
        $cmd = "INSERT INTO project_layers (project_table,layer_name,layer_def,tipus,url,map,name,enabled,layer_order,ms_layer) VALUES ('".PROJECTTABLE."','$name',$def,'$type',$url,$map,'$desc',$enab,NULL,$ms_layer) RETURNING id";
        $res = pg_query($BID,$cmd);
    } else {
        if (!$drop_layer)
            $cmd = "UPDATE project_layers SET layer_name='$name',layer_def=$def,tipus='$type',url=$url,map=$map,name='$desc',enabled=$enab,layer_order=$order,ms_layer=$ms_layer WHERE id='$id' AND project_table='".PROJECTTABLE."'";
        else
            $cmd = "DELETE FROM project_layers WHERE id='$id' AND project_table='".PROJECTTABLE."'";
       pg_query($BID,$cmd);
    }
    exit;
}
/* UPDATE SQL QUERY
 *
 * */
if (isset($_POST['qq'])) {
    $error = 0;
    //$name = preg_replace("/[^A-Za-z0-9-_]/","",$_POST['name']);
    $id = preg_replace("/[^0-9new]/","",$_POST['qq']);
    $cname = preg_replace("/[^A-Za-z0-9-_]/","",$_POST['cname']);

    if (!preg_match('/^layer_data_/',$cname))
        $cname = "layer_data_".$cname;

    $type = preg_replace("/[^querybas]/","",$_POST['type']);
    $enab = preg_replace("/[^tf]/i","",$_POST['enab']);
    $enab = preg_replace("/t/i","TRUE",$enab);
    $enab = preg_replace("/f/i","FALSE",$enab);
    $query = "''";

    $drop_query = 0;

    $main_table = PROJECTTABLE;
    $m = array();
    if (preg_match('/FROM (%F%)?'.PROJECTTABLE.'(_\w+)?/',$_POST['query'],$m)) {
        if (isset($m[2]))
            $main_table = PROJECTTABLE.$m[2];

        $qtable = $main_table;
        $qtable_alias = $qtable;

        if (preg_match('/%F%(.+)%F%/',$_POST['query'],$m)) {
            if (preg_match('/([a-z_]+) (\w+)/i',$m[1],$mt)) {
                $qtable = $mt[1];
                $qtable_alias = $mt[2];
            }
        }

        $id_col = "obm_id";
        $geom_col = $_SESSION['st_col']['GEOM_C'];
        $cmd1 = sprintf("SELECT layer_query,layer_type,rst
                            FROM project_queries
                            WHERE project_table='%s' AND enabled=TRUE AND main_table=%s ORDER BY rst DESC",
                                PROJECTTABLE,quote($main_table));
        $res = pg_query($BID,$cmd1);
        $row = pg_fetch_assoc($res);
        if (preg_match('/%F%(.+)%F%/',$row['layer_query'],$m)) {

            $FROM = sprintf('FROM %s',$m[1]);

            if (preg_match('/([a-z_]+) (\w+)/i',$m[1],$mt)) {
                $qtable = $mt[1];
                $qtable_alias = $mt[2];
                if (preg_match("/([a-z_]+)\.obm_geometry/",$row['layer_query'],$mg)) {
                    $geom_alias = $mg[1];
                    $geom_col = $geom_alias.".obm_geometry";
                }
                if (preg_match("/([a-z_]+)\.obm_id/",$row['layer_query'],$mg)) {
                    $id_alias = $mg[1];
                    $id_col = $id_alias.".obm_id";
                }
            }
            if (preg_match_all('/%J%(.+)%J%/',$row['layer_query'],$mj)) {
                foreach($mj[1] as $mje) {
                    $FROM .= " ".$mje;
                }
            }
        }

        $query = preg_replace('/;/','',$_POST['query']);
        $query = preg_replace('/ DROP /i','',$query);
        $query = preg_replace('/ ALTER /i','',$query);
        $query = preg_replace('/ INSERT /i','',$query);
        $query = preg_replace('/ DELETE /i','',$query);

        $q = preg_replace('/(\w+\.)?%grid_geometry%/',$geom_col,$query); # az aliasokat így nem kezeli!!!
        $q = preg_replace('/%transform_geometry%/','PLEASE REMOVE %transform_geometry% rule from the query',$q);
        $q = preg_replace('/%morefilter%/','',$q);
        $q = preg_replace('/%envelope%/','',$q);
        $q = preg_replace('/%geometry_type%/','',$q);
        $q = preg_replace('/%current_user%/',$_SESSION['Tid'],$q);
        $q = preg_replace('/%selected%/','',$q);
        $q = preg_replace('/%qstr%/','',$q);
        $q = preg_replace('/%taxon_join%/','',$q);
        $q = preg_replace('/%grid_join%/','',$q);
        $q = preg_replace('/%search_join%/','',$q);

        $q = preg_replace('/%F%/','',$q);
        $q = preg_replace('/%J%/','',$q);
        //$q = preg_replace('/%uploading_join%/','',$q);
        $q = preg_replace('/%uploading_join%/',join_table('uploadings', compact('qtable_alias')),$q);
        //$q = preg_replace('/%rules_join%/','',$q);
        $q = preg_replace('/%rules_join%/', join_table('rules', compact('qtable','id_col')),$q);

        $filename_join = ($modules->is_enabled('photos') && get_option('show_attachment_filenames')) ? join_table('filename',compact('qtable')) : "";
        $q = preg_replace('/%filename_join%/', $filename_join, $q);

        $q = preg_replace('/WHERE\s*$/i','',$q);
        $cmd = sprintf("EXPLAIN %s",preg_replace('/%[a-z_]+%/i',0,$q));
        $eres = pg_query($ID,$cmd);
        $ple = pg_last_error($ID);
        if (pg_num_rows($eres)) {
            $query = quote($query);
        } else {
            log_action("SQL query saving error: $ple\n$cmd",__FILE__,__LINE__);
            $query = quote("WRONG QUERY\n$query");
        }
    } else {
        if ($_POST['query']=='')
            $drop_query = 1;
        else
            log_action("project table not referred in the query: ".$_POST['query']);

    }
    $rst = preg_replace("/[^0123]/","",$_POST['rst']);
    //$geom_type = preg_replace("/[^pointlrseyg]/i","",$_POST['geom_type']);
    //$geom_type = preg_replace('/^line$/i','LINESTRING',$geom_type);

    if (!$error) {
        if ($id=='new') {
            //$cmd = "INSERT INTO project_queries (project_table,layer_name,layer_query,enabled,layer_type,rst,layer_cname,geom_type,main_table) VALUES ('".PROJECTTABLE."','$name',$query,$enab,'$type',$rst,'$cname',NULLIF('$geom_type',''),'$main_table')";
            $cmd = "INSERT INTO project_queries (project_table,layer_query,enabled,layer_type,rst,layer_cname,main_table) VALUES ('".PROJECTTABLE."',$query,$enab,'$type',$rst,'$cname','$main_table')";
            $res = pg_query($BID,$cmd);
        } else {
            if (!$drop_query)
                //$cmd = "UPDATE project_queries SET layer_name='$name',layer_query=$query,enabled=$enab,layer_type='$type',rst=$rst,layer_cname='$cname',geom_type=NULLIF('$geom_type',''),main_table='$main_table' WHERE id='$id' AND project_table='".PROJECTTABLE."'";
                $cmd = "UPDATE project_queries SET layer_query=$query,enabled=$enab,layer_type='$type',rst=$rst,layer_cname='$cname',main_table='$main_table' WHERE id='$id' AND project_table='".PROJECTTABLE."'";
            else
                $cmd = "DELETE FROM project_queries WHERE id='$id' AND project_table='".PROJECTTABLE."'";
            $res = pg_query($BID,$cmd);
        }
    }
    exit;
}
//pg_close($BID);
?>
