<?php
/* AJAX request functions
 *
 * */
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require(getenv('OB_LIB_DIR').'modules_class.php');
require(getenv('OB_LIB_DIR').'common_pg_funcs.php');

pg_query($ID,'SET search_path TO system,public,temporary_tables');
pg_query($GID,'SET search_path TO system,public,temporary_tables');

if(!isset($_SESSION['token'])) {
    require(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
require(getenv('OB_LIB_DIR').'languages.php');

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
$modules = new modules();


/* bottom panel, cookie warning
 *
 * */
if (isset($_POST['cookies_accepted'])) {
    $_SESSION['cookies_accepted'] = 1;
    exit;
}
/* Called: maps.js
 * Set current_query_table link
 * */
if (isset($_POST['set-current-query-table'])) {
    unset($_SESSION['current_query_keys']);
    unset($_SESSION['query_build_string']);
    unset($_SESSION['qf']);
    unset($_SESSION['filter_type']);
    //unset($_SESSION['']);
    //"qform":[],
    //"qf":{"szamossag":["darab"]},
    //"taxon_join_filter":0,
    //"uploading_join_filter":0,
    //"filter_type":["text: [\"darab\"]"],
    //"query_build_string":"\"szamossag\" IN ($MtcDqgFhNfzdVbmj$darab$MtcDqgFhNfzdVbmj$)"
    $cmd = sprintf('SELECT 1 FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote($_POST['set-current-query-table']));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $_SESSION['current_query_table'] = $_POST['set-current-query-table'];

        // just for reread modules ...
        $m = new modules(false);
        $_SESSION['modules'] = $m->set_modules();

        if (defined('SHINYURL') and constant("SHINYURL"))
            echo common_message('ok','http://'.URL.'/table/'.$_POST['set-current-query-table'].'/?map');
        else
            echo common_message('ok','http://'.URL.'/?qtable='.$_POST['set-current-query-table'].'&map');
    } else {
        echo common_message('error','Unknown table');
    }
    exit;
}
/* Called: main.js
 * Project admin: upload forms, deactivate a form
 * */
if (isset($_POST['deacform']) and $_POST['deacform'] and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('upload_forms')) {
        if($_POST['active']==1) $x = 2;
        elseif($_POST['active']==2) $x = 1;
        else {
            exit;
        }
        $cmd = sprintf("UPDATE project_forms SET \"active\"=$x WHERE form_id=%d",$_POST['deacform']);
        $res = pg_query($BID,$cmd);
    }
    exit;
}
/* Called: main.js
 * Project admin: upload forms, delete a form
 * */
if (isset($_POST['delform']) and $_POST['delform'] and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('upload_forms')) {
        $cmd = sprintf("DELETE FROM project_forms WHERE form_id=%d AND project_table='%s'",$_POST['delform'],PROJECTTABLE);
        $res = pg_query($BID,$cmd);
    }
    exit;
}
// check POST Content-Length x bytes exceed
if (isset($_POST['check_file_size'])) {
    //debug((substr(ini_get('post_max_size'), 0, -1) ));
    if ($_POST['check_file_size'] > (substr(ini_get('post_max_size'),0, -1) * 1024 * 1024))
        echo common_message('error','Post max size is: '.ini_get('post_max_size'));
    else
        echo common_message('ok','ok');
    exit;
}
/* Called: maps.js,..
 * XMLHttpRequest() automatikus fájl feltöltés
 * Description: shp feltöltés térbeli lekérdezéshez a filterbox_load_layer() függvényből hívva
 *              main map page shape file upload
 *              ....
 * */

if (isset($_POST['load-files']) and vreq()) {

    $dest = ''; //default temporay destination - clean automatically

    if ($_POST['ext']=='shp') {
        //shp feltöltés
        $dest = sys_get_temp_dir()."/ob_shp_upl_".session_id();
    }
    elseif ($_POST['ext']=='list') {
        //form list for list type
        $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
        if ($_FILES['files']['size'][0]>4096) {
            $rr['err'][] = 'File size exceeds the allowed size!';
            echo json_encode($rr);
            exit;
        }
        // nincs befejezve!
        $_SESSION['upl_form_listfile'] = file_get_contents($_FILES['files']['tmp_name'][0]);
        exit;
    }
    elseif ($_POST['ext']=='pict') {
        //képfeltöltés
        $dest = getenv('PROJECT_DIR').'attached_files';
    }
    elseif ($_POST['ext']=='language-files') {
        $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
        $rr = language_file_process();
        echo json_encode($rr);
        exit;
    }
    elseif ($_POST['ext']=='module-files') {
        //non php files can getting through!!!!!!!
        $e = '';
        if(!php_check_syntax($_FILES['files']['tmp_name'][0],$e)) {
            log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
            $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
            $rr['err'][] = $e;
            echo json_encode($rr);
            exit;
        }
        $dest = getenv('PROJECT_DIR').'modules/private';
    }
    //overwrite uploaded file name to the expected module name
    if (isset($_POST['exu']) and $_POST['exu']!='')
        $_FILES['files']['name'][0] = $_POST['exu'];

    if (isset($_FILES['files'])) {
        $r = move_upl_files($_FILES['files'],$dest);
        $rr = json_decode($r);
    } else {
        echo common_message('error','No files sent');
        exit;

    }

    if ($_POST['ext']=='pict') {
        $rr->{'file_names'} = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});
        if ($rr->{'file_names'}===false) {
            echo common_message('error','Upload process failed');
            exit;
        }
    }
    // return uploaded file names or a JSON ARRAY ['tmp'=>'','file_names'=>array(),'err'=>array()];
    //echo implode(',',$rr->{'file_names'});
    //echo json_encode($rr);
    echo common_message('ok',$rr);
    exit;
}
/* turn off local language definition for change it */
/*if (isset($_POST['turn_off_lang']) and vreq()) {
    $_SESSION['skip_local_lang'] = array($_SESSION['LANG']=>1);
    exit;
}*/
/*export language defintion */
/*if (isset($_GET['lang_export']) and vreq()) {
    $file = sprintf("%slanguages/local_%2s.php",getenv('PROJECT_DIR'),$_SESSION['LANG']);
    $content = file($file);
    $r = array("definition,value");
    foreach($content as $row) {
        $m = array();
        preg_match("/define\(['\"](\w+)['\"],['\"]?(.*)['\"]?\);/",$row,$m);
        if (isset($m[1]) and isset($m[2]))
            $r[] = "'$m[1]','$m[2]'";
    }
    $csv = implode("\n",$r);
    $filesize = strlen($csv);
    $filename = sprintf("%s.csv",$_SESSION['LANG']);
    # header definíció!!!
    header("Content-Type: text/csv");
    header("Content-Length: $filesize");
    header("Content-Disposition: attachment; filename=\"$filename\";");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    printf("%s",$csv);
    exit;
}*/
/*export module file */
if (isset($_GET['module_export']) and vreq()) {
    if ( file_exists(sprintf("%smodules/private/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']))) )
        $file = sprintf("%smodules/private/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']));
    elseif ( file_exists(sprintf("%smodules/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']))) ) {
        $file = sprintf("%smodules/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']));
    }
    else {
        printf('no such file');
        exit;
    }
    $fp = file_get_contents($file);
    $filesize = strlen($fp);
    $filename = sprintf("%s",basename($_GET['module_export']));
    # header definíció!!
    header("Content-Type: text");
    header("Content-Length: $filesize");
    header("Content-Disposition: attachment; filename=\"$filename\";");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    printf("%s",$fp);
    exit;
}
/* Custom shp Layer feltöltés
   az eredeti a query_builder-ben volt, mert egy query stringet csinált a végén */
if (isset($_POST['shaperead']) and vreq()) {
    session_write_close();

    $dir = sys_get_temp_dir()."/ob_shp_upl_".session_id();

    $s = glob("$dir/*.shp");
    $shp = array_pop($s);
    $data = array();
    if ($shp=='') {
        $s = glob("$dir/*.kml");
        // pszeudo shp
        $shp = $s[0];
        //$file_extension = (false === $pos = strrpos($_FILES["shpfile"]["name"][0], '.')) ? '' : substr($_FILES["shpfile"]["name"][0], $pos);
        //
        if (isset($s[0])) {
            $xml = file_get_contents($s[0]);
            $data = readKML($xml,'csv');
        } else {
            echo common_message('error','No files sent?');
            exit;
        }
    } else {
        if (!command_exist('ogrinfo')) {
            $errorID = uniqid();
            echo common_message('failed', 'ogrinfo error',$errorID);
            log_action("ErrorID#$errorID# ".'ogrinfo not found',__FILE__,__LINE__);
            exit;
        }
        exec("ogrinfo $shp 2>&1 >/dev/null", $r_stderr);
        if (!count($r_stderr)) {
            //general function to read data into an array
            $data = readSHP($shp,'csv','',true,'');
        } else {
            $errorID = uniqid();
            echo common_message('failed', str_shape_error.": ".implode(' ',$r_stderr),$errorID);
            log_action("ErrorID#$errorID# ".'Read shape error: '.implode(' ',$r_stderr),__FILE__,__LINE__);
            exit;
        }
    }

    $h = 0;
    //$header = array();
    $wkt = array();
    $wid = array();
    $cnt=1;
    $csv_rows_count = count($data);
    $message = array();
    $shp_header = array_shift($data);
    $name_idx = false;
    if (isset($_POST['shp_label'])) {
        $name_idx = array_search($_POST['shp_label'],explode(',',$shp_header));
    }

    foreach ($data as $l) {
        /* Nincs header!! a readSHP-ben el lett dobva!!
         * if ($h==0) {
              $h++;
              $header = str_getcsv($l,',','"');
              continue;
           }
         */
        $a = str_getcsv($l,',','"');
        $geom_string = preg_replace('/^(\w+) \(/','$1(',$a[0]);
        if(trim($geom_string)=='') continue;
        $h = count($a);
        if($cnt) {
                // csak maximum 1 geometriát adunk vissza megjelenítésre...
            $wkt[] = $a[0];
            $cnt--;
        }
        mb_internal_encoding("UTF-8");
        // the name of the shared polygon...
        if ($name_idx!==false)
            $name_column = $a[$name_idx];
        else
            $name_column = $a[1];

        $path_parts = pathinfo($shp);

        $polygon_name = mb_substr($name_column .' - '. $path_parts['filename'],0,128);
        //$c = 0;
        /*foreach($a as $an) {
            if ($c==0) {
                $c++;
                continue;
            }
            $name .= ",".utf8_decode(urldecode($an));
            $c++;
        }*/

        // if logined user SAVE to named selections
        if (isset($_SESSION['Tid'])) {
            pg_query($GID,'BEGIN');
            $cmd = sprintf("INSERT INTO system.shared_polygons (\"geometry\",\"project_table\",\"name\",\"user_id\",\"original_name\") VALUES (ST_GeomFromText(%s,4326),'".PROJECTTABLE."',%s,%d,%s) RETURNING id",quote($geom_string),quote($polygon_name),$_SESSION['Tid'],quote($shp));
            $res = pg_query($GID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $cmd = sprintf("INSERT INTO system.polygon_users (user_id,polygon_id,select_view) VALUES (%d,%d,%d)",$_SESSION['Tid'],$row['id'],'select-upload');
                $res = pg_query($GID,$cmd);
                if (pg_affected_rows($res)) {
                    pg_query($GID,'COMMIT');
                    // visszaküldjük a WKT stringet, habár ez már nincs használva!!!!
                    //echo common_message('ok',json_encode($wkt));
                    $message[] = array('ok',json_encode($wkt));
                } else {
                    pg_query($GID,'ROLLBACK');
                    echo common_message('error',"Geometry saving error");
                    exec("rm -rf $dir");
                    exit;
                }
            } else {
                pg_query($GID,'ROLLBACK');
                echo common_message('error',"Geometry saving error");
                exec("rm -rf $dir");
                exit;
            }
        } else {
            echo common_message('error',"Should be logined!");
            exec("rm -rf $dir");
            exit;
        }
    }
    echo common_multi_message($message);
    // kitörli a /tmp/ob_shp_upl_... könyvtárat
    exec("rm -rf $dir");
    exit;
}
/* not used??
 *
 * */
if (isset($_POST['lbuf_reread'])) {
    require(getenv('OB_LIB_DIR').'interface.php');
    echo filterbox_load_selection();
    exit;
}

/* Load selections
 * Ajax request - called: maps.js
 * */
if (isset($_POST['getWktGeometry'])) {
    $id = preg_replace('/[^0-9]/','',$_POST['getWktGeometry']);

    if (isset($_POST['custom_table']) and isset($_SESSION['private_key'])) {

        $_SESSION['selection_geometry_type'] = 'customPolygon';

        $cipher="AES-128-CBC";
        $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
        $table_reference = openssl_decrypt(base64_decode($_POST['custom_table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        $stcol = st_col($table_reference,'array');

        $cmd = "SELECT {$stcol['ID_C']} AS id,ST_AsText(ST_Transform({$stcol['GEOM_C']},4326)) as q FROM $table_reference WHERE {$stcol['ID_C']}='$id'";

    } else {
        //$cmd = sprintf('SELECT id,ST_AsText(geometry) as q FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id WHERE select_view IN (1,3) AND p.user_id=\'%d\' AND id=\'%d\'',$tid,$id);

        $_SESSION['selection_geometry_type'] = 'sharedPolygon';
        if (isset($_SESSION['Tid']))
            $cmd = sprintf("SELECT id,ST_AsText(geometry) as q
                FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id
                WHERE select_view IN ('1','3','only-select','select-upload') AND p.user_id=%d AND id='%d'",$_SESSION['Tid'],$id);
        else
            $cmd = sprintf("SELECT id,ST_AsText(geometry) as q
                FROM system.shared_polygons s
                WHERE (access IN ('4','public') OR (project_table='%s' AND access IN ('3','project'))) AND id='%d'",PROJECTTABLE,$id);
    }

    $res = pg_query($ID,$cmd);
    if ($row = pg_fetch_assoc($res)){
            //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //$random = substr( str_shuffle( $chars ), 0, 16 );
            //$_SESSION['query_build_string'][$random] = $row['id'];
            //publikus hivatkozás string a lekérdezéshez : WKT string a kirajzoláshoz
            //ez lesz majd a _GET['qstr'] változó, amit a mapservernek a proxy rak össze.

            //print out geometry for drawing it on the map
            echo json_encode(array($row['q']));
            $_SESSION['selection_geometry_id'] = $row['id'];
    } else {
            //echo "t:"."no records available in the current access level";
            echo 0;
    }
    exit;
}
/**
* similar to the singular form, just it queries the geometries with one query
* it accepts the ids as an array
**/
if (isset($_POST['getWktGeometries']) && isset($_POST['custom_table']) and isset($_SESSION['private_key'])) {
    $ids = array_values(array_filter($_POST['getWktGeometries'], function($el) {
        return preg_match('/\d+/',$el);
    }));

    $_SESSION['selection_geometry_type'] = 'customPolygon';

    $cipher="AES-128-CBC";
    $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
    $table_reference = openssl_decrypt(base64_decode($_POST['custom_table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
    $stcol = st_col($table_reference,'array');

    $cmd = "SELECT {$stcol['ID_C']} AS id,ST_AsText(ST_Transform({$stcol['GEOM_C']},4326)) as q FROM $table_reference WHERE {$stcol['ID_C']} IN (".implode(',',$ids).")";

    $res = pg_query($ID,$cmd);
    while ($row = pg_fetch_assoc($res)){
            //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //$random = substr( str_shuffle( $chars ), 0, 16 );
            //$_SESSION['query_build_string'][$random] = $row['id'];
            //publikus hivatkozás string a lekérdezéshez : WKT string a kirajzoláshoz
            //ez lesz majd a _GET['qstr'] változó, amit a mapservernek a proxy rak össze.
            $wkts[] = [$row['q']];
            //print out geometry for drawing it on the map
            $_SESSION['selection_geometry_id'] = $row['id'];
    }
    echo json_encode($wkts);
    exit;
}
/* Project admin
 * Called: main.js
 * Return: string output of image conversion
 * Testing mapserver map file
 * Not used anymore!!
 * */
if (isset($_POST['maptest']) and isset($_SESSION['Tid'])) {
    if (has_access(PROJECTTABLE,'mapserv')) {
        if ($_POST['maptest'] == 'public') $p = 'public';
        elseif ($_POST['maptest'] == 'private') $p = 'private';
        else exit;

        if (!isset($_SESSION['shp2img_process'])) {
            $cmd = "shp2img -m ".getenv('PROJECT_DIR')."$p/$p.map -o ".OB_TMP."test_".PROJECTTABLE."_".session_id().".png -all_debug 5 >".OB_TMP."test_".PROJECTTABLE."_".session_id().".log 2>&1";
            $ps = run_in_background($cmd,0,0);
            if ($ps) {
                $_SESSION['shp2img_process'] = $ps;
                echo '.';
            }
            else {
                echo 'no'; // process not started
                unlink(OB_TMP."test_".PROJECTTABLE."_".session_id().".log");
            }
        } else {
            if(is_process_running($_SESSION['shp2img_process'])) {
                echo '.';
            } else {
                if (file_exists(OB_TMP."test_".PROJECTTABLE."_".session_id().".log")) {
                    echo file_get_contents(OB_TMP."test_".PROJECTTABLE."_".session_id().".log");
                    unlink(OB_TMP."test_".PROJECTTABLE."_".session_id().".log");
                } else echo 'no'; // process previously strted but stopped without results
                unset($_SESSION['shp2img_process']);
            }
        }
    }
    exit;
}
/* Check given process exists
 * Return 1 or 0
 * not used anywhere
 * */
if (isset($POST['check_proc']) and isset($_SESSION['Tid'])) {
    session_write_close();
    //while(is_process_running($ps)) {
    if(is_process_running($ps)) {
        echo '.';
        ob_flush(); flush();
        //sleep(2);
    } else {
        echo 'no';
    }
    exit;
}
/* Project admin
 * Called: main.js
 * Return:
 * Description: Create project's SQL functions
 * */

if (isset($_POST['create-function']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('functions')) {
        $cmd = Create_Project_SQL_functions($_POST['table'],$_POST['create-function'],gisdb_user);
        if (pg_query($ID,$cmd)) echo "Create {$_POST['create-function']} function: done.";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            log_action("$ID: $cmd",__FILE__,__LINE__);
            return;
        }
    }
    exit;
}

if (isset($_POST['create-table']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('create_table')) {
        $cmd = Create_Project_plus_tables($_POST['table'],$_POST['create-table'],gisdb_user);
        if (pg_query($ID,$cmd)) echo "Create {$_POST['create-table']} function: done.";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            return;
        }
    }
    exit;
}

if (isset($_POST['enable-trigger']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('functions')) {
        $cmd = Create_Project_SQL_functions($_POST['table'],$_POST['enable-trigger'],gisdb_user);
        if (pg_query($ID,$cmd)) echo "Setting {$_POST['enable-trigger']} trigger: done.";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            return;
        }
    }
    exit;
}

if (isset($_POST['group-add-to-rule'])) {

    $rule_text = $_POST['rule-function'];
    $rule = preg_split('/ARRAY\s*\(SELECT uploader_id/m',$_POST['rule-function']);
    $groups = "|| ".implode('||',$_POST['group-add-to-rule']);

    for ($i = 0; $i<count($rule) ;$i++) {
        if ($i == 0) {
            $rule_implode = $rule[0];
            continue;
        }

        $rule[$i] = preg_replace('/\)/',") $groups",$rule[$i]);
        $rule_implode .= ' ARRAY (SELECT uploader_id '.$rule[$i];
    }
    if (isset($rule_implode)) $rule_text = $rule_implode;
    echo $rule_text;
    exit;
}

if (isset($_POST['rule-save'])) {
    $cmd = sprintf('CREATE OR REPLACE FUNCTION public.rules_%1$s()
        RETURNS TRIGGER AS
        \'
        '.preg_replace("/'/","''",$_POST['rule-function']).'
        \' LANGUAGE plpgsql;',$_POST['table'],PROJECTTABLE);

    $res = pg_query($ID,$cmd);
    if (!pg_last_error($ID))
        echo common_message('ok','ok');
    else {
        $errorID = uniqid();
        echo common_message('fail','SQL Error',$errorID);
        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
    }
    exit;
}

if (isset($_POST['broadcats-message']) and isset($_SESSION['Tid'])) {

    $news_message = preg_replace("/\n/","<br>",$_POST['broadcats-message']);

    insertNews($news_message,'project',$_SESSION['Tid']);

    if (has_access("master") and isset($_POST['email']) and $_POST['email']=='on') {

        $title_grab = 0;
        $mail_message = array();
        foreach(preg_split("/((\r?\n)|(\r\n?))/",$_POST['broadcats-message'] ) as $line) {
            if (trim($line) != '' and !$title_grab) {
                $title = trim($line);
                $title_grab = 1;
            } elseif ($title_grab) {
                $mail_message[] = $line;
            }
        }
        $message = implode('<br>',$mail_message);

        if (defined("OB_PROJECT_DOMAIN")) {
            $domain = constant("OB_PROJECT_DOMAIN");
            $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
            $reply_email = "noreply@".parse_url('http://'.$domain,PHP_URL_HOST);
        } else {
            $domain = $_SERVER["SERVER_NAME"];
            $server_email = PROJECTTABLE."@".$domain;
            $reply_email = "noreply@".$domain;
        }

        #user's info
        $cmd = sprintf('SELECT "username",email FROM "users" LEFT JOIN project_users ON (user_id=id) WHERE project_table=%s and user_status NOT IN (\'0\',\'banned\')',quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $n = 0;
            while($row = pg_fetch_assoc($res)) {
                $user_email = $row['email'];
                $user_name = $row['username'];
                // master user can broadcast message to all users!!!
                //
                $m = mail_to("$user_email","[".PROJECTTABLE."] $title",$server_email,$_SESSION['Tmail'],$title,$message."<br><br>".$_SESSION['Tname'],'multipart');
                $n++;
                if ($n==10) {
                    sleep(3);
                    $n = 0;
                }
            }
        }
    }
}

/* Profile
 * Called: main.js
 * Return: number of invites left
 * Description: Send invitation letter
 * */
if (isset($_POST['invitesend']) and isset($_SESSION['Tid'])) {
    require(getenv('OB_LIB_DIR').'languages.php');
    if (defined('INVITATIONS')) $inv = constant('INVITATIONS');
    else $inv = 10;
    session_write_close();

    //zero invitation project allow admins to send invitations
    if ($inv==0 and has_access('master'))
        $inv = 20;

   $cmd = "SELECT count(id) as c FROM invites WHERE user_id='{$_SESSION['Tid']}' AND created + interval '7 days' > now()";
   $res = pg_query($BID,$cmd);
   $inv_sent = 0;
   if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);
        $inv_sent = $r['c'];
   }
   if ($inv_sent>=$inv) return;

   $name = '';
   $mail = '';

   $val = strip_tags($_POST['name']);
   if(strlen($val)>64) return;
   $ret_name = $val;
   $name = quote($val);

   if (preg_match('/<([A-Za-z0-9-_.@]+)>/',$_POST['mail'],$m)) {
       $_POST['mail'] = $m[1];
   }
   $post_mail=preg_replace("/[^A-Za-z0-9-_.@]/","",$_POST['mail']);
   if(strlen($post_mail)>64) return;
   $ret_mail = strtolower($post_mail);
   $mail = quote(strtolower($post_mail));

   $comment = strip_tags($_POST['comment']);
   if ($comment != "")
       $comment .= "<br>.............................<br>";

   $code = md5($name.$mail);

    $cmd = "SELECT short,long,language FROM project_descriptions WHERE projecttable='".PROJECTTABLE."'";
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_all($res);

    $k = array_search($_SESSION['LANG'], array_column($row, 'language'));
    if(!$k) $k = 0;

    $sdesc = $row[$k]['short'];
    $ldesc = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/","<br><br>",$row[$k]['long']);

    $Title = t(str_dear)." $ret_name!<br><br>";

    if (defined("OB_PROJECT_DOMAIN")) {
        $domain = constant("OB_PROJECT_DOMAIN");
        $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
        //$reply_email = "noreply@".parse_url('http://'.$domain,PHP_URL_HOST);
    } elseif (defined("OB_DOMAIN")) {
        $domain = constant("OB_DOMAIN");
        $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
        //$reply_email = "noreply@".parse_url('http://'.$domain,PHP_URL_HOST);
    } else {
        $domain = $_SERVER["SERVER_NAME"];
        $server_email = PROJECTTABLE."@".$domain;
        //$reply_email = "noreply@".$domain;
    }

    //$comment = nl2br($comment);
    $comment = str_replace( "\n", '<br />', $comment );

    $cmd = "INSERT INTO \"public\".\"invites\" (user_id,code,created,name,mail,project_table) VALUES ('{$_SESSION['Tid']}','$code',now(),$name,$mail,'".PROJECTTABLE."')";
    $nur = pg_query($BID,$cmd);
    if ($nur) {

        //mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method)
        $err = mail_to("$ret_mail","$sdesc ".str_invitation,"$server_email","{$_SESSION['Tmail']}",$Title,"$comment<br><br> {$_SESSION['Tname']} ".str_invtext1.".<br>
            ".str_invtext2.":<br><br>
            <div style='margin:20px;max-width:50em'><i>\"$sdesc\"</i><div style='font-size:90%'>$ldesc</div></div>
            <b>".str_invtext3.":</b><br>
            <a href='http://".URL."/index.php?registration=$code'>http://".URL."/index.php?registration=$code</a><br><br>/".t(str_invitation_canbe_used_only_once)."/<br><br>".t(str_best_wishes).",<br>{$_SESSION['Tname']} ($domain)",'multipart',$_SESSION['Tname'],$_SESSION['Tname']);
        //echo $err;
        $cmd = "SELECT count(id) as c FROM invites WHERE user_id='{$_SESSION['Tid']}' AND created + interval '7 days' > now()";
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            return printf("%d",5-$r['c']);
        }
    } else {
        log_action("Sending invitation to $mail failed.",__FILE__,__LINE__);
    }
    exit;
}
/* Profile
 * Ajax request - called: main.js
 * Return:
 * Description: Drop selected invites
 * */
if (isset($_POST['delinv']) and isset($_SESSION['Tid'])) {
    session_write_close();
    foreach ($_POST['delinv'] as $d) {
        $pd = preg_replace('[^0-9]','',$d);
        if ($pd == '') continue;
        $cmd = "DELETE FROM invites WHERE id='$pd' AND user_id='{$_SESSION['Tid']}'";
        pg_query($BID,$cmd);
    }
    exit;
}
/* Save Table data for further actions
 * Ajax request - called: uploader.js
 * Only for logined users
 */
if (isset($_POST['state_posth']) and isset($_POST['state_postd']) and vreq()) {
    //
    // Ezt át kell alakítani úgy hogy minden fájl típusra működjön!!!!
    //
    //

    // ajax cross call prevention
    if (isset($_SESSION['upload_ajax_wait']) and $_SESSION['upload_ajax_wait']==1) {
        //waiting for other process
        for ($i=0;$i<15;$i++) {
            if ($_SESSION['upload_ajax_wait']==1) sleep(1);
        }
        // stop save
        if ($_SESSION['upload_ajax_wait']==1) {
            echo common_message('error',"Not saved because an other process is running.");
            exit;
        }
    }

    if (!isset($_SESSION['Tid'])) {
        echo common_message('error',"Not saved. It is available only for logined users.");
        exit;
    }
    if (!isset($_POST['form_id'])) {
        echo common_message('error',"Not saved.");
        exit;
    }

    if (!isset($_SESSION['theader'])) {

        echo common_message('error',"Not saved.");
        exit;
    }

    $_SESSION['upload_ajax_wait']=1;

    // unused database columns
    $header = quote($_POST['state_posth']);

    // default fields
    $default = $_POST['state_postm'];

    // current data sheet
    $j = json_decode($_POST['state_postd']);
    $count = count($j->{'data'});

    if ($_POST['form_type']=='file') {

        if (!isset($_SESSION['sheetDataPage_counter']))
            $startpage = 0;
        else
            $startpage = $_SESSION['sheetDataPage_counter'];

        $n = 0;

        for ( $i=$startpage; $i<($startpage+$count);$i++ ) {
            //$row = $_SESSION['sheetData'][$i];
            $row = get_sheet_row($i+1);
            // {"WKT":31.979730998243,"NAME":"625","HELYSEG":"Alsoujlak","ALLOMANY":"1","TERMOHELY":"kaszaloret","DATUM":"2015.04.15.","MEGJEGYZES":"elohelyfolt hatara","FENOLOGIA":"","FAJNEV":"Fritillaria meleagris","":""}
            $row_data = json_decode($row['data'],true);
            if ($row_data) {
                $k = 1;
                foreach ($row_data as $key=>$val) {
                    if (isset($j->{'data'}[$n])) {
                        if (isset($j->{'data'}[$n][$k])) {
                            $newdata = $j->{'data'}[$n][$k];
                            //$_SESSION['sheetData'][$i][$key] = $newdata;
                            $row_data[$key] = $newdata;
                        }
                    }
                    $k++;
                }
                #if($_SESSION['Tid'] == 1 and $_POST['form_id']==58) {
                #       $order = array("lat","lon","wkt","ele","time","name","cmt","sym","type","extensions","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB");
                #       $ordered_array = array_replace(array_flip($order), $row_data);
                #       $row_data = $ordered_array;
                #}

                update_sheet_row($i+1,$row_data,$j->{'data'}[$n][0]);
            }
            $n++;
        }
        $data = quote(json_encode(array('title'=>$j->{'title'},'header'=>$j->{'header'},'default'=>$default)));
    } else {
        // web, api
        create_upload_temp();
        $data = array();
        for ($i=0;$i<count($j->{'data'});$i++) {
            $one = 0;
            for($ic=0;$ic<count($j->{'data'}[$i]);$ic++) {
                $icn = $ic+1;
                if (isset($j->{'data'}[$i][$icn]))
                    $data[$i][$_SESSION['theader'][$ic]] = $j->{'data'}[$i][$icn];
                if ($one==0) {
                    $one++;
                }
            }
            add_sheet_row($i+1,$data[$i],$j->{'data'}[$i][0]);
        }

        $data = quote(json_encode(array('title'=>$j->{'title'},'header'=>$j->{'header'},'default'=>$default)));
    }

    $form_type=quote(preg_replace('/[^a-z]/','',$_POST['form_type']));
    $form_id=quote(preg_replace('/[^0-9]/','',$_POST['form_id']));

    $cmd = '';
    if (isset($_SESSION['upload']['loadref'])) {
        $ref = $_SESSION['upload']['loadref'];
        $cmd = sprintf("UPDATE system.imports SET datum=NOW(),header=%s,data=%s WHERE project_table='%s' AND ref=%s",$header,$data,PROJECTTABLE,quote($ref));
    } else if (isset($_SESSION['upload']['saveref'])) {
        if (isset($_GET['massiveedit']) and isset($_SESSION['upload']['massiveedit_ids']))
            $massedit_ids = json_encode($_SESSION['upload']['massiveedit_ids']);
        else
            $massedit_ids = NULL;

        $ref = $_SESSION['upload']['saveref'];
        //$ref = crc32(session_id().time());
        //$cmd = sprintf("SELECT ref FROM system.imports WHERE file='%s' AND form_id=%s AND form_type=%s AND user_id='%d'","upload_".PROJECTTABLE."_".session_id(),$form_id,$form_type,$_SESSION['Tid']);
        $cmd = sprintf("SELECT ref FROM system.imports WHERE ref='%s'",$ref);
        $res = pg_query($GID,$cmd);
        if (!pg_num_rows($res))
            $cmd = sprintf("INSERT INTO system.imports (project_table,user_id,ref,datum,header,data,form_type,form_id,file,massive_edit) VALUES ('%s','%d','%s',NOW(),%s,%s,%s,%s,'%s',%s)",PROJECTTABLE,$_SESSION['Tid'],$ref,$header,$data,$form_type,$form_id,"upload_".PROJECTTABLE."_".session_id(),quote($massedit_ids));
        else {
            $cmd = sprintf("UPDATE system.imports SET datum=NOW(),header=%s,data=%s WHERE project_table='%s' AND ref='%s'",$header,$data,PROJECTTABLE,$ref);
        }
    } else {
        if (isset($_GET['editrecord']))
            echo common_message('ok','');
        else {
            // expired session??
            echo common_message('error','Session expired?');
            log_action('Upload autosave session expired or saveref is missing...?',__FILE__,__LINE__);
        }
    }
    if ($cmd!='') {
        $result = pg_query($GID,$cmd);
        $status = pg_result_status($result);
        if ($status!=1) {
            $errorID = uniqid();
            echo common_message('fail',str_sql_error,$errorID);
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            $_SESSION['upload_ajax_wait']=0;
            exit;
        }
    }

    if (isset($ref) and $ref != '') {
        $cmd = sprintf('DROP TABLE IF EXISTS temporary_tables.upload_%1$s_%2$s_%3$s',PROJECTTABLE,session_id(),preg_replace('/[;:".\'$^()\[\]]/','',$ref));
        $result = pg_query($ID,$cmd);
        $cmd = sprintf('CREATE TABLE temporary_tables.upload_%1$s_%2$s_%3$s AS SELECT * FROM temporary_tables.upload_%1$s_%2$s',PROJECTTABLE,session_id(),preg_replace('/[;:".\'$^()\[\]]/','',$ref));
        $result = pg_query($ID,$cmd);
        //$cmd = "UPDATE system.imports SET header=$header,data=$data WHERE ref='{$_SESSION['imports']}'";
        //$n = PGsqlcmd($ID,$cmd);

        //echo "Save the following link to load and continue this uploading:<br><a href='$p?load=$ref'>$p?load=$ref</a>";
        echo common_message('ok','ok');
    }
    $_SESSION['upload_ajax_wait']=0;
    exit;
}
/* upload.php
 * sheets paging */
if (isset($_POST['upload-file-pager']) and vreq()){
    $_SESSION['upload_ajax_wait'] = 1;
    $pager = pager($_SESSION['perpage'],$_SESSION['sheetDataPage'],$_SESSION['sheetDataPage_counter']);
    echo $pager;
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* Change default 20 displayed rows on upload page
 *
 * */
if (isset($_POST['changepp']) and vreq()) {
    $_SESSION['perpage'] = preg_replace('/[^0-9]/','',$_POST['changepp']);
    exit;
}
/*
 *
 * */
if (isset($_POST['upload-file-skip']) and vreq()){
    $_SESSION['upload_ajax_wait'] = 1;
    if (!isset($_SESSION['sheetDataPage_counter']))
        $pagestart = 0;
    else
        $pagestart = $_SESSION['sheetDataPage_counter'];

    $rows = get_sheet_skips($pagestart,$_SESSION['perpage']);
    $checked = array();
    foreach ( $rows as $r ) {
        if ($r['skip_marked']=='t') {
            $checked[] = 1;
        } else {
            $checked[] = 0;
        }
    }
    echo json_encode($checked);
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* upload.php
 * reverse skip */
if (isset($_POST['reverse_table_skip']) and vreq()) {
    $_SESSION['upload_ajax_wait'] = 1;
    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked = NOT skip_marked",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* upload.php
 * deselect skip */
if (isset($_POST['deselect_table_skip']) and vreq()) {
    $_SESSION['upload_ajax_wait'] = 1;
    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked = FALSE",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* Filter distinct values of an upload table column */
if (isset($_POST['filterplus']) and vreq()) {
    $_SESSION['upload_ajax_wait'] = 1;

    $idx  = sprintf('%d',preg_replace('/[^0-9]/','',$_POST['filterplus'])-2);

    $key = $_SESSION['theader'][$idx];

    # SELECT data -> 'szamossag' FROM upload_dinpi_kh2de8h7a7g6sv2iomvhuco5ua
    # ...
    $jdata = get_sheet_page(0,-1);
    $uval = array();
    foreach($jdata as $row) {
        foreach ( $row as $colname=>$value) {
            if ($colname == $key)
                $uval[] = $value;
        }
    }
    $a = array_values(array_unique($uval));
    sort($a);
    echo json_encode($a);

    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* Reverse Skip Mark filtered elements of upload table column */
if (isset($_POST['filterplus_skipmark']) and vreq()) {
    $_SESSION['upload_ajax_wait'] = 1;

    $idx = sprintf('%d',preg_replace('/[^0-9]/','',$_POST['filterplus_idx'])-2);
    $key = $_SESSION['theader'][$idx];

    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked = TRUE",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    $filter = array();
    foreach($_POST['filterplus_skipmark'] as $val) {
        $filter[] = sprintf('data->>\'%1$s\' = %2$s',$key,quote($val));
    }
    $cmd = sprintf('UPDATE temporary_tables.upload_%1$s_%2$s SET skip_marked = FALSE WHERE %3$s',PROJECTTABLE,session_id(),implode(' OR ',$filter));
    $res = pg_query($ID,$cmd);

    $rows = get_sheet_skips(0,-1);
    echo json_encode($rows);

    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}

/* apply obfun on visible cells
 * called in javascript loop
 * only for web forms
 * */
if (isset($_POST['upload_fill_onecell']) and vreq() ) {
    $_SESSION['upload_ajax_wait'] = 1;

    // update cell:                 function
    $data = array();
    foreach ($_POST['rowindex'] as $rowindex) {
        $data[] = upload_table_obfun($_POST['upload_fill_function'],$rowindex,$_POST['colindex'],$_POST['idmatch'],$_POST['autofill_data']);
    }
    echo json_encode($data);
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* upload.php
 * autofill
 * apply obfun on non-visible cells: sheetData */
if(isset($_POST['upload_fill_column-id']) and vreq()) {
    $_SESSION['upload_ajax_wait'] = 1;
    // get column id
    $id  = sprintf('%d',preg_replace('/[^0-9]/','',$_POST['upload_fill_column-id'])-2);
    $key = $_SESSION['theader'][$id];
    //$n = array_keys($_SESSION['sheetData']);
    $sheetData_count = get_upload_count();
    for ($i = 0;$i<$sheetData_count;$i++) {
        $pkey = $key;

        if (get_row_skip($i+1) == 't') continue;
        $row = get_sheet_row($i+1);
        $sheetData = json_decode($row['data'],true);

        // tricky function apply: obfun:[1]replace:120->asd
        // read cell content from the 1st column WHERE its value is 120 apply it in the current column!!!
        // obfun:[FAJ]replace:Parus major->2
        if (preg_match('/^`apply:\[(.+)?\].+$/i',$_POST['upload_fill_function'],$m)) {
            if (count($m)==2) {
                $pkey = $m[1];
                if ($pkey == -1)
                    $pkey = $key;
            } else
                $pkey = $key;
            $_POST['upload_fill_column-value'] = $sheetData[$key];
        } elseif (preg_match('/^`apply:.+$/i',$_POST['upload_fill_function'],$m)) {
            $_POST['upload_fill_column-value'] = $sheetData[$key];
        }

        // update cell:                 function
        $data = upload_table_obfun($_POST['upload_fill_function'],$i,$_POST['upload_fill_column-id'],$_POST['idmatch'],$_POST['autofill_data']);
    }
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
/* upload.php
 * update current sheet
 * */
if (isset($_POST['upload_table_page']) and vreq()) {

    if (!isset($_SESSION['theader'])) {
        echo common_message('error',"Session expired");
        exit;
    }

    if (isset($_SESSION['upload_ajax_wait']) and $_SESSION['upload_ajax_wait']==1) {
        //waiting for other process
        for ($i=0;$i<10;$i++) {
            if ($_SESSION['upload_ajax_wait']==1)
                sleep(1);
        }
        // stop save
        if ($_SESSION['upload_ajax_wait']==1) {
            echo common_message('error',"No paging, because an other process is running $t.");
            exit;
        }
    }

    $_SESSION['upload_ajax_wait'] = 1;

    $j = json_decode($_POST['upload_table_page']);
    //nem a cél lap kell, hanem az aktuális!!!
    if (!isset($_SESSION['sheetDataPage_counter']))
        $startpage = 0;
    else
        $startpage = $_SESSION['sheetDataPage_counter'];

    $selected_form_id = preg_replace('/[^0-9]/','',$_POST['form_page_id']);
    $selected_form_type = preg_replace('[^a-z]','',$_POST['form_page_type']);
    $insert_allowed = $selected_form_id;

    // elvileg kötelező módon küldjük, hogy hányadik lapra érkezünk
    if (isset($_POST['page'])) {
        $page = preg_replace("/[^0-9]/",'',$_POST['page']);
        $_SESSION['sheetDataPage_counter'] = $page;
    }

    //web form esetén létrehozom a sheetData-t
    if ($selected_form_type=='web') {
        create_upload_temp();
        $data = array();
        $skip = array();
        for ($i=0;$i<count($j->{'data'});$i++) {
            if ($j->{'data'}[$i]=='') $j->{'data'}[$i] = array();
            $one = 0;
            // a data oszlopokra
            for($ic=0;$ic<count($j->{'data'}[$i]);$ic++) {
                $icn = $ic+1;
                if (isset($j->{'data'}[$i][$icn])) {
                    // multiselect elements
                    if (is_array($j->{'data'}[$i][$icn]))
                        $j->{'data'}[$i][$icn] = implode(',',$j->{'data'}[$i][$icn]);
                    $data[$i][$_SESSION['theader'][$ic]] = $j->{'data'}[$i][$icn];
                }
                if ($one==0) {
                    //$skip[] = array($j->{'data'}[$i][0]);
                    $one++;
                }
            }
            if (isset($data[$i])) {
                if (implode('',array_values($data[$i])) == '') {
                    // DEFAULT DATA!!!!!
                    $skip = 1;
                    if (isset($_POST['default_data']) && implode('',$_POST['default_data'])!='' and $i==0) {
                        $skip = 0;
                    }
                    if ($skip) {
                        $j->{'data'}[$i][0] = true;
                        echo common_message("ok","force_skip:$i;");
                        $_SESSION['upload_ajax_wait'] = 0;
                        exit;
                    }
                }
                add_sheet_row($i+1,$data[$i],$j->{'data'}[$i][0]);
            } else {
                $skip = 1;
                if (isset($_POST['default_data']) && implode('',$_POST['default_data'])!='' and $i==0) {
                    $skip = false;
                }

                add_sheet_row($i+1,array(),$skip);
                echo common_message("ok","force_skip:$i;");
                $_SESSION['upload_ajax_wait'] = 0;
                exit;
            }
        }
        echo common_message("ok","ok");
        $_SESSION['upload_ajax_wait'] = 0;
        exit;

    } elseif($selected_form_type=='file' ) {
        $n = 0;
        for ( $i=$startpage; $i<($startpage+$_SESSION['perpage']); $i++ ){
            //i. sor
            $row = get_sheet_row($i+1);
             /*{
                "row_id": "20",
                "skip_marked": "f",
                "data": "{\"lat\":\"47.728\",\"lon\":\"19.132\",\"wkt\":\"POINT(19.132 47.728)\",\"ele\":\"102.747\",\"time\":\"2018-07-16T08:51:00Z\",\"name\":\"1019\",\"sym\":\"City (Small)\",\"extensions\":\"\"}"
            }*/
            if (!$row) continue;

            $k = 1;
            // cellák beírása
            $row_data = json_decode($row['data'],true);
            foreach($row_data as $key=>$val) {
                if (isset($j->{'data'}[$n])) {
                    if ( isset($_SESSION['theader'][$k-1]) and $key == $_SESSION['theader'][$k-1]) {
                        if (!isset($_POST['read_visible_cells']) or $_POST['read_visible_cells']) {
                            if (isset($j->{'data'}[$n][$k]))
                                $row_data[$key] = $j->{'data'}[$n][$k];
                        }
                    }
                }
                $k++;
            }
            // extra oszlopok
            if (isset($j->{'data'}[$n])) {
                for ($m=$k-1;$m<count($j->{'data'}[$n])-1;$m++) {
                    //$_SESSION['sheetData'][$i][$_SESSION['theader'][$m]] =  $j->{'data'}[$n][$m+1];
                    if (isset($j->{'data'}[$n][$m+1])) {
                        $row_data[$_SESSION['theader'][$m]] = $j->{'data'}[$n][$m+1];
                    }
                }
                if (isset($j->{'data'}[$n][0]))
                    update_sheet_row($i+1,$row_data,$j->{'data'}[$n][0]);
            }
            $n++;
        }
        // extra sorok hozzáadása a fájl végéhez
        // elegáns lenne, ha bárhol lehetne hozzáadni!
        if (isset($_POST['plusrow']) ) {
            for ($i=0; $i < $_POST['plusrow']; $i++) {
                $a = array();
                foreach($_SESSION['theader'] as $t) {
                    $a[$t] = '';
                }
                if (isset($j->{'data'}[$n][0]))
                    add_sheet_row('',$a,$j->{'data'}[$n][0]);
            }
        }
        ob_start();
        $only_uft = 1;
        $show_geom_ref = '';
        $theader = $_SESSION['theader'];
        $table_type = "file";

        $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%d and active=1",$selected_form_id);
        $res = pg_query($BID,$cmd);
        if ($row=pg_fetch_assoc($res)) {
            if ( $row['destination_table'] == '')
                $selected_form_table = PROJECTTABLE;
            else
                $selected_form_table = $row['destination_table'];
        }

        include('upload_show-table-form.php');
        $table = ob_get_clean();
        if ($_POST['print'])
            echo $table;
        else
            echo common_message("ok","ok");

        $_SESSION['upload_ajax_wait'] = 0;
        exit;
    }

    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}
if(isset($_POST['cancel_massiveedit']) and vreq()) {
    if (isset($_SESSION['upload']['massiveedit_ids']))
        unset($_SESSION['upload']['massiveedit_ids']);
    //if (isset($_SESSION['sheetData']))
    //    unset($_SESSION['sheetData']);
    if (isset($_SESSION['theader']))
        unset($_SESSION['theader']);
}
/* upload / insert data rows into database
 * Ajax request - called: uploader.js
 * should be compatible with API request
 */
if(isset($_POST['upload_table_post']) and vreq()) {

    $selected_form_id = preg_replace('/[^0-9]/','',$_POST['form_page_id']);
    $selected_form_type = preg_replace('[^a-z]','',$_POST['form_page_type']);

    if (isset($_SESSION['openpage_key']))
        $this_page = md5($_SESSION['openpage_key'].$selected_form_id.$selected_form_type);

    if (!isset($this_page) or !in_array($this_page,$_SESSION['upload']['openpages'])) {
        http_response_code(400);
        echo common_message('error',"Invalid page sent!");
        exit;
    }

    include(getenv('OB_LIB_DIR').'upload_funcs.php');
    $up = new upload_process($_POST['massiveedit']);
    $destination_table = $up->access_check($selected_form_id,$selected_form_type);
    if (!$destination_table) {
        http_response_code(401);
        echo common_message('error',str_access_denied);
        exit;
    }

    $res = $up->new_upload($destination_table);
    // check it is failed or success? No, just send message...
    echo $up->messages[0];

    exit;
}
/* Save dragndrop columns' order in file upload
 * called: uploader.js
 */
if(isset($_POST['template_post']) and isset($_POST['template_name']) and isset($_SESSION['Tid'])) {
    session_write_close();
    $table = PROJECTTABLE;

    if ($_POST['template_name'] == '') {
        echo common_message('error','No name??');
        exit;
    }

    $j = json_decode($_POST['template_post']);
    if ($_POST['sheet_name']=='')
        $cmd = sprintf("DELETE FROM settings_import WHERE user_id='{$_SESSION['Tid']}' AND project_table='".PROJECTTABLE."' AND name=%s AND sheet IS NULL",quote($_POST['template_name']));
    else
        $cmd = sprintf("DELETE FROM settings_import WHERE user_id='{$_SESSION['Tid']}' AND project_table='".PROJECTTABLE."' AND name=%s AND sheet=%s",quote($_POST['template_name']),quote($_POST['sheet_name']));
    $n = pg_query($BID,$cmd);

    $cmd = sprintf("INSERT INTO settings_import (user_id,project_table,name,sheet,assignments) VALUES (%d,'%s',%s,%s,%s) RETURNING name",$_SESSION['Tid'],$table,quote($_POST['template_name']),quote($_POST['sheet_name']),quote(json_encode($j->{'assignments'})));
    $res = pg_query($BID,$cmd);
    if(!pg_num_rows($res)) {
        $errorID = uniqid();
        echo common_message('fail','Insert error',$errorID);
        log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
    } else {
        $row = pg_fetch_assoc($res);
        echo common_message('ok',str_done.": ".$row['name'].".");
    }
    exit;
}
// nem használom sehol
if (isset($_POST['wkt_valid']) and vreq()) {
    session_write_close();
    if(is_wkt($_POST['wkt_valid'])) echo 1;
    else echo 0;
    exit;
}
/* upload_show-table-form - uploader.js
 * returnig geometry as WKT string
 * */
if(isset($_POST['getpolygon'])) {
    session_write_close();
    global $ID;
    $post_id=preg_replace("/[^0-9]/","",$_POST['getpolygon']);
    $text = "";

    if(isset($_SESSION['Tid'])) $tid = $_SESSION['Tid'];
    else $tid = 0;

    $cmd = sprintf('SELECT ST_AsText(geometry) as q
            FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id
            WHERE select_view IN (\'2\',\'3\',\'only-upload\',\'select-upload\') AND p.user_id=\'%d\' AND id=\'%d\'',$tid,$post_id);

    $res = pg_query($ID,$cmd);
    if ($res) {
        $row = pg_fetch_assoc($res);
        $text .= $row['q'];
    }
    //module call
    /*if (isset($_SESSION['Tid']))
        $a = "0,1";
    else
        $a = "0";
    $cmd = sprintf("SELECT \"function\" as f,module_name,array_to_string(params,';') as p FROM modules WHERE \"enabled\"=TRUE AND \"file\"=afuncs.php' AND project_table='%s' AND module_access IN ($a)",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $modules = array();
    if(pg_num_rows($res)) {
        $modules = pg_fetch_all($res);
        if(file_exists(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/afuncs.php')) {
            if( php_check_syntax(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/afuncs.php','e')) {
                include_once(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/afuncs.php');
            } else {
                $error[] = str_module_include_error.': '.$e;
            }
        }
    }
    $mki = array_search('getgeom', array_column($modules, 'module_name'));
    if ($mki!==false and function_exists($modules[$mki]['f'])) {
        //$cmd = call_user_func($modules[$mki]['f']);
        //...valami ilyesmi: $cmd = sprintf('SELECT ST_AsText(geometry) as q FROM milvus_alapegysegek,system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id WHERE select_view IN (2,3) AND p.user_id=\'%d\' AND id=\'%d\'',$tid,$post_id);

    } elseif ($mki!==false) {
        #require_once(getenv('OB_LIB_DIR').'default_modules.php');
        //default itegrated module
        #$dmodule = new defModules();
        #$dmodule->set('upload_show-table-form.php');
        #$modules_div .= $dmodule->photo_div();
    }*/
    echo $text;
    exit;
}
/* Save draw selection on main map
 * functions.js GetDataOfSelection()
 * auto called ajax function
 * can't be standalone vreq()
 * */
if (isset($_POST['auto_save_selection']) and vreq()) {

    /* Update Login vars - refresh auth tokens */
    if (isset($_COOKIE['access_token'])) {
        $cookie = json_decode( $_COOKIE[ "access_token" ] );

        $res = true;

        if (time() > $cookie->expiry) {
            // expired
            $res = refresh_token();
            $cookie = json_decode( $_COOKIE[ "access_token" ] );
        }

        if ($res) {
            // JUST check token valisity by a resource request
            $result = oauth_webprofile_request($cookie->data->access_token);

            if ($result === FALSE) {
                if (!isset($_SESSION['Tid'])) {
                    if(!oauth_login($cookie->data->access_token)) {
                        setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
                        unset($_COOKIE['access_token']);
                    }
                }
            }
        }
    } elseif (isset($_COOKIE['refresh_token'])) {
        $cookie = json_decode( $_COOKIE[ "refresh_token" ] );

        if (time() < $cookie->expiry) {
            $res = refresh_token();
            if ($res!==false) {
                $cookie = json_decode( $_COOKIE[ "access_token" ] );
                oauth_login($cookie->data->access_token);
            }
        }
    }

    if (AutoSaveGeometry($_POST['auto_save_selection']))
        echo common_message('ok');
    else
        echo common_message('error',"Error in auto save geometry");
    exit;
}
/* Main map page
 * Save queries: main.js
 * lekérdezés eltárolása, eredmények eltárolása, selections save
 * */
if (isset($_POST['savequery'])) {
    if(!isset($_SESSION['Tid'])) {
        echo "Should be logined!";
        exit;
    }
    # csak bejelentkezett felhasználóknak
    $label = substr($_POST['saveqlabel'], 0, 255);
    echo SaveQuery($label,$_POST['savequery'] );
    exit;
}
/* Interconnect
 * Share Query AJAX call
 * sending request to remote OpenBioMaps project
 * posting csv data to remote OpenBioMaps project
 * */
if (isset($_POST['shareQuery'])) {
    require(getenv('OB_LIB_DIR').'results_builder.php');
    $r = new results_builder(['method'=>'csv','filename'=>'shared query','sep'=>',','quote'=>'"']);
    if ($r->results_query()) {
        ob_start();
        $_SESSION['force_use'] = array('results_asCSV' => true);
        $r->printOut();
        unset($_SESSION['force_use']);
        $out = ob_get_clean();

        $ret = interconnect_master_connect($_POST['shareQuery']);

        if (isset($ret['failed'])) {
            echo common_message('error',$ret['failed']);
            exit;
        }

        $accept_key = '';
        $request_key = '';
        if (isset($ret['accept_key']))
            $accept_key = $ret['accept_key'];
        elseif (isset($ret['request_key']))
            $request_key = $ret['request_key'];

        // sending GET request
        // remote server add this request to interconnect_slave table
        if ($accept_key!='')
            $res = file_get_contents($_POST['shareQuery']."index.php?interconnect_server=".URL."&interconnect_request=$accept_key");
        elseif ($request_key!='')
            $res = file_get_contents($_POST['shareQuery']."index.php?interconnect_server=".URL."&interconnect_request=$request_key");
        else {
            echo common_message('error','key work failed');
            exit;
        }

        $j = json_decode($res,true);
        if ($j['status']=='success') {

            if ($ret) {
                // send csv file content to remote server
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $_POST['shareQuery']."pds.php",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTPHEADER => array('Accept: text/json'),
                    CURLOPT_POST => 1,
                    )
                );

                //Save string into temp file
                $file = tempnam(sys_get_temp_dir(), 'POST');
                file_put_contents($file, $out);

                //Post file
                $post = array();
                if ($accept_key!='')
                    $post = array(
                        #"interconnect_post_file"=>'@'.$file,
                        "accept_key"=>$accept_key,
                        "request_key"=>'',
                        "scope"=>"interconnect_post_file_with_key",
                        "slave_project"=>PROJECTTABLE,
                        "slave_server"=>URL,
                    );
                else
                    $post = array(
                        #"interconnect_post_file"=>'@'.$file,
                        "accept_key"=>'',
                        "request_key"=>$request_key,
                        "scope"=>"interconnect_post_file_with_key",
                        "slave_project"=>PROJECTTABLE,
                        "slave_server"=>URL,
                    );

                $post['interconnect_post_file'] = new CurlFile($file, 'text/csv');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

                $result = curl_exec($curl);

                //Remove the file
                unlink($file);

                echo common_message('ok',str_data_posted);
            }
        } else {
            echo common_message('error',$j['data']);

        }
    }

    exit;
}
/* Set name, accessibility of own polygons
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['own_polygon_update'])) {
    session_write_close();

    $polygon_id = preg_replace('/[^0-9]/','',$_POST['own_polygon_update']);
    $f = array();
    if (isset($_POST['own_poly_access'])) $f[] = "access='".preg_replace('/[^a-z]/','',$_POST['own_poly_access'])."'";
    if (isset($_POST['own_poly_name'])) $f[] = "name=".quote($_POST['own_poly_name']);

    $update_polygon_ok_msg = '';
    $filters = implode(',',$f);
    if ($filters != '') {
        $cmd = "UPDATE system.shared_polygons SET $filters WHERE id=$polygon_id AND user_id={$_SESSION['Tid']} RETURNING name";
        $res = pg_query($GID,$cmd);
        if (pg_affected_rows($res)) {
            $update_polygon_ok_msg = "Updated polygon: $polygon_id";
            $row = pg_fetch_assoc($res);
            $polygon_name = $row['name'];
            if (isset($_POST['own_poly_access']) and $_POST['own_poly_access']=='public') {
                insertNews("{$_SESSION['Tname']} shared a new polygon: $polygon_name",'public');
            }
            elseif (isset($_POST['own_poly_access']) and $_POST['own_poly_access']=='project') {
                insertNews("{$_SESSION['Tname']} shared a new polygon: $polygon_name",'project');
            }
            echo common_message('ok',$update_polygon_ok_msg);
            exit;
        }
        elseif (pg_last_error($GID)) {
            $errorID = uniqid();
            echo common_message('failed',str_sql_error,$errorID);
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            exit;
        }
    }

    echo common_message('error',"An error occured");

    exit;
}
/* Set name, accessibility of all polygons
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['set_polygon_update'])) {
    session_write_close();

    if (!has_access('master')) {
        echo common_message('error',str_access_denied);
        exit;
    }

    $polygon_id = preg_replace('/[^0-9]/','',$_POST['set_polygon_update']);
    $f = array();
    if (isset($_POST['set_poly_access'])) $f[] = "access='".preg_replace('/[^a-z]/','',$_POST['set_poly_access'])."'";
    if (isset($_POST['set_poly_name'])) $f[] = "name=".quote($_POST['set_poly_name']);

    $update_polygon_ok_msg = '';
    $filters = implode(',',$f);
    if ($filters != '') {
        $cmd = "UPDATE system.shared_polygons SET $filters WHERE id=$polygon_id RETURNING name";
        $res = pg_query($GID,$cmd);
        if (pg_affected_rows($res)) {
            $update_polygon_ok_msg = "Updated polygon: $polygon_id";
            $row = pg_fetch_assoc($res);
            $polygon_name = $row['name'];
            if (isset($_POST['set_poly_access']) and $_POST['set_poly_access']=='public') {
                insertNews("{$_SESSION['Tname']} shared a new polygon: $polygon_name",'public');
            }
            elseif (isset($_POST['set_poly_access']) and $_POST['set_poly_access']=='project') {
                insertNews("{$_SESSION['Tname']} shared a new polygon: $polygon_name",'project');
            }
            echo common_message('ok',"$polygon_id updated");
            exit;
        }
        elseif (pg_last_error($GID)) {
            $errorID = uniqid();
            echo common_message('error',str_sql_error,$errorID);
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            exit;
        }
    }

    echo common_message('fail',"An error occured");
    exit;
}

/* Update own polygon attributes to show/hide in for selecting or uploading
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['showhide_polygon'])) {
    session_write_close();
    $id = preg_replace('/[^0-9]/','',$_POST['showhide_polygon']);
    $show = preg_replace('/[^a-z-]/','',$_POST['show']);
    $type = preg_replace('/[^a-z-]/','',$_POST['type']);


    $S = "select_view='none'";
    if ($show=='none' and $type=='upload') {$S = "select_view='only-upload'";$ret='only-upload';}
    elseif ($show=='none' and $type=='select') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='only-upload' and $type=='upload') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='only-upload' and $type=='select') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='upload') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='select') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='select-upload' and $type=='upload') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='select-upload' and $type=='select') {$S = "select_view='only-upload'";$ret='only-upload';}

    $cmd = "SELECT 1 FROM system.polygon_users WHERE user_id='{$_SESSION['Tid']}' AND polygon_id='$id'";
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res))
        $cmd = "UPDATE system.polygon_users SET $S WHERE user_id='{$_SESSION['Tid']}' AND polygon_id='$id'";
    else
        $cmd = "INSERT INTO system.polygon_users (polygon_id,user_id,select_view) VALUES ($id,{$_SESSION['Tid']},'$ret')";

    $res = pg_query($GID,$cmd);

    if (pg_affected_rows($res)) {
        echo common_message('ok',$ret);
    } else {
        $errorID = uniqid();
        echo common_message('fail',str_sql_error,$hash);
        log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
    }

    exit;
}
/* Update force polygon attributes to show/hide in for selecting or uploading
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['force_showhide_polygon'])) {
    session_write_close();

    if (!has_access('master')) {
        echo common_message('error',str_access_denied);
        exit;
    }

    $id = preg_replace('/[^0-9]/','',$_POST['force_showhide_polygon']);
    $show = preg_replace('/[^a-z-]/','',$_POST['show']);
    $type = preg_replace('/[^a-z-]/','',$_POST['type']);
    $ret = $show;

    $S = "select_view='$show'";
    if ($show=='none' and $type=='upload') {$S = "select_view='only-upload'";$ret='only-upload';}
    elseif ($show=='none' and $type=='select') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='only-upload' and $type=='upload') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='only-upload' and $type=='select') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='upload') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='select') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='select-upload' and $type=='upload') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='select-upload' and $type=='select') {$S = "select_view='only-upload'";$ret='only-upload';}

    if (isset($_POST['force_users'])) {
        $ok = 1;
        $hash = '';

        if (is_array($_POST['force_users'])) {
            foreach ($_POST['force_users'] as $u) {

                // user add remove action: clean current user setting for the selected view option
                if ($type == '' and isset($_POST['def_row']) and $_POST['def_row']=='0') {
                    $cmd = sprintf('DELETE FROM system.polygon_users WHERE user_id!=\'%1$s\' AND polygon_id=\'%2$s\' AND select_view=%3$s',$u,$id,quote($show));
                    $res = pg_query($GID,$cmd);
                }

                $cmd = sprintf('SELECT user_id FROM system.polygon_users WHERE user_id=\'%1$d\' AND polygon_id=\'%2$d\'',$u,$id);
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    if ($ret == 'none')
                        $cmd = sprintf('DELETE FROM system.polygon_users WHERE user_id=\'%1$s\' AND polygon_id=\'%2$s\'',$u,$id);
                    else
                        $cmd = sprintf('UPDATE system.polygon_users SET %1$s WHERE user_id=\'%2$s\' AND polygon_id=\'%3$s\'',$S,$u,$id);
                } else {
                    $cmd = sprintf('INSERT INTO system.polygon_users (polygon_id,user_id,select_view) VALUES (%1$d,%2$d,%3$s)',$id,$u,quote($show));
                }
                $res = pg_query($GID,$cmd);
                if (pg_last_error($GID)) {
                    $ok--;
                    $errorID = uniqid();
                    log_action(pg_last_error($GID),__FILE__,__LINE__);
                }
            }
        }
        if ($ok)
            echo common_message('ok',$ret);
        else
            echo common_message('fail','User settings not changed due to some errors. See logs.',$hash);
        exit;
    }

    echo common_message('error','An error occured.');
    exit;
}
/* Drop own polygons
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['own_polygon_drop'])) {
    session_write_close();

    $id = preg_replace('/[^0-9]/','',$_POST['own_polygon_drop']);
    $cmd = "DELETE FROM system.shared_polygons WHERE id=$id AND user_id='{$_SESSION['Tid']}'";
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed','Nothing happened',$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else
        echo common_message('ok',$id);
    exit;
}
/* Drop force polygons
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['force_polygon_drop'])) {
    session_write_close();

    if (!has_access('master')) {
        echo common_message('error',str_access_denied);
        exit;
    }

    $id = preg_replace('/[^0-9]/','',$_POST['force_polygon_drop']);
    $cmd = "DELETE FROM system.shared_polygons WHERE id=$id";
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed','Nothing happened',$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else
        echo common_message('ok',$id);
    exit;
}


/* Drop own queries
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['dropsq'])) {
    session_write_close();
    $d = $_POST['dropsq'];
    $d = array_map('quote',$d);
    $d = implode(',',$d);
    $cmd = sprintf("DELETE FROM custom_reports WHERE user_id='{$_SESSION['Tid']}' AND project='".PROJECTTABLE."' AND id IN (%s)",$d);
    $res = pg_query($BID,$cmd);
    if(!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error",$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        echo common_message('ok',"OK");

    }
    exit;
}
if (isset($_SESSION['Tid']) and isset($_POST['droprq'])) {
    session_write_close();
    $d = $_POST['droprq'];
    $d = array_map('quote',$d);
    $d = implode(',',$d);
    $cmd = sprintf("DELETE FROM queries WHERE user_id='{$_SESSION['Tid']}' AND datetime IN (%s)",$d);
    $res = pg_query($BID,$cmd);
    if(!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error",$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        echo common_message('ok',"OK");

    }
    exit;
}


/* upload reset forms
 * */
if(isset($_POST['closepage']) and isset($_SESSION['upload'])) {
    $key = array_search($_POST['closepage'],$_SESSION['upload']['openpages']);
    if (false !== $key)
    {
        unset($_SESSION['upload']['openpages'][$key]);
    }
    exit;
}
/* Drop selected imports
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['drop_import'])) {
    session_write_close();
    $ref = $_POST['drop_import'];
    $ref = array_map('quote',$ref);
    $ref = implode(',',$ref);

    $cmd = sprintf("DELETE FROM system.imports WHERE ref IN (%s) AND user_id='%d' AND project_table='%s' RETURNING file,ref",$ref,$_SESSION['Tid'],PROJECTTABLE);
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error",$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        while ($row = pg_fetch_assoc($res)) {
            //drop saves
            $cmd = sprintf("DROP TABLE IF EXISTS temporary_tables.%s_s%s",$row['file'],$row['ref']);
            $res = pg_query($GID,$cmd);
        }
        echo common_message('ok',"OK");
    }
    exit;
}
/* Drop selected imports by admin
 * project_admin.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['drop_1import'])) {
    session_write_close();
    $ref = $_POST['drop_1import'];

    $cmd = sprintf("DELETE FROM system.imports WHERE ref IN (%s) AND project_table='%s' RETURNING file,ref",quote($ref),PROJECTTABLE);
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error",$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        while ($row = pg_fetch_assoc($res)) {
            //drop saves
            $cmd = sprintf("DROP TABLE IF EXISTS temporary_tables.%s_s%s",$row['file'],$row['ref']);
            $res = pg_query($GID,$cmd);
        }
        echo common_message('ok',"OK");
    }
    exit;
}
if (isset($_SESSION['Tid']) and isset($_POST['save_import_template'])) {
    session_write_close();
    $d = $_POST['save_import_template'];

    $name = $_POST['name'];

    $cmd = sprintf("UPDATE system.imports SET template_name=%s WHERE ref=%s AND user_id='%d' AND project_table='%s'",quote($name),quote($d),$_SESSION['Tid'],PROJECTTABLE);
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error",$errorID);
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    }
    exit;
}


/* Drop selected access_token key
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['drop_apikey']) and isset($_POST['tokenType'])) {
    session_write_close();

    if ($_POST['tokenType']=="access")
        $cmd = sprintf("DELETE FROM oauth_access_tokens WHERE access_token=%s AND user_id='%s'",quote($_POST['drop_apikey']),$_SESSION['Tmail']);
    elseif($_POST['tokenType']=="refresh")
        $cmd = sprintf("DELETE FROM oauth_refresh_tokens WHERE refresh_token=%s AND user_id='%s'",quote($_POST['drop_apikey']),$_SESSION['Tmail']);

    pg_query($BID,$cmd);
    exit;
}

/* Manage photos on photo_div
 * save,delete buttons
 * update comments and connections
 *
 * */
if (isset($_POST['photomanage']) and isset($_POST['option'])) {
    session_write_close();
    $options = array("save","delete","crop","updateconnection");
    // options validation...

    if ($_POST['option']=='delete') {
        /* Unlink files from system.file_connect table
         *
         * */
        $file = $_POST['photomanage'];
        $conid = preg_split('/,/',$_POST['conid']);
        $conid = implode(',',array_map('quote',$conid));

        if (isset($_SESSION['Tid']))
            $t = $_SESSION['Tid'];
        else
            $t = '0';

        $filter = sprintf(" AND user_id=%d",$t);
        if ($t==0) {
            $filter .= sprintf(" AND sessionid='%s' AND status='progress'",session_id());
        }
	//super adminnak mindent lehet
        if (has_access('master')) {
            $filter = "";
        }

        // unlink file_connection
        $cmd = sprintf('SELECT id FROM system.files WHERE reference=%s %s',quote($file),$filter);
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $file_id = $row['id'];

            $cmd = sprintf('SELECT conid FROM system.file_connect WHERE file_id=%d AND conid IN (%s)',$row['id'],$conid);
            $res = pg_query($GID,$cmd);
            if (!pg_num_rows($res)) {
                // new upload not connected yet
                $cmd = sprintf("DELETE FROM system.files WHERE id=%d AND sessionid='%s' AND status='progress' AND user_id=%d",$file_id,session_id(),$t);
                $res = pg_query($GID,$cmd);
                if (pg_affected_rows($res)) {
                    unlink_attachemnt($file);
                    echo common_message("ok", "done");
                } else {
                    $errorID = uniqid();
                    echo common_message('failed',"Error",$errorID);
                    log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                }

            } else {
                $cmd = sprintf('DELETE FROM system.file_connect WHERE file_id=%d AND conid IN (%s)',$row['id'],$conid);
                $res = pg_query($GID,$cmd);
                if (pg_affected_rows($res))
                    echo common_message("ok", "done");
                else {
                    $errorID = uniqid();
                    echo common_message('failed',"Error",$errorID);
                    log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                }
            }
            exit;
        }
        echo common_message("error", "error");
        // at the moment it can do only this
        exit;

        // delete from system.files


        // and finally phisically remove file
        if ($_POST['unlink_files']=='yes' and $file!='') {
            exit;
            $lc = new LocaleManager();
            $lc->doBackup();
            $lc->fixLocale();

            $root_dir = getenv('PROJECT_DIR').'attached_files';
            unlink($root_dir.'/'.basename($file));
                log_action("unlink $root_dir/".basename($file),__FILE__,__LINE__);

            #search for thumbnails - delete them all
            chdir("$root_dir/thumbnails/");
            //$list = glob("$root_dir/thumbnails/*'".basename($file)."'");
            // Search for all files that match .* or *
            $list = glob("*{".basename($file)."}", GLOB_BRACE);
            foreach($list as $s) {
                log_action("unlink $s",__FILE__,__LINE__);
                unlink($s);
            }
            $lc->doRestore();
        }
        exit;
    }
    elseif($_POST['option']=='save') {
        /* update comment of a file in system.files
         *
         */
        $id = $_POST['data-id'];
        $id = preg_replace('/[^0-9]+/','',$id);
        $reference = $_POST['photomanage'];
        $comment = "";
        if (isset($_POST['comment'])) $comment = $_POST['comment'];

        if (isset($_SESSION['selected_data-table']))
            $data_table = $_SESSION['selected_data-table'];
        else
            $data_table = PROJECTTABLE;

        // filters
        if (isset($_SESSION['Tid']))
            $t = $_SESSION['Tid'];
        else
            $t = 0;
        $filter = sprintf(" AND user_id=%d",$t);
        if ($t==0) {
            $filter .= sprintf(" AND sessionid='%s'",session_id());
        }
        if (has_access('master')) {
            $filter = "";
        }

        $cmd = sprintf("SELECT id,reference,comment FROM system.files WHERE id=%s and data_table=%s AND project_table='%s' %s",quote($id),quote($data_table),PROJECTTABLE,$filter);
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $rt = pg_fetch_assoc($res);
            $connect_reference = $rt['reference'];
            $cmd = sprintf("UPDATE system.files SET comment=%s WHERE id=%s AND data_table=%s AND project_table='%s' %s",quote($comment),quote($id),quote($data_table),PROJECTTABLE,$filter);
            $res = pg_query($GID,$cmd);
            if (pg_affected_rows($res))
                echo common_message('ok',pg_affected_rows($res));
            else {
                $errorID = uniqid();
                echo common_message('failed',"Error",$errorID);
                log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
            }
        } else {
            echo common_message('fail','given file does not exists or not accessible.');
            exit;
        }
        exit;
    } elseif($_POST['option']=='updatetable') {
        /* Update data-table attribute in system.files
         *
         * */
        if (!has_access('master')) {
            echo common_message('error',str_access_denied);
            exit;
        }

        $file_id = $_POST['photomanage'];

        $cmd = sprintf("UPDATE system.files SET data_table=%s WHERE id=%d",quote($_POST['tablelink']),$file_id);
        $res = pg_query($GID,$cmd);
        if (pg_affected_rows($res))
            echo common_message('ok','ok');
        else {
            $errorID = uniqid();
            echo common_message('failed',"Error",$errorID);
            log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
        }

    } elseif($_POST['option']=='addtable') {
        /* Insert new record into system.files
         *
         * */

        if (!isset($_SESSION['Tid'])) {
            echo common_message('error',str_access_denied);
            exit;
        }

        $ref = $_POST['photomanage'];

        if ($_POST['tablelink']!='') {

            $root_dir = getenv('PROJECT_DIR').'attached_files';
            $sum = sha1_file("$root_dir/$ref");
            $finfo = new finfo(FILEINFO_MIME);
            $mimetype = $finfo->file("$root_dir/$ref");

            // exif creation
            if (in_array(exif_imagetype("$root_dir/$ref"),array(IMAGETYPE_JPEG ,IMAGETYPE_TIFF_II , IMAGETYPE_TIFF_MM))) {
                $exif_read = exif_read_data("$root_dir/$ref", 0, false);
                if (json_encode($exif_read)) {
                    $exif = $exif_read;
                } else
                    $exif = array($exif_read['FileName'],$exif_read['FileDateTime'],$exif_read['FileSize'],$exif_read['FileType'],$exif_read['MimeType'],$exif_read['Model'],'corrupted exif...');
            } else {
                $a = exec("mediainfo $root_dir/$ref",$output);
                $n = array();
                foreach ($output as $o) {
                    $on = preg_split("/\s+: /",$o);
                    if(count($on)>1) {
                        if($on[0]=='Complete name') {
                            $on[1] = basename($on[1]);
                        }
                        $n[$on[0]] = $on[1];
                    }
                }
                $exif = $n;
            }
            $filetime = filemtime("$root_dir/$ref");


            $cmd = sprintf("INSERT INTO system.files (project_table,reference,datum,access,user_id,status,sessionid,sum,mimetype,data_table,exif)
                VALUES('%s',%s,%s,0,%d,%s,'%s',%s,%s,%s,%s) RETURNING id",PROJECTTABLE,quote($ref),quote(date ("Y-m-d H:i:s", $filetime)),0,quote('manual'),0,quote($sum),quote($mimetype),quote($_POST['tablelink']),quote(json_encode($exif)));

            $res = pg_query($GID,$cmd);
            if (pg_affected_rows($res))
                echo common_message('ok','ok');
            else {
                $errorID = uniqid();
                echo common_message('failed',"Error",$errorID);
                log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            }
        }

    } elseif($_POST['option']=='updateconnection') {
        /* Insert new record into system.file_connect
         * based on existing obm_files_id content
         * */

        if (!isset($_SESSION['Tid'])) {
            echo common_message('error',str_access_denied);
            exit;
        }

        $sessionid = session_id();

        if(isset($_SESSION['selected_data-table']))
            $data_table = $_SESSION['selected_data-table'];
        else
            $data_table = PROJECTTABLE;

        $file_id = $_POST['photomanage'];

        if ($_POST['datalink']!='') {
            // simplified, multiple links not handled, see below!!!
            $cmd = sprintf("SELECT obm_files_id FROM %s WHERE obm_id=%d",$data_table,$_POST['datalink']);
            $res = pg_query($GID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if ($row['obm_files_id']!='') {
                    // typically bad file connect update - a fixing method which will hopefully be updated
                    $obm_files_id = $row['obm_files_id'];
                    $cmd = sprintf("INSERT INTO system.file_connect (file_id,conid,temporal,sessionid,rownum) VALUES(%d,'%s',FALSE,'%s',NULL)",$file_id,$obm_files_id,$sessionid);
                    $res = pg_query($GID,$cmd);
                    if (pg_affected_rows($res))
                        echo common_message('ok','ok');
                    else {
                        $errorID = uniqid();
                        echo common_message('failed',"Error",$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
                    }
                } else {
                    // data record has no previous file connects, therefore a new connection id should be created or connect to an existing one.
                    // ...
                    #$conid = crypt($file_id.'1'.microtime(true),$sessionid);
                    $conid = uniqid($_SESSION['Tid']."$",true);

                    $cmd = sprintf("INSERT INTO system.file_connect (conid,file_id,sessionid,rownum,temporal) VALUES ('%s',%d,'%s',NULL,FALSE) RETURNING conid",$conid,$file_id,$sessionid);
                    $res = pg_query($ID,$cmd);
                    if (pg_last_error($ID)) {
                        $errorID = uniqid();
                        echo common_message('failed',"Error",$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);

                        exit;
                    }

                    $cmd = sprintf("UPDATE %s SET obm_files_id='%s' WHERE obm_id=%d",$data_table,$conid,$_POST['datalink']);
                    $res = pg_query($ID,$cmd);
                    if (pg_last_error($ID)) {
                        $errorID = uniqid();
                        echo common_message('failed',"Error",$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                        exit;
                    }

                    echo common_message('ok','ok');
                    exit;
                }
            } else {
                echo common_message('error','fail');
                exit;
            }
        } else {
            // drop connection
            // ...
            echo common_message('error','Unlink data from connections is not implemented yet.');
            exit;
        }


    } elseif($_POST['option']=='updatenewconnection') {
        echo common_message('error','not implemented');

    } else {
        //unknown option

        echo common_message('error','fail');
        return false;
    }
    exit;
}
/* taxon name list load
 * authorisation not needed
 * */
if (isset($_POST['tpost'])) {
    session_write_close();
    global $ID;
    $where = array();
    $WHERE = '';

    # trgm_string ["8396#Abramisbalerus"]
    if (isset($_REQUEST['trgm_string']) and $_REQUEST['trgm_string']!='') {
        $wo = array();
        $jo = json_decode($_REQUEST['trgm_string'],true);
        $lq = array();
        $id = '';
        $taxon = '';
        $a_id = array();
        $a_names = array();
        foreach ($jo as $j) {
            $ilt = preg_split('/#/',$j);
            $id = $ilt[0];
            $taxon = $ilt[1];
            $a_id[] = $id;
            $a_name[] = $taxon;
        }
        if ($_REQUEST['allmatch'] and count($a_id)) {
            array_push($where,sprintf("taxon_id IN (%s)",implode(',',$a_id)));
        } else {
            array_push($where,sprintf("meta IN (%s)",implode(',',array_map('quote',$a_name))));
        }
    }
    if (count($where))
        $WHERE = "WHERE ".implode('AND ',$where);

    $species_array = array_unique(array_merge(array($_SESSION['st_col']['SPECIES_C']),$_SESSION['st_col']['ALTERN_C']));
    $species_array = array_filter($species_array);

    /*$species_array = array();
    $cmd = "SELECT DISTINCT lang lang FROM ".PROJECTTABLE."_taxon";
    $result = pg_query($ID,$cmd);
    while(pg_fetch_assoc($row = $result)) {
        $species_array[] = $row['lang'];
    }*/

    $join = array();
    foreach($species_array as $column) {
        $join[] = "t.word=$column";
    }
    $join_s = implode(" OR ",$join);

    $o = implode(",",$species_array);

    $table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
    $cmd = "SELECT $o,t.taxon_id as id FROM $table LEFT JOIN ".PROJECTTABLE."_taxon t ON ($join_s) $WHERE GROUP BY $o,id ORDER BY $o";
    //log_action($cmd);
    $result = pg_query($ID,$cmd);

    $t = array();
    $allid = array();
    while ($row = pg_fetch_assoc($result)) {
        $allid[] = $row['id'];
        $n = "";
        //$diff_counter = array();
        $missing_taxon_id = 0;
        foreach (array_values($species_array) as $s) {
            if (mb_strtolower(mb_ereg_replace(' ','',$row[$s]),'UTF-8') == mb_strtolower($taxon,'UTF-8')) {
                $n .= sprintf("<td style='padding-right:20px'><a href='$protocol://".URL."/?metaname&id={$row['id']}' style='font-variant:normal' target='_blank'>%s</a></td>",$row[$s]);
                $diff_counter[] = $row['id'];
            } elseif ($row[$s] != '') {
                $cmd = sprintf("SELECT taxon_id FROM ".PROJECTTABLE."_taxon t WHERE word=%s",quote($row[$s]));
                $res2 = pg_query($ID,$cmd);
                if (pg_num_rows($res2)) {
                    $row2 = pg_fetch_assoc($res2);
                    $n .= sprintf("<td style='padding-right:20px'><a href='$protocol://".URL."/?metaname&id={$row2['taxon_id']}' target='_blank'>%s</a></td>",$row[$s]);
                    //$diff_counter[] = $row2['taxon_id'];
                    $allid[] = $row2['taxon_id'];
                } else {
                    $n .= "<td style='padding-right:20px'><a href='$protocol://".URL."/?metaname&id={$row['id']}' style='font-variant:normal;color:red' target='_blank' title='This name is missing from the taxon table, please add manually: INSERT INTO ...'>{$row[$s]}</a></td>";
                    $missing_taxon_id = 1;
                }
            }
        }
        //if (count(array_unique($diff_counter))>1)
        if ($missing_taxon_id)
            $t[] = "<tr><td style='padding:2px 8px 0px 2px'><i class='fa fa-2x fa-warning' style='color:red'></i></td>$n</tr>";
        else
            $t[] = "<tr><td style='padding:2px 8px 0px 2px'><i class='fa fa-2x fa-check' style='color:green'></i></td>$n</tr>";
    }
    $out = "<table style='border-top:2px solid #dadada'>";
    $out .= "<tr><th style='border-bottom:1px solid lightgray'></th>";
    foreach($species_array as $sp) {
        $out .= "<th style='border-bottom:1px solid lightgray'>$sp</th>";
    }
    $out .= "<tr>";
    foreach ($t as $row) $out .= $row;
    $out .= "</table>";

    if (count(array_unique($allid))>1) {
        $out .= sprintf("<a href='$protocol://".URL."/?metaname&id[]=%s' style='font-variant:normal' target='_blank' class='pure-button button-href'>%s</a>",implode('&id[]=',array_unique($allid)),str_all_names);
    }
    echo $out;
    exit;
}
//not used yet
if(isset($_POST['change_lang'])) {
    $_SESSION['LANG'] = $_POST['change_lang'];
    exit;
}
/* Upload data
 * upload_show-web-form
 * generated list from autocomplete
 * */
if(isset($_POST['genlist']) and isset($_POST['term'])) {

    $selected_form_id = preg_replace('/[^0-9]/','',$_POST['form_page_id']);
    $term = preg_replace('/;/','',$_POST['term']);
    $id = preg_replace('/;/','',$_POST['id']);

    if (isset($_SESSION['upload']) and $selected_form_id) {
        $table = "";
        $column = "";
        if (is_numeric($id)) {
            $c = $_SESSION['theader'][$id-1];

            if (isset($_SESSION['upload_column_assign'])) {
                $c = $_SESSION['upload_column_assign'][$id-1];
            }

        } else {
            if (preg_match('/default-/',$id))
                $c = preg_replace('/default-/','',$id);
            elseif (preg_match('/f-/',$id)) {
                ## autofill row
                $c = preg_replace('/f-/','',$id);

                if (isset($_SESSION['upload_column_assign'])) {
                    $c = $_SESSION['upload_column_assign'][$c-2];
                } else {
                    $c = $_SESSION['theader'][$c-2];
                }
            } else {
                //should be implemented in upload_show-table-form.php and uploader.js the column name as
                //an attribute
                $c = $id;
            }
        }

        $cmd = sprintf("SELECT \"column\",description,\"type\",\"control\",array_to_string(\"count\",':') as cnt,array_to_string(list,',') as list,obl,fullist,genlist,relation,pseudo_columns,custom_function,default_value,list_definition FROM project_forms_data WHERE form_id=%s AND \"column\"=%s", quote($selected_form_id), quote($c));

        $res = pg_query($BID,$cmd);
        $x = array();
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);

            $sel = new upload_select_list($row, $term);
            $options = $sel->get_options('array');
            echo json_encode(array_column($options,'value'));
        }
        else {
            log_action('project_forms_data query returned with no results',__FILE__,__LINE__);
            echo json_encode(array());
        }
    }
    exit;
}
/* CoL request
 *
 * */
if (isset($_POST['fetch_remote_data'])) {
    //some string validation??
    $string = $_POST['url'];

    //Catalogue of Life
    if ($_POST['fetch_remote_data']=='CoL') {
        $url = "http://www.catalogueoflife.org/col/webservice?name=$string";
        $data = fetch_remote_xml($url);
        $sa = "";
        if (is_object($data)) {
            if ($data['total_number_of_results']!='0') {
                //http://www.catalogueoflife.org/col/search/all/key/Pica+pica/fossil/0/match/1
                $sa .= $data['total_number_of_results']." records found.<br>";
                if ($data['total_number_of_results'] > 1)
                    $sa .= "<a href='http://www.catalogueoflife.org/col/search/all/key/$string/fossil/0/match/1' target='_blank'>Catalogue of Life</a><br>";
                else {
                    $sa .= '<a href="'.$data->result->url.'" target="_blank">Catalogue of Life</a><br>';
                    $sa .='Rank:<b>'.$data->result->rank."</b><br>";
                    $sa .='Name status:<b>'.$data->result->name_status."</b><br>";
                }
            } else {
                $sa .="<a href='$url' target='_blank'>Catalogue of Life</a><br>";
                $sa .= "No results.";
            }
        } else {
            $sa .="<a href='$url' target='_blank'>Catalogue of Life</a><br>";
            $sa .= "URL fetch error.";
        }
        echo $sa;
    }
    //Global Name Index
    if ($_POST['fetch_remote_data']=='GNI') {
        $url = "http://gni.globalnames.org/name_strings.xml?search_term=$string";
        $data = fetch_remote_xml($url);
        $sa = "";
        if (is_object($data)) {
            if(!$data->name_strings[0]['nil']) {
                $sa .= '<a href="'.preg_replace('/\.xml/','.html',$url).'" target="_blank">Global Names Index</a><br>';
                $sa .='Number of records:<b>'.$data->name_strings_total."</b><br>";
            } else {
                $sa .="<a href='$url' target='_blank'>Global Names Index</a><br>";
                $sa .= "No results.";
            }
        } else {
            $sa .="<a href='$url' target='_blank'>Global Names Index</a><br>";
            $sa .= "URL fetch error.";
        }
        echo $sa;
    }
    //Enciclopedia of Life
    if ($_POST['fetch_remote_data']=='EoL') {
        $url = "https://eol.org/api/search/1.0.json?q=$string&page=1&exact=false&filter_by_taxon_concept_id=&filter_by_hierarchy_entry_id=&filter_by_string=&cache_ttl=";
        //$data = fetch_remote_xml($url);
        $data = file_get_contents($url);
        $sa = "";
        //if (is_object($data)) {
        if (is_json($data)) {
            $j = json_decode($data);
            //if(isset($data->entry)) {
            if (isset($j->{'totalResults'})) {
                //$sa .= '<a href="https://eol.org/pages/'.$data->entry[0]->id.'/overview" target="_blank">Enciclopedia of Life</a><br>';
                $sa .= '<a href="'.$j->{'results'}[0]->{'link'}.'" target="_blank">Enciclopedia of Life</a><br>';
                //$sa .='Number of records:<b>'.count($data->entry)."</b><br>";
                $sa .='Number of records:<b>'.$j->{'totalResults'}."</b><br>";
            } else {
                $sa .="<a href='https://eol.org/api/search/1.0.json?q=$string' target='_blank'>Enciclopedia of Life</a><br>";
                $sa .= "No results.";
            }
        } else {
            $sa .="<a href='https://eol.org/api/search/1.0.json?q=$string' target='_blank'>Enciclopedia of Life</a><br>";
            $sa .= "URL fetch error.";
        }
        echo $sa;
    }
    //GBiF
    //https://api.gbif.org/v1/species?name=Puma%20concolor
    //https://api.gbif.org/v1/species/match?verbose=false&name=Oenante%20oenanthe
    /*
     *  kingdom	"Animalia"
        phylum	"Chordata"
        order	"Carnivora"
        family	"Felidae"
        genus	"Puma"
        species	"Puma concolor"
    */
    if ($_POST['fetch_remote_data']=='GBiF') {
        $url = "https://api.gbif.org/v1/species/match?verbose=false&name=$string";
        //$data = fetch_remote_xml($url);
        $data = file_get_contents($url);
        $sa = "";
        //if (is_object($data)) {
        if (is_json($data)) {
            $j = json_decode($data);
            //if(isset($data->entry)) {
            if (isset($j->{'scientificName'})) {
                //$sa .= '<a href="https://eol.org/pages/'.$data->entry[0]->id.'/overview" target="_blank">Enciclopedia of Life</a><br>';
                //https://api.gbif.org/v1/species/9761484/distributions
                $sa .= "<a href='https://api.gbif.org/v1/species/".$j->{'usageKey'}."/distributions' target='_blank'>Distributions</a><br>";
                $sa .= "Taxonomy:<ul>";
                $sa .= "<li><a href='https://api.gbif.org/v1/species/".$j->{'kingdomKey'}."/children' target='_blank'>".$j->{'kingdom'}.'</a></li>';
                $sa .= "<li><a href='https://api.gbif.org/v1/species/".$j->{'phylumKey'}."/children' target='_blank'>".$j->{'phylum'}.'</a></li>';
                $sa .= "<li><a href='https://api.gbif.org/v1/species/".$j->{'orderKey'}."/children' target='_blank'>".$j->{'order'}.'</a></li>';
                $sa .= "<li><a href='https://api.gbif.org/v1/species/".$j->{'familyKey'}."/children' target='_blank'>".$j->{'family'}.'</a></li>';
                $sa .= "<li><a href='https://api.gbif.org/v1/species/".$j->{'genusKey'}."/children' target='_blank'>".$j->{'genus'}.'</a></li>';
                $sa .= "<li><a href='https://api.gbif.org/v1/species/".$j->{'speciesKey'}."' target='_blank'>".$j->{'scientificName'}."</a></li>";
                $sa .= "</ul>";
            } else {
                $sa .="No results, <a href='https://api.gbif.org/v1/species/match?verbose=true&name=$string' target='_blank'>try fuzzy match?</a><br>";
            }
        } else {
            $sa .="<a href='https://gbif.org' target='_blank'>GBiF</a><br>";
            $sa .= "URL fetch error.";
        }
        echo $sa;
    }
    exit;
}
/* fetch openbiomaps servers */
if (isset($_POST['fetch_obm_servers'])) {

    $url = 'https://openbiomaps.org/projects/openbiomaps_network/index.php?query_api={"available":"up"}&output=json&filename=';
    $data = file_get_contents($url);
    echo $data;
    exit;
}
if (isset($_POST['fetch_obm_projects'])) {

    $url = $_POST['fetch_obm_projects'].'/pds.php?scope=get_project_list';
    $data = file_get_contents($url);
    $j = json_decode($data,true);
    echo json_encode($j['data']);
    exit;
}

/* results show switch button */
if(isset($_POST['showList'])) {
    //if(isset($_SESSION['wfs_full_array']))
    //    $put = $_SESSION['wfs_full_array'];
    //elseif (isset($_SESSION['wfs_array']))
    #if (!isset($_SESSION['wfs_array']))
    #    exit;

    $reverse = 'off';
    if(!isset($_SESSION['show_results'])) {
        $_SESSION['show_results'] = array();

        if(defined('DEFAULT_VIEW_RESULT_METHOD'))
            $_SESSION['show_results']['type'] = constant("DEFAULT_VIEW_RESULT_METHOD");
        else
            $_SESSION['show_results']['type'] = 'list';

        $_SESSION['show_results']['visible'] = 'on';
    } else {
        if ($_SESSION['show_results']['type']!='list') {
            $_SESSION['show_results']['type']='list';
            $_SESSION['show_results']['visible']='on';
        } else {
            $reverse = 'on';
            if ($_SESSION['show_results']['visible']=='off')
                $_SESSION['show_results']['visible']='on';
        }
    }
    if (isset($_SESSION['reverse']) and $_SESSION['reverse']=='on' and $reverse=='on') {
        $reverse = 'off';
        $_SESSION['reverse'] = 'off';
    } elseif (isset($_SESSION['reverse']) and $_SESSION['reverse']=='off' and $reverse=='on') {
        $reverse = 'on';
        $_SESSION['reverse'] = 'on';
    }
    if(!isset($_SESSION['reverse'])) $_SESSION['reverse'] = $reverse;

    if ($_SESSION['show_results']['visible']=='on') {

        include(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>$_SESSION['show_results']['type'],'print'=>'on','reverse'=>$reverse]);
        $r->printOut();
        exit;
        //if ($r->id_queue(array("id"=>$_SESSION['wfs_array']['id'],"geom"=>$_SESSION['wfs_array']['geom'],"data"=>$_SESSION['wfs_array']['data']))) {
        //    if($r->results_query($r->response_id)) {
                //if ($r->fetch_data()) {
        //            $r->printOut();
                    //force exit here, we don't wanna to repeat the last res/geom/error block
        //            exit;
                //}
        //    }
        //}
        echo json_encode(array('res'=>'','geom'=>'','error'=>$r->error));
    }
    exit;
}
/* results show switch button */
if(isset($_POST['viewType'])) {

    $reverse = '';
    if (isset($_POST['order']))
        $reverse = $_POST['order'];

    $method = $_POST['viewType'];

    if(!isset($_SESSION['show_results'])) {
        $_SESSION['show_results'] = array();
        $_SESSION['show_results']['visible'] = 'on';
    } else {
        $_SESSION['show_results']['visible']='on';
    }
    if($method=='stable') {
        $_SESSION['show_results']['type'] = $_POST['viewType'];
        $_SESSION['show_results']['rowHeight'] = '30';
        $_SESSION['show_results']['cellWidth'] = '120';
        $_SESSION['show_results']['borderRight'] = '1px solid lightgray';
        $_SESSION['show_results']['borderBottom'] = '1px solid lightgray';
    } else if ($method == 'list') {
        $_SESSION['show_results']['type'] = $_POST['viewType'];
        $_SESSION['show_results']['rowHeight'] = '60';
        $_SESSION['show_results']['cellWidth'] = '400';
        $_SESSION['show_results']['borderRight'] = 'none';
        $_SESSION['show_results']['borderBottom'] = 'none';
    }


    if ($_SESSION['show_results']['visible']=='on') {
        include(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>$_POST['viewType'],'print'=>'on','reverse'=>$reverse]);
        if ( $r->results_query() ) {
            $r->printOut();
            exit;
        }
        echo json_encode(array('res'=>'','geom'=>'','error'=>$r->error));
    }
    exit;
}

/* results rolltable change order by click on viewport header */
if(isset($_POST['rolltable_order'])) {
    $_SESSION['orderby'] = $_POST['order'];
    exit;
}

/* results show switch button */
if(isset($_POST['set-morefilter'])) {

    if ($_POST['set-morefilter'] == 0) {
/*        "SELECT EXISTS (
           SELECT 1
           FROM   information_schema.tables
           WHERE  table_schema = 'temporary_tables'
           AND    table_name = 'temp_filter_%_%s')"
*/
        pg_query($ID,sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
        unset($_SESSION['morefilter_query']);
        unset($_SESSION['morefilter']);
        unset($_SESSION['filter_type']);

        //drop query tables as well
        //if ($_POST['clear-morefilter']==2) {
        //    pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
        //}
        exit;
    }

    $_SESSION['morefilter'] = 'on';


    $cmd = sprintf("CREATE UNLOGGED TABLE IF NOT EXISTS temporary_tables.temp_filter_%s_%s ( rowid INTEGER )",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    $cmd = sprintf("GRANT ALL ON temporary_tables.temp_filter_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
    pg_query($ID,$cmd);

    update_temp_table_index(PROJECTTABLE.'_'.session_id());

    $cmd = sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    /*$cmd = sprintf('SELECT EXISTS (
        SELECT 1
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  n.nspname = \'temporary_tables\'
        AND    c.relname = \'temp_query_%s_%s\'
        AND    c.relkind = \'r\')',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if(pg_affected_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t')
            $cmd = sprintf('INSERT INTO temporary_tables.temp_filter_%1$s_%2$s SELECT obm_id FROM temporary_tables.temp_query_%1$s_%2$s',PROJECTTABLE,session_id());
        else {
            $cmd = sprintf("INSERT INTO temporary_tables.temp_filter_%s_%s VALUES (%d);",PROJECTTABLE,session_id(),$i);
        }
    }*/

    /*$cmd = sprintf('INSERT INTO temporary_tables.temp_filter_%1$s_%2$s SELECT obm_id FROM temporary_tables.temp_%1$s_%2$s',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if(pg_affected_rows($res)) {
    //    echo pg_affected_rows($res);
        //the unlogged "temporary" table name
        $_SESSION['morefilter'] = sprintf('temp_filter_%s_%s',PROJECTTABLE,session_id());
        //he whole JOIN query
        $_SESSION['morefilter_query'] = sprintf("RIGHT JOIN temporary_tables.temp_filter_%s_%s on rowid=obm_id",PROJECTTABLE,session_id());
    }*/
    exit;
}

/* results show switch button */
if(isset($_POST['clear-morefilter'])) {

    $cmd = sprintf('SELECT EXISTS (
        SELECT 1
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  n.nspname = \'temporary_tables\'
        AND    c.relname = \'temp_filter_%s_%s\'
        AND    c.relkind = \'r\')',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t')
            pg_query($ID,sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
    }

    // clear join
    unset($_SESSION['morefilter_query']);
    // clear filter type
    unset($_SESSION['filter_type']);
    // turn-off button
    //$_SESSION['morefilter'] = 'clear';

    exit;
}
/* reset-map call
 * drop query_build_string
 * It is important to reset base layer maps
 * */
if(isset($_POST['clear-session_querybuildstring'])) {
    unset($_SESSION['query_build_string']);
    $cmd = sprintf('SELECT EXISTS (
        SELECT 1
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  n.nspname = \'temporary_tables\'
        AND    c.relname = \'temp_%s_%s\'
        AND    c.relkind = \'r\')',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t')
            pg_query($ID,sprintf("DELETE FROM temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));
    }
    unset($_COOKIE['lastZoom']);
    setcookie('lastZoom', null, -1, sprintf('/projects/%s/',PROJECTTABLE));

    exit;
}

// go back the previous page after login
if(isset($_POST['login'])) {
    unset($_SESSION['login_vars']);
    //excepting in registration
    if(!preg_match('/index.php\?register=[0-9a-z]{32}$/i',$_POST['login'])) {
        $_SESSION['login_vars'] = $_POST['login'];
    }

    exit;
}
/* Identify click on map
 *
 * */
if (isset($_POST['identify_point'])) {

    $point = quote("POINT(".$_POST['lon']." ".$_POST['lat'].")");
    $ce = $_POST['ce'];

    /* MODULE INCLUDES HERE */
    echo $modules->_include('identify_point','return_data',array($point,$ce));

    exit;
}
// get  results specieslist
if (isset($_POST['get_species_list'])) {

    /* MODULE INCLUDES HERE */
    echo $modules->_include('results_specieslist','print_speciestable');

    exit;
}

if (isset($_POST['get_cite_list'])) {

    /* MODULE INCLUDES HERE */
    echo $modules->_include('results_specieslist','print_citetable');

    exit;

}
/* Create postgres user
 *
 * */
if (isset($_POST['create_pg_user'])) {

    /* MODULE INCLUDES HERE */
    if ($modules->is_enabled('create_pg_user'))
        echo $modules->_include('create_pg_user','create_pg_user');
     else
        echo common_message('error','Module not enabled or access denied');

    exit;
}

/* track data view
 * */
if(isset($_POST['track_data_view'])) {
    global $BID;
    $data_id = preg_replace('/[^0-9]/','',$_POST['track_data_view']);
    if ($data_id!='')
        echo track_data($data_id);
    exit;
}
/*roller query subset of huge data
 * */
if (isset($_POST['Rprepare']) and isset($_POST['arrayType'])) {
    $cmd = '';

    if ($_POST['arrayType'] == 'list') {
        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_roll_list_%s_%s',PROJECTTABLE,session_id());
    } elseif ($_POST['arrayType'] == 'table') {
        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_roll_table_%s_%s',PROJECTTABLE,session_id());
    } elseif ($_POST['arrayType'] == 'stable') {
        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_roll_stable_%s_%s',PROJECTTABLE,session_id());
    } elseif ($_POST['arrayType'] == 'normaltable') {
        if (isset($_POST['Table']) and $_POST['Table']!='' and isset($_SESSION['private_key'])) {
            $cipher="AES-128-CBC";
            $iv = base64_decode($_SESSION['openssl_ivs']['asTable_table']);
            $table_reference = openssl_decrypt(base64_decode($_POST['Table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            if ($table_reference!='') {
                list($schema,$table) = preg_split('/\./',$table_reference);
            }
        }

        $cmd = sprintf('SELECT count(*) as c FROM %s.%s',$schema,$table);
    }
    if ($cmd != '') {
        $res = pg_query($ID,$cmd);
        if (pg_last_error($ID)) {
            log_action(pg_last_error($ID));
            echo "Is the module loaded?";
        } else {
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                echo $row['c'];
            } else {
                echo 0;
            }
        }
    }
    exit;
}
// rolltables
if (isset($_POST['RreadFrom']) and isset($_POST['arrayType'])) {
    global $ID;

    $from = preg_replace("/[^0-9]/","",$_POST['RreadFrom']);
    $to = 80;
    $b = array();

    if ($_POST['arrayType'] == 'list') {
        $cmd = sprintf('SELECT rlist FROM temporary_tables.temp_roll_list_%s_%s ORDER BY ord LIMIT %d OFFSET %d',PROJECTTABLE,session_id(),$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_assoc($res)) {
            $b[] = array($row['rlist']);
        }
    } elseif ($_POST['arrayType'] == 'table') {
        $cmd = sprintf('SELECT rlist FROM temporary_tables.temp_roll_table_%s_%s ORDER BY ord LIMIT %d OFFSET %d',PROJECTTABLE,session_id(),$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_assoc($res)) {
            $j = json_decode($row['rlist']);
            $foo = (array) $j;
            $bar = array();
            foreach($foo as $i) {
                $bar[] = $i;
            }
            $b[] = $bar;
        }
    } elseif ($_POST['arrayType'] == 'stable') {
        $cmd = sprintf('SELECT rlist FROM temporary_tables.temp_roll_stable_%s_%s ORDER BY ord LIMIT %d OFFSET %d',PROJECTTABLE,session_id(),$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_assoc($res)) {
            $j = json_decode($row['rlist']);
            $foo = (array) $j;
            $bar = array();
            foreach($foo as $i) {
                $bar[] = $i;
            }
            $b[] = $bar;
        }
    } elseif ($_POST['arrayType'] == 'normaltable') {
        if (isset($_POST['Table']) and $_POST['Table']!='' and isset($_SESSION['private_key'])) {
            $cipher="AES-128-CBC";
            $iv = base64_decode($_SESSION['openssl_ivs']['asTable_table']);
            $table_reference = openssl_decrypt(base64_decode($_POST['Table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            if ($table_reference!='') {
                list($schema,$table) = preg_split('/\./',$table_reference);
            }
            if (isset($_POST['Order'])) {
                $iv = base64_decode($_SESSION['openssl_ivs']['asTable_orderby']);
                $order = 'ORDER BY '.openssl_decrypt(base64_decode($_POST['Order']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            } else
                $order = '';
        }

        $cmd = sprintf('SELECT * FROM %s.%s %s LIMIT %d OFFSET %d',$schema,$table,$order,$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_row($res)) {
            $b[] = $row;
        }
    }

    echo json_encode($b);

    exit;
}
//get mime - set mime icon/ thumbnail
//It can't be called standalone
if (isset($_POST['get_mime']) and vreq()) {
    if (isset($_POST['size']))
        $size = preg_replace('/[^0-9]/','',$_POST['size']);
    if (isset($_POST['file']))
        $file = $_POST['file'];
    if (isset($size) and isset($file)) {
        if ($thumb = mkThumb($file,$size)){
            if (isset($_POST['thumb']) and $_POST['thumb']=='no')
                $url = "http://".URL."/getphoto?ref=$file";
            else
                $url = "http://".URL."/getphoto?ref=/thumbnails/$file";
        } else {
            $url = mime_icon($file,$size);
        }
        echo $url;
    }
    exit;
}
/* uploader.js
 * file upload drop column label over a column
 * set column's params
 * */
if (isset($_POST['column_assign']) ) {

    $_SESSION['upload_ajax_wait'] = 1;

    //$_POST['colname'] can be used to teach auto column assign:
    //store column name (of uploaded data) and column_assign (database column) together and use them to fine tuning
    //auto column assign in upload_funcs-table-form.php

    if (isset($_SESSION['upload_column_assign']) ) {
        while(($key = array_search($_POST['column_assign'],$_SESSION['upload_column_assign'] )) !== false) {
            unset($_SESSION['upload_column_assign'][$key]);
        }
    }
    $_SESSION['upload_column_assign'][$_POST['coli']] = $_POST['column_assign'];

    $_SESSION['upload_ajax_wait'] = 0;

    exit;
}
/* uploader.js
 *
 * */
if (isset($_POST['column_remove']) and isset($_SESSION['upload_column_assign'])) {
    while(($key = array_search($_POST['column_remove'],$_SESSION['upload_column_assign'] )) !== false) {
        unset($_SESSION['upload_column_assign'][$key]);
    }
    //unset($_SESSION['upload_column_assign']);
    //$_SESSION['upload_column_assign'] = array_values($_SESSION['upload_column_assign']);
    exit;
}
/* upload table, change active sheet of uploaded excel files
 *
 * */
if (isset($_POST['change_exs_sheet'])) {
    $_SESSION['upload_file']['activeSheet'] = $_POST['change_exs_sheet'];
    exit;
}
/* dynamic drop-down select list in file upload default elements
 *
 * */
if (isset($_POST['dynamic_upload_list'])) {
    //default-observation_con_id
    $condition = $_POST['condition'];
    //_list string
    $cmd = sprintf("SELECT control,custom_function,list_definition FROM project_forms_data WHERE form_id=%d AND \"column\"=%s",$_POST['sfid'],quote($_POST['dynamic_upload_list']));
    $res = pg_query($BID,$cmd);
    if ($row = pg_fetch_assoc($res)) {
        $sl = process_trigger($row);
        if ($sl) {
            $out = array();
            foreach ($sl['target_column'] as $target_column) {
                $rc = $target_column;
                $cmd = sprintf("SELECT type, list, control, custom_function, default_value,list_definition FROM project_forms_data WHERE form_id=%d AND \"column\"=%s",$_POST['sfid'],quote($target_column));
                $res2 = pg_query($BID,$cmd);
                if ($row2 = pg_fetch_assoc($res2)) {

                    if ($row2['type'] == 'list') {
                        $sel = new upload_select_list($row2);
                        $opt = $sel->get_options('html');
                        $out[] = ['col'=>$rc,'opt'=>implode($opt)];
                    }
                    else {
                        $trg = process_trigger($row2);
                        if ($trg) {
                            $cmd = '';

                            $prefilter = ($trg['preFilterColumn']) ? prepareFilter($trg['preFilterColumn'],$trg['preFilterValue']) : '1 = 1';
                            if ($row2['type'] == 'wkt' || $row2['type'] == 'point' || $row2['type'] == 'line' || $row2['type'] == 'polygon') {
                                $cmd = sprintf('SELECT ST_AsText(%s) as val FROM %s.%s WHERE %s = %s AND %s',$trg['value_col'],$trg['schema'],$trg['table'],$trg['foreign_key'],quote($condition),$prefilter);
                            }
                            elseif ($row2['type'] == 'text') {
                                $cmd = sprintf('SELECT %s as val FROM %s.%s WHERE %s = %s AND %s',$trg['value_col'],$trg['schema'],$trg['table'],$trg['foreign_key'],quote($condition), $prefilter);
                            }

                            if ($cmd != '') {
                                $res = pg_query($ID,$cmd);
                                $val = ($row = pg_fetch_assoc($res)) ? $row['val'] : '';
                                $out[] = ['col' => $rc, 'opt' => $val];
                            }
                        }
                        else
                            log_action("get_value() not formed properly!",__FILE__,__LINE__);
                    }
                }
            }
            echo json_encode($out);
        }
    }

    exit;
}
/* kill R-shiny server */
if (isset($_POST['r_server'])) {
    $rport = 'RSERVER_PORT_'.PROJECTTABLE;
    $rport = constant($rport);
    if ($_POST['r_server'] == 'stop') {
        // test, shiny server is running
        if (stest('localhost',$rport)) {
            $pid = file_get_contents(getenv("PROJECT_DIR")."r-server.pid");
            exec("kill -9 $pid");
        }
    } elseif ($_POST['r_server']=='start') {
        if (!stest('localhost',$rport)) {

            if (defined('RSERVER') and constant("RSERVER")) {
                $rport = 'RSERVER_PORT_'.PROJECTTABLE;
                $rport = constant($rport);
                $pid = exec("Rscript --vanilla --no-save ".getenv('PROJECT_DIR')."shiny/run_server.R --port $rport --project ".PROJECTTABLE." --path ".getenv('PROJECT_DIR')."shiny/ > ".getenv('PROJECT_DIR')."shiny/r-server.log & echo $! &");
                //log_action("Rscript --vanilla --no-save ".getenv('PROJECT_DIR')."shiny/run_server.R --port $rport --project ".PROJECTTABLE." --path ".getenv('PROJECT_DIR')."shiny/ > ".getenv('PROJECT_DIR')."shiny/r-server.log & echo $! &");
                if ($pid) {
                    log_action("R-server PID: $pid",__FILE__,__LINE__);
                    file_put_contents(getenv("PROJECT_DIR")."r-server.pid", $pid);
                }
            }
        }
    }

    echo 1;
    exit;
}
// profile email visibility
if (isset($_POST['email-visible'])) {
    if (!isset($_SESSION['Tid'])) exit;
    $cmd = sprintf("UPDATE \"project_users\" SET visible_mail=%d WHERE user_id=%d AND project_table=%s",$_POST['email-visible'],$_SESSION['Tid'],quote(PROJECTTABLE));
    pg_query($BID,$cmd);
    exit;
}
// profile email visibility
if (isset($_POST['email-receive'])) {
    if (!isset($_SESSION['Tid'])) exit;
    $cmd = sprintf("UPDATE \"project_users\" SET receive_mails=%d WHERE user_id=%d AND project_table=%s",$_POST['email-receive'],$_SESSION['Tid'],quote(PROJECTTABLE));
    pg_query($BID,$cmd);
    exit;
}
/* Create new database table
 * */
if (isset($_POST['create_new_table'])) {

    if (!has_access('create_table')) exit;

    if (!preg_match('/^[a-z0-9_]+$/',$_POST['create_new_table'])) {
        echo common_message('error','Only a-z character 0-9 numbers and _ allowed!');
        exit;
    }
    if (strlen($_POST['create_new_table'])>24) {
        echo common_message('error','Max 24 characters allowed!');
        exit;
    }
    if ($_POST['create_new_table']=='') {
        echo common_message('error','Give a name!');
        exit;
    }
    $table = preg_replace("/^".PROJECTTABLE."_/","",$_POST['create_new_table']);

    $cmd = sprintf('SELECT 1 FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote(PROJECTTABLE."_".$table));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        echo common_message('error',"$table table already exists!");
        exit;
    }

    $cmd = sprintf('INSERT INTO header_names (f_table_schema,f_table_name,f_date_columns,f_cite_person_columns,f_alter_speciesname_columns,f_file_id_columns,f_main_table)
        VALUES(\'%s\',\'%s\',NULL,NULL,NULL,NULL,%s)','public',PROJECTTABLE,quote(PROJECTTABLE.'_'.$table));
    $res = pg_query($BID,$cmd);
    if (!pg_affected_rows($res)) {
        pg_query($BID,'ROLLBACK');
        $errorID = uniqid();
        echo common_message('fail',"$table adding to project failed",$errorID);
        log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
        exit;
    }

    $cmd = "SELECT tgname from pg_trigger WHERE NOT tgisinternal AND tgrelid = '".PROJECTTABLE."'::regclass AND tgname='file_connection'";
    $res = pg_query($GID,$cmd);
    if (!pg_num_rows($res))
        $trigger_row = sprintf('CREATE TRIGGER file_connection AFTER INSERT ON %1$s FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();',PROJECTTABLE);
    else
        $trigger_row = '';

    $cmd = sprintf('CREATE TABLE public.%1$s_%2$s (
        obm_id integer NOT NULL,
        obm_geometry geometry,
        obm_uploading_id integer,
        obm_validation numeric,
        obm_comments text[],
        obm_modifier_id integer,
        CONSTRAINT enforce_dims_obm_geometry CHECK ((st_ndims(obm_geometry) = 2)),
        CONSTRAINT enforce_srid_obm_geometry CHECK ((st_srid(obm_geometry) = 4326))
    );

    COMMENT ON TABLE %1$s_%2$s IS %3$s;

    CREATE SEQUENCE %1$s_%2$s_obm_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;

    ALTER TABLE %1$s_%2$s_obm_id_seq OWNER TO %1$s_admin;

    ALTER TABLE ONLY %1$s_%2$s ADD CONSTRAINT %1$s_%2$s_pkey PRIMARY KEY (obm_id);

    ALTER TABLE ONLY %1$s_%2$s ALTER COLUMN obm_id SET DEFAULT nextval(\'%1$s_%2$s_obm_id_seq\'::regclass);

    ALTER TABLE ONLY %1$s_%2$s ADD CONSTRAINT uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES uploadings(id);

    ALTER TABLE %1$s_%2$s OWNER TO %1$s_admin;%4$s',PROJECTTABLE,$table,quote($_POST['new_table_comment']),$trigger_row);


    if (!pg_query($GID,$cmd)) {
        pg_query($BID,'ROLLBACK');
        $errorID = uniqid();
        echo common_message('fail',"Failed to create ".PROJECTTABLE."_$table",$errorID);
        log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);

        exit;
    } else {
        pg_query($BID,'COMMIT');
        echo common_message('ok',PROJECTTABLE."_$table created");
    }
    exit;
}
/* agree new terms
 *
 * */
if (isset($_POST['agree_new_terms'])) {
    global $BID;
    if (!isset($_SESSION['Tid']) or !isset($_SESSION['Tcrypt']))
        exit;

    $cmd = sprintf("UPDATE users SET terms_agree=1 WHERE \"user\"='%s' AND id=%d",$_SESSION['Tcrypt'],$_SESSION['Tid']);
    $res = pg_query($BID,$cmd);
    if (pg_affected_rows($res))
        $_SESSION['Tterms_agree'] = 1;
    exit;
}
/*
 *
 * */
if (isset($_POST['get-terms-text'])) {
    ob_start();
    $fileName = '';
    switch ($_POST['get-terms-text']) {
    case 'privacy':
        $fileName = 'at';
        break;

    case 'terms':
        $fileName = 'aszf';
        break;
    case 'cookies':
        $fileName = 'cookies';
        break;
    }

    $lang = preg_replace("/[^A-Za-z]/", "", $_POST['lang']);
    if (file_exists(getenv('PROJECT_DIR')."{$fileName}_{$lang}.html"))
        include(getenv('PROJECT_DIR')."{$fileName}_{$lang}.html");
    elseif (file_exists(getenv('PROJECT_DIR')."{$fileName}.html"))
        include(getenv('PROJECT_DIR')."{$fileName}.html");
    elseif (file_exists(OB_ROOT."{$fileName}_{$lang}.html"))
        include(OB_ROOT."{$fileName}_{$lang}.html");
    else
        include(OB_ROOT."{$fileName}.html");

    $out = ob_get_clean();
    echo $out;
}
/*
 *
 * */
if (isset($_POST['process']) && $_POST['process'] = 'getNestedOptions') {

    $cmd = sprintf("SELECT %s as val, %s as name FROM %s WHERE %s = %s ORDER by name;", $_POST['v'], $_POST['n'], $_POST['table'], $_POST['fk'], $_POST['fkv']);
    $res = pg_query($ID,$cmd);
    $results = [];
    while ($row = pg_fetch_assoc($res)) {
        $results[] = $row;
    }
    header("Content-type: application/json");
    print(json_encode($results));
}
/*
 *
 * */
//has_access form-editor??
if (isset($_POST['list-validator']) and isset($_SESSION['Tid'])) {

    $return = false;
    if (preg_match('/^{/',$_POST['list-validator'])) {

        // JSON format given
        echo json_schema_validator($_POST['list-validator'],'upload_list.schema.json');
    } else {

        // simplified format given
        /* create JSON from elements.... ?
         *
         * */
        echo json_schema_validator("{".$_POST['list-validator']."}",'upload_list.schema.json');
    }
    exit;
}

if (isset($_POST['list-content-parser']) and isset($_SESSION['Tid'])) {
    $value = isset($_POST['value']) ? $_POST['value'] : '';
    $schema = isset($_POST['schema']) ? $_POST['schema'] : 'public';

    if ( $_POST['list-content-parser'] == 'optionsTable' )
        $response = getDataTables($value,$schema);
    elseif ( $_POST['list-content-parser'] == 'optionsSchema' )
        $response = getProjectSchemas();
    elseif ( in_array($_POST['list-content-parser'],array('valueColumn','labelColumn','filterColumn','preFilterColumn')) )
        $response = getColumnsOfTables($value,$schema);
    elseif ( $_POST['list-content-parser'] == 'triggerTargetColumn' )
        $response = getColumnsOfTables(PROJECTTABLE,$schema);
    elseif ( in_array($_POST['list-content-parser'],array('add_empty_line','multiselect')) )
        $response = array('true','false');
    elseif ( $_POST['list-content-parser'] == 'Function' )
        $response = array('select_list','get_value');
    elseif ( in_array($_POST['list-content-parser'],array('disabled','selected')) ) {
        if (is_array($value)) {
            $response = array_keys($value);
        } else
            $response = array($value);
    }
    elseif ( $_POST['list-content-parser'] == 'pictures' ) {
        if (is_array($value)) {
            $keys = array_keys($value);
            $response = array_map(function($x){return json_encode(array($x=>'url-string'));},$keys);
        } else
            $response = array($value);

    }

    if (!is_array($response))
        $response = array("string");
    elseif (!count($response))
        $response = array("string");

    echo json_encode($response);
    exit;
}
/* Update local translations
 *
 * */
if (isset($_POST['update_translations']) and isset($_SESSION['Tid'])) {
    //$cmd = sprintf("SELECT id, const, translation FROM translations WHERE scope = 'local' AND project = '%s' AND lang = '%s' ORDER BY const;", PROJECTTABLE, $_SESSION['LANG']);
    $cmd = sprintf('SELECT t2.id,t2.lang,t1.const,t1.translation AS translation_en,t2.translation
                    FROM translations t1  LEFT JOIN translations t2 ON (t1.const=t2.const AND t2.lang=%1$s)
                    WHERE t1.scope = \'local\' AND t1.lang IN (\'en\') AND t1.project=\'%2$s\' ORDER BY t1.const',quote($_SESSION['LANG']),PROJECTTABLE);

    $LANGS = explode(',',LANGUAGES);
    if (pg_send_query($BID, $cmd)) {
        $res = pg_get_result($BID);
        $state = pg_result_error($res);
        if ($state != '') {
            $errorID = uniqid();
            echo common_message('fail',$state,$errorID);
            log_action("ErrorID#$errorID# ".$state,__FILE__,__LINE__);
            return;
        }

        $translations = [];
        while ($row = pg_fetch_assoc($res)) {
            $translations[] = $row;
        }

        $cmd = '';
        $cmdCount = 0;
        $delete = -1;
        foreach ($translations as $t) {
            if ($t["lang"]!='') {
                if (isset($_POST['const-'.$t['id']]) and $_POST['const-'.$t['id']] !== $t['const']) {
                    if ($_POST['const-'.$t['id']] == '') {
                        $delete = $t['id'];
                        $cmd .= sprintf("DELETE FROM translations WHERE id = %d;", $t['id']);
                        $cmdCount++;
                    }
                    else {
                       $cmd .= sprintf("UPDATE translations SET const = %s WHERE id = %s;", quote($_POST['const-'.$t['id']]), $t['id']);
                       $cmdCount++;
                    }
                }

                if (isset($_POST['translation-'.$t['id']]) and $_POST['translation-'.$t['id']] !== $t['translation'] and $delete !== $t['id']) {
                    $cmd .= sprintf("UPDATE translations SET translation = %s WHERE id = %d;", quote($_POST['translation-'.$t['id']]), $t['id']);
                    $cmdCount++;
                }
            }
            elseif ($t["lang"]=='') {
                if (isset($_POST['const-'.$t['id']])) {
                    $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,%s);", PROJECTTABLE,$_SESSION['LANG'],quote($_POST['const-'.$t['id']]),quote($_POST['translation-'.$t['id']]));
                    $cmdCount++;
                } else {
                    $val = '';
                    $idx = array_search($t['const'],$_POST);
                    //const-12new
                    if ($idx!==false) {
                        if (preg_match('/^const(-\d+new)/',$idx,$m)) {
                            $tra = "translation".$m[1];
                            $val = $_POST[$tra];
                            $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,%s);", PROJECTTABLE,$_SESSION['LANG'],quote($_POST[$idx]),quote($val));
                            $cmdCount++;
                        }
                    }
                }
            }
        }

        foreach ($_POST as $key => $value) {
            if (preg_match('/new-const-(\d+)/',$key,$m) && $value != '') {
                if (preg_match('/str_(\w+)/',$value)) {
                    $i = $m[1];
                    if (isset($_POST['new-translation-'.$i]) && $_POST['new-translation-'.$i] !== '') {
                        $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,%s);", PROJECTTABLE,$_SESSION['LANG'],quote($value),quote($_POST['new-translation-'.$i]));
                        $cmdCount++;
                        foreach ($LANGS as $lang) {
                            if ($lang != $_SESSION['LANG'])
                                $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,'%s');", PROJECTTABLE,$lang,quote($value),'__not_translated__');
                                $cmdCount++;
                        }
                    }
                    else {
                        echo common_message('error','translation empty');
                        return;
                    }
                }
                else {
                    echo common_message('error','constant not formed properly!');
                    return;
                }

            }
        }

        if (pg_send_query($BID, $cmd)) {
            for ($i = 0; $i < $cmdCount; $i++) {
                $res = pg_get_result($BID);
                $state = pg_result_error($res);
                if ($state != '') {
                    $errorID = uniqid();
                    echo common_message('failed',$state,$errorID);
                    log_action("ErrorID#$errorID# ".$state,__FILE__,__LINE__);
                    log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                    return;
                }
            }
            echo common_message('ok','done');
        }
    }
    exit;
}
/*
 *
 * */
if (isset($_POST['choose_display_grid_for_map'])) {
    global $ID;

    $cmd = sprintf("SELECT c.column_name, pgd.description FROM pg_catalog.pg_statio_all_tables as st inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
            INNER JOIN information_schema.columns c on
                (pgd.objsubid=c.ordinal_position and c.table_schema=st.schemaname and c.table_name=st.relname and c.table_name = '%s_qgrids' and c.table_schema = 'public')",$_SESSION['current_query_table']);

    $res = pg_query($ID,$cmd);
    while ($row = pg_fetch_assoc($res)) {
        if ($_POST['choose_display_grid_for_map'] == $row['column_name']) {
            $_SESSION['display_grid_for_map'] = $row['column_name'];
            echo common_message('ok','ok');
            exit;
        }
    }
    echo common_message('error','invalid grid');
    exit;
}
/* Called in maps_functions.js
 *
 * Not used???
 * */
if (isset($_POST['check_layer'])) {
    global $BID;

    $layers_state = array();

    if ($modules->is_enabled('grid_view')) {

        $grid_layer = $modules->_include('grid_view','get_grid_layer');
        // get layer name based on js layer name

        for ($i=0; $i<count($_POST['check_layer']);$i++) {

            $cl = $_POST['check_layer'][$i];
            $layers_state[$i] = 'ok';

            $layer = '';
            $cmd = sprintf("SELECT ms_layer FROM project_layers WHERE concat_ws('_',layer_name,layer_order)=%s",quote($cl));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $layer = $row['ms_layer'];
            }

            if (isset($_SESSION['display_grid_for_map'])) {

                // For grid coordinates disable original layer
                foreach ($grid_layer as $gc=>$gl) {
                    if ($_SESSION['display_grid_for_map'] == $gc and $layer!='' and $layer!=$gl) {
                        $layers_state[$i] = 'drop';
                    }
                }
            } else {
                $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                foreach ($grid_layer as $gc=>$gl) {
                    if ($grid_geom_col == $gc and $layer!='' and $layer!=$gl) {
                        $layers_state[$i] = 'drop';
                    }
                }
            }
        }
    }
    echo common_message('ok',json_encode($layers_state));
    exit;
}


/* include the ajax method of the modules
 * */
if (isset($_REQUEST['m'])) {
    switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $request = &$_GET;
        break;
    case 'POST':
        $request = &$_POST;
        break;
    default:
    }

    $m = preg_replace("/[^A-Za-z0-9-_]/", "", $request['m']);
    $modules->_include($m,'ajax',array($request));

    exit;
}
// creates a new gitlab issue
if (isset($_SESSION['Tid']) and isset($_POST['new_gitlab_issue'])) {

    //not a solution yet
    //curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" https://gitlab.example.com/api/v4/projects/4/issues?title=Issues%20with%20auth&labels=bug

    if (defined("AUTO_BUGREPORT_ADDRESS")) {
        if (defined("OB_PROJECT_DOMAIN")) {
            $domain = constant("OB_PROJECT_DOMAIN");
            $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
        } else {
            $domain = $_SERVER["SERVER_NAME"];
            $server_email = PROJECTTABLE."@".$domain;
        }

        /*$cipher="AES-128-CBC";
        if (isset($_POST['eincf']) and $_POST['eincf']!='' and isset($_SESSION['private_key'])) {
            $iv = base64_decode($_SESSION['openssl_ivs']['eincf']);
            $included_files = openssl_decrypt(base64_decode($_POST['eincf']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        }
        if (isset($_POST['edefv']) and $_POST['edefv']!='' and isset($_SESSION['private_key'])) {
            $iv = base64_decode($_SESSION['openssl_ivs']['edefv']);
            $defined_vars = openssl_decrypt(base64_decode($_POST['edefv']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        }*/

        $message = "Page:<br>".$_POST['page']."<br>".$_POST['message'];
        //$message .= "<br>Included files:<br>".$included_files."<br>defined variables:<br>".$defined_vars;

           //mail_to($To,                   $Subject,                              $From,        $ReplyTo,          $Title,         $Message,$Method,    $ReplyToName,$FromName
        $m = mail_to(AUTO_BUGREPORT_ADDRESS,"[".PROJECTTABLE."] {$_POST['title']}",$server_email,$_SESSION['Tmail'],$_POST['title'],$message,'multipart',$_SESSION['Tname']);
        echo common_message('ok','ok');
    } else {
        echo common_message('error','auto bugreport address not defined');
    }
    exit;
}
// switch profile
if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['switchprofile'])) {
    $_SESSION['switchprofile'] = $_POST['switchprofile'];
    echo login_box(FALSE);
    exit;
}

// send sql command from sql console
if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['send-sql-cmd'])) {

    if (!isset($_SESSION['reauth-for-sqlconsole'])) {
        echo login_box(FALSE);
        exit;
    } else {
        if ($_SESSION['reauth-for-sqlconsole'] < time()-1800) {
            echo login_box(FALSE);
            exit;
        }
    }

    #pg_query($ID,'BEGIN');
    $result = pg_query($ID,$_POST['send-sql-cmd']);
    #pg_query($ID,'ROLLBACK');
    $status = pg_result_status($result);
    if ($status!=1) {

        if (!pg_last_error($ID)) {
            echo "<pre style='margin:0px;padding:2px'>";
            while ($row = pg_fetch_row($result)) {
                echo json_encode($row,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_PRESERVE_ZERO_FRACTION);
            }
            echo "</pre>";
        } else
            echo pg_last_error($ID);
        exit;
    } else {
        echo "OK";
    }
    exit;
}
// authentication testing
if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['reauth'])) {
    if (LogIn($_POST['reauth'][0]['value'],$_POST['reauth'][1]['value'])) {
        $_SESSION['reauth-for-sqlconsole'] = time();
    }

    if (isset($_SESSION['switchprofile'])) {
        unset($_SESSION['switchprofile']);
        echo 'reload_profile';
    }
    exit;
}
// syntax highlight an sql command
if (isset($_POST['sql-cmd-highlight'])) {
    require('vendor/autoload.php');

    $h = SqlFormatter::format($_POST['sql-cmd-highlight']);
    $f = SqlFormatter::format($_POST['sql-cmd-highlight'],false);
    echo json_encode(array($h,$f));
    exit;
}

if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['privileges']) && $_POST['privileges'] === 'save') {
    global $BID;

    $cmd1 = sprintf("DELETE FROM group_privileges WHERE project = '%s';", PROJECTTABLE);
    if (!$res = pg_query($BID,$cmd1)) {
        $errorID = uniqid();
        echo common_message('failed',str_sql_error,$errorID);
        log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
        exit;
    }
    $sql = [];
    foreach ($_POST as $name => $value) {
        if (preg_match('/^(\w+)@(\w+)$/',$name,$pairs))
            $sql[] = sprintf("('%s',%s,%s)",PROJECTTABLE,quote($pairs[1]),quote($pairs[2]));
    }
    if (count($sql)) {
        $cmd2 = sprintf('INSERT INTO group_privileges (project, role_id, action) VALUES %s;', implode(',',$sql));
        if (!$res = pg_query($BID,$cmd2)) {
            $errorID = uniqid();
            echo common_message('failed',str_sql_error,$errorID);
            log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
            exit;
        }

    }
    echo common_message('ok','OK!');
    exit;
}

if (isset($_POST['edit-record']) and isset($_POST['table'])) {
    $nullform_id = getNullFormId($_POST['table'],true);

    echo $nullform_id;
    exit;
}

if (isset($_GET['exportimport'])) {

    if ($_GET['exportimport'] == '') {
        // amikor nincs ref, akkor új feltöltés felől jövünk és a legutolsó mentést exportáljuk
        $cmd = sprintf("SELECT file,ref FROM system.imports WHERE file='upload_%s_%s' AND datum > (NOW() - '1 day'::interval) ORDER BY datum DESC LIMIT 1",PROJECTTABLE,session_id());
    } else {
        $REF = quote(preg_replace("/[^0-9a-zA-Z]/",'',$_GET['exportimport']));
        $cmd = "SELECT file,ref FROM system.imports WHERE ref=$REF";
    }
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $filename = $row['file'].'_'.$row['ref'].'.csv';

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=\"$filename\";");
        header("Expires: -1");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);

        $cmd = sprintf('SELECT data FROM temporary_tables."%1$s_%2$s"',$row['file'],$row['ref']);
        $res = pg_query($ID,$cmd);

        $fp = fopen('php://output', 'w');
        while ($row = pg_fetch_assoc($res)) {
            fputcsv($fp, json_decode($row['data'],true));
        }
        fclose($fp);

    }

    exit;
}

?>
