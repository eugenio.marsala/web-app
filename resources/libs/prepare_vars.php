<?php
/* ***************************************************************************  /

This file contains the variable transformations and function calls for
REQUEST POST GET variables and some general variables definitions
 
****************************************************************************** */
if (session_status() == PHP_SESSION_NONE) {
    //Ajaxos kérés esetén futunk ide
    //$lifetime = 3600;
    session_start();
    //session_regenerate_id(TRUE);
    //setcookie(session_name(),session_id(),time()+$lifetime,"/");
}

# call oauth layer 
require('prepare_auth.php');

# prepare global session variables
require('prepare_session.php');



/* Processing GET variables  ******************************************************** */

// Project table variables
//$qtable=PROJECTTABLE;
//$qtable_history=$qtable."_history";
if (isset($_GET['qtable'])) {

    if (!preg_match('/_/',$_GET['qtable'])) {
        //query table can't be in other schema,other prefix!!!
        $cmd = sprintf('SELECT 1 FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote(PROJECTTABLE."_".$_GET['qtable']));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $_SESSION['current_query_table'] = PROJECTTABLE."_".$_GET['qtable'];
        
        } else {
            if (defined('DEFAULT_TABLE'))
                $_SESSION['current_query_table'] = DEFAULT_TABLE;
            else
                $_SESSION['current_query_table'] = PROJECTTABLE;
        }
    } else {
        $cmd = sprintf('SELECT 1 FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote($_GET['qtable']));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $_SESSION['current_query_table'] = $_GET['qtable'];
        
        } else {
            if (defined('DEFAULT_TABLE'))
                $_SESSION['current_query_table'] = DEFAULT_TABLE;
            else
                $_SESSION['current_query_table'] = PROJECTTABLE;
        }
        unset($_GET['qtable']);
    }
}

/* GET and POST variables processing  **************************************** */
// language variables
if (isset($_GET['lang'])) {
    //if ( !isset($_SESSION['LANG']) or (isset($_SESSION['LANG']) and $_SESSION['LANG'] != $_GET['lang']) ) {
    //    $_SESSION['LANG_SET_TIME'] = time();
    //}

    // force update dbcolist
    $_SESSION['update_lang'] = 1;

    $_SESSION['LANG'] = preg_replace('/[^a-z]/i','',substr($_GET['lang'],0,2));
    unset($_GET['lang']);
    setcookie("obm_lang", $_SESSION['LANG'], time()+2592000,"/");
    if (isset($_SESSION['current_page'])) {
        unset($_SESSION['current_page']['rev']);
        $_GET = $_SESSION['current_page'];
    }
}
$_SESSION['current_page'] = $_GET;

# default load pages
$load = array('load_database'=>0,'load_login'=>0,'load_mappage'=>0,'load_map'=>0,'load_useagreement'=>0,'load_savedqueries'=>0,'load_datahistory'=>0,'load_uploadhistory'=>0,'load_profile'=>0,'load_register'=>0,'load_validhistory'=>0,'load_specieslist'=>0,'load_metaname'=>0,'load_data'=>0,'load_message'=>'','load_showuploads'=>0,'load_doimetadata'=>0,'load_getqueryapi'=>0,'load_showapikeys'=>0,'load_lostpw'=>0,'load_mainpage'=>0,'load_orcid_profile_data'=>0,'load_intropage'=>0,'load_getquery'=>0,'load_getreport'=>0,'load_loadmeta'=>0,'load_loaddata'=>0,'load_loaddoi'=>0,'load_userspecieslist'=>0,'load_showiups'=>0,'load_showowngeoms'=>0,'load_showsharedgeoms'=>0,'load_showserverlogs'=>0);

# Regisztrációs doboz
if (isset($_GET['registration']) and $_GET['registration']!='') {
    # unset the current login variables
    unset($_SESSION['Tid']);
    unset($_SESSION['Tcrypt']);
    unset($_SESSION['Tname']);
    unset($_SESSION['Tmail']);
    unset($_SESSION['Tgroups']);
    unset($_SESSION['Tproject']);
    //$registration_code=preg_replace("/[^a-zA-Z0-9]/","",$_GET['registration']);
    $_POST['invcode'] = $_GET['registration'];
    $_POST['inventer'] = 1;
} else if(isset($_GET['registration']) and $_GET['registration']=='') {
    # unset the current login variables
    unset($_SESSION['Tid']);
    unset($_SESSION['Tcrypt']);
    unset($_SESSION['Tname']);
    unset($_SESSION['Tmail']);
    unset($_SESSION['Tgroups']);
    unset($_SESSION['Tproject']);
    $load['load_register'] = 1;
}
#print_r($_SESSION);
# Invited user's registration/first-login process
# 
if(isset($_POST['inventer'])) {

    $ret = inventer($_POST['invcode']);
    if ($ret === true) {
        if (isset($_SESSION['register_um'])) {
            $ret = LogIn($_SESSION['register_um'],$_SESSION['register_upw']);
            if ($ret === true) {
                //helper variable on first login after accepting invitation
                unset($_SESSION['register_um']);
                $load['load_profile'] = 1;
                //create news stream
                //TRANSLATION to project default lang!!
                insertNews("{$_SESSION['Tname']} joined to OpenBioMaps on ".PROJECTTABLE." project.",'project');

                $m = new modules(false);
                $_SESSION['modules'] = $m->set_modules();
            } else {
                $load['load_message'] = str_login_error;
                $load['load_login'] = 1;
            }
        } else {
            $load['load_message'] = str_invitation_success_but_use_your_existing_account_to_login;
        }
    } else {
        $load['load_message'] = $ret;
    }
}

/*Outh authentication */
if (isset($_POST['loginenter'])) {

    $ret = check_expired_password($_POST['loginname'],$_POST['loginpass'] );
    if ($ret === true) {
        $ret = LogIn($_POST['loginname'],$_POST['loginpass']);
    
        if ($ret === FALSE) { /* Handle error */ 
            $load['load_message'] = str_login_error;
            $lang = $_SESSION['LANG'];
            session_destroy();
            session_unset();
            session_cache_expire(0);
            session_set_cookie_params(0,"/");
            session_cache_limiter('nocache');
            ini_set('session.cookie_lifetime',0);
            ini_set('session.gc_maxlifetime',0);
            session_start();
            $_SESSION['LANG'] = $lang;
            $load['load_login'] = 1;
        }
        else {
            $load['load_profile'] = 1;
        }
    } else {
        // expired password
        $load['load_message'] = $ret;
        $lang = $_SESSION['LANG'];
        session_destroy();
        session_unset();
        session_cache_expire(0);
        session_set_cookie_params(0,"/");
        session_cache_limiter('nocache');
        ini_set('session.cookie_lifetime',0);
        ini_set('session.gc_maxlifetime',0);
        session_start();
        $_SESSION['LANG'] = $lang;
        $load['load_login'] = 1;
    }
    unset($_GET['logout']);

    $m = new modules(false);
    $_SESSION['modules'] = $m->set_modules(); 
}
/* ?logout
 *
 * */
if (isset($_GET['logout'])) {
    $savepages = array();
    //save some special pages
    if (isset($_GET['loaddoi']))
        $savepages['doi'] = $_GET['loaddoi'];

    if (isset($_GET['metadata']))
        $savepages['metadata'] = $_GET['metadata'];
    
    include(getenv('OB_LIB_DIR').'logout.php');
    
    if (isset($savepages['doi']))
        $_GET['loaddoi'] = $savepages['doi'];

    if (isset($savepages['metadata']))
        $_GET['metadata'] = $savepages['metadata'];
}


# Lost password
if (isset($_POST['lostpasswd'])) {
    if (LostPw($_POST['loginmail'])) {
        $notice = str_message_sent;
    } else {
        $notice = str_no_such_mail_address;
    }
    if ($notice != '') {
        $load['load_message'] = $notice;
    }
}
/* Reactivate password
 * */
if (isset($_GET['activate']) and isset($_GET['addr'])) {

    unset($_SESSION['login_vars']);
    // ezt le kellene vizsgálni, hogy csak xx percenként lehessen egyet beírni...
    if (strlen($_GET['activate'])!=16) $get_act='';
    else $get_act = $_GET['activate'];
    $get_addr = $_GET['addr'];
    
    $ret = reactivate($get_addr,$get_act);
    if ($ret===true) {
        $ret = LogIn($_SESSION['register_uuu'],$_SESSION['register_upw']);
        if ($ret===true) {
            unset($_SESSION['register_uuu']);
            $load['load_profile'] = 1;
        } else {
            $load['load_message'] = $ret;
            $load['load_login'] = 1;
        }
    } else {
        $load['load_message'] = $ret;
        $load['load_login'] = 1;

    }
    unset($_GET['activate']);
    
    $m = new modules(false);
    $_SESSION['modules'] = $m->set_modules();
}

/* Server interconnect request handling 
 * API call, exit at end of call
 * */
if (isset($_GET['interconnect_request']) and isset($_GET['interconnect_server'])) {

    $ret = interconnect_request($_GET['interconnect_request'],$_GET['interconnect_server']);

    if ($ret===true) 
        echo common_message('OK','Request received');
    else
        echo common_message('failed',$ret);
    exit;
}
/* Local admin decision to accept or reject remote request
 * Link click call, load page with result
 */
if (isset($_GET['interconnect_request_key']) and isset($_GET['interconnect_accept_key']) and isset($_GET['decision']) and isset($_GET['interconnect_project'])) {

    $ret = interconnect_key_accept($_GET['decision'],$_GET['interconnect_accept_key'],$_GET['interconnect_request_key'],$_GET['interconnect_project']);
    if ($ret!==false) 
        $load['load_message'] = 'Interconnect key processed.';
    else
        $load['load_message'] = 'Interconnect key processing failed.';
}
/* Remote admin decision to accept or reject local request
 * API call, exit at end of call
 */
if (isset($_GET['interconnect_request_decision']) and isset($_GET['accept_key']) and isset($_GET['request_key'])) {
    $ret = interconnect_got_decision($_GET['interconnect_request_decision'],$_GET['accept_key'],$_GET['request_key']);
    if ($ret!==false) 
        echo common_message('OK',$ret);
    else
        echo common_message('failed','Decision processing failed');

    exit;
}
# general login variables 
# it should be after the logout vars! 
if (isset($_SESSION['Tid'])) {
    # Bejelentkezett felhasználó nem tudja betölteni a regisztrációs ablakot
    # Bejelentkezett felhasználó nem tudja betölteni a login ablakot
    $load['load_register']=0;
    $load['load_login']=0;
} else {
    $_SESSION['RST_LEVEL'] = 0;
}

# general _GET vars
#$get_urlvars = '';
#foreach($_GET as $key=>$value) {
#    if ($key=='lang') continue;
#    $get_urlvars.="$key=$value&";
#}

/* GET id
 * clean getid
 * */
if (!isset($_GET['id']) and isset($_SESSION['getid'])) { 
    unset($_SESSION['getid']);
    if (isset($_GET['ids']))
        unset($_SESSION['getids']);
}
// set getid (numeric)
if (isset($_GET['id']) and !is_array($_GET['id'])) {
    unset($_SESSION['getids']);
    $_SESSION['getid'] = preg_replace("/[^0-9]/", "", $_GET['id']);
    #simple handling of complex ids
    #$_SESSION['getid'] = preg_replace("/([0-9])/", "$1", $_GET['id']);
    if ($_SESSION['getid'] == '') return;
} elseif (isset($_GET['id']) and is_array($_GET['id']) and count($_GET['id'])) {
    $_SESSION['getids'] = array(); 
    foreach($_GET['id'] as $i) {
        $_SESSION['getids'][] = preg_replace("/[^0-9]/", "", $i);
    }
    array_filter($_SESSION['getids']);
}
// set getuid (alphanumeric)
if (isset($_GET['uid'])) {
    #simple handling of complex ids
    #$_SESSION['getuid'] = preg_replace("/([a-z0-9])/", "$1", $_GET['uid']);
    $_SESSION['getuid'] = preg_replace("/[^a-z0-9]/i", "", $_GET['uid']);
    if ($_SESSION['getuid'] == '') return;
}
#should be enabled???
#if (!isset($_GET['uid']) and isset($_SESSION['getuid'])) 
#    unset($_SESSION['getuid']);

if (isset($_GET['history']) and $_GET['history']=='upload')
    $load['load_uploadhistory'] = 1;

if (isset($_GET['data']))
    $load['load_data'] = 1;

if (isset($_GET['history']) and $_GET['history']=='data')
    $load['load_datahistory'] = 1;

if (isset($_GET['history']) and $_GET['history']=='validation')
    $load['load_validhistory'] = $_GET['blt'];

if (isset($_GET['lostpw']))
    $load['load_lostpw'] = 1;

if (isset($_GET['termsandconditions']))
    $load['load_useagreement'] = 1;

if (isset($_GET['specieslist'])) 
    $load['load_specieslist'] = 1;

if (isset($_GET['metaname'])) 
    $load['load_metaname'] = 1;

if (isset($_GET['metadata'])) {
    $load['load_doimetadata'] = 1;
    $_SESSION['LANG'] = 'en';
}
# login page
if (isset($_GET['login'])) {
    $load['load_login'] = 1;
    unset($_SESSION['login_vars']);
}
# profile specific variables, these settings let people to access info pages with GET requests
if(isset($_GET['profile']) and  isset($_SESSION['Tid'])) {
    # profile page - subitems
    if (isset($_GET['showsavedqueries']))
        $load['load_savedqueries'] = 1;
    elseif (isset($_GET['showiups']))
        $load['load_showiups'] = 1;
    elseif (isset($_GET['showowngeoms']))
        $load['load_showowngeoms'] = 1;
    elseif (isset($_GET['showsharedgeoms']))
        $load['load_showsharedgeoms'] = 1;
    elseif (isset($_GET['showuploads']))
        $load['load_showuploads'] = 1;
    elseif (isset($_GET['showapikeys']))
        $load['load_showapikeys'] = 1;
    # set ORCID profile data
    elseif (isset($_GET['sopd'])) {
        $load['load_profile'] = 1;
        $load['load_orcid_profile_data'] = 1;
    } else {
        // user's hash id
        $load['load_profile'] = preg_replace("/[^a-z0-9]/i", "", $_GET['profile']);
        if ($_SESSION['Tcrypt']==$_GET['profile'])
            $_GET['get_own_profile'] = 1;
    }
}
if(isset($_GET['admin']) and  isset($_SESSION['Tid'])) {
    if (isset($_GET['showserverlogs']))
        $load['load_showserverlogs'] = 1;
}
# Profile page variable
if($load['load_login'] and isset($_SESSION['Tid'])) {
    if (defined('LOGINPAGE')) {
        if (LOGINPAGE=='map') { 
            $load['load_mappage'] = 1;
            $load['load_profile'] = 0;
        }
        elseif (LOGINPAGE=='mainpage') {
            $load['load_mainpage'] = 1;
            $load['load_profile'] = 0;
        } else
            $load['load_profile'] = 1;
    } else
        $load['load_profile'] = 1;

    $load['load_login'] = 0;
}

if (isset($_GET['user_specieslist'])) { 
    $load['load_userspecieslist'] = $_GET['user_specieslist'];
    $load['load_profile'] = 0;
}

# Load LoadQuery meta page
if(isset($_GET['loaddata'])) {
    $load['load_loaddata'] = $_GET['loaddata'];
}
# Load LoadQuery meta page
if(isset($_GET['loadmeta'])) {
    $load['load_loadmeta'] = $_GET['loadmeta'];
}
# Load DOI page
if(isset($_GET['loaddoi'])) {
    $load['load_loadmeta'] = '__DOI__';
    $load['load_loaddoi'] = $_GET['loaddoi'];
}
# load database page
if (isset($_GET['database']))
    $load['load_database'] = 1;
# upload page
if (isset($_GET['upload']))
    $load['load_upload'] = 1;
# load queries
if (isset($_GET['LQ'])) 
    $load['load_loadquery'] = $_GET['LQ'];
# map page
if (isset($_GET['map'])) {
    $load['load_mappage'] = 1;
}
# not used in the code, but let the possibility to call the mainpage
if (isset($_GET['mainpage'])) { 
    $load['load_map'] = 1;
    $load['load_mainpage'] = 1;
}
# load preveusly saved map quieries
# loadqueries
if(isset($_GET['LQ'])) { 
    $load['load_mappage'] = 1;
}
# profile: change own email address
if (isset($_GET['changeemail'])) {
    $code = preg_replace('/[^a-zA-Z0-9]/','',$_GET['changeemail']);
    if(strlen($code)==32) {
        $_SESSION['change_email_response'] = change_email($code);
    }
    $load['load_profile'] = 1;
}
# profile: drop profile
if (isset($_GET['drop_my_profile'])) {
    $code = preg_replace('/[^a-zA-Z0-9]/','',$_GET['drop_my_profile']);
    if(strlen($code)==32) {
        if (drop_profile($code))
            include(getenv('OB_LIB_DIR').'logout.php');
    }
}

# index: upload history
# other?
if (isset($_GET['query_api'])) {

    if (!isset($_GET['boxes']))
        $_GET['boxes'] = false;

    if (!isset($_GET['fixheader']))
        $_GET['fixheader'] = true;

    $load['load_getqueryapi'] = 1;
    $load['load_mappage'] = 1;
    if (isset($_GET['output']) and strtolower($_GET['output'])=='csv') {
        if (isset($_GET['filename']))
            $filename = basename($_GET['filename']);
        else
            $filename = PROJECTTABLE.'_['.date('Y-m-d').']_query.csv';

        $qs = json_decode($_GET['query_api'],TRUE);
        $m = array();
        foreach( $qs as $k=>$v) {
            $ja = json_decode($v,TRUE);
            if (count($ja))
                $v = $ja;

            if (!is_array($v)) {
                $m["qids_$k"] = json_encode(array($v));
            } else {
                $m["qids_$k"] = json_encode($v);
            }
        }
        $_REQUEST = $m;

        $_SESSION['show_results']['type'] = 'csv';
        
        // create query string
        include(getenv('OB_LIB_DIR').'query_builder.php');
        
        // execute query
        include(getenv('OB_LIB_DIR').'results_query.php');

        // read and send results
        include_once(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>'csv','specieslist'=>0,'buttons'=>0,'summary'=>0,'filename'=>$filename]);
        if( $r->results_query() ) {
            if ($r->rq['results_count']) {
                $r->printOut();
            } 
            unset($_SESSION['show_results']);
            exit;
        }
        unset($_SESSION['show_results']);
    }
    elseif (isset($_GET['output']) and strtolower($_GET['output'])=='json') {

        if (isset($_GET['filename']))
            $filename = basename($_GET['filename']);
        else
            $filename = PROJECTTABLE.'_['.date('Y-m-d').']_query.json';

        $qs = json_decode($_GET['query_api'],TRUE);
        $m = array();
        foreach( $qs as $k=>$v) {
            $ja = json_decode($v,TRUE);
            if ($ja!='' and count($ja))
                $v = $ja;

            if (!is_array($v)) {
                $m["qids_$k"] = json_encode(array($v));
            } else {
                $m["qids_$k"] = json_encode($v);
            }
        }
        $_REQUEST = $m;

        $_SESSION['show_results']['type'] = 'json';
        include(getenv('OB_LIB_DIR').'query_builder.php');
        include(getenv('OB_LIB_DIR').'results_query.php');
        include_once(getenv('OB_LIB_DIR').'results_builder.php');

        $r = new results_builder(['method'=>'json','specieslist'=>0,'buttons'=>0,'summary'=>0,'filename'=>$filename]);
        if( $r->results_query() ) {
            if ($r->rq['results_count']) {
                $r->printOut();
            } else {
                echo common_message('failed','Results count: 0');
            } 
            unset($_SESSION['show_results']);
            exit;
        }
        unset($_SESSION['show_results']);

    }
}
# GET queries using query_builder syntax
if (isset($_GET['query'])) {
    $load['load_getquery_text'] = $_GET['query'];
    foreach(preg_split('/&/',$_SERVER['QUERY_STRING']) as $key=>$value){
        parse_str($value,$arr);
        //processing arrays without getting [] after variable names!!!
        //not ready...
    }
    if (!isset($_GET['boxes']))
        $_GET['boxes'] = false;

    if (!isset($_GET['fixheader']))
        $_GET['fixheader'] = true;

    $load['load_getquery'] = 1;
    $load['load_mappage'] = 1;
    if (isset($_GET['output']) and $_GET['output']=='csv') {
        $qs = preg_split('/;/',$_GET['query']);
        $m = array();
        foreach( $qs as $q) {
            $kv = preg_split('/:/',$q);
            $m["qids_$kv[0]"] = json_encode(array($kv[1]));
        }
        $_REQUEST = $m;

        $_SESSION['show_results']['type'] = 'csv';
        
        // create query string
        include(getenv('OB_LIB_DIR').'query_builder.php');
        
        // execute query
        include(getenv('OB_LIB_DIR').'results_query.php');

        // read and send results
        include_once(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>'csv','specieslist'=>0,'buttons'=>0,'summary'=>0,'filename'=>PROJECTTABLE.'_['.date('Y-m-d').']_query.csv']);
        if( $r->results_query() ) {
            if ($r->rq['results_count']) {
                $r->printOut();
            } 
            unset($_SESSION['show_results']);
            exit;
        }
        unset($_SESSION['show_results']);
    }
}
# GET queries using query_builder syntax
if (isset($_GET['report'])) {

    unset($_SESSION['show_results']);
    $_SESSION['load_getreport'] = 1;
    $load['load_getreport_label'] = $_GET['report'];
    $load['load_getreport'] = 1;
    $load['load_mappage'] = 1;

    if (isset($_GET['output']) and $_GET['output']=='csv') {

        $_SESSION['show_results']['type'] = 'csv';
        
        // create query string
        include(getenv('OB_LIB_DIR').'query_builder.php');
        
        // execute query
        include(getenv('OB_LIB_DIR').'results_query.php');

        // read and send results
        include_once(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>'csv','specieslist'=>0,'buttons'=>0,'summary'=>0,'filename'=>$_GET['report'].'.csv']);
        if( $r->results_query() ) {
            if ($r->rq['results_count']) {
                $r->printOut();
                unset($_SESSION['show_results']);
            } 
            exit;
        }
        unset($_SESSION['show_results']);
    }
}
# intropage
# it is a one-shoot* intro page defined in local_vars.php.inc and should be exists in the local includes folder:
# as intropage.php
#
# * one-shoot: if any other GET parameters given, e.g. ?login it will not appear in the session.
if (defined('LOAD_INTROPAGE')) {
    $load['load_intropage'] = constant('LOAD_INTROPAGE');
}
# start page
# ezen még finomítani kell esetelg...
if(count($_GET)==0 and $load['load_profile']==0) { 
    $load['load_mainpage'] = 1;
    $load['load_map'] = 1;
} else {
    if ($load['load_intropage'])
        $_SESSION['intropage_off'] = 1;
}
/* backward compatibility
 * deault entry point is the mappage
 * */
if ($load['load_mainpage'] and $load['load_map'] and defined('MAP_OVER_MAINPAGE')) {
    $MOP = constant('MAP_OVER_MAINPAGE');
    if ($MOP) {
        $load['load_mainpage'] = 0;
        $load['load_mappage'] = 1;
    }

    if (defined('LOGINPAGE') and isset($_POST['loginenter'])) {
        if (LOGINPAGE=='map') {
            $load['load_mainpage'] = 0;
        }
        else {
            $load['load_mappage'] = 0;
        }
    }
}
# mappage allways use the map
if (isset($load['load_mappage']) and $load['load_mappage']) 
    $load['load_map'] = 1;

$load['load_cookies'] = (isset($_GET['p']) && $_GET['p'] == 'cookies') ? 1 : 0;
$load['load_privacy'] = (isset($_GET['p']) && $_GET['p'] == 'privacy') ? 1 : 0;
$load['load_terms'] = (isset($_GET['p']) && $_GET['p'] == 'terms') ? 1 : 0;
$load['load_technical'] = (isset($_GET['p']) && $_GET['p'] == 'technical') ? 1 : 0;
$load['load_whatsnew'] = (isset($_GET['p']) && $_GET['p'] == 'whatsnew') ? 1 : 0;

//debug($load);

$i = 0;
foreach ($load as $key=>$value) {
    $$key = $value;
    /*
     * if ($value!=0 and $value!=-1) {
        $i++;
        if ($i>1) {
            $$key = 0;
            #log_action($$key);
        }
    }*/
}

?>
