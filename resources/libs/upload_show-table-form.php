<?php
if(!isset($_SESSION['upload']['openpages'])) $_SESSION['upload']['openpages']=array();
$this_page = md5($_SESSION['openpage_key'].$insert_allowed.$selected_form_type);
$_SESSION['upload']['openpages'][] = $this_page;

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';


// edit record mode
$editRecord = isset($_GET['editrecord']) ? $_GET['editrecord'] : 0;
// single record mode
$singleRecord = $editRecord ? 1 : 0;
// other options to use single record mode
// ...


//column ordering
$cmd = sprintf("SELECT \"column\",position_order FROM project_forms_data WHERE form_id=%s ORDER BY position_order",quote($insert_allowed));
$respo = pg_query($BID,$cmd);
$column_positions = pg_fetch_all($respo);
$poscolumns = array_column($column_positions, 'column');
$positions = array_column($column_positions, 'position_order');

/* preparing table based on uploaded data
 *
 * */
$scolumn = array();
if ($table_type == 'file') {

    $rows_count = get_upload_count();
    if (!$rows_count) {
        $Error = "No rows in the uploaded sheet!";
        $insert_allowed = 0;
        //exit;
    }
    $theader = $_SESSION['theader'];
    if (isset($_SESSION['feed_header']))
        $feed_header = $_SESSION['feed_header'];
    else
        $feed_header = array();


    //page counter initialization
    if (!isset($_SESSION['sheetDataPage_counter']))
        $_SESSION['sheetDataPage_counter'] = 0;

    //aktuális betöltött teljes adathossz
    $_SESSION['sheetDataPage']= $rows_count;

    //aktuális, max. perpage sor adat hossz
    $table = get_sheet_page($_SESSION['sheetDataPage_counter'],$_SESSION['perpage']);

    // database column list
    $cmd = sprintf("SELECT \"column\" FROM project_forms_data WHERE form_id=%s",quote($insert_allowed));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $scolumn = pg_fetch_assoc($res);
        while ($r = pg_fetch_assoc($res)){
            $scolumn[] = $r['column'];
        }
    }
}

$type_values=array(str_text=>'text',str_numeric=>'numeric',str_list=>'list',str_geometry.':'.str_point=>'point',str_geometry.':'.str_line=>'line',str_geometry.":".str_polygon=>'polygon',str_geometry.':'.str_wkt=>'wkt',str_date=>'date',str_datetime=>'datetime',str_time=>'time',str_photo_id=>'file_id',str_boolen=>'boolean','autocomplete'=>'autocomplete',str_time_to_minutes=>'timetominutes',str_time_interval=>'tinterval');
$control_values=array('nocheck','minmax');
$ccon=0; //column counte
$columns = array();
$dbheader = array();
$style = array();
//$css_class = array();
$input = array();
$extraparams = array();
$fullist = 0;
$fullist_data=array();
$species_column='';
$geometry_column=array();
$attachment_column=array();
$datum_column=array();
$colour_ring_column=array();
$autcomplete_column=array();
$max = array();
$min = array();
$exa = array();
$wkt = array();
$num = array();
$wkt_line = array();
$datum = array();
$db_field_list = array();
$placeholder = array();
$modules_div = '';
$have_geometry = 0;
$html_type = array();

// MODULES ====>
$modules = new modules();

// photo module - hidden div
if ($modules->is_enabled('photos')) {
    $modules_div .= $modules->_include('photos','upload_box');
}
/* LOOP: SET parameters of columns
 * a kiválasztott form minden oszlopára
 * */
$dbcolist = dbcolist('array',$selected_form_table);
if ($editRecord)
    $dbcolist['obm_id'] = "Edit ID";

$st_col = st_col($selected_form_table,'array');

//edit option
if ($modules->is_enabled('massive_edit') and isset($_GET['massiveedit'])) {
    $dbcolist['obm_id'] = 'Edit ID';
    #$dbcolist['obm_files_id'] = 'Attachment ID';
}

if (array_filter($positions))
    $dbcolist = array_merge(array_flip($poscolumns), $dbcolist);
$database_columns_with_label = array();
foreach($dbcolist as $key=>$val) {
    $database_columns_with_label[] = array($key=>$val);
}

$gkeys_allowed = array();

//form id queue: list of form id for each field
$form_id_queue = array_pad(array(), count($dbcolist), $insert_allowed);

$r2 = array('description'=>'');

$non_default_values_count = 0;

$references = array('');

/* Database columns */
$gn = 0;
$column_type_array = array();
while ($col_obj = current($database_columns_with_label)) {
    $keys = array_keys($col_obj);
    $column_name = $keys[0];
    $column_label = $col_obj[$column_name];
    next($database_columns_with_label);
    $gn++;


    $initial = array('max'=>'','min'=>'','exact'=>'','wkt'=>'','num'=>'','wkt_line'=>'','datum'=>'','input'=>'','file'=>'','geom'=>'','epars'=>'','class'=>'','placeholder'=>'','type_sel'=>'','control_sel'=>'','control_text'=>'','count'=>'','color'=>'','hclass'=>'','field_description'=>'','autocomplete'=>'','colourring'=>'','html_type'=>'');
    $initial_opt = array();

    if ($insert_allowed) {
        $fe = array_shift($form_id_queue);
        //$cmd = "SELECT \"column\",\"description\",\"type\",\"control\",\"count\",\"list\",\"obl\",d.\"fullist\",form_access FROM project_forms_data d LEFT JOIN project_forms f ON (f.form_id=d.form_id) WHERE f.\"form_id\"=$e AND \"column\"='$k'";
        $k_split = preg_split('/:/',$column_name);
        if (count($k_split) == 2)
            $k_split = $k_split[1];
        else
            $k_split = $k_split[0];

        $cmd = "SELECT \"column\",description,\"type\",\"control\",array_to_string(\"count\",':') as cnt,array_to_string(list,',') as list,obl,fullist,genlist,relation,
                pseudo_columns,custom_function,default_value,list_definition,column_label
            FROM project_forms_data
            WHERE form_id='$fe' AND \"column\"='$k_split' AND default_value IS NULL";

        $res = pg_query($BID,$cmd);

        if (pg_num_rows($res)) {

            $non_default_values_count++;

            $gkeys_allowed[] = $column_name;
            $r2 = pg_fetch_assoc($res);
            $column_type_array[$r2["column"]] = $r2;
            
            if ($r2['column_label']!='') {
                $label = $r2['column_label'];
                if (preg_match('/^str_/',$label))
                    if (defined($label))
                        $label = constant($label);
                $column_label = $label;
            }


            if ($r2['column'] == $st_col['X_C'] or $r2['column'] == $st_col['Y_C'] or $r2['column'] == $st_col['GEOM_C'])
                $have_geometry = 1;

            if ($r2['pseudo_columns'] != '') {
                //dinpi_meta.*
                $sp = preg_split('/:/',$r2['pseudo_columns']);
                $cmd = sprintf('SELECT form_id FROM project_forms WHERE form_name=%s',quote($sp[0]));
                $pfid_res = pg_query($BID,$cmd);
                $pfid_row = pg_fetch_assoc($pfid_res);
                $p_form = $pfid_row['form_id'];

                $p_cols = preg_split('/,/',$sp[1]);
                $pcg = array();
                $gnpp = $gn;
                foreach($p_cols as $pc) {
                    array_splice($form_id_queue, 0, 0, $p_form);
                    //instead of METANAME, create unique name
                    array_splice($database_columns_with_label,$gnpp,0,array(array($column_name.':'.$pc=>$column_name.': '.$pc)));
                    $gnpp++;
                }

                while (key($database_columns_with_label) !== $gn) next($database_columns_with_label);
                $gn++;
            }

            //if($r2['fullist']==1) $fullist = 1;
            //if($fullist and $column_name==$_SESSION['st_col']['SPECIES_C']) {
            if($r2['fullist']==1) {
                $fullist=1;
                foreach(explode(',',$r2['list']) as $l) {
                    $fullist_data[] = $l;
                }
                /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *
                 * IGY CSAK A FAJNÉVRE MŰKÖDIK A TELJES LISTÁZÁS!!!!!
                 *
                 * */
                $species_column = $ccon;
            }
            $columns[] = $r2['column'];
            //obligatory colorizatioin
            if ($r2['obl']==1) $initial['hclass']="red";
            elseif ($r2['obl']==2) $initial['hclass']="gray";
            elseif ($r2['obl']==3) $initial['hclass']="pink";

            if ($r2['relation']!='') $initial['hclass']='blue';
            // type selection=str_text;
            $n=array_search($r2['type'],$type_values);
            $initial['type_sel'] = $n;
            if ($r2['type'] == 'numeric')
                $initial['html_type'] = 'number';
            else
                $initial['html_type'] = '';

            if($r2['type']=='boolean') {
                $r2['type']='list';
                $r2['fullist']=0;
                // ha nincs kitöltve a lista mező
                if ($r2['list']=='')
                    $r2['list'] = "true,false";
            }
            // list input field
            //if($r2['type']=='list' and $fullist==0) {
            if($r2['type']=='list' and $r2['fullist']==0) {
                $sel = new upload_select_list($r2);
                $initial['input'] = 'select';
                $initial_opt = $sel->get_options('array');
                $initial['epars'] = $sel->get_data_attribute();
                $initial['class'] .= " wauto";
                if ($sel->multiselect) {
                    $initial['epars'] .= ' multiple';
                    $initial['class'] .= ' fTSelect';
                }
                /* valami leírás kéne, hogy mi ez
                 * MILVUSnál
                 * már nem használjuk
                 * */
                if ($modules->is_enabled('extra_params'))
                    $initial['epars'] = $modules->_include('extra_params','add_params',array($r2['column']));

            }
            elseif ($r2['type']=='autocomplete' and ($r2['genlist']!='' or $r2['list_definition']!='')) {
                $sel = new upload_select_list($r2);
                $initial['epars'] = $sel->get_data_attribute();
                $initial['input'] = 'input';
                $initial['class'] .= " genlist";
                $initial['placeholder'] = str_type_to_listing_values;
                $initial['autocomplete'] = $ccon;
            }
            elseif ($r2['type']=='autocompletelist' and ($r2['genlist']!='' or $r2['list_definition'] !== '')) {
                $initial['input'] = 'input';
                $initial['class'] .= " genwlist";
                $initial['placeholder'] = str_type_to_listing_values;
                $initial['autocomplete'] = $ccon;
            }
            else {
                // normal input
                $initial['input'] = 'input';

                if($r2['type']=='wkt') {
                    $initial['wkt'] = 1;
                    $initial['geom'] = $ccon;
                }
                elseif($r2['type']=='numeric' or $r2['type']=='point') {
                    $initial['num'] = 1;
                    if($r2['type']=='point') $initial['geom'] = $ccon;
                }
                elseif($r2['type']=='line') {
                    $initial['wkt_line'] = 1;
                    $initial['geom'] = $ccon;
                }
                elseif($r2['type']=='date') {
                    $initial['datum'] = $ccon;
                }
                elseif($r2['type']=='polygon' or $r2['type']=='point') {
                    $initial['geom'] = $ccon;
                }
                elseif ($r2['type']=='file_id') {
                    $initial['file'] = $ccon;
                }
                elseif ($r2['type']=='date') {
                    $initial['datum'] = $ccon;
                }
                elseif($r2['type']=='datetime') {
                    $initial['datum'] = $ccon;
                }
                elseif($r2['type']=='crings') {
                    $initial['colourring'] = $ccon;
                    $j = json_decode($r2['list_definition'],true);
                    $available_ring_colors = $j['list'];
                }

                // text input field
                // control restriction nocheck, minmax
                $n = 0;
                $n = array_search($r2['control'],$control_values);

                $initial['count'] = $r2['cnt'];

                if ($n==0)
                    $initial['control_sel']="";   #str_arbitrary;
                elseif ($n==1) {
                    $minmax = preg_split('/:/',$initial['count']);
                    if (count($minmax)==1) {
                        $mm_min = 0;
                        $mm_max = $minmax[0];
                    } else {
                        $mm_min = $minmax[0];
                        $mm_max = $minmax[1];
                    }

                    if ($mm_min == $mm_max) {
                        $initial['control_sel'] = str_exact_match."&nbsp;".$mm_min;
                        $initial['exact'] = $mm_min;
                    }
                    else {
                        $hj = array();
                        if ($mm_min > 0) {
                            $hj[] = str_min."&nbsp;".$mm_min;
                            $initial['min'] = $min;
                        }
                        if ($mm_max > 0) {
                            $hj[] = str_max."&nbsp;".$mm_max;
                            $initial['max'] = $mm_max;
                        }
                        $initial['control_sel'] = implode($hj,' ');
                    }
                    $initial['control_sel'] = "(".$initial['control_sel'].")";
                } else {
                    $initial['control_sel'] = '';
                }

                $initial['control_text'] = $initial['control_sel'];
            }
            $ccon++;
        }
        else {
            // skip  columns which non-included in the form
            continue;
        }
    }


    /* create field description */
    if ($r2['description']!='') {
        $translated = $r2['description'];
        if (defined($r2['description'])) $translated = constant($r2['description']);
        $initial['field_description'] = sprintf("%s<br>",$translated);
    }

    if ($table_type == 'file' and ( !isset($_SESSION['upload_column_assign']) or !in_array($column_name,$_SESSION['upload_column_assign'])) ) {
        $dbheader[]='';
        $input[] = 'input';
        $style[] = '';
        $options[] = '';
        $extraparams[]= '';
        $placeholder[] = '';
        $datum_column[]='';
        $colour_ring_column[]='';
        $geometry_column[]='';
        $attachment_column[]='';
        $max[]='';
        $min[]='';
        $exa[]='';
        $wkt[]='';
        $num[]='';
        $wkt_line[]='';
        $autocomplete_column[]='';
        $html_type[] = $initial['html_type'];

    } else {
        $dbheader[]=sprintf("<div class='%s'><span style='font-weight:bold;'>%s</span><br><div style='font-size:90%s;font-style:italic'>%s%s: %s<br>%s</div></div>",$initial['hclass'],$column_label,'%',$initial['field_description'],t(str_type),$initial['type_sel'],$initial['control_text']);
        $style[]=$initial['class'];
        $options[]=$initial_opt;
        $extraparams[]=$initial['epars'];
        $input[]=$initial['input'];
        $datum_column[]=$initial['datum'];
        $colour_ring_column[]=$initial['colourring'];
        $geometry_column[]=$initial['geom'];
        $attachment_column[]=$initial['file'];
        $max[]=$initial['max'];
        $min[]=$initial['min'];
        $exa[]=$initial['exact'];
        $wkt[]=$initial['wkt'];
        $num[]=$initial['num'];
        $wkt_line[]=$initial['wkt_line'];
        $autocomplete_column[]=$initial['autocomplete'];;
        if ($initial['placeholder']!='')
            $placeholder[]="placeholder='{$initial['placeholder']}'";
        else
            $placeholder[]='';
        $html_type[] = $initial['html_type'];
    }

    /* create database column list */
    if(count($scolumn) and array_search($column_name,$scolumn)===false) {
        //ha nincs meg a megengedett oszlopok között nem rakjuk be a listába
        continue;
    }
    else {
        // creating column list
        if ($table_type=='web' or $singleRecord) {
            $disable_drag = 'oListDisabled';
        } else {
            $disable_drag = '';
        }

        $references[] = "$column_label::$column_name";
        if ($singleRecord) {
            $db_field_list[] = sprintf('<div class="%3$s singlerecord-header-label">%1$s<br><div class="colhelptext">%4$s</div></div><div class="colhelptext croptext">%2$s</div>',$column_label,nl2br($initial['field_description']),$initial['hclass'],$initial['type_sel']);
        } else
            //alapból minden elem a bal oldali listába megy
            $db_field_list[] = sprintf('<li id="%7$s" class="%8s draggable-colnames">
                <div class="%1$s" style="padding:1px;height:100%3$s">
                    <div class="field-title">%2$s</div>
                    <div class="colhelptext croptext">%4$s</div><div class="coltypetext">%5$s %6$s</div>
                </div></li>',
                $initial['hclass'],$column_label,'%',nl2br($initial['field_description']),$initial['type_sel'],$initial['control_text'],$column_name,$disable_drag);
               #           1        2  3             4                       5                  6                      7         8
    }
}

if ($table_type=='file') {
    //Too long wfs response handling...
    //page buttons
    $sheets = "";
    if (isset($_SESSION['sheetDataPage']) and !$singleRecord) {
        $sheets .= "<ul class='page-group'>";
        $sheets .= pager($_SESSION['perpage'],$_SESSION['sheetDataPage'],$_SESSION['sheetDataPage_counter']);
        $sheets .= "</ul> <input id='changepp' size='3' style='width:32px;padding:2px;border:1px solid #cacaca' value='{$_SESSION['perpage']}'>/".str_page.".";
        //$error[] = $sheets;
    }
    // a fájlnak több oszlopa van mint az adatbázisnak
    $dbheader_len = count($dbheader);
    $theader_len = count($theader);
    $uploader_file_header_len = $theader_len;
    if ($dbheader_len > $theader_len) {
        $letter = 'A';
        //kezdőbetű beállítása
        for ($i=0;$i<$theader_len;$i++) {
            $letter++;
        }
        for($i=$theader_len;$i<$dbheader_len;$i++) {
            $theader[] = $letter;
            ++$letter;
        }
        $_SESSION['theader'] = $theader;
    }
    //extra oszlopok hozzáadása fájl feltöltés esetén amikor több oszlop van a fájlban mint az adatbázisban
    $theader_len = count($theader);
    for ($i=0;$i<($theader_len-$dbheader_len);$i++) {
        $dbheader[] = '';
        $input[] = 'input';
        $style[] = '';
        $options[] = '';
        $placeholder[] = '';
        $extraparams[]= '';
        $datum[]='';
        $max[]='';
        $geometry_column[] = '';
        $attachment_column[] = '';
        $autocomplete_column[] = '';
        $html_type[] = '';
    }
    //very tricky input formatting for files
    //we get the input settings from the left columns and set in right
    $input_mod = array_fill(0,count($input),'input');
    $options_mod = array_fill(0,count($input),'');
    $extraparams_mod = array_fill(0,count($input),'');
    $placeholder_mod = array_fill(0,count($input),'');
    $style_mod = array_fill(0,count($input),'');
    $header_mod = array_fill(0,count($input),'');
    $datum_mod = array_fill(0,count($input),'');
    $geometry_mod = array_fill(0,count($input),'');
    $autocomplete_mod = array_fill(0,count($input),'');
    $colourring_mod = array_fill(0,count($input),'');
    $attachment_mod = array_fill(0,count($input),'');
    $max_mod = array_fill(0,count($input),'');
    $num_mod = array_fill(0,count($input),'');

    // oszlop mozgatáskor az oszlopok extra kiterjesztéseinek dinamikus hozzárendelése.
    if (isset($_SESSION['upload_column_assign'])) {

        foreach($_SESSION['upload_column_assign'] as $k=>$v){
            if (($key = array_search($v,$gkeys_allowed)) !== false) {
                $input_mod[$k] = $input[$key];
                $options_mod[$k] = $options[$key];
                $header_mod[$k] = $dbheader[$key];
                $style_mod[$k] = $style[$key];
                $placeholder_mod[$k] = $placeholder[$key];
                $extraparams_mod[$k] = $extraparams[$key];
                $datum_mod[$k] = $datum_column[$key];
                $attachment_mod[$k] = $attachment_column[$key];
                $colourring_mod[$k] = $colour_ring_column[$key];
                $geometry_mod[$k] = $geometry_column[$key];
                $autocomplete_mod[$k] = $autocomplete_column[$key];
                $max_mod[$k] = $max[$key];
                $num_mod[$k] = $num[$key];
            }
        }
    }
    $input = $input_mod;
    $options = $options_mod;
    $dbheader = $header_mod;
    $style = $style_mod;
    $placeholder = $placeholder_mod;
    $extraparams = $extraparams_mod;
    $datum_column = $datum_mod;
    $attachment_column = $attachment_mod;
    $colour_ring_column = $colourring_mod;
    $geometry_column = $geometry_mod;
    $autocomplete_column = $autocomplete_mod;
    $max = $max_mod;
    $num = $num_mod;
} else {
    // web type table
    $sheets = '';
    $theader = $dbheader;
    $_SESSION['theader'] = $columns;
}

//geometry columns' options
//GOPT
if (isset($_SESSION['Tid']))
    $cmd = sprintf('SELECT id,name,to_char(timestamp,\'YYYY.MM.DD:HH24:MI:SS\') as datetime
        FROM system.shared_polygons LEFT JOIN system.polygon_users p ON polygon_id=id
        WHERE select_view IN (\'2\',\'3\',\'only-upload\',\'select-upload\') AND p.user_id=%d ORDER BY name,datetime',$_SESSION['Tid']);
else
    $cmd = sprintf('SELECT id,name,to_char(timestamp,\'YYYY.MM.DD:HH24:MI:SS\') as datetime
        FROM system.shared_polygons
        WHERE access IN (\'4\',\'public\') OR (project_table=\'%s\' AND access IN (\'3\',\'project\')) ORDER BY name,datetime',PROJECTTABLE);

$res = pg_query($ID,$cmd);
$gopt = "<option>".str_get_geometry_from_named_list."</option>";
while($row=pg_fetch_assoc($res)) {
    $gopt .= sprintf("<option value='%d'>%s - %s</option>",$row['id'],$row['name'],$row['datetime']);
}


#module call:
#push options from other user defined tables
#
//geometry choose window
$u = "$protocol://".URL."/upload/geomtest/?addgeom";
//$u = "http://".URL."/?onlymap";
$gopt_window = "<div class='fix-modal' id='gpm' style='border:1px solid #666;border-bottom:4px solid #666'>
 <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:-10px -10px 10px -10px !important'>".t(str_geometry)."</h2><button style='margin: -11px' class='pure-button button-passive fix-modal-close' id='gpm-close'><i class='fa fa-close'></i></button>
<br>
<ul style='list-style-type:none;padding:10px'>
<li style='font-size:150%;padding-bottom:1em'><a href='$u' id='map_' class='mapclick'><i class='fa fa-lg fa-map-marker fa-fw'></i> ".str_get_coordinates_from_map."</a></li>
<li style='font-size:150%'><i class='fa fa-lg fa-map fa-fw'></i> <select id='gopt_' class='goptchoice'>$gopt</select></li></ul>
</div>";


/*---------------------------------- SHOW TABLE */
/*
  CREATE OR REPLACE FUNCTION idx(anyarray, anyelement)
  RETURNS INT AS
$$
  SELECT i FROM (
     SELECT generate_series(array_lower($1,1),array_upper($1,1))
  ) g(i)
  WHERE $1[i] = $2
  LIMIT 1;
$$ LANGUAGE SQL IMMUTABLE;
 */


// DEFAULT VALUES
//$order = "'".implode("','",explode(',',$_SESSION['st_col']['ORDER']))."'";

// beállított rendezés helyett abc sorrend
// using PROJECTTABLE not possible if we have /table forms!!!!
#$cmd = "SELECT d.description,default_value,\"column\",short_name,array_to_string(list,',') as list,type,genlist FROM project_forms_data d LEFT JOIN project_forms f ON (d.form_id=f.form_id) LEFT JOIN project_metaname m ON column_name=\"column\" WHERE default_value IS NOT NULL AND d.form_id='$insert_allowed' AND m.project_table='".PROJECTTABLE."' ORDER BY idx(Array[$order],\"column\"::text)";
#

    if (isset($st_col['ORDER']))
        $order_col = json_decode($st_col['ORDER'],true);
    else
        $order_col = array();

    // default ordering
    $values = "('',1)";

    $ordered_values = array();
    if (isset($order_col[$selected_form_table]))
        foreach($order_col[$selected_form_table] as $key=>$val) {
            $ordered_values[] = "('$key',$val)";
        }
    if (count($ordered_values))
        $values = implode(',',$ordered_values);
    // ('adatkozlo',1),('szamossag',3),('gyujto',4),('magyar',5),('eov_x',6),('eov_y',7),('hely',8
    //

    if (array_filter($positions))
        $cmd = "SELECT d.description,default_value,\"column\",short_name,array_to_string(list,',') as list,type,genlist,obl,relation,control,custom_function,list_definition,api_params,column_label
            FROM project_forms_data d
            LEFT JOIN project_forms f ON (d.form_id=f.form_id)
            LEFT JOIN project_metaname m ON column_name=\"column\"
            WHERE default_value IS NOT NULL AND d.form_id='$insert_allowed' AND m.project_table='$selected_form_table' ORDER BY position_order";
    else
        $cmd = "SELECT d.description,default_value,\"column\",short_name,array_to_string(list,',') as list,type,genlist,obl,relation,control,custom_function,list_definition,api_params,column_label
            FROM project_forms_data d
            LEFT JOIN project_forms f ON (d.form_id=f.form_id)
            LEFT JOIN project_metaname m ON column_name=\"column\"
            LEFT JOIN (
              VALUES $values
             ) AS x (id, ordering) on m.\"column_name\" = x.id
            WHERE default_value IS NOT NULL AND d.form_id='$insert_allowed' AND m.project_table='$selected_form_table' ORDER BY x.ordering";

    $res = pg_query($BID,$cmd);

    $default_values_count = pg_num_rows($res);

if (pg_num_rows($res)) {
    // push them into the modules area
    $modules_div .= "<div style='background-color:#f2f2f2;border-bottom:1px solid #dadada;margin:0;padding:10px' class='mezok pure-form'>";

    $default_values_content = "";
    //Listing default values - projects_form_data
    //\"column\",description,\"type\",\"control\",\"count\",array_to_string(list,',') as list,obl,fullist
    while($row=pg_fetch_assoc($res)){

        if ($row['obl']==1) $hclass="red";
        elseif ($row['obl']==2) $hclass="gray";
        elseif ($row['obl']==3) $hclass="pink";

        if ($row['relation']!='') $hclass='blue';

        //description
        if ($row['description']!='') {
            $label = $row['description'];
             if (preg_match('/^str_/',$label))
                 if (defined($label))
                     $label = constant($label);
            $des = "($label)";
        }
        else $des = '';

        // columns' labels
        if ($row['short_name']!='' and $row['column_label']=='') {
            $label = $row['short_name'];
            if (preg_match('/^str_/',$label))
                if (defined($label))
                    $label = constant($label);
            $sn = $label;
        } elseif ($row['column_label']!='') {
            $label = $row['column_label'];
            if (preg_match('/^str_/',$label))
                if (defined($label))
                    $label = constant($label);
            $sn = $label;
        }
        else $sn = '';

        if ($row['column'] == $st_col['X_C'] or $row['column'] == $st_col['Y_C'] or $row['column'] == $st_col['GEOM_C']) {
            $have_geometry = 1;
        }

        $m = array();

        // default input
        if (isset($preSetFields->{$row['column']}))
            $row['default_value'] = $preSetFields->{$row['column']};

        $set_value = '';
        // default user email
        if ($row['default_value'] == '_email') {
            if (isset($_SESSION['Tid'])) {
                $row['default_value'] = $_SESSION['Tmail'];
            } else{
                $row['default_value'] = "_input";
            }
            if ($row['type']=='autocomplete' or $row['type']=='autocompletelist') {
                $set_value = $row['default_value'];
                $row['default_value'] = "_autocomplete";
            }
        }
        if ($row['default_value'] == '_login_name') {
            if (isset($_SESSION['Tid'])) {
                $row['default_value'] = $_SESSION['Tname'];
            } else{
                $row['default_value'] = "_input";
            }
            if ($row['type']=='autocomplete' or $row['type']=='autocompletelist') {
                $set_value = $row['default_value'];
                $row['default_value'] = "_autocomplete";
            }
        }
        $api_params = json_decode($row['api_params']);

        $sticky = '';
        $sticky_class = 'fa-rotate-45';
        if (in_array('sticky',$api_params)) {
            $sticky = 'sticky';
            $sticky_class = '';
        }

        $d = '';
        $sel = new upload_select_list($row);

        if (preg_match('/^_input/',$row['default_value'])) {
            $class = "default";
            $placeh = "";
            $nested = "";
            if ($row['type']=='autocomplete' and ($row['genlist']!='' or $row['list_definition']!='')) {
                $class .= " genlist";
                $nested = $sel->get_data_attribute();
                $placeh = str_type_to_listing_values;
            }
            elseif ($row['type']=='autocompletelist' and ($row['genlist']!='' or $row['list_definition']!='')) {
                $class .= " genwlist";
                $placeh = str_type_to_listing_values;
            }
            $d = sprintf('<div class="%4$s mezok default-input-div pure-u-1-1" ><input data-control="%6$s" id="default-%1$s" %5$s value="%8$s" class="pure-u-1-1 %2$s" placeholder="%3$s"><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %7$s sticky-def %6$s"></i></button></div>',$row['column'],$class,$placeh,$hclass,$nested,$sticky,$sticky_class,$set_value);

        // alias of _input
        } elseif (preg_match('/^_autocomplete/',$row['default_value'])) {

            $class = "default";
            $placeh = "";
            $nested = "";

            if ($row['type']=='autocomplete' and ($row['genlist']!='' or $row['list_definition']!='')) {
                $class .= " genlist";
                $nested = $sel->get_data_attribute();
                $placeh = str_type_to_listing_values;
            }
            elseif ($row['type']=='autocompletelist' and ($row['genlist']!='' or $row['list_definition']!='')) {
                $class .= " genwlist";
                $placeh = str_type_to_listing_values;
                if ($set_value!='') $set_value .= ", ";
            }
            //$d = sprintf('<div class="%4$s mezok pure-u-1-1" style="padding-left:1px;border-radius:2px"><input id="default-%1$s" %5$s value="%6$s" class="pure-u-1-1 %2$s" placeholder="%3$s"></div>',$row['column'],$class,$placeh,$hclass,$nested,$set_value);
            $d = sprintf('<div class="%4$s mezok default-input-div pure-u-1-1" ><input data-control="%6$s" id="default-%1$s" %5$s value="%8$s" class="pure-u-1-1 %2$s" placeholder="%3$s"><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %7$s sticky-def %6$s"></i></button></div>',$row['column'],$class,$placeh,$hclass,$nested,$sticky,$sticky_class,$set_value);

        // boolean list as simple type of _list
        } elseif (preg_match('/^_boolean/',$row['default_value'])) {

            $d = sprintf('<div class="%4$s mezok default-input-div pure-u-1-1"><select id="default-%1$s" data-control="%5$s" class="pure-u-1-1 default"><option></option><option value="TRUE">%2$s</option><option value="FALSE">%3$s</option></select><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %6$s sticky-def %5$s"></i></button></div>',$row['column'],str_true,str_false,$hclass,$sticky,$sticky_class);

        // default list
        } elseif (preg_match('/^_list:?(.+)?/',$row['default_value'],$def)) {
            /* SELECT: előtag után sql DINSTINCT lista generálás
             * */
            $opt = $sel->get_options('html');
            $opt_array = $sel->get_options('array');
            $multiselect = ($sel->multiselect) ? true : false;
            //[{"value":"allatok","selected":"","disabled":"","label":["\u00e1llatok"],"bgImage":""},{"value":"novenyek","selected":"selected","disabled":"","label":["n\u00f6v\u00e9nyek"],"bgImage":""},{"value":"gomak","selected":"","disabled":"","label":["gomb\u00e1k"],"bgImage":""},{"value":"urlenyek","selected":"","disabled":"","label":["\u0171rl\u00e9nyek"],"bgImage":""}]
            $nested = $sel->get_data_attribute();

            $ssiz = $sel->ssiz;
            $siz_height = '';

            if ($ssiz != '') {
                $siz_height = "height: auto;";
            }

            $n = (isset($nested)) ? $nested : '';


            //if (count($opt)<2 && (!stripos($n, 'nested'))) {
                // do not display list with one or 0 element and "data-nested=default"
            //    if (!in_array('hidden',$api_params))
            //        $api_params[] = 'hidden';
            //}

            if (in_array('list_elements_as_buttons',$api_params)) {
                $button_type = ($multiselect) ? 'checkbox': 'radio';

                $radio_buttons = array();
                foreach($opt_array as $radio_option_element) {
                    $checked = $default = "";
                    $button_style = 'button-href';
                    $img_style = '';
                    if ($radio_option_element['selected'] == 'selected') {
                        $checked = 'checked';
                        $default = 'default';
                        $button_style = 'button-href button-secondary';
                    }
                    $label = ($radio_option_element['label'][0]=='') ? "&nbsp;" : $radio_option_element['label'][0];
                    if ($radio_option_element['bgImage']!='') {
                        $label = "<img src='{$radio_option_element['bgImage']}' style='max-height:40px'>";
                        $img_style='padding:0';
                    }

                    $radio_buttons[] = sprintf('<li class="pure-u-1-4 pure-button %9$s" style="%11$s"><input name="radio_options_%1$s" type="%10$s" id="default-%1$s" %2$s %3$s %7$s value="%4$s" class="%8$s" %5$s>%6$s</li>',$row['column'],$n,$ssiz,$radio_option_element['value'],$radio_option_element['disabled'],$label,$checked,$default,$button_style,$button_type,$img_style);
                }
                $d = sprintf('<div class="%1$s mezok pure-u-1-1 options" style="padding-left:1px;border-radius:2px"><ul>%2$s</ul></div>',$hclass,implode('',$radio_buttons));
            } else {

                $selected = '';
                foreach($opt_array as $e) {
                    if ($e['selected'] == 'selected') {
                        $selected = $e['value'];
                    }
                }
                $d = sprintf('<div class="%5$s mezok default-input-div pure-u-1-1">
                    <select id="default-%1$s" data-control="%7$s" data-selected="%9$s" %2$s %3$s class="pure-u-1-1 default" style="%6$s">%4$s</select><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %8$s sticky-def %7$s"></i></button></div>',$row['column'], $n,$ssiz,implode($opt),$hclass,$siz_height,$sticky,$sticky_class,$selected);

            }

            unset($nested);

        // default geometry
        } elseif ($row['default_value']=='_geometry') {
            $d = sprintf('<div class="%3$s mezok default-input-div pure-u-1-1"><input id="default-%1$s" data-control="%4$s" value="" style="width:90%2$s" class="default" ><button class="geom-down" id="gm_default-%1$s"><i class="fa fa-map-marker fa-lg fa-fw"></i></button><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %5$s sticky-def %4$s"></i></button></div>',$row['column'],'%',$hclass,$sticky,$sticky_class);

        // default attachment
        } elseif ($row['default_value']=='_attachment') {
            $d = sprintf('<div class="%3$s mezok default-input-div pure-u-1-1"><input id="default-%1$s" data-control="%4$s" value="" style="width:90%2$s" class="default" ><button class="photo-here" id="gm_default-%1$s"><i class="fa fa-paperclip fa-lg fa-fw"></i></button><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %5$s sticky-def %4$s"></i></button></div>',$row['column'],'%',$hclass,$sticky,$sticky_class);

        // default date
        } elseif ($row['default_value']=='_datum') {
           $d = sprintf('<div class="%3$s mezok default-input-div pure-u-1-1"><input id="default-%1$s" data-control="%4$s" value="" style="width:90%2$s" class="default" ><button class="datepickerb" id="gm_default-%1$s"><i class="fa fa-calendar fa-lg fa-fw"></i></button><button class="sticky-thumb pure-button button-href"><i class="fa fa-lg fa-thumb-tack %5$s sticky-def %4$s"></i></button></div>',$row['column'],'%',$hclass,$sticky,$sticky_class);

        } else {
            $tl = $l_text = $l_val = $l_translated = $l = "";
            $tl = preg_split('/:/',$row['default_value']);
            if (count($tl)>1) {
               $l_text = $tl[0];
               $l_val = $tl[1];
            } else {
               $l_text = $l_val = $row['default_value'];
            }
            if ($l_text == 'true') $l_translated = constant("str_".$l_text);
            elseif ($l_text == 'false') $l_translated = constant("str_".$l_text);
            elseif( defined($l_text)) $l_translated = constant($l_text);
            else $l_translated = $l_text;
            if(trim($l_val)=='') $l_val=$l_text;
            $opt = "<option value='$l_val' selected>$l_translated</option>";


            // readonly input
            if (in_array('readonly',$api_params)) {
                $field = sprintf('<select id="default-%s" data-control="readonly,%s" class="pure-u-1-1 default" style="border:1px solid #c2c2c2;padding:3px;background-color:transparent;-moz-appearance:none">%s</select>',$row['column'],$sticky,$opt);
            } else {
                // no translation for editable elements
                $field = sprintf('<input id="default-%1$s" data-control="%3$s" class="default pure-u-1-1" value="%2$s"><button class="sticky-thumb pure-button button-href" ><i class="fa fa-lg fa-thumb-tack %4$s sticky-def %3$s"></i></button>',$row['column'],$row['default_value'],$sticky,$sticky_class);
            }

            $d = sprintf('<div class="%2$s mezok default-input-div pure-u-1-1">%1$s</div>',$field,$hclass);
        }

        $display_row = 'table-row';
        if (in_array('hidden',$api_params)) {
            $display_row = "none";
        }
        // create div line with default option
        // a default _none talán törölhető opció, az api_params jobb kezelés erre....
        $displayed_default_rows = 0;
        $lb = '<br>';
        if($des=='')$lb='';
        $default_values_content .= sprintf('<div style="display:%s"><label class="pure-u-1-1"><span class="field-title">%s</span>%s<span class="colhelptext">%s</span></label>%s</div>',$display_row,$sn,$lb,$des,$d);
        if ($display_row!='none')
           $displayed_default_rows++;
    }
    if ($displayed_default_rows) {
        $modules_div .= "<b>".t(str_default_values).":</b><div class='default_values'>";
        $modules_div .= $default_values_content;
        $modules_div .= "</div>";
    } else {
        $modules_div .= "<div class='default_values'>";
        $modules_div .= $default_values_content;
        $modules_div .= "</div>";
    }

    $modules_div .= "</div>";
    if(isset($_SESSION['upload']['script'])) $modules_div .= $_SESSION['upload']['script'];
}
// *** END DEFAULT VALUES *** \\

//how many columns we have
$dbheader_len = count($dbheader);
$theader_len = count($theader);

# DATA TABLE
$uft_header = array();
$uft_header['columns'] = array();
$uft_header['row1'] = array();

$uft_header['row1'][] = "<div class='th' style='font-size:80%;vertical-align:bottom;position:absolute;background-color:#ddd;z-index:99;height:120px'>
    <button class='pure-button button-gray' style='width:36px;height:25px;color:#666;position:relative;z-index:91' id='deselectskip' title='".str_deselect_all_skips."'><i class='fa fa-lg fa-check-square'></i></button><br>
    <button class='pure-button button-gray' style='width:36px;height:25px;color:#666;position:relative;z-index:91' id='reverseskip' title='".str_reversing_skips."'><i class='fa fa-lg fa-exchange'></i></button><br>
    <button class='pure-button button-gray' style='width:36px;height:25px;color:#666;position:relative;z-index:91' id='hideskippedrows' title='".str_hide_skips."'><i class='fa fa-lg fa-eye-slash'></i></button>
    </div>";

$uft_fill_header = '<div class="emptycell"></div>';
# column names of imported data
if ($table_type=='file') {
    $f = 0;
    foreach($theader as $h) {
        //$fr = '';
        //if ($f == 0) {
            //$fr = "style='margin-left:36px'";
        //}
        if (isset($feed_header[$h])) {
            $uft_header['row1'][] = "<div class='th gpx'><div class='setsize'><span class='colName'>{$feed_header[$h]}</span></div></div>"; // excel feed header
            $uft_header['columns'][] = $feed_header[$h];
        } else {
            $uft_header['row1'][] = "<div class='th gpx'><div class='setsize'><span class='colName'>$h</span></div></div>";
            $uft_header['columns'][] = $h;
        }
        //$f++;
        //$references[] = $h;
    }
    //more dbcol than file col
    for($i=0;$i<($dbheader_len-$theader_len);$i++) {
        $uft_header['row1'][] = "<div class='th gpx'><div class='setsize'><span class='colName'></span></div></div>";
    }
    //$uft_header .= "<th id='nocolumn'><button class='pure-button button-secondary' id='pluscol'><i class='fa fa-plus fa-lg'></i></button></th>";
    //meg lehetne írni a plus col opciót is
} else {
    $letter = 'A';
    foreach($theader as $h) {
        $uft_header['row1'][] = "<div class='th gpx'><div class='setsize'><span class='colName'>$letter</span></div></div>";
        $letter++;
    }
}

# operational control row: drag'n drop cells
$uft_header['row2'] = array();
$uft_header['row2'][] = "<div class='th'><div class='emptycell'></div></div>";

//set header length
$hlen = ($dbheader_len > $theader_len) ? $dbheader_len : $theader_len;

if ($table_type == "file") {


    /* AUTO COLUMN ASSIGN / TEMPLATE COLUMN ASSIGN ... ==========================================================--->
     * auto assign autoassign
     * */
    if (!count($feed_header))
        $feed_header = $theader;
    else
        $feed_header = array_values($feed_header);


    $ref_array = $references;
    array_shift($ref_array);

    $ref_array_keys = array();
    foreach($ref_array as $re) {
        list($c_key,$c_value) = preg_split("/::/",$re);
        $ref_array_keys[] = $c_value;
    }

    for($f=0;$f<count($feed_header);$f++) {
        // special columns - rename input column to help column assignment

        //skip auto letter column names
        if ($f > $uploader_file_header_len)
            continue;

        if (preg_match('/^wkt/i',$feed_header[$f])) {
            $feed_header[$f] = $st_col['GEOM_C'];
        }
        if (preg_match('/^x$/i',$feed_header[$f])) {
            $feed_header[$f] = $st_col['X_C'];
        }
        if (preg_match('/^y$/i',$feed_header[$f])) {
            $feed_header[$f] = $st_col['Y_C'];
        }

        // auto learn assignment
        //      coming from afuncs column_assign
        // user settings...
    }

    $hsim = array();

    $slice_list = array();

    $which_list_element_assigned = [];
    $allmax_perc = array();
    $max_array = array();
    $wlea_filtered = array();

    $positions = array_map(function($el) use ($feed_header) {
        $db_col = preg_split("/::/",$el);

        $sims = array_map(function($upl_col) use ($db_col) {
            similar_text(strtolower($upl_col), strtolower($db_col[0]), $perc_v);
            similar_text(strtolower($upl_col), strtolower($db_col[1]), $perc_k);

            return max([$perc_v,$perc_k]);
        }, $feed_header);

        return (max($sims) > 50) ? array_search(max($sims),$sims) : -1;
    },$ref_array);

    $positions = array_map(null,$positions, $ref_array);

    $which_list_element_assigned = [];

    for ($i=0; $i < $hlen ; $i++) {
        $idx = array_search($i, array_column($positions,0));
        $which_list_element_assigned[] = ($idx !== FALSE) ? $positions[$idx][1] : '' ;
        $wlea_filtered[] = $i;
    }

    // search for better matching of multiple matches
    // ezt azt hiszem megoldottam három sorral a which_list_element_assigned-ben való visszakereséssel...
    /*$wlea_mirror = $which_list_element_assigned;
    $wlea_filtered = array();
    foreach($wlea_mirror as $wlea) {
        if ($wlea=='') continue;

        $wlea_counter = array();
        $key = array_search($wlea, $wlea_mirror);
        while (($key = array_search($wlea, $wlea_mirror)) !== false)
        {
            unset($wlea_mirror[$key]);
            $wlea_counter[] = $key;
        }
        if (count($wlea_counter)>1) {
            // repeat column assign
            $maxw = array();
            foreach($wlea_counter as $w) {
                $maxw[] = $allmax_perc[$w];
            }
            $idx = array_search(max($maxw),$maxw);
            $allmax_perc_idx = $wlea_counter[$idx];
            $wlea_filtered[] = $allmax_perc_idx;

        } else {
            if (isset($wlea_counter[0]))
                $wlea_filtered[] = $wlea_counter[0];
        }
    }
     */

    // TEMPLATE COLUMN ASSIGN
    // overwrite ....
    if (isset($template_column_assign) and $template_column_assign!='') {
        $which_list_element_assigned = json_decode($template_column_assign);
        $wlea_filtered = array();
        for($we = 0;$we<count($which_list_element_assigned); $we++) {
            if ($which_list_element_assigned[$we]!='')
            $wlea_filtered[] = $we;
        }
    }

    //The upload_column_assign should be set only on initial load of this page:
    $set_upload_column_assing = (! isset($_SESSION['upload_column_assign']));

    // push list elements
    for ($i=0;$i<$hlen;$i++) {
        if ($which_list_element_assigned[$i]!='' and array_search($i,$wlea_filtered)!==false) {
            $idxf = array_search($i,$wlea_filtered);
            $idx = $wlea_filtered[$idxf];
            $e = $which_list_element_assigned[$idx];
            $idx = array_search($e,$ref_array);

            if ($idx!==false) {
                $slice_list[] = $idx;

                // dynamic update of auto assigned columns in header
                if ($set_upload_column_assing) {
                    $_SESSION['upload_column_assign'][$i] = $ref_array_keys[$idx];
                }

                if ($ref_array_keys[$idx] == $st_col['GEOM_C']) {
                    $xaa = array_search($ref_array_keys[$idx],$ref_array_keys);
                    if (isset($_SESSION['upload_column_assign'])) {
                        $xma = array_search($st_col['GEOM_C'],$_SESSION['upload_column_assign']);
                        $geometry_column[$i] = $xma;
                    } else {
                        // set autoassign only if manually not changed the position
                        $geometry_column[$i] = $xaa;
                    }
                }
                elseif (in_array($ref_array_keys[$idx],$st_col['DATE_C'])) {
                    $datum_column[$i] = array_search($ref_array_keys[$idx],$ref_array_keys);
                }
                elseif ($ref_array_keys[$idx] == 'obm_files_id') {
                    $attachment_column[$i] = array_search($ref_array_keys[$idx],$ref_array_keys);
                }
                elseif (isset($column_type_array[$ref_array_keys[$idx]])) {

                    $column_type = $column_type_array[$ref_array_keys[$idx]]['type'];
                    if ($column_type=='autocomplete' or $column_type=='autocompletelist' ) {
                        $autocomplete_column[$i] = array_search($ref_array_keys[$idx],$ref_array_keys);
                        $style[$i] .= " genlist";
                        if ($column_type == 'autocompletelist')
                            $style[$i] = "genwlist";
                        $placeholder[$i] = "placeholder='".str_type_to_listing_values."'";

                        $sel = new upload_select_list($column_type_array[$ref_array_keys[$idx]]);
                        $extraparams[$i] = $sel->get_data_attribute();

                    } elseif ($column_type == 'crings') {
                        $colour_ring_column[$i] = array_search($ref_array_keys[$idx],$ref_array_keys);
                    } elseif($column_type == 'list') {
                        $sel = new upload_select_list($column_type_array[$ref_array_keys[$idx]]);
                        $input[$i] = 'select';
                        $options[$i] = $sel->get_options('array');
                        $extraparams[$i] = $sel->get_data_attribute();
                        $style[$i] .= " wauto";
                        if ($sel->multiselect) {
                            $extraparams[$i] .= ' multiple';
                            $style[$i] .= ' fTSelect';
                        }
                        /* MILVUSnál
                         * ...nem használjuk
                         * */
                        //if ($modules->is_enabled('extra_params'))
                        //    $extraparams[$i] = $modules->_include('extra_params','add_params',array($column_type_array[$ref_array_keys[$idx]]['column']));
                    }
                }

                if ($singleRecord) {
                    $ul = '%s';
                    $width = 'singlerecord-th';
                } else {
                    $ul = "<ul class='oList oListElem'>%s</ul>";
                    $width = '';
                }

                if (isset($disable_drag) and $disable_drag == 'oListDisabled')
                    $uft_header['row2'][] = sprintf("<div class='th $width'>$ul</div>",$db_field_list[$idx]);
                else
                    $uft_header['row2'][] = sprintf("<div class='th $width'>$ul<span class='drophere'><i class='fa fa-copy'></i></span></div>",$db_field_list[$idx]);
            }
        } else {
            // not auto assigned columns
            // default behaviour
            if ($singleRecord)
                $uft_header['row2'][] = "";
            else
                $uft_header['row2'][] = sprintf("<div class='th'><ul class='oList oListElem'>%s</ul><span class='drophere'><i class='fa fa-copy'></i></span></div>",'');
        }
    }

    // slice list of auto assigned columns
    foreach($slice_list as $sl) {
        unset($db_field_list[$sl]);
    }
    /* AUTO COLUMN ASSIGN ==========================================================---<
     *
     * */

} else {
    if ($singleRecord) {
        $ul = '%s';
        $width = 'singlerecord-th';
    } else {
        $ul = "<ul class='oList oListElem oListDisabled'>%s</ul>";
        $width = '';
    }

    # operational control row: drag'n drop cells:
    $db_field_list_len = count($db_field_list);
    for ($i=0;$i<$db_field_list_len;$i++) {
        $uft_header['row2'][] = sprintf("<div class='th $width'>$ul</div>",$db_field_list[$i]);
    }
}

# autofill cells
$uft_header['row3'] = array();
$uft_header['row3'][] = '<div class="th"><div class="emptycell"></div></div>';

$function_button = '<span class="filterplus"><i class="fa fa-filter"></i></span><select class="filterplus_chooser" multiple style="display:none;position:absolute;z-index:1001;max-height:100px"></select>';
//$function_button = "";

// autofill header row's cells
for ($i=0;$i<$hlen;$i++) {
    $s = "";
    $input_extension = "";
    if ($max[$i]!='' and $num[$i]=='') $maxlength = "maxlength='{$max[$i]}'";
    else $maxlength = '';

    if ($geometry_column[$i]!=='') {
        $input_extension = sprintf('<button class="geom-down" data-target="header" target="header"><i class="fa fa-map-marker"></i></button>',$i+2);
    } elseif ($datum_column[$i]!=='') {
        // datum cell
        $s = "datepicker";
        $input_extension = sprintf('<button class="datepickerb" data-target="header" target="header"><i class="fa fa-calendar"></i></button>',$i+2);
    } elseif ($colour_ring_column[$i]!=='') {
        // Colour ring cell
        $s = "colourring";
        $input_extension = sprintf('<button class="colourringb" data-target="header" target="header"><i class="fa fa-tags"></i></button>',$i+2);
    } elseif ($autocomplete_column[$i]!=='') {
        $s = "genlist";
    }
    //this will be sent on paging

    if ($table_type == "file") {
        $uft_fill_header .= sprintf('<div class="th" style="position:relative">%1$s<input class="fill %5$s" placeholder="auto fill" %3$s id="f-%2$d">%4$s</div>',$function_button,$i+2,$maxlength,$input_extension,$s);
        $uft_header['row3'][] = sprintf('<div class="th" style="position:relative">%s<input class="fill" placeholder="auto fill" id="f-%d"></div>',$function_button,$i+1);
    } else {
        $uft_header['row3'][] = sprintf('<div class="th" style="position:relative">%1$s<input class="fill %5$s" placeholder="auto fill" %3$s id="f-%2$d">%4$s</div>',$function_button,$i+2,$maxlength,$input_extension,$s);
    }
}

# print out uploaded data as a table

$geom_ref=array();

// initial_n = Kezdeti táblázat hosszúság
$initial_n = 10;
if($fullist and count($fullist_data)) {
    $initial_n = count($fullist_data);
}

// Set table length if load saved import
if (isset($_SESSION['upload']['loadref']) and isset($_SESSION['Tid']) and isset($data_length)) {
    $initial_n = $data_length;
}

if($table_type=='file') {
    $initial_n = count($table);
}

$data_rows = array();
$uft_data = array();
$n = 0;
if (!isset($_SESSION['sheetDataPage_counter']))
    $start = 0;
else
    $start = $_SESSION['sheetDataPage_counter']+1;

//minden egyes sorra
//web  :  1
//file : 20
for ($r=0;$r<$initial_n;$r++) {


    $display_start = $start;
    $n++;
    $i=1;
    if ($show_geom_ref=='' and isset($row['lat']) and isset($row['lon'])) {
        $geom_ref[]=$row['lon'].','.$row['lat'];
    }

    if ($r==0) {
        $autoskip = '';
        $autoskip_value = 0;

    }
    elseif($table_type=='web') {
        $autoskip = 'autoskip';
        $autoskip_value = 1;
    }

    //kill autoskip for restored data
    if (isset($_SESSION['upload']['loadref']) and isset($_SESSION['Tid']) and isset($data_length)) {
        if ($r<$data_length) {
            $autoskip  = '';
            $autoskip_value = 0;
        }
    }

    if($table_type=='web') {
        $display_start = $start+1;
    }
    $row_content = array();
    //data rows: push the first column (skip box, and row number) into row
    $row_content[] = sprintf('<div style="width:3em;display:table;padding:0"><div style="display:table-cell"><input type="checkbox" class="skip checkbox %s" value="%s" name="skipbox" title="Skip the selected rows on auto fill and insert" id="skip-%s" style="vertical-align:middle"></div><div style="display:table-cell;text-align:center"><span style="font-family:monospace;vertical-align:middle;font-size:%s">%s.</span></div></div>',$autoskip,$autoskip_value,$start,'80%',$display_start);

    $start++;

    // minden egyes adat oszlopra
    for ($c=0;$c<$hlen;$c++) {
        $option = '';
        $value='';
        $drop_style = "";
        $input_extension = "";
        $maxlength = '';
        $this_style = $style[$c];
        $this_extraparams = $extraparams[$c];

        if ($table_type=='file' and isset($theader[$c]) and isset($table[$r][$theader[$c]])) {

            if (is_array($table[$r][$theader[$c]]))
                $table[$r][$theader[$c]] = implode(",",$table[$r][$theader[$c]]);

            // ez miért kell????
            //$value = htmlentities($table[$r][$theader[$c]], ENT_QUOTES);
            $value = mb_ereg_replace("'",'&apos;',$table[$r][$theader[$c]]);
            $value = mb_ereg_replace('"','&quot;',$value);

            //$value = $table[$r][$theader[$c]];

        } elseif($table_type=='web') {
            // species name cell
            if ($c===$species_column and count($fullist_data)) {
                $value = $fullist_data[$r];
            }
            // API: set_fields call
            $geometry_column_keys = array_keys(array_filter($geometry_column));
            if (count($geometry_column_keys)) {
                $geom_col_idx = $geometry_column_keys[0];
                if ($c===$geom_col_idx and isset($preSetFields->{'obm_geometry'})) {
                    $value = $preSetFields->{'obm_geometry'};
                }
            }
        }

        // maximum input length check
        if ($max[$c]!=='' and $num[$c]=='')
            $maxlength = "maxlength='{$max[$c]}'";

        // input extensions
        if ($geometry_column[$c]!=='') {
            // geometry cell
            $fa2x = $editRecord ? 'fa-2x' : '';
            $target = $editRecord ? '' : 'data';
            $input_extension = "<button class='geom-down' id='gm_default-obm_geometry' data-target='$target' target='$target'><i class='fa fa-map-marker $fa2x'></i></button>";
        }
        elseif ($attachment_column[$c]!=='') {
            // photo-id cell
            $fa2x = $editRecord ? 'fa-2x' : '';
            $target = $editRecord ? '' : 'data';
            $input_extension = "<button class='photo-here' id='gm_default-obm_files_id' data-target='$target' target='$target'><i class='fa fa-paperclip $fa2x'></i></button>";
        }
        elseif ($datum_column[$c]!=='') {
            //datum cell
            $fa2x = $editRecord ? 'fa-2x' : '';
            $input_extension = "<button class='datepickerb' data-target='data' target='data'><i class='fa fa-calendar $fa2x'></i></button>";
            $this_style .= " datepicker";
        }
        elseif ($colour_ring_column[$c]!=='') {
            // ringer cell
            $fa2x = $editRecord ? 'fa-2x' : '';
            $input_extension = "<button class='colourringb' data-target='data' target='data'><i class='fa fa-tags $fa2x'></i></button>";
            $this_style .= " colourring";
        } elseif ($autocomplete_column[$c]!=='') {
            // nothing to do here
            // $style[$c] already set
            // $placeholder[$c] already set
        }

        // assemble input
        if ($input[$c]=='select') $perselect = "</select>";
        else $perselect = '';

        // data attributes


        // put the file cell value at the first position of options
        if ($table_type=='file' and $input[$c]=='select') {
            $option = "";
            $this_style .= " list-ok";
            $s = 0;
            for($i=0;$i<count($options[$c]);$i++) {
                $selected = '';
                $dataAttr = '';
                if (isset($options[$c][$i]['data']) && !empty($options[$c][$i]['data']))
                    foreach ($options[$c][$i]['data'] as $dataKey => $dataValue)
                        $dataAttr .= "data-$dataKey=\"$dataValue\" ";

                // processing multiselect values
                if (preg_match('/multiple/',$this_extraparams)) {
                    //$split_values = preg_split(",",$value);
                    if (in_array($options[$c][$i]['value'],preg_split("/,/",$value))) {
                        $selected = "selected";
                        $s++;
                    }
                } else {
                    if ($options[$c][$i]['value'] === $value ) {
                        $selected = 'selected';
                        $s++;
                    }
                }
                $option .= "<option $selected class='list-ok' value='{$options[$c][$i]['value']}' {$options[$c][$i]['disabled']} $dataAttr>{$options[$c][$i]['label'][0]}</option>";
            }
            if (!$s) {
                // nem volt egyezés egyik értékkel sem, megnézzük a címkékkel:
                $option = "";
                $this_style .= " list-ok";
                $s = 0;
                for($i=0;$i<count($options[$c]);$i++) {
                    $selected = '';
                    $dataAttr = '';
                    if (isset($options[$c][$i]['data']) && !empty($options[$c][$i]['data']))
                        foreach ($options[$c][$i]['data'] as $dataKey => $dataValue)
                            $dataAttr .= "data-$dataKey=\"$dataValue\" ";
                    if (mb_in_array($value,$options[$c][$i]['label']) ) {
                        $selected = 'selected';
                        $s++;
                    }
                    $option .= "<option $selected class='list-ok' value='{$options[$c][$i]['value']}' {$options[$c][$i]['disabled']} $dataAttr>{$options[$c][$i]['label'][0]}</option>";
                }

                if (!$s) {
                    $option = "<option class='list-warning'>$value</option>".$option;
                    $this_style .= " list-warning";
                }
            }
        } elseif ($input[$c]=='select') {

            for($i=0;$i<count($options[$c]);$i++) {
                $selected = '';
                if ($options[$c][$i]['value'] === $value ) {
                    $selected = 'selected';
                }
                // background image
                $bg_image = '';
                if (isset($options[$c][$i]['bgImage']) and $options[$c][$i]['bgImage']!='') {
                    $bg_image = 'background-image:url('.$options[$c][$i]['bgImage'].');background-repeat:no-repeat;padding-left:16px';
                }
                // ezt mi használja??
                $dataAttr = '';
                if (isset($options[$c][$i]['data']) && !empty($options[$c][$i]['data']))
                    foreach ($options[$c][$i]['data'] as $dataKey => $dataValue) {
                        $dataAttr .= "data-$dataKey=\"$dataValue\" ";
                    }

                $option .= "<option $selected value='{$options[$c][$i]['value']}' {$options[$c][$i]['selected']} {$options[$c][$i]['disabled']} $dataAttr style='$bg_image'>{$options[$c][$i]['label'][0]}</option>";
            }
        } else {
            $option = "";
        }

        if ($singleRecord) {
            $this_style .= " default ci-default singlerecord-input";
            $this_extraparams .= "id='default-{$uft_header['columns'][$c]}'";
        } else {
            $this_style .= " ci";
        }
        //$field_input = sprintf('<%1$s type="%10$s" %3$s class="cf-%2$d %4$s %8$s" %5$s %6$s %9$s>%7$s',$input[$c],$c+2,$extraparams[$c],$style[$c],"value='$value'",$maxlength,$option.$perselect,$drop_style,$placeholder[$c],$html_type[$c]);
        $field_input = sprintf('<%1$s %3$s class="cf-%2$d %4$s %8$s" %5$s %6$s %9$s>%7$s',$input[$c],$c+2,$this_extraparams,$this_style,"value='$value'",$maxlength,$option.$perselect,$drop_style,$placeholder[$c]);

        // assembe table cell
        //$field_frame = sprintf('<div class="td" style="position:relative">%s%s</div>',$field_input,$input_extension);
        $field_frame = sprintf('%s%s',$field_input,$input_extension);

        $row_content[] = $field_frame;
        $i++;
    }

    $data_rows[] = sprintf('<div class="tr" id="tr-%s">%s</div>',$n,implode(array_map('put_td',$row_content)));
    $uft_data[$n] = $row_content;  //sprintf('<div class="tr" id="tr-%s">%s</div>',$n,$row_content);
}

if ($show_geom_ref==''){
    $show_geom_ref = json_encode($geom_ref);
}

// array_map function
function put_td($value) {
    return sprintf('<div class="td" style="position:relative">%s</div>',$value);
}

$uft_fill_data = implode($data_rows);

// we print it at the first time - in paging we exit here!
if (isset($only_uft)) {
    echo json_encode(array('body'=>$uft_fill_data,'header'=>$uft_fill_header));
    $_SESSION['upload_ajax_wait'] = 0;
    exit;
}

/* **********************************************************************************************
 *
 * output printing
 * only  when we load the page or reload, not on ajax call - paging
 *
 *
 * **********************************************************************************************/

$left_table = "<div id='leftTable' style='display:none'></div>";

# left side: list of database columns
if ( $table_type=='file' ) {
    $leftTablestyle = "";
    if ($editRecord) $leftTablestyle = 'display:none';
    if(!count($db_field_list)) $leftTablestyle = 'display:none';
    $left_table = sprintf("<div id='leftTable' style='$leftTablestyle'><legend class='upll'>".t(str_database)."&nbsp;".str_columns."</legend><div id='leftTable_in'><ul id='sList' class='sList'>%s</ul></div></div>",implode("",$db_field_list));

}

# right side: list of imported data rows
if ($table_type=='file' and isset($_SESSION['sheetDataPage'])) {
    $sDP = t(str_data).": ".$_SESSION['sheetDataPage']." ".str_rows." $sheets";
} elseif ($table_type=='file') {
    $sDP = t(str_data).": ".$_SESSION['perpage']." ".str_rows." $sheets";
} else {
    $sDP = t(str_data);
}
## excel sheets
if (isset($_SESSION['upload_file']['sheetList'])) {
    $selected = $_SESSION['upload_file']['sheetList'][0];
    if( isset($_SESSION['upload_file']['activeSheet']) )
        $selected = $_SESSION['upload_file']['activeSheet'];

    $options = selected_option($_SESSION['upload_file']['sheetList'],$selected);
    $exs = "<div style='padding:2px 0 2px 10px;background-color:#cacaca'><b>".t(str_sheets).":</b> <select id='change_exs_sheet'>$options</select></div>";
} else
    $exs = "<input type='hidden' id='change_exs_sheet'>";

if ($non_default_values_count == 0)
    $table_display = 'none';
else
    $table_display = 'block';

$right_table  = "
    <div class='rightTable_container' style='display:$table_display'><legend class='upll'>$sDP</legend>";

if (!$singleRecord)
$right_table  .= "
    <button title='".str_functions."' id='fillfunction' class='pure-button button-gray'><i class='fa fa-lg fa-flask'></i></button>
    <select id='fillfunction_chooser' size=8>
        <option value='`apply:days.since.1899-12-30'>".str_excel_days."</option>
        <option value='`apply:seconds.since.00-00-00'>".str_excel_time."</option>
        <option value='`apply:[]math.avg'>".str_mean."</option>
        <option value='`apply:[]math.sum'>".str_sum."</option>
        <option value='`apply:increment'>".str_increment."</option>
        <option value='`apply:[]replace.all:///'>".str_replace_all."</option>
        <option value='`apply:replace.match:///'>".str_replace_match."</option>
        <option value='`apply:groupped.fill'>".str_groupped_fill."</option>
        <option value='`apply:days.since.1900-01-01'>".str_nonexcel_days."</option>
        <option value=''>--</option>
    </select>
    <div id='longin_container'>
        <textarea id='longin' class='longin'></textarea>
        <div class='longin' id='longin-function-editor'> &nbsp;
            <div style='display:inline-block'>".str_function.":
                <select id='obfun-name'>
                    <option value='days.since.1899-12-30'>".str_excel_days."</option>
                    <option value='seconds.since.00-00-00'>".str_excel_time."</option>
                    <option value='math.avg'>".str_mean."</option>
                    <option value='math.sum'>".str_sum."</option>
                    <option value='increment'>".str_increment."</option>
                    <option value='replace.all'>".str_replace_all."</option>
                    <option value='replace.match'>".str_replace_match."</option>
                    <option value='groupped.fill'>".str_groupped_fill."</option>
                    <option value='days.since.1900-01-01'>".str_nonexcel_days."</option>
                    </select> ".str_column.":
                <select id='obfun-on' class=''>".selected_option($references)."</select>
            </div>
            <div style='position:relative;display:inline-block'><span class='obfun-references'> ".str_other_column.": </span>
                <select id='obfun-references' class='obfun-references' style='vertical-align:middle' disabled multiple>".selected_option($references)."</select>
            </div>
            <div style='position:relative;display:inline-block'><span class='obfun-params'> ".str_parameters.": </span><input id='obfun-params' class='obfun-params'></div>
            <button title='".str_apply."' class='pure-button button-secondary' id='apply'><i class='fa fa-lg fa-flask'></i></button>
        </div>
        <button title='".str_close."' class='pure-button button-gray' id='obfun-close'><i class='fa fa-close'></i></button><button title='".str_write_back."' class='pure-button button-secondary' id='fillit'><i class='fa fa-pencil fa-lg'></i></button>
    </div>"; // longin_container

$right_table .= "<div id='rightTable'>";

if ($editRecord)
    $right_table .= "<div class='mezok upload-data-table-singlerecord' id='upload-data-table'>";
else
    $right_table .= "<div class='mezok' id='upload-data-table'>";

if ($editRecord) {
    for ($i=0; $i<count($uft_data[1]);$i++) {
        if ($i==0) continue;
        if ($uft_header['row2'][$i]!='')
            $right_table .=  "<div class='singlerecord-tr'><div class='tr'>".$uft_header['row2'][$i]."</div><div style='position:relative'>{$uft_data[1][$i]}</div></div>";
    }

} else {

    $right_table .= "<div class='thead'>";
    foreach ($uft_header as $key=>$uft_row) {
        if ($key=='columns') continue;
        $right_table .=  "<div class='tr'>".implode($uft_row)."</div>";
    }
    $right_table .= "</div>"; // thead

    $right_table .= "<div class='tbody'>";
    //put the data content
    foreach ($uft_data as $key=>$uft_row) {
        $right_table .=  "<div class='tr' id='tr-$key'>".implode(array_map('put_td',$uft_row))."</div>";
    }
    $right_table .= "</div>"; // tbody
}
$right_table .= "</div>"; // upload-data-table

$right_table .= "</div>"; // rightTable

$right_table .= "<div style='display:table-row;'>";

if (!$editRecord) {
    $right_table .= "<button class='pure-button button-secondary' id='plusclick'><i class='fa fa-plus fa-lg'></i></button> <input id='plusrow' value='1' style='padding:6px;border:1px solid gray;border-radius:4px;width:3em;vertical-align:bottom'>".str_row." &nbsp; &nbsp;";
    $right_table .= "<button id='text-size-increase' title='".str_increase_text_size."' class='pure-button button-gray'><i class='fa fa-font fa-fw fa-lg'></i></button><button id='text-size-decrease' title='".str_decrease_text_size."' class='pure-button button-gray'><i class='fa fa-font fa-fw'></i></button>";
    $right_table .= " <button class='pure-button button-success' id='pasteTypeToggle-button'><input type='checkbox' id='pasteTypeToggle' class='hidden'><i class='fa fa-toggle-off fa-lg'></i> CTRL+V multicell</button>";
}
$right_table .= "</div>";
$right_table .= "</div>"; # rightTable_container

# main table
$main_table = "<div id='upload-form-maintable'>";
$main_table .= $gopt_window;
$main_table .= $left_table;
$main_table .= $right_table;

#$main_table .= "<div style='height:800px'></div>";
$main_table .= "</div>$exs";

/*
 *
 * START printing output here
 *
 *
 * */
//display form name and description
//echo $form_title;
/*$def_timeout = ini_get('session.gc_maxlifetime');
echo '<script>
    var timeoutHandle = null;
    function startTimer(timeoutCount) {
        if (timeoutCount > 0) {
            document.getElementById("sessionTimer").innerHTML = (timeoutCount * 1000)/1000 + " '.str_session_timeout.'";
            timeoutHandle = setTimeout(function () { startTimer(timeoutCount-1);}, "1000");
        } else if (timeoutCount == 0) {
            document.getElementById("sessionTimer").innerHTML = "'.str_session_expired.'" ;
        }
    }
    function refreshTimer() {
        clearTimeout(timeoutHandle);
        startTimer(3600);
    }
$(document).ready(function() {
    startTimer('.$def_timeout.');
});
</script><div style="position:absolute;right:10px;top:90px;color:#888"><span id="sessionTimer"></span></div>';
 */
//javascript block
?>
<?php if (isset($preSetFields->submit) && $preSetFields->submit === true) : ?>
<script>
$(document).ready(function() {
    $("#send").trigger('click');
});
</script>
<?php endif; ?>
<script>
$(document).ready(function() {

    // change ctrl+s to our save sheet
    $("body").keydown(function(e) {
        //if (ctrlDown && (e.keyCode == sKey)) {
        if (e.keyCode == 114) {
            save_sheet();
            return false;
        }
    });
    // two minutes - auto save
    window.setInterval(function(){
        save_sheet(false);
    }, 120000);

<?php
/*
 *    On the fly warnings
 *
 *
 *    for ($i=0;$i<count($datum);$i++) {
        if ($datum[$i]!='') {

            printf("$('.mezok').on('keyup','.cf-%d',function(e){
            var val = $(this).val();
            if (val.match(/[0-9]{2,4}.[0-9]{2}.[0-9]{2,4}/)) $(this).removeClass('redbg');
            else { $(this).addClass('redbg');}
            });",$i+1,$datum[$i]);
        }
    }
    for ($i=0;$i<count($num);$i++) {
        if ($num[$i]!='') {

            printf("$('.mezok').on('keyup','.cf-%d',function(e){
            var val = $(this).val();
            if (!isNumeric(val)) $(this).addClass('redbg');
            else { $(this).removeClass('redbg');}
            });",$i+1,$num[$i]);
        }
    }
    for ($i=0;$i<count($wkt);$i++) {
        if ($wkt[$i]!='') {
            printf("$('.mezok').on('focusout','.cf-%d',function(e){
                var v = $(this);
                tmp = parseWKTStrings(v.val());
                if (tmp.length) {
                    var reader = new jsts.io.WKTReader();
                    var input = reader.read(v.val());
                    if(input.isValid()) v.removeClass('redbg');
                    else v.addClass('redbg');
                } else v.addClass('redbg');
            });",$i+1,$wkt[$i]);
        }
    }
    for ($i=0;$i<count($wkt_line);$i++) {
        if ($wkt_line[$i]!='') {
            printf("$('.mezok').on('focusout','.cf-%d',function(e){
                var v = $(this);
                tmp = parseWKTStrings(v.val(),['linestring']);
                if (tmp.length) {
                    var reader = new jsts.io.WKTReader();
                    var input = reader.read(v.val());
                    if(input.isValid()) v.removeClass('redbg');
                    else v.addClass('redbg');
                } else v.addClass('redbg');

            });",$i+1,$wkt_line[$i]);
        }
    }
    for ($i=0;$i<count($min);$i++) {
        if ($min[$i]!='') {

            printf("$('.mezok').on('keyup','.cf-%d',function(e){
            var len = $(this).val().length;
            if (len<%d) $(this).addClass('redbg');
            else {
                $(this).removeClass('redbg');
            }
            });",$i+1,$min[$i]);
        }
    }
    for ($i=0;$i<count($exa);$i++) {
        if ($exa[$i]!='') {

            printf("$('.mezok').on('keyup','.cf-%d',function(e){
            var len = $(this).val().length;
            if (len!=%d) $(this).addClass('redbg');
            else {
                $(this).removeClass('redbg');
            }
            });",$i+1,$exa[$i]);
        }
    }
 */
?>
});
</script>
<?php

$training = false;
if (defined('TRAINING') and constant('TRAINING')===true)
    $training = true;

if ($training) {
    // check form is training form
    $cmd = sprintf("SELECT 1 FROM form_training WHERE form_id=%d",$insert_allowed);
    $res = pg_query($BID,$cmd);
    if (!pg_num_rows($res)) {
        $training = false;
        // check user passed the trainings?
        $cmd = sprintf("SELECT form_id FROM form_training WHERE project_table='%s'",PROJECTTABLE);
        $res = pg_query($BID,$cmd);
        $training_forms = array();
        while ($row = pg_fetch_assoc($res)) {
            $training_forms[$row['form_id']] = 0;
            $cmd = sprintf("SELECT validation FROM system.uploadings WHERE form_id=%d AND project_table='%s' AND uploader_id=%d",$row['form_id'],PROJECTTABLE,$_SESSION['Trole_id']);
            $res2 = pg_query($ID,$cmd);
            while ($row2 = pg_fetch_assoc($res2)) {
                if ($row2['validation']>0.6)
                    $training_forms[$row['form_id']] = 1;
            }
        }
        if (array_sum($training_forms) < count($training_forms)) {
            //"Nincs minden gyakorló feladat még megoldva!";
            echo "<h1>Mielőtt nekiállnál adatot feltölteni, old meg a gyakorló feladatokat!</h1>";
            $insert_allowed = 0;
            $training = true;
        }

    } else {
        // It is a training form
        // Which form will coming?
        $cmd = sprintf("SELECT form_id FROM form_training WHERE project_table='%s' ORDER BY qorder",PROJECTTABLE);
        $res = pg_query($BID,$cmd);
        $training_forms = array();
        while ($row = pg_fetch_assoc($res)) {
            $training_forms[$row['form_id']] = 0;
            $cmd = sprintf("SELECT validation FROM system.uploadings WHERE form_id=%d AND project_table='%s' AND uploader_id=%d",$row['form_id'],PROJECTTABLE,$_SESSION['Trole_id']);
            $res2 = pg_query($ID,$cmd);
            while ($row2 = pg_fetch_assoc($res2)) {
                if ($row2['validation']>0.6)
                    $training_forms[$row['form_id']] = 1;
            }
            if ($row['form_id'] == $insert_allowed)
               break;
            if ($training_forms[$row['form_id']]==0) {
                $training = true;
                $insert_allowed = 0;
                $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id=%d",$row['form_id']);
                $res3 = pg_query($BID,$cmd);
                $row3 = pg_fetch_assoc($res3);

                echo "<p style='padding:30px;font-size:150%'>Ezt a feladatot még nem teljesítheted, lépj a ";
                echo "<a href='$protocol://".URL."/upload/?form=".$row['form_id']."&type=web'>".$row3['form_name']."</a>  küldetésre!</p>";
                break;
            }
        }
    }


    // check current form already used, uploded data is validated
    $accepted = -1;
    $cmd = sprintf("SELECT id,validation FROM system.uploadings WHERE uploader_id=%d AND project='%s' AND form_id=%d",$_SESSION['Trole_id'],PROJECTTABLE,$insert_allowed);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        // training data already sent
        while( $row = pg_fetch_assoc($res)) {

            if ($row['validation']>0.6) {
                // sikeresen teljesítetted a küldetést, lépj a következő szintre!
                $accepted = 2;
                break;
            } elseif ($row['validation']=='') {
                // várj még az ellenőrzésre....
                if ($accepted == -1)
                    $accepted = 1;
            } else {
                // ez sajna nem sikerült, még gyakorolnod kell
                //log_action('not accepted, repeat it');
                $accepted = 0;
            }
        }
    }

    if ($insert_allowed and $accepted == 2) {
        $cmd = sprintf('SELECT form_id FROM form_training WHERE project_table=\'%1$s\' AND enabled=true AND qorder=(SELECT qorder FROM form_training WHERE form_id=%2$d AND project_table=\'%1$s\')+1',PROJECTTABLE,$insert_allowed);
        $res = pg_query($BID,$cmd);
        $row = pg_fetch_assoc($res);
        if (pg_num_rows($res)) {
            echo "<h1>Ez a küldetés sikerült, lépj a következő szintre!</h1>";
            echo "<p style='padding-left:90px;font-size:150%'><a href='$protocol://".URL."/upload/?form=".$row['form_id']."&type=web'>Következő küldetés</a></p>";
        } else
            echo "<p style='padding:30px;font-size:150%'><h1>Gratulálunk, teljesítetted az összes küldetést!</h1></p>";


    } elseif ($insert_allowed and $accepted == 1) {
        echo "<h1>A küldetésed még nincs ellnőrizve, akár meg is ismételheted addig még egyszer!</h1>";
    } elseif ($insert_allowed and $accepted == 0) {
        echo "<h1>A küldetésed nem sikerült, ismételd meg még egyszer!</h1>";
    }
    // egy formhoz egy kérdés max!!!
    $cmd = sprintf("SELECT * FROM form_training WHERE project_table='%s' AND form_id=%d AND enabled=true ORDER BY qorder LIMIT 1",PROJECTTABLE,$insert_allowed);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res) and $accepted!=2 and $insert_allowed) {
        echo "<input type='hidden' id='training_done_url' value='$protocol://".URL."/upload/?form=$insert_allowed&type=web&answered=1'>";

        $row = pg_fetch_assoc($res);
        echo "<div style='padding:30px'>";

        if (!isset($_GET['answered'])) {

            echo "<h2>".$row['qorder'].") ".$row["title"]."</h2>";
            echo "<div style='max-width:800px;font-size:120%'>".$row['html']."</div>";

            $cmd = sprintf("SELECT * FROM form_training_questions WHERE training_id=%d ORDER BY qid",$row['id']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                echo "<br><h3>Ellenőrző kérdések</h3>";
            }

            echo "<div style='max-width:600px;font-size:120%'><form id='training-form'><ol>";

            while ($row2 = pg_fetch_assoc($res)) {
                echo "<li style='padding-bottom:20px'><div id='a".$row2['qid']."' >".$row2['caption']."</div>";
                $j = json_decode($row2['answers'],true);
                if (is_array($j) and count($j)) {
                    echo "<ul style='list-style-type:none'>";
                    foreach ($j as $answer) {
                        if ($row2['qtype']=='singleselect') $input = "<input type='radio' class='training-answer' data-isRight='".$answer['isRight']."' data-name='a".$row2['qid']."' name='a".$row2['qid']."'>";
                        elseif ($row2['qtype']=='multiselect') $input = "<input type='checkbox'  class='training-answer' data-isRight='".$answer['isRight']."' data-name='a".$row2['qid']."' name='a".$row2['qid']."[]'>";
                        echo "<li>$input ".$answer['Answer']."</li>";
                    }
                    echo "</ul>";
                }
                echo "</li>";
            }
            echo "</ol>";

            echo "<button id='post_answers' class='pure-button button-secondary'>".str_send."</button>";
            echo "</form></div>";
        } else {

            echo "<h3>".$row['qorder'].") ".$row["title"]."</h3>";
            if ($row['task_description']!='') {
                echo "<br><h3>Gyakorlati feladat</h3>";
                echo "<div style='max-width:600px;font-size:120%'>";
                echo $row['task_description'];
                echo "</div>";
            }
            $training = false;
        }

        echo "</div>";

    } elseif ($accepted == -1 and !$insert_allowed) {
        // echo "<h1>Mielőtt nekiállnál adatot feltölteni, kérlek vegyél részt egy kis felkészítő játékban...</h1>";
        //$training = false;
    }
}

if (!$training) {
    //state mentéshez kell: form_id, form_type
    echo "<input type='hidden' name='selected_form_id' id='sfid' value='$insert_allowed'>";
    echo "<input type='hidden' name='selected_form_type' id='sftype' value='$selected_form_type'>";

    // form description
    if ($form_text != '') $form_text = "<p id='upload-form-description'>$form_text</p>";

    $upload_form_container_singlerecord = $singleRecord ? 'upload-form-container-singlerecord' : '';
    echo "<div id='upload-form-container' class='$upload_form_container_singlerecord'>$form_text";

    //table
    echo "<div id='upload-form-form'>";

    //include modules: e.g.: hidden fired divs such like photo div or visible divs like default_values
    echo $modules_div;

    // print out main upload table
    echo $main_table;


    echo "</div>"; // upload-form-form

    echo "<div id='upload-form-footer'>"; // upload-form-footer
    echo "<div id='form-settings'>";

    if ($have_geometry) {
        echo t(str_coordsys)." (<a href='http://spatialreference.org/' target='_blank'>epsg srid</a>): ";

        if (count($srid_options))
            echo sprintf("<select id='upl-srid' required>%s</select>",implode('',$srid_options));
        else
            echo "<input id='upl-srid' value='4326' placeholder='Requires an srid' required>";

        echo " &nbsp; <a href='geomtest' target=_blank id='showon' title='".str_if_defined_xy."'><i class='fa fa-globe fa-lg'></i> ".t(str_view)." ".str_onthemap."</a>";
    }

    echo "<br>".t(str_upload).' '.str_comment.":<br><textarea id='upl-desc' style='height:30px;padding:4px' class='resize-on-focus pure-u-1-3'>$upload_comment</textarea>";
    echo "</div>"; // form-settings

    // gombok
    echo "<div id='form-buttons'>";

    if (($modules->is_enabled('massive_edit') and isset($_GET['massiveedit'])) or $editRecord) {
        echo "<button id='send' name='savedata' class='upload_fin_button pure-button button-success button-xlarge pure-u-1-2'><i class='fa fa-refresh'></i> ".t(str_update)."</button>";
        echo " <button id='cancel_massiveedit' class='upload_fin_button pure-button button-secondary button-xlarge pure-u-1-2'><i class='fa'></i> ".t(str_cancel)."</button>";
    }
    else
        echo "<button id='send' name='savedata' class='upload_fin_button pure-button button-success button-xlarge pure-u-1-3'><i class='fa fa-upload'></i> ".t(str_import_data)."</button>";

    //echo "<button id='new' name='newdata' class='pure-button button-success button-xlarge' style='display:none'><i class='fa fa-upload'></i> ".t(str_new)."</button>";
    echo "<br><div id='dbresp' class='pure-u-2-3'></div>";
    echo "</div>"; // form-buttons

    if ($insert_allowed>0) {
        echo "<input type='hidden' id='form_page_id' value='$selected_form_id'>";
        echo "<input type='hidden' id='form_page_type' value='$selected_form_type'>";
    }
    echo "</div>"; // upload-form-footer
    echo "</div>"; // upload-form-container
}

?>
