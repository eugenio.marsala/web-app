<?php
/* Ez a fájl a felhasználói profil lapon a projekt admin funkciókat tartalmazza
 * Előbb vannak a gombok definíciói, ami egy-egy admin lapot hoz be. Ezek a lapok
 * is ebben a fájlban vannak definiálva.
 *
 *
 *
 * */
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

if (isset($_SESSION['token']) and $_SESSION['token']['projecttable']!=PROJECTTABLE) {
    echo "Stranger database connection.";
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

if (!isset($_SESSION['st_col']))
    st_col('default_table');

/* Update Login vars - refresh auth tokens */
if (isset($_COOKIE['access_token'])) {
    $cookie = json_decode( $_COOKIE[ "access_token" ] );

    $res = true;

    if (time() > $cookie->expiry) {
        // expired
        $res = refresh_token();
        $cookie = json_decode( $_COOKIE[ "access_token" ] );
    }

    if ($res) {
        // JUST check token valisity by a resource request
        $result = oauth_webprofile_request($cookie->data->access_token);

        if ($result === FALSE) {
            /* Handle error */
            log_action("Ouath resource request failed ({$cookie->data->access_token}):",__FILE__,__LINE__);
            setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
            unset($_COOKIE['access_token']);
        }
        else {
            if (!isset($_SESSION['Tid'])) {
                if(!oauth_login($cookie->data->access_token)) {
                    setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
                    unset($_COOKIE['access_token']);
                }
                // restore previous SESSION variables somehow? I think it is too expensive to save logined SESSION variables in the database...
            }
        }
    }
} elseif (isset($_COOKIE['refresh_token'])) {
    $cookie = json_decode( $_COOKIE[ "refresh_token" ] );

    if (time() < $cookie->expiry) {
        $res = refresh_token();
        if ($res!==false) {
            $cookie = json_decode( $_COOKIE[ "access_token" ] );
            oauth_login($cookie->data->access_token);
        }
    }
}

//only for logined users
if(!isset($_SESSION['Tid'])) {
    echo "Session expired.";
    include(getenv('PROJECT_DIR').'includes/logout.php');
    exit;
}

$modules = new modules();

// project roles into array
$cmd = "SELECT role_id,description,ARRAY_TO_STRING(container,',') AS roles,user_id FROM project_roles WHERE project_table='".PROJECTTABLE."' ORDER BY description";
$res = pg_query($BID,$cmd);
$csor = pg_fetch_all($res);

if (!isset($_GET['options'])) {
    echo "Project admin page";

} elseif(isset($_GET['options']) and $_GET['options']=='files') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* Fájl kezelő - file manager */
    echo "<h2>".str_file_manager."</h2><div class='be'>";
    $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);

    // which table?
    if (isset( $_GET['choose'])) $choose =  $_GET['choose'];
    else $choose = PROJECTTABLE;

    //set choose table available for ajax file coment save option
    $_SESSION['selected_data-table'] = $choose;

    $options = array();
    while($row = pg_fetch_assoc($res)) {
        $s = "";
        if ($row['m'] == $choose) $s = "selected";
        $options[] = "<option $s>{$row['m']}</option>";
    }
    $list = sprintf("<select id='file_manager_choose_table'><option></option>%s</select>",implode('',$options));
    echo $list;

    $p = array();
    if (is_dir(getenv('PROJECT_DIR').'attached_files')){

        $dir = opendir(getenv('PROJECT_DIR').'attached_files');
        $files = array();
        while (false !== ($fname = readdir($dir)))
        {
            if (is_file(getenv('PROJECT_DIR').'attached_files/'.$fname))
            {
                $files[] = $fname;
            }
        }

        //which columns?
        //only one allowed per table
        /*$file_conns = explode(',',$_SESSION['st_col']['FILE_ID_C']);
        $connect = '';
        foreach ($file_conns as $fc) {
            if ($choose==$fc) {
                $connect = $fc;
                break;
            }
        }*/
        $connect = 'obm_files_id';
        if ($connect!='') {
            $cmd = '';
            if ($choose == '') {
                    /*$cmd = sprintf('SELECT array_to_string(array_agg(fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(pk.%2$s),\',\') as obm_id,f.datum
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                        LEFT JOIN %4$s pk ON (pk.%2$s=k.%3$s)
                        WHERE f.data_table=\'%1$s\' AND f.project_table=\'%4$s\'
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.reference',$choose,'obm_id',$choose,PROJECTTABLE);*/
                    /*$cmd = sprintf('SELECT array_to_string(array_agg(fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                        WHERE f.data_table=\'%1$s\' AND f.project_table=\'%1$s\'
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.reference',$choose,'obm_id');*/


                //} else {
                //debug(12);
                    // no table selected
                    //echo "No proper interconnect col defined to $choose table.";
                    $cmd = sprintf('SELECT array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,f.comment,f.datum,f.data_table
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        WHERE f.project_table=\'%1$s\'
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.data_table,f.datum',PROJECTTABLE);

                //}
            } else {
                $cmd = sprintf('SELECT f.data_table,array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                        WHERE f.project_table=\'%3$s\' AND (k.obm_files_id IS NOT NULL OR
                               (k.obm_files_id IS NULL AND f.data_table=\'%1$s\') )
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.datum',$choose,'obm_id',PROJECTTABLE);
            }
            $res = pg_query($ID,$cmd);

            if (pg_last_error($ID)) {
                //echo str_db_error_photo_id_column;
                echo "The file connect columns not set properly. Check in 'database columns' option.";
            } else {
                echo "<ul class='filemanager'><li><div style='min-width:60px;font-weight:bold'>icon</div><div style='min-width:500px;font-weight:bold'>filename</div><div style='font-weight:bold'>".str_comment."</div><div style='font-weight:bold'>references</div><div style='font-weight:bold'>".str_update."</div><div style='font-weight:bold'>".str_delete."</div></li>";
                $n=0;
                while ($row = pg_fetch_assoc($res)) {
                    $key = array_search($row['reference'],$files);
                    if ($key!==false) {
                        $fname = $files[$key];
                        unset($files[$key]);

                        if (isset($row['obm_id'])) {
                            $gids = explode(',',$row['obm_id']);
                            sort($gids);
                            $gids = implode(', ',$gids);
                            $gids_input = sprintf('<input class="data_link" id="origgids%1$d_%2$s" value="%3$s">',$n,$row['obm_id'],$gids);
                            $action = 'save_connection';
                        } else {
                            $gids_input = sprintf('<input class="table_link" id="origtable%1$d_%2$s" value="%2$s">',$n,$row['data_table']);
                            $action = 'update_table';
                        }

                        $comment = $row['comment'];


                        if ($thumb = mkThumb($row['reference'],60)) {
                            // image attachment
                            $thf = "http://".URL."/getphoto?ref=/thumbnails/{$row['reference']}";
                            $p[] = "<li style='background-color:white'>
                                        <div><a href='http://".URL."/getphoto?c={$row['conid']}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$thf' title='{$row['comment']}' class='thumb'></a></div>
                                        <div>$fname<br><span style='font-size:80%'>{$row['datum']}</span></div>
                                        <div>$comment</div>
                                        <div>$gids_input</div>
                                        <div><button id='file_{$row['id']}' class='$action pure-button button-success'>".str_save."</button></div>
                                        <div><button id='file_{$row['id']}' class='delete_file pure-button button-warning' disabled>".str_delete."</button></div>

                                        </li>";
                        } else {
                            // non-image attachment
                            $mime_url = mime_icon($fname,32);
                            $p[] = "<li style='background-color:white'>
                                        <div><a href='http://".URL."/getphoto?ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$mime_url' title='{$row['comment']}' class='thumb'></a></div>
                                        <div>$fname<br><span style='font-size:80%'>{$row['datum']}</span></div>
                                        <div>$comment</div>
                                        <div>$gids_input</div>
                                        <div><button id='file_{$row['id']}' class='$action pure-button button-success'>".str_save."</button></div>
                                        <div><button id='file_{$row['id']}' class='delete_file pure-button button-warning' disabled>".str_delete."</button></div>
                                        </li>";
                        }
                    }
                    $n++;
                }
            }
            $e = implode(" ",$p);

            //nincs db bejegyzés
            if ($choose == '') {
                $t = 0;
                foreach($files as $fname) {
                    $mime_url = mime_icon($fname,32);
                    $p[] = "<li style='background-color:orange'>
                            <div><a href='http://".URL."/getphoto?ref=$fname' id='gfnodb_$t' class='photolink' target='_blank'><img src='$mime_url' class='thumb'></a></div>
                            <div>$fname</div>
                            <div></div>
                            <div><input class='table_link' id='newtable$t' value=''></div>
                            <div><button id='file_$t' data-attr='$fname' class='add_file pure-button button-success'>".str_save."</button></div>
                            <div><button id='file_$t' data-attr='$fname' class='delete_file pure-button button-warning' disabled>".str_delete."</button></div></li>";
                }
                $e = implode(" ",$p);
                $t++;
            }
            echo $e.'</ul>';
        } else {
        }
    } else {
        echo str_photos_dir_not_exists;
    }
    echo "</div>";


} elseif(isset($_GET['options']) and $_GET['options']=='query_def') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    // project sql query definitions
    echo "<h2>".str_query_definitions."</h2><div class='be'>
    <div class='infotitle'>In the mapfile, the wms layers should contain DATA definitions. Here you can set those SQL queries.<br> All SQL query should be connected with one web maps layer. In the last column you can set these connections. In the SQL queries there are two substitute varibles to perform dymaic queries: %qstr% and %morefilters%.</div>";

    // layers in the mapfile
    $map = getenv('PROJECT_DIR').'private/private.map';
    ob_start();
    passthru("grep LAYER $map -A 1|grep -i NAME");
    $var = ob_get_contents();
    ob_end_clean();
    $var = preg_replace('/NAME/i','',$var);
    $var = preg_replace('/\'/i','',$var);
    $var = preg_replace('/"/i','',$var);

    $map_layers = array_filter(preg_split('/\n|\r/',$var));

    // echo mapserver layers
    echo '<b>'.t(str_mapserver_layer)."</b><br>The following layers are available in your mapfile:";
    echo "<p style='padding-left:30px'>".implode($map_layers,'<br>').'</p>';

    // echo openlayers defined leyers
    /*$cmd = sprintf("SELECT layer_name FROM project_layers WHERE project_table='%s' ORDER BY 1",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $clayers = array();
    while ($row = pg_fetch_assoc($res)) {
        $clayers[] = $row['layer_name'];
    }

    if (count($clayers)) {
        echo "<p style='text-indent:30px'>";
        echo implode($clayers," - ");
        echo "</p>";
    } */
    //else
    //    echo '<b>'.t(str_webmap_layer).":</b><br>The following <i>".str_webmap_layer."</i> are available to  connect with <i>".str_mapserver_layer."</i>:";

    $tbl = new createTable();
    $tbl->def(['tid'=>'mytable','tclass'=>'admintable']);

    $layer_types = array('public','private','special');
    $layer_enabled = array('f'=>'disabled','t'=>'enabled');
    $cname_prefix = 'layer_data_';

    $cmd = "SELECT * FROM project_queries WHERE project_table='".PROJECTTABLE."' ORDER BY enabled,layer_cname";
    $res = pg_query($BID,$cmd);
    while ($row = pg_fetch_assoc($res)) {

        $cname_prefix = 'layer_data_';
        $m = array();
        if (preg_match('/^layer_data_(.+)/',$row['layer_cname'],$m)) {
            $cname = $m[1];
        } else
            $cname = $row['layer_cname'];


        $syne = '';
        if(preg_match('/WRONG/',$row['layer_query']) or $row['layer_query']=='') {
            $syne = 'orange';
        }
        $r = array();
        //array_push($r,sprintf("<select id='qnam-{$row['id']}'>%s</select>",selected_option($map_layers,$row['layer_name'])));
        array_push($r,sprintf("<textarea rows='8' cols=100 id='qquery-{$row['id']}' style='background-color:$syne'>%s</textarea><br>",$row['layer_query']));
        array_push($r,sprintf("<select id='qena-{$row['id']}' name='enabled'>".selected_option(array(str_true.'::t',str_false.'::f'),$row['enabled'])."</select>"));
        array_push($r,"<select id='qtype-{$row['id']}' style='width:120px' name='tipus'>".selected_option(array('','query & base::query','base'),$row['layer_type'])."</select>");
        array_push($r,sprintf("<select id='qrst-{$row['id']}' name='enabled'>".selected_option(array('Public::0','Private::1','Special::2'),$row['rst'])."</select>"));
        //array_push($r,sprintf("<select id='qgt-{$row['id']}'>".selected_option(array('','POINT','LINESTRING','POLYGON'),$row['geom_type'])."</select>"));
        array_push($r,sprintf("$cname_prefix<input id='qcname-{$row['id']}' value='$cname'>"));
        array_push($r,sprintf("<button name='qopld' class='button-warning button-xlarge pure-button q_update' id='qopld_".$row['id']."'>".str_modifyit."</button>"));
        //array_push($r,sprintf("<button class='button-warning button-xlarge pure-button'>".str_save."</button>"));
        $tbl->addRows($r);
    }

    $wms = "<span style='color:orange;font-family:sans-serif'>SELECT</span> obm_id, <span style='color:tomato'>%grid_geometry% AS</span>  obm_geometry <span style='color:deepskyblue'>%selected%</span><br>
<span style='color:orange'>FROM</span> <span style='color:deepskyblue'>%F%".PROJECTTABLE." ".substr(PROJECTTABLE,0,1)."%F%</span><br>
&nbsp; &nbsp; <span style='color:deepskyblue'>%uploading_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%rules_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%taxon_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%grid_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%search_join%</span><br>
&nbsp; &nbsp; <span style='color:deepskyblue'>%morefilter%</span><br>
<span style='color:orange'>WHERE</span> <span style='color:deepskyblue'>%geometry_type%</span> <span style='color:deepskyblue'>%envelope%</span> <span style='color:deepskyblue'>%qstr%</span>";

    $wmsq = "<br> <b>Example <i>query type</i> query</b><p style='padding:0px 0px 10px 10px'>$wms</p>";

    /* if ($_SESSION['st_col']['GEOM_C']=='') {
        $wms = '';
        $wmsq = '';
        echo "<div style='color:red'>SET YOUR COLUMNS FIRST! - and reload page</div>";
    } */

    echo $wmsq;

    $tbl->addRows(array(
        "<textarea id='qquery-new' style='width:100%;height:90px;background-color:#eFeFeD' placeholder='copy the example from above'></textarea>",
        "<select id='qena-new'><option>TRUE</option><option>FALSE</option></select>",
        "<select id='qtype-new'><option>base</option><option value='query'>query & base</option></select>",
        "<select id='qrst-new'><option value=0>public</option><option value=1>private</option><option value=2>special</option></select>",
        "$cname_prefix<input id='qcname-new'>",
        "<input type='button' name='aqd' class='button-success button-xlarge pure-button q_update' id='aqd_new' value='".str_add."'>"
    ));
        //sprintf("<select id='qnam-new'>%s</select>",selected_option($map_layers,' ')),
        //"<select id='qgt-new'><option></option><option>POINT</option><option>LINESTRING</option><option>POLYGON</option></select>",
    //layer_name 	layer_query 	enabled 	layer_type 	rst 	layer_cname
    $tbl->addHeader(array("SQL ".str_query,t(str_enabled),t(str_type),t(str_access),t(str_sql_reference),t(str_actions)));
    echo $tbl->printOut();
    echo "</div>";



} elseif(isset($_GET['options']) and $_GET['options']=='openlayes_def') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* OpenLayers definitions */
    echo "<h2>Openlayers ".str_layer." ".str_definitions."</h2><div class='be'>
    <div class='infotitle'>".str_layers_help."</div>";

    $map = getenv('PROJECT_DIR').'private/private.map';
    ob_start();
    passthru("grep LAYER $map -A 1|grep -i NAME");
    $var = ob_get_contents();
    ob_end_clean();
    $var = preg_replace('/NAME/i','<li>',$var);
    $var = preg_replace('/\'/i','',$var);
    $var = preg_replace('/"/i','',$var);

    $map_layers = array_filter(preg_split('/\n|\r/',$var));

    $layer = preg_replace('/\n|\r/','',$var);
    $layers = explode('<li>',$layer);
    $layers = array_map('trim',array_filter($layers, create_function('$a','return trim($a)!=="";')));

    //echo preg_replace('/\n|\r/','</li>',$var);
    //echo "</ul>";

    $tbl = new createTable();
    $tbl->def(['tid'=>'mytable','tclass'=>'admintable']);

    $cmd = "SELECT layer_cname FROM project_queries WHERE project_table='".PROJECTTABLE."'";
    $layers_cname = pg_query($BID,$cmd);
    $cname_options = array();
    while ($row = pg_fetch_assoc($layers_cname)) {
        $m = array();
        if (preg_match('/^layer_data_(.+)/',$row['layer_cname'],$m)) {

            $cname_options[] = "$m[1]::".$row['layer_cname'];
        }
    }


    $cmd = "SELECT * FROM project_layers WHERE project_table='".PROJECTTABLE."' ORDER BY enabled,layer_name";
    $layers_res = pg_query($BID,$cmd);
    while ($layers_row = pg_fetch_assoc($layers_res)) {
        $r=array();
        if(in_array($layers_row['layer_name'],$layers)) {
            $k = array_search($layers_row['layer_name'],$layers);
            unset($layers[$k]);
        }
        array_push($r,sprintf("<select id='oplnam-{$layers_row['id']}'>%s</select>",selected_option( $cname_options, $layers_row['layer_name'] )));
        array_push($r,"<select id='opltype-{$layers_row['id']}' style='width:100px' name='tipus'>".selected_option(array('WMS','WFS'),$layers_row['tipus'])."</select>");
        array_push($r,sprintf("<input id='oplmslayer-{$layers_row['id']}' value='%s'>",$layers_row['ms_layer']));
        array_push($r,sprintf("<textarea id='opldef-{$layers_row['id']}' style='width:200px;height:100px'>%s</textarea>",preg_replace("/,/",",\n",$layers_row['layer_def'])));

        // queries
        $cmd2 = "SELECT * FROM project_queries WHERE project_table='".PROJECTTABLE."' AND layer_cname='{$layers_row['layer_name']}' ORDER BY enabled";
        $res2 = pg_query($BID,$cmd2);
        $layer_types = array('public','private','special');
        $layer_enabled = array('f'=>'disabled','t'=>'enabled');
        if(pg_num_rows($res2)) {
            $rn = array();
            while($row2=pg_fetch_assoc($res2)){
                array_push($rn,sprintf("%s %s %s",$layer_types[$row2['rst']],$row2['layer_type'],$layer_enabled[$row2['enabled']]));
            }
            array_push($r,implode($rn));
        }
        else {
            array_push($r,sprintf("<div style='background-color:#FF5C0B !important;color:black;font-size:1.3em'>NO QUERY defined for this layer!</div>"));
        }

        array_push($r,sprintf("<input id='oplurl-{$layers_row['id']}' value='%s'>",$layers_row['url']));
        array_push($r,sprintf("<input id='oplmap-{$layers_row['id']}' size=8 value='%s'>",$layers_row['map']));
        array_push($r,sprintf("<input id='opldesc-{$layers_row['id']}' value='%s'>",$layers_row['name']));
        array_push($r,sprintf("<input id='oplorder-{$layers_row['id']}' size=2 value='%s'>",$layers_row['layer_order']));
        array_push($r,sprintf("<select id='oplena-{$layers_row['id']}' name='enabled'>".selected_option(array(str_true.'::t',str_false.'::f'),$layers_row['enabled'])."</select>"));
        array_push($r,sprintf("<button name='copld' class='button-warning button-xlarge pure-button opl_update' id='copld_".$layers_row['id']."'>".str_modifyit."</button>"));

        $tbl->addRows($r);
    }

    $lk = '';
    $def_def = '';
    $si = '';
    $def = '';
    $de = '';
    if (count($layers)) {
        $lk = trim(array_pop($layers));
        $def_def = preg_replace("/map-file-layer-name/",$lk,"layers:'map-file-layer-name', isBaseLayer:'false', visibility:'true', opacity:'1.0', format:'image/png', transparent:'true', numZoomLevels:'20'");
        $de = 'Data layer';
        if (preg_match('/_query/',$lk)) {
            $si = 'selected';
            $de = 'Query layer';
        }
        $def = 'default';
    }

    // new line at the end of the table
    $tbl->addRows(array("<select  id='oplnam-new'>".selected_option($cname_options,' ')."</select>",
        "<select style='width:100px' id='opltype-new'><option>WMS</option><option>WFS</option></select>",
        "<select id='oplmslayer-{$layers_row['id']}' style='width:100px' name='tipus'>".selected_option($map_layers,'')."</select>",
        "<textarea style='width:200px;height:100px' id='opldef-new'>$def_def</textarea>",
        "",
        "<input id='oplurl-new' placeholder='proxy' value='$def'>",
        "<input id='oplmap-new' size=8 placeholder='default' value='$def'>",
        "<input id='opldesc-new' value='$de'>","<input id='oplorder-new' size=2>",
        "<select id='oplena-new'><option>TRUE</option><option>FALSE</option></select>",
        "<input type='button' name='aopld' class='button-success button-xlarge pure-button opl_update' id='aopld_new' value='".str_add."'>"));
    $tbl->addHeader(array(t(str_sql_reference),"Mapserver ".t(str_type),t(str_mapserver_layer),"OpenLayers ".str_layer_def,"SQL ".str_state,t(str_url),"MAP Proxy","OpenLayers ".str_name,t(str_order),t(str_enabled),t(str_actions)));
    echo $tbl->printOut();
    echo "</div>";

} elseif(isset($_GET['options']) and $_GET['options']=='serverinfo') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

        $files = array();

        $root_dir = OB_ROOT.'projects/'.PROJECTTABLE;
        $supervisor_time_log_file = $root_dir.'/supervisor_time_log_file';
        $supervisor_last_update = 0;
        if (is_file($supervisor_time_log_file))
            $supervisor_last_update = filemtime($supervisor_time_log_file);

        if (is_dir($root_dir.'/includes'))
        if ($handle = opendir($root_dir.'/includes')) {
            while (false !== ($file = readdir($handle))) {
                if (is_file($file)) {
                    $modified = filemtime($file);
                    if ($supervisor_last_update & $modified > $supervisor_last_update)
                        $files[$modified] = $file;
                }
            }
            closedir($handle);
        }
        if (is_dir($root_dir.'/includes/modules'))
        if ($handle = opendir($root_dir.'/includes/modules')) {
            while (false !== ($file = readdir($handle))) {
                if (is_file($file)) {
                    $modified = filemtime($file);
                    if ($supervisor_last_update & $modified > $supervisor_last_update)
                        $files[$modified] = $file;
                }
            }
            closedir($handle);
        }
        if (is_dir($root_dir.'/js'))
        if ($handle = opendir($root_dir.'/js')) {
            while (false !== ($file = readdir($handle))) {
                if (is_file($file)) {
                    $modified = filemtime($file);
                    if ($supervisor_last_update & $modified > $supervisor_last_update)
                        $files[$modified] = $file;
                }
            }
            closedir($handle);
        }
        if (is_dir($root_dir))
        if ($handle = opendir($root_dir)) {
            while (false !== ($file = readdir($handle))) {
                if (is_file($file)) {
                    $modified = filemtime($file);
                    if ($supervisor_last_update & $modified > $supervisor_last_update)
                        $files[$modified] = $file;
                }
            }
            closedir($handle);
        }
        if (is_dir($root_dir.'/css'))
        if ($handle = opendir($root_dir.'/css')) {
            while (false !== ($file = readdir($handle))) {
                if (is_file($file)) {
                    $modified = filemtime($file);
                    if ($supervisor_last_update & $modified > $supervisor_last_update)
                        $files[$modified] = $file;
                }
            }
            closedir($handle);
        }


        krsort($files);

        #$last_modified_file = array_shift($files);

        echo "<h2>Last updated files [".date("F d, Y. H:i:s",$supervisor_last_update)."]</h2><div class='be'>";
        foreach ($files as $file) {
            echo $file."<br>";
        }
        echo "</div>";

} elseif(isset($_GET['options']) and $_GET['options']=='access') {
    /* Projekt elérés */
    echo "<h2>".str_data_access."</h2><div class='be'>
    <div class='infotitle'>".str_access_help."</div>";
    $acc = array(0=>'everybody',1=>'logined users',2=>'specified group members');
    $mod = array(0=>'everybody',1=>'logined_users',2=>'specified group members');
    $levels = array('0'=>0,'1'=>1,'2'=>2,'public'=>0,'login'=>1,'group'=>2);

    //access levels
    echo "<div>".str_data_read_acc.": ";
    $n = array();
    foreach ($acc as $k=>$v) {
        if ($levels[ACC_LEVEL] == $k)
            $n[] = "<b>$v</b>";
        else
            $n[] = "$v";
    }
    echo "[".implode('] &nbsp; [',$n)."].</div>";


    //mod leveles
    echo "<div>".str_data_mod_acc.": ";
    $n = array();
    foreach ($mod as $k=>$v) {
        if ($levels[MOD_LEVEL] == $k)
            $n[] = "<b>$v</b>";
        else
            $n[] = "$v";
    }
    echo "[".implode('] &nbsp; [',$n)."].</div>";

    echo "<br><i>You can change these options in the local_vars.php.inc file.</i><br><br>";

    echo "<div class='infotitle'>Access rules per table and per line</div>";

    $cmd = "SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '".PROJECTTABLE."_rules') AS exists";
    $result = pg_query($ID,$cmd);
    $rules_table = pg_fetch_assoc($result);


    $schema = 'public';
    $cmd = sprintf("SELECT f_main_table,f_geom_column, f_species_column, f_quantity_column, f_id_column,
        coalesce(f_x_column,'') as f_x_column,coalesce(f_y_column,'') as f_y_column,
        f_srid, f_order_columns,f_use_rules,
        ARRAY_TO_STRING(f_cite_person_columns,',') AS f_cite_person_columns,
        ARRAY_TO_STRING(f_date_columns,',') AS f_date_columns,
        ARRAY_TO_STRING(f_file_id_columns,',') AS f_file_id_columns,
        ARRAY_TO_STRING(f_alter_speciesname_columns,',') AS f_alter_speciesname_columns
    FROM header_names
    WHERE f_table_schema=%s AND f_table_name='%s'",quote($schema),PROJECTTABLE);
    $result = pg_query($BID,$cmd);

    $acclevel = ACC_LEVEL;
    $modlevel = MOD_LEVEL;

    while( $row = pg_fetch_assoc($result)) {

        echo sprintf("<b>%s</b><br>",$row['f_main_table']);
        $restrict_prestate = $row['f_use_rules'];
        $restriction = "not set";

        $acm = $modules->is_enabled('allowed_columns',$row['f_main_table']);

        if ($rules_table['exists']=='t') {
            if ("$acclevel" == "2" or "$acclevel" == "group") {
                if ($restrict_prestate=='0' or $restrict_prestate=='false' or $restrict_prestate=='-1' or $restrict_prestate===false or $restrict_prestate=='f'){
                    $restriction = "disabled";
                } elseif ($restrict_prestate!='' or $restrict_prestate!=NULL) {
                    $restriction = "enabled";
                }
            } elseif ("$acclevel" == "1" or "$acclevel" == "login") {
                if ($restrict_prestate){
                    $restriction = "enabled but not applicable due to public access level";
                }
            } elseif ("$acclevel" == "0" or "$acclevel" == "public") {
                if ($restrict_prestate){
                    $restriction = "enabled but not applicable due to public access level";
                }
            }
        } else {
            if ("$acclevel" == "2" or "$acclevel" == "group") {
                echo "<div style='warning'>The global access level is `group` but ".PROJECTTABLE."_rules table does not exists!</div>";
            }
        }
        echo sprintf("<div class=''>Restriction by rules (usage of public.%s_rules) is <u>%s</u> in biomaps.header_names for {$row['f_main_table']}</div>",PROJECTTABLE,$restriction);

        if (("$acclevel" == "1" or "$acclevel" == "login" or "$acclevel" == "2" or "$acclevel" == "group") and !$acm) {

            echo "<div style='warning'><i>Allowed columns</i> module is not enabled for ".$row['f_main_table'].",
                    therfore all columns disabled for non logined users, and all accessible for logined users!</div>";

        }
        elseif (("$acclevel" == "1" or "$acclevel" == "login" or "$acclevel" == "2" or "$acclevel" == "group") and $acm) {

            $params = preg_split("/;/",$modules->get_params('allowed_columns',$row['f_main_table']));
            $li = "";
            foreach ($params as $p) {
                $for = preg_split("/:/",$p);
                if ($for[0] == 'for_sensitive_data')
                    $li .= "<li>for sensitive data: $for[1]</li>";
                elseif ($for[0] == 'for_no-geom_data')
                    $li .= "<li>for no-geom data: $for[1]</li>";
                elseif ($for[0] == 'for_general') {
                    if ("$acclevel" == "2" or "$acclevel" == "group")
                        $li .= "<li>in general: $for[1]</li>";
                    else
                        $li .= "<li class='warning'>There are general column restrictions but does not applicable. Group level access setting is needed in local_vars.php.inc!</li>";
                } else {
                    $li .= "<li class='warning'>Not valid column settings in module parameter! (Either of the following rules should be defined before the list of the columns:
                            <br>for_sensitive_data, for_no-geom_data, for_general</li>";
                }
            }

            echo "<div>Accessible columns<ul>$li</ul></div>";

        } elseif ($acm) {
            echo "<div class='warning'><i>Allowed columns</i> module is enabled but not applicable due to public access level.</div>";

        }
    }


    // other local_vars variables??

    echo "</div>";

} elseif(isset($_GET['options']) and $_GET['options']=='imports') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* Importok betöltése
     * interrupted imports
     * */
    echo "<h2>".str_intr_uploads."</h2><div class='be' style='line-height:32px'>";
    echo "<div class='infotitle'>".str_load_import_help."</div>";
    $timeZone = date_default_timezone_get();
    $cmd = "begin;";
    $res = pg_query($ID,$cmd);
    $cmd = "set local timezone to '$timeZone'";
    $res = pg_query($ID,$cmd);
    $cmd = "SELECT ref,user_id,to_char(datum, 'YYYY-MM-DD HH24:MI:SS') as datum,form_id,form_type,file FROM system.imports WHERE project_table='".PROJECTTABLE."' ORDER BY datum DESC,form_id,user_id";
    $res = pg_query($ID,$cmd);
    while($row=pg_fetch_assoc($res)) {
        $cmd = sprintf("SELECT username FROM users WHERE id=%d",$row['user_id']);
        $rs = pg_query($BID,$cmd);
        $r = pg_fetch_assoc($rs);

        $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id=%d",$row['form_id']);
        $rs2 = pg_query($BID,$cmd);
        $r2 = pg_fetch_assoc($rs2);

        $cmd = sprintf('SELECT 1
                FROM information_schema.tables
                WHERE table_schema = \'temporary_tables\'
                AND table_name = \'%1$s_%2$s\'',$row['file'],$row['ref']);
        $rs3 = pg_query($ID,$cmd);
        if (pg_num_rows($rs3)) {
            $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.%1$s_%2$s',$row['file'],$row['ref']);
            $rs3 = pg_query($ID,$cmd);
            $r3 = pg_fetch_assoc($rs3);
        } else
            $r3 = array("c"=>"missing");

        echo "<button data-id='{$row['ref']}' class='dropimport pure-button button-warning'>".str_delete."</button> <a href='includes/ajax?exportimport={$row['ref']}' class='pure-button button-href button-success'>".str_export."</a> <a href='http://".URL."/upload/?load={$row['ref']}' target='_blank'>{$r['username']}, {$row['datum']} - {$r2['form_name']} ({$row['form_type']}). ".t(str_rows).": {$r3['c']}</a><br>";
    }

    $cmd = "end;";
    $res = pg_query($ID,$cmd);
    echo "</div>";

} elseif(isset($_GET['options']) and $_GET['options']=='dbcols') {
    /* Adatbázis oszlopok - database columns */

    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    echo "<h2>".str_db_cols."<a href='#tab_basic' id='open-sql-console' class='pure-button button-href' alt='SQL console' data-url='' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-terminal'></i> SQL</a></h2><div class='be'>";
    echo "<div class='infotitle'><h3>".str_db_cols_help."</h3></div>";

    $mtable = PROJECTTABLE;
    if (isset($_GET['mtable'])) $mtable = $_GET['mtable'];

    $modules->set_main_table($mtable);

    echo $modules->_include('list_manager','modal');

    $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);
    $mrow = array();
    while ($row = pg_fetch_assoc($res)) {
        $mrow[] = $row['m'];
    }
    echo str_choose_mtable.": <select class='main_table_selector' id='dbcols'>".selected_option($mrow,$mtable)."</select> <button class='main_table_refresh' id='dbcols'><i class='fa fa-refresh'></i></button><br>";

    $col_names = array();
    $cmd = sprintf("SELECT column_name,short_name,\"order\",project_table,description FROM project_metaname WHERE project_table='%s'",$mtable);
    $rs = pg_query($BID,$cmd);
    if(pg_num_rows($rs))
        $col_names = array_merge($col_names, pg_fetch_all($rs));

    /* special columns from header_names */
    $cmd = sprintf("SELECT f_geom_column,f_species_column,
        f_quantity_column,f_id_column,f_x_column,f_y_column,f_use_rules,f_utmzone_column,
        ARRAY_TO_STRING(f_date_columns,',') AS f_date_columns,
        ARRAY_TO_STRING(f_cite_person_columns,',') AS f_cite_person_columns,f_srid,f_order_columns,
        ARRAY_TO_STRING(f_file_id_columns,',') AS f_file_id_columns,
        ARRAY_TO_STRING(f_alter_speciesname_columns,',') AS f_alter_speciesname_columns
        FROM header_names
        WHERE f_table_name='".PROJECTTABLE."' AND f_main_table=%s",quote($mtable));
    $res = pg_query($BID,$cmd);

    if (!pg_num_rows($res)) {
        $main_col = array();
        echo "no header_names entries about $mtable!";
        log_action("$mtable not defined in header_names",__FILE__,__LINE__);
    } else {
        $main_col = pg_fetch_assoc($res);
    }

    //$order_col = explode(",",$main_col['f_order_columns']);
    if (isset($main_col['f_order_columns']))
        $order_col = json_decode($main_col['f_order_columns'],true);
    else
        $order_col = array();

    /* column names from postgres */
    $w = array();
    $w[] = sprintf("attrelid='public.%s'::regclass",$mtable);

    // default ordering
    $values = "('',1)";


    $ordered_values = array();
    if (isset($order_col[$mtable])) {
        foreach($order_col[$mtable] as $key=>$val) {
            if (!is_numeric($val)) $val = 0;
            $ordered_values[] = "('$key',$val)";
        }
    }

    //('eov_x',Xc),('eov_y',Yc)

    if (count($ordered_values))
        $values = implode(',',$ordered_values);

    // if there invalid table names in "projects" there will be no columns in project_admin's column page!
    $cmd = sprintf('SELECT attrelid::regclass,attnum,attname,column_comment FROM pg_attribute
        LEFT JOIN (
            SELECT
                cols.column_name,
                (
                    SELECT
                        pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                    FROM
                        pg_catalog.pg_class c
                    WHERE
                        c.oid = (SELECT (\'"\' || cols.table_name || \'"\')::regclass::oid)
                        AND c.relname = cols.table_name
                ) AS column_comment
            FROM
                information_schema.columns cols
            WHERE
                cols.table_catalog    = \'%1$s\'
                AND cols.table_name   = \'%2$s\'
                AND cols.table_schema = \'public\'
        ) as cato ON cato.column_name=pg_attribute.attname
        LEFT JOIN (
          VALUES %3$s
         ) AS x (id, ordering) on pg_attribute.attname = x.id
            WHERE (%4$s) AND attnum>0 AND NOT attisdropped ORDER BY x.ordering',gisdb_name,$mtable,$values,join(' OR ',$w));

    $res = pg_query($ID,$cmd);
    if (!pg_num_rows($res)) {
        echo "Might be wrong query based on <i>main_table</i> content.<br>";
        log_action($cmd,__FILE__,__LINE__);
    }

    $tbl = new createTable();
    $tbl->def(['tid'=>'cola','tclass'=>'resultstable']);
    $header = array(str_column,str_visible_name,str_comment,'OpenBioMaps '.str_type,str_order);

    //list_manager include
    if ($modules->is_enabled('list_manager',$mtable)) {
        $header[] = $modules->_include('list_manager','db_cols_header');
    }

    $tbl->addHeader($header);
    $n = 0;

    $orphaned_columns = array();
    $existing_columns = array();

    while ($row = pg_fetch_assoc($res)) {
        $order = '';
        if (isset($order_col[$row['attrelid']])) {
            $order_table = $order_col[$row['attrelid']];
            if (array_key_exists($row['attname'],$order_table) and is_numeric($order_table[$row['attname']])) {
                $order = $order_table[$row['attname']];
            }
        }
        # skip the system cols
        if  ($row['attname']=='obm_id') continue;
        if  ($row['attname']=='obm_modifier_id') continue;
        if  ($row['attname']=='obm_uploading_id') continue;
        if  ($row['attname']=='obm_comments') continue;
        if  ($row['attname']=='obm_validation') continue;
        if  ($row['attname']=='taxon_id') continue;
        $column_comment = $row['column_comment'];

        $val = '';
        $ro = '';
        $ph = 'non handled field';
        $selected = "";
        foreach ($col_names as $cn) {
            if ($cn['column_name']==$row['attname'] and $cn['project_table']==$row['attrelid']) {
                $val = $cn['short_name'];
                $selected='data';
                $existing_columns[] = $cn['column_name'];

                if ($cn['description']!='')
                    $column_comment = $cn['description'];
            } elseif ($cn['project_table']==$row['attrelid']) {
                $orphaned_columns[] = $cn['column_name'];
            }
        }
        // special column marking and naming if no name defined
        foreach ($main_col as $key=>$value) {
            $split_value = preg_split('/,/',$value);

            foreach ($split_value as $svalue) {
                if($key=='f_file_id_columns' and $row['attname']=='obm_files_id' and $row['attrelid']==$svalue) {
                    $selected='attachment';
                    $ro='';
                    $ph='special field';
                }

                //set default translation if local not exists
                //dinpi.faj == ...
                if ($svalue==$row['attname']) {
                    #f_species_column	f_date_column	f_quantity_column	f_id_column	f_x_column	f_y_column	f_cite_person	f_srid	f_geom_column
                    if ($key=='f_quantity_column') {
                        $selected='numind';
                        $ro='';
                        $ph='special field';
                    }
                    elseif  ($key=='f_date_columns') {
                        $selected='datum';
                        $ro='';
                        $ph='special field';
                    }
                    elseif  ($key=='f_species_column') {
                        $selected='species';
                        $ro='';
                        $ph='special field';
                    }
                    elseif  ($key=='f_id_column') {
                        $selected='id';
                        $ro='readonly';
                        $ph='special field';
                        $val='';
                    }
                    elseif  ($key=='f_x_column') {
                        $selected='Xc';
                        $ro='';$ph='special field';
                    }
                    elseif  ($key=='f_y_column') {
                        $selected='Yc';
                        $ro='';
                        $ph='special field';
                    }
                    elseif  ($key=='f_cite_person_columns') {
                        $selected='cp';
                        $ro='';
                        $ph='special field';
                    }
                    elseif  ($key=='f_geom_column') {
                        $selected='geometry';
                        $ro='';
                        $ph='special field';
                    }
                    elseif ($key=='f_alter_speciesname_columns') {
                        $selected='alternames';
                        $ro='';
                        $ph='special field';
                    }
                    elseif ($key=='f_utmzone_column') {
                        $selected='utmzone';
                        $ro='';
                        $ph='special field';
                    }
                }
            }
        }

        $field = "<input class='element' value='$val' id='pcsi-$n' $ro placeholder='$ph' style='width:250px'>";

        $options = array('',str_data.'::data',str_spatial_geometry.'::geometry',str_sci_name.'::species',str_alt_names.'::alternames',str_date.'::datum',str_no_inds.'::numind',str_rowid.'::id',str_latitude.'::Yc',str_longitude.'::Xc',str_cite_person.'::cp',str_attachment.'::attachment',str_utmzone.'::utmzone');

        $rows = array(
            "<span style='color:gray'>{$row['attrelid']}.</span><input class='element' value='{$row['attname']}' readonly style='width:250px'>",
            $field,
            "<input class='element' id='pcomm-$n' value='".htmlentities($column_comment,ENT_QUOTES)."'>",
            "<select class='element proj_cols_set' id='pcs-$n'>".selected_option($options,$selected)."</select>",
            "<input class='element' size=3 value='$order'>"
        );

        if ($modules->is_enabled('list_manager',$mtable)) {
            $pa = [
                'table' => $mtable,
                'colname' => $row['attname']
            ];
            $rows[] = $modules->_include('list_manager','manage_list_button',$pa);
        }
        $tbl->addRows($rows);

        $n++;
    }

    $orphaned_columns = array_unique($orphaned_columns);
    $diff_columns = array_diff($orphaned_columns,$existing_columns);
    foreach ($diff_columns as $oc) {
            $tbl->addRows(
            array("<span style='color:gray'>$mtable.</span><input class='element' value='$oc' readonly style='width:250px;color:red;font-weight:bold'>",
            "<input class='element' readonly value='$oc' id='pcsi-$n' placeholder='$ph' style='width:250px;color:red;font-weight:bold'>",
            "<input class='element' readonly id='pcomm-$n' value='".htmlentities($column_comment,ENT_QUOTES)."'>",
            "<select class='element proj_cols_set' id='pcs-$n'>".selected_option($options,'')."</select>",
            "<input class='element' readonly size=3 value='$order'>"));
        $n++;
    }

    echo $tbl->printOut();
    if ($n>=2) echo "<button class='button-success button-xlarge pure-button' id='proj_column'><i id='ficon' class='fa fa-floppy-o'></i> ".str_save."</button>";

    echo "</div>";

    $tbl = new createTable();
    $tbl->def(['tid'=>'new-col','tclass'=>'resultstable']);
    $tbl->addHeader(array(str_column,str_visible_name,str_comment,'PostgreSQL '.str_type.' <a href="https://www.postgresql.org/docs/9.6/datatype.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',str_length,str_default_value.' <a href="https://www.postgresql.org/docs/9.6/ddl-default.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',str_array.' <a href="https://www.postgresql.org/docs/9.6/arrays.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',str_constraints.' <a href="https://www.postgresql.org/docs/9.6/ddl-constraints.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>'));

    echo "<div class='infotitle'><h3>".str_new_column."</h3></div>";
    $column_types = array(str_decimal_number.'::numeric',str_integer.'::integer',str_date.'::date','true/false::boolean',
        'timestamp (without timezone)::timestamp without timezone',
        'characters (max 4)::character varying (4)',
        'characters (max 8)::character varying (8)',
        'characters (max 16)::character varying (16)',
        'characters (max 32)::character varying (32)',
        'characters ('.str_set_max.')::character varying',
        'characters ('.str_set_it.')::character',
        ''.str_long_text.'::text');

    $tbl->addRows(
            array("<input class='element' id='column_name' value='' style='font-size:150%;font-weight:bold;width:12em'>",
            "<input class='element' value='' id='column_label' style='font-size:150%;font-weight:bold;width:12em'>",
            "<input class='element' value='' id='column_comment' style='font-size:150%;font-weight:bold'>",
            "<select class='element' id='column_type'>".selected_option($column_types,'')."</select>",
            "<input class='element' id='column_length' value='' style='font-size:150%;font-weight:bold;width:4em'>",
            "<input class='element' id='column_default' value='' style='font-size:150%;font-weight:bold'>",
            "<select class='element' id='column_array'><option value=''></option><option value='[]'>[]</option></select>",
            "<input class='element' id='column_check' value='()' style='font-size:150%;font-weight:bold'>",
        ));
    echo $tbl->printOut();
    echo "<button class='button-warning button-xlarge pure-button' id='add_new_column'><i id='ficon' class='fa fa-floppy-o'></i> ".str_add."</button>";

    echo "</div>";


    // clean dbcolist cache
    obm_cache('delete',"dbcolist.$mtable",'',0,FALSE);

} elseif(isset($_GET['options']) and $_GET['options']=='languages') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* Languages */

    echo "<h2>".str_lang." ".str_definitions."</h2><div class='be'>";

    echo "<h3><i class='fa fa-plus showHide' data-target='globalTranslations' style='color: rgb(66, 184, 221); cursor: pointer;'></i> ". str_global_translations." </h3></br>";
    $tbl = new createTable();
    $tbl->def(['tid'=>'globalTranslations','tclass'=>'resultstable hidden']);
    $tbl->addHeader(array(str_constant_name,str_translation));
    //$cmd = sprintf("SELECT id, const, translation FROM translations WHERE scope = 'global' AND lang = '%s' ORDER BY const;", $_SESSION['LANG']);
    $cmd = sprintf('SELECT t1.id,t2.lang,t1.const,t1.translation AS translation_en,t2.translation
                    FROM translations t1  LEFT JOIN translations t2 ON (t1.project IS NOT DISTINCT FROM t2.project AND t1.const=t2.const AND t2.lang=%1$s)
                    WHERE t1.scope = \'global\' AND t1.lang IN (\'en\') ORDER BY t1.const',quote($_SESSION['LANG']));

    if (pg_send_query($BID, $cmd)) {
        $res = pg_get_result($BID);
        $state = pg_result_error($res);
        if ($state != '') {
            log_action($state,__FILE__,__LINE__);
            return;
        }

        while ($row = pg_fetch_assoc($res)) {
            if ($row['lang']=='')
                $trans = ":=--> translation row is missing from the translation file <--=:";
            else {
                if ($row['translation'] == '')
                    $trans = ":=--> translation is missing for this constant <--=:";
                else
                    $trans = $row['translation'];
            }

            $tbl->addRows([
                htmlentities($row['const']),
                htmlentities($trans)
            ]);
        }
        echo $tbl->printOut();
    }

    echo "<h3><i class='fa fa-minus showHide' data-target='update_translations' style='color: rgb(66, 184, 221); cursor: pointer;'></i> ".str_local_translations." </h3></br>";
    $tbl = new createTable();
    $tbl->def(['tid'=>'translationsTable','tclass'=>'resultstable']);
    $tbl->addHeader(array(str_constant_name,str_translation));

    //$cmd = sprintf("SELECT id, const, translation FROM translations WHERE scope = 'local' AND project = '%s' AND lang = '%s' ORDER BY const;", PROJECTTABLE, $_SESSION['LANG']);
    $cmd = sprintf('SELECT t2.id,t2.lang,t1.const,t1.translation AS translation_en,t2.translation
                    FROM translations t1  LEFT JOIN translations t2 ON (t1.project IS NOT DISTINCT FROM t2.project AND t1.const=t2.const AND t2.lang=%1$s)
                    WHERE t1.scope = \'local\' AND t1.lang IN (\'en\') AND t1.project=\'%2$s\' ORDER BY t1.const',quote($_SESSION['LANG']),PROJECTTABLE);

    if (pg_send_query($BID, $cmd)) {
        $res = pg_get_result($BID);
        $state = pg_result_error($res);
        if ($state != '') {
            log_action($state,__FILE__,__LINE__);
            return;
        }

        $n = 1;
        while ($row = pg_fetch_assoc($res)) {
            $trans = "";
            $placeholder = $row['translation_en'];

            if ($row['lang']=='') {
                $trans = "__not_translated__";
            } else {
                if ($row['translation'] == '')
                    $trans = "__not_translated__";
                else {
                    $trans = $row['translation'];
                }
            }
            if ($row['id']=='')
                $row['id'] = $n.'new';

            $warning = ($trans == '__not_translated__') ? 'red-border' : '';

            $tbl->addRows([
                "<input transupdate name='const-{$row['id']}' style='width: 300px' value='" . htmlentities($row['const']) . "'>",
                "<textarea name='translation-{$row['id']}' style='width:550px;height:40px' placeholder='" . htmlentities($placeholder,ENT_QUOTES) . "' class='$warning'>" . htmlentities($trans,ENT_QUOTES) . "</textarea>"
            ]);
            $n++;
        }
        $tbl->addRows([
            "<input name='new-const-1' style='width: 300px' value=''>",
            "<textarea name='new-translation-1' style='width:550px;height:40px' placeholder=''></textarea>"
        ]);
        echo '<form id="update_translations"><input name="update_translations" value="true" hidden>';
        echo $tbl->printOut();
        echo "<div style='padding-bottom: 30px'><button type='button' id='new_translation' data-id='1' class='button-secondary pure-button'><i class='fa fa-plus'></i> ".str_add_translation."</button></div>";
        echo "<button type='submit' class='button-success button-xlarge pure-button' id='language-update-button'><i id='ficon' class='fa fa-floppy-o'></i> ".str_save."</button>";
        echo "</form>";
    }
    echo "</div>";

    // Modules module settings
} elseif(isset($_GET['options']) and $_GET['options']=='modules') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    $mtable = (isset($_GET['mtable'])) ? $_GET['mtable'] : PROJECTTABLE;

    $modules->set_main_table($mtable);
    $custom_menu = $modules->which_has_method('getMenuItem',$mtable);

    $n = array();
    foreach ($csor as $row) {
        //$sel = "";
        //if (in_array($row['role_id'],$g)) $sel = "selected";
        //$groups .= "<option value='{$row['group_id']}' $sel>".$row['description']."</option>";
        $n[] = $row['description']."::".$row['role_id'];
    }


    /* Modules */
    echo "<h2>".str_modules."</h2><div class='be'>";

    $module_list = $modules->list_modules();
    $datalist = "<datalist id='modules'>";
    foreach ($module_list as $ml) {
        $datalist .= "<option value='$ml'>";
    }
    $datalist .= "</datalist>";
    echo $datalist;

    $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);
    $mrow = array();
    while ($row = pg_fetch_assoc($res)) {
        $mrow[] = $row['m'];
    }
    echo str_choose_mtable.": <select class='main_table_selector' id='modules'>".selected_option($mrow,$mtable)."</select> <button class='main_table_refresh' id='modules'><i class='fa fa-refresh'></i></button> ".wikilink('modules.html',str_module_documents)."<br>";

    $cmd = "SELECT id,\"enabled\",\"module_name\",\"function\",\"module_access\",array_to_string(params,'#~#') as params,file,array_to_string(group_access,',') as ga FROM modules WHERE project_table='".PROJECTTABLE."' AND main_table=".quote($mtable)." ORDER BY module_name,function";
    $res = pg_query($BID,$cmd);
    $tbl = new createTable();
    $tbl->def(['tid'=>'mytable','tclass'=>'admintable']);
    while ($row = pg_fetch_assoc($res)) {

        $params = explode('#~#',$row['params']);
        for ($i = 0; $i < count($params); $i++) {
            if (preg_match("/^JSON:(.*)$/",$params[$i],$json))
                $params[$i] = base64_decode($json[1]);
        }

        $access_groups_options = selected_option($n,explode(',',$row['ga']));

        $id = $row['id'];
        $r=array();

        $module_exists = 'color:#FF6F6F;';

        if (file_exists(getenv('PROJECT_DIR').'includes/modules/'.$row['module_name'].'.php')) {
            $module_exists = 'color:green;';
            $module_name = $row['module_name'].'.php';
        }
        if (file_exists(getenv('PROJECT_DIR').'includes/modules/private/'.$row['module_name'].'.php')) {
            $module_exists = 'color:#FFED71;';
            $module_name = $row['module_name'].'.php';
        }
        //array_push($r,sprintf("<input readonly style='$module_exists' id='module-file-$id' value='%s'>",$module_name));

        array_push($r,sprintf('<input class=\'pure-input pure-u-1\' id=\'module-name-%1$s\' style=\'%3$s\' value=\'%2$s\'><input type=\'hidden\' readonly id=\'module-file-%1$s\' value=\'%4$s\'>',$id,$row['module_name'],$module_exists,$module_name));

        $placeholder_text = "newline separated list of parameters";
        if ($e = $modules->get_example($row['module_name'])) {
            $placeholder_text = $e;
        }
        $module_params_interface = sprintf("<textarea id='module-params-$id' style='width:350px;height:60px' placeholder='$placeholder_text'>%s</textarea>",implode("\n",$params));

        $m = $row['module_name'];
        if (in_array($m,$modules->which_has_method('module_params_interface'))) {
            $row['textarea'] = $module_params_interface;
            $row['mtable'] = $mtable;
            $module_params_interface = $modules->_include($m, 'module_params_interface', array($row));
        }

        array_push($r,$module_params_interface);
        array_push($r,sprintf("<select id='module-ena-$id' name='enabled' style='font-size:1.4em' size=2>".selected_option(array(str_true.'::t',str_false.'::f'),$row['enabled'])."</select>"));
        array_push($r,sprintf("<select id='module-access-$id' style='font-size:1.4em' size=2>".selected_option(array('everybody::0','logined users::1'),$row['module_access'])."</select>"));
        array_push($r,sprintf("<select multiple id='module-gaccess-$id'>$access_groups_options</select>"));
        array_push($r,sprintf("<button name='copld' class='button-warning pure-button module_update' id='module-mod_".$id."'>".str_modifyit."</button>"));

        $cm = sprintf("<button class='button-gray button-xlarge pure-button'><i class='fa fa-cog'></i></button>");
        if (count($custom_menu) && in_array($row['module_name'], $custom_menu)) {
            $m = $modules->_include($row['module_name'],'getMenuItem');
            $cm = "<button class='module_submenu button-success button-xlarge pure-button' data-url='includes/project_admin.php?options={$m['url']}&mtable=$mtable'><i class='fa fa-cog'></i></button>";
        }
        array_push($r,$cm);

        array_push($r,sprintf("<select id='module-function-$id' size=2>%s</select>",selected_option(array('default','private'),$row['function'])));
        array_push($r,sprintf("%s<br>%s",button("ajax?module_export={$row['file']}",'export','fa fa-download'),"<input type='file' name='{$row['file']}' id='module_file-upload' title='upload .php file'>"));
        $tbl->addRows($r);
        $id++;
    }

    $tbl->addRows(array("<input list='modules' id='module-name-new'><input type='hidden' id='module-file-new' value=''>","<input id='module-params-new'>","<select id='module-ena-new'><option>TRUE</option><option>FALSE</option></select>","<select id='module-access-new'><option value='0'>everybody</option><option value='1'>logined users</option></select>","<select id='module-gaccess-new'>$access_groups_options</select>","<input type='button' name='aopld' class='button-success button-xlarge pure-button module_update' id='module-new_new' value='".str_add."'>","","<input type='hidden' id='module-function-new' value='default'>default",""));

    $tbl->addHeader(array('Module name','Parameters',t(str_enabled),t(str_access),t(str_group)." ".str_access,t(str_operations),'','Function',t(str_replace_module)));
    echo $tbl->printOut();

    echo "</div>";
} elseif(isset($_GET['options']) and $_GET['options']=='groups') {
    ## csoportok
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* Groups */
    echo "<h2>".str_groups."</h2><div class='be'>
    <div>".str_groupcreateexplain.".</div><br>";

    foreach ($csor as $csrow) {
        // skip user roles
        if ($csrow['user_id']!='') continue;

        $a = array();
        $tbl = new createTable();
        $tbl->def(['tid'=>'mytable','tclass'=>'resultstable']);

        printf("<h3><i>%s</i></h3>",t($csrow['description']));

        $cmd = "SELECT description, role_id, ARRAY_TO_STRING(container,',') AS g FROM project_roles WHERE project_table = '".PROJECTTABLE."' AND role_id!='{$csrow['role_id']}' ORDER BY description";
        $n = array();
        $res = pg_query($BID,$cmd);
        while($grow = pg_fetch_assoc($res)) {
            //$n[] = $grow['description']."::".$grow['role_id'];
            if (in_array($grow['role_id'],explode(',',$csrow['roles']))) $s = 'checked';
            else $s = '';
            $n[] = "<input class='ng-{$csrow['role_id']}' type='checkbox' value='{$grow['role_id']}' $s> ".$grow['description'];
        }

        //$options = selected_option($n,explode(',',$csrow['roles']));
        //$a[] = "<select name='nested_groups' id='ng-{$csrow['role_id']}' multiple>$options</select>";
        $a[] = '<div style="max-height:10em;overflow-y:scroll;">'.implode($n,'<br>').'</div>';
        $a[] = "<button class='button-warning button-large pure-button mrg_send' id='edg-{$csrow['role_id']}'><i class='fa fa-cog'></i> ".str_save."</button>";
        $a[] = "<button class='button-error button-large pure-button mrg_drop' id='edt-{$csrow['role_id']}'><i class='fa fa-trash'></i> ".str_delete."</button>";

        $tbl->addRows($a);

        $tbl->addHeader(array('included roles','','drop role'));
        echo $tbl->printOut();

    }
    echo "<br><h3>".t(str_new)." ".str_group.":</h3>
        <div class='pure-form pure-form-stacked'>
        <fieldset>";
    echo "<label for='crgr-name'>".t(str_group)." ".str_name.":</label>";
    echo "<input id='crgr-name' class='pure-u-1-5'>";
    echo "<button' id='crgr' class='pure-u-1-5 button-success button-large pure-button'><i class='fa fa-lg fa-cog'></i> ".str_create."</button></fieldset></div>";

    echo "</div>";

} elseif(isset($_GET['options']) and $_GET['options']=='members') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* tagok */
    echo "<h2>".str_members."</h2><div class='be'>
    <div>".str_groupmembercreateexplain.".</div>";

    $table = new createTable();
    $table->def(['tid'=>'tagok','tclass'=>'resultstable']);
    $table->tformat(['format_col'=>'1','format_string'=>'<a href=\'?profile=COL-8\'>COL-1</a>']);

    // query all members
    $cmd = "SELECT id,username,institute,address,email,round(validation,2) as validation,user_status,'groups' AS groups,\"user\"
            FROM users u
            LEFT JOIN project_users pu ON (pu.user_id=u.id)
            WHERE pu.project_table='".PROJECTTABLE."'
            ORDER BY familyname,givenname,username";

    $res = pg_query($BID,$cmd);
    while ( $user_row = pg_fetch_assoc($res) ) {

        # groups
        $options = array();
        foreach ($csor as $cs) {
            if ($cs['role_id']==$user_row['id']) continue;
            $options[] = "{$cs['description']}::{$cs['role_id']}";
        }

        $role_id = -1;
        $cmd = sprintf("SELECT role_id FROM project_roles WHERE user_id=%s AND project_table='%s'",quote($user_row['id']),PROJECTTABLE);
        $role_res = pg_query($BID,$cmd);
        if (pg_num_rows($role_res)) {
            $role_row = pg_fetch_assoc($role_res);
            $role_id = $role_row['role_id'];
        }

        $selected = array();
        $cmd = sprintf("SELECT role_id FROM project_roles WHERE %d=ANY(container)",$role_id);
        $rres = pg_query($BID,$cmd);
        while ($row = pg_fetch_assoc($rres))
            $selected[] = $row['role_id'];

        $options = selected_option($options,$selected);
        $user_row['groups'] = "<select name='edbox' id='dc-{$user_row['id']}' multiple size=4>$options</select>";

        $status = array('0'=>'banned','1'=>'normal','2'=>'master','banned'=>'banned','normal'=>'normal','master'=>'master');

        $user_row['user_status'] = $status[$user_row['user_status']];

        $options = selected_option(array(sprintf('%s::normal',str_normal),sprintf('%s::master::%s',str_master,str_maintainer_access_explanation),sprintf('%s::banned::%s',str_parked,str_zero_access_explanation)),$user_row['user_status']);
        $user_row['user_status'] = "<select name='edbox' id='sdc-{$user_row['id']}'>$options</select>";


        if ($user_row['id']==$_SESSION['Tid']) $col = "button-warning";
        else $col = "button-success";
        array_push($user_row,"<button class='$col button-large pure-button mrb_send' id='ed-{$user_row['id']}'><i class='fa fa-cog'></i> ".str_save."</button>");

        $table->addRows($user_row);
    }

    $table->addHeader(array(-1,str_nickname,str_institute,str_paddress,str_email,str_validation,str_status,str_group,-1,str_save));
    echo $table->printOut();
    echo "</div>";

/* mapserver */
} elseif(isset($_GET['options']) and $_GET['options']=='mapserv') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    echo "<h2>Mapserver ".str_map.' '.str_definitions."</h2><div class='be'>";

    echo "<h3>What are the mapfiles?</h3>
    <div>Read more about mapfiles here: <a href='http://mapserver.org/mapfile/'>http://mapserver.org/mapfile/</a></div><br><br>";

    echo "<h3>".t(str_proj_geom)."</h3>";
    $cmd = "SELECT Find_SRID('public', '".PROJECTTABLE."', '{$_SESSION['st_col']['GEOM_C']}')";
    $res = pg_query($ID,$cmd);

    echo "<div>";
    if ($res) {
        echo str_map_extent_text."<br>";
        $row = pg_fetch_assoc($res);
        echo '<b>'.str_data_srid.":</b> ".$row['find_srid'].", ".str_project_srid.": ".$_SESSION['st_col']['SRID_C'].'<br>';
        $res = pg_query($ID,"SELECT ST_Extent({$_SESSION['st_col']['GEOM_C']}) AS extent FROM ".PROJECTTABLE);
        $row = pg_fetch_assoc($res);
        $e = preg_replace("/[^0-9., ]/",'',$row['extent']);
        $e = preg_replace("/,/"," ",$e);
        echo '<b>'.str_data_extent.":</b> $e";
    } else {
        $d = pg_last_error($ID);
        echo str_no_srid."<br>$d";
        log_action($cmd,__FILE__,__LINE__);
    }
    echo "</div><br><br>";

    $map = getenv('PROJECT_DIR').'private/private.map';
    ob_start();
    passthru("grep LAYER $map -A 1|grep -i NAME");
    $var = ob_get_contents();
    ob_end_clean();
    $grep_layers = array_filter(preg_split('/\n|\r/',$var));
    $var = preg_replace('/NAME/i','',$var);
    $var = preg_replace('/\'/i','',$var);
    $var = preg_replace('/"/i','',$var);
    $map_layers = array_map('trim',array_filter(preg_split('/\n|\r/',$var)));

    $geom_types = array();
    foreach ($grep_layers as $gl) {
        $map = getenv('PROJECT_DIR').'private/private.map';
        ob_start();
        if (preg_match('/"/',$gl)) $quote = "'";
        else $quote = '"';
        passthru("grep $quote$gl$quote $map -A 10|grep -i \" TYPE [pl]\"");
        $var = ob_get_contents();
        ob_end_clean();
        $var = preg_replace('/\s+TYPE\s+/i','',$var);
        $geom_types[] = $var;
    }

    // echo mapserver layers
    echo '<h3>'.t(str_mapserver_layer)."</h3>The following layers are detected in this mapfile:";
    echo "<form id='mapfile-form'><table style='margin-left:30px'><tr><td>layer</td><td>geometry type</td></tr>";
    for ($i=0;$i<count($map_layers);$i++) {
        $cmd = sprintf("SELECT 1 FROM project_mapserver_layers WHERE mapserv_layer_name=%s AND project_table=%s AND geometry_type=%s",quote($map_layers[$i]),quote(PROJECTTABLE),quote(strtoupper(trim($geom_types[$i]))));
        $res = pg_query($BID,$cmd);
        $bgcolor = '';
        if (!pg_num_rows($res)) {
            $bgcolor = 'background-color:red';
        }

        echo "<tr><td><input name='mp[]' id='mp_$i' class='mapserver_layer' value='{$map_layers[$i]}' readonly style='$bgcolor'></td><td><input class='mapserver_layer_geometry' style='$bgcolor' name=mg[] id='mg_$i' value='{$geom_types[$i]}'></td></tr>";
    }
    echo '</table></form><br>';


    $private_map = file(getenv('PROJECT_DIR').'private/private.map');
    echo '<h3>OpenBioMaps '.str_private.' mapfile</h3>';
    if(!is_writable(getenv('PROJECT_DIR').'private/private.map')) {
        echo '<span class="err">'.str_file_not_writable.'</span>';
    } else {
        echo "<textarea style='width:1100px;height:600px;font-family:monospace' id='private_map'>";
        $template_vars = array();
        foreach ($private_map as $line) {
            $m = array();
            if (preg_match_all('/(@@\w+@@)/',$line,$m)) {
                foreach($m[1] as $q) $template_vars[]=$q;
            }
            echo $line;
        }
        echo "</textarea><br>";
        if (count(array_unique($template_vars))) {
            echo "<br><div style='color:red'>".str_replacvar.":<br>";
            foreach(array_unique($template_vars) as $var){
                echo $var."<br>";
            }
            echo "</div>";
        }

        echo "<br><button id='private' class='button-success button-large pure-button map_update'><i id='priv-icon' class='fa fa-cog'></i> ".str_save."</button> &nbsp; ";
        echo "<button id='getmaptest-private' class='button-secondary button-large pure-button maptest'><i class='fa fa-eye'></i> ".str_view."</button><br>";
    }
    /* AJAX check
     * */
    echo "Map check result:<div class='infobox' id='maptest-private'></div>";

    $public_map = file(getenv('PROJECT_DIR').'public/public.map');
    echo '<br><h3>'.t(str_public).' mapfile</h3>';
    echo '<div style="color:red;max-width:700px">This mapfile is not used by OpenBioMaps! If it is set properly, these layers will be accessible through OpenBioMaps proxy service without any restrictions to any mapserver client applications like QGIS or web map clients.</div>';
    if(!is_writable(getenv('PROJECT_DIR').'public/public.map')) {
        echo '<span class="err">'.str_file_not_writable.'</span>';
    } else {
        echo "<textarea style='width:1100px;height:300px;font-family:monospace;background-color:#efeafa' id='public_map'>";
        foreach ($public_map as $line) {
            echo $line;
        }
        echo "</textarea><br><button id='public' class='button-success button-large pure-button map_update'><i id='pub-icon' class='fa fa-cog'></i> ".str_save."</button> &nbsp; ";
        // AJAX check
        echo "<button id='getmaptest-public' class='button-secondary button-large pure-button maptest'><i class='fa fa-eye'></i> ".str_view."</button>";
    }
    echo "<br>".str_map_check_result.":<div class='infobox' id='maptest-public'></div>";
    echo "</div>";

/* Taxon
 * taxon_names
 * taxon name manager
 * */
} elseif(isset($_GET['options']) and $_GET['options']=='taxon') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    echo "<h2>".str_taxon_names."</h2><div class='be'>
    <div class='infotitle'></div>";

    $cmd = sprintf('SELECT count(*) as c FROM %1$s_taxon WHERE lang=%2$s',PROJECTTABLE,quote($_SESSION['st_col']['SPECIES_C']));
    $res = pg_query($ID,$cmd);
    $rows=pg_fetch_all($res);
    echo $rows[0]['c'].' '.str_taxon_name.'<br><br>';
    ?>
    <form>
    <table>
        <tr><td colspan=2 class='title' style='text-align:center;'><? echo str_taxon_filter; ?></td></tr>
        <tr><td class='title'><?php echo str_taxon_name?>:</td><td><input style='width:100%;font-size:15px' type='text' id='taxon_sim' value="" /></td></tr>
        <tr><td class='title' colspan=2><div id='tsellist'></div></td></tr>
        <tr><td colspan=2><input type='radio' id='onematch' name='match'> <?php echo str_one_match ?><br><input type='radio' id='allmatch' checked name='match'> <?php echo str_all_match ?></td></tr>
    </table>
    <br>
    <?php
    echo "<button class='pure-button button-success' id='taxonname_load'><i class='fa fa-search'></i> ".str_load."</button>";
    echo "</form>";
    echo "<br><div id='taxonlist'>";
    echo "</div>";

/* SQL trigger functions */
} elseif(isset($_GET['options']) and $_GET['options']=='functions') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }


    /* Postgresql functions */
    echo "<h2>".t(str_functions).': '.str_create.", ".str_update."</h2><div class='be'>
    <div class='infotitle'>".str_psql_function_help."</div>";


    $mtable = PROJECTTABLE;
    if (isset($_GET['mtable'])) $mtable = $_GET['mtable'];

    $st_col = st_col($mtable,'array');

    $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);
    $mrow = array();
    while ($row = pg_fetch_assoc($res)) {
        $mrow[] = $row['m'];
    }
    echo str_choose_mtable.": <select class='main_table_selector' id='functions'>".selected_option($mrow,$mtable)."</select><br>";

    $cmd = sprintf("SELECT event_object_schema as table_schema,
           event_object_table as table_name,
           trigger_schema,
           trigger_name,
           action_timing as activation,
           string_agg(event_manipulation, ',') as event,
           action_condition as condition,
           action_statement as definition
        FROM information_schema.triggers
        WHERE event_object_table='%s'
        GROUP BY 1,2,3,4,5,7,8
        ORDER BY table_schema,
         table_name",$mtable);

    $res = pg_query($ID,$cmd);
    $tbl = new createTable();
    $tbl->def(['tid'=>'cola','tclass'=>'resultstable']);
    $tbl->addHeader(array('table_schema','table_name','trigger_schema','trigger_name','event','activation','condition','definition'));

    while ($row = pg_fetch_assoc($res)) {
        $cmd = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='%s'",$row['trigger_name']);
        $res2 = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row2 = pg_fetch_assoc($res2);
            if ($row2['tgenabled'] == 'O') {
                $row['definition'] = "<span style='color:green;font-weight:bold'>".$row['definition']."</span>";
            } else {

                $row['definition'] = "<span style='color:orange;font-weight:bold'>".$row['definition']."</span>";
            }
        }

        $tbl->addRows($row);
    }

    echo "<br>";
    echo $tbl->printOut();

    if (isset($st_col['SPECIES_C']) and $st_col['SPECIES_C']!='') {

        echo "<br><h3>".t(str_taxon_list_update).":</h3>";
        $on = 'off';
        $color = 'button-passive';
        $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='taxon_update_%s'",$mtable);
        $result = pg_query($ID,$trigger_enabled);
        $cbc = "button-warning";
        if ( pg_num_rows($result) ) {
            $row = pg_fetch_assoc($result);
            if ($row['tgenabled'] == 'O') {
                $on = 'on';
                $color = 'button-success';
            }
            $cbc = "button-passive";
        }

        ## proname MAX 64 character!!!
        echo "<b>update_".$mtable."_taxonlist()</b> <button id='taxonlist-".$mtable."' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button> <button id='taxonlist_trigger-".$mtable."' class='enablefunction $color pure-button'><i class='fa-toggle-$on fa'></i></button>";
        $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname='update_%s_taxonlist'",$mtable,'%');
        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $count = (substr_count($row['prosrc'], "\n") + 1);
        echo sprintf("<br><textarea style='font-family:\"Courier New\",monospace;white-space:pre;font-size:12px;width:900px;' rows='%s'>{$row['prosrc']}</textarea>",$count);


        echo "<br><br><h3>".t(str_taxon_name_update).":</h3>";
        $on = 'off';
        $color = 'button-passive';
        $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='%s_name_update'",PROJECTTABLE);
        $result = pg_query($ID,$trigger_enabled);
        $cbc = "button-warning";
        if ( pg_num_rows($result) ) {
            $row = pg_fetch_assoc($result);
            if ($row['tgenabled'] == 'O') {
                $on = 'on';
                $color = 'button-success';
            }
            $cbc = "button-passive";
        }

        echo "Trigger: <b>update_".PROJECTTABLE."_taxonname()</b> <button id='taxonname-".$mtable."' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button>  <button id='taxonname_trigger-".PROJECTTABLE."' class='enablefunction $color pure-button'><i class='fa-toggle-$on fa'></i></button>";
        $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname='update_%s_taxonname'",PROJECTTABLE,'%');
        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $count = (substr_count($row['prosrc'], "\n") + 1);
        echo sprintf("<br><textarea style='font-family:\"Courier New\",monospace;white-space:pre;font-size:12px;width:900px;' rows='%s'>{$row['prosrc']}</textarea>",$count);
    }

    // HISTORY -----
    echo "<br><br><h3>".str_history_create.":</h3>";
    $on = 'off';
    $color = 'button-passive';
    $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='history_update_%s'",$mtable);
    $result = pg_query($ID,$trigger_enabled);
    $cbc = "button-warning";
    if ( pg_num_rows($result) ) {
        $row = pg_fetch_assoc($result);
        if ($row['tgenabled'] == 'O') {
            $on = 'on';
            $color = 'button-success';
        }
        $est_cmd = sprintf("SELECT reltuples AS approximate_row_count FROM pg_class WHERE relname = '%s_history'",PROJECTTABLE);
        $result = pg_query($ID,$est_cmd);
        $row=pg_fetch_assoc($result);
        echo "Estimated rows count in history table: ".$row['approximate_row_count']."<br>";
        $cbc = "button-passive";
    }

    echo "<b>".$mtable."_history()</b> <button id='history-".$mtable."' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button> <button id='history_trigger-".$mtable."' class='enablefunction $color pure-button'><i class='fa-toggle-$on fa'></i></button>";
    $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname='history_%s'",$mtable,'%');
    $res = pg_query($ID,$cmd);
    $row = pg_fetch_assoc($res);
    $count = (substr_count($row['prosrc'], "\n") + 1);
    echo sprintf("<br><textarea style='font-family:\"Courier New\",monospace;white-space:pre;font-size:12px;width:900px;' rows='%s'>{$row['prosrc']}</textarea>",$count);


    # Rules functions...
    #
    $on = 'off';
    $color = 'button-passive';
    $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='rules_%s'",$mtable);
    $result = pg_query($ID,$trigger_enabled);
    $cbc = "button-warning";
    if ( pg_num_rows($result) ) {
        $row = pg_fetch_assoc($result);
        if ($row['tgenabled'] == 'O') {
            $on = 'on';
            $color = 'button-success';
        }
        $cbc = "button-passive";
    }

    echo "<br><br><h3>".t(str_rules_create).":</h3>";
    echo "<b>".$mtable."_rules()</b> <button id='rules-".$mtable."' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button> <button id='rules_trigger-".$mtable."' class='enablefunction $color pure-button'><i class='fa-toggle-$on fa'></i></button>";

    $cmd = "SELECT id,username,institute,address,email,round(validation,2) as validation,user_status,'groups' AS groups,\"user\"
            FROM users u
            LEFT JOIN project_users pu ON (pu.user_id=u.id)
            WHERE pu.project_table='".PROJECTTABLE."'
            ORDER BY familyname,givenname,username";

    $res = pg_query($BID,$cmd);
    while ( $user_row = pg_fetch_assoc($res) ) {

        $options = array();
        foreach ($csor as $cs) {
            if ($cs['role_id']==$user_row['id']) continue;
            $options[] = "{$cs['description']}::{$cs['role_id']}";
        }
    }

        $selected = array();
        /*
        $cmd = sprintf("SELECT role_id FROM project_roles WHERE project_table='%s' ORDER BY description",PROJECTTABLE);
        $rres = pg_query($BID,$cmd);
        while ($row = pg_fetch_assoc($rres))
            $selected[] = $row['role_id'];
        */

    $options = selected_option($options,$selected);
    $groups_sel = "<select style='vertical-align:bottom' id='groups-to-rule' multiple size=4>$options</select>";
    echo " Add group to rule: " . $groups_sel . " <button id='rule-add-group' class='pure-button button-success'> <i class='fa fa-user-plus fa-large'></i> </button>";

    echo " " . t(str_save)." <button id='rule_save-$mtable' class='rule-save pure-button button-warning'> <i class='fa fa-floppy-o fa-large'></i> </button>";

    $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname LIKE 'rules_%s'",$mtable,'%');
    $res = pg_query($ID,$cmd);
    $row = pg_fetch_assoc($res);
    $count = (substr_count($row['prosrc'], "\n") + 1);
    echo sprintf("<br><textarea id='rule-function' style='font-family:\"Courier New\",monospace;white-space:pre;font-size:12px;width:900px;' rows='%s'>{$row['prosrc']}</textarea>",$count);

    echo "</div>";


} elseif(isset($_GET['options']) and $_GET['options']=='upload_forms') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    /* upload forms  - feltöltő formok - ürlap -  kitöltő ív
     *
     * */
    echo "<h2>".t(str_upload_forms);
    
    if (isset($_GET['editor'])) $editor = $_GET['editor'];
    else if (isset($_GET['edit_form'])) $editor = 'edit';
    else $editor = 'new';

    if ($editor == 'edit')
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=upload_forms&editor=new' title='".t(str_new)."  ".str_form."' class='admin_direct_link pure-button button-href' style='margin-top:-10px;float:right'>".t(str_new)." ".str_form." <i class='fa fa-gavel'></i></a>";
    else
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=upload_forms&editor=edit' title='".t(str_edit_form)."' class='admin_direct_link pure-button button-href' style='margin-top:-10px;float:right'>".t(str_edit_form)." <i class='fa fa-pencil-square-o'></i></a>";

    echo "</h2>";

    if ($editor == 'edit') {
        // list available forms
        //$cmd = "SELECT form_id,form_name,active,published FROM project_forms WHERE project_table='".PROJECTTABLE."' ORDER BY form_name";
        $cmd = sprintf('(SELECT form_id,form_name,form_id,active,published, published_form_id 
            FROM project_forms p1 
            WHERE project_table=\'%1$s\' AND published IS NULL
            ORDER BY form_name)
            UNION
            (
            SELECT t1.form_id,t1.form_name,t1.form_id,t1.active,t1.published, t1.published_form_id  FROM project_forms t1
            JOIN 
            (
               SELECT form_name,MAX(published) AS MAXDATE
               FROM project_forms
               WHERE project_table=\'%1$s\'
               GROUP BY form_name
            ) t2
            ON t1.form_name = t2.form_name
            AND t1.published = t2.MAXDATE
            WHERE t1.project_table=\'%1$s\')
            ORDER BY form_name',PROJECTTABLE);

        $res = pg_query($BID,$cmd);

        echo "<table class='resultstable' style='margin-bottom:20px'><tr><th>".t(str_form)." ".str_name."</th><th>".t(str_edit)."</th><th>".t(str_state)."</th><th>".t(str_delete)."</th></tr>";
        while($row = pg_fetch_assoc($res)) {
            if($row['active']==1) {
                $rd = t(str_ban);
                $fa = 'fa-ban';
            } else {
                $rd = t(str_activate);
                $fa = 'fa-check';
            }
            $label = $row['form_name'];
            if (preg_match('/^str_/',$label))
                if (defined($label))
                    $label = constant($label);

            echo "<tr>
            <td><span title='id: {$row['form_id']}'>$label</span></td>
            <td><button id='editform' data-tag='{$row['form_id']}' data-val='{$row['active']}' class='pure-button'><i class='fa fa-pencil-square-o fa-lg'></i> ".str_edit."</button></td>
            <td><button id='deacform' data-tag='{$row['form_id']}' data-val='{$row['active']}' class='pure-u-1-1 pure-button'><i class='fa $fa fa-lg text-warning'></i> $rd</button></td>
            <td><button id='delform' data-tag='{$row['form_id']}' data-val='{$row['active']}' class='pure-button'><i class='fa fa-trash fa-lg text-error'></i> ".str_delete."</button></td>
            </tr>";
        }
        echo "</table>";
    }

    //form header table
    $h = "";
    $name='';
    $s1_sel = array('','','');
    $s2_sel = array('','','');
    $g = array();
    $dg = array();
    $style_mod = "disabled readonly";
    $rclass='button-passive';
    $description = "";
    $dest_table = PROJECTTABLE;
    $srid_value = '';
    $published = '';
    $form_list_group = '';

    if (isset($_GET['edit_form'])) {
        $s1_values = array(0,1,2);
        $s2_values = array('file','web','api');
        $e = preg_replace('/[^0-9]/','',$_GET['edit_form']);
        $cmd = "SELECT form_name,array_to_string(\"form_type\",'|') as ft,form_access,array_to_string(\"groups\",'|') as g,description,destination_table,
                    ARRAY_TO_STRING(srid,',') as srid,array_to_string(\"data_groups\",'|') as dg, published
                FROM project_forms 
                WHERE form_id='$e' AND project_table='".PROJECTTABLE."'";
        $res2 = pg_query($BID,$cmd);
        $r2 = pg_fetch_assoc($res2);
        $name = $r2['form_name'];
        $published = $r2['published'];
        $description = $r2['description'];
        $srid_value = $r2['srid'];
        if ($r2['destination_table']!='')
            $dest_table = $r2['destination_table'];
        //valid request
        if ($name!='') {
            $h = $e;
        }
        //access selection
        $n=-1;
        $n=array_search($r2['form_access'],$s1_values);
        if($n>=0) {
            $s1_sel[$n]='selected';
            if($n==2) {
                $style_mod = "";
                $rclass = '';
            }
        }
        //type selection
        foreach(explode('|',$r2['ft']) as $ft) {
            $n=0;
            foreach($s2_values as $t) {
                if($ft==$t) {
                    $s2_sel[$n]='selected';
                }
                $n++;
            }
        }

        //group selection
        $g = explode('|',$r2['g']);
        $dg = explode('|',$r2['dg']);

        $cmd = sprintf("SELECT name FROM project_forms_groups WHERE project_table='%s' AND %d=ANY(form_id)",PROJECTTABLE,$e);
        $res3 = pg_query($BID,$cmd);
        if (pg_num_rows($res3)) {
            $row3 = pg_fetch_assoc($res3);
            $form_list_group = $row3['name'];
        }
    }

    //access groups
    $n = array();
    foreach ($csor as $row) {
        //$sel = "";
        //if (in_array($row['role_id'],$g)) $sel = "selected";
        //$groups .= "<option value='{$row['group_id']}' $sel>".$row['description']."</option>";
        $n[] = $row['description']."::".$row['role_id'];
    }
    $access_groups_options = selected_option($n,$g);

    //desnitaion groups
    $n = array();
    foreach ($csor as $row) {
        //$sel = "";
        //if (in_array($row['role_id'],$dg)) $sel = "selected";
        //$dgroups .= "<option value='{$row['role_id']}' $sel>".$row['description']."</option>";
        $n[] = $row['description']."::".$row['role_id'];
    }
    $destination_groups_options = selected_option($n,$dg);

    /* form definitions */
    # IDE JÓ LENNE VALAMI PLUGIN szerű dolog egyedi kiegészítőknek
    /*$cmd = sprintf('SELECT 1 FROM projects WHERE \'%1$s_projects\'=ANY(main_table) AND project_table=\'%1$s\'',PROJECTTABLE);
    if(pg_query($BID,$cmd)){
        #$cmd = sprintf('SELECT %1$s_projects.id as id,nev,modszer FROM %1$s_projects LEFT JOIN %1$s_methods ON (%1$s_methods.id=ANY(modszerek)) GROUP BY nev,modszer,%1$s_projects.id',PROJECTTABLE);
        $cmd = sprintf('SELECT %1$s_projects.id as id,nev,orszagos,helyi_program FROM %1$s_projects ORDER by nev',PROJECTTABLE);
        $pm_res = PGquery($ID,$cmd);
    }*/


    if ($editor == 'edit' and !isset($_GET['edit_form'])) {
        goto no_form_editor_displayed;
    }

    if (isset($_GET['edit_form'])) {
        echo "<h3>".t(str_edit_form)."</h3>";
        $to = "<option>$dest_table</option>";
    } else {
        echo "<h3>".t(str_new)." ".str_form."</h3>";
        $to = '';

        $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
        $res = pg_query($BID,$cmd);

        while ($row = pg_fetch_assoc($res)) {
            $s = "";
            if (isset($_GET['new_form_table']) and $_GET['new_form_table']==$row['m']) $s = "selected";
            elseif (!isset($_GET['new_form_table']) and $row['m']==PROJECTTABLE) $s = "selected";
            $to .= "<option $s>{$row['m']}</option>";
        }
    }
    echo "<form method='post' id='upl-form'><input type='hidden' id='edit_form_value' value='$h'>";
    echo "<table class='resultstable'>";

    echo "<tr><th>".t(str_form)." ".str_table.":<br>".wikilink('upload_forms.html#destination-table',str_what_this,'faq-light')."</th>
                <td><select name='formtable' id='form_table'>$to</select></td>
                <td colspan=2><input type='hidden' id='form-published-at' value='not-published'>";

    if (isset($_GET['edit_form'])) {
        if ($published=='')
            echo "<input type='hidden' id='published-at' value='now'><button class='pure-button button-warning button-xlarge pure-u-1-2' id='form-publish'>".str_publish." <i class='fa fa-fw fa-lock fa-lg'></i></button> ".wikilink('upload_forms.html#form-publish',str_what_this);
        else
            echo "<input type='hidden' id='published-at' value='$published'><i class='fa fa-fw fa-lock fa-lg'></i> Published at ".$published;
    }

    echo "</td></tr>";

    echo "<tr><th>".t(str_form)." ".str_name.":<br>".wikilink('upload_forms.html#name-of-the-form',str_what_this,'faq-light')."</th>
                <td><input name='formname' id='form_name' class='pure-u-1' value='$name'></td>
                <th colspan=2>".t(str_description).":<br>".wikilink('upload_forms.html#form-description',str_what_this,'faq-light')."</th></tr>";
    
    echo "<tr><th>".t(str_form)." ".str_type."::<br>".wikilink('upload_forms.html#form-type',str_what_this,'faq-light')."</th>
            <td><select name='type' class='text-lg pure-u-1' id='form_type' multiple='multiple' size=3>
                    <option value='web' {$s2_sel[1]}>".t(str_web_form)."</option>
                    <option {$s2_sel[0]} value='file'>".t(str_file_upload)."</option>
                    <option {$s2_sel[2]} value='api'>API (mobile)</option></select></td>
            <td colspan='2' rowspan='3' style='vertical-align:top'>
                <textarea name='description' id='form_description' style='width:50em;height:15em;resize:both'>$description</textarea></td>
            </tr>";

    echo "<tr><th>".t(str_form)." ".str_access.":<br>".wikilink('upload_forms.html#form-access',str_what_this,'faq-light')."</th>
            <td><select name='restriction' id='form_access' class='pure-u-1'>
                    <option value=0 {$s1_sel[0]}>".str_public."</option>
                    <option value=1 {$s1_sel[1]}>all logined users</option>
                    <option value=2 {$s1_sel[2]}>only specified groups</option></select></td></tr>";
    echo "<tr><th>".t(str_group)." ".str_access.":<br>".wikilink('upload_forms.html#group-access',str_what_this,'faq-light')."</th>
            <td><select multiple name='group_restriction' class='pure-u-1 $rclass' $style_mod id='form_group_access'>$access_groups_options</select></td></tr>";

    echo "<tr><th>".t(str_group)." ".str_data_assign.":<br>".wikilink('upload_forms.html#data-access',str_what_this,'faq-light')."</th>
            <td><select multiple name='data_access' id='form_data_access'>$destination_groups_options</select></td>
            <th>form group:<br>".wikilink('upload_forms.html#form-groupping',str_what_this,'faq-light')."</th><td><input id='form-list-group' list='form-groups' class='pure-u-1' value='$form_list_group'></td></tr>";

    echo "<tr><th>".t(str_form)." <a href='http://spatialreference.org/' target='_blank'>epsg srid</a>:<br>".wikilink('upload_forms.html#form-srid',str_what_this,'faq-light')."</th>
            <td colspan=3><input class='pure-u-1' name='srid' id='form_srid' placeholder='4326:WGS84,23700:EOV' value='$srid_value'></td></tr>";
    echo "</table><br>";
        
    echo '<datalist id="form-groups">';
    $cmd = sprintf("SELECT name FROM project_forms_groups WHERE project_table='%s' ORDER BY name",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);
        echo "<option value='{$r['name']}'>";
    }
    echo '</datalist>';


    if (isset($_GET['new_form_table'])) $dest_table = $_GET['new_form_table'];

    /* form columns */
    if ($dest_table) {

        echo "<table class='resultstable ctable' id='table_$dest_table'><tr>
            <th>".t(str_included)."?<br>".wikilink('upload_forms.html#included',str_what_this,'faq-light')."</th>
            <th>".t(str_order)."<br>".wikilink('upload_forms.html#column-order',str_what_this,'faq-light')."</th>
            <th>".t(str_column)."<br>".wikilink('upload_forms.html#column',str_what_this,'faq-light')."</th>
            <th>".t(str_obligatory)."<br>".wikilink('upload_forms.html#obligatory',str_what_this,'faq-light')."</th>
            <th>".t(str_description)."<br>".wikilink('upload_forms.html#column-description',str_what_this,'faq-light')."</th>
            <th>".t(str_type)."<br>".wikilink('upload_forms.html#column-type',str_what_this,'faq-light')."</th>
            <th>".t(str_input)." ".str_control."<br>".wikilink('upload_forms.html#input-control',str_what_this,'faq-light')."</th>
            <th>".t(str_list)." ".str_definition."<br>".wikilink('upload_forms.html#list-definition',str_what_this,'faq-light')."</th>
            <th>".t(str_default_values)."<br>".wikilink('upload_forms.html#default-values',str_what_this,'faq-light')."</th>
            <th>".t(str_api_params)."<br>".wikilink('upload_forms.html#api-params',str_what_this,'faq-light')."</th>
            <th>".t(str_relation)."<br>".wikilink('upload_forms.html#column-relations',str_what_this,'faq-light')."</th>
            <th>".t(str_pseudo_column)."<br>".wikilink('upload_forms.html#pseudo-columns',str_what_this,'faq-light')."</th></tr>";

        $type_values=array('text','numeric','list','point','line','polygon','wkt','date','datetime','time','file_id','boolean','autocomplete','timetominutes','tinterval','crings','autocompletelist','array');
        $control_values=array('nocheck','minmax','regexp','spatial','custom_check');

        $g = dbcolist('array',$dest_table);
        $g['obm_id'] = 'Edit ID';


        /* It would be possible to get low level description...
         * $cmd = sprintf('SELECT
                    cols.column_name,
                    (
                        SELECT
                            pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                        FROM
                            pg_catalog.pg_class c
                        WHERE
                            c.oid = (SELECT (\'"\' || cols.table_name || \'"\')::regclass::oid)
                            AND c.relname = cols.table_name
                    ) AS column_comment
                FROM
                    information_schema.columns cols
                WHERE
                    cols.table_catalog    = \'%1$s\'
                    AND cols.table_name   = \'%2$s\'
                    AND cols.table_schema = \'public\'',gisdb_name,$dest_table);

        $res5 = pg_query($ID,$cmd);
        $comment_rows = pg_fetch_all($res5);
        $comments = array_column($comment_rows, 'column_comment', 'column_name');*/

        if (isset($_GET['edit_form'])) {
            $cmd = "SELECT \"column\",position_order FROM project_forms_data WHERE form_id=$e ORDER BY position_order";
            $respo = pg_query($BID,$cmd);
            $column_positions = pg_fetch_all($respo);
            $columns = array_column($column_positions, 'column');
            $positions = array_column($column_positions, 'position_order');

            // [{"column":"adatkozlo","position_order":"1"},{"column":"altema","position_order":"2"},
            // {"adatkozlo":"adatk\u00f6zl\u0151","szamossag":"szamossag","gyujto":"gyujto",
            if (array_filter($positions))
                $g = array_merge(array_flip($columns), $g);
        }

        foreach($g as $k=>$column_label) {
            $cmd = "SELECT column_name, data_type,character_maximum_length FROM information_schema.columns WHERE table_name = '$dest_table' AND column_name = '$k'";
            $rest = pg_query($ID,$cmd);
            $rowt = pg_fetch_assoc($rest);
            $db_type = $rowt['data_type'];
            if ($rowt['character_maximum_length']!='')
                $db_type .= " ({$rowt['character_maximum_length']})";


            $cmd = "SELECT description FROM project_metaname WHERE project_table='$dest_table' AND column_name='$k'";
            $resm = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($resm);

            $type_sel=array('','','','','','','','','','','','','','','','','','');
            $selected_control = '';
            $fullist_sel=array('','');
            $obl_sel=array('','','');
            $count = '';
            $list='';
            $real_list=$list;
            $checked='';
            $oblcolor1='';
            $oblcolor2='';

            $default_value = '';
            $api_params = array('sticky','hidden','readonly','list_elements_as_buttons','once');
            $api_params_options = array();
            foreach ($api_params as $t) {
                    $api_params_options[] = "<option>$t</option>";
            }

            $relation = '';
            $pseudo_columns = '';
            $position_order = '';

            //if (isset($comments[$k]) and $comments[$k]!='')
            //    $row['description'] = $comments[$k];

            if (isset($_GET['edit_form'])) {
                $e = preg_replace('/[^0-9]/','',$_GET['edit_form']);
                // az OR csak kompatibilitás miatt van
                $cmd = "SELECT custom_function,\"description\",\"type\",\"control\",array_to_string(\"count\",':') as cn,array_to_string(list,',') AS list,\"obl\",\"fullist\",default_value,\"column\",genlist,
                    api_params,relation,regexp,ST_AsText(spatial) AS spatial,pseudo_columns,list_definition,position_order,column_label
                        FROM project_forms_data
                        WHERE \"form_id\"=$e AND \"column\"='$k'";

                $res2 = pg_query($BID,$cmd);
                $r2 = pg_fetch_assoc($res2);

                if ($r2) {

                    $k = $r2['column'];
                    $row['description'] = $r2['description'];

                    if ($r2['column_label']!='')
                        $column_label = $r2['column_label'];

                    //type
                    $n=-1;
                    $n=array_search($r2['type'],$type_values);
                    if($n>=0) $type_sel[$n]='selected';

                    //control
                    $n=-1;
                    $n=array_search($r2['control'],$control_values);
                    if($n>=0) {
                        $selected_control = $control_values[$n];
                    }

                    //count
                    $count = $r2['cn'];
                    if($r2['regexp']!='')
                        $count = $r2['regexp'];

                    if($r2['spatial']!='')
                        $count = $r2['spatial'];

                    if($r2['custom_function']!='')
                        $count = $r2['custom_function'];

                    //list select option show
                    $list=$r2['list'];
                    $real_list = $list;
                    $glist=$r2['genlist'];
                    if ($glist != '') {
                        $list = $glist;
                    }
                    if ($r2['list_definition']!='') {
                        $l = json_decode($r2['list_definition']);

                        // user friendly list definition
                        $list = [];
                        $real_list = $list;
                        if (isset($l->list)) {

                            foreach ($l->list as $value => $keys) {

                                $ll = '';
                                if (count($keys))
                                    $ll .= implode('#',$keys). ':';

                                $ll .= ($value == '_empty_') ? '' : $value;

                                $list[] = $ll;
                            }
                            $list = implode(', ',$list);
                        }
                        elseif (isset($l->optionsTable)) {
                            //select elements from table
                            $list = "SELECT:".$l->optionsTable.".".$l->valueColumn;
                            if (isset($l->labelColumn) and $l->labelColumn!='')
                                $list .= ":".$l->labelColumn;

                        }
                        else
                            $list = json_encode(json_decode($r2['list_definition']),JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);

                        $real_list = json_encode(json_decode($r2['list_definition']),JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
                    }
                    // mandatory check
                    $checked='checked';
                    if ($r2['obl']==1) {
                        $oblcolor2=$oblcolor1="style='background:#FF636E'";
                        $obl_sel[0] = 'checked';
                    }
                    elseif ($r2['obl']==2) {
                        $oblcolor2=$oblcolor1="style='background:#B6B6B6'";
                        $obl_sel[1] = 'checked';
                    }
                    elseif ($r2['obl']==3) {
                        //soft error
                        $oblcolor2=$oblcolor1="style='background:#FFB6BB'";
                        $obl_sel[2] = 'checked';
                    }

                    if ($r2['fullist']==0)
                        $fullist_sel[0] = 'selected"';
                    else
                        $fullist_sel[1] = 'selected';

                    if ($r2['default_value']==NULL) $default_value = '';
                    else $default_value = $r2['default_value'];

                    $ap = array();
                    $api_params_options = array();
                    if ($r2['api_params']!=NULL and $r2['api_params']!='') {
                        $ap = json_decode($r2['api_params'],TRUE);
                    }
                    foreach ($api_params as $t) {
                        $sel_par = '';
                        if (in_array($t,$ap)) {
                            $sel_par = 'selected';
                        }
                        $api_params_options[] = "<option $sel_par>$t</option>";
                    }

                    if ($r2['relation']==NULL) $relation = '';
                    else {
                        $relation = $r2['relation'];
                        $oblcolor1="style='background:#73CBFE'";
                    }

                    if ($r2['pseudo_columns']==NULL) $pseudo_columns = '';
                    else {
                        $pseudo_columns = $r2['pseudo_columns'];
                    }

                    if ($r2['position_order']==NULL) $position_order = '';
                    else {
                        $position_order = $r2['position_order'];
                    }
                }
            }
            $coptions = selected_option(array_merge(array(str_no_check.'::nocheck','min : max::minmax','regexp::regexp',str_spatial.'::spatial',str_custom_check.'::custom_check')),$selected_control);

            $trstyle = '';
            $typeselstyle = '';
            if ($k=='obm_id') $trstyle = "style='background-color:yellow'";
            elseif ($k=='obm_files_id' and $type_sel[10]!='selected' and $checked=='checked') $typeselstyle = "style='background-color:darkorange'";
            elseif ($type_sel[0]=='selected' and $checked=='checked' and !preg_match('/^(character)|(text)/',$db_type)) $typeselstyle = "style='background-color:darkorange'";

            // automatic type selection
            if ($checked=='' and $db_type=='integer') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='numeric') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='real') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='double precision') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='smallint') {
                $type_sel[1]='selected';
                $coptions = selected_option(array_merge(array(str_no_check.'::nocheck','min : max::minmax','regexp::regexp',str_spatial.'::spatial',str_custom_check.'::custom_check')),'minmax');
                $count = "-32768:32767";
            }
            elseif ($checked=='' and $db_type=='date') $type_sel[7]='selected';
            elseif ($checked=='' and preg_match('/^timestamp with/',$db_type)) $type_sel[8]='selected';
            elseif ($checked=='' and preg_match('/^time /',$db_type)) $type_sel[9]='selected';
            elseif ($checked=='' and preg_match('/^character varying \((\d+)\)/',$db_type,$m)) {
                $type_sel[0]='selected';
                $coptions = selected_option(array_merge(array(str_no_check.'::nocheck','min : max::minmax','regexp::regexp',str_spatial.'::spatial',str_custom_check.'::custom_check')),'minmax');
                $count = "0:$m[1]";
            }
            elseif ($checked=='' and $k=='obm_files_id') $type_sel[10]='selected'; // automatic type selection

            // UPLOAD FORM-EDITOR TABLE
            echo "<tr $trstyle><td class='uplf_color1-$k' $oblcolor1><input type='checkbox' class='uplf_ckb' name='uplf_ckb[]' value='1' id='uplf_ckb-$k' $checked></td><td class='uplf_color1-$k' $oblcolor1><input id='uplf_order-$k' value='$position_order' list='available_items' class='available_item_trigger uplf_column_order' size=2 style='text-align:center'></td><td class='uplf_color2-$k' $oblcolor1><div contenteditable='true' id='uplf_column_label-$k'>$column_label</div><i>$k</i></td>
 <td class='uplf_color3-$k' $oblcolor2>
    <div style='vertical-align:middle'><input type='radio' name='obl-$k' id='uplf_obl_y-$k' class='uplf_obl' name='obligatory' title='Choose yes, if this field should not be empty' value='1' style='vertical-align:middle' {$obl_sel[0]}> ".str_yes."</div>
    <div style='vertical-align:middle'><input type='radio' name='obl-$k' id='uplf_obl_n-$k' class='uplf_obl' name='obligatory' title='Choose no, if this field can be empty' style='vertical-align:middle' value='2' {$obl_sel[1]}> ".str_no."</div>
    <div style='vertical-align:middle'><input type='radio' name='obl-$k' id='uplf_obl_s-$k' class='uplf_obl' name='obligatory' title='Choose soft, if this field should not be empty, but possible' value='3' style='vertical-align:middle' {$obl_sel[2]}> ".str_soft_error."</div>
</td>
<td><textarea name='uplf_description[]' id='uplf_description-$k' style='resize:both'>{$row['description']}</textarea></td><td $typeselstyle>$db_type<br><select class='uplf_type' name='uplf_tpye[]' id='uplf_type-$k'><option title='any type of characters allowed' {$type_sel[0]} value='text'>".t(str_text)."</option><option title='only numeric input allowed' {$type_sel[1]} value='numeric'>".t(str_numeric)."</option><option title='input from predefined list' {$type_sel[2]} value='list'>".t(str_list)."</option><option title='wkt geometry definition' value='point' {$type_sel[3]}>".t(str_geometry.':',str_point)."</option><option title='wkt geometry definition' value='line' {$type_sel[4]}>".t(str_geometry.':',str_line)."</option><option title='wkt geometry definition' value='polygon' {$type_sel[5]}>".t(str_geometry.':',str_polygon)."</option><option value='wkt' title='wkt geometry definition' {$type_sel[6]}>".t(str_geometry.':',str_any)."</option><option value='date' {$type_sel[7]}>".t(str_date)."</option><option value='datetime' {$type_sel[8]}>".t(str_datetime)."</option><option value='time' {$type_sel[9]}>".t(str_time)."</option>
<option value='timetominutes' {$type_sel[13]}>".t(str_time_to_minutes)."</option>
<option value='tinterval' {$type_sel[14]}>".t(str_time_interval)."</option>
<option {$type_sel[10]} value='file_id'>File connect</option><option value='boolean' {$type_sel[11]}>".t(str_boolen)."</option><option value='autocomplete' {$type_sel[12]}>Autocomplete</option><option value='autocompletelist' {$type_sel[16]}>Autocomplete list</option><option value='crings' {$type_sel[15]}>Colour rings</option><option value='array' {$type_sel[17]}>".t(str_array)."</option></select></td>
<td><select class='uplf_length' id='uplf_length-$k' name='uplf-length' name='textlength'>$coptions</select><br><input id='uplf_count-$k' name='uplf_count' size='24' value='$count'></td>
<td><textarea class='magnify list-editor' id='uplf_list-$k' placeholder='JSON array' data-simplified='$list' style='resize:both'>";
    echo $real_list;
echo "</textarea></td>";
        echo "<td><input id='uplf_defval-$k' value='$default_value' list='default_options' placeholder='dblclick to get options'></td>";
        echo "<td><select multiple id='uplf_api_params-$k'>".implode('',$api_params_options)."</select></td>";
        echo "<td><textarea class='magnify uplf_relation' id='uplf_relation-$k' placeholder='(column_name=condition) {function(parameter);function(parameter)},..' style='resize:both'>$relation</textarea></td>";
        echo "<td><input class='magnify' id='uplf_pseudocol-$k' placeholder='form_name:column1,column2' value='$pseudo_columns'></td>";
        echo "</tr>";

        }
        echo "</table>";
        echo '<datalist id="default_options">
    <option value="_input">
    <option value="_list">
    <option value="_geometry">
    <option value="_login_name">
    <option value="_email">
    <option value="_autocomplete">
    <option value="_boolean">
    <option value="_attachment">
    <option value="_datum">
    <option value="_auto_geometry">
    </datalist>';
        echo '<datalist id="available_items"></datalist>';

    }

    if ($published!='') {
        echo "<br><input type='submit' value=' ".str_update." ' class='button-success button-xlarge pure-button pure-u-1-5' id='upl-form-submit'>";
        echo " &nbsp; &nbsp; <input type='submit' value=' ".str_create_draft." ' class='button-success button-xlarge pure-button pure-u-1-5' id='draft-submit'>";
    } else {
        echo "<br><input type='submit' value=' ".str_save." ' class='button-success button-xlarge pure-button pure-u-1-5' id='upl-form-submit'>";
    }

    echo "<br><br><input type='hidden' name='new-form'>";
    echo "</form>";

    $lm_widget = ($modules->is_enabled('list_manager',$dest_table)) ? $modules->_include('list_manager','upload_form_builder_widget') : '' ;

    echo "<div id='list-editor'>
        <input type='hidden' id='list-editor-return'>
<div id='list-editor-help-box'>
    <span class='add-item'>list</span>: {
            option-1: [label-1, label-2]
    },
    <span class='add-item'>optionsSchema</span>: <div class='item-content' data-type='optionsSchema' data-value='' data-schema=''>string</div>,
    <span class='add-item'>optionsTable</span>: <div class='item-content' data-type='optionsTable' data-value='optionsSchema' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>valueColumn</span>: <div class='item-content' data-type='valueColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>labelColumn</span>: <div class='item-content' data-type='labelColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>filterColumn</span>: <div class='item-content' data-type='filterColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>pictures</span>:  <div class='item-content' data-type='pictures' data-value='list'>{ option-1: url-string }</div>,
    <span class='add-item'>triggerTargetColumn</span>: <div class='item-content' data-type='triggerTargetColumn' data-value=''>string</div>,
    <span class='add-item'>Function</span>: <div class='item-content' data-type='Function' data-value=''>string</div>,
    <span class='add-item'>disabled</span>: <div class='item-content' data-type='disabled' data-value='list'>[option-1]</div>,
    <span class='add-item'>preFilterColumn</span>: <div class='item-content' data-type='preFilterColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>preFilterValue</span>: string,
    <span class='add-item'>multiselect</span>: <div class='item-content' data-type='multiselect' data-value=''>boolean</div>,
    <span class='add-item'>selected</span>: <div class='item-content' data-type='selected' data-value='list'>[option-1]</div>,
    <span class='add-item'>size</span>: integer,
    <span class='add-item'>add_empty_line</span>: <div class='item-content' data-type='add_empty_line' data-value=''>boolean</div>
</div>

$lm_widget

         <!--<div style='position:relative'>Simple editor: <button class='pure-button button-small button-secondary pure-u-3-1' style='position:absolute;right:2px;top:20px'>".str_update."</button></div>
        <textarea id='list-editor-simple'></textarea><br>-->

        <div style='position:relative'><br><!-- <button class='pure-button button-small button-secondary pure-u-3-1' style='position:absolute;right:2px;top:20px'>".str_update."</button>--></div>
        <textarea id='list-editor-json'></textarea><br><br>

        <button class='pure-button button-secondary pure-u-3-1' id='list-editor-save'>".str_save."</button>
        <button class='pure-button button-gray pure-u-3-1' id='list-editor-cancel'>".str_cancel."</button>
        </div>";

    no_form_editor_displayed:
    echo "";

} elseif(isset($_GET['options']) and $_GET['options']=='server_logs') {
    include_once(getenv('OB_LIB_DIR').'interface.php');
    server_logs();

} elseif(isset($_GET['options']) and $_GET['options']=='r_server') {
    /* R Shiny server start/stop/status
     *
     * */
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    echo "<h2>R Shiny server</h2><div class='be'>";

    if (defined('RSERVER') and constant("RSERVER")) {

        $rport = 'RSERVER_PORT_'.PROJECTTABLE;
        $rport = constant($rport);
        // test, shiny server is running
        if (stest($_SERVER['SERVER_NAME'],$rport)) {
            echo "R-shiny server is running at {$_SERVER['SERVER_NAME']}:$rport<br>";
            echo "<button id='control-R-server' value='stop' class='pure-button button-warning'>stop R server</button>";
        } else {
            echo "R-shiny server is not running on {$_SERVER['SERVER_NAME']}:$rport!<br>";
            echo "<button id='control-R-server' value='start' class='pure-button button-success'>start R server</button>";
        }
    } else if (!defined('RSERVER')) {
        echo "RSERVER is not defined";

    } else if (defined('RSERVER') and !RSERVER) {
        echo "RSERVER is not enabled";
    }
    echo "</div>";

} elseif(isset($_GET['options']) and $_GET['options']=='create_table') {
    /* create table
     * add table
     * new table
     * */
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }
    echo "<h2>".t(str_create_table)."</h2><div class='be'>";

    echo "<form class='pure-form pure-form-stacked' style='width:400px'>";
    echo "<fieldset><legend>Create & add a new table in this project</legend>";
    echo "<div class='pure-control-group'>";
        echo '<label for="new_table">'.str_table_name.":</label> <input class='pure_input pure-u-1-2' id='new_table' maxlength='24' required><div>";
    echo "<div class='pure-control-group'>";
        echo '<label for="new_table_comment">'.str_table_comment.":</label> "."<input class='pure_input pure-u-1' id='new_table_comment' maxlength='128'></div>";
    echo "<div class='pure-controls'>";
        echo "<button id='create_new_table' class='pure-button button-warning'><i class='fa fa-cog'></i> ".str_create."</button></div></fieldset></form>";
    echo "</div>";
    echo "</div>";


} elseif (isset($_GET['options']) && $_GET['options'] == 'privileges') {
    // Page for managing the privileges of diffrent user types
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }

    echo "<h2>".t(str_group_privileges)."</h2><div class='be'>";

    $admin_pages = get_admin_pages_list();

    $pages  = array_column($admin_pages, 'page');
    $groups = array_column($admin_pages, 'group');
    $labels = array_column($admin_pages, 'label');
    array_multisort($groups, SORT_ASC, $labels, SORT_NATURAL | SORT_FLAG_CASE, $admin_pages);

    $user_classes = array();
    foreach ($csor as $csrow) {
        // skip user roles
        if ($csrow['user_id']!='') continue;
        $user_classes[$csrow['role_id']] = $csrow['description'];
    }

    $privileges = get_privileges();

    echo "<form class='pure-form pure-form-stacked' id='privileges-form'>";
    $tbl = new createTable();
    $tbl->def(['tid'=>'privileges-table','tclass'=>'resultstable']);
    $tbl->addHeader(array_merge([str_function],$user_classes));
    $rows = [];
    foreach ($admin_pages as $ap) {
        $label = $ap['label'];
        $page = $ap['page'];
        $group = $ap['group'];
        $row = [$label];
        foreach ($user_classes as $role_id=>$group_name) {
            $checked = (isset($privileges[$role_id]) && in_array($page,$privileges[$role_id]))
                ? 'checked="checked"'
                : '';
            $row[] = "<input type='checkbox' name='$role_id@$page' $checked>";
        }
        if ($group == 'project_admin' and $page!='privileges' and $page!='groups' and $page!='members')
            $tbl->addRows($row,'style="background-color:orange"');
        elseif ($page == 'privileges' or $page == 'groups' or $page == 'members')
            $tbl->addRows($row,'style="background-color:red;color:white"');
        else
            $tbl->addRows($row);
    }

    echo $tbl->printOut();
    echo "<input type='hidden' name='privileges' value='save'>";
    echo "<button class='button-warning button-xlarge pure-button' id='save_privileges' type='submit'><i id='ficon' class='fa fa-floppy-o'></i> ".str_save."</button></form>";
    echo "</div>";


} elseif (isset($_GET['options']) and in_array($_GET['options'], $modules->which_has_method('adminPage'))) {
    echo $modules->_include($_GET['options'], 'adminPage',['mtable' => $_GET['mtable']]);
}
?>
