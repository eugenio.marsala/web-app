<?php

    $mf = new mainpage_functions();
?>
<h2><?php echo t(str_project_statistics)?></h2>

<div class='leftbox boxborder'><h3><?php echo t(str_members)?></h3>
    <ul class='boxul'>
    <li class='blarge'><?php echo $mf->count_members(); ?></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_uploads)?></h3>
    <ul class='boxul'>
        <li class='blarge'><?php $mf->count_uploads_cache = 5;echo $mf->count_uploads(); ?></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_data)?></h3>
    <ul class='boxul'>
        <li class='blarge'>
        <?php 
            $mf->table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE ;
            echo $mf->count_data(); 
        ?>
        </li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_species)?></h3>
    <ul class='boxul'>
        <li class='blarge'><a href='?specieslist'><?php $mf->count_species_cache = 5; echo $mf->count_species(true)?></a></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_species_stat)?></h3>
    <ul class='boxul'>
        <li><?php echo $mf->stat_species()?></li>
    </ul>
</div>
