<?php
/*
 *
 *
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

// csak bejelentkezve és csak a saját profilunkat tudjuk szerkeszteni
if(!isset($_SESSION['Tid'])) {
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

$table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;

// Project level rights needed
if (!has_access('taxon')) exit;


if (isset($_POST['mispelled-update'])) {
    
    if (!isset($_SESSION['Tid'])) 
        $mfid = '0';
    else
        $mfid = $_SESSION['Tid'];

        $g = array();
        $g['id'] = preg_replace('/[^0-9]/','',$_POST['mispelled-taxonname-id']);
        $g['newname'] = $_POST['newname'];
        $g['name'] = $_POST['mispelled'];
        $g['lang'] = $_POST['mispelled-taxonname-lang'];
        /* elírás javítása */
              
        pg_query($ID,'BEGIN');
        $cmd = sprintf('SELECT taxon_id FROM %1$s_taxon WHERE lang=%3$s AND meta=replace(%2$s,\' \',\'\')',PROJECTTABLE,quote($g['newname']),quote($g['lang']));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)==0) {
            /* Ha nem létezik az új név még a taxon táblában 
             * itt csak a taxon név táblát frissítem, mert az update_..._taxonname trigger frissti az adat táblát! 
             * */
            $cmd = sprintf('UPDATE %1$s_taxon SET word=%2$s,meta=replace(%2$s,\' \',\'\'),modifier_id=%6$d WHERE taxon_id=%3$d AND lang=%4$s AND word=%5$s',PROJECTTABLE,quote($g['newname']),$g['id'],quote($g['lang']),quote($g['name']),$mfid);
            $res = pg_query($ID,$cmd);
            if (pg_affected_rows($res)) {
                $ok = 1;
            } else {
                log_action("Taxon update failed: ".pg_last_error($ID),__FILE__,__LINE__);
            }

        } else {
            /* Kitörlöm a taxon név táblából
             * Frissítem az adatbázist, ami triggerrel visszaírja a taxon táblában a nevet 
             * Ha mégsem futott le a trigger, akkor beszúrom a nevet taxon táblába
             * */
            
            $cmd_1 = sprintf('DELETE FROM %1$s_taxon WHERE taxon_id=%2$d AND lang=%3$s AND word=%4$s',PROJECTTABLE,$g['id'],quote($g['lang']),quote($g['name']));
            $cmd_2 = sprintf('UPDATE %1$s SET "%2$s"=%3$s,obm_modifier_id=%5$d WHERE "%2$s"=%4$s',$table,$g['lang'],quote($g['newname']),quote($g['name']),$mfid);
            $cmd_3 = '';
            //No ez jó, de alternévre még belerakok egy ellenőrzést a latin név egyezésre
            if ($g['lang'] != $_SESSION['st_col']['SPECIES_C']) {
               $cmd31 = sprintf('SELECT word FROM %1$s_taxon WHERE taxon_id=%2$d AND lang=%3$s',PROJECTTABLE,$g['id'],quote($g['lang']));
               $rs = pg_query($ID,$cmd31);
               $ra = pg_fetch_assoc($rs);
               $cmd_2 = sprintf('UPDATE %1$s SET "%2$s"=%3$s,obm_modifier_id=%5$d WHERE "%2$s"=%4$s',$table,$g['lang'],quote($g['newname']),quote($g['name']),$mfid);
            }
            $cmd31 = sprintf('SELECT taxon_id FROM %1$s_taxon WHERE lang=%2$s AND word=%3$s',PROJECTTABLE,quote($g['lang']),quote($g['newname']));
            $rs = pg_query($ID,$cmd31);
            if (!pg_num_rows($rs)) {
                $cmd_3 = sprintf('INSERT INTO %1$s_taxon (meta,word,lang,modifier_id) (%2$s,%3$s,%4$s,%5$d)',PROJECTTABLE,quote(preg_replace('/ /','',$g['name'])),quote($g['newname']),quote($g['lang']),$mfid);
            }

            //$cmd = $cmd_1.';'.$cmd_2.';'.$cmd_3;

            $ok = 0;
            if (pg_query($ID,$cmd_1)) {
                $ok = 1;
            }
            else {
                log_action($cmd_1,__FILE__,__LINE__);
            }
            if (pg_query($ID,$cmd_2)) {
                $ok = 1;
            }
            else {
                log_action($cmd_2,__FILE__,__LINE__);
            }
            if ($cmd_3!='') {
                if (pg_query($ID,$cmd_3)) {
                    $ok = 1;
                }
                else {
                    log_action($cmd_3,__FILE__,__LINE__);
                }
            }
        }
            
        if ($ok) {
            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
            pg_query($ID,"COMMIT;");
            $message = common_message('ok',"$protocol://".URL."/?metaname&id={$g['id']}");
        } else {
            pg_query($ID,"ROLLBACK;");
            $message = common_message('error',pg_last_error($ID));
        }

    echo $message;

    exit;
}

if (isset($_POST['ops'])) {

    $message = array();

    $jo = json_decode($_POST['ops']);
    #{"s_1-8410":{"id":"8410","accepted":0,"common":0,"synonym":0,"lang":"sci","name":"Angang"}}
    $v = get_object_vars($jo);
    
    /*$taxon_group = array_keys($v);
    $multi_sci = array();
    $multi_alt = array();
    foreach($taxon_group as $t) {
        //a_2-591
        $group_id = array();
        preg_match('/[as]_\d+-\d+/',$t,$group_id);
        if($group_id[1]=='s')
            $multi_sci[]=$group_id[3];
        if($group_id[1]=='a')
            $multi_alt[]=$group_id[3];
    }
    $multi_sci = array_unique($multi_sci);
    $multi_alt = array_unique($multi_alt);*/
    //$_POST['ops_connect'] = '["5","591"]';

    // Connect names
    if (isset($_POST['ops_connect'])) {
        $con = json_decode($_POST['ops_connect']);
        $con = array_unique($con);
        if (count($con) and $con[0]!='') {
            $focal = array_pop($con);
            if ($focal!='' and count($con)) {
                $cmd = sprintf('UPDATE %s_taxon SET taxon_id=\'%d\' WHERE taxon_id IN (%s)',PROJECTTABLE,$focal,implode(',',$con));
                pg_query($ID,$cmd);
            } else {
                $message[] = array('error','Already joined');
            }
        }
    }

    if (!isset($_SESSION['Tid'])) 
        $mfid = '0';
    else
        $mfid = $_SESSION['Tid'];

    /* Ez a ciklus az egyes elküldött nevekre */
    foreach ($v as $key=>$k) {
        $g = array();
        $g['accepted'] = preg_replace('/[^0-9]/','',$k->{'accepted'});
        $g['lang'] = preg_replace('/[^a-z_]/','',$k->{'lang'});
        $g['common'] = preg_replace('/[^0-9]/','',$k->{'common'});
        $g['synonym'] = preg_replace('/[^0-9]/','',$k->{'synonym'});
        $g['id'] = preg_replace('/[^0-9]/','',$k->{'id'});
        $g['name'] = $k->{'name'};

        if ($g['accepted']==1) {
            $cmd = sprintf("UPDATE %s_taxon SET status='1' WHERE taxon_id=%d and lang='%s' and word=%s",PROJECTTABLE,$g['id'],$g['lang'],quote($g['name']));
            $res = pg_query($ID,$cmd);
            if (pg_affected_rows($res)) 
                    //echo common_message('ok');
                    $message[] = array('ok','ok');


            /* üres helyek feltöltése a taxonnévvel: */
            if ($g['lang'] != $_SESSION['st_col']['SPECIES_C']) {
                $cmd = sprintf("SELECT meta FROM %s_taxon WHERE taxon_id=%d AND lang=%s",PROJECTTABLE,$g['id'],quote($_SESSION['st_col']['SPECIES_C']));
                $res = pg_query($ID,$cmd);
                $row = pg_fetch_assoc($res);
                if ($row) {
                    //$species = array_search($g['lang'],$species_array);
                    $cmd = sprintf('UPDATE "%1$s" SET "%2$s"=%3$s,obm_modifier_id=%6$d WHERE (%2$s=\'\' OR %2$s IS NULL) AND replace("%4$s",\' \',\'\')=%5$s',$table,$g['lang'],quote($g['name']),$_SESSION['st_col']['SPECIES_C'],quote($row['meta']),$mfid);
                    $res = pg_query($ID,$cmd);
                    if (pg_affected_rows($res)) 
                        //echo common_message('ok');
                        $message[] = array('ok','ok');
                }
            }
        } elseif ($g['common']==1) {
            $cmd = sprintf("UPDATE %s_taxon SET status='4' WHERE taxon_id=%d and lang='%s' and word=%s",PROJECTTABLE,$g['id'],$g['lang'],quote($g['name']));
            $res = pg_query($ID,$cmd);
            if (pg_affected_rows($res))
                    //echo common_message('ok');
                    $message[] = array('ok','ok');
        } elseif ($g['synonym']==1) {
            $cmd = sprintf("UPDATE %s_taxon SET status='3' WHERE taxon_id=%d and lang='%s' and word=%s",PROJECTTABLE,$g['id'],$g['lang'],quote($g['name']));
            $res = pg_query($ID,$cmd);
            if (pg_affected_rows($res))
                    //echo common_message('ok');
                    $message[] = array('ok','ok');
        } else {
            //echo common_message('error','nothing happened');
            $message[] = array('ok',str_nothing_happend. ": ".$g['name']);
        }
    }

    $ok = 0;
    foreach ($message as $m) {
        if ($m[0] == 'ok')
            $ok++;
    }
    if (count($message) == $ok)
        echo common_message('ok','ok');
    else
        echo common_message($message,null,1);
}
//pg_close($ID);
?>
