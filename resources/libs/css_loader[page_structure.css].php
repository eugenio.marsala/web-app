<?php
function setExpires($expires) {
header(
'Expires: '.gmdate('D, d M Y H:i:s', time()+$expires).'GMT');
}
setExpires(300);

header("Content-type: text/css", true);

if (file_exists('../css/private/page_structure.css')) {

    include('../css/private/page_structure.css');

} elseif (file_exists('../css/page_structure.css')) {
    
    include('../css/page_structure.css');

}
?>
