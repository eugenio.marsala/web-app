/* Map related variables and functions
 *
 * */
var map,nav,drawControls,highlightLayer,drawLayer,popup,markerLayer;
var dataLayers = new Array();
var dataLayers_ns = new Array();
var points = new Array();
//var proj = new OpenLayers.Projection("EPSG:4326");
var parallel = 0; // párhuzamos szálak számlálója
var zoomTo = null; //zoomTo features.geometry
var zoom = 16;
var myQVar = {} // used by all query modules 

/* OpenLayers styles */
var highlight_style = { 
    pointRadius:15,
    strokeColor:'#FFAC00',//orange
    strokeWidth: 2, 
    fillColor:'#529ccd', 
    fillOpacity:0.2 
};
var click_style = { 
    pointRadius:5,
    strokeColor:'#f60000',//red
    strokeWidth: 2, 
    strokeDashstyle: "solid",
    fillColor:'#000000', 
    fillOpacity:0,
};
/*var buf_style = new OpenLayers.Style({
    strokeWidth: "${strokeWidth}",
    fillColor: "#ffffff",
    fillOpacity: 0.1,
  } , {
    context: {
        strokeColor:'#FFAC00',//orange
        strokeOpacity:0.4,
        strokeWidth: function(feature) {
            return feature.attributes.strokeWidth;
        }
    }
});*/
var styleMap = new OpenLayers.StyleMap({
    "default": new OpenLayers.Style({
            pointRadius:6,
            strokeColor: "#f60000",//red
            strokeOpacity: 0.9,
            strokeWidth: 2,
            fillColor: "#000000",
            fillOpacity: 0.1,
            cursor: "default",
            graphicName: 'circle',
    }),
    "temporary": new OpenLayers.Style({
            pointRadius:"${getSize}",
            strokeColor: "#f60000",//red
            strokeOpacity: 1,
            strokeWidth: 3,
            fillColor: "#fff500", // yellow
            fillOpacity: .2,
            cursor: "default",
            graphicName: 'circle',
            },{ context: {
                getSize: function(feature) {
                // calculate size instead set it in pixel!!!
                /* buffer around point */
                /*if ($('#buffer').length > 0) {
                    b = $("#buffer").val();
                } else { 
                    b = obj.buffer; 
                }

                var reader = new jsts.io.WKTReader();
                var input = reader.read(new String(feature.geometry));
                var buffer = input.buffer(b);
                var parser = new jsts.io.OpenLayersParser();
                buffer = parser.write(buffer);
                var feature = new OpenLayers.Feature.Vector(buffer, null, highlight_style);
                */
                return 10;
                //return $("#buffer").val() // feature.layer.map.getResolution();
            }}
    }),
    "select": new OpenLayers.Style({
            pointRadius:6,
            strokeColor: "#0033ff",//blue
            strokeOpacity: .7,
            strokeWidth: 5,
            fillColor: "#0033ff",
            fillOpacity: 0,
            graphicZIndex: 2,
            cursor: "default",
            graphicName: 'circle',
    })
});
/* OpenLayers initialization */
function init(){
    /* Load previously save queries: LQ=...
     * Load XML and call WFSGet */
    eval(obj.loadq);
    var options = {
        projection: new OpenLayers.Projection("EPSG:900913"),
        displayProjection: new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid),
        units: "m",
        numZoomLevels: 23,
        maxResolution: 156543.0339,
        maxExtent: new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508.34),
        controls: [],
    };
    map = new OpenLayers.Map('map',options);

    if (obj.subfilters == 1) {
        $.post('ajax',{'set-morefilter':1},function(data){});
        $("#apptq").find('i').toggleClass('fa-toggle-on');
        $("#apptq").find('i').toggleClass('fa-toggle-off');
        $("#apptq").addClass('button-success');
    }

    /* data and base layers
     * these are dynamic perproject paramters - see in biomaps.project_layers table */
    var dlayers = new Array();
    var blayers = new Array();
    var i = 0;
    var w = 0;
    var e = 0;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (key.match(/^layer_[a-zA-Z0-9-_]+/)) {
                if (key.match(/^layer_data[a-zA-Z0-9-_]*/)) {
                    //data layers
                    //$.post('ajax',{'check_layer':key},function(data){
                    //    var retval = jsendp(data);
                    //    if (retval['status']=='success' && retval['data']=='ok') {

                            dataLayers[w] = new Array();
                            eval("dataLayers["+w+"] = " + obj[key] + ";");
                            eval("dataLayers_ns["+w+"] = '" + key + "';");
                            dlayers.push("dataLayers["+w+"]");
                            w++;
                    //    }
                    //});
                } else {
                    //background layers
                    blayers.push(key);
                    eval("var " + key + "= " + obj[key] + ";");
                }
                i++;
            }
        }
    }

    /* marker layer */
    markerLayer = new OpenLayers.Layer.Markers( "Markers" );

    /* highlight the selected points */ 
    highlightLayer = new OpenLayers.Layer.Vector("Highlight features", {
         styleMap:styleMap,
         displayInLayerSwitcher: false,
         isBaseLayer: false,
         visibility:true,
         features:[],
    });

    /* Ez a réteg veszi fel a selection rajzokat,
     * a pont és vonal után a 'buffer készítés közben törlődik */
    drawLayer = new OpenLayers.Layer.Vector( "Select features",{
         styleMap:styleMap,
         displayInLayerSwitcher: false,
         isBaseLayer: false,
         visibility:true,
         features:[],
    });

    /* Draw on map layer:
     *  SELECT by draw... ADD features by draw...
     * */
    var pointLayer = new OpenLayers.Layer.Vector("Point Layer");
    drawControls = {
        addPolygon: new OpenLayers.Control.DrawFeature(pointLayer,      OpenLayers.Handler.Polygon),
        addPoint: new OpenLayers.Control.DrawFeature(pointLayer,      OpenLayers.Handler.Point),
        addLine: new OpenLayers.Control.DrawFeature(pointLayer,      OpenLayers.Handler.Path),
        selectPoint: new OpenLayers.Control.DrawFeature(drawLayer,      OpenLayers.Handler.Point),
        selectLine: new OpenLayers.Control.DrawFeature(drawLayer,      OpenLayers.Handler.Path),
        selectPolygon: new OpenLayers.Control.DrawFeature(drawLayer,      OpenLayers.Handler.Polygon),
        zoomArea: new OpenLayers.Control.ZoomBox({alwaysZoom:false}),
    };
    for(var key in drawControls) { map.addControl(drawControls[key]); }
    /* attach to events 
     * call the function 'report' after using the selection tools
     * */
    drawLayer.events.on({ "sketchcomplete": OpenLayers.Function.bind(GetDataOfSelection, this)});

    pointLayer.events.on({ "sketchcomplete": OpenLayers.Function.bind(addpoint, this)});

    OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {                
                defaultHandlerOptions: {
                    'single': true,
                    'double': true,
                    'pixelTolerance': 0,
                    'stopSingle': false,
                    'stopDouble': false
                },
                initialize: function(options) {
                    this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );
                    OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    ); 
                    this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click': this.trigger
                        }, this.handlerOptions
                    );
                }, 
                trigger: function(e) {
                    if (obj.identify_point_trigger=='on') {
                        var pX = e.pageX
                        var pY = e.pageY;
                        var lonlat = map.getLonLatFromPixel(e.xy);
                        var currentExtent = map.getExtent();
                        ce = currentExtent.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
                        $.post('ajax',{'identify_point':1,'lat':lonlat.lat,'lon':lonlat.lon,'ce':JSON.stringify(ce)},function(data){
                            if (data != '') {
                                v = JSON.parse(data);
                                $( "#dialog" ).html(v['content']);
                                $( "#dialog" ).dialog( "option", "position", { my: 'left-3 top+3',of: e } );
                                $( "#dialog" ).dialog( "option", "title", ""+v['str_listed']+": "+v['records']+" ("+v['str_sumall']+": "+v['all_records']+")." );
                                $( "#dialog" ).dialog( "option", "height", 160 );
                                var isOpen = $( "#dialog" ).dialog( "isOpen" );
                                if(!isOpen) $( "#dialog" ).dialog( "open" );
                            }
                        });
                    }
                }
    });

    //turn off non-active layers dynamically
    //on page load - it is maybe not necessary
    /*$.post('ajax',{'check_layer':dataLayers_ns},function(data) {
        var retval = jsendp(data);
        if (retval['status']=='success') {
            var v = retval['data'];
            var layers_state = JSON.parse(v);
            for(var i=0;i<layers_state.length;i++) {
                var o = dataLayers[i];
                if (layers_state[i]=='drop') {
                    //set visibility false prevent send query to this layer!
                    //dataLayers[i].visibility = false
                }
            }
        }*/

        //assemble layers
        var l = [blayers.join(),dlayers.join(),'drawLayer','markerLayer','highlightLayer'];
        l = l.filter(function(e){return e});
        eval('map.addLayers([' + l.join() + '])');

        // default turn off layers
        if (obj.turn_off_layers!='') {
            for(var i=0;i<dataLayers.length;i++) {
                var o = dataLayers[i];
                off = obj.turn_off_layers.split(',');
                var index = jQuery.inArray( dataLayers_ns[i] , off );
                if (index > -1) {
                    o.setVisibility(false);
                }
            }
        }

        //geomtest layer
        if (typeof geojson_enable!=='undefined') {
            map.addLayer('geojson_layer');
        }

        if(typeof $.cookie('wz')!=='undefined'){obj.zwe=$.cookie('wz');}
        //map.addControl(panel);
        nav = new OpenLayers.Control.Navigation({
            'zoomWheelEnabled': eval(obj.zwe),
            'zoomBoxEnabled' : true,
            'handleRightClicks' : true,
            //'defaultDblClick': function ( event ) { return; }
        });
        map.addControl(nav);

        var click = new OpenLayers.Control.Click();
        map.addControl(click);
        click.activate();

        //map.addControl(new OpenLayers.Control.Navigation({"zoomWheelEnabled":false}));
        map.addControl(new OpenLayers.Control.ScaleLine({"geodesic":true}));
    
        if (obj.load_map_page=='1') {
            map.addControl(new OpenLayers.Control.LayerSwitcher());
            map.addControl(new OpenLayers.Control.PanPanel());     // arrows above zoom panel
            map.addControl(new OpenLayers.Control.ZoomPanel());    // zoom panel
        } else {
             map.addControl(new OpenLayers.Control.Zoom());        // zoom + - buttons
        }

        map.events.register("mousemove", map, function(e) {
            var pixel = new OpenLayers.Pixel(e.xy.x,e.xy.y);
            var lonlat = map.getLonLatFromPixel(pixel);
            // show wgs84 coordinates below map window || layer projection??
            // if box_load_coord module enabled load coordinates into there
            if (document.getElementById("coord_query_x")) {
                lonlat.transform(map.getProjectionObject(), new OpenLayers.Projection("EPSG:"+$("#epsg_set").val()) );
                $("#coord_query_x").val(lonlat.lon.toFixed(5))
                $("#coord_query_y").val(lonlat.lat.toFixed(5))
            }
            else if (document.getElementById("mouse-position")) {
                lonlat.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
                // if not, looad coordinates into a specified simple area for them
                OpenLayers.Util.getElement("mouse-position").innerHTML = lonlat.lon.toFixed(5) + ' ' + lonlat.lat.toFixed(5);
            }
        });
        if (typeof $.cookie('lastZoom')!=='undefined') {
            try {
                v = JSON.parse($.cookie('lastZoom'));
                var point = new OpenLayers.LonLat(v.center.lon, v.center.lat);
                zoom = v.zoom;
            } catch (e) {}
        } else {
            var point = new OpenLayers.LonLat(obj.lon, obj.lat);
            point.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
            zoom = obj.zoom;
        }
       
        if (obj.overview_map!='0' && obj.load_map_page=='1') {
            var overview1 = new OpenLayers.Control.OverviewMap({
                    maximized: true,
                    maximizeTitle: 'Show the overview map',
                    minimizeTitle: 'Hide the overview map'
                });
            map.addControl(overview1);
        }
        if (obj.load_map_page=='0') {
            zoom = obj.zoom;
        }
        map.setCenter(point, zoom);

        // default control
        //var dcontrol = drawControls['selectPoint'];
        //dcontrol.activate();
        if (obj.identify_point_trigger=='on')
            $("#map").css('cursor','help');

        if (obj.fixheader==0) {
            $('.olControlZoomPanel').css('top','151px'); // 71
            $('.olControlPanPanel').css('top','90px');   // 10
            $('.olControlLayerSwitcher').css('top','85px'); // 5
            $("#OpenLayers_Control_MaximizeDiv_innerImage").attr('src',obj.url + 'images/Antu_dialog-layers.svg').width(48).css('margin-left','-30px');
        }
    //});

} //init
