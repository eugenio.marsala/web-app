/* main trigger events
 * 
 * */

var attachment_id_column = 'obm_files_id';
var geom_id_column = 'geom_id_column';
var rr = new Roller({rollerSend:50,ajax:'ajax',target:'#scrollbar'});
var arrayType = 'list';
var geom_populate = '';
var projecturl = obj.url;
var timer = null;

$(document).ready(function() {
    $('#cookie-ok').click(function(){
            $.post('ajax', {'cookies_accepted':'1' },
                function(data){
                    $("#cookie_div").hide();
            }); 
    });

    var h=$('body').innerHeight(); // New height
    /* srollable tables
     * */
    $('#scrollbar').on('scroll', function() {

        // prevent continous ajax call: render(), just one shot when scrolling finished 
        if (timer !== null) {
            clearTimeout(timer);        
        }
        timer = setTimeout(function() {
            // do something
            rr.render();
        }, 150);

        if ($('#slider').length && $('#slider').css('display') == 'block') {
            var id = $("#slider").data('id');
            $("#"+id).addClass('sliderExpanded');
        }
    });
    
    /* Ajax load - cursor progress */
    $(document).ajaxStart(function (event) {
        $('body').css('cursor', 'progress');
    }).ajaxStop(function () {
        $('body').css('cursor', 'auto');
    });

    /* */
    $( "#dialog" ).dialog({
        modal: true,
        autoOpen: false,
        show: {effect: "blind",duration: 500},
        hide: {effect: "blind",duration: 50},
        title: "message"
    });
    $( "#dialog-message" ).dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
            }
        },
        show: {effect: "blind",duration: 500},
        hide: {effect: "blind",duration: 50},
        title: "message"
    });
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:140,
		width:'auto',
        modal: true,
        autoOpen: false,
        buttons: {
            "Do it?": function() {
                $( this ).dialog( "close" );
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
    /*
     * */
    if (typeof $('#message').html() != "undefined" && $('#message').html()!='' ) {
        $( "#dialog" ).text($('#message').html());
        var isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog" ).dialog( "open" );
        $('#message').html('');
    };
    /* hide navigation dropdown menu
     * if click anywhere
     * */
    $(document).click(function(e) {
        $('.topnav').find('ul.subnav').hide();
        $('.topnav').find("li a.to").removeClass('to-t');
    });
    /* tabs menu subnav open
     * it not works with nested subnavs, however we don't need it yet
     * */
    $("ul.topnav li .to").click(function(e) { //When trigger is clicked...
        e.stopPropagation();
        e.preventDefault();
        $(".topnav").find("ul.subnav").hide();
        $('.topnav').find("li a.to").removeClass('to-t');

        if ($(this).parent().find("ul.subnav").css('display') == 'block') {
            $(this).parent().find("ul.subnav").slideUp('slow');
            return;
        }
		//Following events are applied to the subnav itself (moving subnav up and down)
        $(this).addClass('to-t');
        var w=$(this).parent().width();
        $(this).parent().find("ul.subnav").css({'min-width':w});
        $(this).parent().find("ul.subnav").slideDown('fast').show(); //Drop down the subnav on click
        $(this).parent().hover(
            //function() {
		//}, function(){	
			//$(this).parent().find("ul.subnav").slideUp('slow'); //When the mouse hovers out of the subnav, move it back up
		//});
		//Following events are applied to the trigger (Hover events for the trigger)
	    //}).hover(
            function() { 
                $(this).addClass("subhover"); //On hover over, add class "subhover"
	    }, function(){	//On Hover Out
                //$(this).removeClass("subhover"); //On hover out, remove class "subhover"
            });
    });

    /* fetch remote xml data
     * taxon-db management page
     * */
    $(".fetch-xml").click(function(){
        var id = $(this).attr('id');
        var url = $(this).data('url');
        var target = $(this).data('target');
        $("#"+target).html("<i class='fa fa-4x fa-spinner fa-pulse'></i>");
        $.post('ajax',{fetch_remote_data:id,url:url},function(data){
            $('#'+target).html(data);
        });
    });
    /* Go back to the original URL after on click LOGIN button
     * */
    $("#login").click(function(e){
        e.preventDefault();
        var loginUrl = $(this).attr('href');
        var returnUrl = window.location.href;
        // remove logout string
        var re = /logout/;
        if ( re.test(returnUrl) ) {
            returnUrl = loginUrl;
        }
        $.post('ajax',{login:returnUrl},function(data){
            //$('#login').unbind('click');
            window.location = loginUrl;
        });
    });
    /* Your Opinion button band show
     * */
    $("body").on('click','.yo',function(){
	    var id=$(this).attr('id');
	    var widgetId=id.substring(id.indexOf('-')+1,id.length);
        // ez csak a profilenál van, de ez general függvény
        $(".yoband").toggle('slow');
        $(this).parent().find('.bubby').show().css({top:'-='+h}).animate({top:'-=30'},458, 'swing', function() { });
    });
    /* opinions buborék szöveg elrejtése
     * */
    $("body").on('click','.bubby',function(){
        $(this).hide();
    });
    /* table design */
    $('.resultstable tr').hover(function() {  
            $(this).find('td').addClass('hovered');  
    }, function(){  
            $(this).find('td').removeClass('hovered');  
    });
    //$(".resultstable").live("resize",function(e) { $(this).simpleResizableTable() });
    //$(".resultstable").simpleResizableTable();
    $('body').on('click','#message',function(){
        $(this).hide();
    });

    /* results page "Save query" click
     * lekérdezés eredményének eltárolása */
    $('body').on('click','.saveq',function(){
        var div = $(this).parent();
        $.post("ajax", { savequery:$(this).val(),saveqlabel:$("#saveqlabel-"+$(this).val()).val() },function(data) { 
            var retval = jsendp(data);
            if (retval['status']=='error')
                alert(retval['message']);
            else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') { 
                div.html(retval['data']); 
            }
        });
    });

    // ezt érdemes lenne meghagyni és külön project jogosultsággal kezelni	
    $('body').on('click','.comment_send',function(){
            id=$(this).attr('id');
            comment=$('#dc_'+id).val();
            
            $.post(projecturl+"includes/update_comment.php", {comment:comment,id:id },
            function(data){
                alert('Thank you for your comment!');
                $("#dp_"+id).append(data);
                $("#dc_"+id).val('');
                $(".comment_send").remove(); 
            });
    });

    // ezt miért így csináltam????
    /* data update: results_builder.php -> interface.php
     * */
    $('body').on('click','.edbox_send',function(e){
        var id=$(this).attr('id');
        var parent_span = $(this).parent('span');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var inputId='dc-'+widgetId;
        var fieldtext=$('#'+inputId).val();
        var name=$('#'+inputId).attr('name');
        var params = {};
        if (location.search) {
            var parts = location.search.substring(1).split('&');
            for (var i = 0; i < parts.length; i++) {
                var nv = parts[i].split('=');
                if (!nv[0]) continue;
                params[nv[0]] = nv[1] || true;
            }
        }

        var target = $("#"+inputId).data('table');
        /*if (params.login) var process = 'update_profile';
        else if (params.register) var process = 'update_profile';
        else if (params.activate) var process = 'update_profile';
        else var process = 'update_fields';*/

        // add files to existing files - obm_files_id extension
        // in data_sheet interface.php
        var refresh_add = '';
        if ($(this).hasClass('refresh-add'))
            refresh_add = 'add';
        
        $.post(projecturl+'includes/update_fields.php', {name:name,text:fieldtext,id:widgetId,target:target,refresh_add:refresh_add },function(data){
            $('#'+inputId).removeClass('edbox-active');
            $('#'+inputId).removeClass('edbox-error');
            var retval = jsendp(data);
            if (retval['status']=='error') {
                $('#'+inputId).addClass('edbox-error');
                parent_span.append(" ERROR!");
            } else if (retval['status']=='fail') {
                $('#'+inputId).addClass('edbox-error');
                parent_span.append(" ERROR!");
                console.log(retval['data']);
            } else if (retval['status']=='success') { 
                $('#'+inputId).addClass('edbox-done');
                // hide all buttons in span
                parent_span.find('.edbox_send').each(function() {
                    $(this).hide();
                });
                // if parent span not exists hide the button
                // It is an ugly solution, should be updated to be more clean in every place where it is used....
                $('#'+id).hide();
                parent_span.append(" OK!");
            }
        });
    });

    /* Show data evaulation 'küld +-' button
     * interface.php profile.php */
    $('body').on('click','.pm',function(e){
	    var id=$(this).attr('id');
        var target=$(this).attr('rel');
	    var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var value=id.substring(3,4);
        var comment=$("#evdt-"+widgetId).val();
        var relid=$("#relid-"+widgetId).val();
        $.post(projecturl+'includes/evaluation.php', {target:target,id:widgetId,value:value,comment:comment,relid:relid},
            function(data) {
                $('#valm-'+widgetId).removeClass('button-error');
                $('#valm-'+widgetId).addClass('pure-button-disabled');
                $('#valz-'+widgetId).removeClass('button-secondary');
                $('#valz-'+widgetId).addClass('pure-button-disabled');
                $('#valp-'+widgetId).removeClass('button-success');
                $('#valp-'+widgetId).addClass('pure-button-disabled');

                var retval = jsendp(data);
                if (retval['status']=='error') {
                    $( "#dialog" ).text(retval['message']);
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" ); 
                } else if (retval['status']=='fail') {
                    $( "#dialog" ).text(retval['data']);
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" ); 
                    console.log(retval['data']);
                } else if (retval['status']=='success') {
                    $( "#dialog-message" ).text(retval['data']);
                    $( "#dialog-message" ).dialog({
                      modal: true,
                      buttons: {
                        Ok: function() {
                          $( this ).dialog( "close" );
                          location.reload();
                        }
                      }
                    });
                    var isOpen = $( "#dialog-message" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog-message" ).dialog( "open" ); 
                }
            });
    });
    /* editable form input with individual save button */
    $('body').on('click','.edbox',function(e){
	    var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        $('#span-'+widgetId).hide();
        var buttonId='ed-'+widgetId;
        $(this).addClass('edbox-active');
        $('#'+buttonId).show();
        var buttonId='edr-'+widgetId;
        $('#'+buttonId).show();
    });
    /* span with text over input */
    $('body').on('click','.edbox-span',function(e){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        $(this).hide();
        $('#dc-'+widgetId).show();
        var buttonId='ed-'+widgetId;
        $('#dc-'+widgetId).addClass('edbox-active');
        // password field
        if ($('#dc-'+widgetId).val()=='*****') {
            $('#dc-'+widgetId).val('');
        }
        $('#'+buttonId).show();
    });

    /* main.js
     * toggle data blocks, header blocks
     * */
    $('body').on('click','.togglerbutton',function(e){
        var id = $(this).data('id');
	    var widgetId=id.substring(id.indexOf('-')+1,id.length);
            $(".toggler").removeClass('sliderExpanded');
            $('#'+id).addClass('sliderExpanded');
            $( "#dialog" ).text("Loading ...");
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog" ).dialog( "open" );
        
            $.post(projecturl+"includes/read_results.php", {id:widgetId },
                function(data){
                    $( "#dialog" ).dialog( "close" );
                    if (data) {
                        $("#slider").html(data);
                        $("#slider").data('id',id);
                    }
            });
    });

    $('body').on('click','.toggler',function(e){
        var id = $(this).attr('id');
	    var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if($(this).hasClass('sliderExpanded')) {
            //
            $('#slider').hide();
            $(".toggler").removeClass('sliderExpanded');
        } else {
            $("#slider").html('');
            $(".toggler").removeClass('sliderExpanded');
            $(this).addClass('sliderExpanded');
            $( "#dialog" ).text("Loading ...");
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog" ).dialog( "open" );
        
            $.post(projecturl+"includes/read_results.php", {id:widgetId },
                function(data){
                    $( "#dialog" ).dialog( "close" );
                    if (data) {
                        $("#slider").html(data);
                        $("#slider").data('id',id);
                    }
            });
            $('#slider').show();
        }
    });
    $("body").on("click",".close",function(){
        $(this).parent().hide();
        $(".toggler").removeClass('sliderExpanded');
    }); 

    /* expand textarea after click on it
     * Result pages
     * */
    $("body").on('click','.expandTextarea',function(){
	    var id=$(this).attr('id');
        $("#"+id).animate({height: "90px"}, 500);
        $("#"+id).removeClass('expandTextarea');
    });
   
    /* Ez egy jquery UI function
     * Taxon név szűréshez csinálja az autocomplete megjelenítést
     * Nem lehet valami specifikusabb dologra cserélni??
     * pl: typeahead
     * */
    $("#taxon").autocomplete({
	        source: projecturl+"includes/getList.php?type=taxon",
	        minLength: 2,
	        select: function(event, ui) { 
                $("#taxon_id").val(ui.item.id+'#'+ui.item.meta);
            }
    });
    /* Taxon Scientific name-order */
    $(".name_order").click(function() {
        //sa_1-5
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);//5
        var pre=id.substring(0,id.indexOf('_'),id.length);//sa
        var xId=id.substring(0,1);//s
        var xWid=id.substring(id.indexOf('_')+1,4);//1
        //uncheck similar+common if accepted checked
        if(pre=='sa') {
            $("#ss_"+xWid+"-"+widgetId).prop('checked', false);
            $("#sc_"+xWid+"-"+widgetId).prop('checked', false);
            $("#sm_"+xWid+"-"+widgetId).prop('checked', false);
        } else {
            $("#sa_"+xWid+"-"+widgetId).prop('checked', false);   
        }
        $("#smnewn_"+xWid+"-"+widgetId).val("");
    });
    /* Taxon Alternative name-order */
    $(".cname_order").click(function() {
        //sa_1-5
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);//5
        var pre=id.substring(0,id.indexOf('_'),id.length);//sa
        var xId=id.substring(0,1);//s
        var xWid=id.substring(id.indexOf('_')+1,4);//1
        //uncheck similar+common if accepted checked
        if(pre=='aa') {
            $("#as_"+xWid+"-"+widgetId).prop('checked', false);
            $("#ac_"+xWid+"-"+widgetId).prop('checked', false);
            $("#am_"+xWid+"-"+widgetId).prop('checked', false);
        } else {
            $("#aa_"+xWid+"-"+widgetId).prop('checked', false);   
        }
        $("#amnewn_"+xWid+"-"+widgetId).val("");
    });
    /* Taxon name-order */
    $(".modname").change(function(){
        var id=$(this).attr('id');
        var pre=id.substring(0,1);
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        //console.log("#"+pre+"m_"+widgetId);
        //scientific|alternative+[accepted,common,similar]+id
        $("#"+pre+'a_'+widgetId).prop("checked",false);
        $("#"+pre+'c_'+widgetId).prop("checked",false);
        $("#"+pre+'s_'+widgetId).prop("checked",false);
        $("#"+pre+"m_"+widgetId).prop("checked",true);
    });
    /* taxon names - metaname */
    $(".ops-connect").change(function(){
        if($(this).is(":checked")) {
            var id = $(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            var v=$(".ops-connect").val().split(',');
            v.push(widgetId);
            v.sort();
            v = $.unique( v );
            v = v.filter(Boolean)
            $(".ops-connect").val(v.toString());
            $(".name-connect-"+widgetId).css({'color':'#50B464'});
            $(".con-"+widgetId).each(function(){
                $(this).prop('checked', true);
            })
        } else {
            var id = $(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            var v=$(".ops-connect").val().split(',');
            //v.push(widgetId);
            var index = jQuery.inArray(widgetId,v);
            if (index > -1) {
                v.splice(index, 1);
            }
            v.sort();
            v = $.unique( v );
            v = v.filter(Boolean)
            $(".ops-connect").val(v.toString());
            $(".name-connect-"+widgetId).css({'color':'#555555'});
            $(".con-"+widgetId).each(function(){
                $(this).prop('checked', false);
            })
        }
    });
    /* update mispelled taxon-name */
    $("#update-mispelled-taxonname").click(function(e){
        e.preventDefault()
        $.post(projecturl+"includes/update_taxonc.php", {'mispelled-update':1,'mispelled-taxonname-id':$('#mispelled-taxonname-id').val(),'mispelled-taxonname-lang':$("#mispelled-taxonname-lang").val(),'mispelled':$("#mispelled-taxonname-to-update").val(),'newname':$("#new-taxonname").val()},
        function(data){
            var curl = "";
            if (typeof this.href != "undefined") {
               curl = this.href.toString().toLowerCase();
            } else {
               curl = document.location.toString().toLowerCase();
            }
            
            var retval = jsendp(data);
            if (retval['status']=='error')
                $("#response").html(retval['message']);
            else if (retval['status']=='fail') {
                $("#response").html(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') 
                window.location = retval['data'];
        });
    });
    /* taxon names processing - send data */
    $("#name-change").click(function(){
        //var ops = new Array();
        var ops = {};
        var ops_connect = $(".ops-connect").val().split(',');
        $(".name_order").each(function() {
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length); //28
            var xId=id.substring(0,id.indexOf('-')); //sa_1
            var xwId=id.substring(id.indexOf('_')+1,xId.length); //1
            var xw=id.substring(0,2,xId);//sa
            var nt = {};
            //alter names
            if (xw=='sm') {
                nt['id']=widgetId;
                //accepted
                if ($("#sa_"+xwId+'-'+widgetId).is(":checked")) nt['accepted']=1;
                else nt['accepted']=0;
                //common
                if ($("#sc_"+xwId+'-'+widgetId).is(":checked")) nt['common']=1;
                else nt['common']=0;
                //synonim
                if ($("#ss_"+xwId+'-'+widgetId).is(":checked")) nt['synonym']=1;
                else nt['synonym']=0;
                nt['lang']=$("#sl_"+xwId+'-'+widgetId).val();
                nt['name']=$("#sn_"+xwId+'-'+widgetId).val();
                ops['s_'+xwId+'-'+widgetId]=nt;
            }
        });
        /* taxon names processing  - admin.js */
        $(".cname_order").each(function() {
            //aa_1-4940
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length); //4940
            var xId=id.substring(0,id.indexOf('-')); //aa_1
            var xwId=id.substring(id.indexOf('_')+1,xId.length); //1
            var xw=id.substring(0,2,xId);//am
            var nt = {};
            if (xw=='am') {
                nt['id']=widgetId;
                //accepted
                if ($("#aa_"+xwId+'-'+widgetId).is(":checked")) nt['accepted']=1;
                else nt['accepted']=0;
                //common
                if ($("#ac_"+xwId+'-'+widgetId).is(":checked")) nt['common']=1;
                else nt['common']=0;
                //synonim
                if ($("#as_"+xwId+'-'+widgetId).is(":checked")) nt['synonym']=1;
                else nt['synonym']=0;
                nt['lang']=$("#al_"+xwId+'-'+widgetId).val();
                nt['name']=$("#an_"+xwId+'-'+widgetId).val();
                ops['a_'+xwId+'-'+widgetId]=nt;
            }
        });
        $.post(projecturl+"includes/update_taxonc.php", {'ops':JSON.stringify(ops),'ops_connect':JSON.stringify(ops_connect)},
        function(data){
            /*var curl = "";
            if (typeof this.href != "undefined") {
               curl = this.href.toString().toLowerCase();
            } else {
               curl = document.location.toString().toLowerCase();
            }*/
            var retval = jsendp(data);
            if (retval['status']=='error')
                $("#response").html(retval['message']);
            else if (retval['status']=='fail') {
                console.log(retval['data']);
                $("#response").html(retval['message']);
            } else if (retval['status']=='success') {
                alert(retval['data']);
                location.reload();
            }
        });
    });

    /* results_asList.php
     * */
    $("body").on("click",".rs_hb",function(){
        var tr = $(this).closest('tr');
        tr.find(".rs_hb").removeClass("rs_hbs");
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        var value=id.substring(3,4);
        $("#optd_"+widgetId).find('.rs_hbd').hide();
        $("#opbd"+value+"_"+widgetId).show();
        $(this).addClass('rs_hbs');
    });

    /* FILTERBOX 
     * MapfilterBox:
     * send text query to WFS server
     * */
    $("#new-map-window").click(function(e){
        e.preventDefault();
        $("#navigator").hide();
        var qids = new Array();
        var qval = new Array();
        var table = $(this).attr('data-table');

        // a map_filter_box.php.inc-ben lévő 
        // qf class input/select metők tartalmai
        // kerülnek a myVar objectbe
        $('.qf').each(function(){
            var stri = "";
            var stra = new Array();
            var qid=$(this).attr('id');
            if( $(this).prop('type') == 'hidden' ) {
                stra.push($(this).val());
            } else if( $(this).prop('type') == 'text' ) {
                stra.push($(this).val());
            } else {
                $("#" + qid + " option:selected").each(function () {
                    stra.push($(this).val());
                });
            }
            if (stra.length){
                qids.push(qid);
                qval.push(JSON.stringify(stra));
            }
        });
        var myVar = {}
        for (i=0;i<qids.length;i++) {
            myVar["qids_" + qids[i]] = qval[i];
        }

        var w = window.open(projecturl+'?qtable='+table+'&query_api='+JSON.stringify(myVar)+'','map query','menubar=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1024,height=768,left=50,top=50,resizable=yes');
    });

    /* magnifier on rolltable cells
     * */
    $("body").on("click","#viewportDiv",function(){
        $(this).remove();
    });
    $("body").on("click",".viewport",function(){
        $("#viewportDiv").remove();
        val = $(this).html();
        var pt = $(this).offset().top;
        var pl = $(this).offset().left;
        $("body").append('<div id="viewportDiv" style="top:'+pt+'px;left:'+pl+'px;">'+val+'</div>');
    });

    /* rolltable header clickable column names for ORDER BY */
    $("body").on("mouseover",".viewport-header",function(e){
        var idx=$(this).index();
        $(this).resizable({
            maxWidth: 360,
            minWidth: 60,
            handles: 'e',
            stop: function( event, ui ) {
                var width = $(event.target).width();
                var we = eval(width-115);
                $("#scrollbar-header").width(eval($("#scrollbar-header").width()+we))
                $("#scrollbar").width(eval($("#scrollbar").width()+we))
                $("#rollertable").find('.rdiv').each(function(){
                    $(this).find('.cdiv').eq(idx).width(eval(width+2));
                });
            }
        });
    });

    $("body").on("click",".sort",function(e){
        var viewport = $(this).parent();
        //e.preventDefault();
        arrayType = viewport.data('type');
 
        //fulltable
        if (arrayType == 'rolltable') {
            $.post("ajax", {rolltable_order:1,order:viewport.attr('id')},
                function(data) {
                location.reload();
            });

        
        } else {

            // stable
            $.post("ajax", {viewType:arrayType,order:viewport.attr('id').substring(7)},
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    $( "#dialog" ).text(retval['message']);

                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                } else if (retval['status']=='fail') {
                    $( "#dialog" ).text("Invalid response received."); 
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                    console.log(retval['data']);
                } else if (retval['status']=='success') {
                    v = retval['data'];
                    rr.set({rowHeight:v.rowHeight,cellWidth:v.cellWidth,borderRight:v.borderRight,borderBottom:v.borderBottom,arrayType:v.arrayType});
                    var va = JSON.parse(v.scrollbar_header)
                    rr.prepare(va['w'],va['i']);
                    rr.render();
                    $("#scrollbar").show();
                    $("#drbc").height($("#scrollbar").height()+100);
                }
            });
        }
    });

    
    /* Results: view method change buttons click
     * stable, list
     * */
    $('body').on('click',".changeView",function(e){
        var button = $(this);
        e.preventDefault();
        arrayType = $(this).data('type');
        $(".changeView").removeClass('button-darkgray');
        $(this).addClass('button-darkgray');
 
        $.post("ajax", {viewType:arrayType,order:$(this).attr('id')},
        function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                $( "#dialog" ).text(retval['message']);
            } else if (retval['status']=='fail') {
                $( "#dialog" ).text("Invalid response received.");
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                v = retval['data'];
                rr.set({rowHeight:v.rowHeight,cellWidth:v.cellWidth,borderRight:v.borderRight,borderBottom:v.borderBottom,arrayType:v.arrayType});
                var va = JSON.parse(v.scrollbar_header)
                rr.prepare(va['w'],va['i']);
                rr.render();
                $("#scrollbar").show();
                $("#drbc").height($("#scrollbar").height()+100);
            }
        });
    });

    /* upload table - set geometry icon */
    $("body").on('click','.geom-down',function(){
        var target = $(this).data('target');
        var data;
        var srid;
        if (typeof id != "undefined") {
            target = $(this).attr('target');
        }
        //gm_data-5
        var ri,ci,pos,data;
        if (target == 'data') {
            ri = $(this).closest('.tr').index();
            ci = (1+(+$(this).closest('.td').index()));
            pos = ri+','+(1+(+$(this).closest('.td').index()));
            data = $("#upload-data-table > .tbody > .tr:eq("+ri+") > .td:nth-child("+ci+")").find('input').val();
            srid=$('#upl-srid').val();
        } else if (target=='header') {
            ri = $(this).closest('.tr').index();
            ci = (1+(+$(this).closest('.th').index()));
            pos = ri+','+(1+(+$(this).closest('.th').index()));
            data = $("#upload-data-table > .thead > .tr:eq("+ri+") > .th:nth-child("+ci+")").find('input').val();
            srid=$('#upl-srid').val();
        } else if (target=='in') {
            var id=$(this).attr('id');
            pos = id.substring(id.indexOf('-')+1,id.length);
            data = $("#dc-"+pos).val();
            srid='4326';
        } else if (target=='sheet') {
            var id=$(this).attr('id');
            pos = id.substring(id.indexOf('-')+1,id.length);
            data = $("#dc-"+pos).val();
            srid='4326';
        } else {
            //default
            var id=$(this).attr('id');
            target = 'default';
            pos = id.substring(id.indexOf('-')+1,id.length);
            data = $("#default-"+pos).val();
            srid=$('#upl-srid').val();
        }
        //továbbadott értékek a felugró ablaknak
        //
        $(".goptchoice").attr('id',"gopt_"+target+"-"+pos);
        $(".mapclick").attr('id',"map_"+target+"-"+pos);
        //wkt string átadása &wkt=... formátumban
        var url = $('.mapclick').attr('href').replace(/&wkt=.+$/,'');
        $('.mapclick').attr('href',url);
        $(".mapclick").attr('href',$('.mapclick').attr('href')+'&wkt='+data+'&srid='+srid);
        $("#gpm").show();
    });
    $("body").on('click','#gpm-close',function(){
        $("#gpm").hide();
    });
    /*  upload table - set geometry icon - open map window for selecting */
    $("body").on('click','.mapclick',function(event){
        event.preventDefault();
        event.stopPropagation();
        var url = $(this).attr('href');
        geom_populate = $(this).attr('id');
        mapwindow = window.open(url,'_blank','menubar=0,scrollbars=yes,status=0,titlebar=0,toolbar=0,width=800,height=600');
    });
    /* web form - geometry from named list 
    * */
    $(".goptchoice").change(function(){
        //gopt_default-valami
        //gopt_header
        var widgetId=$(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        //js v1.7
        //var [target,pos] = widgetId.split('-');
        var spl=widgetId.split('-');
        var target=spl[0];
        var pos=spl[1]; 
        var val = $(this).val();
        $.post("ajax", {'getpolygon':val },
        function(data){
            if(target=='default') {
                $("#default-"+pos).val(data);
            } else if(target=='header') {
                //js v1.7
                //var [rowIndex,colIndex] = pos.split(',');
                var spl=pos.split(',');
                var rowIndex=spl[0];
                var colIndex=spl[1]; 
                $("#upload-data-table > .thead > .tr:eq("+rowIndex+") > .th:nth-child("+colIndex+")").find('input').val(data);
                var input = $("#upload-data-table > .thead > .tr:eq("+rowIndex+") > .th:nth-child("+colIndex+")").find('input');
                fillfunction(input,'fill');
            } else {
                //js v1.7
                //var [rowIndex,colIndex] = pos.split(',');
                var spl=pos.split(',');
                var rowIndex=spl[0];
                var colIndex=spl[1]; 
                $("#upload-data-table > .tbody > .tr:eq("+rowIndex+") > .td:nth-child("+colIndex+")").find('input').val(data);
            }
            $("#gpm").hide();
            $(".goptchoice").prop('selectedIndex',0);
            //refreshTimer();
        });
    });

    $("body").on('click','#message-send',function(e) {
        var text = $("#message-body").val();
        var d = 'off';
        if ($("#message-as-email").is(":checked")) {
            d = 'on';
        } 
        
        $.post('ajax',{'broadcats-message':text,'email':d},function(data){
            $('#tab_basic').load(projecturl+'includes/profile.php?options=newsread');

        });
    });

    // user registration - privacy and terms
    /*$("body").on('click','#privacy_ok',function(e) {
        if ($("#terms_ok").is(":checked") & $("#privacy_ok").is(":checked")) {
                //$('#body').load(projecturl+'includes/profile.php?profile=1&privacy_terms_accepted');
            $.post('ajax',{'agree_new_terms':1},function(data){
                location.reload();
            });
        }
    });
    $("body").on('click','#terms_ok',function(e) {
        if ($("#privacy_ok").is(":checked") & $("#terms_ok").is(":checked")) {
            $.post('ajax',{'agree_new_terms':1},function(data){
                location.reload();
            });
                //$('#body').load(projecturl+'includes/profile.php?profile=1&privacy_terms_accepted');
        }
    });*/

    $("body").on("click",".showHide",function(ev) {
        ev.preventDefault();
        const target = $(this).data('target');
        $("#"+target).toggleClass('hidden');
        $(this).toggleClass('fa-plus');
        $(this).toggleClass('fa-minus');
    });

    /**
     * Observers autocomplete
     * **/
    $( function() {
        function split( val, regexp = /,\s*/ ) {
            return val.split( regexp );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( function() {
            $.widget( "custom.catcomplete", $.ui.autocomplete, {
                _create: function() {
                    this._super();
                    this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                },
                _renderMenu: function( ul, items ) {
                    var that = this,
                        currentCategory = "";
                    $.each( items, function( index, item ) {
                        var li;
                        if ( item.category != currentCategory ) {
                            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                            currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );
                        if ( item.category ) {
                            li.attr( "aria-label", item.category + " : " + item.label );
                        }
                    });
                }
            });

            $( ".multi-autocomplete" ).catcomplete({
                delay: 0,
                source: function( request, response ) {
                    $.getJSON( projecturl+"includes/getList.php", {
                        type: "search",
                        term: extractLast( request.term )
                    }, response );
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    var ids = split( $(this).data('ids'),/,/);
                    
                    // remove the current input
                    terms.pop();
                    
                    // add the selected item
                    terms.push( ui.item.value );
                    ids.push( ui.item.id );

                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );

                    var searchBox = $('#obm_search_' + ui.item.category);
                    searchBox.append('<div class="'+ui.item.category+' search-badge pure-button" data-id='+ui.item.id+'>'+ui.item.value+'<span>x</span></div>');
                    searchBox.find('.search-relation').removeClass('hidden');
                    this.value = '';
                    return false;
                }
            });
            $('.search-relation').click(function(){
                  $(this).toggleText('AND', 'OR');
            })
        } );
    } );

    $.fn.toggleText = function(t1, t2){
          if (this.text() == t1) this.text(t2);
          else                   this.text(t1);
          return this;
    };
    $.fn.visibleHeight = function() {
        if (this.is(":hidden")) return 0;
        var elBottom, elTop, scrollBot, scrollTop, visibleBottom, visibleTop;
        scrollTop = $(window).scrollTop();
        scrollBot = scrollTop + $(window).height();
        elTop = this.offset().top;
        elBottom = elTop + this.outerHeight();
        visibleTop = elTop < scrollTop ? scrollTop : elTop;
        visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
        return visibleBottom - visibleTop
    }

    if ($('#new-terms-agree-div').length) {

        $('.terms-langswitch').on('click',function() {
            let lang = $('.active').data('lang');
            $('.terms-langswitch').removeClass('active');
            $(this).addClass('active');
            $('#terms-'+lang).addClass('hidden');
            lang = $('.active').data('lang');
            $('#terms-'+lang).removeClass('hidden');
            $('.terms-text').hide();
        });

        $(".termsText").on('click',function(e) {
            e.preventDefault();
            let lang = $('.active').data('lang');
            var text = $(this).data('target');
            $.post('ajax', {'get-terms-text': text, 'lang': lang }, function(data) {
                $('.terms-text').html(data);
                $('.terms-text').show();
            });
        });

        // 2018.05.25 New Terms and Privacy Agree button
        $("body").on('click','#agree-new-terms',function(e) {
            $.post('ajax', {'agree_new_terms':1 }, function(data) {
                $('#new-terms-agree-div').hide();
                $('body').css('overflow-y','auto');
                $('#cookie_div').hide();
            });
        });
    }
    // bugreport box events
    $("body").on("click","#bugreport",function() {
        $("#bugreport-box").show();
    });
    $("body").on("click","#bugreport-submit",function(e) {
        e.preventDefault();
        $.post('ajax', {'new_gitlab_issue':1,'title':$('#issue-title').val(),'message':$('#issue-body').val(),'page':$('#page').val(),'eincf':$('#eincf').val(),'edefv':$('#edefv').val() }, function(data) {
            var retval = jsendp(data);
            if (retval['status']=='error')
                alert(retval['message']);
            else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') { 
                alert(retval['data']);
                $("#bugreport-box").hide();
                $("#issue-title").val('');
                $("#issue-body").val('');
                $("#issue-body").text('');
            }
        });
    });
    $("body").on("click","#bugreport-cancel",function(e) {
        e.preventDefault();
        $("#bugreport-box").hide();
    });

    $("body").on("click",".edit-record",function(e) {
        e.preventDefault();
        var table = $(this).data('table');
        var id = $(this).data('id');
        $.post('ajax', {'edit-record':id,'table':table},function(data) {
            window.location = projecturl+"/upload/?editrecord="+id+"&form="+data+"&type=file";
        });
    });
});
