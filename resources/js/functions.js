/* uploader.js 
 * paging if uploded file rows more than 200
 * */
function storeTblValues(id)
{
    var TableData = new Array();
    var columns = $('#'+id).children('.thead').children('.tr:first').find('.th').length;
    //var header = $('#'+id).children('thead').children('tr:first');
    var i=0;
    var oList = new Array();
    var tList = new Array();

    //$("#upload-data-table > .thead > .tr").each(function(){
    //$("#upload-data-table > .tbody > .tr").each(function(){
    $('#'+id+' .tr').each(function(row, tr){

        var p = new Array();
        var autoskip = 0;
        if (row=='2') {return;} // skip autofill row
        for (var i = 0; i < columns; i++) {
            if (row=='0') {
                tList.push($(tr).children('.th:eq('+i+')').html());
            } 
            else if (row=='1') {
                oList.push($(tr).children('.th:eq('+i+')').html());
            } 
            else {
                //colname = $(header).children().eq(i).text()
                if (i==0) {
                    if( $(tr).children('.td:eq('+i+')').find('input').is( ":checked" ) )
                        p.push('1');
                    else
                        p.push('0');
                    if ($(tr).children('.td:eq('+i+')').find('.skip').val()==1)
                        autoskip = 1;
                } else {
                    var type = $(tr).children('.td:eq('+i+')').find(":input").getType();
                    if (type=='text') {
                        p.push($(tr).children('.td:eq('+i+')').find('input').val());
                    }
                    else if(type=='select') {
                        p.push($(tr).children('.td:eq('+i+')').find('select').val());
                    }
                    else {
                        //non handled input type yet
                        p.push('');
                    }
                }
            }
        }
        if (autoskip == '1') {
            return;
        }

        if (row>2) {
            // a header 3 sorát eldobjuk
            n = row-3
            TableData[n]=p;
        }
        
    }); 

    //TableData.shift();  // first row will be empty - so remove
    var v = {
        title:tList,
        header:oList,
        data:TableData
    }
    return v;
}
/* :input type type
 *
 * */
$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
/**/
function sortevent(){
    $( "ul.oList" ).sortable({
        items: "li:not(.ui-state-disabled)",
        cancel: ".oListDisabled",
        connectWith: "ul",
        dropOnEmpty: true,
        snap: "ul",
        placeholder: "drop-highlight",
        helper: function( event ) {
            var text = $(this).find('.field-title').text();
            return $( "<div style='width:auto;height:auto;padding:2px;border:1px solid gray;background:white;opacity:0.5'>"+text+"</div>" );
        },
        cursorAt: { bottom:0,left:0 },
        start: function (event, ui ) {
            $(this).closest('.th').find('.drophere').show();
        
        },
        receive: function( event, ui ) {
            var k=1;
            var p;

            $(this).closest('.th').find('.drophere').hide();
            //overdrop: put back element to end of list
            $(this).children().each(function(){
                var attr = $(this).attr('id');
                if (typeof attr !== typeof undefined && attr !== false) {
                    if ($(this).attr('id')!=ui.item.attr('id')) {
                        var text = $(this).html();
                        var id = $(this).attr('id');
                        $(".sList").append("<li id='"+ id +"'>"+ text +"</li>");
                    }
                }
            });
            //put element
            $(this).html("<li id='"+ ui.item.attr('id') +"'>"+ ui.item.html() +"</li>");
 
            var colIndex = $(this).closest('.th').index()-1;
            var colName = $("#upload-data-table > .thead > .tr:eq(0) > .th:nth-child("+colIndex+")").find('.colName').text();
            $.post("ajax", {'column_assign':ui.item.attr('id'),'coli':colIndex,'colname':colName}, function(data) {
                paging(event,0);
            });
        }, 
        stop: function( event, ui ) {
            if ($("ul.sList li").length>0) {
                $("#leftTable").show();
                $("#rightTable").width(eval($("#rightTable").width() + $("#leftTable").width()));
            }

        }
    });
}
function ConvertDMSToDD(degrees, minutes, seconds, direction) {
    var dd = Number(degrees) + Number(minutes)/60 + Number(seconds)/(60*60);

    if (direction == "S" || direction == "W") {
        dd = dd * -1;
    } // Don't do anything for N or E
    return dd;
}
/* main.js
 * upload picture response function
 * */
function UplResponse(data) {
    var retval = jsendp(data);
    if (retval['status']=='error') {
        $( "#dialog" ).dialog( "open" );
        $( "#error" ).html(retval['message']);
    } else if (retval['status']=='fail') {
        $( "#dialog" ).dialog( "open" );
        $( "#dialog" ).text("An error occured.");
    } else if (retval['status']=='success') {
        var data = retval['data'];

        var m = data['file_names'];
        var r = data['err'];
        for(k = 0; k < m.length; k++) {
            //get photo thumbnail url
            if (typeof r[k] !== 'undefined' && r[k]!=''){
                // check_file_exist() return 0
                // file exists
                //continue;
                //console.log(m[k]+": "+r[k]);
            }
            $.post("getphoto", {ref:m[k],getthumbnailimage:1,url:1}, function(imgurl){
                var j = JSON.parse(imgurl);
                var fp = j['file_properties'];
                for (f = 0; f < fp.length; f++) {
                    var img = document.createElement('img');
                    var upf = fp[f];
                    // should be changed to local
                    img.src = 'http://openbiomaps.org/Images/32px_none.png';
                    $.ajax({
                        url: 'ajax',
                        data: {
                            'get_mime': 1,
                            'thumb': 'yes',
                            'file': upf['filename'],
                            'size': 32
                        },
                        type: 'POST'
                    }).done(function(r){
                          img.src = r;
                    }).fail(function(){
                          console.log('ajax mime get failed');
                    });

                    img.setAttribute('class', 'thumb');
                    img.setAttribute('alt', upf['filename']);
                    if (typeof upf['exif'] !== 'undefined' && upf['exif']!='') {
                        var coords = extraxt_XYcoordinates_from_exif(upf['exif']);
                        img.setAttribute('data-coords',coords.join());
                        img.setAttribute('data-satellites',extraxt_satellites_from_exif(upf['exif']));
                    }

                    $("#uresponse").append("<a href='"+upf['url']+'/'+upf['filename']+"' class='photolink' id='gf_"+upf['id']+"'></a>");
                    $("#uresponse").find('a').last().html(img);
                    $("#uresponse").append(' ');
                    $("#uresponse").find('a').last().draggable({
                        revert: "invalid",
                        containment: "document",
                        helper: "clone",
                        cursor: "move"
                    });
                }
            });
        }
    } else {
        $( "#dialog" ).dialog( "open" );
        $( "#dialog" ).text("An error occured.");

    }
}
function extraxt_satellites_from_exif(exif) {
    var j = JSON.parse(exif);
    var satellites = '';
    if (typeof j['GPSSatellites'] !== 'undefined') {
        satellites = j['GPSSatellites'];
    }
    return satellites;
}
function extraxt_XYcoordinates_from_exif(exif) {
    var j = JSON.parse(exif);
    var coords = new Array();
    if (typeof j['GPSLongitude'] !== 'undefined') {
        var dp = new Array();
        for (var i=0;i<j['GPSLongitude'].length;i++) {
            dp.push(eval(j['GPSLongitude'][i]));
        }
        //25° 35' 13.11" E
        coords.push(dp[0]+'° '+dp[1]+"' "+dp[2]+'" '+j['GPSLongitudeRef']);
    }
    if (typeof j['GPSLatitude'] !== 'undefined') {
        var dp = new Array();
        for (var i=0;i<j['GPSLatitude'].length;i++) {
            dp.push(eval(j['GPSLatitude'][i]));
        }
        //25 deg 35' 13.11" E
        coords.push(dp[0]+'° '+dp[1]+"' "+dp[2]+'" '+j['GPSLatitudeRef']);
    }
    return coords;
}
function updateProgress (oEvent) {
  if (oEvent.lengthComputable) {
    var percentComplete = oEvent.loaded / oEvent.total;
    //console.log(percentComplete);
    // ...
  } else {
    // Unable to compute progress information since the total size is unknown
    //console.log(0);
  }
}
function transferComplete(evt) {
  //console.log("The transfer is complete.");
  // is it compatible with the any browser??
}
function transferFailed(evt) {
  //console.log("An error occurred while transferring the file.");
}
function transferCanceled(evt) {
  //console.log("The transfer has been canceled by the user.");
}
/* main.js
 * uploader.js
 * */
var Uploader = function () {
// file upload class
}
Uploader.prototype.after = function(arg){
    //do something after transfer complete
    this.doAfter = arg;
}
Uploader.prototype.complete = function(arg){
    //transfer complete function
    this.completeFunction = arg;
}
Uploader.prototype.button = function(id){
    //uploader button function
    this.buttonId = id;
    this.uc = $("#"+id).find('i').attr('class');
    $("#"+id).find('i').removeClass();
    $("#"+id).find('i').addClass('fa fa-spinner fa-spin');
}
Uploader.prototype.param = function(arg){
    //optional form paramter value
    this.optParam = arg;
}
Uploader.prototype.param2 = function(arg){
    //optional form paramter value
    this.optParam2 = arg;
}

Uploader.prototype.file_upload = function(files,ext){
    var responseFun = this.completeFunction || "";
    var button = this.buttonId || "";
    var uc = this.uc || "";
    var doAfter = this.doAfter || "";
    var optParam = this.optParam || "";
    var optParam2 = this.optParam2 || "";
    
    // xhr version 
    var formData = new FormData();
    for(var i in files) {
        formData.append("files[]", files[i]);
    }
    formData.append("load-files", 1);
    formData.append("ext", optParam);
    formData.append("exu", optParam2);
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("progress", updateProgress);
    xhr.addEventListener("load", transferComplete);
    xhr.addEventListener("error", transferFailed);
    xhr.addEventListener("abort", transferCanceled);
    xhr.open("POST", 'ajax');
    xhr.onreadystatechange = function (r) {
        var R = {'file_names':'','err':'','message':'','status':''};
        if (this.responseText!='') {
            try {
                R = JSON.parse(this.responseText);
            }
            catch(e) { 
                console.log('wrong formed response');
            }
	}
        if (xhr.readyState == 4) {
            //request finished and response is ready
            if (this.response == 2) {
                if (responseFun!='') {
                    //give AJAX response to function (R)
                    window[responseFun](R);
                    $("#"+button).find('i').removeClass();
                    $("#"+button).find('i').addClass('fa fa-refresh fa-spin');
                } else {
                    alert('Upload failed:' + "\n" + R['file_names'].join("\n")+"\n"+R['message'].join("\n"));
                }
            } else {
                //'File received!';
                if (responseFun!=''){
                    window[responseFun](R);
                    $("#"+button).find('i').removeClass();
                    $("#"+button).find('i').addClass(uc);
                    eval(doAfter)
                } else {
                    alert('Upload failed:' + "\n" + R['file_names'].join("\n")+"\n"+R['message'].join("\n"));
                }
            }
        } else if (xhr.readyState == 0) {
            //request not initialized
            if (responseFun!='') {
                window[responseFun](R['file_names']);
                $("#"+button).find('i').removeClass();
                $("#"+button).find('i').addClass(uc);
            } else {
                alert('Upload failed:' + "\n" + R['file_names'].join("\n")+"\n"+R['message'].join("\n"));
            }
        } else {
            if (R['status']=='fail' || R['status']=='error') {
                alert('Upload failed:' + "\n" + R['message']);
            }
            //'Processing...';
        }
    };
    xhr.send(formData);
}

/* uploader.js
 * web form geometry from map selection
 * */
function parseWKTStrings(ps,T) {
    T = T || new Array();
    // linestring length validation??
    var i, j, lat, lng, tmp, tmpArr,arr=[],o=0;
    var type = ps.match(/^[a-zA-Z]+/);
    if (T.length == 0)
        var T = new Array("point", "linestring", "polygon", "multipoint", "multilinestring", "multipolygon");
    if(type!=null) {
        for (i=0;i<T.length;i++) {
            if(T[i].toUpperCase() === type[0].toUpperCase()) o = 1;   
        }
    } else return arr;
    if (o==0) return arr;
    //match '(' and ')' plus contents between them which contain anything other than '(' or ')'
    m = ps.match(/\([^\(\)]+\)/g);
    if (m !== null) {
        for (i = 0; i < m.length; i++) {
            //match all numeric strings
            tmp = m[i].match(/-?\d+\.?\d*/g);
            if (tmp !== null) {
                //convert all the coordinate sets in tmp from strings to Numbers and convert to LatLng objects
                for (j = 0, tmpArr = []; j < tmp.length; j+=2) {
                    lat = Number(tmp[j]);
                    lng = Number(tmp[j + 1]);
                    //tmpArr.push(new google.maps.LatLng(lat, lng));
                    tmpArr.push([lat,lng])
                }
                arr.push(tmpArr);
            }
        }
    }
    //array of arrays of LatLng objects, or empty array
    return arr;
}
/* ******************************************************************************
 *                                                                              *
 * maps.js functions                                                            *
 *                                                                              *
 * *****************************************************************************/
/*
 *
 * */
Array.prototype.contains = function(k) {
    for(var i=0; i < this.length; i++){
        if(this[i] === k) {
            return true;
        }
    }
    return false;
}
/* maps.js
 * upload - webform - geometry from map selection
 * set geometry sting ...
 * */
function setValue(val) {
    if(val=='') return false;
    var widgetId=geom_populate.substring(geom_populate.indexOf('_')+1,geom_populate.length);
    //js v.17
    //var [target,pos] = widgetId.split('-');
    var spl = widgetId.split('-');
    var target=spl[0];
    var pos=spl[1];
    if(target=='default')
        $("#default-"+pos).val(val);
    else if(target=='header') {
        //js v 1.7
        //var [rowIndex,colIndex] = pos.split(',');
        var spl=pos.split(',');var rowIndex=spl[0];var colIndex=spl[1];
        $("#upload-data-table .thead .tr:eq("+rowIndex+") .th:nth-child("+colIndex+")").find('input').val(val);
        //$("#upload-data-table thead tr:eq("+rowIndex+") th:nth-child("+colIndex+")").find('input').trigger('paste');
        var input = $("#upload-data-table .thead .tr:eq("+rowIndex+") .th:nth-child("+colIndex+")").find('input')
        fillfunction(input,'fill');
    } else if(target=='in') {
        /*
         * */
        $("#dc-"+pos).val(val);
    } else if(target=='sheet') {
        /*
         * */
        $("#dc-"+pos).val(val);
    } else {
        //normal
        //js v.17
        //var [rowIndex,colIndex] = pos.split(',');
        var spl=pos.split(',');var rowIndex=spl[0];var colIndex=spl[1];
        $("#upload-data-table .tbody .tr:eq("+rowIndex+") .td:nth-child("+colIndex+")").find('input').val(val);

        var this_autoskip = $("#upload-data-table .tbody .tr:eq("+rowIndex+")").find(".autoskip");
        if (this_autoskip.val()==1 & $("#upload-data-table .tbody tr:eq("+rowIndex+") .td:nth-child("+colIndex+")").find('input').val()!='') {
           this_autoskip.val(0);
           this_autoskip.removeClass('autoskip');
        }
    }
    $("#gpm").hide();
    mapwindow.close();
    return true;
}
/*
 * */
function toggleControl(element) {
    for(key in drawControls) {
        var control = drawControls[key];
        if(element.value == key && element.checked) { control.activate(); } 
        else { control.deactivate(); }
    }
}
/*
 * */
function allowPan(element) {
    var stop = !element.checked;
    for(var key in drawControls) {
        drawControls[key].handler.stopDown = stop;
        drawControls[key].handler.stopUp = stop;
    }
}
/* Add new data point related function
 * geomtest.php <- upload-show-table-form.php
 * */
function addpoint(event) {
    //SAVE last zoom position...
    var v = {'center':map.getCenter(),'zoom':map.getZoom()}
    $.cookie('lastZoom',JSON.stringify(v));
    highlightLayer.destroyFeatures();
    drawLayer.destroyFeatures();
    /* Select tools: point, line, polygon
     * create buffers around selection */
    if ($('#buffer').length > 0) {
        b = $("#buffer").val();
    } else { 
        b = obj.buffer; 
    }
    if (new String(event.feature.geometry).match(/POINT|LINE/) && b>0) {
        /* create buffer around point */
        /*var geometry = new OpenLayers.Geometry.Polygon.createRegularPolygon(event.feature.geometry, 500, 30);
        var addCircul = new OpenLayers.Feature.Vector(geometry, null, click_style);*/
        var reader = new jsts.io.WKTReader();
        var input = reader.read(new String(event.feature.geometry));
        var buffer = input.buffer(b);
        var parser = new jsts.io.OpenLayersParser();
        buffer = parser.write(buffer);
        var geometry = buffer;
        var feature = new OpenLayers.Feature.Vector(buffer, null, highlight_style);
        /* Draw the polygon on map */
        drawFeatures(feature,drawLayer);
        drawLayer.refresh({force: true});
    } else {
        /* polygon */
        var geometry = event.feature.geometry.clone();
        var feature = new OpenLayers.Feature.Vector(geometry, null, highlight_style);
        drawFeatures(feature,drawLayer);
        drawLayer.refresh({force: true});
    }
    g = geometry.clone();
    g.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
    var wkt_string = g.toString();
    //this send back the coordinates to the upload process if it was called from a form's geometry cell
    if(window.opener) {
        $("#matrix").html("<div id='flyingadd'><button id='setValue' class='pure-button button-success button-large' value='"+wkt_string+"'>pick up this geometry</button><br><span style='font-size:80%'>"+wkt_string+"</span></div>");
    }
}
/* SELECTION on map and report the WFS result
 * callback function 
 * SEPARATE TO three FUNCTIONS:
 * draw buffered point and line string 
 * wfs query on polygons
 * highlight selected points
 * */
var geom_here;
function GetDataOfSelection(event) {

    //SAVE last zoom position...
    var v = {'center':map.getCenter(),'zoom':map.getZoom()}
    $.cookie('lastZoom',JSON.stringify(v));
    //Dialog
    
    //$( "#dialog" ).dialog( "option", "position", { my: 'center', of: window } );
    //$( "#dialog" ).dialog( "option", "title", obj.message );
    //$( "#dialog" ).text(obj.processing_query+'...');

    //var isOpen = $( "#dialog" ).dialog( "isOpen" );
    //if (!isOpen) $( "#dialog" ).dialog( "open" );
    
    // should be included https://github.com/bjornharrtell/jsts
    // https://www.npmjs.com/package/jsts
    var b,g,xy;
    $("#navigator").hide();
    //hide data layer
    drawLayer.destroyFeatures();
    
    if (obj.turn_off_layers!='') {
        for(var i=0;i<dataLayers.length;i++) {
            var o = dataLayers[i];
            off = obj.turn_off_layers.split(',');
            var index = jQuery.inArray( dataLayers_ns[i] , off );
            if (index > -1) {
                o.setVisibility(false);
            }
        }
    }
    //$( "#dialog" ).text("Drawing query map...");

    /* Select tools: point, line, polygon
     * create buffers around selection 
     * Zoom to results */
    if ($('#buffer').length > 0) {
        b = $("#buffer").val();
    } else { 
        b = obj.buffer; 
    }
    //var neighbour = 0;
    //var ce;
    geometry_query_neighbour = 0;
    geometry_query_obj = '';
    geometry_query_session = 0;
    if (new String(event.feature.geometry).match(/POINT|LINE/) && b>0) {
        /* create buffer around point */
        /*var geometry = new OpenLayers.Geometry.Polygon.createRegularPolygon(event.feature.geometry, 500, 30);
        var addCircul = new OpenLayers.Feature.Vector(geometry, null, click_style);*/
        var reader = new jsts.io.WKTReader();
        var input = reader.read(new String(event.feature.geometry));
        var buffer = input.buffer(b);
        var parser = new jsts.io.OpenLayersParser();
        //input = parser.write(input);
        buffer = parser.write(buffer);
        var geometry = buffer;
        var feature = new OpenLayers.Feature.Vector(buffer, null, click_style);
        /* Draw the polygon on map */
        drawFeatures(feature,drawLayer);
        drawLayer.refresh({force: true});
    } else {
        /* polygon | zero buffer */
        var geometry = event.feature.geometry.clone();
        if (new String(event.feature.geometry).match(/POINT/) && b==0) {
            // query nearest neighbourhoud
            //neighbour = 1;
            geometry_query_neighbour = 1;
            var currentExtent = map.getExtent();
            // previously ce
            geometry_query_obj = currentExtent.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
        }
    }
    g = geometry.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
    // it will be used in HighLightFeature()
    zoomTo = new OpenLayers.Geometry.Collection(g);
    //wkt string trasformation
    var geomlist = g.toString();
    geom_here = geomlist;
    
    //Save selection
    $.post("ajax", { auto_save_selection:geomlist }, function(data) {
        if (data) {
            var retval = jsendp(data);
            if (retval['status']=='error') {
                $( "#dialog" ).dialog( "open" );
                $( "#error" ).html(retval['message']);
            } else if (retval['status']=='fail') {
                $( "#dialog" ).dialog( "open" );
                $( "#dialog" ).text("An error occured while creating the geometry selection.");
            } else if (retval['status']=='success') {
                geometry_query_session = 1;
                //$('#sendQuery').removeClass('button-gray');
                //$('#sendQuery').addClass('button-success');

                var $blink = $("#sendQuery");
                var blink_counter1 = 10;
                var backgroundInterval = setInterval(function(){
                    $blink.toggleClass("button-success");
                    blink_counter1--;
                    if (blink_counter1==0) {
                        clearInterval(backgroundInterval);
                    }
                },600)



                //v = retval['data'];

                //var myVar = { 'geom_selection':'session','neighbour':neighbour,'ce':JSON.stringify(ce) }
                //loadQueryMap(myVar);
            }
        } else {
            $( "#dialog" ).text("Saveing selection failed.");
        }
    });

}
/* refresh WMS query map layer after text ot spatial query (drawn or loaded)
 * */
function loadQueryMap(params) {

    $( "#dialog" ).dialog( "option", "position", { my: 'center', of: window } );
    $( "#dialog" ).dialog( "option", "title", obj.message );
    $( "#dialog" ).text(obj.processing_query+'...');
    var isOpen = $( "#dialog" ).dialog( "isOpen" );
    if(!isOpen) $( "#dialog" ).dialog( "open" );

    /* Sending query paramters to query_builder. It is returning with the data extent
     * */
    // disable wheelzoom after click query
    /* should be custom
     * if($("#wz").find('i').hasClass('fa-toggle-on')) {
            //console.log('a')
            $('#wz').find('i').toggleClass('fa-toggle-on');    
            $('#wz').find('i').toggleClass('fa-toggle-off');    
            $('#wz').removeClass('button-success');
            nav.disableZoomWheel();
    }*/

    $.post(projecturl+"includes/query_builder.php", params,
        function(data){

            /* clean previous results*/
            $('#error').html('');
            $('#matrix').html('');
            $('#scrollbar_header').hide().html("");
            $('#scrollbar').hide().html("");
            $("#scrollbar-header").hide();
            $('#scrollbar').hide();

            /* new results */
            if (data=='1') {
                // query id list ?and bbox? of drawn geoms
                $.post(projecturl+"includes/results_query.php", {'doo':1},
                    function(data) {
                    if (data >= 1) {
                        //if ($("#apptq").find('i').hasClass('fa-toggle-on')) {
                        //    LoadResults(); 
                        //}
                        //document.location.href='includes/queries.php?csv=1.csv';
                        LoadResults() 

                    } else {
                        //"There were no data in the query, try to change the query terms!"
                        //
                        var no_results_extension = '';
                        //var jo = JSON.parse('{"enabled":"true","text":"<br>Add data <a href=\\"localhost\/biomaps\/projects\/dinpiupload\/?form=57&type=web&set_fields=@jgf@\\">here?<\/a>"}');
                        var jo = JSON.parse(obj.no_res_exts);
                        if (jo['enabled']=='true') {
                            var gf = {
                                'obm_geometry': geom_here,
                            }
                            jgf = JSON.stringify(gf);
                            no_results_extension = jo['text'].replace('@jgf@',jgf);
                            no_results_extension = no_results_extension.replace(/#/g,"'");
                        }
                        $( "#dialog" ).html(obj.no_results_for_the_query + no_results_extension);
                    }
                });

            } else {
                $( "#dialog" ).html(obj.failed_to_assemble_query_string+' :('+"<br>"+data.replace(/\n/g,"<br>"));
            }
    });
}

// loading response data in various ways
function LoadResults() {

    $( "#dialog" ).text( obj.preparation_of_results+'...' );

    var resp_control = {id:[],geom:[],data:[],buttons:'on',specieslist:'on',summary:'on',method:default_view_result_method};
    // processing id list to create different kind of outputs
    $.post(projecturl+"includes/wfs_show_results.php",{'response_control':JSON.stringify(resp_control)},function(data) {
        var retval = jsendp(data);
        if (retval['status']=='error') {
            $( "#dialog" ).dialog( "close" );
            $( "#error" ).html(retval['message']);
            $( "#matrix" ).html('');
        } else if (retval['status']=='fail') {
            //$( "#dialog" ).text(retval['message']);
            $( "#dialog" ).dialog( "close" );
            $( "#dialog" ).text("An error occured while loading the attributes.");
            $( "#matrix" ).html('');
        } else if (retval['status']=='success') {
                v = retval['data'];
                // call to redraw query layers
                for(var i=0;i<dataLayers.length;i++) {
                    dataLayers[i].redraw(true);
                }

                rr.set({rowHeight:v.rowHeight,cellWidth:v.cellWidth,borderRight:v.borderRight,borderBottom:v.borderBottom,arrayType:v.arrayType});
                var va = {w:'',i:''};
                try {
                    va = JSON.parse(v.scrollbar_header);
                    rr.prepare(va['w'],va['i']);
                }
                catch(e) {
                    rr.prepare(500,'');
                }
                rr.render();
                $("#scrollbar").show();
                $("#drbc").height($("#scrollbar").height()+100);
                
                if (v.geom.length && v.geom[0] != '') {
                    bounds = new OpenLayers.Bounds();
                    bounds.extend(new OpenLayers.LonLat(v.geom[0],v.geom[1]));
                    bounds.extend(new OpenLayers.LonLat(v.geom[2],v.geom[3]));
                    bounds.transform(new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), new OpenLayers.Projection("EPSG:900913"));
                    bounds.toBBOX();
                    if (bounds.left==bounds.right && bounds.top==bounds.bottom) {
                        bounds.left = bounds.left-(bounds.left*0.0002)
                        bounds.right = bounds.right+(bounds.right*0.0002)
                        bounds.top = bounds.top+(bounds.top*0.0002)
                        bounds.bottom = bounds.bottom-(bounds.bottom*0.0002)
                    }
                    map.zoomToExtent(bounds,false);
                }

                $( "#dialog" ).dialog( "close" );
                $('#navbuttons').show();
                $("#navigator").show();
                $('#matrix').html(v.res);
                if ( (typeof openSpeciesList !== 'undefined') ) {
                    $('#specieslist-toggle-icon').trigger('click');
                }
                var $div2blink = $("#navbuttons").find(".button-href");
                var blink_counter1 = 21;
                var backgroundInterval = setInterval(function(){
                    $div2blink.toggleClass("button-success");
                    blink_counter1--;
                    if (blink_counter1==0) {
                        clearInterval(backgroundInterval);
                    }
                },1100)
                var $div2blink2 = $("#scrolldown");
                var blink_counter2 = 21;
                var backgroundInterval2 = setInterval(function(){
                    $div2blink2.toggleClass("button-secondary");
                    blink_counter2--;
                    if (blink_counter2==0) {
                        clearInterval(backgroundInterval2);
                    }
                },1100)
        }

        /*var v = {res:'',error:''};
        try {
            v = JSON.parse(data);
        } 
        catch(e) { 
            $( "#dialog" ).text("An error occured while loading the attributes.");
        }*/

        /*if (v.error.length) {
            $.each(v.error,function(i, value){
                $('#error').append(value + '<br>');
            });
        } else if (v.error!='') {
            $('#error').html(v.error + '<br>');
        } else {
            if (v.res=='') {
                $( "#dialog" ).text("Query returns no data :(");
                $('#matrix').html('');
            } else {
                // call to redraw query layers
                for(var i=0;i<dataLayers.length;i++) {
                    dataLayers[i].redraw(true);
                }
                rr.set({rowHeight:v.rowHeight,cellWidth:v.cellWidth,borderRight:v.borderRight,borderBottom:v.borderBottom,arrayType:v.arrayType});
                var va = {w:'',i:''};
                try {
                    va = JSON.parse(v.scrollbar_header);
                    rr.prepare(va['w'],va['i']);
                }
                catch(e) {
                    rr.prepare(500,'');
                }
                rr.render();
                $("#scrollbar").show();
                if (v.geom.length && v.geom[0] != '') {
                    bounds = new OpenLayers.Bounds();
                    bounds.extend(new OpenLayers.LonLat(v.geom[0],v.geom[1]));
                    bounds.extend(new OpenLayers.LonLat(v.geom[2],v.geom[3]));
                    bounds.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
                    bounds.toBBOX();
                    map.zoomToExtent(bounds,false);
                }
                /*geometry.bounds.left = boundingBox.l;
                geometry.bounds.bottom = boundingBox.b;
                geometry.bounds.right = boundingBox.r;
                geometry.bounds.top = boundingBox.t;
        // Zoom to higlighted selection 
        // polygon or selected points
        // zoomTo may has been calculated earlier...
        if (zoomTo == null) {
            zoomTo = new OpenLayers.Geometry.Collection(geometry);
        } else {
            zoomTo.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
        }
        var Zoomfeature = new OpenLayers.Feature.Vector(zoomTo, null, null);
        map.zoomToExtent(Zoomfeature.geometry.getBounds(), closest=false);
        zoomTo = null;
    */ /*
                $( "#dialog" ).dialog( "close" );
                $('#navbuttons').show();
                $("#navigator").show();
                $('#matrix').html(v.res);
            }
        }*/
    });
}
// NOT USED!
// apply text filters over spatial query
// it is using the query result of the spatial query, collect and give the response id to the text queries
// LIMITATION
function WFSGetNext(response) {
    var skip_processing = this.skip_processing;
    var resp_control = {id:[],geom:[],data:[],buttons:'on',specieslist:'on',summary:'on',method:default_view_result_method};
    $( "#dialog" ).text("Loading data...");
    var isOpen = $( "#dialog" ).dialog( "isOpen" );
    if (!isOpen) $( "#dialog" ).dialog( "open" );
    $('#error').html('');
    $('#matrix').html('');
    highlightLayer.destroyFeatures();
    $.post(projecturl+"includes/wfs_processing.php",{'skip_processing':skip_processing,'return':'none'},
        function(data){
            //very long response???
            var v = {id:'',error:''};
            try {
                v = JSON.parse(data);
            }
            catch(e) { 
                $( "#dialog" ).text("Invalid WFS XML response received.");
            }
            if(v.id!='') {
                //highlightFeatures(v.geom);
                //console.log(v.id);
            } 
            if (v.error!='') {
                // Error handling
                if (v.error==='0')
                    $( "#dialog" ).text("No matching records found.");
                else {
                    $( "#dialog" ).text("While processing WFS XML response some errors occured.");
                    if (v.error.isArray) {
                        $.each(v.error,function(i, value){
                            $('#error').append(value + '<br>');
                        })
                    } else {
                        $('#error').html(v.error + '<br>');
                    }
                }
            } else {
                //$("#navigator").hide();
                //drawLayer.destroyFeatures();
                //hide data layer

                if (obj.turn_off_layers!='') {
                    for(var i=0;i<dataLayers.length;i++) {
                        var o = dataLayers[i];
                        off = obj.turn_off_layers.split(',');
                        var index = jQuery.inArray( dataLayers_ns[i] , off );
                        if (index > -1) {
                            o.setVisibility(false);
                        }
                    }
                }

                var taxon_val = "";
                var trgm_string = "";
                var allmatch = 0;
                var onematch = 0;
                var qids = new Array();
                var qval = new Array();
                $("#tsellist").html('');
                // a map_filter_box.php.inc-ben lévő 
                // qf class input/select metők tartalmai
                // kerülnek a myVar objectbe
                $('.qf').each(function(){
                    var stri = "";
                    var stra = new Array();
                    var qid=$(this).attr('id');
                    if( $(this).prop('type') == 'text' ) {
                        stra.push($(this).val());
                    } else {
                        $("#" + qid + " option:selected").each(function () {
                            stra.push($(this).val());
                        });
                    }
                    if (stra.length){
                        qids.push(qid);
                        qval.push(JSON.stringify(stra));
                    }
                });
                /* többszörös fajválasztó */
                if ($("#taxon_sim").val()!='') {
                    var stra = new Array();
                    if ($("#allmatch").is(":checked")) { allmatch = 1; }
                    if ($("#onematch").is(":checked")) { onematch = 1; }
                    $("#taxon_trgm option:selected").each(function(){ stra.push($(this).val()); });
                    if (stra.length==0){
                        $("#taxon_trgm option").each(function(){ stra.push($(this).val()); });
                    }    
                    var trgm_string = "";
                    species_plus = species_plus.concat(stra); 
                }
                if (species_plus.length){
                    trgm_string = JSON.stringify(species_plus);
                    species_plus = new Array();
                }
                var myVar = { trgm_string:trgm_string,allmatch:allmatch,onematch:onematch, }
                for (i=0;i<qids.length;i++) {
                    myVar["qids_" + qids[i]] = qval[i];
                }
                //here we extend the text query width the returned id list
                myVar["qids_obm_id"] = '["on"]';
    
                //assembling the WFS get string
                $.post(projecturl+"includes/query_builder.php", myVar,
                function(data){
                    if (data!='') {
                        var mapurl = obj.query_url + '&REQUEST=GetFeature&TYPENAME=' + obj.qbuild_layer + '&qstr=' + data + '&time='+ new Date().getTime() ;
                        $( "#dialog" ).text("Waiting for the WFS response...");
                        var isOpen = $( "#dialog" ).dialog( "isOpen" );
                        if(!isOpen) $( "#dialog" ).dialog( "open" );
                        OpenLayers.Request.GET({url:mapurl,callback:WFSGet,scope:{"skip_processing":'no'}});
                    } else {
                        $( "#dialog" ).text("Empty query");
                        var isOpen = $( "#dialog" ).dialog( "isOpen" );
                        if(!isOpen) $( "#dialog" ).dialog( "open" );
                    }
                });
            }
    });
}
/* Megcsinál egy WFS lekérdezést
 * ---1. betölti a results_builder által visszaadott adatokat az adatmezőbe
   ---2. körül rajzolja a WFS query response pontokat 
   Ez most már csak a loadQuery ágban van használva?
   */
function WFSGet(response) {
    var skip_processing = this.skip_processing;
    var resp_control = {id:[],geom:[],data:[],buttons:'on',specieslist:'on',summary:'on',method:default_view_result_method};
    $( "#dialog" ).text("Loading data...");
    var isOpen = $( "#dialog" ).dialog( "isOpen" );
    if (!isOpen) $( "#dialog" ).dialog( "open" );
    //}
    // clear results area
    $('#error').html('');
    $('#matrix').html('');
    highlightLayer.destroyFeatures();
    
    /*if (typeof xy != "undefined" && xy.lon) {
        var xylonlat = xy.lon+' '+xy.lat;
    } else 
        var xylonlat = '';*/
    //sending a request for processes wfs xml data
    $.post(projecturl+"includes/wfs_processing.php",{'skip_processing':skip_processing},
        function(data){
            //very long response???
            var v = {geom:'',error:''};
            try {
                v = JSON.parse(data);
            }
            catch(e) { 
                $( "#dialog" ).text("Invalid WFS XML response received.");
            }
            if(v.geom!='') {
                highlightFeatures(v.geom);
            } 
            if (v.error!='') {
                // Error handling
                if (v.error==='0')
                    $( "#dialog" ).text("No matching records found.");
                else {
                    $( "#dialog" ).text("While processing WFS XML response some errors occured.");
                    if (v.error.isArray) {
                        $.each(v.error,function(i, value){
                            $('#error').append(value + '<br>');
                        })
                    } else {
                        $('#error').html(v.error + '<br>');
                    }
                }
            } else {
                /* normal query on the map: text or geometry
                 * show results in the selected view type
                 * */
                $( "#dialog" ).text( "Loading attributes..." );
                $.post(projecturl+"includes/wfs_show_results.php",{'response_control':JSON.stringify(resp_control)},function(data){                    
                    var retval = jsendp(data);
                    if (retval['status']=='error') {
                        $( "#dialog" ).dialog( "close" );
                        $( "#error" ).html(retval['message']);
                        $( "#matrix" ).html('');
                    } else if (retval['status']=='fail') {
                        //$( "#dialog" ).text(retval['message']);
                        $( "#dialog" ).dialog( "close" );
                        $( "#dialog" ).text("An error occured while loading the attributes.");
                        $( "#matrix" ).html('');
                    } else if (retval['status']=='success') {
                            v = retval['data'];
                            $( "#dialog" ).dialog( "close" );

                    //var v = {res:'',error:''};
                    /*try {
                        v = JSON.parse(data);
                        $( "#dialog" ).dialog( "close" );
                    } 
                    catch(e) { 
                        $( "#dialog" ).text("An error occured while loading the attributes.");
                    }
                    if (v.error.length) {
                        $.each(v.error,function(i, value){
                            $('#error').append(value + '<br>');
                    });
                    } else if (v.error!='') {
                        $('#error').html(v.error + '<br>');
                    } else {*/
                        rr.set({rowHeight:v.rowHeight,cellWidth:v.cellWidth,borderRight:v.borderRight,borderBottom:v.borderBottom,arrayType:v.arrayType});
                        var va = JSON.parse(v.scrollbar_header)
                        rr.prepare(va['w'],va['i']);
                        rr.render();
                        $("#scrollbar").show();
                        $("#drbc").height($("#scrollbar").height()+100);
                        $("#navigator").show();
                        $('#matrix').html(v.res);
                    }
            });
        }
    });
}
/* Highlight and zoom to given points */
function highlightFeatures(geom) {
    if (!geom) return;

    var points = []; 
    var lines = []; 
    var polygonList = [];
    var type = [];
    var a,lon,lat,b;
    if ($('#buffer').length > 0) {
        b = $("#buffer").val();
    } else { 
        b = obj.buffer; 
    }
    while(a=geom.pop()){
        if (a.hasOwnProperty('linearRing')) {
            var f = new String(a.linearRing[0]).valueOf();
            var l = new String(a.linearRing[a.linearRing.length - 1]).valueOf();
            if (f != l ) {
                type.push('line');
                var pointList = []; 
                for(var i=0;i<a.linearRing.length;i++) {
                    var lonLat = new OpenLayers.LonLat(lon=a.linearRing[i][0],lat=a.linearRing[i][1]).transform( 
                        new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), 
                        new OpenLayers.Projection("EPSG:900913") 
                    ); 
                    pointList.push(new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat)); 
                }
                var lineString = new OpenLayers.Geometry.LineString(pointList);
                lines.push(lineString);
            } else {
                type.push('linearRing');
                var pointList = []; 
                for(var i=0;i<a.linearRing.length;i++) {
                    var lonLat = new OpenLayers.LonLat(lon=a.linearRing[i][0],lat=a.linearRing[i][1]).transform( 
                        new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), 
                        new OpenLayers.Projection("EPSG:900913") 
                    ); 
                    pointList.push(new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat)); 
                }
                var linearRing = new OpenLayers.Geometry.LinearRing(pointList);
                var polygon = new OpenLayers.Geometry.Polygon([linearRing]);
                /* buffer around line and polygon */
                var reader = new jsts.io.WKTReader();
                var input = reader.read(new String(polygon));
                var buffer = input.buffer(b);
                var parser = new jsts.io.OpenLayersParser();
                buffer = parser.write(buffer);
                var polygon = buffer;
                polygonList.push(polygon);
            }
        }
        else if (a.hasOwnProperty('point')) {
            type.push('point');
            var lonLat = new OpenLayers.LonLat(lon=a.point[0],lat=a.point[1]).transform( 
                new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), 
                new OpenLayers.Projection("EPSG:900913") 
            );
            points.push(new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat));
        }
    }
    var boundingBox = {
        b:'',
        l:'',
        t:'',
        r:'',
    }
    if (type.contains('linearRing')) {
        geometry = new OpenLayers.Geometry.MultiPolygon(polygonList);
        var feature = new OpenLayers.Feature.Vector(geometry, null, highlight_style);
        feature.attributes.strokeWidth = 5;
        highlightLayer.addFeatures(feature);
        try {
            // van olyan, hogy nem tudja lekérdezni a keretet...
            // amikor pont és vonal vegyesen van...?? ezt majd meg kell nézni!!!
            // István küldte: Date: Tue, 19 Apr 2016 14:34:07 +0300
            boundingBox.l = geometry.getBounds().left;
            boundingBox.b = geometry.getBounds().bottom;
            boundingBox.r =  geometry.getBounds().right;
            boundingBox.t =  geometry.getBounds().top;
        }
        catch(e) { 
        }
    }
    if (type.contains('line')) {
        var geometry = new OpenLayers.Geometry.MultiLineString(lines); 
        var feature = new OpenLayers.Feature.Vector(geometry, null, highlight_style);
        feature.attributes.strokeWidth = 5;
        highlightLayer.addFeatures(feature);
        var l = geometry.getBounds().left
        var b = geometry.getBounds().bottom;
        var r = geometry.getBounds().right;
        var t = geometry.getBounds().top;
        if (boundingBox.l!='' && l<boundingBox.l) {boundingBox.l=l;} else if ((boundingBox.l=='')) {boundingBox.l=l;}
        if (boundingBox.b!='' && b<boundingBox.b) {boundingBox.b=b;} else if ((boundingBox.b=='')) {boundingBox.b=b;}
        if (boundingBox.t!='' && t>boundingBox.t) {boundingBox.t=t;} else if ((boundingBox.t=='')) {boundingBox.t=t;}
        if (boundingBox.r!='' && r>boundingBox.r) {boundingBox.r=r;} else if ((boundingBox.r=='')) {boundingBox.r=r;}
    }
    if (type.contains('point')) {
        var geometry = new OpenLayers.Geometry.MultiPoint(points); 
        var feature = new OpenLayers.Feature.Vector(geometry, null, highlight_style);
        highlightLayer.addFeatures(feature);
        var l = geometry.getBounds().left
        var b = geometry.getBounds().bottom;
        var r = geometry.getBounds().right;
        var t = geometry.getBounds().top;
        if (boundingBox.l!='' && l<boundingBox.l) {boundingBox.l=l;} else if ((boundingBox.l=='')) {boundingBox.l=l;}
        if (boundingBox.b!='' && b<boundingBox.b) {boundingBox.b=b;} else if ((boundingBox.b=='')) {boundingBox.b=b;}
        if (boundingBox.t!='' && t>boundingBox.t) {boundingBox.t=t;} else if ((boundingBox.t=='')) {boundingBox.t=t;}
        if (boundingBox.r!='' && r>boundingBox.r) {boundingBox.r=r;} else if ((boundingBox.r=='')) {boundingBox.r=r;}
    }
    geometry.bounds.left = boundingBox.l;
    geometry.bounds.bottom = boundingBox.b;
    geometry.bounds.right = boundingBox.r;
    geometry.bounds.top = boundingBox.t;
    // Zoom to higlighted selection 
    // polygon or selected points
    // zoomTo may has been calculated earlier...
    if (zoomTo == null) {
        zoomTo = new OpenLayers.Geometry.Collection(geometry);
    } else {
        zoomTo.transform(new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), new OpenLayers.Projection("EPSG:900913"));
    }
    var Zoomfeature = new OpenLayers.Feature.Vector(zoomTo, null, null);
    map.zoomToExtent(Zoomfeature.geometry.getBounds(), closest=false);
    zoomTo = null;
    
    highlightLayer.setVisibility(true);
    highlightLayer.redraw();
}
/* Highlight mapserver response GML 
 * not used
 * */
function highlightGML(response) {

    //var format = new OpenLayers.Format.GML({extractAttributes: true});

    var format = new OpenLayers.Format.GML({
        'internalProjection': new OpenLayers.Projection("EPSG:900913"),
        'externalProjection': new OpenLayers.Projection("EPSG:4326")
    });

    var features = format.read(response.responseXML || response.responseText);
    drawLayer.addFeatures(features);
    drawLayer.setVisibility(true);
    drawLayer.redraw();
    //drawFeatures(features,highlightLayer);
    var points = []; 
    parallel = features.length;
    $( "#dialog" ).text("Processing the query...");
    var isOpen = $( "#dialog" ).dialog( "isOpen" );
    if(!isOpen) $( "#dialog" ).dialog( "open" );

    for(var i=0;i<features.length;i++) {
        var g = features[i].geometry.clone();
        g.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
        var filter = new OpenLayers.Filter.Spatial({
            type: OpenLayers.Filter.Spatial.INTERSECTS,
            value: g
        });

        //proxy and wfs xml processing
        //should be called only once !!!
        var format = new OpenLayers.Format.Filter();
        var str = format.write(filter); 
        var params = {
            "REQUEST": "GetFeature",
            "TYPENAME": obj.query_layer,
            "FILTER": str.outerHTML
        };
        var url = obj.query_url + "&" + OpenLayers.Util.getParameterString(params);
        var req = OpenLayers.Request.GET({url:url,callback:WFSGet,scope:{"skip_processing":'no'}});
    }
}
/* add features (object) to existing layer */
function drawFeatures(f,l) {
    if(!l) return;
    l.destroyFeatures();
    if (f) {
        l.addFeatures(f);
        l.setVisibility(true);
    }
    l.redraw();
}
/* create results object from wfs respond object 
 * it is removing the null values                  */
function wfsDataDump(f) {
    var out = {};
    for (var i in f) {
        if (new String(f[i]) != 'null') {
            out[i] = f[i];
        }
    }
    return out;
}
/* Nincs használva! */
function popupDestroy(e) {
    popup.destroy();
    popup = null;
    OpenLayers.Util.safeStopPropagation(e);
}
/* Draw a polygon from given WKT string
 * wkt is coming as a JSON object
 * */
function drawPolygonFromWKT(input,zoom) {

    var epsg4326 = new OpenLayers.Projection('EPSG:4326'), epsg900913 = new OpenLayers.Projection('EPSG:900913');
    var v = JSON.parse(input);
    /* multigeometry trick; would be better with mapserver support */
    var wkt = new OpenLayers.Format.WKT({
        'internalProjection': epsg900913,
        'externalProjection': epsg4326});
    if(v.constructor != Array) {
        v = [v];
    }
    for (var j=0;j<v.length;j++) {
        var geometry = OpenLayers.Geometry.fromWKT(v[j]);
        var features = wkt.read(v[j]);
        var bounds;
        if(features) {
                if(features.constructor != Array) {
                    features = [features];
                }
                for(var i=0; i<features.length; ++i) {
                    if (!bounds) {
                        bounds = features[i].geometry.getBounds();
                    } else {
                        bounds.extend(features[i].geometry.getBounds());
                    }
                }
                drawLayer.addFeatures(features);
        }
    }
    if (zoom) {
        map.zoomToExtent(geometry.getBounds().transform(
                new OpenLayers.Projection("EPSG:4326"),
                map.getProjectionObject() 
            ), closest=false);
    }
    //drawLayer.setVisibility(true);
    //drawLayer.redraw();
    //return v.length;
}
/* not used??
 * */
function lbuf_reread() {
    $.post("ajax", {lbuf_reread:1},
        function(data){
            $('#load_selection').html(data);
    });
}
/* uploader.js*/

//autofill events for filling the current rows
//$('#upload-data-table').on("change paste propertychange",".fill",function(event){
function fillfunction(t,fun) {

    var idx = 1+(+t.closest('.th').index()); // post execute on SESSION['sheetData'] [nth col]
    if (idx == 1) {
        $( "#dialog" ).text('Invalid header request');
        var isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog" ).dialog( "open" );
        return;
    }

    var rowIndex;
    var autofill_data = t.val();

    var pattern = /`apply:\[(.+)\]/;
    var idmatch = new Array('','');

    if (fun!='fill') {
        if (pattern.test(fun)) {
            //set column index by header cell name
            var idmatch = pattern.exec(fun);
        }
    }

    if ($("#form_page_type").val() == 'web') {
        /* web form */
        var rowIndex_array = new Array();
        var t_array = new Array();
        $("#upload-data-table .td:nth-child("+idx+")").each(function() {

            if ( ! $(this).closest('.tr').hasClass('skippedRow')) {
                
                // uncheck autoskipped rows
                if ( $(this).closest('.tr').find('.autoskip').val()==1) {
                    $(this).closest('.tr').find('.autoskip').val(0);
                    $(this).closest('.tr').find('.autoskip').removeClass('autoskip');
                }

                rowIndex_array.push($(this).closest('.tr').index());
                t_array.push($(this));
                // update only one cell
            } else {
                console.log(3);
            }
        });
        $.post("ajax", {'upload_fill_function':fun,'autofill_data':autofill_data,'upload_fill_onecell':1,'rowindex':rowIndex_array,'colindex':idx,'idmatch':idmatch[1]}, 
            function(return_data) {
                j = JSON.parse(return_data);
                for (var i = 0; i<j.length ; i++) {
                    var data = j[i];
                    var t = t_array[i];
                    if (t.find('.ci').prop('type')=='text') {
                        t.find('.ci').val(data)
                    } else {
                        // label based value set for select menus
                        t.find('.ci option').each(function() {
                            if ($(this).text()==data) {
                                data = $(this).val();
                            }
                        });
                        t.find('.ci').val(data);
                    }
                    getNestedOptions(t.find('.ci'),'table');
                }
        });

        return;
    }


    $('#upload-data-table,input').css( 'cursor', 'progress' );

    // update all cell values in the background
    $.post("ajax", {'upload_fill_function':fun,'upload_fill_column-id':idx,'idmatch':idmatch[1],'autofill_data':autofill_data}, function(data){
        $('#upload-data-table,input').css( 'cursor', 'initial' );
        //read current sheet from SESSION['sheet_data']
        var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
        paging('',page,0);
    });
}
/* obfun fill+ functions
 * */
function fillplus_on_selected(fun) {

    var pattern = /`apply:\[.*\]/;
    if (pattern.test(fun)) {
        var pattern = /`apply:\[.*\]([a-zA-Z0-9-.]+):?(.+)?/;
    } else {
        var pattern = /`apply:([a-zA-Z0-9-.]+):?(.+)?/;
    }
    if (pattern.test(fun)) {
        matches = pattern.exec(fun);
        
        $('#obfun-name').val(matches[1]);
        $('#obfun-params').val(matches[2]);
    
        $("#obfun-params").prop('disabled', true);
        $("#obfun-params").val('');
        if (matches[1]=='replace.match' || matches[1]=='replace.all') {
            $("#obfun-params").prop('disabled', false);
            $("#obfun-params").val('///');
        }
    }

    $('#apply').show();
    $('#fillit').hide();
    $('#obfun-close').show();

    $('#longin-function-editor').css('display','inline-block');
    $('#longin-function-editor').show();
    $('#longin-function-editor').focus();
    $('#longin').hide();
    return; 
}

/* populate nested select inputs
 * */

//proto functions
function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}
function isLetter(str) {
  return str.length === 1 && str.match(/[a-z]/i);
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
/* JSEND style AJAX message checking and parsing
 *
 * */
function jsendp (message) {
    // xhr abort message object
    if ( typeof message === 'object' ) {
        if ( typeof message['responseText'] !== 'undefined' ) {
            message = message['responseText'];
        } else {
            message = JSON.stringify(message);
        }
    }
    // message is JSON text
    try {
        j = JSON.parse(message);
        if (j==null) {
            return {'status':'fail','data':'message parsing error: '+message};
        } else {
            return j;
        }
    }
    catch(e) { 
        return {'status':'fail','data':'message parsing error: '+message};
    }
}

// function for requesting list elements and final value for the dynamic select flow
function getNestedOptions(s,wh) {
    const form_type = $('input[name=selected_form_type]').val();

    const selected = $(s).find(':selected');
    if ($(s).data('nested')) {

        // form field with default value
        if (wh == 'default') {
            var id=$(s).attr('id');

            var pos = id.substring(id.indexOf('-')+1,id.length);
            const condition = ($(s).hasClass('genlist')) ?  $(s).val() : selected.data('fkey');
            $.post("ajax", {'dynamic_upload_list':pos,'sfid':$('#sfid').val(),'condition':condition}, function (data) {
                if (data!='') {
                    j = JSON.parse(data);

                    for (var i = 0; i<j.length ; i++) {

                        var col = j[i]['col'];
                        var opt = j[i]['opt'];
                        var target = '#default-'+col;

                        if ($(target).length != 0) {
                            if ($(target).is("input")) 
                                $(target).val(opt); 
                            else
                                $(target).empty().append(opt);
                        }
                        // if the child element is in the table: 
                        else {
                            var idx = $('#'+col).closest('.th').index();
                            var rows = $("#upload-data-table > .tbody > .tr").each(function() {
                                target = $(this).find('.td:eq(' + (idx) + ') input');
                                if ($(target).length != 0)
                                    $(target).val(opt);
                                else {
                                    target = $(this).find('.td:eq(' + (idx) + ') select');
                                    $(target).empty().append(opt);
                                }
                            });
                        }
                    }
                }
            });
        }
        // table fields
        else if (wh == 'table') {
            var col = $(s).closest('.td').index() ;
            var row = $(s).closest('.tr').index();

            var lav = $(s).data('lav');

            var pos = $("#upload-data-table > .thead > .tr:eq(1) > .th:eq("+(col)+") ul li").attr('id');
            const condition = (lav == 'false' || $(s).hasClass('genlist')) ?  $(s).val() : selected.data('fkey');
            $.post("ajax", {'dynamic_upload_list':pos,'sfid':$('#sfid').val(),'condition': condition}, function (data) {
                if (data!='') {
                    j = JSON.parse(data);

                    for (var i = 0; i<j.length ; i++) {
                        var col = j[i]['col'];
                        var opt = j[i]['opt'];
                        var idx = $('#'+col).closest('.th').index();
                        var target = $("#upload-data-table > .tbody > .tr:eq(" + (row) + ") > .td:eq("+(idx)+") select");

                        if ($(target).length != 0) 
                            var oldValue = target.val();

                        $(target).empty().append(opt);
                        if (oldValue)
                            $(target).val(oldValue);
                        else {
                            target = $("#upload-data-table > .tbody > .tr:eq(" + (row) + ") > .td:eq("+(idx)+") input");
                            $(target).val(opt);
                        }
                    }
                }
            });
        }
    }
}

