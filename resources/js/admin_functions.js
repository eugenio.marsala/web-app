/* functions for logined users
 *
 * */
// invitation sending from profile
function sendInvitation() {
    var to_val = $('#name').val();
    var to_mail = $('#email').val();
    var to_comm = $('#invcomment').val();
    $.post("ajax", {name:to_val,mail:to_mail,comment:to_comm,invitesend:1 },
    function(data) {
        var url = $("#invites").attr("data-url");
        if (typeof url !== "undefined") {
            var pane = $("#invites"), href = $('#invites').attr('href');
            // ajax load from data-url
            //$(href).load(url,function(result){ //pane.tab('show'); });
            $(href).load(projecturl+url,function(result){
            });
        }
    });
}
/* Change group
 * */
$('body').on('click','.change_group',function(e){
    e.stopPropagation();
    e.preventDefault();
    var id=$(this).attr('id');
    var widgetId=id.substring(id.indexOf('-')+1,id.length);
    $.post("ajax", { change_group:widgetId },function(data){ 
        $('#tab_basic').load(projecturl+'includes/profile.php?options=profile',function(result){
            //$(".customScroll").mCustomScrollbar({theme:'dark',scrollInertia:100,scrollButtons:{ enable: true }});
        });
    });
});
/* profile update: interface.php -> profile.php
 * */
function sendInputProfile() {
    $('body').on('click','.edbox_send_profile',function(e){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var inputId='dc-'+widgetId;
        var fieldtext=$('#'+inputId).val();
        var name=$('#'+inputId).attr('name');
        
        $.post(projecturl+'includes/update_profile.php', {name:name,text:fieldtext,id:widgetId },
            function(data){
                $('#'+inputId).removeClass('edbox-active');
                var pt = 0;
                var pt1=data.indexOf("Invalid request:");
                if (pt1>-1) pt = pt1+15;
                var pt1=data.indexOf("false");
                if (pt1>-1) pt = pt1+5;
                if(pt>0) {
                    var dt=data.substring(pt,data.length);
                    $('#'+inputId).addClass('edbox-error');
                    //$('#message').html(dt);
                    //$('#message').show();
                    $( "#dialog" ).text(dt);
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                }
                else if(data.match('OK')) {
                    $('#'+inputId).addClass('edbox-done');
                    if (name=='password') {
                        $('#'+inputId).val('*****');
                        $('#pwnotice').hide();
                    }
                }
                else if(data=='check_your_mailbox') {
                    $('#'+inputId).addClass('edbox-wait');
                    $('#cer').text('Check your mailbox!');
                }
                $('#'+id).hide();
            });

    });
}
// taxon admin function, 
function setTaxon( message ) {
        var a = message.split("#");
        species_plus = species_plus.concat(a[0] + "#" + a[1]); 
        $("<button class='pure-button button-href remove-species' value='" + a[0] + "#" + a[1] + "'>" + a[2] + " &nbsp; <i class='fa fa-close'></i></button>").prependTo( "#tsellist" );
        $("#taxon_sim").val("");
        $("#taxon_sim").html("");
        $("#taxon_sim").text("");
        $("#taxon_sim").focus();
}

function clear_list() {
    const list_editor_json = $("#list-editor-json");
    list_editor_json.val("");
}

function get_list(key = null) {
    const list_editor_json = $("#list-editor-json");
    const editor_string = list_editor_json.val(); 

    try {
        j = JSON.parse(editor_string);
    } catch(e) {
        j = {};
    }

    if (key == null)
        return j;
    else if (Object.keys(j).indexOf(key) >= 0)
        return j[key];
    else 
        return [];
}

function update_list(key, value) {
    const list_editor_json = $("#list-editor-json");

    j = get_list();

    try {
        value = JSON.parse(value);
    } catch(e) {
        value = value;
    }

    j[key] = value;

    if (['triggerTargetColumn','preFilterColumn'].indexOf(key) >= 0 && !Array.isArray(j[key])) { 
        j[key] = [ j[key] ];
    }
    if (key == 'list' && Array.isArray(j[key])) {
        let obj = {};
        for (let i = 0; i < j[key].length; i++) {
            const el = j[key][i];
            obj[el] = [el];
        }
        j[key] = obj;
    }

    list_editor_json.val(JSON.stringify(j,null,2));
}

