<?php

exit;

# job-master - receiving jobs from remote job sender
#
$function = $argv[1];
$data = json_decode(base64_decode($argv[2]),true);

if ($function == 'mailsend') {

    $m = mail_to($data['mailto'],$data['subject'],$data['mailfrom'],$data['mailfrom'],"<h1>{$data['title']}</h1>",$data['message'],"multipart"); 
    echo $m;

    exit;
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/* php mailer */
function mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method,$ReplyToName='No Reply',$FromName=PROJECTTABLE) {
    
    require_once('vendor/autoload.php');
 
    //base64 subject
    $Subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';

    if ($Method == 'simple') 
    {
        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $body = $stripped_title."\n\n".$stripped_message;
        $plain_body = $body;
        $isHTML = false;
    }
    elseif ($Method == 'multipart') 
    {
        $body = "<h2>$Title</h2>$Message";
        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $plain_body = $stripped_title."\n\n".$stripped_message;
        $isHTML = true;
    }
 
    $mail = new PHPMailer(true);        // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        if (SENDMAIL=='sendmail') {
            $mail->isSendmail();
        
        } elseif(SENDMAIL=='smtp') {
            $From = defined('SMTP_SENDER') ? SMTP_SENDER : SMTP_USERNAME;

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = SMTP_HOST;                              // Specify main and backup SMTP servers: 'smtp1.example.com;smtp2.example.com'
            $mail->SMTPAuth = SMTP_AUTH;                          // Enable SMTP authentication
            $mail->Username = SMTP_USERNAME;                      // SMTP username
            $mail->Password = SMTP_PASSWORD;                      // SMTP password
            $mail->SMTPSecure = defined('SMTP_SECURE') ? SMTP_SECURE : 'tls'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = defined('SMTP_PORT') ? SMTP_PORT : 25;                                  // TCP port to connect to    
        }

        //Recipients
        $mail->setFrom($From, $FromName);
        #$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
        if (is_array($To)) {
            #$mail->addCC('cc@example.com');
            #$mail->addBCC('bcc@example.com');
            $first_rcp = array_pop($To);
            $mail->addAddress($first_rcp);
            foreach ($To as $cc) {
                $mail->addCC($cc);
            }
        }
        else
            $mail->addAddress($To);               // Name is optional
        $ReplyTo = preg_replace('/localhost/','openbiomaps.org',$ReplyTo);
        $mail->addReplyTo($ReplyTo, $ReplyToName);

        //Attachments
        #$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        #$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML($isHTML);                                  // Set email format to HTML
        $mail->Subject = $Subject;
        $mail->Body    = $body;
        $mail->AltBody = $plain_body;

        $mail->CharSet = 'UTF-8';
        $mail->send();
        //"Message successfully sent!";
        return 2;
    } catch (Exception $e) {
        return 1;
    }    
}

?>
