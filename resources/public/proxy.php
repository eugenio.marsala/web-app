<?php
# that is the mapserver wrapper!
# this env-var was set in .htaccess

# Options -Indexes
# RewriteEngine On
# RewriteCond %{QUERY_STRING} !MAP=PMAP [NC]
# RewriteRule (.*) /mapserv [R=301,L]
# SetEnvIf Request_URI "(.*)" ACCESS=public
# <Files *.map>
#    Order Deny,Allow
#    Deny from all
#    #Allow from 127.0.0.1
# </Files>

//ini_set("log_errors", 1);
//ini_set("error_log", "/tmp/php-error.log");

#xdebug_start_trace('/tmp/xdebug');

session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

// If login session expired
if (!isset($_SESSION['token'])) {
    require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
# project switch check
if (isset($_SESSION['current_query_table'])) {
    if (!preg_match('/^'.PROJECTTABLE.'/',$_SESSION['current_query_table'])) {
        require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    }
}

/* Use cache 
 * 
 * XCache not works with php-7 
 *
 * Every SQL query should be cached here!
 *
 * */

//melyik map fájl könyvtárból lett meghívva
$accessLevel = isset($_SERVER['ACCESS']) ? $_SERVER['ACCESS'] : '';

/* public mapfile usage deprecated for OBM!
 * */ 
if ($accessLevel=='public') {
    # should be only non logined!!!
    # wfs return columns and other settings should be applied as needed!!
    # Set the map path 
    if (defined('PUBLIC_MAPFILE')) {
        // custom mapfile
        //$MAP = realpath( './'.PUBLIC_MAPFILE ); - not good if the mapfile is an other server
        $p_p = pathinfo(PUBLIC_MAPFILE);
        if ($p_p['extension']=='map' and $p_p['filename']=='public') {
            if ($p_p['dirname'] != '.') {
                $MAP = $p_p['dirname'].'/'.$p_p['basename'];
            } else {
                $MAP = realpath( './'.PUBLIC_MAPFILE );
            }
        }
    } else {
        // default mapfile
        $MAP = realpath('./public.map');
    }
} elseif ($accessLevel=='private') {
    # There can be different map files based on specified access levels
    # Set the map path 
    
    $MAP = realpath('./private.map');
    if (defined('PRIVATE_MAPFILE')) {
        // custom mapfile
        $p_p = pathinfo(PRIVATE_MAPFILE);
        if ($p_p['extension']=='map' and $p_p['filename']=='private') {
            if ($p_p['dirname'] != '.') {
                $MAP = $p_p['dirname'].'/'.$p_p['basename'];
            } else {
                $MAP = realpath( './'.PRIVATE_MAPFILE );
            }
        }
    } elseif (defined('SELECT_MAPFILE') and SELECT_MAPFILE==1) {
        //nincs kidolgozva!!!
        //valahogy egy csoportot kell a felhasználónak választani? Mikor, hol, hogyan magyarázzuk meg a felhasználónak?
        //automatikus választás?

        $res = pg_query($BID,"SELECT mapfiles FROM projects? WHERE project_table='".PROJECTTABLE."' ");
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            //eg.: group_titkos1.map
            //     
            $MAP = realpath( './'.$row['mapfile'] );
        }
    }

} else {
    # Set the map NULL path 
   $MAP = ''; 
}

$XML_TYPES = array('GetCapabilities','DescribeFeatureType','GetFeature','GetFeatureInfo','GetSchemaExtension');
$IMG_TYPES = array('GetMap','GetLegendGraphic');

if (defined('MAPSERVER'))
    $MAPSERVER = MAPSERVER."?";
else
    $MAPSERVER = "http://localhost/cgi-bin/mapserv?";

$referer = '';

if ($MAP!=''){
    /* ez amiatt kell mert a mapfile extent nagyon le tud maradni és így röptében, 
     * szöveges lekérdezéshez tudunk kb jó kiterjedést adni 
     * viszont jó lenne nem megismételni ezt a lekérdezést itt 100x - cache vagy constans!!!
     */
    // USING CACHE !
    /*$extent = obm_cache('get',"map_extent");

    if (!$extent) {
        $extent = "";

        if (defined('POSTGIS_V') and POSTGIS_V>'2.0')
            //PostGis 2.1
            #$res = pg_query($ID,"SELECT ST_Expand(ST_Estimated_Extent('public','".PROJECTTABLE."','{$_SESSION['st_col']['GEOM_C']}'),0.1) AS extent");
            $res = pg_query($ID,"SELECT ST_Extent(ST_Transform({$_SESSION['st_col']['GEOM_C']},3857)) AS extent FROM ".PROJECTTABLE);
        else
            //PostGis <2.1
            $res = pg_query($ID,"SELECT ST_Extent({$_SESSION['st_col']['GEOM_C']}) AS extent FROM ".PROJECTTABLE);
        
        if ($res) {
            $row = pg_fetch_assoc($res);
            $extent = preg_replace("/[^0-9., ]/",'',$row['extent']);
            $extent = preg_replace("/ /",",",$extent);
            //  Jobb megoldás esetén kiszedendő
            if ($extent!='') {
                $extent = "&BBOX=$extent";
                obm_cache('set',"map_extent",$extent,60);
            }
        } else {
            log_action('The table which contains the spatial data should be ANALYSED! Go to phppgadmin...');
        }

    }*/
    #log_action($extent);
    $extent = '';

    //will be processed in .htaccess
    //maybe it is used only in public access: to change the real path to dummy path
    $map = "&ACCESS=$MAP";

    //default query_string - not used the default any more!
    //$url = $_SERVER['QUERY_STRING'].$map;

    // parse QUERY_STRING
    parse_str($_SERVER['QUERY_STRING'], $HttpQuery);

    //assembling SQL query string using ACCESS level text filters and spatial filters and subfilters
    //wms layers param is the reference
    $query = '';
    if($accessLevel=='private') {

        if (isset($HttpQuery['LAYERS'])) {
            $_SESSION['map_query'] = true;
            $qq = assemble_query($HttpQuery['LAYERS'],'proxy',$HttpQuery['BBOX'],$HttpQuery['CNAME']);
            //remove layer connect name
            unset($HttpQuery['CNAME']);
            //debug($HttpQuery['LAYERS'],__FILE__,__LINE__);
            //debug($qq,__FILE__,__LINE__);
            //debug(count($qq),__FILE__,__LINE__);
            if (is_array($qq) and count($qq) > 1) {
                // more than 1 data layer queried
                //$query = '('.join(') UNION (',$qq).')';
                $query = $qq[0];
            } elseif (is_array($qq) and isset($qq[0])) {
                $query = $qq[0];
            } else {
                $im = imagecreatefrompng("../images/empty.png");
                imagealphablending($im, true);
                imagesavealpha($im, true);
                header("Content-type: image/png");
                imagepng($im);
                imagedestroy($im);
                //log_action('No query, exit with empty image: '.$url);
                exit;
            }
        }
    }
    //if (!isset($_SESSION['cnt_maps_req'])) $_SESSION['cnt_maps_req'] = 0;
    //$_SESSION['cnt_maps_req']++;
    //log_action($HttpQuery['BBOX']);
    //log_action($_SESSION['cnt_maps_req']);

    //log_action($query,__FILE__,__LINE__);

    //push query and assemble the new url
    $HttpQuery['query'] = $query;
    $mapserver_get_string = http_build_query($HttpQuery);
    $mapserver_get_string = preg_replace('/TILE=X%2BY%2BZ/','TILE=X+Y+Z',$mapserver_get_string);

    # Assemble the final mapserver GET url with forcing real BBOX
    $url = $mapserver_get_string.$extent.$map;

    /* repeat  - shall we use it? */ 
    if (isset($HttpQuery['repeat'])) {
        $_SESSION['wfs_rows_counter']=$HttpQuery['repeat'];
        //pages, requets from maps.js
        if(isset($_SESSION['repeat_url'])) {
            $url=$_SESSION['repeat_url'];
        } else {
            exit;
        }
    }
    $_SESSION['repeat_url'] = $url;

    // Close session write
    session_write_close();

    /* we don't print the output xml if has refered from the web apps... */
    if (isset($_SERVER['HTTP_REFERER']))
        $referer = implode("/",array_slice(preg_split("/\//",$_SERVER['HTTP_REFERER']), 2,-1));

    /* show xml response in local query
     * This called from the maps.js->#customLayer".click function
     * when somebody load a custom shp file as a new Layer */
    if (isset($HttpQuery['showXML'])) {
        $referer = ''; 
    }
    
    // Automatic authentication by using _SESSION login parameters
    /*if (isset($_SESSION['Tth'])) {
        //currently it is not works, because I don't know how deal with php password_hash crypt compatible passwords
        //and it causes segmentation faults
        //...
        $auth = sprintf("Authorization: Basic %s",base64_encode("{$_SESSION['Tmail']}:{$_SESSION['Tth']}"));
        $opts = array('http'=>array('method'=>'GET','header'=>$auth));
        $context = stream_context_create($opts);
        $fp = fopen($MAPSERVER.$url, 'r', false, $context);
    } else { */

        // POST not works, POST-XML?
        //$opts = array('http'=>array('method'=>'POST','header'=>"Content-type: application/x-www-form-urlencoded\r\n",'content'=>$mapserver_get_string));
        //$context = stream_context_create($opts);
        //$fp = fopen($MAPSERVER, 'r', false, $context);
        $fp = fopen($MAPSERVER.$url, 'r', false);
    //}
    
    if (isset($HttpQuery['REQUEST'])) {
        if (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $XML_TYPES))!==false) $type = 'xml';
        elseif (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $IMG_TYPES))!==false) $type = 'image';
        else $type = 'unknown';
    } else {
        # skip_processing: paging...
        $type = '';
    }
    
    //close pg here
    //pg_close($ID);

    if ($type=='xml') {
        # xml output
        $ses = session_id();
        
        # rewrite the wfs:FeatureCollection xsi:schemaLocation
        //$content = preg_replace("/http:\/\/localhost\/cgi-bin\/mapserv/ius","http://localhost{$_SERVER['PHP_SELF']}/",$content);
        $e = 0;

        $list = glob(OB_TMP."conts_$ses*.xml");
        natsort($list);
        $file = array_pop($list);
        $m = array();
        if (preg_match('/(\d)+\.xml/',$file,$m)) {
            $e = $m[1];
        }
        while (file_exists(OB_TMP."conts_$ses-$e.xml")) {
            $e++;
        }
        $filename=OB_TMP."conts_$ses-$e.xml";
        $dest = fopen($filename, 'w');
        if ($fp===false) {
            error_log("WFS XML READ ERROR");
            exit;
        }
        if ($dest===false) {
            error_log("WFS XML WRITE ERROR");
            exit;
        }

        if(!stream_copy_to_stream($fp,$dest)) {
            error_log( "ERROR WHILE STREAMING XML DATA TO FILENAME: $filename\n" );
            exit;
        }

        //file_put_contents("/mnt/data/tmp/conts_$ses-$e.xml", $content);
        /* we don't print the output xml if has refered from the web apps... 
         * if we don't want to draw with the setHTML() */
        if (URL!=$referer) {
            header('Content-type: text/xml');
            $handle = fopen($filename, "rb");
            echo fread($handle, filesize($filename));
            //echo $content;
        }
        exit;
    } elseif($type=='image') {
        if (is_resource($fp)) {

            if (isset($HttpQuery['FORMAT'])) $format = $HttpQuery['FORMAT'];
            else $format = 'image/png';
            header("Content-type: $format");
            echo stream_get_contents($fp);
            //echo $content;
        } else {
            log_action('Mapserver does not available',__FILE__,__LINE__);
        }
        exit;
    } else {
        echo stream_get_contents($fp);
        //echo $content;
        exit;
    }
} else {
    print "No map defined.";
    exit;
}

#xdebug_stop_trace();


?>
