<?php
# that is the mapserver wrapper!
# this env-var was set in .htaccess
# this wrapper called by using url parameter setting in the project layers
# if it is "default" the proxy wrapper called

# Options -Indexes
# RewriteEngine On
# RewriteCond %{QUERY_STRING} !MAP=PMAP [NC]
# RewriteRule (.*) /mapserv [R=301,L]
# SetEnvIf Request_URI "(.*)" ACCESS=public
# <Files *.map>
#    Order Deny,Allow
#    Deny from all
#    #Allow from 127.0.0.1
# </Files>

session_start();
session_write_close();

$accessLevel = isset($_SERVER['ACCESS']) ? $_SERVER['ACCESS'] : '';

if ($accessLevel=='public') {
    # Set the map path 
    $MAP = realpath('./public.map');
}
elseif($accessLevel=='private') {
    # Set the map path 
    $MAP = realpath('./private.map');
} else {
    # Set the map NULL path 
   $MAP = ''; 
}
$XML_TYPES = array('GetCapabilities','DescribeFeatureType','GetFeature','GetFeatureInfo');
$IMG_TYPES = array('GetMap');

if (defined('MAPSERVER'))
    $MAPSERVER = MAPSERVER."?";
else
    $MAPSERVER = "http://localhost/cgi-bin/mapserv?";

if (defined('MAPCACHE'))
    $MAPCACHE = MAPCACHE."?";
else
    $MAPCACHE = "http://localhost/mapcache?";

if ($MAP!=''){
    require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    $url = $MAPCACHE.$_SERVER['QUERY_STRING'];

    parse_str($_SERVER['QUERY_STRING'], $HttpQuery);

    # automatic save query_tmp
    $ses = session_id();

    # unhide the query string from query_builder.php
    if (isset($_SESSION['query_build_string'])) {
        $HttpQuery['qstr']=$_SESSION['query_build_string'];
        $q = http_build_query($HttpQuery);
        $url = $MAPSERVER.$q."&ACCESS=".$MAP;
    }
    /*if (isset($_SESSION['Tth'])) {
        $cred = sprintf('Authorization: Basic %s',base64_encode("{$_SESSION['Tmail']}:{$_SESSION['Tth']}") );
        $opts = array(
            'http'=>array(
                'method'=>'GET',
                'header'=>$cred)
        );
        $context = stream_context_create($opts);
        $content = file_get_contents($url, false, $context);
    } else */
        $content = file_get_contents($url, false);

    if (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $XML_TYPES))) $type = 'xml';
    elseif (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $IMG_TYPES))) $type = 'image';
    else $type = 'unknown';

    if ($type=='xml') {
        header('Content-type: text/xml');
        echo $content;
        exit;
    } elseif($type=='image') {
        header("Content-type: $format");
        echo $content;
        exit;
    } else {
        echo $content;
        exit;
    }

} else {
    print "No map defined.";
    exit;
}



?>
