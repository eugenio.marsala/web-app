<?php

$ref =  sprintf('%s://%s/',$protocol,URL);
?>

<div id='bheader'>
    <div id='maintenance' style='color:darkred;background-color:gold;text-align:center;font-weight:bold'><?php echo $maintenance_message ?></div>
    <div class='htitle'>
<?php
    $hidden = (get_option('project_title') == 'hide') ? 'hidden' : '';
    echo "<p style='clear:both;padding:0'><a href='$protocol://".URL."' style='color:inherit'><img src='$protocol://".URL."/images/logo.png' class='main-logo-img'><span class='main-logo-text $hidden'>".$OB_project_title."</span></a></p>";
?>
    </div>
    <!--Register / Login -->
<?php

    if (isset($_SESSION['Tcrypt'])) $user = $_SESSION['Tcrypt'];
    else $user = 0;

    if (defined('SHINYURL') and constant("SHINYURL"))
        $profile = "profile/$user/";
    else
        $profile = "?profile=$user";

    if(!isset($_SESSION['Tid'])) $reglink = "<a href='".$ref."?login' id='login'><span class='navlabel'><i class='fa fa-sign-in fa-lg'></i> ".t(str_login)."</span></a>";
    else {
        $cmd = "SELECT username,to_char(datum, 'Dy, Month DD. YYYY, HH24:MI') as d,news,uploader,receiver
        FROM project_news_stream LEFT JOIN users ON (users.id=uploader) 
        WHERE project_table='".PROJECTTABLE."' AND level='personal' AND receiver={$_SESSION['Tid']} AND readed='false' 
        ORDER BY datum DESC";
        $res = pg_query($BID,$cmd);
    
        $out = "";
        $n = pg_num_rows($res);
        if ($n)
            $out .= "<a style='color:green;font-weight:bold' href='#tab_basic' data-url='includes/profile.php?options=newsread&personal=".$_SESSION['Tid']."' title='$n ".str_you_have_new_message."' class='profilelink'><span style='position:absolute;right:3px;top:2px;color:red'>$n</span><i class='fa fa-envelope'></i></a>";

        $reglink = "<a href='#' class='to'>".$_SESSION['Tname']."&nbsp;</a>$out<ul class='subnav'><li><a href='".$ref."$profile'><span class='navlabel'><i class='fa fa-user fa-fw fa-lg'></i> ".t(str_profile)."</span></a></li><li><a href='".$ref."index.php?logout' id='logout'><span class='navlabel'><i class='fa fa-sign-out fa-fw fa-lg'></i> ".t(str_logout)."</span></a></li></ul>";
    }
?>
    <!-- Navigation -->    
    <div style='display:table-cell;width:auto;padding-top:10px;float:right;padding-right:20px'>
    <div id='nav'>
     <ul class='topnav tablink'>
        <li><a href='<?php echo $ref ?>upload/'><span class='navlabel'><i class='fa fa-upload fa-fw'></i> <?php echo t(str_upload) ?></span></a></li>
        <li><a href='<?php echo $ref ?>index.php?map'><span class='navlabel'><i class='fa fa-globe fa-fw'></i> <?php echo t(str_map) ?></span></a></li>
        <li><?php echo $reglink ?></li>
        <li><a href='#' class='to'><i class='fa fa-navicon'></i></a>
            <ul class='subnav'>
                <li><a href='<?php echo $ref ?>upload/'><i class='fa fa-upload fa-fw'></i> <?php echo t(str_upload) ?></a></li>
                <li><a href='<?php echo $ref ?>index.php?map'><i class='fa fa-globe fa-fw'></i> <?php echo t(str_map) ?></a></li>
                <li><a href='<?php echo $ref ?>index.php?database'><i class='fa fa-info fa-fw'></i> <?php echo t(str_summary) ?></a></li>
                <li><a href='<?php echo $protocol.'://'.OB_DOMAIN; ?>/projects/'><i class='fa fa-database fa-fw'></i> <?php echo t(str_dbs) ?></a></li>
            </ul></li>
        <li><a href='#' class='to'><span class="flag-icon flag-icon-<?= $_SESSION['LANG']; ?>"></span></a>
            <ul class="subnav">
            <?php
                foreach ($LANGUAGES as $L=>$label) {
                    //if ( file_exists(sprintf("%slanguages/%2s.php",getenv('PROJECT_DIR'),$L))) 
                        echo "<li><a href='?lang=$L' class='fl'><span class='flag-icon flag-icon-$L'></span>$label</a></li>";
                }
            ?>
            </ul>
        </li>
    </ul></div>
    </div>
</div>
