<?php

require_once(__DIR__.'/../includes/base_functions.php');
function job_log($message,$file=NULL,$line=NULL) {
    if(is_array($message)) {
        $message = json_encode($message,JSON_PRETTY_PRINT);
    }
    elseif(is_object($message)) {
        $message = json_encode($message,JSON_PRETTY_PRINT);
    }
    $custom_log = "/var/log/openbiomaps.log";

    $pointer = "";
    if($file!=NULL or $line!=NULL)
        $pointer = " $file $line";

    $date = date('M j h:i:s'); 
    print("[OBM_JOB_".PROJECTTABLE."] $date$pointer: $message\n");
}

/* pg functions */
function PGconnectSQL($db_user,$db_pass,$db_name,$db_host) {
    if ($db_host=='' or $db_user=='' or $db_pass=='' or $db_name=='') return;
    //$conn = pg_pconnect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    $conn = pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    return $conn;
}


function remote_job_call($function,$data,$type,$remote,$user) {
    ## ssl curl or ssh 
    # POST data to remote address
    #
    if ($type == 'ssh') {
        # simlified way with ssh auth
        $connection = ssh2_connect($remote, 22, array('hostkey'=>'ssh-rsa'));

        if(ssh2_auth_pubkey_file($connection, $user, '~/.ssh/id_rsa.pub', '~/.ssh/id_rsa')) {
            echo "Public Key Authentication Successful\n";
            $stream = ssh2_exec($connection, '/usr/bin/php job_master.php mailsend '.base64_encode($data));
            if ($stream !== false) {
                return $stream;
            } else 
                return false;
            
        } else {
            #die('Public Key Authentication Failed');
            return false;
        }
    }
}

function roundUp($n,$x=5) {
    return (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/* php mailer */
if (! function_exists('mail_to')) {
    function mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method,$ReplyToName='No Reply',$FromName=PROJECTTABLE) {

        require_once(__DIR__.'/../includes/vendor/autoload.php');

        //base64 subject
        $Subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';

        if ($Method == 'simple') 
        {
            $stripped_title = strip_tags($Title);
            $stripped_message = strip_tags($Message,'<a><br>');
            $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
            $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
            $body = $stripped_title."\n\n".$stripped_message;
            $plain_body = $body;
            $isHTML = false;
        }
        elseif ($Method == 'multipart') 
        {
            $body = "<h2>$Title</h2>$Message";
            $stripped_title = strip_tags($Title);
            $stripped_message = strip_tags($Message,'<a><br>');
            $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
            $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
            $plain_body = $stripped_title."\n\n".$stripped_message;
            $isHTML = true;
        }

        $mail = new PHPMailer(true);        // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            if (SENDMAIL=='sendmail') {
                $mail->isSendmail();

            } elseif(SENDMAIL=='smtp') {
                $From = defined('SMTP_SENDER') ? SMTP_SENDER : SMTP_USERNAME;

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = SMTP_HOST;                              // Specify main and backup SMTP servers: 'smtp1.example.com;smtp2.example.com'
                $mail->SMTPAuth = SMTP_AUTH;                          // Enable SMTP authentication
                $mail->Username = SMTP_USERNAME;                      // SMTP username
                $mail->Password = SMTP_PASSWORD;                      // SMTP password
                $mail->SMTPSecure = defined('SMTP_SECURE') ? SMTP_SECURE : 'tls'; // Enable TLS encryption, `ssl` also accepted
                $mail->Port = defined('SMTP_PORT') ? SMTP_PORT : 25;                                  // TCP port to connect to    
            }

            //Recipients
            $mail->setFrom($From, $FromName);
            #$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
            if (is_array($To)) {
                #$mail->addCC('cc@example.com');
                #$mail->addBCC('bcc@example.com');
                $first_rcp = array_pop($To);
                $mail->addAddress($first_rcp);
                foreach ($To as $cc) {
                    $mail->addCC($cc);
                }
            }
            else
                $mail->addAddress($To);               // Name is optional
            $ReplyTo = preg_replace('/localhost/','openbiomaps.org',$ReplyTo);
            $mail->addReplyTo($ReplyTo, $ReplyToName);

            //Attachments
            #$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            #$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML($isHTML);                                  // Set email format to HTML
            $mail->Subject = $Subject;
            $mail->Body    = $body;
            $mail->AltBody = $plain_body;

            $mail->CharSet = 'UTF-8';
            $mail->send();
            //"Message successfully sent!";
            return 2;
        } catch (Exception $e) {
            return 1;
        }    
    }
}

if (! function_exists('read_jobs') ) {
    function read_jobs() {

        $jobs_dir = __DIR__;
        if (!is_writable($jobs_dir)) {
            return ['error' => 'Jobs directory missing or not writable!'];
        }

        $table = file($jobs_dir.'/jobs.table'); 
        $jobs = array();
        foreach($table as $t) {
            $te = preg_split('/ /',$t);
            $jobs[trim($te[3])] = array($te[0],$te[1],$te[2]);
        }
        return $jobs;
    }
}

if (! function_exists('write_jobs') ) {
    function write_jobs($jobs) {
        if (is_null($jobs)) {
            return ['error' => 'write_jobs argument missing'];
        }

        $jobs_dir = __DIR__;
        if (!is_writable($jobs_dir)) {
            return ['error' => 'Jobs directory missing or not writable!'];
        }
        $content = '';
        foreach ($jobs as $job => $times) {
            $row = implode(' ',$times) . ' ' . $job . "\n";
            if (!preg_match('/^(\d{1,2}|\*) (\d{1,2}|\*) (\d|\*) ([[:alnum:]._-]+)/', $row, $output_array)) {
                return ['error' => "Syntax error: $row"];
            }
            $content .= $row;
        }

        return (file_put_contents($jobs_dir.'/jobs.table', $content) !== false);

    }
}
?>
