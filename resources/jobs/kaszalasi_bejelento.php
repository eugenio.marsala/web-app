<?php
/* A jobs/job.php
 * cron script
 *
 * */

/*
CREATE TABLE ..._alert (
    id integer NOT NULL,
    bejelentes_id integer NOT NULL,
    row_biotika text,
    status character varying(6),
    datetime timestamp with time zone DEFAULT now(),
    row_id integer,
    row_datum date,
    row_faj character varying(64)
);
CREATE TABLE ..._alert_people (
    email character varying(128) NOT NULL,
    geometry geometry NOT NULL,
    name character varying(128),
    CONSTRAINT enforce_dims_geometry CHECK ((st_ndims(geometry) = 2)),
    CONSTRAINT enforce_geotype_geometry CHECK ((geometrytype(geometry) = 'POLYGON'::text)),
    CONSTRAINT enforce_srid_geometry CHECK ((st_srid(geometry) = 4326))
);
-- kaszálási bejelentő tábla
CREATE TABLE ..._bejelentesek (
    id integer NOT NULL,
    iktatoszam character varying(16),
    ugyfel character varying(64),
    cim character varying(255),
    telepules character varying(64),
    hrszkod character varying(16),
    alrkod character varying(2),
    blokkaz character varying(16),
    terulet numeric,
    terv_date date,
    bej_date date,
    n2000 boolean,
    mtet character varying(32),
    kultura character varying(32),
    megjegyzes text,
    dok text,
    dok_tipus character varying(32),
    felt_datum date DEFAULT now(),
    geometry geometry(Geometry,4326),
    kasz_tipus character varying(128),
    CONSTRAINT enforce_dims_geometry CHECK ((st_ndims(geometry) = 2)),
    CONSTRAINT enforce_geotype_geometry CHECK ((geometrytype(geometry) = 'POLYGON'::text)),
    CONSTRAINT enforce_srid_geometry CHECK ((st_srid(geometry) = 4326))
);
CREATE SEQUENCE ..._id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TRIGGER bejelentes_trigger AFTER INSERT OR UPDATE ON ..._bejelentesek FOR EACH ROW EXECUTE PROCEDURE bejelentes_trigger();

CREATE OR REPLACE FUNCTION bejelentes_trigger() 
RETURNS trigger 
AS 
$bejelentes_trigger$
BEGIN
    IF tg_op = 'INSERT' OR tg_op = 'UPDATE'  THEN
         INSERT INTO ..._alert (bejelentes_id,status,row_id,row_datum,row_faj,row_biotika)
         SELECT new.id,'uj',obm_id,datum,faj,
            row_to_json((SELECT d FROM (SELECT egyed,adatkozlo,gyujto,gyujtesi_mod,felmeres_mod) d)) AS data
         FROM ... WHERE ST_Contains(new.geometry,obm_geometry);
         RETURN new;
     END IF;
END
$bejelentes_trigger$
LANGUAGE plpgsql;
 */


// Read variables
//$date = date('Y-01-01');
$months = 36; // how oldest data will be queried
$mailfrom = 'alert@obm.bnpi.hu'; //default central email address
$def_mailto = 'banm@vocs.unideb.hu'; //default central email address
$def_title = 'Kedves kolléga!';
$subject_template = "Új kaszálás bejelentés";
$project = 'dhte';
$url = 'obm.bnpi.hu/projects/dhte';
$path = $argv[1];
// other statements


$verbode = 1;

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/../local_vars.php.inc');
require_once($path.'/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

#
# query new alerts' data counts
$cmd = sprintf('SELECT DISTINCT bejelentes_id,iktatoszam,ugyfel,cim,telepules,hrszkod,alrkod,blokkaz,terulet,terv_date,bej_date,n2000,mtet, 
    kultura,megjegyzes,dok,dok_tipus,felt_datum,kasz_tipus,email,name
                FROM "%1$s_alert" a LEFT JOIN "%1$s_bejelentesek" b ON (a.bejelentes_id=b.id)
                    LEFT JOIN dhte_alert_people ap ON (st_intersects(ap.geometry,b.geometry))
                WHERE status=\'uj\'',$project);
$result = pg_query($ID,$cmd);
$alert_name = array();
$alert_mail = array();
$bejelentes_id = array();
$bejelentes_data = array();
while($r = pg_fetch_assoc($result)) {
    $alert_name[] = $r['name'];
    $alert_mail[] = $r['email'];
    $bejelentes_id[] = $r['bejelentes_id'];
    $bejelentes_data[] = array($r['iktatoszam'],$r['ugyfel'],$r['cim'],$r['telepules'],$r['hrszkod'],$r['alrkod'],$r['blokkaz'],$r['terulet'],$r['terv_date'],$r['bej_date'],$r['n2000'],$r['mtet'],$r['kultura'],$r['megjegyzes'],$r['dok'],$r['dok_tipus'],$r['felt_datum'],$r['kasz_tipus']);
}
#$subject = sprintf("%d %s [%s]",count($bejelentes_id),$subject_template,'DHTE');

if (count($bejelentes_id) == 0)
    exit;
																
#$message = "<!DOCTYPE html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><style>h2{color:indianred}h3{color:mediumslateblue}ul{list-style-type:none;color:gray}</style></head>";
#$message .= "Bejelentések: ";
#foreach ($bejelentes_id as $bid) {
#    $message .= sprintf('<a href="#b%1$d">%1$s</a> ',$bid);
#}
#$message .= "<hr>";

$n = 0;
$mail_sent = 0;   
$mail_array = array();

# 522 526
foreach ($bejelentes_id as $bid) {

    $subject = sprintf("%s [%d - %s]",$subject_template,$bid,'DHTE');

    $message = "<!DOCTYPE html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><style>h2{color:indianred}h3{color:mediumslateblue}ul{list-style-type:none;color:gray}</style></head>";
    $message .= "<br>";
    $message .= "<h2>Bejelentés adatok</h2><h3><a name='b$bid'>Bejelentési azonosító: $bid</a></h3><ul>";
    $message .= "<li>".implode('</li><li>',$bejelentes_data[$n])."</li>";

    # query alerts
    $cmd = sprintf("SELECT id,bejelentes_id,row_biotika,status,datetime,row_id,row_datum,row_faj FROM \"%s_alert\" WHERE status = 'uj' AND row_datum > NOW() - INTERVAL '%d month' AND bejelentes_id=%d ORDER by row_faj",$project,$months,$bid); 
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result) == 0) {
        $message .= "<h3>Nincsenek érintett biotikai adatok a területről</h3>";
    } else {
        $message .= "<h3>Biotikai adatok</h3>";
    }
    # 522-1 522-2 522-3 522-4
    while($row=pg_fetch_assoc($result)) {
        $message .= sprintf("<a href='http://%s/index.php?data&id=%d'>%s [%s]</a>: <ul><li>%s</li></ul>",$url,$row['row_id'],$row['row_faj'],$row['row_datum'],implode("</li><li>",json_decode($row['row_biotika'],true)));
    }

    $message .= "</ul></html>";

    // sending mail
    if ($verbode)
        print "Sending mails!\n";

    if ($alert_mail[$n]!='') $mailto = $alert_mail[$n];
    else $mailto = $def_mailto;
    if (preg_match('/,/',$mailto)) {
        $mailto = preg_split('/,/',$mailto);
    }

    if ($alert_name[$n]!='') $title = "Kedves ".$alert_name[$n]."!";
    else $title = $def_title;

    $m = mail_to($mailto,$subject,$mailfrom,$mailfrom,"<h1>$title</h1>",$message,"multipart"); 
    #$mail_array['mailto'] = $mailto; 
    #$mail_array['subject'] = $subject;
    #$mail_array['mailfrom'] = $mailfrom;
    #$mail_array['title'] = $title;
    #$mail_array['message'] = $message;
    #$m = remote_job_call('mail',$mail_array,'ssh','openbiomaps.org','banm');

    if ($verbode) {
        print "Mail status: $m\n";
    }
    if ($m == 2) {
        // update alert table if mail sent
        if ($verbode)
            print "Update alert statuses!\n";
        $cmd = sprintf("UPDATE \"%s_alert\" SET status='sent' WHERE bejelentes_id = %d",$project,$bid);
        $result = pg_query($ID,$cmd);
        $mail_sent++;
    } else {
        if ($verbode) {
            print "Mail sending failed.\n";
            print "Save alert report to file.\n";
        }

    }
    $n++;
}

if ($mail_sent < count($bejelentes_id)) {
    $filename = sprintf("data_%s.html",implode('-',$bejelentes_id));
    $fp = fopen($filename, 'w');
    fwrite($fp, "<h1>$title</h1>".$message);
    fclose($fp);
}


/* pg functions */
function PGconnectSQL($db_user,$db_pass,$db_name,$db_host) {
    if ($db_host=='' or $db_user=='' or $db_pass=='' or $db_name=='') return;
    //$conn = pg_pconnect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    $conn = pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    return $conn;
}

?>
