<?php
/* maintenance scheduler code */

$schedule_day = "2011/12/13";
$from = "23:00";
$length = "2 hours";
$address = "127.0.0.1";
$local_mconf = 0;
$system_mconf = 0;
$mconf = "../maintenance.conf";
$maintenance_message = "";

if (file_exists('./maintenance.conf'))
    $local_mconf = filemtime('./maintenance.conf');

if (file_exists('../maintenance.conf'))
    $system_mconf = filemtime('../maintenance.conf');

if ($local_mconf>$system_mconf)
    $mconf = "./maintenance.conf";

if (file_exists($mconf)) {
    $contents = file($mconf); 
    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>"0");

    foreach ($contents as $line) {

        if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $schedule_day = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $from = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $length = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $address = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }

        if (preg_match('/^## start day/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['day'] = 1;
        }
        elseif (preg_match('/^## from/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['from'] = 1;
        }
        elseif (preg_match('/^## length/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['length'] = 1;
        }
        elseif (preg_match('/^## maintenance address/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['address'] = 1;
        }
    }
    $date = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start_format = $date->format('Y-m-d H:i');
    $length = preg_replace("/hour.?/","H",$length);
    $length = preg_replace("/minute.?/","M",$length);
    $date_end = $date->add(new DateInterval('PT'.str_replace(" ","",$length)));
    $date_end_format = $date_end->format('Y-m-d H:i');

    $date_now = new DateTime();
    $interval = $date_now->diff($date_start);
    $diff = $interval->format('%r%d %h %i');

    #var_dump($interval);
    #echo 

    if ($_SERVER['REMOTE_ADDR']!=$address) {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24)
            $maintenance_message = "<span style='padding:10px;display:inline-block;'>Database maintenance will be scheduled between $date_start_format and $date_end_format!</span>";
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "<span style='padding:10px;display:inline-block;'>Database maintenance!</span>";
            include("maintenance.php");
        }
    } else {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24)
            $maintenance_message = "<span style='padding:10px;display:inline-block;background-color:pink;width:100%'>Database maintenance will be scheduled between $date_start_format and $date_end_format!</span>";
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "<span style='padding:10px;display:inline-block;background-color:pink;width:100%'>Database maintenance!</span>";
        }
    }
}
// **************************************************************************************************** \\


if (!isset($_COOKIE['LoadCookie'])) {
    $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
    setcookie("LoadCookie", 1, time()+3600,'/',$domain,false,true); 
}

#Header("Cache-Control: no-cache, must-revalidate");
#$offset = 60 * 60 * 24 * 3;
#$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
#Header($ExpStr);
session_cache_limiter('nocache');
session_cache_expire(0);
session_start();

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database with biomaps.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

// scheduled or topical updates
#require_once(getenv('OB_LIB_DIR').'db_updates.php');
#new db_updates('language files to db'); # 2018-08-
#new db_updates('project user status to varchar');   # 2018-09-08
#new db_updates('access & select_view to varchar');  # 2018-09-10

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');
if (isset($_SESSION['Tid']))
    require_once(getenv('OB_LIB_DIR').'languages-admin.php');


if (!isset($_SESSION['private_key']))
    $_SESSION['private_key'] = genhash();

#testing memcached
#$mcache = new Memcached();
#$mcache->addServer('localhost', 11211);
#$mcache->set('key', 'hello openbiomaps');
#log_action($mcache->getAllKeys());

/* Visitor counting
 * 
 * */
track_visitors('index');

if (!isset($_SESSION['st_col']))
    st_col('default_table');

/* Project name and description */
if(!isset($_SESSION['LANG'])) {
    //project default
    $_SESSION['LANG'] = LANG;
}

/* Custom intropage
 *
 * */
if ($load_intropage) {
    if (!isset($_SESSION['intropage_off'])) {
        if (file_exists('includes/intropage.php')) {
            include_once('includes/intropage.php');
            exit;
        }
    }
}

$cmd = "SELECT short,long,language,email FROM project_descriptions LEFT JOIN projects ON (project_table=projecttable) WHERE projecttable='".PROJECTTABLE."'";
$res = pg_query($BID,$cmd);
$row = pg_fetch_all($res);
$k = array_search($_SESSION['LANG'], array_column($row, 'language'));
if(!$k) $k = 0;
$OB_project_title = $row[$k]['short'];
//should be changed to markdown process !!!
//https://github.com/michelf/php-markdown/
//https://allinthehead.com/retro/364/dont-parse-markdown-at-runtime
$OB_project_description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/","<br><br>",$row[$k]['long']);
$OB_contact_email = $row[$k]['email'];

?>
<!DOCTYPE html>
<html>
<head>
<?php
if (defined('GOOGLE_ANALYTICS')) { 
    echo "    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=".GOOGLE_ANALYTICS."\"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '".GOOGLE_ANALYTICS."');
    </script>";
}
?>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<?php
if ($load_map) {
/*    header("Cache-Control: public, max-age=86400");
    $offset = 60 * 60 *24;
    $ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
    header($ExpStr);*/
    #echo '    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />';
    #echo '    <meta http-equiv="Pragma" content="no-cache" />';
    #echo '    <meta http-equiv="Expires" content="0" />';
    #echo '    <meta http-equiv="Cache-Control" content="public, max-age=3600" />';
    #echo '    <meta http-equiv="Expires" content="'.gmdate('D, d M Y H:i:s', time()+3600).' GMT" />';
}
?>

    <title>OBM - <?php echo $OB_project_title ?></title>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.css?rev=<?php echo rev('js/ui/jquery-ui.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/pure-min.css?rev=<?php echo rev('css/pure/pure-min.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/grids-responsive-min.css?rev=<?php echo rev('css/pure/grids-responsive-min.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/font-awesome/css/font-awesome.min.css?rev=<?php echo rev('css/font-awesome/css/font-awesome.min.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/vote.css?rev=<?php echo rev('css/vote.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/dropit.css?rev=<?php echo rev('css/dropit.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/style.css?rev=<?php echo rev('css/style.css'); ?>" type="text/css" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.cookie.js?rev=<?php echo rev('js/jquery.cookie.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/ui/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/dropit.js?rev=<?php echo rev('js/dropit.js'); ?>"></script>
    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('js/functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('js/roller.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/main.js?rev=<?php echo rev('js/main.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/includes/modules.js.php?rev=<?php echo rev('includes/modules.js.php'); ?>"></script>

<?php
// Include custom css if exists
// szerintem ez nem kell, hanem helyette a css_loader[] kell használni
if (file_exists('css/private/custom.css')) {
    echo '<link rel="stylesheet" href="'.$protocol.'://'. URL .'/css/private/custom.css?rev='.rev('css/private/custom.css').'" type="text/css" />';
}
//include project's own javascript
//e.g.
//include google api with project level api key

# USE local_maps_functions.js.php !!!
if ($load_map) {
    if (file_exists('includes/private/private_map_js.php'))
        include_once('includes/private/private_map_js.php');
    else
        //google api is no more available by default, should be placed into private_map_js.php
        echo "<script src='https://maps.google.com/maps/api/js?v=3.5'></script>";
}
?>
<?php
if (isset($_SESSION['Tid'])) {
?>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/profile.css?rev=<?php echo rev('css/profile.css'); ?>" type="text/css" />
<!-- Include main -->

<?php
    if ($load_profile or $load_showapikeys) {
?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/admin_functions.js?rev=<?php echo rev('js/admin_functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/admin.js?rev=<?php echo rev('js/admin.js'); ?>"></script>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/admin.css?rev=<?php echo rev('css/admin.css'); ?>" type="text/css" />

<?php
    }
}
?>
</head>
<?php 
// ez mi?
// css class nem lehet a page...
$page = (!empty($_SESSION['current_page'])) ? array_keys($_SESSION['current_page'])[0] : 'mainpage'; ?>
<body id='index' data-page="<?= $page ?>" <?php 

    if ($load_map) echo "onload='init();";

    if ($load_getqueryapi) {
        // query api call
        // that means parameters coming as a json array
        // example:
        // index.php?query_api={"qids_...":"[\"...\"]"}
        $j = $_GET['query_api'];
        if(!$load_map) echo "onload='";
        echo "loadQueryMap($j);";
        if(!$load_map) echo "'";

    } else if ($load_getquery) {
        // that means parameters coming as plain text
        // example:
        // index.php?query=faj:Pica pica;...
        $qs = preg_split('/;/',$load_getquery_text);
        $m = array();
        foreach( $qs as $q) {
            $kv = preg_split('/:/',$q);
            $m["qids_$kv[0]"] = json_encode(array($kv[1]));
        }
        $p = json_encode($m);
        if(!$load_map) echo "onload='";
        echo "loadQueryMap($p);";
        if(!$load_map) echo "'";

    } else if ($load_getreport) {
        // sql queries stored in the database and called by with their names
        if(!$load_map) echo "onload='";
        echo "loadQueryMap({ \"report\":\"$load_getreport_label\" });";
        if(!$load_map) echo "'";

    }

    if ($load_map) echo "'"; ?>>
<div id="holder">
<?php
// Header band -->
// OB_project_title OB_project_description
// defined in header.php.inc
if (!isset($_GET['query_api'])) {
    include('header.php.inc');
}
agree_new_terms();


// Custom pages includes        
if (isset($_GET['includes']) and file_exists('includes/custom_pages.php')) {
    $custom_load_page = $_GET['includes'];
    if (isset($_GET['subpage']))
        $clp_subpage = $_GET['subpage'];
    else
        $clp_subpage = '';
    include_once('includes/custom_pages.php');
}

// Photo viewer div
if (!isset($_SESSION['Tid'])) {
?>
<div id="photodiv">
    <div id="photodiv-buttons">
        <button class='pure-button button-success button-large p-download' title='<?php echo str_download_full_size ?>'><i class='fa fa-download fa-lg'></i></button> 
        <button class='pure-button button-secondary button-large p-expand'><i class='fa fa-arrows-alt fa-lg'></i></button> 
        <button class='pure-button button-secondary button-large p-exif'>Info</button>
        <button class='pure-button button-warning button-large p-close'><i class='fa fa-times fa-lg'></i></button>
    </div>
    <div id="photodiv-frame"></div>
    <div style='padding:1px 5px 3px 0px;width:auto;position:relative;z-index:1;background-color:black;padding-top:10px;height:100%'>
        <div id='photodiv-comment' style='margin-top:4px;width:auto;min-width:200px'></div>
        <ul id='photodiv-exif'></ul>
        <div id='photodiv-thumbnails'></div>
    </div>
</div>
<?php
} else {
// Photo editor div
?>
<div id="photodiv">
    <div id="photodiv-buttons">
        <button class='pure-button button-success button-large p-download' title='<?php echo str_download_full_size ?>'><i class='fa fa-download fa-lg'></i></button> 
        <button class='pure-button button-secondary button-large p-expand'><i class='fa fa-arrows-alt fa-lg'></i></button> 
        <button class='pure-button button-secondary button-large p-exif' >Info</button>
        <button class='pure-button button-error button-large p-delete' title='<?= str_delete_image_from_the_folder ?>'><i class='fa fa-trash fa-lg'></i></button> 
        <button class='pure-button button-warning button-large p-save' title='<?= str_save_image_with_comment ?>'><i class='fa fa-floppy-o fa-lg'></i></button>
        <button class='pure-button button-secondary button-large p-close'><i class='fa fa-times fa-lg'></i></button> 
    </div>
    <div id="photodiv-frame"></div>
    <div style='padding:1px 5px 3px 0px;width:auto;position:relative;z-index:1;background-color:black;padding-top:10px;height:100%'>
        <div id="photodiv-data"></div>
        <textarea id='photodiv-comment' style='margin-top:4px;width:auto;min-width:400px'></textarea>
        <ul id='photodiv-exif'></ul>
        <div id='photodiv-thumbnails'></div>
    </div>
</div>
<?php
}

// Body div
?>
<div id='body'>
<div id='message'><?php echo $load_message; ?></div>
<div id="dialog" title="<?php echo t(str_dialog) ?>"><p id='Dt'></p></div>
<div id="dialog-message" title="<?php echo t(str_message) ?>"><p id='Dm'></p></div>
<div id="dialog-confirm" title="<?php echo t(str_confirm) ?>"><p id='Dtc'></p></div>
<?php
// Messages
if (isset($em)) {
    echo $em;    
}

if ($load_loaddata) {
    global $BID;
    list($id,$s) = preg_split('/@/',$load_loaddata);
    $cmd = "SELECT result FROM \"queries\" WHERE queries.id='$id' AND sessionid='$s'"; 
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) 
    {
        $row=pg_fetch_assoc($result);
        $obj = $row['result'];
        echo "<style>pre {outline: 1px solid #ccc; padding: 5px; margin: 15px; }
.string { color: green; }
.number { color: darkorange; }
.boolean { color: blue; }
.null { color: magenta; }
.key { color: red; }</style>";
        echo "<div id='jsontext'></div>";
?>
        <script>
        function syntaxHighlight(json) {
            json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
        }
<?php
        echo "var str = JSON.stringify($obj, null, 2);
                document.getElementById('jsontext').appendChild(document.createElement('pre')).innerHTML = syntaxHighlight(str);
            </script>";

    }

}


if ($load_loaddoi) {
    global $BID;
    $cmd = sprintf("SELECT doi FROM projects WHERE doi ILIKE %s",quote('%'.$load_loaddoi));
    
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) {
        $load_loadmeta = 0;
        $load_doimetadata = 1;
    }
}

if ($load_loadmeta) {
    global $BID;
    if ($load_loadmeta == '__DOI__') {
        $cmd = sprintf("SELECT name,datetime,query,to_char(datetime,'YYYY-MM-DD HH:MI') as date,\"type\",abstract,authors,q.licence,q.licence_uri,q.id,sessionid,\"domain\",username,q.doi,query,length(result) as size,ARRAY_TO_STRING(q.subjects,';;') as subjects,formats,data_collectors,lower(date_range) as date_range_from, upper(date_range) AS date_range_to,publisher 
            FROM \"queries\" q LEFT JOIN header_names ON (f_main_table=q.comment) LEFT JOIN \"projects\" ON (f_table_name=project_table) LEFT JOIN users ON (user_id=users.id) WHERE q.doi ILIKE %s",quote('%'.$load_loaddoi)); 
        $load_loadmeta = $load_loaddoi;
    
    } else {
        list($id,$s) = preg_split('/@/',$load_loadmeta);
        $cmd = "SELECT name,datetime,query,to_char(datetime,'YYYY-MM-DD HH:MI') as date,\"type\",abstract,authors,q.licence,q.licence_uri,q.id,sessionid,\"domain\",username,q.doi,query,length(result) as size,ARRAY_TO_STRING(q.subjects,';;') as subjects,formats,data_collectors,lower(date_range) as date_range_from, upper(date_range) AS date_range_to,publisher
            FROM \"queries\" q LEFT JOIN header_names ON (f_main_table=q.comment) LEFT JOIN \"projects\" ON (f_table_name=project_table) LEFT JOIN users ON (user_id=users.id) WHERE q.id='$id' AND sessionid='$s'"; 
    }

    $result = pg_query($BID,$cmd);
    $em = '';
    if (pg_num_rows($result)) 
    {    
        $row=pg_fetch_assoc($result);

        $project_hash = substr(base_convert(md5($row['date'].$row['username'].PROJECTTABLE), 16,32), 0, 12);

        if ($row['doi']!='') {
            $doi = $row['doi'];
        } else 
            $doi = "<span style='color:red'>NOT REGISTERED YET</span> <i>10.18426/obm.$project_hash</i>";
            if ($row['domain']!='')
                $link = sprintf("%s",$row['domain']);
            else
                $link = URL;

            $em = "<h2 style='font-size:2em !important'>Metadata page for \"<i>$load_loadmeta</i>\" stored query<br></h2>";
            $em .= '<ul style="list-style-type:none">';

            $em .= "<li><h2>Identifier</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>$doi</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Creators</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>".$row['authors']."</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Titles</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>".$row['name']."</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Publisher</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>{$row['publisher']}</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Publication date</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>".$row['date']."</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Subject</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                //$subem .= sprintf("<li>Data subset based on query from ".PROJECTTABLE." database:<br> %s</li>",preg_replace("/\\\/","",str_replace(array("\\r\\n", "\\r", "\\n"), "<br />",$row['query'])));
                foreach (preg_split('/;;/',$row['subjects']) as $s) {
                    $subem .= "<li>$s</li>";
                }
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Contributors</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>Type: DataCollector</li><ul>";
                $subem .= "<li>ContributorName: {$row['data_collectors']}</li>";
                //sprintf("<li>Data subset based on query from ".PROJECTTABLE." database:<br> %s</li>",preg_replace("/\\\/","",str_replace(array("\\r\\n", "\\r", "\\n"), "<br />",$row['query'])));
                $subem .= "</ul></ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Dates</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>From {$row['date_range_from']} to  {$row['date_range_to']}</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Languages</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li></li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Resource Type</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>{$row['type']}</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Format</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>{$row['formats']}</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Alternate identifier</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>URL: <a href='$protocol://$link/LQ/{$row['id']}@{$row['sessionid']}/'>$protocol://$link/LQ/{$row['id']}@{$row['sessionid']}/</a>"."</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Sizes</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>{$row['size']} bytes</li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Rights</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>Licence:<ul><li>{$row['licence']}</li></ul></li>";
                $subem .= "<li>Licence-URI:<ul><li>{$row['licence_uri']}</li></ul></li>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= "<li><h2>Description</h2>";
                $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
                $subem .= "<li>Abstract:<ul><li>{$row['abstract']}</li></ul></li>";
                $subem .= "<li>Other: 
                    <ul><li>Raw data url: <a href='$protocol://$link/LQ/{$row['id']}@{$row['sessionid']}/data/'>$protocol://$link/LQ/{$row['id']}@{$row['sessionid']}/data/</a></li></li></ul>
                    <ul><li>Map url: <a href='$protocol://$link/LQ/{$row['id']}@{$row['sessionid']}/map/'>$protocol://$link/LQ/{$row['id']}@{$row['sessionid']}/map/</a></li></li></ul>";
                $subem .= "</ul>";
            $em .= $subem."</li>";
            $em .= '</ul>';
    }
    echo $em;
}

/* Data page of a data row => edit attributes
 * 
 * */
if ($load_data) {

    if (file_exists('includes/private/private_map_js.php'))
        include_once('includes/private/private_map_js.php');
    else
        echo "<script src='https://maps.google.com/maps/api/js?v=3.5'></script>";

    if (!isset($_SESSION['current_query_table']))
        $_SESSION['current_query_table'] = PROJECTTABLE;

    $restore_cqt = $_SESSION['current_query_table'];
    if (isset($_GET['table'])) {
        if (preg_match("/^".PROJECTTABLE."/",$_GET['table']))
            $_SESSION['current_query_table'] = $_GET['table'];
    }

    include_once(getenv('OB_LIB_DIR').'results_builder.php');
    echo "<h2 style='font-size:2em !important;line-height:2.5em !important'>".t(str_datapage).": [{$_SESSION['current_query_table']}] {$_SESSION['getid']}</h2>"; 
    include_once(getenv('OB_LIB_DIR').'data-sheet-page.php');
    $_SESSION['current_query_table'] = $restore_cqt;
}

/* History pages 
 *
 * */
if ($load_datahistory) {

    if (!isset($_SESSION['current_query_table']))
        $_SESSION['current_query_table'] = PROJECTTABLE;

    $restore_cqt = $_SESSION['current_query_table'];
    if (isset($_GET['table'])) {
        if (preg_match("/^".PROJECTTABLE."/",$_GET['table']))
            $_SESSION['current_query_table'] = $_GET['table'];
    }

    include_once(getenv('OB_LIB_DIR').'results_builder.php');
    echo "<h2>".t(str_datahistorypage).": {$_SESSION['getid']}</h2>"; 
    echo "<div style='max-width:100vw;overflow-x: auto;margin-right:30px'>";

    include_once(getenv('OB_LIB_DIR').'data-history-page.php');

    echo "</div>";
    $_SESSION['current_query_table'] = $restore_cqt;
}

/* Voting history page
 *
 * */
if ($load_validhistory) {
    include_once(getenv('OB_LIB_DIR').'interface.php');
    echo "<p><h2>".t(str_validhist)."</h2><br>";
    echo valid_history(); 
}

/* Upload history page
 *
 * */
if ($load_uploadhistory) {
    include_once(getenv('OB_LIB_DIR').'interface.php');
    echo "<p><h2>".str_uphist."</h2><br>";
    echo upload_history(); 
}

/* MainPage
 * templatable
 * */
if ($load_mainpage) {
    # mainpage grid
    echo print_grid();
}

/* DISPLAY MAP div and
 * display the click results in the matrix div
 * */
if ($load_mappage) {
    //result slides...
    unset($_SESSION['slide']);
    unset($_SESSION['slides']);
    unset($_SESSION['wfs_array']);
    unset($_SESSION['wfs_full_array']);

?>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/roller.css?rev=<?php echo rev('css/roller.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/mapsdata.css?rev=<?php echo rev('css/mapsdata.css'); ?>" type="text/css" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/OpenLayers.js?rev=<?php echo rev('js/OpenLayers.js'); ?>"></script>
    <script>obj.load_map_page=1;</script>
<?php
    //include project's own maps javascript OR local maps
    if (file_exists('includes/local_maps_functions.js.php'))
        include_once('includes/local_maps_functions.js.php');
    else
        echo '<script type="text/javascript" src="'.$protocol.'://'.URL.'/js/maps_functions.js?rev='.rev('js/maps_functions.js').'"></script>'
?>
    
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/proj4js-compressed.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/javascript.util.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jsts.js"></script>
    <!--<script src="https://cdn.rawgit.com/bjornharrtell/jsts/gh-pages/1.4.0/jsts.min.js"></script>-->

    
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/maps.js?rev=<?php echo rev('js/maps.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/colour-ring-box.js?rev=<?php echo rev('js/colour-ring-box.js'); ?>"></script>
    
<?php
    include_once(getenv('OB_LIB_DIR').'interface.php');

    // map page with filters and map
    echo load_map();

    // right-side navigator buttons
    echo load_navigator();
} 

/* Lost Password
 *
 * */
if ($load_lostpw and !isset($_SESSION['Tid'])) { ?>
    <br><h2><?php echo t(str_lostpasswd) ?></h2><br>
    <form method=post  class='form pure-form'>
    <?php echo t(str_newactivation) ?><br><br> <input placeholder='<?php echo str_email ?>' size=40 name='loginmail'><br>
    <br>
    <button type='submit' class='button-success button-xlarge pure-button' name='lostpasswd' value='<?php echo t(str_send) ?>'><?php echo t(str_send) ?> <i class="fa fa-send"></i></button>
    <br>
    <br>
    <br>
    </form>
<?php 
} elseif ($load_lostpw and isset($_SESSION['Tid'])) {
    $load_profile = 1;
}

/* Profile page
 *
 * */
if ($load_profile !== 0) {
    include_once(getenv('OB_LIB_DIR').'interface.php');
    if (isset($_SESSION['Tcrypt'])) {
        if (isset($_SESSION['login_vars'])) {
            /* Go back to the page where login button clicked instead of default profile page*/
            $n = $_SESSION['login_vars'];
            unset($_SESSION['login_vars']);
            $n = preg_replace('/logout/','login',$n);
            echo "<script>window.location = '$n'</script>";
        } else {
            echo profile($load_profile,$load_orcid_profile_data);
        }
    }
    else
        $load_message = "Login error!";
}
/* Show saved queries
 * */
if ($load_savedqueries) {
    if (isset($_SESSION['Tid'])) {
        include_once(getenv('OB_LIB_DIR').'interface.php');

        echo tabs($_SESSION['Tcrypt']);
        echo  "<div style='padding:30px 0px 0px 20px' id='tab_basic'>";
        ShowSavedQueries($_SESSION['Tid']);
        echo "</div>";
    }
}
/* Show own API keys 
 * */
if ($load_showapikeys) {
    if (isset($_SESSION['Tid'])) {
        include_once(getenv('OB_LIB_DIR').'interface.php');

        echo tabs($_SESSION['Tcrypt']);
        echo  "<div style='padding:30px 0px 0px 20px' id='tab_basic'>";
        ShowAPIKeys($_SESSION['Tid']);
        echo "</div>";
    }
}

/* Show user's uploads
 * */
if ($load_showuploads) {
    if (isset($_SESSION['Tid'])) {
        include_once(getenv('OB_LIB_DIR').'interface.php');

        echo tabs($_SESSION['Tcrypt']);
        echo  "<div style='padding:30px 0px 0px 20px' id='tab_basic'>";
        ShowUploads($_SESSION['getuid']);
        echo "</div>";
    }
}
/* DOI meta page
 *
 * */
if ($load_doimetadata) {
    include_once(getenv('OB_LIB_DIR').'interface.php');
    echo load_database_doimetadata(); 
}
/* Show Database Summary page
 *
 * */
if ($load_database) { 
    include_once(getenv('OB_LIB_DIR').'interface.php');
    echo load_database_summary(); 
}
/* Species list
 *
 * */
if ($load_specieslist) { 
    include_once(getenv('OB_LIB_DIR').'interface.php');
?>
    <h2><?php echo $OB_project_title.' - '.str_specieses; ?></h2>
    <div style='padding-top:30px'><?php echo taxonlist(); ?></div>
<?php
}
/* Own species list
 *
 * */
if ($load_userspecieslist) { 
    include_once(getenv('OB_LIB_DIR').'interface.php');
    echo usertaxonlist($load_userspecieslist);
}

/* Species sheet: view and edit
 *
 * */
if ($load_metaname) { 
    include_once(getenv('OB_LIB_DIR').'interface.php');
?>
    <h2><?php echo t(str_taxon_names); ?></h2>
    <div style='padding-top:30px'><?php echo metaname(); ?></div>
<?php
}

// no function calls below
/* Login Box
 *
 * */
if ($load_login and !isset($_SESSION['Tid'])) {
    echo login_box();
} 
/* Registration process
 *
 * */
if ($load_register) {
    if (!isset($registration_code) or strlen($registration_code)!=32) $registration_code='';

?>  <form method='post' class='pure-form' style='margin:30px;font-size:150%;max-width:800px'>
    <?php echo t(str_invitate); ?>
    <br>
    <br>
    <label for='invcode' style='width:130px;display:block'><?php echo t(str_inv_code); ?>:</label> 
    <input name='invcode' style='font-family: Consolas, Lucida Console, monospace' size='32' value='<?php echo $registration_code; ?>'>
    <br>
    <br>
    <input type='submit' class='button-success button-xlarge pure-button pure-u-1-3' name='inventer' value='<?php echo t(str_send); ?>'>
    </form> <?php
}
/* Data usage agreement
 *
 * */
if ($load_useagreement) { ?>
    <h2>
    <?php echo t(str_tc); ?>
   </h2>
    <div style='min-height:300px;padding-top:30px'>
        <?php
            if ($_SESSION['LANG']=='hu')
                include('DataUseAndSharingAgreement-hu.html');
            else
                include('DataUseAndSharingAgreement-en.html');
        ?>    
    </div>
<?php
}
elseif ($load_privacy) { ?>

    <br><br> 
    <a href='http://openbiomaps.org/OBM_at.pdf'><h2><?php echo t(str_privacy_policy) ?></h2></a>
        <?php 
        if (file_exists('./at_'.$_SESSION['LANG'].'.html'))
            include('./at_'.$_SESSION['LANG'].'.html');
        else
            include('./at.html');
        ?>    
<?php 
}

/**/
elseif ($load_terms) { ?>

    <br><br> 
    <a href='http://openbiomaps.org/OBM_aszf.pdf'><h2><?php echo str_user_agreement ?></h2></a>
    <div style='text-align:left;width:700px;white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */'>
    <?php 
        if (file_exists('./aszf_'.$_SESSION['LANG'].'.html'))
            include('./aszf_'.$_SESSION['LANG'].'.html');
        else
            include('./aszf.html');
    ?>  
</div>
<?php 
}

/**/
elseif ($load_technical) { ?>

    <br><br> 
    <h2><?php echo str_technical_info ?></h2>
        <?php 
        if (file_exists('./server_'.$_SESSION['LANG'].'.html'))
            include('./server_'.$_SESSION['LANG'].'.html');
        else
            include('./server.html');
        ?>    
<?php 
}

/* Using Cookies */
elseif ($load_cookies) { ?>
    <h2><?php echo str_cookies ?></h2>
        <?php 
        if (file_exists('./cookies_'.$_SESSION['LANG'].'.html'))
            include('./cookies_'.$_SESSION['LANG'].'.html');
        else {
            include('./cookies.html');
        }
        ?>
<?php
}

/* What's new? */

elseif ($load_whatsnew) { ?>
    <h2><?php echo str_whatsnew ?></h2>
        <?php 
        if (file_exists('includes/whatsnew_'.$_SESSION['LANG'].'.html'))
            include('includes/whatsnew_'.$_SESSION['LANG'].'.html');
        else {
            include('includes/whatsnew.html');
        }
        ?>
<?php
}


?>
</div><!--/body-->

    <?php
if (!isset($_SESSION['Tterms_agree']) || !$_SESSION['Tterms_agree']) {
    if (defined('OB_PROJECT_DOMAIN')) {
        $domain = OB_PROJECT_DOMAIN;
    } elseif (defined('OB_DOMAIN')) {
        $domain = OB_DOMAIN;
    } else 
        $domain = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];

    if (!isset($_SESSION['cookies_accepted'])) {
        echo "<div id='cookie_div' style='text-align:center;position:fixed;z-index:9999;bottom:0;padding:20px 50px 20px 50px;background-color:black;color:#acacac'>";
        if ($_SESSION['LANG']!='hu') {
            echo "
        This site uses cookies to deliver our services and to offer you a better browsing experience. By using our site, you acknowledge that you have read and understand our <a href='$protocol://$domain/cookies/'>Cookie Policy</a>, <a href='$protocol://$domain/privacy/'>Privacy Policy</a>, and our <a href='$protocol//$domain/terms/'>Terms of Services</a>. Your use of OpenBioMaps’s Products and Services, including the OpenBioMaps Network, is subject to these policies and terms. <button class='pure-button' id='cookie-ok'>Accept</button>";
        } else {
            echo "
        Ez a weboldal Sütiket használ az oldal működésének biztosításához és a jobb felhasználói élmény biztosítása érdekében. A Sütik weboldalon történő használatával kapcsolatos részletes tájékoztatás az <a href='$protocol://$domain/privacy/'>Adatkezelési Tájékoztatóban</a> és a <a href='$protocol://$domain/cookies/'>Süti Tájékoztatóban</a> és a <a href='$protocol://$domain/terms/'>Felhasználói szabályzatban</a> található. <button class='pure-button' id='cookie-ok'>Rendben</button>";
        }
        echo "</div>";
    }
}
    ?>

<?php
if (!isset($_GET['query_api'])) {
    include('footer.php.inc');
}
//pg_close($ID);
//pg_close($GID);
//pg_close($BID);
?>
</div><!--/holder-->
</body>
</html>
