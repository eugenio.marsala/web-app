<?php
    /* Logging out
     *
     * */
    require_once(getenv('PROJECT_DIR').'local_vars.php.inc');

    require_once(getenv('OB_LIB_DIR').'modules_class.php');
    require_once(getenv('OB_LIB_DIR').'db_funcs.php');
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");
    if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        die("Unsuccesful connect to UI database.");
    if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");


    pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));
    pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
    pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
    pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_table_%s_%s",PROJECTTABLE,session_id()));
    pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_stable_%s_%s",PROJECTTABLE,session_id()));
    pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_list_%s_%s",PROJECTTABLE,session_id()));

    function is_session_started() {
        if ( php_sapi_name() !== 'cli' ) {
            if ( version_compare(phpversion(), '5.4.0', '>=') ) {
                return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
            } else {
                return session_id() === '' ? FALSE : TRUE;
            }
        }
        return FALSE;
    }

    if ( is_session_started() === FALSE ) session_start();
    //$path = PATH.'/'.PROJECTTABLE;
    //$lifetime=3600;
    //if (isset($_COOKIE['PHPSESSID']) and $_COOKIE['PHPSESSID']!=session_id()) {
        //setcookie(session_name(),$_COOKIE['PHPSESSID'],time()-$lifetime,'/');
        /*setcookie(session_name(),$_COOKIE['PHPSESSID'],time()-$lifetime,"$path/upload");
        setcookie(session_name(),$_COOKIE['PHPSESSID'],time()-$lifetime,"$path/upload/");
        setcookie(session_name(),$_COOKIE['PHPSESSID'],time()-$lifetime,"$path/includes");
        setcookie(session_name(),$_COOKIE['PHPSESSID'],time()-$lifetime,"$path/includes/");*/
    //}
    //setcookie(session_name(),session_id(),time()-$lifetime,'/');
    /*setcookie(session_name(),session_id(),time()-$lifetime,"$path/upload");
    setcookie(session_name(),session_id(),time()-$lifetime,"$path/upload/");
    setcookie(session_name(),session_id(),time()-$lifetime,"$path/includes");
    setcookie(session_name(),session_id(),time()-$lifetime,"$path/includes/");*/
    session_destroy();
    session_unset();
    session_cache_expire(0);
    session_set_cookie_params(0,"/");
    session_cache_limiter('nocache');
    ini_set('session.cookie_lifetime',0);
    ini_set('session.gc_maxlifetime',0);

    session_start();
    session_regenerate_id(TRUE);
    $_GET = array();
    $_POST = array();
    $_REQUEST = array();

    $m = new modules();
    $_SESSION['modules'] = $m->set_modules();

    if ( isset($_COOKIE['access_token']) ) {
        setcookie("access_token",$_COOKIE['access_token'],time()-1,'/');
        unset($_COOKIE["access_token"]);
    }    
    if ( isset($_COOKIE['refresh_token']) ) {
        setcookie("refresh_token",$_COOKIE['refresh_token'],time()-1,'/');
        unset($_COOKIE["refresh_token"]);
    }

    
?>
