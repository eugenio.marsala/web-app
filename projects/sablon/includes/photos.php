<?php
session_start();
session_write_close();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

$root_dir = getenv('PROJECT_DIR').'/attached_files';
if (!is_dir("$root_dir")) mkdir("$root_dir");

//create thumbnails directory automagically
if (!is_dir("$root_dir/thumbnails")) mkdir("$root_dir/thumbnails");


$file = '';
$files = array();
$file_exifs = array();
$fullsize = 0;
$thumb = 0;
$img = 1;
$url = 0;
$r = array();
$size = 'small';

if (isset($_REQUEST['id'])) $id = preg_replace("/[^0-9]/","",$_REQUEST['id']);

$lc = new LocaleManager();
$lc->doBackup();
$lc->fixLocale();

// get file description
if (isset($_REQUEST['description'])) {
    //No validity and access check!!
    //$id = preg_replace("/[^0-9]/","",$_POST['description']);
    $ref = $_REQUEST['description'];
    $r['reference'] = $ref;
    $r['data-connect'] = sprintf('%s',"No connected data");
    $r['comment'] = "";
    $data_table = PROJECTTABLE;

    $cmd = sprintf('SELECT data_table FROM system.files WHERE reference=%s AND project_table=%s',quote($ref),quote(PROJECTTABLE));
    $res = pg_query($ID,$cmd);
    if(pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['data_table'])
            $data_table = $row['data_table'];
    }

    //query file's comment and data connections
    $cmd =  sprintf('SELECT f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.exif
                    FROM system.files f 
                    LEFT JOIN system.file_connect fc ON f.id=fc.file_id 
                    LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                    WHERE f.project_table=\'%4$s\' AND f.reference=%3$s
                    GROUP BY f.id,f.reference,f.comment,fc.conid 
                    ORDER BY fc.conid',$data_table,'obm_id',quote($ref),PROJECTTABLE);

    $row = array();
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $r['comment'] = $row['comment'];
        if ($row['obm_id']!='')
            $r['data-connect'] = sprintf('<a href="http://'.URL.'/index.php?data&id=%1$d" target="_blank" id="%1$d">%1$d</a>',$row['obm_id']);
    } 

    if (isset($row['exif']) and $row['exif']!='') { 
        $r['exif'] = json_decode($row['exif'],true);
    } else {
        if (file_exists("$root_dir/$ref")) {
            if (in_array(exif_imagetype("$root_dir/$ref"),array(IMAGETYPE_JPEG ,IMAGETYPE_TIFF_II , IMAGETYPE_TIFF_MM, IMAGETYPE_PNG))) {
                $exif_read = exif_read_data("$root_dir/$ref", 0, false);
                if (json_encode($exif_read))
                    $r['exif'] = $exif_read;
                else
                    $r['exif'] = array($exif_read['FileName'],$exif_read['FileDateTime'],$exif_read['FileSize'],$exif_read['FileType'],$exif_read['MimeType'],$exif_read['Model'],'corrupted exif...');
            } else {
                $a = exec("mediainfo $root_dir/$ref",$output);
                $n = array();
                mb_regex_encoding('UTF-8');
                mb_internal_encoding("UTF-8");
                foreach ($output as $o) {
                    $on = mb_split("\s+: ",$o);
                    if(count($on)>1) {
                        if($on[0]=='Complete name') {
                            $on[1] = basename($on[1]);
                        }
                        $n[$on[0]] = $on[1];
                    } 
                }
                $r['exif'] = $n;
            }
        }
    }
    //exit; 
}

//get file connection informations
if (isset($_REQUEST['connId'])) {
    //No validity and access check!!
    //$id = preg_replace("/[^0-9]/","",$_POST['description']);
    $connId = $_REQUEST['connId'];
    $cmd = sprintf("SELECT reference,id FROM system.file_connect LEFT JOIN system.files ON (files.id=file_connect.file_id) WHERE conid=%s ORDER BY files.id",quote($connId),PROJECTTABLE);
    $res = pg_query($ID,$cmd);
    while($row = pg_fetch_assoc($res)) {
        $files[$row['reference']] = $row['id'];
    }
}

//url information request - for full access
if (isset($_REQUEST['url'])) {
    // photo url request
    $url = 1;
    $img = 0;
}

// which files? reference/id/filename requests
if (isset($_REQUEST['ref'])) {
    //thumbnail requested by using url, not params
    //the thumbnail size ignored here
    //if(preg_match('/^\/thumbnails\/\d+x\d+_(.+)$/',$_REQUEST['ref'],$m)) {
    // hisoric way to handle image size:
    //
    //  http://...getphoto?ref=/thumbnails 
    //  too complicated?
    if (preg_match('/^\/thumbnails\/(.+)$/',$_REQUEST['ref'],$m)) {
        // set default size
        $size = 'thumb';
        $_REQUEST['ref'] = $m[1];
    } else if (preg_match('/^\/(.+)$/',$_REQUEST['ref'],$m)) {
        // set default size
        $size = 'small';
        // starter / removed
        $_REQUEST['ref'] = $m[1];
    }
    // photo request by name
    $cmd = sprintf("SELECT id,reference,comment,mimetype,exif FROM system.files WHERE reference=%s",quote($_REQUEST['ref']));
    $res = pg_query($ID,$cmd);
    if ($row = pg_fetch_assoc($res)) {
        $files[$row['reference']] = $row['id'];
        $file_exifs[$row['reference']] = $row['exif'];
    } else {
        $lc->doRestore();
        echo common_message('error','invalid request');
        return;

    }
} elseif (isset($id) and trim($id)!='') {
    // do we need it really?
    // where?
    $cmd = sprintf("SELECT id,reference,comment,mimetype,exif FROM system.files WHERE id=%s",quote($id));
    $res = pg_query($ID,$cmd);
    if ($row = pg_fetch_assoc($res)) {
        $files[$row['reference']] = $row['id'];
        $file_exifs[$row['reference']] = $row['exif'];
    } else {
        $lc->doRestore();
        echo common_message('error','invalid request');
        return;
    }
} elseif (isset($_REQUEST['file'])) {
    //??? where we use it???
    $file = basename($_REQUEST['file']);
    $files[$file] = '-1';
}


// get first file of connected files
if (isset($_REQUEST['connectionId'])) {
    //No validity and access check!!
    //$id = preg_replace("/[^0-9]/","",$_POST['description']);
    $cid = $_REQUEST['connectionId'];
    #$cmd = sprintf("SELECT reference,id FROM system.file_connect LEFT JOIN system.files ON (files.id=file_connect.file_id) WHERE conid=%s AND conid>0 ORDER BY datum",quote($cid));
    $cmd = sprintf("SELECT reference,id FROM system.file_connect LEFT JOIN system.files ON (files.id=file_connect.file_id) WHERE conid=%s AND conid!='0' ORDER BY datum",quote($cid));
    $res = pg_query($ID,$cmd);

    $file = pg_fetch_assoc($res);

    header("Content-type: application/json");
    print(json_encode($file));
    // exit?

}

//pg_close($ID);

//image size requests
if(isset($_REQUEST['getfullimage'])) {
    //full access request
    $size = 'full';
}
elseif (isset($_REQUEST['getthumbnailimage'])) {
    //thumbnail request
    if ($_REQUEST['getthumbnailimage']==1)
        $size = 'thumb';
    else
        $size = 'small';
} 
elseif (isset($_REQUEST['getwidth'])) {
    // exact size requests
    $getwidth = preg_replace('/[^0-9]/','',$_REQUEST['getwidth']);
    $size = 'exact';
}


$url_array = array();

foreach ($files as $file=>$id) {
    $purl = array("url"=>'',"type"=>'',"filename"=>'',"id"=>'');
    $is_image = 0;
    $ori_name = $file;
    $con_str = isset($connId) ? "c=$connId&" : '';
    if (file_exists("$root_dir/$file")) {
        //image validation
        if(false !== (list($width,$height) = @getimagesize("$root_dir/$file"))){
            $is_image = 1;
        }
        if($size == 'full') {
            //get the file - should be disabled the direct access with .htaccess
            $file = basename($file);
            $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'',"filename"=>$file,"id"=>$id);
        } elseif($size=='thumb') {
            //get a thumbnail of file if possible
            if($is_image){
                if ($thumb = mkThumb($file,60)) {
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'thumbnails',"filename"=>$file,"id"=>$id);
                    $file = "/thumbnails/$thumb";
                } else {
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'icon',"filename"=>$file,"id"=>$id);
                }
            } else {
                //not image, but can create preview image
                if ($thumb = mkThumb($file,60)){
                    // not image!! sor remove .jpg extension from the reference
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'thumbnails',"filename"=>$file,"id"=>$id);
                    $file = "/thumbnails/$thumb";
                } else {
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'icon',"filename"=>$file,"id"=>$id);
                }
            }
        } else if ($size == 'small') {
            //small - default
            if($is_image){
                if($width>600 or $height>400) {
                    if ($thumb = mkThumb($file,600)) {
                        if (!is_dir("$root_dir/thumbnails")) mkdir("$root_dir/thumbnails");
                        $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'thumbnails',"filename"=>$file,"id"=>$id);
                        $file = "/thumbnails/$thumb";
                    } else {
                        $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'icon',"filename"=>$file,"id"=>$id);
                    }
                }
            } else {
                //not image, but can create preview image
                if ($thumb = mkThumb($file,600)) {
                    if (!is_dir("$root_dir/thumbnails")) mkdir("$root_dir/thumbnails");
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'thumbnails',"filename"=>$file,"id"=>$id);
                    $file = "/thumbnails/$thumb";
                } else {
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'icon',"filename"=>$file,"id"=>$id);
                }
            }
        } else if ($size == 'exact') {
            //user defined size
            if($is_image){
                if ($width>$getwidth) {
                    if ($thumb = mkThumb($file,$getwidth)) {
                        if (!is_dir("$root_dir/thumbnails")) mkdir("$root_dir/thumbnails");
                        $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'thumbnails',"filename"=>$file,"id"=>$id);
                        $file = "/thumbnails/$thumb";
                    } else {
                        $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'icon',"filename"=>$file,"id"=>$id);
                    }
                }
            } else {
                //not image, but can create preview image
                if ($thumb = mkThumb($file,$getwidth)) {
                    if (!is_dir("$root_dir/thumbnails")) mkdir("$root_dir/thumbnails");
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'thumbnails',"filename"=>$file,"id"=>$id);
                    $file = "/thumbnails/$thumb";
                } else {
                    $purl = array("url"=>"http://".URL."/getphoto?{$con_str}ref=","type"=>'icon',"filename"=>$file,"id"=>$id);
                }
            }

        }

        //print out the file
        if(!$url) {
            $mime = mime_content_type("$root_dir/$file");
            header("Content-Type: $mime");
            header('Content-Length: '.filesize("$root_dir/$file")); 
            if ((isset($_GET['Disposition']) and $_GET['Disposition'] == 'attachment' ) or !isset($_GET['Disposition']))
                header("Content-Disposition: attachment; filename=\"$file\";");
            header("Expires: -1");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            
            readfile("$root_dir/$file");

            $lc->doRestore();
            exit;
        }
    } else {
        log_action('File does not exists: '.$file,__FILE__,__LINE__);
    }
    if(count($purl)) {
        if (isset($file_exifs[$ori_name]))
            $purl['exif'] = $file_exifs[$ori_name];
        $url_array[] = $purl;
    }
}
//print out a json_array
//
if ($url) {
    $r['file_properties'] = $url_array;
    print json_encode($r, JSON_PRETTY_PRINT);
}

$lc->doRestore();
exit;

?>
