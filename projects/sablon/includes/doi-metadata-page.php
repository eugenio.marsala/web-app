<?php

    $lang = $_SESSION['LANG']; //default english in DOI

    $cmd = "SELECT short,long,language,email FROM project_descriptions LEFT JOIN projects ON (project_table=projecttable) WHERE projecttable='".PROJECTTABLE."'";
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_all($res);
    $k = array_search($_SESSION['LANG'], array_column($row, 'language'));
    if(!$k) $k = 0;
    $OB_project_title = $row[$k]['short'];
    $OB_project_description = $row[$k]['long'];
    $OB_contact_email = $row[$k]['email'];

    $cmd = 'SELECT "Creator" as founder,to_char(creation_date,\'YYYY mon. DD.\') as fdate,creation_date,
        to_char(running_date,\'YYYY mon. DD.\') as rdate,doi,licence,rum,licence_uri,array_to_string(alternate_id,\';\') as aid,lower(collection_dates) as cdate1,upper(collection_dates) as cdate2,project_hash,ARRAY_TO_STRING(subjects,\';;\') AS subjects,geolocation
FROM projects WHERE project_table=\''.PROJECTTABLE.'\'';
    $result = pg_query($BID,$cmd);
    $project_row=pg_fetch_assoc($result);

    $out = sprintf("<h2 style='font-size:2em !important'>DataCite DOI metadata page for \"<i>%s</i>\" database<br></h2>",PROJECTTABLE);
    $json = array();

    # DOI
    $project_hash = substr(base_convert(md5($project_row['creation_date'].$project_row['founder'].PROJECTTABLE), 16,32), 0, 12);
    if ($project_row['doi']!='') {
        $doi = '<label class="unselectable">doi: </label>'.$project_row['doi'];
        $json['Identifier'] = 'doi: '.$project_row['doi'];
    } else if ($project_row['project_hash']!='') {
        $doi = "<span style='color:red'>DOI: NOT REGISTERED YET</span> <i>10.18426/obm.".$project_row['project_hash']."</i>";
        $json['Identifier'] = 'DOI: NOT REGISTERED YET 10.18426/obm'.$project_row['project_hash'];
    } else {
        $out .= "<ul><li>Fill the project_hash: ".$project_hash."</li></ul>";
        return $out;
    }
    $out .= "<a name='Identifier'></a><h2>Identifier</h2>";
    $out .= sprintf("<ul style='padding:0 0 30px 30px;list-style-type:none'><li style='font-size:2.0em'>%s</li></ul>", $doi);

    # CREATOR
    $out .= "<a name='Creators'></a><h2>Creators</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>%s</li>",$project_row['founder']);
    $out .= "</ul>";
    $json['Creator'] = $project_row['founder'];

    # TITLE
    $out .= "<a name='Titles'></a><h2>Titles</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $cmd = "SELECT short FROM project_descriptions WHERE language = 'en' AND projecttable='".PROJECTTABLE."'";
    if ($res = pg_query($BID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $OB_project_title = $row['short'];
    }

    $out .= sprintf("<li>%s</li>",$OB_project_title);
    $out .= "</ul>";
    $json['Titles'] = $OB_project_title;

    # PUBLISHER
    $out .= "<a name='Publisher'></a><h2>Publisher</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>%s</li>","The OpenBioMaps Consortium");
    $out .= "</ul>";
    $json['Publisher'] = "The OpenBioMaps Consortium";

    # PUBLICATION YEAR
    $out .= "<a name='PublicationYear'></a><h2>Publication Year</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>%s</li>",$project_row['fdate']);
    $out .= "</ul>";
    $json['PublicationYear'] = $project_row['fdate'];

    # SUBJECT
    $out .= "<a name='Subject'></a><h2>Subject</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    foreach (preg_split('/;;/',$project_row['subjects']) as $s) {
        $out .= "<li>$s</li>";
    }
    $out .= "</ul>";
    $json['Subject'] = $project_row['subjects'];

    # CONTRIBUTORS
    $out .= "<a name='contributors'></a><h2>Contributors</h2>";
    $cmd = 'SELECT id,username,institute,email,orcid
    FROM users u 
    LEFT JOIN project_users pu ON (pu.user_id=u.id)
    WHERE pu.project_table=\''.PROJECTTABLE.'\' AND user_status::varchar IN (\'2\',\'master\')';
    $result = pg_query($BID,$cmd);
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    //$out .= sprintf("<li>Type: DataCurator</li><ul><li>Name: %s</li><li>Affiliation: %s &nbsp; %s</li>",$row['username'],$row['institute'],$row['email']);
    while ($row=pg_fetch_assoc($result)) {
        $out .= sprintf("<li>Type: DataManager</li><ul><li>Name: %s</li><li>Affiliation: %s &nbsp; %s</li>",$row['username'],$row['institute'],$row['email']);
        if ($row['orcid']!='') {
            $out .= sprintf("<li>Identifier: <ul><li>Identifier Scheme: ORCID</li><li>Scheme URI: http://orcid.org/</li><li>Identifier: %s</li></ul></li>",$row['orcid']);
        }
        $out .= "</ul>";
    }
    $out .= "</ul>";
    $json['Contributors'] = array(
        'Type'=>array(
            'Data Manager'=>array(
                'Name'=>$row['username'],'Affiliation'=>$row['institute'].' '.$row['email'],'Identifier'=>array(
                'Identifier Scheme'=>'ORCID','Scheme URI'=>'http://orcid.org/','Identifier'=>$row['orcid']))
    ));

    # DATES
    $out .= "<a name='Dates'></a><h2>Dates</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>Accepted: %s</li>",$project_row['rdate']);
    if ($project_row['cdate1']!='' and $project_row['cdate2']!='')
        $out .= sprintf("<li>Collected: %s - %s</li>",$project_row['cdate1'],$project_row['cdate2']);
    $out .= "</ul>";
    $json['Dates'] = array('Accepted'=>$project_row['rdate'],'Collected'=>"{$project_row['cdate1']},{$project_row['cdate2']}");

    ## LANGUAGE
    $out .= "<a name='Languages'></a><h2>Languages</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";

    $l = array('en'=>'English','hu'=>'Hungarian','ro'=>'Romanian');
    $cmd = "SELECT language FROM project_descriptions WHERE projecttable='".PROJECTTABLE."'";
    $result = pg_query($BID,$cmd);
    $jl = array();
    while ($row=pg_fetch_assoc($result)) {
        if (array_key_exists($row['language'],$l)) {
            $out .= sprintf("<li>%s</li>",$l[$row['language']]);
            $jl[] = $l[$row['language']];
        } else {
            $out .= sprintf("<li>%s</li>",$row['language']);
            $jl[] = $row['language'];
        }
    }
    $out .= "</ul>";
    $json['Languages'] = $jl;

    # RESOURCE TYPE
    $out .= "<a name='ResourceType'></a><h2>Resource Type</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>Service</li>");
    $out .= "</ul>";
    $json['ResourceType'] = 'Service';

    # ALTERNATE IDENTIFIER
    $out .= "<a name='AlternateIdentifier'></a><h2>Alternate Identifier</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>URL: http://%s</li>",URL);
    if ($project_row['aid']!='')
        $out .= sprintf("<li><i>(type)</i>: %s</li>",$project_row['aid']);
    $out .= "</ul>";
    $json['AlternateIdentifier'] = $project_row['aid'];

    # SIZES
    $out .= "<a name='Sizes'></a><h2>Sizes</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";

    $cmd = "SELECT f_main_table FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
    $result = pg_query($BID,$cmd);
    $approx = 0;
    while($row = pg_fetch_assoc($result)) {
        $cmd = "SELECT reltuples AS approximate_row_count FROM pg_class WHERE relname = '{$row['f_main_table']}'";
        $result2 = pg_query($ID,$cmd);
        $rowe = pg_fetch_assoc($result2);
        $approx += $rowe['approximate_row_count'];
    }

    $out .= sprintf("<li>%s tables</li>",pg_num_rows($result));
    $out .= sprintf("<li>%d rows</li>",$approx);
    $out .= "</ul>";
    $json['Sizes'] = $row['array_length'].'/'.$approx;

    # RIGHTS
    $out .= "<h2>Rights</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>Rights: %s <ul><li>URI: %s</li></ul></li>",$project_row['licence'],$project_row['licence_uri']);
    $out .= "</ul>";
    $json['Rights'] = Array($project_row['licence'],Array('URI'=>$project_row['licence_uri']));

    # DESCRIPTION / ABSTRACT
    $out .= "<h2>Description</h2>";

    $cmd = "SELECT long FROM project_descriptions WHERE language = 'en' AND projecttable='".PROJECTTABLE."'";
    if ($res = pg_query($BID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $OB_project_description = $row['long'];
    }
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= "<li>Abstract: <ul><li style='width:900px;text-align:justify'>$OB_project_description</li></ul></li>";
    $json['Description'] = $OB_project_description;

    # DESCRIPTION / OTHER
    $out .= "<li>Other: <ul>";
    $cmd = "SELECT srid,type FROM geometry_columns WHERE f_table_name='".PROJECTTABLE."'";
    if ($res = pg_query($ID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $out .= sprintf('<li>GEO Projection (epsg): %1$s</li>',$row['srid']);
        $out .= sprintf("<li>PostGIS geometry type: %s</li>",strtolower($row['type']));
    }

    if (defined('PUBLIC_MAPFILE')) {
        $url = "http://".URL."/public/proxy.php?MAP=PMAP&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities";
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if ($xml !== FALSE) {
            $out .= "<li>Public <i>WFS</i> service: $url</li>";
        }
        $url = "http://".URL."/public/proxy.php?MAP=PMAP&SERVICE=WMS&VERSION=1.1.0&REQUEST=GetCapabilities";
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if ($xml !== FALSE) {
            $out .= "<li>Public <i>WMS</i> service: $url</li>";
        }
    }
    $out .= sprintf("<li>OBM openness class: %s <ul><li>URI: %s</li></ul></li>",$project_row['rum'],'http://openbiomaps.org/documents/en/faq.html#what-is-the-rum');
    $out .= "</ul></li></ul>";

    # GeoLocation
    $out .= "<h2>GeoLocation</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>geoLocationPlace: %s</li>",$project_row['geolocation']);
    $out .= "</ul>";
    $json['GeoLocation'] = Array($project_row['geolocation']);



    $out .= "</ul>";

    $_SESSION['LANG'] = $lang;

?>
