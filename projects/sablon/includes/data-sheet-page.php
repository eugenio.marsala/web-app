<?php

    st_col($_SESSION['current_query_table']);
    $_SESSION['evaluates'] = array();

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $ACC_LEVEL = ACC_LEVEL;

    //getid can be complex id of different included tables separated by _ the first number is the maintable id
    $getid = preg_replace('/[^0-9]+/','',$_SESSION['getid']);

    if (isset($_SESSION['modules']) && isset($_SESSION['current_query_table'])) {
        $modules = new modules();
        $sheet_table = $_SESSION['current_query_table'];
    }
    else
        return;

    //track data view
    track_data($getid);

    // photo module - hidden div
    echo $modules->_include('photos','upload_box');

    require_once(getenv('OB_LIB_DIR').'results_builder.php');
    $rb = new results_builder();
    if ($rb->results_query($getid,true)) {
        $result = pg_query($ID,$rb->rq['query']);
    } else {
        echo $rb->error;
        return;
    }
    $COLUMNS = $rb->rq['columns'];
    //unset($COLUMNS[$_SESSION['st_col']['GEOM_C']]); 

    $dh = sprintf("<ul style='list-style-type:none;padding-left:10px;max-width:1000px'>");

    if(isset($_SESSION['Tid'])) $pe = 1;
    else $pe = 0;

    $CITE_C = $_SESSION['st_col']['CITE_C'];
    $DATE_C = $_SESSION['st_col']['DATE_C'];

    $SPECIES_C = $_SESSION['st_col']['SPECIES_C'];
    if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$SPECIES_C,$m))
        $SPECIES_C = $m[2];
    $FILE_ID_C = 'obm_files_id';
    $GEOM_C = $_SESSION['st_col']['GEOM_C'];
    
    $BOLD_YELLOW = ($modules->is_enabled('bold_yellow')) 
        ? $modules->_include('bold_yellow','mark_text',[],true)
        : array();

    //ezt az edit gombok használják
    $t = 'b'.$getid;

    /* Ez majd lehet az alapja, hogy lehessen kapcsolt táblákban is módosítani
     *
     * */ 
    $table = '';
    /*if ($rb->rq['joins_vnames']!='') {
        //log_action($rb->rq['joins']);
        //LEFT JOIN dinpi_mirror ON (dinpi_mirror.obm_id=dinpi.obm_id)
        $join_tables = array();
        $join_conditions = array();
        foreach($rb->rq['joins_vnames'] as $table=>$columns) {
            if(preg_match("/$table\.([a-z0-9_]+)/",$rb->rq['joins'],$m)) {
                $join_conditions[$table] = $m[1];
            }
            $join_cols = array();
            foreach($columns as $key=>$val) {
                $join_cols[] = sprintf("%s_%s",$table,$key);
            }
            $join_tables[$table] = $join_cols;
        }
    }*/
     
    $allowed_cols = array_keys($COLUMNS);
    $allowed_general_columns = array();

    //a subset of the main cols, depending on user's access rights
    if ($modules->is_enabled('allowed_columns')) {
        $allowed_cols = $modules->_include('allowed_columns','return_columns',array(array_keys($COLUMNS)),true);
        $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array(array_keys($COLUMNS)),true);
    } else {
        if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
            // drop all column for non logined users if access level is higher
            $allowed_cols = array();
        }
    }

    // simplified access restriction
    $general_restriction = 0;
    if (!grst(PROJECTTABLE,4) and count($allowed_general_columns)) {
        $general_restriction = 1;
        $allowed_cols = $allowed_general_columns;
    }

    // csak a graybg miatt
    $main_cols = dbcolist('columns',$sheet_table);

    if (!isset($_SESSION['Tid']))
        $tid = 0;
    else
        $tid = $_SESSION['Tid'];
    if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
        $tgroups = 0;
    else
        $tgroups = $_SESSION['Tgroups'];

    $non_repetative = array();
    $ek = array();

    $geom_show = "";
    while ($e=pg_fetch_assoc($result)) {

        $ek = $e;
        //ha van jogom szerkeszteni, akkor megjelenik a ceruza ikon a sorok előtt
        if (rst('mod',$e['obm_id'],$sheet_table)) {$pe = 1;$pi='<i class="fa fa-pencil"></i>';}
        else {$pe = 0;$pi='';}

        $acc = 1;
        if (!rst('acc',$e['obm_id'],$sheet_table)) 
            $acc = 0;

        $geometry_restriction = 0;
        // drop geometry column if it is restricted
        if (isset($_SESSION['st_col']['RESTRICT_C']) and $_SESSION['st_col']['RESTRICT_C'] and array_search('obm_geometry',$allowed_cols)!==false) {
            
            if (!grst(PROJECTTABLE,4)) {
                $cmd = sprintf("SELECT CASE WHEN sensitivity::varchar NOT IN ('1','no-geom') OR (sensitivity::varchar IN ('1','no-geom') AND ($tid=ANY(read))) THEN 0 ELSE 1 END AS geometry_restriction 
                    FROM %s_rules WHERE data_table=%s AND row_id=%d",PROJECTTABLE,quote($sheet_table),$e['obm_id']);

                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    if ($row['geometry_restriction']) {
                        $geometry_restriction = 1;
                    }
                }
            }
            /*if (!grst(PROJECTTABLE,4)) {
               $cmd = sprintf("SELECT CASE WHEN sensitivity::varchar NOT IN ('2','restricted') OR (sensitivity::varchar IN ('2','restricted') AND ($tid=ANY(read))) THEN 0 ELSE 1 END AS column_restriction 
                    FROM %s_rules WHERE data_table=%s AND row_id=%d",PROJECTTABLE,quote($sheet_table),$e['obm_id']);

                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    if ($row['column_restriction']) {
                        $column_restriction = 1;
                    }
                }
            }*/
        }


        foreach($COLUMNS as $cn=>$label) 
        {
            if (preg_match('/(\w)+\.(.+)$/',$cn,$m)) {
                $cn = $m[2];
                if (preg_match('/(\w)+\.(.+)$/',$label,$m)) {
                    $label = $m[2];
                }
            }

            /* High level restrictions handling
            /* we can query data but drop details if it is out of allowed */

            if (!$acc) {

                if (isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group' )) {
                    $general_restriction = 1;
                    
                } elseif ( !in_array($cn,$allowed_cols) ) {
                    continue;
                }
            }
            
            if ($geometry_restriction and $cn == 'obm_geometry') continue;

            // general restrictions for group level access projects
            // which prevent all access excepting admins
            if ($general_restriction) {
                if ( !in_array($cn,$allowed_general_columns) ) {
                    continue;
                }
            }

            //we do not print the history time column if it passed, as it is a special column and there is no meaning to edit it ever!
            if ($cn=='hist_time') continue;
            
            //mark joined tables
            $jts = ""; //gray style
            if (!in_array($cn,$main_cols)) {
                $jts = "graybg";
            } else {
                # main table columns
                # restore main table name
                //$table = PROJECTTABLE;
                $table = $sheet_table;
            }
            
            if (in_array($cn,$BOLD_YELLOW) and isset($e[$cn])  and !in_array($cn,$non_repetative)) { 

                if ($jts=='')
                    $non_repetative[] = $cn;
                else {
                    $pe = 0;
                    $pi = '';
                }
                //auto translation of scientific name
                //if ($cn==$SPECIES_C ) $label = lang_label('str_sci_name');
                // bold yellow attributes
                //
                // edit ($value,$id,$name,$enabled,$function,$active=0,$action='') 
                $dh .= sprintf("<li class='lmh $jts'><span class='obm_label' style='display:table-cell'>%s:</span> <span style='display:table-cell'>%s %s</span></li>",t($label),$pi,edit($e[$cn],"$cn$t",$cn,$pe,'edbox_send',0,'',$table));
            }
            elseif ($cn == $FILE_ID_C and isset($e[$cn]) and !in_array($cn,$non_repetative)) {

                if ($jts=='')
                    $non_repetative[] = $cn;
                else {
                    $pe = 0;
                    $pi = '';
                }

                $cmd = sprintf("SELECT id,reference,substring(comment,1,12) as comment FROM system.files LEFT JOIN system.file_connect ON (files.id=file_connect.file_id) WHERE conid='%s' AND status!='deleted'",$e[$cn]);
                //log_action($cmd);
                $res = pg_query($ID,$cmd);
                $p = array();
                $dh .= sprintf("<li class='lm $jts'><span class='obm_label' style='display:table-cell'>%s:</span>  <span style='display:table-cell'>%s %s</span></li>",t($label),'<i class="fa fa-paperclip"></i>',edit('',"$cn$t",$cn,$pe,'edbox_send',0,'photo-here',$table));
                if(pg_num_rows($res)) {
                    while ($row = pg_fetch_assoc($res)) {
	                if ($thumb = mkThumb($row['reference'],60)) {
                            $thf = "$protocol://".URL."/getphoto?ref=/thumbnails/{$row['reference']}";
                            $p[] = "<a href='$protocol://".URL."/getphoto?c={$e[$cn]}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$thf' title='{$row['comment']}' class='thumb'></a>";
                	} else {
                    	    $mime_url = mime_icon($row['reference'],32);
                            $p[] = "<a href='$protocol://".URL."/getphoto?c={$e[$cn]}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$mime_url' title='{$row['comment']}' class='thumb'></a>";
                    	}
                    }
                    $e[$cn] = implode(" ",$p);
                    $dh .= sprintf("<li class='lm $jts'><span class='obm_label' style='display:table-cell'></span>  <span style='display:table-cell;vertical-align:bottom;padding-top:5px;padding-bottom:15px'>%s</span></li>",$e[$cn]);
                }
                //else {
                    //there is a photo connect id in data, but it is deleted,
                    //so upload a new file...
                //}
            }
            elseif ($cn==$FILE_ID_C and $FILE_ID_C!='' and !in_array($cn,$non_repetative)) {

                if ($jts=='')
                    $non_repetative[] = $cn;
                else {
                    $pe = 0;
                    $pi = '';
                }

                $dh .= sprintf("<li class='lm $jts'><span class='obm_label' style='display:table-cell'>%s:</span>  <span style='display:table-cell'>%s %s</span></li>",t($label),'<i class="fa fa-paperclip"></i>',edit('',"$cn$t",$cn,$pe,'edbox_send',0,'photo-here',$table));
            
            } elseif ($cn==$GEOM_C and $GEOM_C!='' and !in_array($cn,$non_repetative)) {

                if ($jts=='')
                    $non_repetative[] = $cn;
                else {
                    $pe = 0;
                    $pi = '';
                }

                if (isset($_SESSION['Tid']))
                    $cmd = sprintf('SELECT id,name,to_char(timestamp,\'YYYY.MM.DD:HH24:MI:SS\') as datetime 
                        FROM system.shared_polygons LEFT JOIN system.polygon_users p ON polygon_id=id 
                        WHERE select_view IN (\'2\',\'3\',\'upload\',\'select-upload\') AND p.user_id=%d 
                        ORDER BY name,datetime',$_SESSION['Tid']);
                else 
                    $cmd = sprintf('SELECT id,name,to_char(timestamp,\'YYYY.MM.DD:HH24:MI:SS\') as datetime 
                        FROM system.shared_polygons 
                        WHERE access IN (\'4\',\'public\') OR (project_table=\'%s\' AND access IN (\'3\',\'project\')) 
                        ORDER BY name,datetime',PROJECTTABLE);

                $res = pg_query($ID,$cmd);
                $gopt = "<option>".str_get_geometry_from_named_list."</option>";
                while($row=pg_fetch_assoc($res)) {
                    $gopt .= sprintf("<option value='%d'>%s - %s</option>",$row['id'],$row['name'],$row['datetime']);
                }

                $u = "$protocol://".URL."/upload/geomtest/?addgeom";
                $gopt_window = "<div class='fix-modal' id='gpm' style='border:1px solid #666;border-bottom:4px solid #666'>
                    <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:-10px -10px 10px -10px !important'>".t(str_geometry)."</h2><button style='margin: -11px' class='pure-button button-passive fix-modal-close' id='gpm-close'><i class='fa fa-close'></i></button>
                    <br>
                    <ul style='list-style-type:none;padding:10px'>
                        <li style='font-size:150%;padding-bottom:1em'><a href='$u' id='map_' class='mapclick'><i class='fa fa-lg fa-map-marker fa-fw'></i> ".str_get_coordinates_from_map."</a></li>
                        <li style='font-size:150%'><i class='fa fa-lg fa-map fa-fw'></i> <select id='gopt_' class='goptchoice'>$gopt</select></li></ul>
                    </div>";

                //$dh .= sprintf("<li class='lm $jts'>$gopt_window<span class='obm_label' style='display:table-cell'>%s:</span>  <span style='display:table-cell'>%s %s</span></li>",t($label),'<i class="fa fa-map-marker"></i>',edit($e[$cn],"$cn$t",$cn,1,'edbox_send',0,'geom-down',$table,'sheet'));

                $geom_show = $e[$cn];
/*
 * ez a transform_data azt hiszem nem csinál semmit. kikommenteltem 2019-02-27-én. Gábor
                if ($modules->is_enabled('transform_data')) {
                    $params = $modules->get_params('transform_data');
                    #Array("modszer:shunt","hely:geom");
                    $pmi = array();
                    foreach($params as $pm) {
                        $c = preg_split('/:/',$pm);
                        if ($c[0] == $GEOM_C) {
                            $e[$cn] = $modules->_include('transform_data','text_transform','obm_geometry:geom_wkt',array($e[$cn],$cn,''));
                        }
                    }
                }
             */
                // display on small map
                $geom_show = $e[$cn];
                //$e[$cn] = $dmodule->transform_data('text_transform',$e[$cn],$cn,$modules[$mki]['p']);
                
                $dh .= sprintf("<li class='lm $jts'>%s<span class='obm_label' style='display:table-cell'>%s:</span>  <span style='display:table-cell'>%s %s</span></li>",$gopt_window,t($label),'<i class="fa fa-map-marker"></i>',edit($e[$cn],"$cn$t",$cn,$pe,'edbox_send',0,'geom-down',$table,'sheet'));

            } elseif (isset($e[$cn]) and !in_array($cn,$non_repetative)) {

                if ($jts=='')
                    $non_repetative[] = $cn;
                else {
                    $pe = 0;
                    $pi = '';
                }

                // normal attributes
                /*    $mki = array_search('transform_results', array_column($modules, 'module_name'));
                    if ($mki!==false and function_exists($modules[$mki]['f'])) {
                        $e[$cn] = call_user_func($modules[$mki]['f'],$e[$cn],$cn,$modules[$mki]['p']);
                    } elseif ($mki!==false) {
                        //default itegrated module
                        $dmodule = new defModules();
                        $dmodule->set('results_builder.php');
                        $e[$cn] = $dmodule->transform_data($e[$cn],$cn,$modules[$mki]['p']);
                 }*/

                $dh .= sprintf("<li class='lm $jts'><span class='obm_label' style='display:table-cell'>%s:</span>  <span style='display:table-cell'>%s %s</span></li>",t($label),$pi,edit($e[$cn],"$cn$t",$cn,$pe,'edbox_send',0,'',$table));
            }
            elseif ( $e[$cn]=='' and !in_array($cn,$non_repetative)) {
                if ($jts=='')
                    $non_repetative[] = $cn;
                else {
                    $pe = 0;
                    $pi = '';
                }

                // empty attributes
                $dh .= sprintf("<li class='lm $jts'><span class='obm_label' style='display:table-cell'>%s:</span>  <span style='display:table-cell'>%s %s</span></li>",t($label),$pi,edit('',"$cn$t",$cn,$pe,'edbox_send',0,'',$table));
            }
        }
    }
    $dh .= sprintf("</ul>");

    //non sensitive data and or existing data in the current query table
    if (isset($ek['obm_id'])) {

        // small map
        //
        if ($geom_show!='') {

            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
            if (preg_match('/^point|line|polygon/i',$geom_show))
                $cmd = "SELECT st_AsGeoJSON('$geom_show'::text,15,0) as gjson, st_AseWKT('$geom_show'::text) as wkt";
            else
                $cmd = "SELECT st_AsGeoJSON('$geom_show'::geometry,15,0) as gjson, st_AseWKT('$geom_show'::geometry) as wkt";

            /* Ha lenne a pontnak neve és van json type (psql 9.3<)
             * $cmd = "SELECT row_to_json(fc) FROM (SELECT 'Feature' As type, 
                      ST_AsGeoJSON('$text')::json As geometry, 
                      row_to_json((SELECT l FROM (SELECT 'valami' AS name) As l)) As properties) As fc";*/
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                            //{"type":"Point","coordinates":[18.767587411656972,47.785560590815244]}
                
                //$geojson = '{"type":"Feature","geometry": {"type": "Point","coordinates": [125.6, 10.1]},"properties": {"name": "Dinagat Islands"}}';
                $wkt = preg_replace('/SRID=\d+;/','',$row['wkt']);
                $w = strtolower(preg_replace('/[^MUPOINTLGESYR]/','',$wkt));
                $geom_show = addslashes($row['gjson']);
            }

            $smallmap = '
         <link rel="stylesheet" href="'.$protocol.'://'.URL.'/css/mapsdata.css?rev='.rev('css/mapsdata.css').'" type="text/css" />
         <script type="text/javascript" src="'.$protocol.'://'.URL.'/js/OpenLayers.js?rev='.rev('js/OpenLayers.js').'"></script>';
            $smallmap .= '
         <script type="text/javascript" src="'.$protocol.'://'.URL.'/js/maps_functions.js?rev='.rev('js/maps_functions.js').'"></script>';

            $smallmap .= '
        <script type="text/javascript" src="'.$protocol.'://'.URL.'/js/proj4js-compressed.js"></script>
        <script type="text/javascript" src="'.$protocol.'://'.URL.'/js/javascript.util.js"></script>
        <script type="text/javascript" src="'.$protocol.'://'.URL.'/js/jsts.js"></script>';
            if (file_exists('includes/local_main_javascripts.php')) {
                ob_start();
                include_once('includes/local_main_javascripts.php');
                $smallmap .= ob_get_contents();
                ob_end_clean();
            }

            $smallmap .= '
        <script type="text/javascript">

        $(function() {
                init();
                var coords = "'.$geom_show.'";
            
                coords = coords.replace(/\\\/g,\'\');

                var geojson_format = new OpenLayers.Format.GeoJSON();
                var feature = geojson_format.read(coords);
                var p = feature[0].geometry.getCentroid();
                p.transform(new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), new OpenLayers.Projection("EPSG:900913"));
                
                markerLayer.clearMarkers();
                
                var size = new OpenLayers.Size(48,48);
                var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
                var icon = new OpenLayers.Icon(\'js/img/map-marker-pink.png\',size,offset);
                markerLayer.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(p.x,p.y),icon));
                map.setCenter(new OpenLayers.LonLat(p.x, p.y), eval(1 + 1*obj.zoom));
                });
        </script>';
            

            $smallmap .= "<div style='float:right'><div id='map' class='smallmap' style='height:500px;width:700px'></div><span id='mouse-position'></span></div>";
            echo  $smallmap;
        }


        // header
        $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",PROJECTTABLE));

        $dh .= '<h2>'.t(str_header_information).'</h2><div style="padding-left:10px;padding-top:5px;line-height:2em">';

        // adatfeltöltés adatai
        $dh .= sprintf("%s: ",t(str_uploader));
        if ($ulink=fetch_user($ek['uploader_id'],'roleid','link'))
            $dh .= $ulink; 
        else 
            $dh .= $ek['uploader_name'];

        // SHINYURL global control variable
        // create readable links instead of long get urls
        if (defined('SHINYURL') and constant("SHINYURL"))
            $eurl = sprintf($protocol.'://'.URL.'/uplinf/%d/',$ek['upid']);
        else
            $eurl = sprintf($protocol.'://'.URL.'/index.php?history=upload&id=%d',$ek['upid']);


        $dh .= sprintf(' [<a href=\'%1$s\' target=\'_blank\'>%2$d</a>]',$eurl,$ek['upid']);
        $dh .= sprintf(" %s<br>",preg_replace('/:\d{2}$/','',preg_replace('/\.\d+/','',$ek['uploading_date'])));

        // History
        if (preg_match('/([0-9]+)_([0-9]+)/',$ek['obm_id'],$m)) $genid = $m[1];
        else $genid=$ek['obm_id'];

        $last_mod = data_changes($qtable_history,$sheet_table,$genid);
        $dh .= sprintf("%s: %s %s<br>",t(str_last_modify),$last_mod['time'],$last_mod['link']);
        // SHINYURL global control variable
        // create readable links instead of long get urls
        if (defined('SHINYURL') and constant("SHINYURL"))
            $eurl = sprintf($protocol.'://'.URL.'/table/%s/data/%d/history/',$sheet_table,$ek['obm_id']);
        else
            $eurl = sprintf($protocol.'://'.URL.'/index.php?table=%s&history=data&id=%d',$sheet_table,$ek['obm_id']);

        $dh .= sprintf("%s: [<a href='%s' target='_blank'>%s</a>]<br>",t(str_changes),$eurl,$last_mod['count']);

        // Metrics
        //metrics - views
        $views = metrics($ek['obm_id'],'data_views');
                
        //metrics - citations
        $citations = implode(json_decode(cite_metrics($ek['obm_id'],'citations',$sheet_table)),"<br>");
                
        //metrics - downloads
        $downloads = metrics($ek['obm_id'],'downloads');

        // metrics - validitás + evaluation
        $dh .= sprintf("<abbr title='Known citations'>%s</abbr>: %s<br>
                        <abbr title='Estimated value'>%s</abbr>: %s<br>
                        %s: %s<br>",t(str_citations),$citations,t(str_views),$views,t(str_downloads),$downloads);


        $dh .= "</div>";
        $dh .= '<h2>'.t(str_evaluation).'</h2><div style="padding-left:10px;padding-top:5px;line-height:2em">';

        // validation
        $dh .= sprintf('<h3>%1$s</h3><div style="margin-left:20px"><abbr title="mean value of project+data+uploading+user evaluates"><span style="font-size:150%2$s;font-weight:bold">%3$s</span></abbr></div>',t(str_evaluation_score),'%',validation($ek['obm_id'],$sheet_table));

        // obm_comments
        $dh .= sprintf("<h3>%s</h3><div style='max-width:700px;margin-left:20px' id='dp_%s'>%s</div>",t(str_comments),$ek['obm_id'],comments($ek['obm_id'],$sheet_table,'data'));

        // csak RST_ACC=1 jogosult felhasználók küldhetnek commentet
        $dh .= sprintf('<br><h3>%3$s</h3>
        <div style="width:600px">
        <div class=\'opinioning\'><div class=\'bubby\'><h3>'.str_thanks_for.'!</h3>'.str_opinion_post_1.'<br><br><span style=\'font-variant:small-caps\'>'.str_avoid.'…</span><br>'.str_opinion_post_2.'</div>
        <textarea class=\'expandTextarea yo\' id=\'evdt-%2$s\'></textarea></div>
        <div><b>%1$s:</b> (<a href="?history=validation&id=%2$s&blt=%4$s" target="_blank">%5$.2f%6$s</a>)
        <button class=\'button-error pure-button pm\' id=\'valm-%2$s\' rel=\'data\' title="'.str_dnlike.'"><i class=\'fa-hand-o-down fa\'></i></button> 
        <button class=\'button-secondary pure-button pm\' id=\'valz-%2$s\' rel=\'data\' title=\'ok\'>ok</button>
        <button class=\'button-success pure-button pm\' id=\'valp-%2$s\' rel=\'data\' title=\''.str_like.'\'><i class=\'fa-hand-o-up fa\'></i></button></div>',
        t(str_valuation),$ek['obm_id'],t(str_your_opinion),PROJECTTABLE,$ek['data_eval']*100,'%');

        //$dh .= sprintf ("<a href='?history=validation&id=%s&blt=%s' target='_blank'>%.2f%s</a><br><br>",$ek['id'],PROJECTTABLE,$ek['data_eval']*100,'%');
        $dh .= "</div></div>";
        echo $dh;
    } else {
        echo "<div style='margin-left:50px;font-size:150%'>This data identifier does not exist or is not available.</div>";
    }

?>
