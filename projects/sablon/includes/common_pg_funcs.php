<?php
/* ***************************************************************************  /
    
VARIABLES FOR ALL BIOMAPS DATABASES

****************************************************************************** */
#xdebug_start_trace('/tmp/xdebug');

/* ***************************************************************************  /
    
GENERAL and GLOBAL FUNCTIONS
Do not change them
Use the local_funcs.php if you need specific functions

****************************************************************************** */
/* recursive copy of directory structure
 * create_new_project
 *
 * */


# result table printout class
class createTable {
    public $def=array('tid'=>'','tclass'=>'','theader'=>'','tbody'=>'','tstyle'=>'');
    public $table;
    public $cols = 0;
    public $th=array();
    public $tr=array();
    public $cell_id = 0;
    public $row_id = 0;
    public $diff = array();
    public $format_cols = array();
    public $format_string = array();

    function def($arr) {
        foreach ($arr as $key=>$val) {
            if ($key == 'tid') $this->def['tid'] = $val;
            elseif ($key == 'tclass') $this->def['tclass'] = $val;
            elseif ($key == 'tbody') $this->def['tbody'] = $val;
            elseif ($key == 'theader') $this->def['theader'] = $val;
            elseif ($key == 'tstyle') $this->def['tstyle'] = $val;
        }
    }
    function addCells($line) {
        $tr = array();
        foreach($line as $cell) {
            # content replace function call
            array_push($tr,$cell);
        }
        return $tr;
    }
    function addRows($line) {
        array_push($this->tr,$this->addCells($line));
    }
    function addHeader($line) {
        $this->th = $this->addCells($line);
    }
    function diffRows() {
        $i=0;
        $diff = array();
        $dr = $this->tr;
        $hrp = array();
        while($row = array_shift($dr)) {
            if (count($hrp))
                $diff[$i] = array_keys(array_diff_assoc($hrp,$row));
            $hrp = $row;
            $i++;
        }
        $this->diff = $diff;
    }
    /*  format_col: hányadik oszlopokkal
     *  format_string: mit csinál
     *  kissé bonyolult...
     * */
    function tformat($func) {
        #'format_col'=>'2';'format_string'=>'<a href=\'?id=COL-1\'>COL-2</a>'
        $this->format_cols = explode(';',$func['format_col']);
        $a = array();
        $this->format_string = explode(';',$func['format_string']);
        for($i=0;$i<count($this->format_cols);$i++) {
            $b = $this->format_cols[$i];
            $a[$b]=$this->format_string[$i];
        }
        $this->format_string = $a;
    }
    function formatHeader($rows) {
        $lines = "";
        $row_id = 0;
        $line = "";
        $cell_id = 0;
        $cpan=0;
        $csprint=0;
        foreach ($rows as $cell) {
            $this->cols++;
            if ($cell == '-1') continue;
            $m=array();
            $csp = '';
            if (preg_match('/^COLSPAN=(\d+)$/',$cell,$m)) {
                $cpan = $m[1];
                $csprint=1;
                continue;
            }
            if ($csprint) {
                $csp = "colspan='$cpan'";
                $csprint--;
                $cpan--;
                $cpan--;
            }
            elseif ($cpan>0) {
                $cpan--;
                continue;
            }

            $line .= sprintf('<th %s id=\'%sh-%d-%d\'>%s</th>',$csp,$this->def['tid'],$row_id,$cell_id,$cell);
            $cell_id++;
        }
        $lines .= sprintf("<tr>%s</tr>\n",$line);
        return $lines;
    }
    function formatRow($rows) {
        $lines = "";
        $row_id = 0;
        foreach ($rows as $row) {
            $line = "";
            $cell_id = 0;
            $cpan='';
            $csprint=0;
            for ($i=0;$i<$this->cols;$i++) {
                if ($this->th[$i] == '-1') continue;
                $cell = $row[$i];
                if (isset($this->format_string[$i])) $f = $this->format_string[$i];
                else $f = 'undef';
                for($k=0;$k<$this->cols;$k++) {
                    if ($f=='undef') continue;
                    if (preg_match("/COL-$k/",$f)) {
                        $f = preg_replace("/COL-$k/",$row[$k],$f);
                        $cell = $f;
                    }
                }
                $m=array();
                $csp = '';
                if (preg_match('/^COLSPAN=(\d+)$/',$cell,$m)) {
                    $cpan = $m[1];
                    $csprint=1;
                    continue;
                }
                if ($csprint) {
                    $csp = "colspan='$cpan'";
                    $csprint--;
                    $cpan--;
                    $cpan--;
                }
                elseif ($cpan>0) {
                    $cpan--;
                    continue;
                }
                $line .= sprintf('<td %s id=\'%s-%d-%d\'>%s</td>',$csp,$this->def['tid'],$row_id,$cell_id,$cell);
                $cell_id++;
            }
            $row_id++;
            $lines .= sprintf("<tr>%s</tr>\n",$line);
        }
        return $lines;
    }
    // should be renamed to printTable!!!
    function printOut() {
        //$this->table .= sprintf("%d rows",count($this->tr));
        $this->table .= sprintf("<table id='%s' class='%s' style='%s'>\n",$this->def['tid'],$this->def['tclass'],$this->def['tstyle']);
        $this->table .= sprintf("<thead>%s</thead>",$this->formatHeader($this->th));
        $this->table .= sprintf("<tbody>%s</tbody>",$this->formatRow($this->tr));
        $this->table .= "</table><br>";
        return $this->table;
    }
    function nRows() {
        return count($this->tr);
    }
}

# WMS WFS Layer definitions
# defLayer('WMS','Dinpi Biotika',$url,$WMS_MAP,'project'=>'dinpi','l'=>'dinpi','b'=>'true','o'=>'1.0','f'=>'image/png','v'=>'true','t'=>'true')
#new OpenLayers.Layer.WMS('Dinpi Biotika','http://$url/dinpi/$auth/mapserv',
#   {map:'$WMS_MAP',layers:'dinpi',isBaseLayer:false,visibility:true,opacity:1.0,format:'image/png',transparent:true});"
class defLayer {

    public $Olayer='';
    private $opts='';
    private $def=array();
    private $S='';
    public $singleTile = false;

    function def($arr) 
    {
        $error = array();

        foreach ($arr as $key=>$val) {
            if ($key == 'S') $this->S = "$val";
            elseif ($key == 'name') $this->def[0] = "'$val'";
            elseif ($key == 'url' and $val!='') $this->def[1] = "'$val'";
            elseif ($key == 'map' and $val!='') $this->opts = "map:'$val',";
            elseif ($key == 'opts' and $val!='') $this->opts .= "$val,";
            elseif ($key == 'cname' and $val!='') $this->opts .= "cname:'$val'";
            elseif ($key == 'singleTile' and $val=='t') $this->singleTile = true;
        }
    }
    /*function rdef($arr) {
        foreach ($arr as $key=>$val) {
            if ($key == 'S') $this->S = "$val";
            elseif ($key == 'name') $this->def[0] = "'$val'";
            elseif ($key == 'url') $this->def[1] = "'$val'";
            elseif ($key == 'map') $this->opts = "$val";
            else $this->opts[$key] = "$key:'$val'";
        }
    }*/
    function printOut() {
        //$options = implode(',',$this->opts);
        $options = $this->opts;
        $definit = implode(',',$this->def);
        if (preg_match('/^\[/',$options))
            $O = "new OpenLayers.Layer.".$this->S."($definit,".$options.");";
        elseif($options!='') {
            if ($this->S == 'OSM') $url = 'null,';
            #if ($this->S == 'OSM') $url = '[\'http://a.tile.openstreetmap.org/${z}/${x}/${y}.png\',\'http://b.tile.openstreetmap.org/${z}/${x}/${y}.png\',\'http://c.tile.openstreetmap.org/${z}/${x}/${y}.png\'],';
            else $url = '';
            if ($this->singleTile)
                $O = "new OpenLayers.Layer.".$this->S."($definit,$url".'{'."$options".'},{singleTile: true, ratio: 1}'.");";
            else
                $O = "new OpenLayers.Layer.".$this->S."($definit,$url".'{'."$options".'}'.");";
        }
        else
            $O = "new OpenLayers.Layer.".$this->S."($definit);";
        $this->Olayer = $O;

        return $this->Olayer;
    }

}
class clearLayer extends defLayer {
    public $opts = array();
}

## normal functions

function copyr($source, $dest, $make_symlinks = 0){
    $err = 0;
    if(is_dir($source)) {
        $dir_handle=opendir($source);
        while ($file=readdir($dir_handle)){
            if ($file!="." && $file!=".."){
                if (is_dir($source."/".$file)){
                    if (!mkdir($dest."/".$file)) $err++;
                    copyr($source."/".$file, $dest."/".$file, $make_symlinks);
                } else {
                    if ($make_symlinks) {
                        if (!symlink($source."/".$file, $dest."/".$file)) $err++;
                    } else {
                        if (!copy($source."/".$file, $dest."/".$file)) $err++;
                    }
                }
            }
        }
        closedir($dir_handle);
    } else {
        if ($make_symlinks) {
            if (!symlink($source, $dest)) $err++;
        } else {
            if (!copy($source, $dest)) $err++;
        }
    }
    return $err;
}
/* results_builder
 * interface
 * NOT USED !!!
 * */
function lang_label($lab) {
    # return the constans value if its exists
    # specifically used for set the language specific "species name" label
    if (defined($lab)) $label = constant($lab);
    else $label = $lab;

    return $label;
}

// user info
// return: http link with the user's name
function fetch_user($id,$type='userid',$return='link') {

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    global $BID; 
    if ($id=='') return false;

    // possible define other types, e.g., fetch by name...
    if ($type=='userid') {
        $id = preg_replace("/[^0-9-]/","", $id);
        $cmd = sprintf("SELECT username,\"user\" FROM users WHERE id=%s",quote($id));
        $res = pg_query($BID,$cmd);
    } elseif ($type=='roleid') {
        $id = preg_replace("/[^0-9-]/","", $id);
        $cmd = sprintf("SELECT username,\"user\" FROM users LEFT JOIN project_roles ON (id=user_id AND project_table='%s') WHERE role_id=%s",PROJECTTABLE,quote($id));
        $res = pg_query($BID,$cmd);    
    }

    if ($return=='link') {
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            // SHINYURL global control variable
            // create readable links instead of long get urls
            if (defined('SHINYURL') and constant("SHINYURL"))
                $eurl = sprintf("$protocol://".URL.'/profile/%s/',$row['user']);
            else
                $eurl = sprintf("$protocol://".URL.'/index.php?profile=%s',$row['user']);

            $link = "<a href=$eurl target='_blank'>{$row['username']}</a>";
            return $link;
        }
    }
    return false;
}
// flexible functions to get user's data
class userdata {
    private $type;
    private $id;
    public $hash;

    public function __construct ($id,$type) {
        $this->idtype = $type;
        $this->id = $id;
        if ($type == 'hash')
            $this->hash = $id;
        elseif ($type == 'roleid') {
            $this->hash = $this->hash_from_roleid($id);
        }
        elseif ($type == 'userid') {
            $this->hash = $this->hash_from_userid($id);
        }
    }
    function get_username() {
        global $BID;
        $cmd = sprintf("SELECT username FROM \"users\" WHERE \"user\"=%s",quote($this->hash,true));
        $result = pg_query($BID,$cmd);
        if (!pg_num_rows($result))
            return false;
        $row = pg_fetch_assoc($result);
        return $row['username'];
    }
    function get_roleid() {
        global $BID;
        $cmd = sprintf("SELECT role_id FROM project_roles LEFT JOIN \"users\" ON (id=user_id AND project_table='%s') WHERE \"user\"=%s",PROJECTTABLE,quote($this->hash,true));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['role_id'];
        }
        return -1;
    }
    function get_userid() {
        global $BID;
        $cmd = sprintf("SELECT id FROM \"users\" WHERE \"user\"=%s",quote($this->hash,true));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['id'];
        }
        return -1;
    }

    // get user_id from roleid
    function hash_from_roleid($roleid) {
        global $BID;
        $cmd = sprintf("SELECT \"user\" FROM \"users\" LEFT JOIN project_roles ON (id=user_id AND project_table='%s') WHERE role_id=%s",PROJECTTABLE,quote($roleid));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['user'];
        }
        return -1;
    }
    // get role_id from user_id
    function hash_from_userid($userid) {
        global $BID;
        $cmd = sprintf("SELECT \"user\" FROM \"users\" LEFT JOIN project_roles ON (id=user_id AND project_table='%s') WHERE id=%s",PROJECTTABLE,quote($userid));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['user'];
        }
        return -1;
    }
}

// get users' profile data
// called in profile.php
// some as pds's profile()
function get_profile_data($table,$userhash) {
    global $BID,$ID;
    # Ha a userid tömb: (id1,id2,...)...
    # akkor nem ad vissza semmit
    # ha az egyes lekérdezések atomi funkciók volnának, akkor a profile, össze tudná rakni, de lényegében nincs értelme
    
    $userdata = new userdata($userhash,'hash');
    //$username = $userdata->get_username();
    $role_id = $userdata->get_roleid();
    $userid = $userdata->get_userid();

    if (!$userid) return false;

    $userdata = obm_cache('get',"get_profile_data_$userid");
    if ($userdata!==FALSE) {
        return $userdata;
    }

    // feltöltések száma
    $cmd = sprintf("SELECT id,project_table FROM system.uploadings WHERE uploader_id=%d AND project=%s",$role_id,quote(PROJECTTABLE));
    $res = pg_query($ID,$cmd);
    $uplcount = pg_num_rows($res);
    $upid = array();
    $upid_table = array();
    $upld = array();
    $modcount = '';
    while ($row=pg_fetch_assoc($res)) {
        if (!isset($upid_table[$row['project_table']]))
            $upid_table[$row['project_table']] = array();
        $upid_table[$row['project_table']][] = $row['id'];
        array_push($upid,$row['id']);
    }

    // feltöltött rekordok és módosítások száma
    if ($table) {
        foreach ($upid_table as $project_table=>$id_list) {
            // feltöltött rekordok száma
            // nagyon nagy hosszú tud lenni!!!
            $cmd = sprintf("SELECT count(*) AS c FROM \"%s\" WHERE obm_uploading_id IN (%s)",$project_table,implode(',',$id_list));
            $res2 = pg_query($ID,$cmd);
            if (pg_num_rows($res2)) {
                $row2=pg_fetch_assoc($res2);
                $upld[] = $row2['c'];
            }
        }
    }
    
    $upld = array_sum($upld);
    if ($table) {
        // módosított sorok száma
        $cmd = sprintf("SELECT count(*) AS c FROM \"%s\" WHERE obm_modifier_id=%s",$table,quote($userid));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row=pg_fetch_assoc($res);
            $modcount = $row['c'];
        }
    }

    $st = "";
    if ($table) {
        $st = "LEFT JOIN project_users pup ON (pup.user_id=users.id AND pup.project_table='$table')";
    }
    
    // groups eliminated!!!
    $cmd = sprintf("SELECT %d AS uplcount,'%s' AS upid,%d AS upld,%d AS modcount,pu.user_status,status,
        institute,orcid,address,username,array_to_string(givenname,' ') as givenname,array_to_string(familyname,' ') as familyname,
        array_to_string(\"references\",',') as \"references\",\"user\",email,validation,pu.project_table,pu.receive_mails,pu.visible_mail
        
        FROM public.users 
        LEFT JOIN project_users pu ON (pu.user_id=users.id )
        $st
        WHERE public.users.id=%s 
        GROUP BY users.id,pu.project_table,pu.user_status,pu.receive_mails,pu.visible_mail
        ORDER BY pu.project_table",$uplcount,implode(',',$upid),$upld,$modcount,quote($userid));

    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r1 = pg_fetch_all($res);
        $c = array_combine(array_keys($r1[0]),array_values($r1[0]));

        obm_cache('set',"get_profile_data_$userid",json_encode($c),180);
        return json_encode($c);
    }
    return false;
}

// login window
function login_box () {
    $out = "<form method='post' class='loginform'>";
    if (defined('str_local_login_message')) {
        $out .= "<h3>".str_local_login_message."</h3>";
    }
    $out .= "<div style='font-size:200%'>".t(str_signin)."</div>
            <br>
            <span style='font-weight:bold'>".t(str_email).":</span>
            <div class='input-group' style='margin-bottom:1em'>
                <span class='input-group-addon'><i class='fa fa-envelope-o fa-fw fa-lg'></i></span>
                <input class='form-control' type='text' placeholder='".t(str_email)."' name='loginname' style='font-size:115%'>
            </div>
            <span style='font-weight:bold;'>".t(str_password).":</span>
            <div class='input-group'>
                <span class='input-group-addon'><i class='fa fa-key fa-fw fa-lg'></i></span>
                <input class='form-control' type='password' name='loginpass' style='font-size:115%'>
            </div>
            <br>
            <br>
            <button type='submit' class='button-success button-xlarge pure-button' name='loginenter'><i class='fa fa-sign-in'></i> ".t(str_enter)."</button><br><br>
            <!--<a href='?login=orcid' class='button-small button-href pure-button'><img id='orcid-id-logo' src='http://orcid.org/sites/default/files/images/orcid_16x16.png' style='vertical-align:middle' width='16' height='16' alt='ORCID logo' /> ".t(str_login_orcid)."</a>-->
            <br>
            <div style='text-align:right;font-size:120%'><a href='?registration'>".t(str_registration)."</a></div>
            <div style='text-align:right;font-size:120%'><a href='?lostpw'>".t(str_lostpasswd)."</a></div>
        </form>";
    return $out;
}

//data changes
function data_changes($hist_table,$data_table,$data_row_id) {
    global $ID;
    $hm = '';
    $ht = '';
    $hc = 0;
    $ulink = '';
    $cmd = sprintf('SELECT to_char(hist_time,\'YYYY-MM-DD HH24:MI\') as hist_time,obm_modifier_id 
        FROM %1$s LEFT JOIN %2$s ON(row_id=obm_id) 
        WHERE row_id=%3$s AND %1$s.data_table=\'%2$s\' ORDER BY hist_time DESC',$hist_table,$data_table,quote($data_row_id));
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res); 
        $hm = $row['obm_modifier_id'];
        $ht = $row['hist_time'];
        $hc = pg_num_rows($res);
    }
    //fetch modifier user's profile link
    $ulink = fetch_user($hm,'userid','link');
    return array('time'=>$ht,'link'=>$ulink,'count'=>$hc);
}

/* shell function
 * called in results_builder class
 * */
function gpsbabel($d) {
    #$res = system("echo '$csv' | R --vanilla --slave \"a<-read.csv(pipe('cat /dev/stdin'),sep='#'); summary(a)\"");
    #

    $gpx_file = sprintf("%s%s_download.gpx",OB_TMP,session_id());
    $tmp_file = sprintf("%s%s_download_gpx.csv",OB_TMP,session_id());
    $handle = fopen($tmp_file, "w");
    $fwrite = fwrite($handle,$d);
    fclose($handle);

    if (!$fwrite)
        log_action("No data written to create GPX, No space left on device?",__FILE__,__LINE__);

    //$res = system("echo '$d' | gpsbabel -t -i unicsv -f - -x transform,wpt=trk,del -o gpx -F $f 2>&1",$r);
    system("cat $tmp_file | gpsbabel -t -i unicsv -f - -x transform,wpt=trk,del -o gpx -F $gpx_file",$ret);
    if ($ret==127) {
        log_action("gpsbabel not found.",__FILE__,__LINE__);
    } else {
        unlink($tmp_file);

        $handle = fopen($gpx_file, "rb");
        $contents = fread($handle, filesize($gpx_file));
        fclose($handle);
        unlink($gpx_file);
        return $contents;
    }
    return false;
}
/* return with a random hash string
 * */
function genhash($len=16) {
    // return with default(16) byte long random string
    $hash='';
    $base='ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz';
    $max=strlen($base)-1;
    mt_srand((double)microtime()*1000000);
    while (strlen($hash)<$len+1) $hash.=$base{mt_rand(0,$max)};
    return $hash;
}

/* create a php file with
 * define('',''); structure
 * create_new_project.php
 * */
function create_define_file($target,$defs) {
    if(!$fp=fopen("$target",'w'))
        return false;
    fwrite($fp, sprintf("<?php\n"));
    foreach ($defs as $key=>$var) {
        if (!preg_match('/^sprintf/',$var)) $var = "'$var'";
        fwrite($fp, sprintf("define('%s',%s);\n",preg_replace('/[^a-z0-9_]/i','_',$key),$var));
    }
    fwrite($fp, sprintf("?%s\n",">")); // just for vi syntax highlight
    fclose($fp);
    return true;
}
/* Assemble an SQL query string for mapserver and results_query
 * layer = optional | mapserver's layer name
 * query_type  = base/query or proxy as alias of both
 * bbox  = optional | boundary box for query: four numeric coordinate value of corners
 * */
function assemble_query($layer='',$query_type='',$bbox='',$cname='') {
    global $BID,$ID; 

    $grid_filter_query = '';
    $modules = new modules();

    if ($grid_mki = $modules->is_enabled('grid_view')) {
        $grid_layer = $modules->_include('grid_view','get_grid_layer');
        #{"kef_5":"dinpi_grid","utm_2.5":"dinpi_grid","utm_10":"dinpi_grid","utm_100":"dinpi_grid","original":"dinpi_points"}
        
        if (isset($_SESSION['display_grid_for_map'])) {
            
            // For grid coordinates disable original layer
            foreach ($grid_layer as $gc=>$gl) {
                
                if ($_SESSION['display_grid_for_map'] == $gc and $layer!='' and $layer!=$gl) {
                    //log_action("DROP: {$_SESSION['display_grid_for_map']} == $gc & $layer != $gl",__FILE__,__LINE__);
                    return;
                }
            }
        } else {
            $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
            foreach ($grid_layer as $gc=>$gl) {

                if ($grid_geom_col == $gc and $layer!='' and $layer!=$gl) {
                    //log_action("DROP: $grid_geom_col == $gc & $layer != $gl",__FILE__,__LINE__);
                    return;
                }
            }
        }
        //$grid_filter_query = $dmodule->grid_view('filter_query',$modules[$grid_mki]['p']);
    }

    // proxy lekérdezés hosszú várakozás után.... nincsenek session változók!!!
    if (!isset($_SESSION['current_query_table']))
       $_SESSION['current_query_table'] = PROJECTTABLE; 

    $st_col = st_col($_SESSION['current_query_table'],'array');

    // query query
    $layer_type = "";
    $layer_name = "";
    $query = "";
    $rst = " IS NULL";

    if ($layer != '') $layer_name = "=".quote($layer);
    else $layer_name = ' IS NOT NULL';

    if ($cname != '') {
        $layer_cname = "=".quote($cname);
    }
    else $layer_cname = ' IS NOT NULL';

    if ($query_type == 'proxy') $layer_type = "=ANY('{base,query}')";
    elseif ($query_type == 'query') $layer_type = "='query'";
    else $layer_type = " LIKE '%'";

    /* Ezen a szinten a jogosultság kezelés  0-1-2-3 szintek; jelentése: non-login, login, projekt-member, admin
     * Az oauth bevezetése óta az 1-2 szint nem különbözik! Csak a 2-es létezik: 0,2,3
     * */
    $prst = prst();
    if ($query_type == 'proxy')
        $cmd = sprintf("SELECT layer_query,mapserv_layer_name AS layer_name,layer_type,geometry_type as geom_type,rst 
                    FROM project_queries pq 
                    LEFT JOIN project_mapserver_layers ml ON (pq.project_table=ml.project_table)
                    WHERE layer_query!='' AND pq.project_table='%s' AND layer_type%s AND mapserv_layer_name%s AND layer_cname%s AND rst<=%d AND enabled=TRUE AND main_table=%s %s
                    ORDER BY rst DESC",
                        PROJECTTABLE,$layer_type,$layer_name,$layer_cname,$prst,quote($_SESSION['current_query_table']),$grid_filter_query);
    else
        $cmd = sprintf("SELECT layer_query,'' AS layer_name,layer_type,'' as geom_type,rst 
                    FROM project_queries 
                    WHERE layer_query!='' AND project_table='%s' AND layer_type%s AND rst<=%d AND enabled=TRUE AND main_table=%s %s 
                    ORDER BY rst DESC",
                        PROJECTTABLE,$layer_type,$prst,quote($_SESSION['current_query_table']),$grid_filter_query);


    $res = pg_query($BID,$cmd);
    //log_action($cmd,__FILE__,__LINE__);

    // we expect only one row!
    //while($row = pg_fetch_assoc($res)) {
    if (pg_num_rows($res)) {

        /*  Module actions over subqueries
            module include as query_builder.php */

        
        /* ok, it is already allowed!
         * if(pg_num_rows($res)>1) {
            log_action('common_pg_funcs - WARNING: more than one assembled query!!!');
            log_action('common_pg_funcs - SQL: '. $cmd);
            //point - line - polygon
        }*/
        $qq = array();
        $rst_counter = 0;
        $id_alias = PROJECTTABLE; # default id_alias for strange cases

        // 
        if (!isset($st_col['GEOM_C'])) $geom_col = '';
        else {$geom_col = $st_col['GEOM_C'];

            $geom_noalias_col = $st_col['GEOM_C'];
        }

        if (!isset($st_col['SPECIES_C'])) $species_col = '';
        else $species_col = $st_col['SPECIES_C'];

        if (!isset($st_col['CITE_C'])) $observer_cols = '';
        else $observer_cols = $st_col['CITE_C'];
        
        /* If not necessary do not query it, bacause proxy sends lots of queriees, every data column send a query....
        $rst = 0;
        if (rst('acc')) {
            $rst = 1;
        } */

        $box = preg_split('/,/',$bbox);
        $ebox = '';
        if (count($box)==4) {
            $bcmd = sprintf("SELECT concat_ws(',',
                    st_X(st_transform(st_geomfromtext('POINT($box[0] $box[1])',900913),4326)), st_Y(st_transform(st_geomfromtext('POINT($box[0] $box[1])',900913),4326)),
                    st_X(st_transform(st_geomfromtext('POINT($box[2] $box[3])',900913),4326)), st_Y(st_transform(st_geomfromtext('POINT($box[2] $box[3])',900913),4326))) AS box");
            $bres = pg_query($BID,$bcmd);
            $brow = pg_fetch_assoc($bres);
            $ebox = $brow['box'];
        }

        // snesitive data handling
        // query only for the allowed users or groups
        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        while ($row = pg_fetch_assoc($res)) {
            //csak a legmagassabb rst értékű rétegeket olvassuk be
            //ez azért van így mert lehet, hogy több azonos színtű rétegünk van és mindegyiket végrehajtjuk egyszerre. Pl.: point, line, polygon
            //sql szinten nem tudom, hogyan lehetne ezt kezelni
            if ($rst_counter==0) {
                $rst_max = $row['rst'];
                $rst_counter++;
            }
            if ($row['rst'] < $rst_max) {
                break;
            }

            $_SESSION['ignore_joined_tables'] = 0;
            $id_col = "obm_id";

            $qtable = $_SESSION['current_query_table'];
            $qtable_alias = $qtable;

            if (preg_match('/%F%(.+)%F%/',$row['layer_query'],$m)) {
                        
                $FROM = sprintf('FROM %s',$m[1]);

                if (preg_match('/([a-z0-9_]+) (\w+)/i',$m[1],$mt)) {
                    $qtable = $mt[1];
                    $qtable_alias = $mt[2];
                    if (preg_match("/([a-z0-9_]+)\.obm_geometry/",$row['layer_query'],$mg)) {
                        $geom_alias = $mg[1];
                        $geom_col = $geom_alias.".obm_geometry";
                    }
                    /*if (preg_match("/([a-z_]+)\.%transform_geometry%/",$row['layer_query'],$mg)) {
                        $geom_alias = $mg[1];
                        $geom_col = $geom_alias.".obm_geometry";
                    }*/
                    if (preg_match("/([a-z0-9_]+)\.obm_id/",$row['layer_query'],$mg)) {
                        $id_alias = $mg[1];
                        $id_col = $id_alias.".obm_id";
                    }                    
                }
                if (preg_match_all('/%J%(.+)%J%/',$row['layer_query'],$mj)) {
                    foreach($mj[1] as $mje) {
                        if (isset($_SESSION['ignoreJoins']) and $_SESSION['ignoreJoins'])
                            $_SESSION['ignore_joined_tables']++;
                        $FROM .= " ".$mje;
                    }
                }
            }


            //geom column name should be passed here to module!!!
            /*$module_name = array_values(array_intersect(array('snap_to_grid','transform_geometries','transform_geometry'), array_column($modules, 'module_name')));
            $mki = (count($module_name)) ? array_search($module_name[0], array_column($modules, 'module_name')) : false;
            if ($mki!==false) {
                require_once(getenv('OB_LIB_DIR').'default_modules.php');
                $dmodule = new defModules();
                $geom_col = $dmodule->snap_to_grid('geom_column',$modules[$mki]['p'],$geom_col);
            }*/

            $orderby = array();
            $limit = '';
            //if ($query_type == 'proxy') 
            //    $limit = 16384;
            $query = $row['layer_query'];
            $layer_type = $row['layer_type'];
            $geom_type = preg_split('/,/',$row['geom_type']);
    
            /* Example usage of replace-variables in the query string:
             *
                CASE WHEN '%admin%'='admin' THEN 1 ELSE 0
                END AS super,

                CASE WHEN %uid%=ANY(owners) THEN 1 ELSE 0
                END AS mydata,

                CASE WHEN %grp%=ANY(groups) THEN 1 ELSE 0
                END AS groupdata

             * **********************************/
            // default patterns
            $uid = 0;
            $grp = 0;
            $admin = 'user';
            $morefilter = '';

            // access control settings
            // applied in both: base and query layers
            if (isset($_SESSION['Tid'])) $uid = $_SESSION['Tid'];
            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') $grp = $_SESSION['Tgroups'];
            $prst_class = array('public','login','project','admin')[$prst];
            
            /* Repleace variables for controlling MAPSERVER map styling 
             * %rules% String: public,login,project,admin
             * %grp% Comma separated Numeric List of Logined user's group or 0
             * %uid% Numeric logined user's ID or 0
             *
             * */

            //a bit different: access class independently from the project rules: factor 0,1,2,3/public,login,project,admin
            $query = preg_replace('/%rules%/',"$prst_class",$query);
            //login groups
            $query = preg_replace('/%grp%/',"$grp",$query);
            //user's id
            $query = preg_replace('/%uid%/',"$uid",$query);

            //remove join_table marks
            $query = preg_replace('/%F%/','',$query);
            $query = preg_replace('/%J%/','',$query);

            //geometry_column
            //$query = preg_replace('/(\w\.)?%transform_geometry%/',$geom_col,$query); # csak egy karakteres aliasokkal működik így!!!!

            //grid geometry column
            //obm_geometry dupla megjelentését le kell védeni!
            
            if ($grid_mki) {
                if (isset($_SESSION['display_grid_for_map'])) {
                    $grid_geom_col = $_SESSION['display_grid_for_map'];
                    $geom_col = 'qgrids.'.'"'.$grid_geom_col.'"';
                    $query = preg_replace('/%grid_geometry%/','qgrids.'.'"'.$grid_geom_col.'"',$query);
                } else {
                    $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                    $geom_col = 'qgrids.'.'"'.$grid_geom_col.'"';
                    $query = preg_replace('/%grid_geometry%/','qgrids.'.'"'.$grid_geom_col.'"',$query);
                }
                $geom_noalias_col = $grid_geom_col;

                //$geom_type = array('POLYGON','MULTIPOLYGON','POINT','LINESRTING','MULTILINESTRING');
            } else {
                $query = preg_replace('/%grid_geometry%/',$id_alias.".".'"'.$geom_col.'"',$query);
            }

            if (isset($_SESSION['query_build_string'])) {
                //preserve $ quotes
                $q = preg_replace('/\$/','\\\$',$_SESSION['query_build_string']);
                $q = preg_replace('/qgrids\.[\w.]+/',$geom_col,$q);
                if (!is_array($geom_type))
                    $q = preg_replace('/@@geom_type@@/',"$geom_type",$q);
                else
                    $q = preg_replace('/@@geom_type@@/',"$geom_type[0]",$q); //passz, nem tudom, hogy így jó lesz-e???
                
                // query set parameters
                if (trim($q)!='')
                    $query = preg_replace('/%qstr%/',"AND $q",$query);
                else
                    $query = preg_replace('/%qstr%/',"",$query);

                $query = preg_replace('/%selected%/',",1 AS selected",$query);

            } else {
                $query = preg_replace('/%selected%/',",0 AS selected",$query);
                $query = preg_replace('/%qstr%/',"",$query);
            }

            // bbox of openlayers request square
            // obm_geometry && ST_MakeEnvelope (%bbox%,4326)
            //if ($ebox!='' and $geom_col != '') {
            //    //$query = preg_replace('/%envelope%/',"AND $geom_col && ST_MakeEnvelope({$ebox},4326)",$query);
            //    $query = preg_replace('/%envelope%/',"AND ST_Intersects($geom_col,ST_MakeEnvelope({$ebox},4326))",$query);
            //} else {
                //text query no bbox
            //    $query = preg_replace('/%envelope%/',"",$query);
            //}
            
            //
            if ($query_type == 'proxy' and $geom_type != '' and $geom_col != '') {
                if (is_array($geom_type)) {
                    $geom_type = "'".implode("','",$geom_type)."'";
                    $query = preg_replace('/%geometry_type%/',"AND GeometryType($geom_col) IN ($geom_type)",$query);
                }
                else
                    $query = preg_replace('/%geometry_type%/',"AND GeometryType($geom_col)='$geom_type'",$query);
            }
            else
                $query = preg_replace('/%geometry_type%/',"",$query);

            // optional taxon join
            if (isset($_SESSION['taxon_join_filter']) and $_SESSION['taxon_join_filter'] and $species_col != '')
                $query = preg_replace('/%taxon_join%/',sprintf("LEFT JOIN %s_taxon ON (replace(%s,' ','')=meta)",PROJECTTABLE,$species_col),$query);
            else
                $query = preg_replace('/%taxon_join%/','',$query);
            // optional search join
            if (isset($_SESSION['search_join_filter']) and $_SESSION['search_join_filter']) {
                $query = preg_replace('/%search_join%/',sprintf('LEFT JOIN %1$s_search_connect oc ON (oc.data_table=\'%2$s\' AND oc.row_id = %3$s)',PROJECTTABLE,$_SESSION['current_query_table'],$id_col),$query);
            }
            else
                $query = preg_replace('/%search_join%/','',$query);

            // optional uploading join
            if (isset($_SESSION['uploading_join_filter']) and $_SESSION['uploading_join_filter'])
                $query = preg_replace('/%uploading_join%/',"LEFT JOIN system.uploadings u ON (u.id=".$qtable_alias.".obm_uploading_id)",$query);
            else
                $query = preg_replace('/%uploading_join%/','',$query);
            

            // _rules JOIN and sensitive check if not super admin!!
            if ($st_col['RESTRICT_C']) {
                /*SENSITIVE DATA: 
                 * DO NO QUERY sensitive data - only for the owners of them
                 * It is different than the acc check
                 */

                // rules join
                $query = preg_replace('/%rules_join%/',sprintf('LEFT JOIN %1$s_rules r ON (r.data_table=\'%2$s\' AND %3$s=r.row_id)',PROJECTTABLE,$_SESSION['current_query_table'],$id_col),$query);
                if (!grst(PROJECTTABLE,4)) {
                    //No group access checking on SENSITIVE DATA, we DO NO QUERY sensitive data - only for the owners of these
                    $query .= " AND ( (sensitivity::varchar IN ('3','only-owner') AND ARRAY[$grp] && read) ";
                    
                    $sens_levels = "'0','1','2','public','no-geom','restricted'";
                    if (isset($_SESSION['map_query']) and $_SESSION['map_query']==true ) {
                        $query .= " OR (sensitivity::varchar IN ('1','no-geom') AND ARRAY[$grp] && read) ";
                        $_SESSION['map_query'] = false;
                        $sens_levels = "'0','2','public','restricted'";
                    }

                    $query .= " OR sensitivity IS NULL OR sensitivity::varchar IN ($sens_levels) ) ";
                }
            } else {
                $query = preg_replace('/%rules_join%/','',$query);
            }

            // grid join
            $query = preg_replace("/%grid_join%/",'LEFT JOIN '.$_SESSION['current_query_table'].'_qgrids qgrids ON ('.$id_col.'=qgrids.row_id)',$query);

             
            //log_action($query,__FILE__,__LINE__);

            /*  subfiltering 
                applied in query layers
                point - line - polygon */
            if (isset($_SESSION['morefilter']) and $_SESSION['morefilter']!='clear') {
                $res4 = pg_query($ID,sprintf("SELECT EXISTS (
                   SELECT 1
                   FROM   information_schema.tables 
                   WHERE  table_schema = 'temporary_tables'
                   AND    table_name = 'temp_filter_%s_%s');",PROJECTTABLE,session_id()));
                $row = pg_fetch_assoc($res4);
                if ($row['exists']=='t') {

                    //$resm = pg_query($ID,sprintf("SELECT rowid FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
                    //if (pg_num_rows($resm)) {
                    if (isset($_SESSION['morefilter_query']))
                        $query = preg_replace('/%morefilter%/',"{$_SESSION['morefilter_query']}",$query);
                    else
                        $query = preg_replace('/%morefilter%/',"",$query);

                    //} else {
                    //    $query = preg_replace('/%morefilter%/',"",$query);
                    //}
                    //} else {
                } else {
                    $query = preg_replace('/%morefilter%/',"",$query);
                }
            } else {
                    $query = preg_replace('/%morefilter%/',"",$query);
            }
            
            // order by module call
            //          not implemented
            // limit module call
            //          not implemented
            $orderby = implode(' ',$orderby);
            if (trim($orderby != '')) {
                if (preg_match('/order by/i',$query)) {
                    $query = preg_replace('/ORDER BY(\s)/i',"$orderby",$query);
                } else {
                    $query .= " ORDER BY $orderby";
                }
            }
            if ($limit != '') {
                $query .= " LIMIT $limit";
            }

            if ($ebox!='' and $geom_noalias_col != '') {
                if (preg_match('/%envelope%/',$query)) {
                    $query = preg_replace('/%envelope%/',"",$query);
                    $query = "WITH \"$geom_noalias_col\" (geom) AS (SELECT ST_MakeEnvelope($ebox,4326)) ".$query;
                }
            }

            //text query no bbox
            $query = preg_replace('/%envelope%/',"",$query);
 
            $query = preg_replace('/WHERE\s+AND/i','WHERE',$query);
            $query = preg_replace('/WHERE\s*$/i','',$query);
            //log_action($query,__FILE__,__LINE__);

            $qq[] = $query;
        }
        return $qq;
    
    } else {

        if (pg_last_error($BID)) 
            log_action(pg_last_error($BID),__FILE__,__LINE__);

        //log_action('WARNING: No query for layer in project_queries: '.$layer,__FILE__,__LINE__);
        //log_action('SQL: '. $cmd,__FILE__,__LINE__);
        return;
    }
}
/*
 *
 * */
function update_temp_table_index ($name,$prefix='temp',$interconnect=false) {
    global $ID,$GID;

    //update temp_index
    $res = pg_query($ID,sprintf("SELECT 1 FROM system.temp_index WHERE table_name='%s_%s'",$prefix,$name));
    if (!pg_num_rows($res)) {
        if ($interconnect)
            $interconnect = 't';
        else
            $interconnect = 'f';
        pg_query($ID,sprintf("INSERT INTO system.temp_index (table_name,interconnect) VALUES ('%s_%s','%s')",$prefix,$name,$interconnect));
    } else {
        pg_query($ID,sprintf("UPDATE system.temp_index SET datum=now() WHERE table_name='%s_%s'",$prefix,$name));
    }
    // clear unused (older than 3 days) temp tables - in general - 
    // the temp tables dropped by automatically using drop_temporay_table trigger function:
    // BEGIN
    //    IF (TG_OP = 'DELETE') THEN
    //        execute format('DROP TABLE IF EXISTS temporary_tables.%I',OLD.table_name);
    //        RETURN OLD;
    //    END IF; 
    // END
    pg_query($GID,"DELETE FROM system.temp_index WHERE datum < NOW() - INTERVAL '3 days' AND table_name LIKE '$prefix%_".PROJECTTABLE."_%'");
}
/* Get named XML node
 *
 * */
function &getXMLnode($object, $param) {
        foreach($object as $key => $value) {
            if(isset($object->$key->$param)) {
                return $object->$key->$param;
            }
            if(is_object($object->$key)&&!empty($object->$key)) {
                $new_obj = $object->$key;
                $ret = getXMLnode($new_obj, $param);   
            }
        }
        if($ret) return (string) $ret;
        return false;
}

function LogIn($user,$password) {
    global $ID;

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    //debug (gen_password_hash($password) );
        
    if (defined('OAUTHURL'))
        $url = OAUTHURL.'/oauth/token.php';
    else
        $url = $protocol.'://'.URL.'/oauth/token.php';

    $data = array('username'=>$user, 'password'=>$password, 'client_id'=>'web', 'client_secret'=>'web', 'scope'=>'webprofile','grant_type'=>'password');

    $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
    );

    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    
    if ($result === FALSE) { 
            /* Handle error */
            log_action("Logging in failed with {$user}",__FILE__,__LINE__); 
            return false;
    } else {
        
            if (oauth($result)) {

                if (isset($_SESSION['morefilter_query'])) unset($_SESSION['morefilter_query']);
                if (isset($_SESSION['morefilter'])) unset($_SESSION['morefilter']);
                if (isset($_SESSION['filter_type'])) unset($_SESSION['filter_type']);
                if (isset($_SESSION['query_build_string'])) unset($_SESSION['query_build_string']);
                if (isset($_SESSION['upload'])) unset($_SESSION['upload']);
                if (isset($_SESSION['theader'])) unset($_SESSION['theader']);
                
                pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));
                pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
                pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
                pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_table_%s_%s",PROJECTTABLE,session_id()));
                pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_stable_%s_%s",PROJECTTABLE,session_id()));
                pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_list_%s_%s",PROJECTTABLE,session_id()));
                return true;
            } 
            log_action('Oauth login failed - missing user from project_users table?',__FILE__,__LINE__);
    }
    return false;
}
/* It does a compatibility check and can be used for real password expiry check...
 *
 * */
function check_expired_password($user,$password,$uid=false) {
    global $BID;
    require_once(getenv('OB_LIB_DIR').'languages.php');

    $cmd = sprintf('SELECT "id","password","pwstate" FROM "users" WHERE LOWER("email")=%s',quote(strtolower($user)));
    $res = pg_query($BID,$cmd);
    if (!pg_num_rows($res)) {
        return str_no_such_user.'!';
    } else {
        $row = pg_fetch_assoc($res);
        if ($row['pwstate']=='t') {
            // verify is not necessery ...  double check - error messages       
            if (password_verify($password,$row['password'])) {
                if ($uid) 
                    return $row['id'];
                return true;
            } else {
                return str_login_error;
            }
        } else {
            return str_password_expired;
        }
    }
    return false;
}
/* Oauth  
 * result - Ouath json response array
 * */
function oauth($result) {

    $j = json_decode($result);
    //debug(headers_list(),__FILE__,__LINE__);
    //debug($j->{'access_token'});
    
    // set coockies
    $expiry = time() + 3559;
    $data = (object) array( "access_token" => $j->{'access_token'} );
    $cookieData = (object) array( "data" => $data, "expiry" => $expiry );
    setcookie( "access_token", json_encode( $cookieData ), $expiry ,"/");
            
    $expiry = time() -1 + 60*60*24*14;
    $data = (object) array( "refresh_token" => $j->{'refresh_token'} );
    $cookieData = (object) array( "data" => $data, "expiry" => $expiry );
    setcookie( "refresh_token", json_encode( $cookieData ), $expiry ,"/");
    
    if (oauth_login($j->{'access_token'}))
        return true;

    return false;

}
function oauth_login ($access_token) {
    global $BID;
    $cmd = sprintf("SELECT users.user,\"status\",email,id,username,terms_agree 
        FROM users 
            LEFT JOIN oauth_access_tokens oa ON (LOWER(oa.user_id)=LOWER(email)) 
            LEFT JOIN project_users pu ON (pu.user_id=id)
            WHERE access_token=%s AND pu.project_table='%s' GROUP BY users.id",quote($access_token),PROJECTTABLE);

    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);

        // parked users!!
        if ($row['status'] == '0') {
            return false;
        }

        // set LogIn SESSION variables
        $_SESSION['Tid'] = $row['id'];
        // How it can be possible?
        // Sometimes there are more than one role_id for the same user_id, I think it is a bug, but I can handle it by LIMIT the subquery results

        $role_cmd = sprintf('SELECT role_id,container[1] as cont,user_id 
                    FROM project_roles 
                    WHERE project_table=\'%1$s\' AND 
                        (SELECT role_id FROM project_roles WHERE user_id=%2$d AND project_table=\'%1$s\' LIMIT 1)=ANY(container)',
                    PROJECTTABLE,$row['id']);
        $rres = pg_query($BID,$role_cmd);
        if (pg_last_error($BID)) {
            log_action(pg_last_error($BID),__FILE__,__LINE__);
            log_action($role_cmd);
        }
        $roles = array();
        $cont = '';
        //role_id  cnt
        // 12       12
        //205        5
        //201        1
        //202      202
        while ($grow = pg_fetch_assoc($rres)) {
            $roles[] = $grow['role_id'];
            if ($grow['role_id'] == $grow['cont'] and $grow['user_id']==$_SESSION['Tid'])
                $cont = $grow['role_id'];
        }
        //these are roles not groups - like in PostgreSQL
        $login_groups = implode(',',array_filter(array_unique($roles)));
        $_SESSION['Tgroups'] = $login_groups;
        $_SESSION['Trole_id'] = $cont;
        $_SESSION['Tterms_agree'] = $row['terms_agree'];
        $_SESSION['Tname'] = $row['username'];
        $_SESSION['Tcrypt'] = $row['user']; // hash string
        $_SESSION['Tproject'] = PROJECTTABLE;
        $_SESSION['Tmail'] = strtolower($row['email']);
        
        return true;
    }

    return false;
}
//log_action($_SESSION['Tgroups']);
/* reinitialize session after long sleep
 *
 * */
function init_session() {
    require_once('prepare_vars.php'); 

    //session változók beállítása
    st_col('default_table');

    return true;
}

/* Oauth refresh token */
function refresh_token() {
    
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    
    if (isset($_COOKIE['refresh_token'])) {

        $cookie = json_decode( $_COOKIE[ "refresh_token" ] );
        $expiry = $cookie->expiry;
        $refresh_token = $cookie->data->refresh_token;
        
        if (time() > $cookie->expiry) {
            // expired
            setcookie("refresh_token",$_COOKIE['refresh_token'],time()-1,"/");
            unset($_COOKIE['refresh_token']);
            return false;
        }
        if (defined('OAUTHURL'))
            $url = OAUTHURL.'/oauth/token.php';
        else
            $url = $protocol.'://'.URL.'/oauth/token.php';

        $data = array('refresh_token'=>$refresh_token, 'client_id'=>'web', 'client_secret'=>'web', 'grant_type'=>'refresh_token');
        $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { 
            log_action("TOKEN ($refresh_token) refreshing failed",__FILE__,__LINE__);

            setcookie("refresh_token",$_COOKIE['refresh_token'],time()-1,"/");
            unset($_COOKIE['refresh_token']);
            return false;
        } 
        else {

            $j = json_decode($result);
            // set coockies
            $expiry = time() + 3559;
            $data = (object) array( "access_token" => $j->{'access_token'} );
            $cookieData = (object) array( "data" => $data, "expiry" => $expiry );
            setcookie( "access_token", json_encode( $cookieData ), $expiry ,"/");
            // immediate access
            $_COOKIE["access_token"] = json_encode( $cookieData );
            
            $expiry = time() -1 + 60*60*24*14;
            $data = (object) array( "refresh_token" => $j->{'refresh_token'} );
            $cookieData = (object) array( "data" => $data, "expiry" => $expiry );
            setcookie( "refresh_token", json_encode( $cookieData ), $expiry ,"/");
            // immediate access
            $_COOKIE["refresh_token"] = json_encode( $cookieData );
            
            return true;
        }
    }
    return false;
}

/*
 *
 * */
function oauth_webprofile_request($access_token) {
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    if (defined('OAUTHURL'))
        $url = OAUTHURL.'/oauth/resource.php';
    else
        $url = $protocol.'://'.URL.'/oauth/resource.php';

    $data = array('access_token'=>$access_token, 'scope'=>'webprofile', 'table'=>PROJECTTABLE);
    $options = array(
        'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;
}
/*
 *
 *
 * */
function LostPw($addr) {
    global $BID; 
    $post_addr = quote(strtolower($addr));

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    require_once(getenv('OB_LIB_DIR').'languages.php');


    $cmd = sprintf("SELECT u.email AS email,p.email AS aemail FROM projects p 
        LEFT JOIN project_users pu ON (pu.project_table=p.project_table) 
        LEFT JOIN users u ON (user_id=u.id) 
        WHERE p.project_table='%s' AND user_status::varchar IN ('2','master')",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $emails = array();
    while ( $row = pg_fetch_assoc($res)) {
        $emails[] = $row['email'];
        $emails[] = $row['aemail'];
    }
    $emails = array_unique($emails);

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    $token = substr( str_shuffle( $chars ), 0, 16 );

    $cmd = "UPDATE users SET reactivate='$token' WHERE LOWER(email)=$post_addr RETURNING \"username\"";
    $res = pg_query($BID,$cmd);

    if (defined("OB_PROJECT_DOMAIN")) {
        $domain = constant("OB_PROJECT_DOMAIN");
        $server_email = PROJECTTABLE."@".parse_url("$protocol://".$domain,PHP_URL_HOST);
        $reply_email = "noreply@".parse_url("$protocol://".$domain,PHP_URL_HOST);
    } else {
        $domain = $_SERVER["SERVER_NAME"];
        $server_email = PROJECTTABLE."@".$domain;
        $reply_email = "noreply@".$domain;
    }

    $Message = '';
    $Title = "<br><br>";
    if(pg_affected_rows($res)) {
        $row = pg_fetch_assoc($res);
        $username = $row['username'];
        if ($username != '')
            $Title = t(str_dear)." $username!<br><br>";

        $Message.= "".str_reset_password_link."<br><br>";
        $Message.= "<a href='$protocol://".URL."/index.php?activate=$token&addr=".strtolower($addr)."'>$protocol://".URL."/index.php?activate=$token&addr=".strtolower($addr)."</a><br><br>";
        $Message.= "".str_set_your_password."<br><br>";
        $Message.= sprintf("".str_contact_the_maintainers." %s.<br><br><br>",implode(' or ',$emails));
        $Message.= "".str_project_team."<br>$protocol://".URL."/";
        //mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method) 
        $m = mail_to(strtolower($addr),''.str_password_reset_request.'',"$server_email","$reply_email",$Title,$Message,"multipart"); 
        //processing return message?
        return 1;
    }
    return 0;
}
/* multidim array search
 *
 *
 * */
function searchForId($name,$id, $array) {
   foreach ($array as $key => $val) {
       if ($val[$name] == $id) {
           return $key;
       }
   }
   return null;
}
/* encode a passed string with the passed key string
 *
 *
 * */
function encode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0;
    $hash = '';
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}
/* decode a passed string with the passed key string
 *
 *
 * */
function decode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0;
    $hash = '';
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}
/* --> queries.php
 * save wfs/wms result gml into files
 * NINCS már használva ez a funkció!!!
 * */
function SaveQueryTemp($query) {
    //global $_SESSION; 
    if(!isset($_SESSION['Tth'])) return;
    $ses = session_id();
    $query = preg_replace('/ /','%20',$query);
    file_put_contents("/tmp/query_$ses.txt", $query);

    # !!! A resp válasz lementése itt dupla lekérdezést eredményez, mert a 
    # result builderben is ott van az openlayers hívás miatt.... !!!
    $ses = session_id();
    $password = decode($_SESSION['Tth'],session_id());
    $context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("{$_SESSION['Tname']}:$password")
    )
    ));
    $content = file_get_contents($query, false, $context);
    file_put_contents("/tmp/conts_$ses.xml", $content);
}
/* Ajax-call function 
 * its called from functions.js
 * it saves the draw selecions as a geometry for further actions */
function AutoSaveGeometry($wkt) {
    global $GID; 
    
    
    $srid = '4326'; // default on the maps..
    //$cmd = sprintf("SELECT ST_GeomFromText('POLYGON(($query))',$srid)");
    if (isset($_SESSION['Tid'])) {
        pg_query($GID,'SET search_path TO system,public');
        $cmd = sprintf("INSERT INTO system.query_buff (\"user_id\",\"geometry\",\"datetime\",\"table\") VALUES ('%d',ST_GeomFromText(%s,4326),now(),'%s') RETURNING id",$_SESSION['Tid'],quote($wkt),PROJECTTABLE);
        $res = pg_query($GID,$cmd);
        if (!pg_num_rows($res)) {
            log_action($cmd,__FILE__,__LINE__);
            return 0;
        } else {
            $row = pg_fetch_assoc($res);
            $_SESSION['selection_geometry_id'] = $row['id'];
            $_SESSION['selection_geometry'] = $wkt;
            $_SESSION['selection_geometry_type'] = 'draw';
            return 1;
        }
    } else {
        $_SESSION['selection_geometry'] = $wkt;
        return 2;
    }
}
/* --> prepare_vars
 * save wfs/wms results gml files into database
 * map results -> main.js -> queries.php ->
 * lekérdezés eltárolása folyamat
 * */
function SaveQuery($label='',$save_type) {
    global $ID,$BID,$GID,$_SESSION; 

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    if (!isset($_SESSION['Tid'])) common_message('failed','Should be logged in!');

    // save selection
    if ($save_type == 3) {
        pg_query($GID,'BEGIN');
        pg_query($GID,'SET search_path TO system,public');
        $cmd = sprintf("DELETE FROM system.query_buff WHERE id=%s AND user_id=%d RETURNING geometry",$_SESSION['selection_geometry_id'],$_SESSION['Tid']);
        $res = pg_query($GID,$cmd);
        $row = pg_fetch_assoc($res);

        $cmd = sprintf("INSERT INTO system.shared_polygons (project_table,geometry,name,user_id) VALUES ('%s','%s',%s,%d) RETURNING id",PROJECTTABLE,$row['geometry'],quote($label),$_SESSION['Tid']);
        $res = pg_query($GID,$cmd);
        //$row = pg_fetch_assoc($res);
        //$cmd = sprintf("INSERT INTO system.polygon_users (user_id,polygon_id) VALUES (%d,%d)",$_SESSION['Tid'],$row['id']);
        //$res = pg_query($ID,$cmd);
        if (pg_affected_rows($res)) {
            $row = pg_fetch_assoc($res);
            $geometry_id = $row['id'];
            pg_query($GID,'COMMIT');
            if (defined('SHINYURL') and constant("SHINYURL"))
                return common_message("ok","<a href='$protocol://".URL."/profile/owngeoms/'>visit geometry shareing page</a>");
            else
                return common_message("ok","<a href='$protocol://".URL."/?profile&options=owngeoms'>visit geometry shareing page</a>");
        } else {
            pg_query($GID,'ROLLBACK');
            return common_message("error","Saving error");
        }
    }

    // save query or save results
    $ses = session_id();
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random_string = substr( str_shuffle( $chars ), 0, 16 );
    $link = '';

    //if (file_exists("/mnt/data/tmp/conts_$ses.xml")) {
    //$list = glob(OB_TMP."conts_$ses*.xml");
    //$cmd = sprintf("SELECT * FROM temp_query_%s_%s",PROJECTTABLE,session_id());
    $cmd = sprintf("SELECT attrelid::regclass, attnum, attname FROM pg_attribute WHERE attrelid = 'temporary_tables.temp_query_%s_%s'::regclass
        AND attnum > 0 AND NOT attisdropped ORDER BY attnum",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    $tempcols=array();
    while($row=pg_fetch_assoc($res)) {
        $tempcols[] = $row['attname'];
    }
    $tc = '"'.implode('","',$tempcols).'"';
    
    $cmd = sprintf("SELECT row_to_json(fc) as geojson
    FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
    FROM ( SELECT 'Feature' As type, ST_AsGeoJSON(lg.%s)::json As geometry, row_to_json((SELECT l FROM (SELECT %s) As l)) As properties
    FROM temporary_tables.temp_query_%s_%s As lg ) As f )  As fc",$_SESSION['st_col']['GEOM_C'],$tc,PROJECTTABLE,session_id());
    
    //row_to_json
    $res = pg_query($ID,$cmd);

    //if(count($list)) {
    if(pg_num_rows($res)) {
        //asort($list);
        //$file = array_pop($list);
        #if (file_exists("/tmp/query_$ses.txt")) {
        #    $query = file_get_contents("/tmp/query_$ses.txt", true);
        #    $query = '$'.$random_delimeter.'$'.$query.'$'.$random_delimeter.'$';
        //$result = file_get_contents("/mnt/data/tmp/conts_$ses.xml", true);
        //$result = file_get_contents($file, true);
        $row = pg_fetch_assoc($res);
        $result = $row['geojson'];

        //berakjuk a szelekció geometriát
        $geometry=NULL;
        if(isset($_SESSION['selection_geometry_id']) and isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='draw') {
            $cmd = "SELECT geometry FROM system.query_buff WHERE id={$_SESSION['selection_geometry_id']}";
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                $geometry = $r['geometry'];
            }
        } elseif(isset($_SESSION['selection_geometry_id']) and isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='customPolygon') {
            // hogyan????
            $geometry = '';
        
        } elseif(isset($_SESSION['selection_geometry_id']) and isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='sharedPolygon') {
            $cmd = "SELECT geometry FROM system.shared_polygons WHERE id={$_SESSION['selection_geometry_id']}";
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                $geometry = $r['geometry'];
            }
        } else {
            // text query
            $cmd = sprintf("SELECT ST_Extent(obm_geometry)::Geometry AS geometry FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id());
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                $geometry = $r['geometry'];
            }
        }
        if ($geometry=='') $geometry = 'NULL';
        else $geometry = quote($geometry);

        /* multiple data collectors columns joined
         *
         * SELECT ARRAY_TO_STRING(ARRAY_AGG(cite),', ') FROM
(
(SELECT DISTINCT gyujto AS cite FROM dinpi WHERE "faj" LIKE 'Calopteryx%' OR "faj" LIKE 'Agrion%' OR "faj" LIKE 'Lestes%' OR "faj" LIKE 'Chalcolestes%' OR "faj" LIKE 'Sympecma%' OR "faj" LIKE 'Coenagrion%' OR "faj" LIKE 'Erythromma%' OR "faj" LIKE 'Ischnura%' OR "faj" LIKE 'Enallagma%' OR "faj" LIKE 'Pyrrhosoma%' OR "faj" LIKE 'Platycnemis%' OR "faj" LIKE 'Aeshna%' OR "faj" LIKE 'Anaciaeschna%' OR "faj" LIKE 'Brachytron%' OR "faj" LIKE 'Anax%' OR "faj" LIKE 'Hemianax%' OR  "faj" LIKE 'Gomphus%' OR "faj" LIKE 'Stylurus%' OR "faj" LIKE 'Onychogomphus%' OR "faj" LIKE 'Ophiogomphus%' OR "faj" LIKE 'Cordulegaster%' OR "faj" LIKE 'Epitheca%' OR "faj" LIKE 'Cordulia%' OR "faj" LIKE 'Somatochlora%' OR "faj" LIKE 'Orthetrum%' OR "faj" LIKE 'Libellula%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Crocothemis%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Leucorrhinia%')
UNION
(SELECT DISTINCT gyujto2 AS cite FROM dinpi WHERE "faj" LIKE 'Calopteryx%' OR "faj" LIKE 'Agrion%' OR "faj" LIKE 'Lestes%' OR "faj" LIKE 'Chalcolestes%' OR "faj" LIKE 'Sympecma%' OR "faj" LIKE 'Coenagrion%' OR "faj" LIKE 'Erythromma%' OR "faj" LIKE 'Ischnura%' OR "faj" LIKE 'Enallagma%' OR "faj" LIKE 'Pyrrhosoma%' OR "faj" LIKE 'Platycnemis%' OR "faj" LIKE 'Aeshna%' OR "faj" LIKE 'Anaciaeschna%' OR "faj" LIKE 'Brachytron%' OR "faj" LIKE 'Anax%' OR "faj" LIKE 'Hemianax%' OR  "faj" LIKE 'Gomphus%' OR "faj" LIKE 'Stylurus%' OR "faj" LIKE 'Onychogomphus%' OR "faj" LIKE 'Ophiogomphus%' OR "faj" LIKE 'Cordulegaster%' OR "faj" LIKE 'Epitheca%' OR "faj" LIKE 'Cordulia%' OR "faj" LIKE 'Somatochlora%' OR "faj" LIKE 'Orthetrum%' OR "faj" LIKE 'Libellula%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Crocothemis%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Leucorrhinia%')
UNION
(SELECT DISTINCT gyujto3 AS cite FROM dinpi WHERE "faj" LIKE 'Calopteryx%' OR "faj" LIKE 'Agrion%' OR "faj" LIKE 'Lestes%' OR "faj" LIKE 'Chalcolestes%' OR "faj" LIKE 'Sympecma%' OR "faj" LIKE 'Coenagrion%' OR "faj" LIKE 'Erythromma%' OR "faj" LIKE 'Ischnura%' OR "faj" LIKE 'Enallagma%' OR "faj" LIKE 'Pyrrhosoma%' OR "faj" LIKE 'Platycnemis%' OR "faj" LIKE 'Aeshna%' OR "faj" LIKE 'Anaciaeschna%' OR "faj" LIKE 'Brachytron%' OR "faj" LIKE 'Anax%' OR "faj" LIKE 'Hemianax%' OR  "faj" LIKE 'Gomphus%' OR "faj" LIKE 'Stylurus%' OR "faj" LIKE 'Onychogomphus%' OR "faj" LIKE 'Ophiogomphus%' OR "faj" LIKE 'Cordulegaster%' OR "faj" LIKE 'Epitheca%' OR "faj" LIKE 'Cordulia%' OR "faj" LIKE 'Somatochlora%' OR "faj" LIKE 'Orthetrum%' OR "faj" LIKE 'Libellula%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Crocothemis%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Leucorrhinia%')
) AS foo 
         */
        /* date range of query
            SELECT min(dt_from),max(dt_from) FROM dinpi WHERE "faj" LIKE 'Calopteryx%' OR "faj" LIKE 'Agrion%' OR "faj" LIKE 'Lestes%' OR "faj" LIKE 'Chalcolestes%' OR "faj" LIKE 'Sympecma%' OR "faj" LIKE 'Coenagrion%' OR "faj" LIKE 'Erythromma%' OR "faj" LIKE 'Ischnura%' OR "faj" LIKE 'Enallagma%' OR "faj" LIKE 'Pyrrhosoma%' OR "faj" LIKE 'Platycnemis%' OR "faj" LIKE 'Aeshna%' OR "faj" LIKE 'Anaciaeschna%' OR "faj" LIKE 'Brachytron%' OR "faj" LIKE 'Anax%' OR "faj" LIKE 'Hemianax%' OR  "faj" LIKE 'Gomphus%' OR "faj" LIKE 'Stylurus%' OR "faj" LIKE 'Onychogomphus%' OR "faj" LIKE 'Ophiogomphus%' OR "faj" LIKE 'Cordulegaster%' OR "faj" LIKE 'Epitheca%' OR "faj" LIKE 'Cordulia%' OR "faj" LIKE 'Somatochlora%' OR "faj" LIKE 'Orthetrum%' OR "faj" LIKE 'Libellula%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Crocothemis%' OR "faj" LIKE 'Sympetrum%' OR "faj" LIKE 'Leucorrhinia%'
         */
        $type = "";
        if ($save_type == 2) {
            $result = "";
            $geometry = "NULL";
            $type = 'query-only';

            $cmd = sprintf("INSERT INTO \"public\".\"custom_reports\" (query_string,user_id,key,project,project_table) 
                        VALUES (%s,%d,%s,'".PROJECTTABLE."','".$_SESSION['current_query_table']."') returning id",
                        quote(json_encode($_SESSION['query_build_string'])),$_SESSION['Tid'],quote($label));
        }
        else {
            $cmd = sprintf("INSERT INTO \"public\".\"queries\" (user_id,comment,query,result,datetime,type,sessionid,name,selection) 
                        VALUES ('%s','%s',%s,%s,now(),'%s','%s',%s,%s) returning id",
                        $_SESSION['Tid'],PROJECTTABLE,quote(json_encode($_SESSION['query_build_string'])),quote($result),$type,$random_string,quote($label),$geometry);
        
        }

        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            if ($save_type == 2) {
                $link = sprintf('Query (%2$s) saved:<br><a href="%5$s://%1$s/?report=%3$d@%2$s&qtable=%4$s" target="_blank">%5$s://%1$s/?report=%3$d@%2$s&qtable=%4$s</a>',URL,$label,$r['id'],preg_replace('/^'.PROJECTTABLE.'_/','',$_SESSION['current_query_table']),$protocol);
            } else {
                $link = sprintf('Query (%4$s) saved:<br><a href="%5$s://%1$s/?LQ=%2$s@%3$s" target="_blank">%5$s://%1$s/?LQ=%2$s@%3$s</a>',URL,$r['id'],$random_string,$label,$protocol);
            }
        }
        return common_message("ok",$link);
    } else
        return common_message("error","Saving error");
}
/* get user comments from evaluations table
 *
 *
 * */
function comments($id,$table='',$db) {
    global $ID, $BID; 
    if ($id=='') return;

    if ($db == 'data') {
        $conn = $ID;
        if ($table=='')
            $table = PROJECTTABLE;
        $schema = 'system';
    }
    elseif ($db == 'upload') {
        $conn = $ID;
        $table = "uploadings";
        $schema = 'system';
    }
    else {
        $conn = $BID;
        $table = $table;
        $schema = 'public';
    }
    $m = array();
    if (preg_match('/([0-9]+)_([0-9]+)/',$id,$m)) $id = $m[1];

    $cmd = "SELECT comments,user_name,to_char(datum,'YYYY-MM-DD HH24:MI') as datum,valuation FROM $schema.evaluations WHERE \"table\"='$table' and row='$id'";
    $res = pg_query($conn,$cmd);
    $e = "<ul style='padding:0;margin:0;list-style-type:none'>";
    if (pg_num_rows($res)) {
        while ($row=pg_fetch_assoc($res)){
            $name = preg_replace("/^NULL$/","-",$row['user_name']);
            if ($row['valuation']>0) $color = "#B2DC53";
            elseif ($row['valuation']<0) $color = "#CA3C3C";
            else $color = "#42B8DD";
            $e .= "<li style='border-left:10px solid $color;line-height:1.5em;padding:3px;color:#222222;text-align:justify'>".$row['comments'];
            $e .= ' <span style="color:#333333;font-size:0.7em">['.$name.' '.$row['datum'].']</span>';
            $e .= '</li>';
        }
    }
    $e .= "</ul>";
    return $e;
}
/* track data view
 * */
function track_data ($id) {
    global $BID;
    $data_id = preg_replace('/[^0-9]/','',$id);

    $cmd = sprintf("SELECT counter FROM track_data_views WHERE project_table='%s' AND id=%s",PROJECTTABLE,quote($data_id));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $cmd = sprintf("UPDATE track_data_views SET counter=counter+1 WHERE project_table='%s' AND id=%s AND session_id!='%s'",PROJECTTABLE,quote($data_id),session_id());
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            if (pg_last_error($BID)) {
                log_action(pg_last_error($BID),__FILE__,__LINE__);
                return "not";
            }
        } else
            return $row['counter']+1;
    } else {
        $cmd = sprintf("INSERT INTO track_data_views (project_table,id,counter,session_id) VALUES('%s',%s,1,'%s')",PROJECTTABLE,quote($data_id),session_id());
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            log_action(pg_last_error($BID),__FILE__,__LINE__);
            return "error";
        } else
            return '1';
    }
    return "error";
}
/* track download
 * */
function track_download ($id) {
    global $BID;
    if (!is_array($id)) {
        $data_id = array($id);
    } else {
        $data_id = $id;
    }
    $cmd = "";
    $rows = array();
    foreach ($data_id as $p) {

        $scmd = sprintf("UPDATE track_downloads SET counter=counter+1 WHERE project_table='%s' AND id=%s AND session_id!='%s'",PROJECTTABLE,quote($p),session_id());
        $res = pg_query($BID,$scmd);
        // Számláló frissítve, egy sessionben csak 1x frissítek!
        // else
        if (!pg_affected_rows($res)) {
            // nem volt frissítés mert már volt ebben a sessionben? 
            $scmd = sprintf("SELECT counter FROM track_downloads WHERE project_table='%s' AND id=%s",PROJECTTABLE,quote($p));
            $res = pg_query($BID,$scmd);
            if(!pg_num_rows($res))
                $rows[] = sprintf("%s\t%d\t1\t%s",PROJECTTABLE,$p,session_id());
        }
    }
    //insert executes: all at once
    if (count($rows)) pg_copy_from($BID, "track_downloads", $rows, "\t","\\NULL");
    return;
}
function metrics ($id,$t) {
    global $BID;
    $cmd = sprintf("SELECT counter FROM track_%s WHERE project_table='%s' AND id=%d",$t,PROJECTTABLE,$id);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return($row['counter']);
    }
}
function cite_metrics ($id,$target,$table) {
    global $BID;
    if ($target == 'citations')
        $cmd = sprintf("SELECT ARRAY_TO_STRING(source,', ') AS source FROM track_%s WHERE project_table='%s' AND data_table=%s AND id=%d",'citations',PROJECTTABLE,quote($table),$id);
    else
        return json_encode(array());

    $res = pg_query($BID,$cmd);
    $a = array();
    while ($row = pg_fetch_assoc($res)){
        $a[] = $row['source'];
    }
    return(json_encode($a));
}
// visitor statistics
function track_visitors ($page) {
    global $BID;
    $bot = 0;
    #if (preg_match('/robot/',$_SERVER['HTTP_USER_AGENT']))
    #    $bot = 1;
    #elseif (preg_match('/bot\.html/',$_SERVER['HTTP_USER_AGENT']))
    #    $bot = 1;

    if (!$bot) {
        $request = $_REQUEST;
        if (isset($request['loginpass'])) $request['loginpass'] = '';
        $cmd = sprintf("INSERT INTO project_visitors (date,ip_addr,project_table,page,action) VALUES(now(),'%s','%s','%s',%s)",$_SERVER['REMOTE_ADDR'],PROJECTTABLE,$page,quote(json_encode($request)));
        pg_query($BID,$cmd);
    }
}

/* Data validation score calculation
 * Return the validation score for a data id
 *
 * */
function validation ($id,$table=PROJECTTABLE) {
    global $ID, $BID; 
    if ($id=='') return;
    $m = array();
    if (preg_match('/([0-9]+)_([0-9]+)/',$id,$m)) $id = $m[1];

    $valid = obm_cache('get',"mean_validation".$id);
    if ($valid !== false ) 
        return $valid;
    
    $id = quote($id);
    $mean = 0;

    // project validation
    $header_val = project_validation(PROJECTTABLE);
    
    // data validation
    $cmd = "SELECT obm_validation as v,obm_uploading_id FROM ".$table." WHERE obm_id=$id";
    $result = pg_query($ID,$cmd);
    $row=pg_fetch_assoc($result);
    $uploading_id = $row['obm_uploading_id'];
    $cal_val = $row['v'];

    
    // uploading validation
    $uploading_id = quote($row['obm_uploading_id']);
    //$cmd = "SELECT array_avg(validation) as v,uploader_id FROM system.uploadings WHERE id=$uploading_id";
    $cmd = "SELECT validation as v,uploader_id FROM system.uploadings WHERE id=$uploading_id";
    $result = pg_query($ID,$cmd);
    $row=pg_fetch_assoc($result);
    $uploader_id = $row['uploader_id'];
    $uploading_val = $row['v'];

    // user's validation

    $userdata = new userdata($uploader_id,'hash');
    $user_val = user_validation($userdata->get_userid());

    if($header_val!=0 and $header_val!='') $mean++;
    if($cal_val!=0 and $cal_val!='') $mean++;
    if($uploading_val!=0 and $uploading_val!='') $mean++;
    if($user_val!=0 and $user_val!='') $mean++;
    
    if($mean)
        $valid = sprintf("%.2f",($header_val+$cal_val+$user_val+$uploading_val)/$mean);
    else
        $valid = '';
    
    obm_cache('set',"mean_validation".$id,$valid,30);
    
    return $valid;
}
// user's validation
function user_validation ($user_id) {
    global $BID;

    $user_id = preg_replace('/[^0-9]/','',$user_id);
    if(!$user_id) return;
    
    $cmd = sprintf("SELECT validation as v FROM users WHERE id=%d",$user_id);
    $result = pg_query($BID,$cmd);
    $row=pg_fetch_assoc($result);
    $v = $row['v'];

    return $v;
}
// project validation
function project_validation ($project_table) {
    global $BID;
    $cmd = sprintf('SELECT validation as v FROM projects WHERE project_table=%s',quote($project_table));
    $result = pg_query($BID,$cmd);
    $row=pg_fetch_assoc($result);
    $v = $row['v'];
    return $v;
}
/* general column names from header_names
 * Össze lehetne vonni az other_col függvénnyel is és akkor a hstore
 * típusok tömbbként lennének az st_col-ban tárolva
 * */
function st_col ($table,$output='session') {
    global $BID,$ID; 

    $ACC_LEVEL = ACC_LEVEL;

    if ($table == 'default_table' or $table == '') {
        if (defined('DEFAULT_TABLE'))
            $table = DEFAULT_TABLE;
        else
            $table = PROJECTTABLE;
    }
    
    $st_col = obm_cache('get',"st_col.$table",'','',FALSE);

    if ($st_col===FALSE) {
        $schema = 'public';
        $sp = preg_split('/\./',$table); 
        if (count($sp)==2) {
            $table = $sp[1];
            $schema = $sp[0];
        }
        $cmd = sprintf("SELECT f_geom_column, f_species_column, f_quantity_column, f_id_column,
            coalesce(f_x_column,'') as f_x_column,coalesce(f_y_column,'') as f_y_column,
            f_srid, f_order_columns,f_restrict_column,
            ARRAY_TO_STRING(f_cite_person_columns,',') AS f_cite_person_columns,    
            ARRAY_TO_STRING(f_date_columns,',') AS f_date_columns,
            ARRAY_TO_STRING(f_file_id_columns,',') AS f_file_id_columns,
            ARRAY_TO_STRING(f_alter_speciesname_columns,',') AS f_alter_speciesname_columns
        FROM header_names 
        WHERE f_table_schema=%s AND f_table_name='%s' AND f_main_table=%s",quote($schema),PROJECTTABLE,quote($table));
        $result = pg_query($BID,$cmd);
        $row = pg_fetch_assoc($result);
        
        $st_col = array();
        $st_col['GEOM_C'] = trim($row['f_geom_column']);
        $st_col['SPECIES_C'] = trim($row['f_species_column']);
        $st_col['NUM_IND_C'] = trim($row['f_quantity_column']);
        $st_col['ID_C'] = trim($row['f_id_column']); //should be obm_id
        $st_col['X_C'] = trim($row['f_x_column']);
        $st_col['Y_C'] = trim($row['f_y_column']);
        $st_col['SRID_C'] = trim($row['f_srid']);
        $st_col['ORDER'] = trim($row['f_order_columns']);
        $st_col['CITE_C'] = preg_split("/,/",($row['f_cite_person_columns']));
        $st_col['DATE_C'] = preg_split("/,/",$row['f_date_columns']);
        $st_col['ALTERN_C'] = preg_split("/,/",$row['f_alter_speciesname_columns']);
        $restrict_prestate = $row['f_restrict_column'];

        /* Visszatartott adatsort jelentő oszlop a restriction és a secret modulok használják
         * It is depend on the extince on the rules table and the global ACC_LEVEL
         * */

        $cmd = "SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '".PROJECTTABLE."_rules') AS exists";
        $result = pg_query($ID,$cmd);
        //pg_close($nID);
        $rules_table = pg_fetch_assoc($result);

        if ($rules_table['exists']=='t') {
            if ("$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group') {
                if ($restrict_prestate=='0' or $restrict_prestate=='false' or $restrict_prestate=='-1' or $restrict_prestate===false or $restrict_prestate=='f' or $restrict_prestate==''){
                    //drop restrict rules if table explicitley disabled for this
                    $st_col['RESTRICT_C'] = false;
                } else {
                    $st_col['RESTRICT_C'] = true;
                }
            } else {
                $st_col['RESTRICT_C'] = false;
            }
        } else {
            $st_col['RESTRICT_C'] = false;

            if ("$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group')
                log_action("The global access level is `group` but ".PROJECTTABLE."_rules table does not exists!",__FILE__,__LINE__);
        }
    
        //itt gyakorlatilag nem az oszlop nevét használom, hanem a tényt hogy van attachment használat - ez modulként kellene nem így
        $st_col['FILE_ID_C'] = trim($row['f_file_id_columns']); 
    
        obm_cache('set',"st_col.$table",$st_col,300,FALSE);
    }

    if ($output == 'array')
        return $st_col;
    else {
        stcol_to_session($st_col);
    }
}
/* Return taxon name columns
 * language_label=>column_name
 * NOT USED EVER!
 *
 * */
function lang_col($table) {
    global $BID; 

    $columns = obm_cache('get',"lang_col",'','',FALSE);
    if ($columns) {
        return $columns;
    }

    $cmd = sprintf("SELECT (each(f_alter_speciesname_columns)).key as key, (each(f_alter_speciesname_columns)).value as value FROM header_names WHERE f_table_name='%s' AND f_main_table=%s",PROJECTTABLE,quote($table));
    $result = pg_query($BID,$cmd);
    $COLUMNS = array();
    while($row = pg_fetch_assoc($result)) {
        $COLUMNS[$row['key']]=$row['value'];
    }

    obm_cache('set',"lang_col",$COLUMNS,300,FALSE);
    return $COLUMNS;    
}
/* st_col elements as _SESSION array
 *
 * */
function stcol_to_session($st_col) {

    global $BID; 

    $_SESSION['st_col'] = array();
    
    foreach ($st_col as $k=>$v) {
        $_SESSION['st_col'][$k] = $v;
    }

    /* tábla tábla összekapcsolások
     * amikor automatikusan akarunk ilyet csinálni, nem modulból statikusan akkor lehet használni:
     *
     * */
    /*
    $_SESSION['ic_col'] = array();
     * $cmd = sprintf("SELECT project_table,inter_table,pt_id_col,it_id_col FROM project_interconnects WHERE project_table='%s'",PROJECTTABLE);
    $result = pg_query($BID,$cmd);

    while($row = pg_fetch_assoc($result)) {
        $_SESSION['ic_col'][$row['inter_table']] = $row['it_id_col'];
    }*/
}
// random color
// Creates a random color string
// return a 6byte long hex string like AABBCC
function rand_color() {
    $rc = '';
    for ($i=0;$i<3;$i++) {
        $rnd = dechex(rand(0,255));
        if (strlen($rnd) == 1 ) $rnd = "0$rnd";
        $rc .= $rnd;
    }
    return $rc;
}
/* invitation code processing
 *
 *
 * */
function inventer ($code) {
    global $BID;
    require_once(getenv('OB_LIB_DIR').'languages.php');

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    # TRANSLATIONS!!
    $err = '';
    $post_code=preg_replace("/[^a-zA-Z0-9]/","",$code);
    if (strlen($post_code)!=32) $post_code='';

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    $random_pw = substr( str_shuffle( $chars ), 0, 16 );

    if ($post_code!='') {
        $post_code = quote($post_code);
        $cmd = "SELECT u.id as uid,i.id,project_table,mail,email FROM invites i LEFT JOIN users u ON mail=email WHERE code=$post_code AND created + interval '60 days' > now()";
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            $inv_id = $r['id'];
            $inv_pt = $r['project_table'];
            $inv_m = $r['mail'];
            if ($r['uid']!='') {
                //already registered user
                $user_id = $r['uid'];
                $registered_user = 1;
            }
            else {
                $user_id = '';
                $registered_user = 0;
            }
            //$inv_gr = $r['group'];
        } else {
            #pg_last_error($BID);
            log_action("Expired or invalid invitation: ".$cmd,__FILE__,__LINE__);
            return "Expired or invalid invitation code";
        }
        if ($user_id=='') {

            if (!$hash = gen_password_hash($random_pw)) {
                return false;
            }

            pg_query($BID,'BEGIN');
            $cmd = sprintf('INSERT INTO "users" ("user",password,status,username,institute,address,email,inviter,pwstate) 
                                SELECT \'\',%s,1,name,\'\',\'\',mail,user_id,true
                                FROM invites 
                                WHERE id=\'%d\'
                            RETURNING id',quote($hash),$inv_id);
            $res = pg_query($BID,$cmd); 
            if (!pg_affected_rows($res)){
                if (preg_match('/duplicate key value/',pg_last_error($BID))) $err = "The user already exists.";
                else $err = "Registration error.";
                log_action($err,__FILE__,__LINE__);
                pg_query($BID,'ROLLBACK');
                return $err;
            } else {
                $row = pg_fetch_assoc($res);
                $user_id = $row['id'];
            }
        }

        $cmd = "INSERT INTO project_users (project_table,user_id) VALUES ('$inv_pt',$user_id)";
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            if (preg_match('/duplicate key value/',pg_last_error($BID))) $err = "This email already registered in this project.";
            else $err = "Registration error.";
            log_action($err,__FILE__,__LINE__);
            pg_query($BID,'ROLLBACK');
            return $err;
        }

        # inviter's info
        $cmd = sprintf('SELECT mail,email,name,project_table,"user" FROM "users" u LEFT JOIN invites i ON i.user_id=u.id WHERE i.id=%d',$inv_id);
        $res = pg_query($BID,$cmd);
        $inviter = '';
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $inviter = $row['email'];
            $invited = $row['name'];
            $destination = $row['project_table'];
        }
        
        #new user's info
        $cmd = sprintf('SELECT "user",email FROM "users" WHERE id=%d',$user_id);
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $user_email = $row['email'];
            $user_profile_id = $row['user'];
        }
        //drop all invitation
        //$cmd = "DELETE FROM invites WHERE id='$inv_id'";
        $cmd = sprintf("DELETE FROM invites WHERE mail='%s' AND project_table='%s'",$user_email,PROJECTTABLE);
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res)) {
            pg_query($BID,"COMMIT");
            //mail to the inviter
            if ($inviter!='') {

                if (defined("OB_PROJECT_DOMAIN")) {
                    $domain = constant("OB_PROJECT_DOMAIN");
                    $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
                    $reply_email = "noreply@".parse_url('http://'.$domain,PHP_URL_HOST);
                } else {
                    $domain = $_SERVER["SERVER_NAME"];
                    $server_email = PROJECTTABLE."@".$domain; 
                    $reply_email = "noreply@".$domain;
                }

                $m = mail_to("$inviter",t(str_invitation_accomplished),$server_email,$reply_email,t(str_your_invitations_accepted), sprintf(str_signed_up_into, $invited, $destination) , 'multipart');
                $m = mail_to("$user_email",t(str_welcome_to),$server_email,$reply_email,t(str_welcome_to_project_in_domain),"
    ".str_already_activated_link.":<br>
    &nbsp; &nbsp; &nbsp;<a href='$protocol://".URL."/index.php?profile=$user_profile_id'>".URL."/index.php?profile=$user_profile_id</a>
    <br><br>
    ".str_create_new_password_link.":<br>
    &nbsp; &nbsp; &nbsp;<a href='$protocol://".URL."/index.php?lostpw'>".URL."/index.php?lostpw</a>
    <br><br>
    ".str_copy_paste_link."",'multipart');
            }
            //processing return code?
            //...
            if ($registered_user==0) {
                $_SESSION['register_upw']=$random_pw; //generated password
                $_SESSION['register_um']=$inv_m; //invited email
                return true;
            } else {
                // already registered user
                return true;
            }
        } else {
            $_SESSION['le'] = "sql error";
            pg_query($BID,'ROLLBACK');
            return "Registration error.";
        }
    } else {
        return "Registration error";
    }
}
/* generate password hash
 *
 * */
function gen_password_hash($password_string) {

    if (function_exists ( 'mcrypt_create_iv' ))
        $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
    elseif (function_exists( 'random_bytes' ))
        $salt = random_bytes(22);
    else {
        log_action("ERROR!! No random salt function available to create password!",__FILE__,__LINE__);
        return false;
    }

    $options = [
        'cost' => 10,
        'salt' => $salt,
    ];
    $hash = password_hash($password_string, PASSWORD_BCRYPT, $options);
    if ($hash!='')
        return $hash;
    else
        return false;
}

/* Update password hash and delete reactivate string 
 * it called from prepare_vars.php when isset GET['activate']
 * users clicked on reactivate link
 * */
function reactivate($addr,$code) {
    global $BID;
    $post_code=preg_replace("/[^a-zA-Z0-9]/","",$code);

    if ($post_code!='') {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $random_pw = substr( str_shuffle( $chars ), 0, 16 );
        
        if (function_exists ( 'mcrypt_create_iv' ))
            $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        elseif (function_exists( 'random_bytes' ))
            $salt = random_bytes(22);
        else {
            log_action("ERROR!! No random salt function available to create password!",__FILE__,__LINE__);
            return "Sorry, there is a system problem.";
        }


        $options = [
                'cost' => 10,
                'salt' => $salt,
        ];
        $hash = password_hash($random_pw, PASSWORD_BCRYPT, $options);

        $cmd = sprintf('UPDATE users SET password=%s,reactivate=NULL,pwstate=TRUE WHERE LOWER(email)=%s AND reactivate=%s RETURNING "email"',quote($hash),quote(strtolower($addr)),quote($post_code));
        $res = pg_query($BID,$cmd); 
        if($row = pg_fetch_assoc($res)){
            $_SESSION['register_upw']=$random_pw;
            $_SESSION['register_uuu']=strtolower($row['email']);
            return true;
        } else {
            return "Not valid email or activation code. Probably your activation code expired.";
        }
    }
    return false;
}
/*
 *
 *
 * */
function floatcmp($f1,$f2,$precision = 10)
{
    $e = pow(10,$precision);
    return (intval($f1 * $e) == intval($f2 * $e));
}
/* create html select menu
 * should be generalized??
 *
 */
class select_input_template {
    private $def_style="width:100%";
    public $style="width:100%";
    public $id;
    public $reference;
    public $menu;
    public $size=4;
    public $multi="multiple='multiple'";
    public $autocomplete=false;
    public $colour_rings=false;
    public $empty_line="";
    public $format_query="";
    public $access_class="";
    public $destination_table = '';
    public $tdata = '';
    public $filter_column = '';
    public $filter_value = '';
    public $all_options = 'off';
    public $onlyoptions = 'off';
    
    function set_style($style) {
        if ($style=='')
            $this->style = $this->def_style;
        else
            $this->style = $style;
    }

    function add_empty_line($e) {
        $this->empty_line = $e;
    }

    function format_query($e) {
        $this->format_query = $e;
    }
    
    function new_menu($reference='',$filter='') {
        $this->reference = $reference;
        if ($this->destination_table=='')
            $this->destination_table = $_SESSION['current_query_table'];
        
        if ($this->autocomplete)
            $this->push_autocomplete_menu();
        elseif ($this->colour_rings)
            $this->push_colour_rings_menu();
        else
            $this->push_selectmenu($filter);
    }

    // creates an input field for autocomplete input
    function push_autocomplete_menu() {
        $id = $this->reference; 

        $pm_split = preg_split('/\./',$this->reference);
        if (count($pm_split) > 1) {
            $id = preg_replace('/\./','-',$this->reference);
        }
        if ($this->id!='')
            $id = $this->id;

        // data-... elements
        $tdata = '';
        if ($this->tdata != '') {
            foreach ($this->tdata as $target=>$value) {
                $tdata .= "data-$target='".$value."' ";
            }
        }

        $this->menu = "<input id='$id' class='qfautocomplete $this->access_class' $tdata>";
    }

    // colour rings div
    function push_colour_rings_menu() {
        $id = $this->reference; 

        $pm_split = preg_split('/\./',$this->reference);
        if (count($pm_split) > 1) {
            $id = preg_replace('/\./','-',$this->reference);
        }
        if ($this->id!='')
            $id = $this->id;

        // data-... elements
        $tdata = '';
        if ($this->tdata != '') {
            foreach ($this->tdata as $target=>$value) {
                $tdata .= "data-$target='".$value."' ";
            }
        }

        $colors = array('red','pink','green','lightgreen','orange','yellow','blue','lightblue','white','black','brown','purple','violet','metal');
        $codes = array('R','P','G','g','O','Y','B','b','W','K','N','U','V','M');
        $textcolors = array('K','K','K','K','K','K','W','K','K','W','W','W','K','K');

        // It is coming from the form settings
        // e.g.
        //$available_ring_colors = "[XX],blue:B,red:B";
        if (isset($available_ring_colors) and count($available_ring_colors)!=0) {
            $arc = array_keys($available_ring_colors);
            $custom_colors = array();
            $custom_codes = array();
            if (preg_match('/^\[(.+)\]$/',$arc[0],$m)) {
                array_shift($available_ring_colors);
                $ring_position_pattern = $m[1]; 
            }
            foreach ($available_ring_colors as $cc=>$clabel) {
                $label = strtolower($clabel[0]);
                $custom_colors[] = $label;
                if ($cc!='')
                    $custom_codes[] = $cc;
                else {
                    $ci = array_search($label,$colors);
                    $custom_codes[] = $codes[$ci];
                }
            }
        }

        $li = array();
        for($i=0;$i<count($colors);$i++) {

            $dca = array($colors[$i],$codes[$i],$textcolors[$i]);
            
            if (isset($arc)) {
                $index = array_search($dca[0],$custom_colors);
                if ($index===false) continue;
                if (isset($custom_codes[$index]))
                    $dca[1] = $custom_codes[$index];
            }
            $title = t(constant('str_'.$dca[0]));
            if ($dca[0]=='metal') $dca[0] = 'silver';
            if ($dca[2]=='K') $textcolor = '#000';
            else $textcolor = '#fff';
            $li[] = "<li style='background-color:$dca[0];color:$textcolor' data-color='$dca[1]' data-text='$dca[2]' title='$title'>$dca[1]</li>";
        }

        $out = "
        <div id='colourringdiv'>
            <div style='display:table;border-spacing:2px'>

                <div style='display:table-row;'>
                    <div style='display:block;text-align:right;background-color: #b3cee6;margin-top: -7px;margin-left: -7px;margin-right: -7px;'>
                        <button id='colourringdiv-close' class='pure pure-button button-gray' style='height:30px'><i class='fa fa-close fa-lg'></i></button>
                    </div>
                </div>

                <div style='display:table-row'>
                    <div style='display:table-cell;vertical-align:top;background:repeating-linear-gradient( 45deg, #888, #888 3px, #aaa 3px, #aaa 6px);width:100%'>";
                        //$default_colours = array('red:R:K,','pink:P:K','green:G:K','lightgreen:g:K','orange:O:K','yellow:Y:K','blue:B:W','lightblue:b:K','white:W:K','black:K:W','brown:N:W','purple:U:W','violet:V:K','metal:M:K');
                        if (isset($ring_position_pattern)) 
                            $out .= "<input type='hidden' value='$ring_position_pattern' id='ring_position_pattern'>";
                        else
                            //two rings on each position
                            $out .= "<input type='hidden' value='XX' id='ring_position_pattern'>";
                        $out .= "
                            <ul class='ring-box ring'>"; $out .= implode('',$li); $out .= "</ul>
                    </div>
                </div>

                <div>
                    <div id='legbox'>
                        <div class='left'>
                            <ul id='leg-left-top' class='ring-position ring'></ul>
                            <ul id='leg-left-bottom' class='ring-position ring'></ul>
                            <span id='leftlabel' class='leftrightlabel'>".str_left."</span>
                        </div>
                        <div class='right'>
                            <ul id='leg-right-top' class='ring-position ring'></ul>
                            <ul id='leg-right-bottom' class='ring-position ring'></ul>
                            <span id='rightlabel' class='leftrightlabel'>".str_right."</span>
                        </div>
                    </div>
                    <div style='float:right'>
                        <button id='rotate' class='pure pure-button button-gray' style='width:40px;'><i class='fa fa-refresh fa-lg fa-rotation'></i></button>
                    </div>
                </div>
                
                <div>
                    <div><input id='cr_code' style='border:none;background:transparent'></div>
                    <div id='ring-text-div'>".str_ring_label.": <input id='ring-text' class='pure-input pure-u-1' data-text='' data-color=''></div>
                    <div><button class='pure-button button-success pure-u-1' id='addcolourings'>".str_set."</button></div>
                    <input type='hidden' id='colour-ring-referenceid' value='$id'>
                </div> 
            </div>
        </div> <!-- colourringdiv -->";

        $this->menu = "$out<input id='$id' class='qfautocomplete show-colour-ring-box $this->access_class' $tdata>";
    
    }

    // creates a select input fields with options queried from a table
    function push_selectmenu($filter='') {
        global $ID;
        $op = "";
        if ($this->empty_line == 1) {
            $op = "<option value=''></option>";
        }
        # using complex SQL columns naming - ## will be changed the requested column name dinamically
        # I don't know where and how can I use it!!!
        /*if ($format_query != '') {
            $s = preg_replace('/##/',$this->id,$format_query);
        } else*/
        
        $id = $this->reference; 
        $pm_column = $this->reference;
        $table_alias = '';

        $pm_split = preg_split('/\./',$this->reference);
        if (count($pm_split) > 1) {
            $pm_column = $pm_split[1];
            /*if (preg_match('/(.+):(.+)/',$pm_column,$m)) {
                $pm_column = $m[1];
                $pm_column_id = $m[2];
        }*/
            $table_alias = $pm_split[0];
            //html id
            $id = preg_replace('/\./','-',$this->reference);
        }
        if ($this->id!='')
            $id = $this->id;

        $transform_to_boolen = 0;
        if ($filter != '') {
            if (preg_match('/_boolen/',$filter,$m)) {
                $transform_to_boolen = 1;
                $filter = '';
            } elseif (preg_match('/values\((.*)\)/',$filter,$m)) {
                if ($m[1]=='')
                    $filter = 'WHERE 1=0';
                
            } else {
                $filter = '';
            }
        }
        
        // imporved filtering
        if ($this->filter_column!='' and $this->filter_value!='') {
            $this->filter_column = preg_replace('/-/','.',$this->filter_column);
            $imp = implode(',',array_map('quote',$this->filter_value));
            $filter = "WHERE $this->filter_column IN ($imp)";
        }

        // data-... elements
        $tdata = '';
        if ($this->tdata != '') {
            foreach ($this->tdata as $target=>$value) {
                $tdata .= "data-$target='".$value."' ";
            }
        }

        if (array_key_exists($pm_column,dbcolist('array',$this->destination_table))) {
            $stcol = st_col($this->destination_table,'array');
            // FUll list, no distinct, but ID
            if ($this->all_options=='on')
                $cmd = "SELECT $pm_column AS n, substring($pm_column::character varying,1,24) AS l, {$stcol['ID_C']} AS id FROM ".$this->destination_table." $table_alias $filter";
            else        
                // select-options no ID, just distinct!!!
                $cmd = "SELECT DISTINCT $pm_column AS n, substring($pm_column::character varying,1,24) AS l FROM ".$this->destination_table." $table_alias $filter";

            $result = pg_query($ID,$cmd);
            $sort = array();
            while($row=pg_fetch_assoc($result)) {
                if ($this->size>1 and $row['n']=='') {
                    continue;
                }
                $value = $row['n'];
                $label = $row['l'];
                if ($transform_to_boolen and $label == 1) {
                    $label = str_true;
                } elseif ($transform_to_boolen and $label == 0) {
                    $label = str_false;
                }
                if (isset($row['id']))
                    $dataid = $row['id'];
                else
                    $dataid = '';

                if (defined($value))
                    $label = constant($value);
                $sort[$label] = "<option value='$value' data-id='$dataid' title='$label'>$label</option>";
            }
            #$col = new \Collator('hu_HU');
            #$col->asort( $sort );
            ksort($sort);
            if ($this->size>1 and count($sort)<$this->size) {
                $size = count($sort);
            } else {
                $size = $this->size;
            }
            $m = "<select size='$size' style='{$this->style}' {$this->multi} id='$id' class='$this->access_class' $tdata>";
            $mo = implode("",$sort);
            if ($this->onlyoptions=='on')
                $this->menu = $mo;
            else
                $this->menu = $m.$mo."</select>";
        } else {
            if ($pm_column!='' and !preg_match('/^_table_prefix/',$pm_column)) {
                $this->menu = "NA";
                log_action("$pm_column column does not exists in {$this->destination_table} table!",__FILE__,__LINE__);
            }
        }
    }
}
// create html table rows put td cell colspan, etc...
class table_row_template {
    public $tmpl_col = array();
    public $tmpl_row = array();
    public $colspan=0;
    /*     f: cell content
     *    cp: colspan
     * class: css class
     * style: css style
     * */
    function cell($f,$cp=1,$class='',$style='') {
        $colspan = "";
        if ($cp>1) $colspan = "colspan='$cp'";
        $this->tmpl_col[] = "<td $colspan class='$class' style='$style'>$f</td>";
    }
    function row($class='',$style='') {
        $r = implode("",$this->tmpl_col);
        $this->tmpl_col = array();
        $this->tmpl_row[] = "<tr class='$class' style='$style'>$r</tr>";
    }
    function printOut() {
        return implode("",$this->tmpl_row);
    }
}
/* Send mail to...
 *
 *
 * */
function mail_to_ori($To,$Subject,$FromName,$FromEmail,$Title,$Message,$method,$domain,$realemail) {
    $stripped_title = strip_tags($Title);
    $stripped_message = strip_tags($Message,'<a>');
      
    $HTMLMessage = "<h2>$Title</h2>$Message";
    $TEXTMessage = $stripped_title."\n\n".$stripped_message;
    $subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';
    $mime_boundary = "openbiomaps.org_".md5(time()); 
      
    if ($method == 'simple') 
    {
        # Mail headers
        $Headers  = "From: $FromName <$FromEmail@$domain>\r\n";
        $Headers .= "Reply-To: $realemail\r\n";
        $Headers .= "MIME-Version: 1.0\r\n";
        $Headers .= "Content-Type: text/plain; charset=utf8\r\n";
        $Headers .= 'X-Mailer: PHP/' . phpversion();
    }
    elseif ($method == 'multipart') 
    {
        # Mail headers
        $Headers  = "From: $FromName <$FromEmail@$domain>\r\n";
        $Headers .= "Reply-To: $realemail\r\n";
        $Headers .= "MIME-Version: 1.0\r\n";
        $Headers .= "Content-Type: multipart/alternative;\n\tboundary=--PHP-alt-$mime_boundary\r\n";
        $Headers .= 'X-Mailer: PHP/' . phpversion();
          
        ob_start(); //Turn on output buffering
?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/plain;charset=utf8
Content-Transfer-Encoding: 8bit

<?php echo $TEXTMessage."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/html;charset=utf8
Content-Transfer-Encoding: 8bit

<?php echo $HTMLMessage."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>--

<?php
        echo "\r\n";
        $message = ob_get_clean();    
    }
    #ini_set("SMTP","vocs.unideb.hu");
    # Send mail
    $mail_sent = @mail( $To, $subject, $message, $Headers );
    if (!$mail_sent) return "Failed to sent mail.";
    else return $mail_sent;
}
/* pear/Mail deprecated */
function pear_mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method) {
    //https://pear.php.net/package/Mail
    //https://pear.php.net/package/Net_SMTP/
    //require_once "Mail.php";
    require_once('vendor/autoload.php');
 
    //base64 subject
    $Subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';

    //mime boundary
    $mime_boundary = "openbiomaps.org_".md5(time()); 
      
    if ($Method == 'simple') 
    {
        $content_type = 'text/plain; charset=utf8';
        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $body = $stripped_title."\n\n".$stripped_message;
    }
    elseif ($Method == 'multipart') 
    {
        $body = "<h2>$Title</h2>$Message";

        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $plain_body = $stripped_title."\n\n".$stripped_message;

        $content_type = "multipart/alternative;\n\tboundary=--PHP-alt-$mime_boundary";
        ob_start(); //Turn on output buffering
    ?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/plain;charset=utf8
Content-Transfer-Encoding: 8bit

    <?php echo $plain_body."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/html;charset=utf8
Content-Transfer-Encoding: 8bit

    <?php echo $body."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>--

    <?php
        echo "\r\n";
        $body = ob_get_clean();    
    }
 
    if (defined('SMTP_PORT')) 
        $port = array('port' => SMTP_PORT);
    else 
        $port = array();
 
    $headers = array (
        'From' => $From,
        'To' => $To,
        'Reply-To' => $ReplyTo,
        'Subject' => $Subject,
        'MIME-Version' => '1.0',
        'Content-Type' => $content_type,
        'X-Mailer' => 'PHP/' . phpversion()
    );

    if (SENDMAIL=='sendmail')
        $smtp = Mail::factory('sendmail');
    elseif(SENDMAIL=='smtp') {
        // overwrite SMTP sender with SMTP_USERNAME from local_vars.php.inc 
        $headers["From"] = SMTP_USERNAME;

        $smtp = Mail::factory('smtp',
        array_merge($port,array ('host' => SMTP_HOST,
               'auth' => SMTP_AUTH,
               'username' => SMTP_USERNAME,
               'password' => SMTP_PASSWORD)));
    }

    $mail = $smtp->send($To, $headers, $body);

    # $pear = new PEAR(); $pear->isError($mail);
    //if (PEAR::isError($mail)) {
    if (@PEAR::isError($mail)) {
        log_action('Message sending failed.',__FILE__,__LINE__);
        log_action($mail->getMessage(),__FILE__,__LINE__);
        return 1;
    } else {
 
        //"Message successfully sent!";
        return 2;
    }
}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/* php mailer */
function mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method,$ReplyToName='No Reply',$FromName=PROJECTTABLE) {
    
    require_once('vendor/autoload.php');
 
    //base64 subject
    $Subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';

    if ($Method == 'simple') 
    {
        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $body = $stripped_title."\n\n".$stripped_message;
        $plain_body = $body;
        $isHTML = false;
    }
    elseif ($Method == 'multipart') 
    {
        $body = "<h2>$Title</h2>$Message";
        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $plain_body = $stripped_title."\n\n".$stripped_message;
        $isHTML = true;
    }
 
    $mail = new PHPMailer(true);        // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        if (SENDMAIL=='sendmail') {
            $mail->isSendmail();
        
        } elseif(SENDMAIL=='smtp') {
            $From = defined('SMTP_SENDER') ? SMTP_SENDER : SMTP_USERNAME;

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = SMTP_HOST;                              // Specify main and backup SMTP servers: 'smtp1.example.com;smtp2.example.com'
            $mail->SMTPAuth = SMTP_AUTH;                          // Enable SMTP authentication
            $mail->Username = SMTP_USERNAME;                      // SMTP username
            $mail->Password = SMTP_PASSWORD;                      // SMTP password
            $mail->SMTPSecure = defined('SMTP_SECURE') ? SMTP_SECURE : 'tls'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = defined('SMTP_PORT') ? SMTP_PORT : 25;                                  // TCP port to connect to    
        }

        //Recipients
        $mail->setFrom($From, $FromName);
        #$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
        if (is_array($To)) {
            #$mail->addCC('cc@example.com');
            #$mail->addBCC('bcc@example.com');
            $first_rcp = array_pop($To);
            $mail->addAddress($first_rcp);
            foreach ($To as $cc) {
                $mail->addCC($cc);
            }
        }
        else
            $mail->addAddress($To);               // Name is optional
        $ReplyTo = preg_replace('/localhost/','openbiomaps.org',$ReplyTo);
        $mail->addReplyTo($ReplyTo, $ReplyToName);

        //Attachments
        #$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        #$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML($isHTML);                                  // Set email format to HTML
        $mail->Subject = $Subject;
        $mail->Body    = $body;
        $mail->AltBody = $plain_body;

        $mail->CharSet = 'UTF-8';
        $mail->send();
        //"Message successfully sent!";
        return 2;
    } catch (Exception $e) {
        log_action('Message sending failed.',__FILE__,__LINE__);
        log_action($mail->ErrorInfo,__FILE__,__LINE__);
        return 1;
    }    
}
/*
 *
 *
 * */
function get_type($var)
{ 
    if (is_array($var)) return "array";
    if (is_bool($var)) return "boolean";
    if (is_float($var)) return "float";
    if (is_int($var)) return "integer";
    if (is_null($var)) return "NULL";
    if (is_numeric($var)) return "numeric";
    if (is_object($var)) return "object";
    if (is_resource($var)) return "resource";
    if (preg_match('/([0-9]{1,4}).([0-9]{1,2}).([0-9]{1,4}).?/',$var,$m)) {
        $date = array($m[1],$m[2],$m[3]);
        $y_i = array_keys($date, max($date));
        $year = array_slice($date,$y_i[0],1);
        $year = $year[0];
        if (checkdate($date[0], $date[1], $year))
            return "datum"; #"$year-{$date[0]}-{$date[1]}";
        else
            return "Invalid date";
    }
    if (is_string($var)) return "string";
    return "unknown";
}
/* upload.php 
 * This function return with the ul list of other_col,alter_names,date,quantity elements
 * argument
 * count: return with number of elements
 * array: return with associative array column=>short_name
 * columns: return with array of colum names
 * */
function dbcolist($control='',$table=PROJECTTABLE,$filter=array()) {
    global $BID,$GID,$ID; 

    //USING CACHE!!!
    $cache = obm_cache('get',"dbcolist.$table",'','',FALSE);

    if (isset($_SESSION['update_lang']))  {
        $cache = false;
        unset($_SESSION['update_lang']);
    }
    // automatic cache clean if page did not updated ??? 
    //if ($_SESSION['LANG_SET_TIME'] > time()-300)
    //    $cache = false;

    if ($cache) {
        if ($control=='count') 
            return count($cache);
        elseif($control=='columns')
            return array_keys($cache);
        else
            return $cache;
    }
    
    $st_col = st_col($table,'array');

    $H = array();
    $cmd = "SELECT column_name as key,short_name as value FROM project_metaname WHERE project_table='$table' ORDER BY column_name";
    $result = pg_query($BID,$cmd);
    //$skip_col = array();
    if (pg_num_rows($result)) {
        while($row=pg_fetch_assoc($result)) {
            //if(in_array($row['key'],$skip_col)) continue;

            if (preg_match('/^str_/',$row['value']))
                if (defined($row['value'])) {
                    $row['value'] = constant($row['value']);
                }
            $H[$row['key']]=$row['value'];
        }
    } else {
        $sp = preg_split('/\./',$table);
        if (count($sp) == 2) {
            $table = $sp[1];
            $schema = $sp[0];
        }
        // no assigned columns just registered table in header_names
        $cmd = sprintf('SELECT f_table_schema FROM header_names WHERE f_table_name=\'%1$s\' and f_main_table=%2$s',PROJECTTABLE,quote($table));
        $result = pg_query($BID,$cmd);
        if (pg_num_rows($result)) {
            $row = pg_fetch_assoc($result);
            $schema = $row['f_table_schema'];

            $cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_name = %s AND table_schema=%s",quote($table),quote($schema));
            // GID ???
            $result = pg_query($ID,$cmd);
            while($row=pg_fetch_assoc($result)) {
                $H[$row['column_name']]=$row['column_name'];
            }
        }
    }
    /* Species name: default names
     * only in the main table
    foreach($st_col['ALTERN_C'] as $k=>$v){
            if ($k=='') continue;
            if ($v=='sci') {
                if (preg_match('/^'.$table.'\.(\w+)/',$k,$m)) {
                    $k = $m[1];
                    $v = str_sci_name;
                    $H[$k]=$v;
                }
            }
            else {
                if (preg_match('/^'.$table.'\.(\w+)/',$k,$m)) {
                    $k = $m[1];
                    $v = str_alt_name;
                    $H[$k]=$v;
                }
            }
    }
    */

    // ordering
    $order_table = array();
    $order_col = json_decode($st_col['ORDER'],true);
    //{"ddnpi":{"faj":"10","magyar":"20","egyedszam":"30","szamossag":"31","dt_from":"40","dt_to":"50","time":"51","eov_x":"60","eov_y":"70","obm_geometry":"71","gyujto":"80","gyujto2":"90","gyujto3":"100","adatkozlo":"110","megj":"115","hatarozo":"120","kor":"130","him":"140","nosteny":"150","eloford_al":"160","modsz":"170","visszafogas":"180","hely":"190","elohely":"191","veszteny":"192","feszkeles":"218","gyuruszam":"219","szinesgyuru":"220","vedettseg_p":"229","natura_2000_p":"230","rogz_modsz":"240","rogz_dt":"241"}}
    if (is_array($order_col) and isset($order_col[$table]))
        $order_table = $order_col[$table];
    //{"faj":"10","magyar":"20","egyedszam":"30","szamossag":"31","dt_from":"40","dt_to":"50","time":"51","eov_x":"60","eov_y":"70","obm_geometry":"71","gyujto":"80","gyujto2":"90","gyujto3":"100","adatkozlo":"110","megj":"115","hatarozo":"120","kor":"130","him":"140","nosteny":"150","eloford_al":"160","modsz":"170","visszafogas":"180","hely":"190","elohely":"191","veszteny":"192","feszkeles":"218","gyuruszam":"219","szinesgyuru":"220","vedettseg_p":"229","natura_2000_p":"230","rogz_modsz":"240","rogz_dt":"241"}

    $ms = array();
    if (count(array_values($order_table))) {
        $cl = max(array_values($order_table))+1;
        foreach(array_keys($H) as $k) {
            $order = '';
            if (array_key_exists($k,$order_table)) {
                $order = $order_table[$k];
            }
            if($order!=='')
                $ms[] = $order;
            else
                $ms[] = $cl;
        }
        array_multisort($ms,$H);
    }

    obm_cache('set',"dbcolist.$table",$H,300,FALSE);
    
    if ($control=='count') return count($H);
    elseif ($control=='columns') return array_keys($H);
    elseif ($control=='array') return $H;
    else return false;
}
/*
 *
 *
 * */
function parse_csv($file, $options = null) {
    $delimiter = empty($options['delimiter']) ? "," : $options['delimiter'];
    $to_object = empty($options['to_object']) ? false : true;
    $str = file_get_contents($file);
    $lines = explode("\n", $str);
    //pr($lines);
    $field_names = explode($delimiter, array_shift($lines));
    foreach ($lines as $line) {
        // Skip the empty line
        if (empty($line)) continue;
        $fields = explode($delimiter, $line);
        $_res = $to_object ? new stdClass : array();
        foreach ($field_names as $key => $f) {
            if ($to_object) {
                $_res->{$f} = $fields[$key];
            } else {
                $_res[$f] = $fields[$key];
            }
        }
        $res[] = $_res;
    }
    return $res;
}

/* XML - GML processing functions
 * not used*/
function GML_coords_from_bounds($child,$namespaces) {
        $GML_Bounds = '';
        if ($child->getName() != 'boundedBy') return;
        //echo $child->getName() . " ";
        foreach($child->children($namespaces['gml']) as $boundedBy){
            //echo $boundedBy->getName() . " ";
            foreach($boundedBy->children($namespaces['gml']) as $coordinates){
                //echo $coordinates->getName() . ": $coordinates";
                $GML_Bounds .= $coordinates; 
            }
        }
        return $GML_Bounds;
}

/* XML - GML processing functions
 * coordinates from wfs 1.1.0 xml response*/
function GML_coords_from_points($child,$namespaces) {
        $GML_Bounds = '';
        //if ($child->getName() != 'Point') return;
        //echo $child->getName() . " ";
        foreach($child->children($namespaces['gml']) as $coordinates){
            //echo $coordinates->getName() . ": $coordinates";
            $GML_Bounds .= $coordinates; 
        }
        return $GML_Bounds;
}
/* XML - GML processing functions
 * coordinates from wfs 1.1.0 xml response*/
function GML_coords_from_Mpoints($child,$namespaces) {
        $GML_Bounds = array();
        //if ($child->getName() != 'MultiPoint') return;
        foreach($child->children($namespaces['gml']) as $pointMember)
            foreach ($pointMember as $pos)
                $GML_Bounds[] = $pos->{'pos'}->__toString();
        
        return $GML_Bounds;
}
/* create project specific taxon and history table
 * can be use for custom tables?
 * */
function Create_Project_plus_tables($table,$f,$owner) {
    global $ID;
    $send = '';

    $st_col = st_col($table,'array');


    if ($f == 'rules') {
        // Rules table
        $send = sprintf('
    SET statement_timeout = 0;
    SET lock_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s_rules (
        "data_table" character varying(128) NOT NULL,
        "row_id" integer NOT NULL,
        "owner" character varying(255),
        "read" integer[],
        "write" integer[],
        "sensitivity" character varying(10) DEFAULT \'public\'
    );
    ALTER TABLE %1$s_rules OWNER TO %2$s;
    ALTER TABLE ONLY %1$s_rules ADD CONSTRAINT %1$s_rules_ukey UNIQUE ("data_table", row_id);
    REVOKE ALL ON TABLE %1$s_rules FROM PUBLIC;
    REVOKE ALL ON TABLE %1$s_rules FROM %2$s;
    GRANT ALL ON TABLE %1$s_rules TO %2$s;',$table,$owner);


    } elseif ( $f == 'history' ) {
        // History Table 
        $send = sprintf('
    SET statement_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s_history (
        operation character(1) NOT NULL,
        hist_time timestamp without time zone NOT NULL,
        userid text NOT NULL,
        query text,
        row_id integer NOT NULL,
        hist_id integer NOT NULL,
        modifier_id integer,
        data_table character varying(64)
    );
    ALTER TABLE public.%1$s_history OWNER TO %2$s;
    CREATE SEQUENCE %1$s_history_hist_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    ALTER TABLE public.%1$s_history_hist_id_seq OWNER TO %2$s;
    ALTER SEQUENCE %1$s_history_hist_id_seq OWNED BY %1$s_history.hist_id;
    ALTER TABLE ONLY %1$s_history ALTER COLUMN hist_id SET DEFAULT nextval(\'%1$s_history_hist_id_seq\'::regclass);
    ALTER TABLE ONLY %1$s_history
        ADD CONSTRAINT %1$s_history_pkey PRIMARY KEY (hist_id);
    CREATE INDEX %1$s_history_row_id_idx ON %1$s_history USING btree (row_id);',$table,$owner);

    } else if ( $f == 'taxon' ) {
        // Taxon table
        $send = sprintf('
    SET statement_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s_taxon (
        meta character varying(254),
        word character varying(254),
        lang character varying(16),
        taxon_id integer NOT NULL,
        status numeric(1,0) DEFAULT 0,
        modifier_id integer DEFAULT 0,
        frequency integer DEFAULT 0,
        taxon_db integer DEFAULT 0
    );
    ALTER TABLE public.%1$s_taxon OWNER TO %2$s;
    CREATE SEQUENCE %1$s_taxon_taxon_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    ALTER TABLE public.%1$s_taxon_taxon_id_seq OWNER TO %2$s;
    ALTER SEQUENCE %1$s_taxon_taxon_id_seq OWNED BY %1$s_taxon.taxon_id;
    ALTER TABLE ONLY %1$s_taxon ALTER COLUMN taxon_id SET DEFAULT nextval(\'%1$s_taxon_taxon_id_seq\'::regclass);
    ALTER TABLE ONLY %1$s_taxon ADD CONSTRAINT %1$s_taxon_metaword_key UNIQUE (meta,word);
    CREATE INDEX %1$s_trgm_idx ON %1$s_taxon USING gist (meta gist_trgm_ops);
    CREATE INDEX %1$s_meta_idx ON %1$s_taxon USING btree (meta);
    CREATE INDEX %1$s_word_idx ON %1$s_taxon USING btree (word);',$table,$owner);

    }
    return $send;
}
/* create project specific taxon and history trigger functions
 * can be use for custom triggers???
 * */
function Create_Project_SQL_functions($table,$f,$owner) {
    global $ID;
    $send = '';

    $st_col = st_col($table,'array');

    if ($f == 'taxonlist_trigger') {

        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='taxon_update_%s' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s DISABLE TRIGGER taxon_update_%1$s',$table);

        } else {
            $send = sprintf('DROP TRIGGER IF EXISTS taxon_update_%1$s ON public.%1$s;
            CREATE TRIGGER taxon_update_%1$s BEFORE INSERT OR UPDATE ON %1$s FOR EACH ROW EXECUTE PROCEDURE update_%1$s_taxonlist();',$table);
        }
    } elseif ($f == 'taxonname_trigger') {
        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='%s_name_update' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s DISABLE TRIGGER %1$s_name_update',$table);

        } else {

            $send = sprintf('DROP TRIGGER IF EXISTS %1$s_name_update ON public.%1$s_taxon;
            CREATE TRIGGER %1$s_name_update AFTER UPDATE ON %1$s_taxon FOR EACH ROW EXECUTE PROCEDURE update_%1$s_taxonname();',$table);
        }
    } elseif ($f == 'history_trigger') {

        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='history_update_%s' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s DISABLE TRIGGER history_update_%1$s',$table);

        } else {

            $send = sprintf('DROP TRIGGER IF EXISTS history_update_%1$s ON public.%1$s;
            CREATE TRIGGER history_update_%1$s AFTER DELETE OR UPDATE ON %1$s FOR EACH ROW EXECUTE PROCEDURE history_%1$s();',$table);
        }
    } elseif ($f == 'rules_trigger') {

        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='rules_%s' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s DISABLE TRIGGER rules_%1$s',$table);

        } else {

            $send = sprintf('DROP TRIGGER IF EXISTS rules_%1$s ON public.%1$s;
            CREATE TRIGGER rules_%1$s AFTER DELETE OR UPDATE OR INSERT ON %1$s FOR EACH ROW EXECUTE PROCEDURE rules_%1$s();',$table);

        }
        // Create table if not exists
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE  table_schema = 'public' AND table_name = '%s_rules' )",$table);
        $result = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($result);
        if ($row['exists']=='f') {
            $cmd = Create_Project_plus_tables($table,'rules',gisdb_user);
            $result = pg_query($ID,$cmd);
        }

    } elseif ($f == 'taxonlist') {
        $SPECIES_C = $st_col['SPECIES_C'];
        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
            $SPECIES_C = $m[2];

        $send = sprintf('DROP FUNCTION IF EXISTS public.update_%1$s_taxonlist() cascade;
        CREATE OR REPLACE FUNCTION public.update_%1$s_taxonlist()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF tg_op = \'\'INSERT\'\' THEN
        INSERT INTO %1$s_taxon (meta,word,lang,modifier_id) 
            SELECT replace(new.%2$s,\'\' \'\',\'\'\'\'),new.%2$s,\'\'%2$s\'\',new.obm_modifier_id 
            WHERE NOT EXISTS ( 
                    SELECT taxon_id FROM %1$s_taxon WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=meta
            ) AND new.%2$s IS NOT NULL; ',$table,$st_col['SPECIES_C']);
        foreach($st_col['ALTERN_C'] as $column) {
            if (trim($column)=='') continue;
        
            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$column,$m))
            //    $column = $m[2];
            if ($SPECIES_C == $column) continue;

            $send.= sprintf('
        INSERT INTO %1$s_taxon (taxon_id,meta,word,lang,modifier_id)
        SELECT sq.taxon_id,replace(new.%3$s,\'\' \'\',\'\'\'\'),new.%3$s,\'\'%3$s\'\',new.obm_modifier_id
        FROM (
            SELECT DISTINCT ON (h.taxon_id) h.taxon_id 
            FROM %1$s_taxon s LEFT JOIN %1$s_taxon h ON s.taxon_id=h.taxon_id WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=s.meta) as sq
        WHERE NOT EXISTS (
            SELECT 1 FROM %1$s_taxon WHERE replace(new.%3$s,\'\' \'\',\'\'\'\')=meta
        ) AND new.%3$s IS NOT NULL;',$table,$SPECIES_C,$column);
            }
        $send.= sprintf('RETURN new;END IF;'); 
        $send .= sprintf('
    IF tg_op = \'\'UPDATE\'\' THEN
        INSERT INTO %1$s_taxon (meta,word,lang,modifier_id) 
            SELECT replace(new.%2$s,\'\' \'\',\'\'\'\'),new.%2$s,\'\'%2$s\'\',new.obm_modifier_id 
            WHERE NOT EXISTS ( 
                    SELECT taxon_id FROM %1$s_taxon WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=meta
            ) AND new.%2$s IS NOT NULL; ',$table,$st_col['SPECIES_C']);
        foreach($st_col['ALTERN_C'] as $column) {
            if (trim($column)=='') continue;
        
            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$column,$m))
            //    $column = $m[2];
            if ($SPECIES_C == $column) continue;

            $send.= sprintf('
        INSERT INTO %1$s_taxon (taxon_id,meta,word,lang,modifier_id)
        SELECT sq.taxon_id,replace(new.%3$s,\'\' \'\',\'\'\'\'),new.%3$s,\'\'%3$s\'\',new.obm_modifier_id
        FROM (
            SELECT DISTINCT ON (h.taxon_id) h.taxon_id 
            FROM %1$s_taxon s LEFT JOIN %1$s_taxon h ON s.taxon_id=h.taxon_id WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=s.meta) as sq
        WHERE NOT EXISTS (
            SELECT 1 FROM %1$s_taxon WHERE replace(new.%3$s,\'\' \'\',\'\'\'\')=meta
        ) AND new.%3$s IS NOT NULL;',$table,$SPECIES_C,$column);
        }
            $send.= sprintf('RETURN new;
    END IF;'); 

            $send.= sprintf('
END \' LANGUAGE plpgsql;');
            $send .= sprintf('ALTER FUNCTION update_%1$s_taxonlist() OWNER TO %2$s;',PROJECTTABLE,$owner);
    }  elseif($f=='taxonname') {
        $SPECIES_C = $st_col['SPECIES_C'];
        //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
        //    $SPECIES_C = $m[2];

        $send = sprintf('DROP FUNCTION IF EXISTS public.update_%3$s_taxonname() cascade;
        CREATE OR REPLACE FUNCTION public.update_%3$s_taxonname()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF tg_op = \'\'UPDATE\'\' THEN
        IF new.lang=\'\'%2$s\'\' THEN
            UPDATE %1$s SET %2$s=new.word,obm_modifier_id=new.modifier_id WHERE %1$s.%2$s=old.word;',$table,$SPECIES_C,PROJECTTABLE);
        foreach($st_col['ALTERN_C'] as $column) {
            if (trim($column)=='') continue;

            if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$column,$m))
                $column = $m[2];
            if ($SPECIES_C == $column) continue;

            $send .= sprintf('
        ELSE
            UPDATE %1$s SET %2$s=new.word,obm_modifier_id=new.modifier_id WHERE %1$s.%2$s=old.word;',$table,$column);
        }
$send .= sprintf('
        END IF; 
        RETURN new; 
    END IF; 
END \' LANGUAGE plpgsql;',$table);
    } elseif($f=='history') {
        $send = sprintf('DROP FUNCTION IF EXISTS public.history_%2$s() cascade;
        CREATE FUNCTION public.history_%2$s()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF (TG_OP = \'\'DELETE\'\') THEN
        INSERT INTO %1$s_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT \'\'D\'\', now(), user, CONCAT(OLD.*), OLD.obm_id, OLD.obm_modifier_id, \'\'%2$s\'\';
        RETURN OLD;
    ELSIF (TG_OP = \'\'UPDATE\'\') THEN
        INSERT INTO %1$s_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT \'\'U\'\', now(), user, CONCAT(NEW.*), OLD.obm_id, OLD.obm_modifier_id, \'\'%2$s\'\';
        RETURN NEW;
    END IF; 
END \' LANGUAGE plpgsql;',PROJECTTABLE,$table);

    } elseif($f=='rules') {
        $send = sprintf('DROP FUNCTION IF EXISTS public.rules_%1$s() cascade;
        CREATE FUNCTION public.rules_%1$s()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF (TG_OP = \'\'DELETE\'\') THEN
        DELETE FROM %1$s_rules WHERE row_id=OLD.obm_id ;
        RETURN OLD;
    ELSIF (TG_OP = \'\'UPDATE\'\') THEN
        RETURN NEW;
    ELSIF (TG_OP = \'\'INSERT\'\') THEN
        INSERT INTO %1$s_rules ("data_table",row_id,read,write) 
            SELECT \'\'%1$s\'\',NEW.obm_id,ARRAY(SELECT uploader_id 
            FROM system.uploadings WHERE uploadings.id=NEW.obm_uploading_id),ARRAY(SELECT uploader_id FROM system.uploadings 
            WHERE uploadings.id=NEW.obm_uploading_id);
        RETURN NEW;
    END IF; 
END \' LANGUAGE plpgsql;',$table);


    } elseif ($f=='main') {
        // kell ez valamire valahol???
        // main triggers
        $cmd = sprintf("SELECT event_object_table,trigger_name,event_manipulation,action_statement,action_timing 
                        FROM information_schema.triggers 
                        WHERE event_object_table=\'%1$s\' 
                        ORDER BY event_object_table,event_manipulation",$table);
        $result = pg_query($ID,$cmd);
        while($row=pg_fetch_assoc($result)) {
            print_r($row);
        }
        $send = sprintf('    
        ',$table);
    }

    return $send;
}

function file_upload($files) {
    $log = '';
    $e = array(
        0=>"There is no error, the file uploaded with success",
        1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
        2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
        3=>"The uploaded file was only partially uploaded",
        4=>"No file was uploaded",
        6=>"Missing a temporary folder"
    ); 
    $i = 0;
    if (is_array($files['error'])) {
        foreach ($files['error'] as $error) {
            if ($error) $log .= "Error: ".$files['name'][$i].": ".$e[$error];
            $i++;
        }
    } else {
        if ($files['error']) $log .= "Error: ".$files['name'].": ".$e[$error];
    }
    if ($log != '')
        log_action("FILE upload error: $log",__FILE__,__LINE__);
    
    return $log;
}
# create array from array
# args: a multidimensional array
# return: an other m.array
# used: not! 
function rearrange( $arr ){
    foreach( $arr as $key => $all ){
        foreach( $all as $i => $val ){
            $new[$i][$key] = $val;   
        }   
    }
    return $new;
}
/* read KML file
 * */
function readKML($xml,$out,$file='') {
    $data = array();

    /*$dom = new DOMDocument();
    $dom->loadXml($xml);

    $xpath = new DOMXpath($dom);
    $rootNamespace = $dom->lookupNamespaceUri($dom->namespaceURI);
    $xpath->registerNamespace('kml', 'http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd'); 

    $Placemark = $dom->getElementsByTagName('Placemark');
    debug($Placemark);
    return;*/


    //$xpath->registerNamespace('kml', 'http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd');
    //$xpath->registerNamespace('kml', 'http://schemas.opengis.net/kml/2.3/ogckml23.xsd');
    //$xpath->registerNamespace('kml', 'http://www.opengis.net/kml/2.2');
    //$places = $xpath->evaluate('//kml:Placemark', NULL, FALSE);

    //debug( $dom->saveXML() );
    //return;
    
    //foreach ($places as $place) {
    //
    
    //$kml = new SimpleXMLElement($xml);
    //$namespaces = $kml->getNamespaces(true);
    // debug($namespaces); {"":"http:\/\/earth.google.com\/kml\/2.2"}
    //$namespaces = $kml->getDocNamespaces();
    // debug($namespaces); {"":"http:\/\/earth.google.com\/kml\/2.2"}
    //$result = $kml->xpath('//Placemark');

    //while(list( , $node) = each($result)) {
    //    debug( '/Placemark: '.$node );
    //}
    //return;


    $kml = simplexml_load_string($xml,'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
    $path = array('Document'=>false,'Folder'=>false,'Placemark'=>false);

    // simple path evaluation
    // xpath not works with many kml, however it is strange and I can't understand why...
    foreach ($kml->children() as $child) {
        if ($child->getName() == 'Document') {
            $path['Document'] = true;
            foreach ($child->children() as $subchild) {
                if ($subchild->getName() == 'Folder') {
                    $path['Folder'] = true;

                    foreach ($subchild->children() as $subchild2) {
                        if ($subchild2->getName() == 'Placemark') {
                            $path['Placemark'] = true;
                        }
                    }
                }
                if ($subchild->getName() == 'Placemark') {
                    $path['Placemark'] = true;
                }
            }
        }
    }

    //$kml->registerXPathNamespace('kml', 'http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd');
    //$Placemark = $kml->xpath('//Document/Folder/Placemark');
    if ($path['Document'] and $path['Folder'] and $path['Placemark'])
        $Placemark = $kml->Document->Folder->Placemark;
    elseif ($path['Document'] and !$path['Folder'] and $path['Placemark'])
        $Placemark = $kml->Document->Placemark;


    foreach($Placemark as $place){

        
        $point = array();
        $lines = array();
        $linearring = array();

        if (isset($place->{'Point'})) {
            $point = preg_split('/,/',$place->{'Point'}->{'coordinates'}[0]);
        }
        elseif (isset($place->{'LineString'})) {
            $lines = preg_split('/\s/',$place->{'LineString'}->{'coordinates'}[0]);
        }
        elseif (isset($place->{'Polygon'})) {
            // it is deeper some path evaluation is necessaryy....
            // 	<outerBoundaryIs> <LinearRing> <coordinates>
            $linearring = preg_split('/\s/',$place->{'Polygon'}->outerBoundaryIs->LinearRing->{'coordinates'}[0]);
        }

        // xpath not works in many cases
        //$point = explode(',', $xpath->evaluate('string(kml:Point/kml:coordinates)', $place, FALSE), 3);
        //$line = array_filter(preg_split('/\s/', $xpath->evaluate('string(kml:LineString/kml:coordinates)', $place, FALSE)));
        //$linearring = array_filter(preg_split('/\s/', $xpath->evaluate('string(kml:Polygon//kml:coordinates)', $place, FALSE)));

        $wkt = "";
        //height not processed
        if (count($point)>1) {
            $wkt = "POINT ({$point[0]} {$point[1]})";
        }
        elseif (count($line)>1) {
            $linea = array();
            foreach ($line as $co) {
                $coi = explode(",",$co);
                $linea[] = $coi[0].' '.$coi[1];
            }
            $l = join(',',$linea);
            $wkt = "LINESTRING ($l)";
        }
        elseif (count($linearring)>1) {
            //multilinear string not processed in this version!!
            $linea = array();
            foreach ($linearring as $co) {
                $coi = explode(",",$co);
                $linea[] = $coi[0].' '.$coi[1];
            }
            $l = join(',',$linea);
            $wkt = "POLYGON (($l))";
        }
        
        $name = mb_convert_encoding($place->{'name'}[0], 'UTF-8', 'auto');
        
        if ($out=='obj') {

            $data[] = array(
                //'name' => $xpath->evaluate('string(kml:name)', $place, FALSE),
                'name' => $name,
                'wkt' => $wkt
            );
        }
        elseif ($out=='csv') {
            //$data[] = '"'.$wkt.'","'.$xpath->evaluate('string(kml:name)', $place, FALSE).'"';
            $data[] = '"'.$wkt.'","'.$name.'"';

        }
    }
        //var_dump($result);
        /*foreach ($xml->Document->Placemark as $coord) {
                    var_dump($coord);
                    $coord = (string) $coord->Point->coordinates."<br/>";
                    $args     = explode(",", $coord);
                    $coords[] = array($args[0], $args[1], $args[2]);
        }
        print_r($coords);*/
    return $data;
}
/* read SQLite / spatialite files 
 *
 * */
function readSQLITE($sqlite,$out,$file='',$header=false,$selectedLayer) {
    //only one layer supported actually!!!
    if (!command_exist('ogrinfo')) {
        log_action('ogrinfo not found',__FILE__,__LINE__);
        return false;
    }

    $data = array();
    $ret = exec("ogrinfo \"$sqlite\" |head -1|grep -o FAILURE");
#    $prjstring = exec("gdalsrsinfo \"$sqlite\" |grep -hoP 'PROJCS\[\"\w+'|sed -e 's/PROJCS\[\"//'");
#    if (in_array(chop($prjstring),array('HD72','HD72_EOV','HD_1972_Egyseges_Orszagos_Vetuleti','Hungarian_1972_Egyseges_Orszagos_Vetuleti'))) {
#        log_action("HD72 correction applied...",__FILE__,__LINE__);
#        $eov_srs = "-s_srs '+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +bounds=16.1200,45.7800,22.9100,48.6000'";
#    } else
#       $eov_srs = '';

    if ($ret!='') {
        log_action("read spatialite error: $ret",__FILE__,__LINE__);
        echo "<div class='error-message'>".str_shape_error."</div>"; //Ez itt vajon mit csinál?
        return;
    }
    if ($out=='csv') { 
        // SQLite layer selection
        if (isset($_SESSION['upload_file']['activeSheet'])) {
            $selectedLayer = $_SESSION['upload_file']['activeSheet'];
            $selectedLayer = preg_replace('/\d+:\s(.+)\s\([multipontyesrgc ]+\)$/i','$1',$selectedLayer);
        }

        if ($selectedLayer=='') {
            $output = array();
            $ret = exec("ogrinfo \"$sqlite\"", $output);
            // we are expecting an array like this:
            // ["INFO: Open of `\/tmp\/ob_phpoxsikk\/adatok.sqlite'","using driver `SQLite' successful.","1: rtm (Multi Point)"....
            // using the first layer
            
            $list_of_layers = $_SESSION['upload_file']['sheetList'] = array_slice($output,2);
            // 1: rtm (Multi Point)
            $selectedLayer = preg_replace('/\d+:\s(.+)\s\([multipontyesrgc ]+\)$/i','$1',$output[2]);
            // rtm
        } else {
            $output = array();
            $ret = exec("ogrinfo \"$sqlite\"", $output);
            $_SESSION['upload_file']['sheetList'] = array_slice($output,2);
        }
        
        # eov settings for ogr2ogr
        if ($file=='') 
        {
            //-dim 2 backward compatibility to do not process elevation
            //
            $fp = popen("ogr2ogr -f CSV /dev/stdout \"$sqlite\" -t_srs EPSG:4326 -sql \"select * from $selectedLayer\" -lco GEOMETRY=AS_WKT -dim 2", "r");
            #log_action("ogr2ogr -f CSV /dev/stdout $shp -t_srs EPSG:4326 -lco GEOMETRY=AS_WKT -dim 2 $eov_srs");
        } else {
            // a sleep  lehet, hogy kell
            exec("ogr2ogr -f CSV $file \"$sqlite\" -t_srs EPSG:4326 -sql \"select * from $selectedLayer\" -lco GEOMETRY=AS_WKT -dim 2; sleep 1");
            #debug("ogr2ogr -f CSV $file \"$sqlite\" -t_srs EPSG:4326 -sql \"select * from $selectedLayer\" -lco GEOMETRY=AS_WKT -dim 2; sleep 1");
            $fp = fopen($file,"r");
        }
        if (!is_resource($fp)) {
            log_action("ogr2ogr -f CSV ... execution failed.",__FILE__,__LINE__);
            echo "<div class='error-message'>ogr2ogr -f CSV ... execution failed.</div>";
            return;
        }
        $data = array();
        while (!feof($fp)) {
            $l = fgets($fp);
            //karakter kódolási problémák miatt nem biztos, hogy itt kell kezelni, esetleg ki lehet vinni php-ból...
            /*mb_internal_encoding('utf8');
            mb_regex_encoding('utf8');
            $l = mb_ereg_replace('^("[A-Z]+) \(','\\1(',$l);*/
            $data[] = chop($l); 
        }
        //log_action($data,__FILE__,__LINE__);
        //remove header!
        if (!$header)
            array_shift($data);

        if ($file=='')
            pclose($fp);
        else
            fclose($fp);
    }
    return $data;
}

# read a shape file
# args: shp path; output format; [output file]
# return:
# used: afuncs.php, read_upload_file.php
function readSHP($shp,$out,$file='',$header=false,$s_srs='') {
    //only one layer supported actually!!!
    //
    // check .prj exists! if not set -s_srs EPSG:4326
    //

    if (!command_exist('ogrinfo')) {
        log_action('ogrinfo not found',__FILE__,__LINE__);
        return false;
    }
    $err = exec("ogrinfo \"$shp\" |head -1|grep -o FAILURE");

    if ($err!='') {
        log_action("Read shape error: $ret",__FILE__,__LINE__);
        echo "<div class='error-message'>".str_shape_error."</div>";
        return false;
    }
    $err = exec("gdalsrsinfo \"$shp\" 2>&1");
    if (preg_match('/ERROR 1/',$err)) {

        if (!preg_match('/(\d+)$/',$s_srs,$m)) {
            log_action("Read shape error: $err",__FILE__,__LINE__);
            echo "<div class='error-message'>Can't transform coordinates, source layer has no coordinate system. Please add SRS info in 'Extra arguments' field, like [epsg:23700    ]</div>";
        }
    }

    $_SESSION['upload']['hd72-correction'] = 0;
    $eov_srs = '';
    $prjstring = exec("gdalsrsinfo \"$shp\" |grep -hoP 'PROJCS\[\"\w+'|sed -e 's/PROJCS\[\"//'");
    if (in_array(chop($prjstring),array('HD72','HD72_EOV','HD_1972_Egyseges_Orszagos_Vetuleti','Hungarian_1972_Egyseges_Orszagos_Vetuleti'))) {
        $_SESSION['upload']['hd72-correction'] = 1;
        log_action("HD72 correction applied...",__FILE__,__LINE__);
        $eov_srs = "-s_srs '+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +bounds=16.1200,45.7800,22.9100,48.6000'";
    }

    $data = array();
    
    // csv output
    if ($out=='csv') {
        if ( $s_srs != '' ) {
            if (preg_match('/(\d+)$/',$s_srs,$m)) {
                $s_srs = "-s_srs EPSG:".$m[1];
            }
        }

        //-skipfailures
        # eov settings for ogr2ogr
        if ($file=='') 
        {
            //-dim 2 backward compatibility to do not process elevation
            //
            $fp = popen("ogr2ogr -f CSV /dev/stdout \"$shp\" -t_srs EPSG:4326 $s_srs -lco GEOMETRY=AS_WKT -dim 2 $eov_srs","r");
             //log_action("ogr2ogr -f CSV /dev/stdout \"$shp\" -t_srs EPSG:4326 -lco GEOMETRY=AS_WKT -dim 2 $eov_srs");
        } else {
            // a sleep  lehet, hogy kell
            exec("ogr2ogr -f CSV $file \"$shp\" -t_srs EPSG:4326 $s_srs -lco GEOMETRY=AS_WKT -dim 2 $eov_srs;sleep 1");
            //log_action("ogr2ogr -f CSV $file \"$shp\" -t_srs EPSG:4326 $s_srs -lco GEOMETRY=AS_WKT -dim 2 $eov_srs",__FILE__,__LINE__);
            $fp = fopen($file,"r");
        }
        if (!is_resource($fp)) {
            log_action("ogr2ogr -f CSV ... execution failed.",__FILE__,__LINE__);
            echo "<div class='error-message'>ogr2ogr -f CSV ... execution failed.</div>";
            return false;
        }
        $data = array();
        while (!feof($fp)) {
            $l = fgets($fp);
            //karakter kódolási problémák miatt nem biztos, hogy itt kell kezelni, esetleg ki lehet vinni php-ból...
            /*mb_internal_encoding('utf8');
            mb_regex_encoding('utf8');
            $l = mb_ereg_replace('^("[A-Z]+) \(','\\1(',$l);*/
            if (chop($l)!='')
                $data[] = chop($l);
        }
        if (count($data)==0) {
            echo "<div class='error-message'>".str_shape_error.' Please check your shp file with this command: `ogr2ogr -f CSV teszt.csv "something.shp" -t_srs EPSG:4326 -lco GEOMETRY=AS_WKT -dim 2 '.$eov_srs.'`</div>';
            return false;
        }
        //remove header!
        if (!$header)
            array_shift($data);

        if ($file=='')
            pclose($fp);
        else
            fclose($fp);
    }
    // Postgres output
    if ($out=='pgsql') {
        $p = popen("shp2pgsql -I -s 4326 -W LATIN1 \"$shp\" upload_temp","r");
        $data = "";
        while ($l = fgets($p)){ $data[] = $l; }
        pclose($p);
    }

    // Shape data as memory object using a php lib:
    // https://packagist.org/packages/phpmyadmin/shapefile
    // it is an experimental code, not used anywhere!
    // should be installed...
    // composer require phpmyadmin/shapefile
    // To be able to read and write the associated DBF file, you need dbase extension:
    // pecl install dbase
    // echo "extension=dbase.so" > /etc/php5/conf.d/dbase.ini
    // 
    if ($out=='obj') {

        require_once 'vendor/autoload.php';
        $shp_obj = new ShapeFile(1);
        $shp_obj->loadFromFile($shp);
        $i = 1;
        $data = array();
        foreach ($shp_obj->records as $i => $record) {
            $shp_data = $record->SHPData;
            $dbf_data = $record->DBFData;
            $data[] = array($shp_data,$dbf_data);
        }
        # returning with a multidim.array
        # https://www.phpclasses.org/package/1741-PHP-Read-vectorial-data-from-geographic-shape-files.html
        /*include_once(getenv('OB_LIB_DIR').'ShapeFile.inc.php');
        $shp = new ShapeFile($shp, array('noparts'=>false));
    	$i = 0;
        $data = array();
        while ($record = $shp->getNext()) {
        	$dbf_data = $record->getDbfData();
	    	$shp_data = $record->getShpData();
	        $data[] = array($shp_data,$dbf_data);
        }*/
    }
    return $data;
}
/* read JSON file */
function readJSON ($inputFileName) {
    $file = new SplFileObject($inputFileName);
    $sheetData = array();
    
    $line = '';
    // Loop until we reach the end of the file.
    while (!$file->eof()) {
        // Echo one line from the file.
        $line .= $file->fgets();
    }
    $foo = (array) json_decode($line);

    $columns = array_keys($foo);
    $rowcount = 0;
    if (is_array($foo[$columns[0]]))
        $rowcount = count($foo[$columns[0]]);

    // transform array to an other array type:
    /*
     * read simple format 
    {
        "asd": [1,2],
        "dsfg": [10,11],
        "fgh": [3,6]
    }
    write redundant format
    [
        {"asd":"1","dsgf":"10","fgh":"3"},
        {"asd":"2","dsgf":"11","fgh":"6"}
    ]
     */ 
    for($i=0;$i<$rowcount;$i++) {
        $put = array();
        foreach($foo as $key=>$value) {
            # {"asd":"1","dsgf":"10","fgh":"3"},
            if (is_array($value)) {
                $put[$key] = $value[$i];
            }
        }
        $sheetData[] = $put;
    }
    // Unset the file to call __destruct(), closing the file handle.
    $file = null;

    return $sheetData;
}

/* read Fasta file */
function readFASTA ($inputFileName,$sep) {
    $finfo = new finfo(FILEINFO_MIME);
    $mimetype = $finfo->file($inputFileName);
    $arr = preg_split('/;/',$mimetype);
    if(isset($arr[0]) and $arr[0]=='text/plain') {
    
    }
    $file = new SplFileObject($inputFileName);
    $sheetData = array();

    // Loop until we reach the end of the file.
    while (!$file->eof()) {
        // Echo one line from the file.
        $line = $file->fgets();
        // line processing should be coming from spcific modules!

        // Barbara's special put to a module!!!
        # >GWY_1_(reversed) 
        if (preg_match('/^>(.+)/',$line,$m)) {
            $id_blocks = preg_split("/$sep/",$m[1]);
            //$triplecode = $individual_id = $other_things = null;
            /*
            if (count($m>=2)) {
                $triplecode = $m[1];
                $individual_id = $m[2];
                if (isset($m[3]))
                    $other_things = $m[3];
            }*/
            //$put = array('triplecode'=>$triplecode,'inidiv_id'=>$individual_id,'other_comm'=>$other_things);
            //
            $gi = count($id_blocks);
            $letter = 'A';
            for($i=0;$i<$gi;$i++) {
                $put[$letter] = $id_blocks[$i];
                ++$letter;
            }

            $sheetData[] = $put;
        }
    }
    // Unset the file to call __destruct(), closing the file handle.
    $file = null;
    return $sheetData;

}

/* readCSV*/
function readCSV ($inputFileName,$bom = null) {

    $finfo = new finfo(FILEINFO_MIME);

    $mimetype = $finfo->file($inputFileName);
    $arr = preg_split('/;/',$mimetype);
    if(isset($arr[0]) and $arr[0]=='text/plain') {
        # text, csv
        $charset = '';
        if(isset($arr[1])) {
            $brr = preg_split('/=/',$arr[1]);
            if (isset($brr[1])) $charset = $brr[1];
        }
            
        if (strtolower($charset) != 'utf-8' and $charset!='') {
            //$text = file_get_contents($_FILES["file"]["tmp_name"][$i]);
            //setlocale(LC_ALL, 'hu_HU.UTF8');
            //$ttext = iconv($charset, "UTF-8//IGNORE", $text);
            system("iconv -f $charset -t utf-8 $inputFileName > $inputFileName-utf8", $retval);
            if ($retval !== 0) {
                log_action('ICONV failed.',__FILE__,__LINE__);
                unlink("$inputFileName-utf8");
                $out = system("iconv -l | grep -i $charset");
                // unknown character set
                if ($out=="") {
                    // converting any to utf-8 but dropping unknown characters
                    // keep utf8 characters
                    log_action("ICONV: Silently discard characters that cannot be converted instead of terminating when encountering such characters.",__FILE__,__LINE__);
                    system("iconv -f utf8 -t utf-8 $inputFileName -c > $inputFileName-utf8", $retval);
                }
            }

            if ($charset == 'utf-16le') $bom = 'nobom';
            
            // delete UTF-8 BOM:
            if ($bom == 'nobom') {
                system("awk 'NR==1{sub(/^\xef\xbb\xbf/,\"\")}1' $inputFileName-utf8 > $inputFileName-utf8-nobom ");
                rename("$inputFileName-utf8-nobom","$inputFileName");
            }
            else
                rename("$inputFileName-utf8","$inputFileName");

            #ini_set('mbstring.substitute_character', "none");
            #$ttext= mb_convert_encoding($text, 'UTF-8',$charset);
            #$ttext = mb_convert_encoding($text, "UTF-8", mb_detect_encoding($text, "UTF-8, ISO-8859-1, ISO-8859-15", true));
            #file_put_contents($_FILES["file"]["tmp_name"][$i], $ttext);
        }
            
    }

    /** Include path **/
    set_include_path(get_include_path() . PATH_SEPARATOR . '../../libs/');

    /** parsecsv https://code.google.com/p/parsecsv-for-php */
    //include_once 'parsecsv.lib.php';
    //require 'parsecsv-for-php-master/parsecsv.lib.php';
    require_once('vendor/autoload.php');

    if (file_exists($inputFileName) and filesize($inputFileName)) {
        # handling # comment lines at the beginning of csv files as file comment?

        //$csv = new parseCSV();
        $csv = new ParseCsv\Csv();
        //manual encoding & delimeter:
        //$csv->delimiter = "\t";
        //$csv->encoding('UTF-16', 'UTF-8');
        //$csv->parse($inputFileName);
        
        //change header line feeding 
        if(isset($_SESSION['feed_header_line']) and $_SESSION['feed_header_line']=='no')
            $csv->heading=FALSE;
        //useing: $csv->titles
        //auto-detect delimiter character
        $csv->auto($inputFileName);
        $n = 0;
        foreach($csv->data as $k) {
            if(isset($k['WKT']))
                $csv->data[$n]['WKT'] = preg_replace('/^([A-Z]+) \(/','$1(',$k['WKT']);
            $n++;
        }
        $sheetData = $csv->data;

        $loop_update = 0;
        if (isset($_SESSION['character_encoding']) and preg_match('/(cp|windows-)?1252/i',$_SESSION['character_encoding'])) {
            $ascii = array();
            $ok = array();
            // további betvek kellhetnek!!!
            $ascii[251] = 'û'; //ű
            $ascii[245] = 'õ';
            $ascii[244] = 'ô';
            $ok[251] = 'ű'; //ű
            $ok[245] = 'ő'; //ő
            $ok[244] = 'ő'; //ő
            $loop_update = 1;
        }
        if (isset($_SESSION['decimal']) and $_SESSION['decimal']!='.') {
            $loop_update = 2;
        }
        if ($loop_update) {
            //$sheetData[0] = str_replace($ascii, $ok, $sheetData[0]);
            //print_r( $sheetData[0]);
            $cnt = count($sheetData);
            for($i=0;$i<$cnt;$i++) {
                foreach($sheetData[$i] as $key=>$val) {
                    if ($loop_update == 1)
                        $sheetData[$i][$key] = str_replace($ascii, $ok, $val);
                    else {
                        $sheetData[$i][$key] = preg_replace("/(\d+){$_SESSION['decimal']}(\d+)/",'$1.$2', $val);
                    }
                }
            }
        }
        return $sheetData;

        #$sheetData = parse_csv($inputFileName, array("delimiter"=>";"));
    } else {
        return 0;
    }
}
/* Create a temporay directory
 * */
function tempdir($dir=false,$prefix='ob_php') {
    $tempfile=tempnam(sys_get_temp_dir(),$prefix);
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}
//array walk internal function
//in results_builder.php
function prefix(&$item1, $key, $prefix)
{
    if (!preg_match("/^$prefix\./",$item1))
        $item1 = "$prefix.$item1";
}

/* Check given process is runnning
 * session based check would be desired?
 * */
function is_process_running($PID) {
    exec("ps $PID", $ProcessState);
    return(count($ProcessState) >= 2);
}
/* Run shell process in background
 * */
function run_in_background($Command, $Priority=0,$redirect='null') {
    if ($redirect==='null') $null = "2> /dev/null";
    else $null = "";

    if($Priority)
        $PID = shell_exec("nohup nice -n $Priority $Command $null & echo $!");
    else
        $PID = shell_exec("nohup $Command $null & echo $!");

    return($PID);
}
/* return given WKT string as geojson
 * */
function wkt_to_geojson ($text) {
    //set_include_path(get_include_path() . PATH_SEPARATOR . '../../libs/');
    require_once('vendor/autoload.php');
    //include_once ('gisconverter.php');
    $decoder = new gisconverter\WKT();
    return $decoder->geomFromText($text)->toGeoJSON();
}
/* return given geojson as WKT string
 * */
function geojson_to_wkt ($text) {
    require_once('vendor/autoload.php');
    //include_once ('gisconverter.php');
    $decoder = new gisconverter\GeoJSON();
    return $decoder->geomFromText($text)->toWKT();
}
function json_to_wkt($json) {
    require_once('vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    $geom = geoPHP::load($json,'json');
    if ($geom)
        return $geom->out('wkt');
    else
        return '';
}
function wkt_to_json($wkt) {
    require_once('vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    $geom = geoPHP::load($wkt,'wkt');
    if($geom)
        return $geom->out('json');
    else
        return '';
}
//wkt string validation
function is_wkt($wkt) {
    require_once('vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    try {
        $geom = geoPHP::load($wkt,'wkt');
        return true; # output geometry in GeoJSON format
    } catch (InvalidText $itex) {
        //"WKT not well formed";
        return false;
    } catch (Exception $ex) {
        //"General exception.";
        return false;
    }
} 
//wkt string validation
function is_geojson($wkt) {
    require_once('vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    try {
        $geom = geoPHP::load($wkt,'json');
        return true; # output geometry in GeoJSON format
    } catch (InvalidText $itex) {
        //"WKT not well formed";
        return false;
    } catch (Exception $ex) {
        //"General exception.";
        return false;
    }
} 
function language_file_process() {
    $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
    if(!isset($_SESSION['skip_local_lang'])) {
        $rr['err'][] = "Turn off the local language files first!";
        return $rr;
    }


    //$dest = sys_get_temp_dir()."/ob_lang_upl_".session_id();
    $r = move_upl_files($_FILES['files']);
    $rj = json_decode($r);

    $files = (array) $rj->{'file_names'};

    #{"file_names":["hu.csv"],"err":[],"sum":["0065e0e713d4e2e230a2fc72da81f21719dc753f"],"tmp":"\/tmp\/ob_phpvMsJM1\/","type":["text\/plain; charset=us-ascii"]}
    for($i=0;$i<count($files);$i++) {
        $name = $files[$i];
        $file = $rj->{'tmp'}.$files[$i];

        if($s = readCSV($file)) {
            // file name
            $m = array();
            if(preg_match('/(\w+)\.csv/',$name,$m)) {
                $filename = sprintf("%slanguages/local_%2s.php",getenv('PROJECT_DIR'),$m[1]);
            } else {
                $rr['err'][] = "File name should be xx.csv, where xx is the languge code!";
                return $rr;
            }
            
            // check lang_code turned off
            if(!isset($_SESSION['skip_local_lang'][$m[1]])) {
                $rr['err'][] = "Turn off the local $m[1] language defintion first!";
                return $rr;
            }

            // file content
            $out = array();
            foreach ($s as $line) {
                //echo $line['definition'].' '.$line['value'];
                if(!isset($line['definition'])) {
                    $wrongline = array_values($line);
                    $line = array();
                    $line['definition'] = $wrongline[0];
                    $line['value'] = $wrongline[1];
                }
                if(!isset($line['value'])) {
                    $wrongline = array_values($line);
                    $line = array();
                    $line['definition'] = $wrongline[0];
                    $line['value'] = $wrongline[1];
                }
                $d = trim($line['definition'],"'");
                $d = trim($d,'"');
                $v = trim($line['value'],"'");
                $v = trim($v,'"');

                if (trim($d)!='') {
                    if(defined($d)) $rr['err'][] = "$d already defined, skipped!";
                    else $out[trim($d)] = $v;
                }
            }
            if (!count($out)) {
                $rr['err'][] = "Empty processed array, might be wrong csv syntax!";
                return $rr;
            }
            
            // create language definition file
            if(!create_define_file($filename,$out)) {
                $rr['err'][] = "File creating error!";
                return $rr;
            }
            unset($_SESSION['skip_local_lang']);
            $rr['file_names'][] = sprintf("local %2s language defintions created",$m[1]);
        }
        else $rr['err'][] = "Read csv error!";
    }
    return $rr;
}
/* Move uploaded files to a temp dir
 * Returning: an array: 'temp dir name','file name','errors'
 * should call after: rm tmp dir!!
 * */
function move_upl_files($F,$dest='') {
    //Check disk free space...
    //disk_free_space ( string $directory )
    $r = array('file_names'=>array(),'err'=>array(),'sum'=>array(),'tmp'=>'','type'=>array()); 
    if ($dest=='') {
        $t = tempdir();
        if (!$t) {
            $r['err'][] = "Could not create temp directory: $t";
            return json_encode($r);
        }
    } else {
        if ( !file_exists($dest) ) 
            mkdir($dest);
        
        if ( is_dir($dest) and is_writable($dest) ) {
            $t = $dest;
        } else {
            $r['err'][] = "Destination directory is not exist or not writable: $dest";
            return json_encode($r);
        }
    }
    $t = "$t/";
    $r['tmp'] = $t;

    if (isset($F["tmp_name"]) and is_array($F["tmp_name"])) {
        for ($i=0;$i<count($F["tmp_name"]);$i++) {

            $finfo = new finfo(FILEINFO_MIME);
            $r['type'][] = $finfo->file($F["tmp_name"][$i]);

            $sha1 = sha1_file($F['tmp_name'][$i]);
            $r['sum'][] = $sha1;
            $cnam = check_file_exist($F['name'][$i],$t,$F['tmp_name'][$i],$sha1);
            if ($cnam) {
                if (is_uploaded_file($F["tmp_name"][$i])) {
                    if ( move_uploaded_file($F["tmp_name"][$i], "$t".$cnam)) {
                        $r['file_names'][] = $cnam;
                    } else {
                        //echo $F["error"][$i];
                        $e = '';
                        switch ($F['error'][$i]) {
                            case UPLOAD_ERR_OK:
                                $e = 'Ok.';
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                $e = 'No file sent.';
                                break;
                            case UPLOAD_ERR_INI_SIZE:
                                $e = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
                                break;
                            case UPLOAD_ERR_FORM_SIZE:
                                $e = 'Exceeded filesize limit.';
                                break;
                            default:
                                $e = 'Unknown errors.';
                                break;
                        }
                        $r['err'][] = "Can't move to desnitaion '".$F['name'][$i]."' - $e";
                    }
                } else {
                    // unpacked or other way placed files /e.g. reloaded page/
                    // tmp_dir basename check!!!
                    if (copy($F["tmp_name"][$i], "$t".$cnam)) {
                        unlink($F["tmp_name"][$i]);
                        $r['file_names'][] = $F['name'][$i];
                    } else
                        $r['err'][] = "Can't copied file to desnitaion.";
                }
            } else {
                $r['file_names'][] = $F['name'][$i];
                $r['err'][] = "File already exists";
            }
        }
    } else if (isset($F["tmp_name"])) {
        $sha1 = sha1_file($F['tmp_name']);
        $r['sum'][] = $sha1;

        $finfo = new finfo(FILEINFO_MIME);
        $r['type'][] = $finfo->file($F["tmp_name"]);
        $cnam = check_file_exist($F['name'],$t,$F['tmp_name'],$sha1);
        if($cnam) {
            if(move_uploaded_file($F["tmp_name"], "$t".$cnam)) {
                $r['file_names'][] = $cnam;
            } else {
                $r['err'][] = "Move file to desnitaion error: ".$F['name'].".";
            }
        } else {
            $r['file_names'][] = $F['name'];
            $r['err'][] = "File already exists";
        }
    }
    return json_encode($r);
}
function check_file_exist($origin, $dest, $tmp_name, $tmp_sum)
{
    $lc = new LocaleManager();
    $lc->doBackup();
    $lc->fixLocale();
    $origin = basename($origin);
    $lc->doRestore();
    $fulldest = $dest.$origin;
    $filename = $origin;

    if (file_exists($fulldest) and sha1_file($fulldest) == $tmp_sum) {
        return 0;
    }
    mb_internal_encoding("UTF-8");
    for ($i=1; file_exists($fulldest); $i++)
    {
        $fileext = (mb_strpos($origin,'.')===false?'':'.'.mb_substr(strrchr($origin, "."), 1));
        $filename = mb_substr($origin, 0, mb_strlen($origin)-mb_strlen($fileext)).'['.$i.']'.$fileext;
        $fulldest = $dest.$filename;
        if (file_exists($fulldest) and sha1_file($fulldest) == $tmp_sum) {
            return $filename;
        }
    }
    return $filename;
}
class LocaleManager
{
    /** @var array */
    private $backup;


    public function doBackup()
    {
        $this->backup = array();
        $localeSettings = setlocale(LC_ALL, 0);
        if (strpos($localeSettings, ";") === false)
        {
            $this->backup["LC_ALL"] = $localeSettings;
        }
        // If any of the locales differs, then setlocale() returns all the locales separated by semicolon
        // Eg: LC_CTYPE=it_IT.UTF-8;LC_NUMERIC=C;LC_TIME=C;...
        else
        {
            $locales = explode(";", $localeSettings);
            foreach ($locales as $locale)
            {
                list ($key, $value) = explode("=", $locale);
                $this->backup[$key] = $value;
            }
        }
    }


    public function doRestore()
    {
        foreach ($this->backup as $key => $value)
        {
            setlocale(constant($key), $value);
        }
    }


    public function fixLocale()
    {
        setlocale(LC_ALL, "C.UTF-8");
    }
}
function check_file_uploaded_name ($filename)
{
    (bool) ((preg_match("`^[-0-9A-Z_\.]+$`i",$filename)) ? true : false);
}
function check_file_uploaded_length ($filename)
{
    return (bool) ((mb_strlen($filename,"UTF-8") > 225) ? true : false);
}
/* utf8 ucfirst
 * glue arbitray arguments into a string
 * */
function t() {
    $encoding='UTF-8';
    mb_internal_encoding($encoding);
    $s = func_get_args();
    $string = implode(' ',$s);
    if ($string=='') {
        return "<i>".str_undefined."</i>";
    }
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    if ($firstChar!='')
        return mb_strtoupper($firstChar, $encoding) . $then;
    else {
        if (is_array($s)) return "";
        else return $s;
    }
}
/* Validate JSON
 * used in geomtest.php
 * */
function is_json($str){ 
    return json_decode($str) != null;
}
// pretty json format
// https://gist.github.com/GloryFish/1045396
function format_json($json, $html = false, $tabspaces = null) {
    $tabcount = 0;
    $result = '';
    $inquote = false;
    $ignorenext = false;

    if ($html) {
        $tab = str_repeat("&nbsp;", ($tabspaces == null ? 4 : $tabspaces));
        $newline = "<br/>";
    } else {
        $tab = ($tabspaces == null ? "\t" : str_repeat(" ", $tabspaces));
        $newline = "\n";
    }

    for($i = 0; $i < strlen($json); $i++) {
        $char = $json[$i];

        if ($ignorenext) {
            $result .= $char;
            $ignorenext = false;
        } else {
            switch($char) {
                case ':':
                    $result .= $char . (!$inquote ? " " : "");
                    break;
                case '{':
                    if (!$inquote) {
                        $tabcount++;
                        $result .= $char . $newline . str_repeat($tab, $tabcount);
                    }
                    else {
                        $result .= $char;
                    }
                    break;
                case '}':
                    if (!$inquote) {
                        $tabcount--;
                        $result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char;
                    }
                    else {
                        $result .= $char;
                    }
                    break;
                case ',':
                    if (!$inquote) {
                        $result .= $char . $newline . str_repeat($tab, $tabcount);
                    }
                    else {
                        $result .= $char;
                    }
                    break;
                case '"':
                    $inquote = !$inquote;
                    $result .= $char;
                    break;
                case '\\':
                    if ($inquote) $ignorenext = true;
                    $result .= $char;
                    break;
                default:
                    $result .= $char;
            }
        }
    }
    return $result;
}
//validate session with projecttable
function vreq() {
    if (isset($_SESSION['token'])) {
        if (isset($_SESSION['token']['projecttable']) and $_SESSION['token']['projecttable']==PROJECTTABLE)
            if (isset($_SESSION['token']['session_id']) and $_SESSION['token']['session_id']==session_id())
                return true;
    }
    return false;
}
/* http://php.net/manual/en/function.php-check-syntax.php
 *
 * */
function php_check_syntax( $php, $isFile=false )
{
    # Get the string tokens
    $tokens = token_get_all( '<?php '.trim( $php  ));
   
    # Drop our manually entered opening tag
    array_shift( $tokens );
    token_fix( $tokens );

    # Check to see how we need to proceed
    # prepare the string for parsing
    if( isset( $tokens[0][0] ) && $tokens[0][0] === T_OPEN_TAG )
       $evalStr = $php;
    else
        $evalStr = "<?php\n{$php}?>";

    if( $isFile OR ( $tf = tempnam( NULL, 'parse-' ) AND file_put_contents( $tf, $php ) !== FALSE ) AND $tf = $php )
    {
        # Prevent output
        $output = shell_exec('php -l "'.$php.'"');

        if( !preg_match('/^No syntax/',$output))
        {
            return false;
        }
        return true;
    }
    return false;
}

//fixes related bugs: 29761, 34782 => token_get_all returns <?php NOT as T_OPEN_TAG
function token_fix( &$tokens ) {
    if (!is_array($tokens) || (count($tokens)<2)) {
        return;
    }
   //return of no fixing needed
    if (is_array($tokens[0]) && (($tokens[0][0]==T_OPEN_TAG) || ($tokens[0][0]==T_OPEN_TAG_WITH_ECHO)) ) {
        return;
    }
    //continue
    $p1 = (is_array($tokens[0])?$tokens[0][1]:$tokens[0]);
    $p2 = (is_array($tokens[1])?$tokens[1][1]:$tokens[1]);
    $p3 = '';

    if (($p1.$p2 == '<?') || ($p1.$p2 == '<%')) {
        $type = ($p2=='?')?T_OPEN_TAG:T_OPEN_TAG_WITH_ECHO;
        $del = 2;
        //update token type for 3rd part?
        if (count($tokens)>2) {
            $p3 = is_array($tokens[2])?$tokens[2][1]:$tokens[2];
            $del = (($p3=='php') || ($p3=='='))?3:2;
            $type = ($p3=='=')?T_OPEN_TAG_WITH_ECHO:$type;
        }
        //rebuild erroneous token
        $temp = array($type, $p1.$p2.$p3);
        if (version_compare(phpversion(), '5.2.2', '<' )===false)
            $temp[] = isset($tokens[0][2])?$tokens[0][2]:'unknown';

        //rebuild
        $tokens[1] = '';
        if ($del==3) $tokens[2]='';
        $tokens[0] = $temp;
    }
    return;
}
/* proxy or wfs...
 * not used?
 * */
function getUrlContents($url,$file1)
{
   $url_parsed = parse_url($url);

   $host = $url_parsed["host"];
   if ($url == '' || $host == '') {
       return false;
   }
   $port = 80;
   //$path = (empty($url_parsed["path"]) ? '/' : $url_parsed["path"]);
   //$path.= (!empty($url_parsed["query"]) ? '?'.$url_parsed["query"] : '');
   $out = "GET $path HTTP/1.0\r\nHost: $host\r\nConnection: Close\r\n\r\n";
   $fp = fsockopen($host, $port, $errno, $errstr, 30);
   fwrite($fp, $out);
   $headers = '';
   $content = '';
   $buf = '';
   $isBody = false;
   while (!feof($fp) and !$isBody) {
          $buf = fgets($fp, 1024);
          if ($buf == "\r\n" ) {$isBody = true;}
          else{$headers .= $buf;}
   }
   //$file1 = fopen(basename($url_parsed["path"]), 'w');
   $bytes=stream_copy_to_stream($fp,$file1);
   fclose($fp);
   return $bytes;
}
/* news stream..
 *
 * */
function insertNews($message,$level,$sender=0,$receiver=0) {
    global $BID;
    if ($level=='project') $l = 'project';
    elseif ($level=='public') $l = 'public';
    elseif ($level=='personal') $l = 'personal';
    else return;

    $cmd = sprintf("INSERT INTO project_news_stream (datum,news,uploader,project_table,level,receiver) VALUES(NOW(),%s,%d,'%s','%s',%d) RETURNING id",quote("$message"),$sender,PROJECTTABLE,$l,$receiver);
    pg_query($BID,$cmd);
    // no response
}
function rainbow($start=array(0,0,0),$end=array(0,0,0),$steps=1) {

    $colors=array(hexcolorFromArraycolor($start)); //You want the start color to be part of the result array
    $intervals=$steps-1; //You want 3 steps to mean 2 intervals

    $current=$start;
    if ($intervals>0) $delta=array(
       ($end[0]-$start[0])/$intervals,
       ($end[1]-$start[1])/$intervals,
       ($end[2]-$start[2])/$intervals);
    else $delta = array($end[0]-$start[0],$end[1]-$start[1],$end[2]-$start[2]);

    for ($i=1;$i<$intervals;$i++) {
        $current=array($current[0]+$delta[0],$current[1]+$delta[1],$current[2]+$delta[2]);
        $colors[]=hexcolorFromArraycolor(array(round($current[0]),round($current[1]),round($current[2])));
    }

    $colors[]=hexcolorFromArraycolor($end); //You want the end color to be part of the result array
    return $colors;
    //return hexcolorFromArraycolor($colors);
}
function hexcolorFromArraycolor($arraycolor) {
    return '#'
    .substr('0'.dechex($arraycolor[0]),-2)
    .substr('0'.dechex($arraycolor[1]),-2)
    .substr('0'.dechex($arraycolor[2]),-2);
}
function mkThumb($image,$thumbWidth,$thumbHeight=0) {
    # create or not a thumbnail from an image
    #
    $photo_folder = getenv('PROJECT_DIR').'attached_files';
    $thumbnail_folder = getenv('PROJECT_DIR').'attached_files/thumbnails';
    
    $pattern = sprintf("%s/%dx[0-9]+_%s*",$thumbnail_folder,$thumbWidth,$image);
    $list = glob($pattern);
    foreach ($list as $l) {
        #should be localized
        #$lc = new LocaleManager();
        #$lc->doBackup();
        #$lc->fixLocale();
        return basename($l);
        #$lc->doRestore();
    }
    // get extension
    $is_image_by_ext = 0;
    $ext = '';
    if (preg_match('/\.(\w{1,4})$/i',$image,$m)) {
        $images = array('jpg','jpeg','gif','png');
        $ext = strtolower($m[1]);
        if (in_array(strtolower($m[1]),$images)) $is_image_by_ext = 1;
    }
    
    //check is this processable image?
    if($is_image_by_ext) {
        if(false !== (list($width,$height) = @getimagesize($photo_folder."/".$image))){
            if ($thumbHeight==0) {
                $new_width = $thumbWidth;
                $new_height = floor( $height * ( $thumbWidth / $width ) );
            } else {
                $new_height = $thumbHeight;
                $new_width = floor( $width * ( $thumbHeight / $height ) );    
            }

            $output = sprintf("%s/%dx%d_%s",$thumbnail_folder,$new_width,$new_height,$image);
            if (file_exists($output))
                return sprintf("%dx%d_%s",$new_width,$new_height,$image);

            if($ext == 'jpg' or $ext == 'jpeg')
                $source = open_image($photo_folder."/".$image);
            elseif($ext == 'png')
                $source = open_image($photo_folder."/".$image);
            elseif($ext == 'gif')
                $source = open_image($photo_folder."/".$image);

            if ($source!==false) {
                $thumb = imagecreatetruecolor($new_width,$new_height);
                imagecopyresampled($thumb,$source,0,0,0,0,$new_width,$new_height,$width,$height);
                imagejpeg($thumb,$output);
                imagedestroy($thumb);
                return sprintf("%dx%d_%s",$new_width,$new_height,$image);
            } else 
                return false;
        }
    } else {
        //not an image -- ?pdf
        if ($ext=='pdf') {
            $finfo = new finfo(FILEINFO_MIME);
            $mimetype = $finfo->file($photo_folder."/".$image);
            if (preg_match('/application\/pdf/',$mimetype)) {
                if ($thumbWidth==600) {
                    $new_width = 400;
                    $new_height = 600;
                } else {
                    $new_width = 60;
                    $new_height = 60;
                }
                // php 7.2 fails to use...
                if (class_exists('imagick')) {
                    $im = new imagick();
                    $im->setResolution(100, 100);
                    //$im->setSize($new_width, $new_height);

                    try {
                        if ($im->readImage($photo_folder."/".$image.'[0]')) {
                            $im->thumbnailImage($new_width , $new_height , TRUE);
                            $im->setBackgroundColor(new ImagickPixel('white')); 
                            $im->setImageFormat('jpg');
                            $output = sprintf("%s/%dx%d_%s.jpg",$thumbnail_folder,$new_width,$new_height,$image);
                            if (file_exists($output)) return sprintf("%dx%d_%s.jpg",$new_width,$new_height,$image);
                            $im->writeImage($output);
                            return sprintf("%dx%d_%s.jpg",$new_width,$new_height,$image);
                        }
                    } catch (Exception $e) {
                        log_action($e->getMessage());
                    }
                }
            }
        }
    }
    return false;
}
function open_image ($file) {
    $size = getimagesize($file);
    switch($size["mime"]){
        case "image/jpeg":
            $im = imagecreatefromjpeg($file); //jpeg file
            break;
        case "image/gif":
            $im = imagecreatefromgif($file); //gif file
            break;
        case "image/png":
            $im = imagecreatefrompng($file); //png file
            break;
        default: 
            $im=false;
            break;
    }
    return $im;
}
function mime_icon ($file,$size) {

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $path = '/images/Free-file-icons-master/';
    $icon_ext = "png";
    
    //default icon: none
    $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,32,'_blank.png');
    $icon = sprintf('%1$s.%2$s','_blank',$icon_ext);
    if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
        $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);

    if (preg_match('/\.(docx)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(doc)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(odt)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(odp)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','ppt',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(xls)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(ods)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(xlsx)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(txt)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(csv)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','txt',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(pdf)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(zip)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(tar)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','tgz',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(gz)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','tgz',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(tgz)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(wav)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(mp4)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(mp3)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(avi)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(sql)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(xml)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',$m[1],$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    }

    return $src;
}
/* create database lines for attached files
 * it processing arrays
 *
 * */
function file_upload_process($list,$sum,$mimetype) {
    global $ID,$GID;

    pg_query($GID,'SET search_path TO system,public');

    $root_dir = getenv('PROJECT_DIR').'attached_files';

    $done = array();
    $comment = '';
    $sessionid = session_id();
    if(isset($_SESSION['Tid'])) {
        $t = $_SESSION['Tid'];
    }
    else {
        $t = 0;
    }

    $lc = new LocaleManager();
    $lc->doBackup();
    $lc->fixLocale();
    for($i=0;$i<count($list);$i++) {
        $ref = $list[$i];
        $exif = array();

        if (in_array(exif_imagetype("$root_dir/$ref"),array(IMAGETYPE_JPEG ,IMAGETYPE_TIFF_II , IMAGETYPE_TIFF_MM))) {
            $exif_read = exif_read_data("$root_dir/$ref", 0, false);
            if (json_encode($exif_read)) {
                $exif = $exif_read;
                /* automatic rotation 
                 *
                 * if (isset($exif_read['COMPUTED'])) {
                    if (isset($exif_read['COMPUTED']['Orientation'])) {
                        $exif_orientation = $exif_read['COMPUTED']['Orientation'];
                        $eit = exif_imagetype("$root_dir/$ref");

                        switch($exif_orientation) {
                            case 3:
                                if ($eit == IMAGETYPE_JPEG) {
                                    $im = @imagecreatefromjpeg("$root_dir/$ref");
                                    $image_p = imagerotate($im, 180, 0);
                                    imagejpeg($im, "$root_dir/$ref");
                                }
                                break;
                            case 6:
                                if ($eit == IMAGETYPE_JPEG) {
                                    $im = @imagecreatefromjpeg("$root_dir/$ref");
                                    $image_p = imagerotate($im, -90, 0);
                                    imagejpeg($im, "$root_dir/$ref");
                                }
                                break;
                            case 8:
                                if ($eit == IMAGETYPE_JPEG) {
                                    $im = @imagecreatefromjpeg("$root_dir/$ref");
                                    $image_p = imagerotate($im, 90, 0);
                                    imagejpeg($im, "$root_dir/$ref");
                                }
                                break;
                        }
                    }
                }*/
            } else
                $exif = array($exif_read['FileName'],$exif_read['FileDateTime'],$exif_read['FileSize'],$exif_read['FileType'],$exif_read['MimeType'],$exif_read['Model'],'corrupted exif...');
        } else {
            $a = exec("mediainfo $root_dir/$ref",$output);
            $n = array();
            foreach ($output as $o) {
                $on = preg_split("/\s+: /",$o);
                if(count($on)>1) {
                    if($on[0]=='Complete name') {
                        $on[1] = basename($on[1]);
                    }
                    $n[$on[0]] = $on[1];
                } 
            }
            $exif = $n;
        }

        $cmd = sprintf("SELECT status,id,reference FROM system.files WHERE reference=%s AND sum=%s AND project_table='%s'",quote($ref),quote($sum[$i]),PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        if (!pg_num_rows($res)) {
            if (isset($_SESSION['current_query_table']))
                $data_table = $_SESSION['current_query_table'];
            else
                $data_table = PROJECTTABLE;

            $cmd = sprintf("INSERT INTO system.files (project_table,reference,comment,datum,access,user_id,status,sessionid,sum,mimetype,data_table,exif) VALUES('%s',%s,%s,NOW(),0,%d,%s,'%s',%s,%s,%s,%s) RETURNING id",PROJECTTABLE,quote($ref),quote($comment),$t,quote('progress'),$sessionid,quote($sum[$i]),quote($mimetype[$i]),quote($data_table),quote(json_encode($exif)));
            $res = pg_query($GID,$cmd);
            if (pg_last_error($GID)){
                $lc->doRestore();
                log_action(pg_last_error($GID));
                return false;
            } else {
               $done[] = $ref;
            }
        } else {
            $row = pg_fetch_assoc($res);
            if ($row['status']=='progress') {
                // file already exists and might be uploded recently 
                $done[] = $row['reference'];
            }
            elseif ($row['status']=='valid') {
                // file already exists and connected previosuly
                // the file owner should be warned that the same file is used again?
                // something else management stpes?
                $done[] = $row['reference'];
            }
            elseif ($row['status']=='deleted') {
                // file has been deleted and now reuploaded
                // it the same file - sha1_file()
                $cmd = sprintf("UPDATE system.files SET status='progress' WHERE id='%d'",$row['id']);
                $res = pg_query($GID,$cmd);
                $done[] = $ref;
            }
        }
    }

    $lc->doRestore();
    return $done;
}
/* create connections for files
 * returning conid
 * */
function file_connection_process($list,$conid,$table,$sessionid,$rownum) {
    global $ID;

    $connect_list = array_unique($list);
    array_filter($connect_list);

    $first_file = 0;

    $conid_container = array();

    //$sessionid = session_id();

    foreach ($connect_list as $cl) {
        if (!$cl) continue;
        //ha minden felsorolt elem már megvan a kapcsolat táblában és egy azonosítóval van összekötve
        //$cmd = sprintf("SELECT DISTINCT conid,count(*) FROM system.file_connect WHERE conid>0 AND file_id IN (SELECT id FROM files WHERE reference IN (%s) and project_table='%s' %s) GROUP BY conid",implode(',',array_map('quote',$connect_list)),PROJECTTABLE,$filter);

        $file_id = 0;
        $cmd = sprintf('SELECT id FROM system.files WHERE reference=%s AND project_table=%s',quote($cl),quote($table));
        //log_action($cmd,__FILE__,__LINE__);
        $res1 = pg_query($ID,$cmd);
        if (pg_num_rows($res1)) {
            $row1 = pg_fetch_assoc($res1);
            $file_id = $row1['id'];
        }
        if (!$file_id) continue;

        if (!$conid) {
            if (!$sessionid) {
                $sessionid = session_id();
            }
            // upload_funcs.php
            // update_fields.php
            // clean current conid in new upload
            $cmd = sprintf('DELETE FROM system.file_connect WHERE temporal=true AND sessionid=\'%s\' AND rownum=%d',$sessionid,$rownum);
            pg_query($ID,$cmd);
            //debug($cmd);
            
            // ganerate short hash from available strings
            // using it instead of numeric conid
            $conid = crypt($file_id.$rownum.microtime(),$sessionid);

            $cmd = sprintf("INSERT INTO system.file_connect (conid,file_id,sessionid,rownum) VALUES ('%s',%d,'%s',%d) RETURNING conid",$conid,$file_id,$sessionid,$rownum);
            //debug($cmd);
            pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                log_action(pg_last_error($ID));
                return false;
            }
        } else {
            if (!$sessionid) {
                $sessionid = session_id();
            }

            // update_fields.php
            $cmd = sprintf("INSERT INTO system.file_connect (conid,file_id,sessionid,rownum,temporal) VALUES ('%s',%d,'%s',%d,false)",$conid,$file_id,$sessionid,$rownum);
            pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                log_action(pg_last_error($ID));
                return false;
            }
        }
    }
    return $conid;
}
//file_connect sub function
// NOT USED !!
function create_new_connectid_with_elements($connect_list,$filter) {
    global $GID;

    $f = array_pop($connect_list);
    $cmd = sprintf("INSERT INTO system.file_connect (file_id) SELECT id FROM files WHERE reference=%s AND project_table='%s' %s RETURNING conid",quote($f),PROJECTTABLE,$filter);
    $res = pg_query($GID,$cmd);
    $row = pg_fetch_assoc($res);
    $conid = $row['conid'];
    if($conid)
        add_elements_to_connectid($conid,$connect_list,$filter);
    return $conid;
}
//file_connect sub function
// NOT USED !!!
function add_elements_to_connectid($conid,$connect_list,$filter) {
    global $GID;
    foreach ($connect_list as $e) {
        $cmd = sprintf("INSERT INTO system.file_connect (file_id,conid) SELECT id,%d FROM files WHERE reference=%s AND project_table='%s' %s RETURNING conid",$conid,quote($e),PROJECTTABLE,$filter);
        $res = pg_query($GID,$cmd);
    }
}
/* File attachment unlink
 *
 * */
function unlink_attachemnt($file) {
    $root_dir = getenv('PROJECT_DIR').'attached_files';
    unlink($root_dir.'/'.basename($file));
    
    log_action("unlink $root_dir/".basename($file),__FILE__,__LINE__);
    
    #search for thumbnails - delete them all
    chdir("$root_dir/thumbnails/");
    //$list = glob("$root_dir/thumbnails/*'".basename($file)."'");
    // Search for all files that match .* or *
    $list = glob("*{".basename($file)."}", GLOB_BRACE);
    foreach($list as $s) {
        unlink($s);
        log_action("unlink $s",__FILE__,__LINE__);
    }
}
/* Profile function
 * change own email address
 * This called, when user confirm it by follow an url
 * */
function change_email($code) {
    global $BID;
    if (!isset($_SESSION['Tid'])) return false;

    if (isset($_SESSION['emailchange']) and $_SESSION['emailchange']['code']==$code and $code!='') {
        $cmd = sprintf("UPDATE \"public\".\"users\" SET \"email\"=%s WHERE id='{$_SESSION['Tid']}'",quote(strtolower($_SESSION['emailchange']['addr'])));
        unset($_SESSION['emailchange']);
        $res = pg_query($BID,$cmd);
        if(pg_affected_rows($res)) {
            return true;
        }
        else return pg_last_error($BID);
    }
    return false;
}
/* Profile function 
 * drop my profile
 * */
function drop_profile($code) {
    global $BID;
    if (!isset($_SESSION['Tid'])) return false;
    if (isset($_SESSION['drop_my_profile']) and $_SESSION['drop_my_profile']['code']==$code and $code!='') {
        $cmd = sprintf('DELETE FROM "users" WHERE id=%d AND "user"=%s',$_SESSION['Tid'],quote($_SESSION['drop_my_profile']['profile']));
        $res = pg_query($BID,$cmd);
        if(pg_affected_rows($res)) {
            return true;
        }
    }
    log_action( pg_last_error($BID),__LINE__,__FILE__ );
    return false;
}
### PHP compatibility before php 5.5
if(!function_exists("array_column"))
{

    function array_column($array,$column_name)
    {

        return array_map(function($element) use($column_name){return $element[$column_name];}, $array);

    }

}

/* upload - file upload, pager
 *
 * */
function pager($perpage,$pages,$counter){
    $m = '';
    if($pages!='') {
        $stepb = (round($counter/$perpage)-1)*$perpage;
        $stepf = (round($counter/$perpage)+1)*$perpage;

        if($counter > 0) {
            $m .= "<li class='pure-button button-small button-transp'><a href='0' class='paging'><i class='fa fa-backward'></i></a></li>";
            $m .= "<li class='pure-button button-small button-transp'><a href='$stepb' class='paging'><i class='fa fa-chevron-left'></i></a></li>";
        } else {
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-backward' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-chevron-left' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
        }

        # mennyi lap van
        $lp = ceil($pages/$perpage);

        # számláló állása
        $c = round($counter/$perpage);

        # honnan kezdődik
        if($c<5) {
            $c = 0;
        }
        elseif($c>4 and $c<($lp-4) and $lp>10) {
            $c = $c-5;    
        } elseif ($c>=($lp-4) and $lp>10) {
            $c = $lp-10;
        }

        # meddig megy a számláló
        if ($lp>($c+10)) {
            $n = $c+10;
        } else
            $n = $lp;
        
        //köztes gombok
        for($i=$c;$i<$n;$i++) {
            $a = '';
            if ($counter/$perpage == $i) {
                $a = "pure-button-active";
            }
            $m .= sprintf("<li class='pure-button button-small button-transp $a'><a href='%d' class='paging'>%d.</a></li>",$i*$perpage,$i+1);
        }

        //egész osztásnál ne adjon egy + üres lapot
        if (fmod($pages,$perpage)) {
            $op = 0;
        } else
            $op = $_SESSION['perpage'];

        //záró gombok
        if($stepf < $pages) {
            $m .= sprintf("<li class='pure-button button-small button-transp'><a href='%d' class='paging'><i class='fa fa-chevron-right'></i></a></li>",$stepf);
            $m .= sprintf("<li class='pure-button button-small button-transp'><a href='%d' class='paging'><i class='fa fa-forward'></i></a></li>",(floor($pages/$perpage)*$perpage)-$op);
        } else {
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-chevron-right' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-forward' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
        }

    }
    return $m;
}
/* fetch remote xml xontent
 *
 * */
function fetch_remote_xml($url) {
    $xml = file_get_contents($url); 
    if($xml===false) $data = '';
    else $data = new SimpleXMLElement($xml);
    return $data;
}
/* create a href button 
 * to access an included php 
 * It used in results_builder action buttons */
function button($href,$title,$i='',$target='_blank',$class='button-href pure-button',$data='') {
    //awesome class
    if ($i!='') $icon = "<i class='$i'></i> ";
    if ($data!='') {
        $n = preg_split('/=/',$data);
        $data = "data-$n[0]=$n[1]";
    }
    if (preg_match('/^upload\//',$href)) $href=$href;
    else $href="includes/$href";
    return "<a href='$href' target='$target' class='$class' $data>$icon$title</a>";
}
/* revision as timestamp
 * not used?
 * */
function rev($file) {
    $timestamp = 0;#time();
    if (!file_exists("$file")) {
        $file = getenv('OB_LIB_DIR').$file;
    }
    if (file_exists("$file")) {
        $fp = fopen("$file", "r");
        $fstat = fstat($fp);
        $timestamp = $fstat['mtime'];
    }
    return $timestamp;
}
/* function of upload_table_post ajax request 
 * called in upload_funcs.php
 * */
function warning($row,$column_label,$message,$column='') {
    //global $colnames;
    $r = '';
    if ($column=='')
        $column = $column_label;
    if ($column_label!='') {
        if (isset($_SESSION['api_warnings'])) {
            $_SESSION['api_warnings'][$column][$row] = $message;
            return $row;
        } else {
            // normal error warning
            if ($row!=='') {
                $r = "$row. ".str_in_row;
            }
            return "$r <span style='font-style:italic;background-color:#accade'>$column_label</span> ".str_column.": $message!";
        }
    } else {
        if (isset($_SESSION['api_warnings'])) {
            log_action('column should be defined',__FILE__,__LINE__);
        }
        // no column name
        if ($row!='') {
            $r = "$row. ".str_in_row;
        }
        return "$r: $message!";
    }
}
/* edit span button 
 *
 * */
function edit ($value,$id,$name,$enabled,$function,$active=0,$action='',$table=PROJECTTABLE,$target='') {
    // return an edbox input field
    if ($enabled) {
        if ($value=='....................') $valuec="";
        else $valuec = preg_replace("/'/","&#39;",$value);

        if ($value=='') $value="&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

        if ($target!='') $target = "data-target='$target'"; 

        if ($name=='obm_files_id')
            $refresh_add_button = "<button class='button-secondary button-small pure-button edbox-button $function refresh-add' id='edr-$id'><i class='fa fa-refresh fa-lg'></i> ".str_add."</button>";
        else
            $refresh_add_button = "";

        if ($active)
            return "<span class='edbox-span' id='span-$id' style='display:none'>$value&nbsp;</span><input name='$name' style='display:inline' class='edbox edbox-active' $target data-table='$table' id='dc-$id' value='$valuec'><button class='button-success button-small pure-button edbox-button $function' style='display:inline' id='ed-$id'><i class='fa fa-floppy-o fa-lg'></i>".str_save."</button>$refresh_add_button";
        else
            return "<span class='edbox-span' id='span-$id'>$value&nbsp;</span><input name='$name' class='edbox $action' $target data-table='$table' id='dc-$id' value='$valuec'><button class='button-success button-small pure-button edbox-button $function' id='ed-$id'><i class='fa fa-floppy-o fa-lg'></i> ".str_save."</button>$refresh_add_button";
    } else
        return "<span style='font-size:0.9em;padding-left:2px'>$value</span>";
}

/* wiki help
 * */
function wikilink($wiki,$text) {
    //turn off help messages by using define('HELPS','off') in local_vars.php
    if (defined('HELPS') and HELPS=='off')
        return;

    $wikiurl = "http://openbiomaps.org/documents/".$_SESSION['LANG']."/$wiki";
    $link = sprintf("<i class='fa fa-lg fa-question-circle-o'></i> <a href='%s' target='_blank' class='faq'>%s</a>",$wikiurl,t($text));
    return $link;
}
/**
 * PseudoCrypt by KevBurns (http://blog.kevburnsjr.com/php-unique-hash)
 * Reference/source: http://stackoverflow.com/a/1464155/933782
 * 
 * I want a short alphanumeric hash that’s unique and who’s sequence is difficult to deduce. 
 * I could run it out to md5 and trim the first n chars but that’s not going to be very unique. 
 * Storing a truncated checksum in a unique field means that the frequency of collisions will increase 
 * geometrically as the number of unique keys for a base 62 encoded integer approaches 62^n. 
 * I’d rather do it right than code myself a timebomb. So I came up with this.
 * 
 * Sample Code:
 * 
 * echo "<pre>";
 * foreach(range(1, 10) as $n) {
 *     echo $n." - ";
 *     $hash = PseudoCrypt::hash($n, 6);
 *     echo $hash." - ";
 *     echo PseudoCrypt::unhash($hash)."<br/>";
 * }
 * 
 * Sample Results:
 * 1 - cJinsP - 1
 * 2 - EdRbko - 2
 * 3 - qxAPdD - 3
 * 4 - TGtDVc - 4
 * 5 - 5ac1O1 - 5
 * 6 - huKpGQ - 6
 * 7 - KE3d8p - 7
 * 8 - wXmR1E - 8
 * 9 - YrVEtd - 9
 * 10 - BBE2m2 - 10
 */
class PseudoCrypt {

    /* Key: Next prime greater than 62 ^ n / 1.618033988749894848 */
    /* Value: modular multiplicative inverse */
    private static $golden_primes = array(
        '1'                  => '1',
        '41'                 => '59',
        '2377'               => '1677',
        '147299'             => '187507',
        '9132313'            => '5952585',
        '566201239'          => '643566407',
        '35104476161'        => '22071637057',
        '2176477521929'      => '294289236153',
        '134941606358731'    => '88879354792675',
        '8366379594239857'   => '7275288500431249',
        '518715534842869223' => '280042546585394647'
    );

    /* Ascii :                    0  9,         A  Z,         a  z     */
    /* $chars = array_merge(range(48,57), range(65,90), range(97,122)) */
    private static $chars62 = array(
        0=>48,1=>49,2=>50,3=>51,4=>52,5=>53,6=>54,7=>55,8=>56,9=>57,10=>65,
        11=>66,12=>67,13=>68,14=>69,15=>70,16=>71,17=>72,18=>73,19=>74,20=>75,
        21=>76,22=>77,23=>78,24=>79,25=>80,26=>81,27=>82,28=>83,29=>84,30=>85,
        31=>86,32=>87,33=>88,34=>89,35=>90,36=>97,37=>98,38=>99,39=>100,40=>101,
        41=>102,42=>103,43=>104,44=>105,45=>106,46=>107,47=>108,48=>109,49=>110,
        50=>111,51=>112,52=>113,53=>114,54=>115,55=>116,56=>117,57=>118,58=>119,
        59=>120,60=>121,61=>122
    );

    public static function base62($int) {
        $key = "";
        while(bccomp($int, 0) > 0) {
            $mod = bcmod($int, 62);
            $key .= chr(self::$chars62[$mod]);
            $int = bcdiv($int, 62);
        }
        return strrev($key);
    }

    public static function hash($num, $len = 5) {
        $ceil = bcpow(62, $len);
        $primes = array_keys(self::$golden_primes);
        $prime = $primes[$len];
        $dec = bcmod(bcmul($num, $prime), $ceil);
        $hash = self::base62($dec);
        return str_pad($hash, $len, "0", STR_PAD_LEFT);
    }

    public static function unbase62($key) {
        $int = 0;
        foreach(str_split(strrev($key)) as $i => $char) {
            $dec = array_search(ord($char), self::$chars62);
            $int = bcadd(bcmul($dec, bcpow(62, $i)), $int);
        }
        return $int;
    }

    public static function unhash($hash) {
        $len = strlen($hash);
        $ceil = bcpow(62, $len);
        $mmiprimes = array_values(self::$golden_primes);
        $mmi = $mmiprimes[$len];
        $num = self::unbase62($hash);
        $dec = bcmod(bcmul($num, $mmi), $ceil);
        return $dec;
    }

}
#http://stackoverflow.com/questions/959957/php-short-hash-like-url-shortening-websites
class BaseIntEncoder {

    //const $codeset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //readable character set excluded (0,O,1,l)
    const codeset = "23456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";

    static function encode($n){
        $base = strlen(self::codeset);
        $converted = '';

        while ($n > 0) {
            $converted = substr(self::codeset, bcmod($n,$base), 1) . $converted;
            $n = self::bcFloor(bcdiv($n, $base));
        }

        return $converted ;
    }

    static function decode($code){
        $base = strlen(self::codeset);
        $c = '0';
        for ($i = strlen($code); $i; $i--) {
            $c = bcadd($c,bcmul(strpos(self::codeset, substr($code, (-1 * ( $i - strlen($code) )),1))
                    ,bcpow($base,$i-1)));
        }

        return bcmul($c, 1, 0);
    }

    static private function bcFloor($x)
    {
        return bcmul($x, '1', 0);
    }

    static private function bcCeil($x)
    {
        $floor = bcFloor($x);
        return bcadd($floor, ceil(bcsub($x, $floor)));
    }

    static private function bcRound($x)
    {
        $floor = bcFloor($x);
        return bcadd($floor, round(bcsub($x, $floor)));
    }
}
function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}
/* `apply function: + click functions in upload table
 * content = content read from cell to apply function
 * if pointer given to an other cell, content will be the pointed cell content
 * and the selected cell content is the fill
 *
 * */
function upload_table_obfun($apply_function,$rowindex,$colindex,$idmatch,$autofill_data) {
    
    $idx  = sprintf('%d',preg_replace('/[^0-9]/','',$colindex)-2);

    $key = $_SESSION['theader'][$idx];

    $row = get_sheet_row($rowindex+1); 
    $j = json_decode($row['data'],true);

    if (!isset($j[$key]))
        $content = "";
    else
        $content = $j[$key];


    if ($idmatch!='' and $idmatch!=-1) {
        // if use function to refer an other column
        // apply:[EGYEDSZAM]
        // idmatch is EGYEDSZAM
        $fill = $content;
        //$content = $j[$idmatch];
    }
    elseif ($idmatch=='-1') {
        if ($rowindex==0) return $content;
        
        $row = get_sheet_row($rowindex+1);
        $j1 = json_decode($row['data'],true);
        $content = $j1[$key];

        $row = get_sheet_row($rowindex);
        $j0 = json_decode($row['data'],true);
        $fill = $j0[$key];

        $j = $j1;
    }

    if (preg_match('/^`apply:(\[.+?\])?(.+)$/i',$apply_function,$fm)) {
        // referring an other cell in function e.g. apply:[EGYEDSZAM]
        $pointer_cell = preg_replace('/^\[/','',$fm[1]);
        $pointer_cell = preg_replace('/\]$/','',$pointer_cell);

        $function = preg_replace('/^\[\]/','',$fm[2]);
        
        if (preg_match('/days.since.(\d{4})-(\d{2})-(\d{2})/',$function,$km) and is_numeric($content)) {
            // excel days
            $con_date = strtotime("$km[1]-$km[2]-$km[3]");
            $unix_date = strtotime("1970-01-01");
            $datediff = $unix_date - $con_date;
            
            $int = (int)$content;
            $float = (float)$content;

            if ($float != $int) {
                $daysdiff = $datediff / (60 * 60 * 24);
                $UNIX_DATE = ($content - $daysdiff) * 86400;
                $content = gmdate("Y-m-d H:i:s", $UNIX_DATE);
            } else {
                $daysdiff = floor($datediff / (60 * 60 * 24));
                $UNIX_DATE = ($content - $daysdiff) * 86400;
                $content = gmdate("Y-m-d", $UNIX_DATE);
            }
        } 
        elseif (preg_match('/seconds.since.(\d{2})-(\d{2})-(\d{2})/',$function,$km) and is_numeric($content)) {
            // excel time
            $t = $content * 86400;
            $hours = floor($t / 3600);
            $t = $t % 3600;
            $minutes = floor($t / 60);
            $seconds = $t % 60;
            $content = sprintf("%02d:%02d:%02d",$hours,$minutes,$seconds);
        } 
        elseif ($function=='increment' and $content!='') {

            if ($pointer_cell=='') {
                $content++;
            } else {
                $content++;
            }
        } 
        elseif ($function=='math.sum') {

            if ($pointer_cell=='') {
                if (preg_match_all('/([0-9.-]+)/',$content,$mm))
                    $content = array_sum($mm[1]);
            } else {
                // reference cells
                $references = preg_split('/,/',$pointer_cell);
                $avg_values = array();
                foreach($references as $ref) {
                    $content = $j[$ref];
                    
                    if (preg_match_all('/([0-9.-]+)/',$j[$ref],$mm2)) {
                        $avg_values = array_merge($avg_values,$mm2[1]);
                    }
                }
                if (count($avg_values))
                    $content = array_sum($avg_values);
                else
                    $content = $fill;

            }
        } 
        elseif ($function=='math.avg') {

            if ($pointer_cell=='') {
                if (preg_match_all('/([0-9.-]+)/',$content,$mm))
                    $content = array_sum($mm[1])/count($mm[1]);
            } else {
                // reference cells
                $references = preg_split('/,/',$pointer_cell);
                $avg_values = array();
                foreach($references as $ref) {
                    $content = $j[$ref];
                    
                    if (preg_match_all('/([0-9.-]+)/',$j[$ref],$mm2)) {
                        $avg_values = array_merge($avg_values,$mm2[1]);
                    }
                }
                if (count($avg_values))
                    $content = array_sum($avg_values) / count($avg_values);
                else
                    $content = $fill;
            }
        } 
        elseif (preg_match('/^replace.all:\/(.*)\/(.*)\//u',$function,$mm)) {
            // replace cell content to the given string in the cell value matches to the pattern
            // match test can be an other field

            if ($pointer_cell=='') {
                if (preg_match("/$mm[1]/",$content)) {
                    $content = $mm[2];
                }
            } else {
                // special case we read compare content from an other column

                if (preg_match("/$mm[1]/",$content,$m3)) {
                    if (preg_match('/\$(\d)/',$mm[2],$m4))
                        $mm[2] = $m3[$m4[1]];
                    $content = $mm[2];
                } else {
                    // protect non-mached cells, write back the original content into the content variable
                    $content = $fill;
                }
            }
        }
        elseif (preg_match('/^replace.match:\/(.*)\/(.*)\//u',$function,$mm)) {
            // replace matched characters to the given string in the cell values

            if (preg_match("/$mm[1]/",$content)) {
                $content = preg_replace("/$mm[1]/", $mm[2], $content);
            }
        }
        elseif ($function=='groupped.fill') {
            if ($idmatch=='') {
                if ($rowindex==0) return $content;

                $row = get_sheet_row($rowindex+1);
                $j1 = json_decode($row['data'],true);
                $content = $j1[$key];
                $row = get_sheet_row($rowindex);
                $j0 = json_decode($row['data'],true);
                $fill = $j0[$key];

                $j = $j1; 
            }

            if ($content == '') {
                $content = $fill;
            }
        }
    } else {

        // not function based filling
        // just overwrite content with the given string.
        $content = $autofill_data;
    }

    $j[$key] = $content;
    $res = update_sheet_row($rowindex+1,$j);
    return $content;

}
// check system command exists
function command_exist($cmd) {
    //$returnVal = shell_exec(sprintf("which %s", escapeshellarg($cmd)));
    $returnVal = exec(sprintf("which %s", escapeshellarg($cmd)));
    return !empty($returnVal);
}
/* quote an sql column name
 * split by .
 * remove extra characters...
 */
function quote_column($column) {
    $table = '';
    $m = preg_split('/\./',$column);
    if (count($m)>1) {
        $table = preg_replace('/[^a-z0-9_]/i','',$m[0]);
        $column = $m[1];
    }
    $column = preg_replace('/[^a-z0-9_]/i','',$column);

    if ($table != '')
        return sprintf('"%s"."%s"',$table,$column);
    else
        return sprintf('"%s"',$column);
}

/* array [normal|associative], array_element
 * create an "<option></option>" list from the element the array and set the selected element
 * */
function selected_option($options,$selected='') {
    $e = array();
    foreach($options as $val) {
        $key = $val;
        $title = '';
        $m = preg_split('/::/',$val);
        if (count($m) > 1) {
            $key = $m[0];
            $val = $m[1];
            if(isset($m[2]))
                $title = $m[2];
        }
        if (!is_array($selected))
            $selected = array($selected);

        $s = "";
        foreach ($selected as $sel) {
            if (trim($val) == trim($sel)) {
                $s = "selected";
            }
        }
        $value = "<option $s value='$val' title='$title'>$key</option>";
        
        $e[$key] = $value;
    }
    // extra empty line
    if ($selected==' ') $e[] = "<option selected></option>";
    return implode($e,'');
}
/* insert assoc key=>elem to specified pos in array */
function insertAt($array = [], $item = [], $position = 0) {
    $previous_items = array_slice($array, 0, $position, true);
    $next_items     = array_slice($array, $position, NULL, true);
    return $previous_items + $item + $next_items;
}
/* Retreive and create citation from doi
 * It is an experimental function, not used yet anywhere...
 *
 * doi:
 *      10.1242/jeb.040394
 * url:
 *      https://doi.org/
 *      https://api.crossref.org/works/
 *      ...
 * type:
 *      bibtex,Citeproc.JSON,RDF.XML,text
 * style:
 *      apa,mla,harvard3 
 *      list of available style:
 *      https://github.com/citation-style-language/styles 
 * accept:
 *      Accept: application/x-bibtex
 *      ...
 *      
 * */
function doi_get($doi,$url='https://doi.org/',$type='text',$style='apa',$accept="") {

    $curl = curl_init();

    // examples:
    // https://citation.crosscite.org/docs.html
    
    // simple bibtex format
    // dx.doi.org
    // curl -LH "Accept: application/x-bibtex" http://dx.doi.org/10.1242/jeb.040394
    // www.doi2bib.org
    // curl http://www.doi2bib.org/doi2bib?id=10.1242/jeb.040394
    if ($type == 'bibtex') {
        //$url = "http://www.doi2bib.org/doi2bib?id=";
        //$url = "http://dx.doi.org/";
        //$url = "http://doi.org/";

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url$doi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Accept: application/x-bibtex'),
            CURLOPT_POST => false,
            )
        );
        $result = curl_exec($curl);
        return $result;
    }
    
    
    // Citeproc JSON from crossref.org
    // curl https://api.crossref.org/works/10.1242/jeb.040394/transform/application/vnd.citationstyles.csl+json
    // curl -LH "Accept: application/rdf+xml;q=0.5, application/vnd.citationstyles.csl+json;q=1.0" https://doi.org/10.1242/jeb.040394
    // return JSON
    if ($type == 'Citeproc.JSON') {
        if (preg_match("api.crossref.org",$url)) {
            $url = "https://api.crossref.org/works/";
            $doi .= "/transform/application/application/vnd.citationstyles.csl+json";
            $accept = "";
        } elseif (preg_match('doi.org')) {
            $url = "https://doi.org/";
            $accept = "Accept: application/rdf+xml;q=0.5, application/vnd.citationstyles.csl+json;q=1.0";
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url$doi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array($accept),
            CURLOPT_POST => false,
            )
        );
        $result = curl_exec($curl);
        //$j = json_decode($result);
        return $result;
    }

    // styled text
    // possible styles: https://github.com/citation-style-language/styles
    // APA styled from doi.org
    // curl -LH "Accept: text/x-bibliography;style=apa" https://doi.org/10.1242/jeb.040394
    // return text
    if ($type == 'text') {
        $accept = "Accept: text/x-bibliography;style=$style; locale=en-GB";
        //$url = "https://doi.org/";
        //$url = "https://dx.doi.org/";
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url$doi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array($accept),
            CURLOPT_POST => false,
            )
        );
        $result = curl_exec($curl);
        return $result;
    }

    // RDF XML
    // get xml
    // curl -LH "Accept: application/vnd.crossref.unixref+xml;q=1, application/rdf+xml;q=0.5" https://doi.org/10.1242/jeb.040394
    //
    // get JSON
    if ($style == "RDF.XML") {
        //$url = "https://doi.org/"
        if (preg_match('doi.org')) {
            $url = "https://doi.org/";
            $accept = "Accept: application/vnd.crossref.unixref+xml;q=1, application/rdf+xml;q=0.5";
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url$doi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array($accept),
            CURLOPT_POST => false,
            )
        );
        $result = curl_exec($curl);

        /* xml processing example
         *
         * $xml = simplexml_load_string($result);
         * if (isset($xml->{'doi_records'})) { 
            $title = $xml->{'doi_records'}->{'doi_record'}->{'crossref'}->{'journal'}->{'journal_article'}->{'titles'}->{'title'};
            return;
        } */

        return $result;
 
    }
    // Search by title or other text fields...
    // curl http://search.crossref.org/dois?q=Discordancy+or+template-based+recognition?+Dissecting+the+cognitive+basis+of+the+rejection+of+foreign+eggs+in+hosts+of+avian+brood+parasites
    // $j = json_decode($result)
    // $doi = $j[0]['doi'];
    // ...
}
/* return standard JSON status message
 * https://labs.omniti.com/labs/jsend
 * */
function common_message ($return,$explanation=NULL,$multiple=false) {

    if ($multiple) {
        //log_action($return);
        $mstatus = array();
        foreach($return as $marray) {
            $mstatus[] = $marray[0];
            $mexpl[] = $marray[1];
        }
        if (count(array_unique($mstatus))>1) {
            $return = "fail";
            $explanation = implode(',',$mexpl);
        }
        else {
            $return = $mstatus[0];
            $explanation = implode(',',$mexpl);
        }
    }

    $status = 'unknown';
    $data = '';
    $message = '';
    $result = null;

    if (preg_match('/ok/i',$return)) {
        $status = "success";
        $data = $explanation;
    } elseif (preg_match('/fail/i',$return)) {
        $status = "fail";
        $data = $explanation;
    } elseif (preg_match('/error/i',$return)) {
        $status = "error";
        $message = $explanation;
    } elseif (preg_match('/warning/i',$return)) {
        $status = "warning";
        $message = $explanation;
    } elseif ($return) {
        $status = "success";
        $data = $explanation;
    } elseif (!$return) {
        $status = "error";
        $message = $explanation;
    } elseif ($return == 'empty') {
        $status = "fail";
        $data = $explanation;
    } else {
        log_action('invalid ajax message: '.$return,__FILE__,__LINE__);
    }
    
    if ($data !== '')
        $result = array('status'=>$status,'data'=>$data);
    elseif ($message !== '')
        $result = array('status'=>$status,'message'=>$message);

    return (json_encode($result));
}
//Check if a port is open - run R-shiny server
function stest($ip='localhost', $port=8001) {

    $fp = @fsockopen($ip, $port, $errno, $errstr, 0.1);
    if (!$fp) {
        return false;
    } else {
        fclose($fp);
        return true;
    }
}
// mgrid-area function
function area($area) {
        $area = preg_replace('/^\s*/m','"',$area);
        $area = preg_replace('/$/m','"',$area);
        $area = preg_replace('/\s+/m',' ',$area);
        return $area;
}
// print mainpage grid
function print_grid() {
    global $load_map;

    if (file_exists(getenv('PROJECT_DIR')."includes/private/mainpage_grid.php"))
        require_once(getenv('PROJECT_DIR')."includes/private/mainpage_grid.php");
    elseif (file_exists(getenv('PROJECT_DIR')."includes/mainpage_grid.php"))
        require_once(getenv('PROJECT_DIR')."includes/mainpage_grid.php");
    else {
        log_action("File does not exists: includes/mainpage_grid.php",__FILE__,__LINE__);
        return;
    }

    if (file_exists(getenv('PROJECT_DIR')."includes/private/mainpage_functions.php"))
        require_once(getenv('PROJECT_DIR').'includes/private/mainpage_functions.php');
    elseif (file_exists(getenv('PROJECT_DIR')."includes/mainpage_functions.php"))
        require_once(getenv('PROJECT_DIR').'includes/mainpage_functions.php');
    else {
        log_action("File does not exists: includes/mainpage_functions.php",__FILE__,__LINE__);
        return;
    }

    $mgrid = new mgrid();

    echo "<script>$(document).ready(function(){});</script>";
    echo "<style>.wrapper {grid-gap:".$mgrid->gap.";grid-template-areas:".area($mgrid->areas).";}</style>";
    echo "<style>@media only screen and (min-width: 600px) {.wrapper {grid-gap:".$mgrid->gap600.";grid-template-columns:".$mgrid->columns600.";grid-template-areas:".area($mgrid->areas600).";}}</style>";
    echo "<style>@media only screen and (min-width: 900px) {.wrapper {grid-gap:".$mgrid->gap900.";grid-template-columns:".$mgrid->columns900.";grid-template-areas:".area($mgrid->areas900).";}}</style>";
    echo "<style>@media only screen and (min-width: 1400px) {.wrapper {grid-gap:".$mgrid->gap1400.";grid-template-columns:".$mgrid->columns1400.";grid-template-areas:".area($mgrid->areas1400).";}}</style>";
    
    foreach ($mgrid->elements as $mii ) {

        if (file_exists(getenv('PROJECT_DIR')."includes/private/mainpage_$mii.php")) {
            ob_start();
            include_once(getenv('PROJECT_DIR')."includes/private/mainpage_$mii.php");
            $content = ob_get_contents();
            $cellc = $content;
            ob_end_clean();
            $mgrid->definition = preg_replace("/#$mii/",$cellc,$mgrid->definition);
        }
        elseif (file_exists(getenv('PROJECT_DIR')."includes/mainpage_$mii.php")) {
            ob_start();
            include_once(getenv('PROJECT_DIR')."includes/mainpage_$mii.php");
            $content = ob_get_contents();
            $cellc = $content;
            ob_end_clean();
            $mgrid->definition = preg_replace("/#$mii/",$cellc,$mgrid->definition);
        }
    }
    echo $mgrid->definition;

    /*if (!isset($mgrid->$cell))
        return;
    $def = $mgrid->$cell;

    $bgcol = '';
    $rowspan = '';
    $colspan = '';
    $cellc = '';
    $classes = '';

    if (isset($def['bg']) and $def['bg'] == 'randomcolor')
        $bgcol = "background-color:#".rand_color();
    elseif (isset($def['bg']) and $def['bg'] == 'color')
        $bgcol = "background-color:{$def['bgcolor']}";

    if (isset($def['rowspan']) and $def['rowspan'])
        $rowspan = "rowspan='{$def['rowspan']}'";
    
    if (isset($def['colspan']) and $def['colspan'])
        $rowspan = "colspan='{$def['colspan']}'";

    if (isset($def['class']) and $def['class']!='')
        $classes = $def['class'];

    if (file_exists("includes/$cell.php")) {
        ob_start();
        include_once("includes/$cell.php");
        $content = ob_get_contents();
        $cellc = $content;
        ob_end_clean();
        //return "<td style='$bgcol' class='$classes' $rowspan $colspan id='$cell'>$cellc</td>";
        return "<div style='$bgcol' class='box $classes' id='$cell'>$cellc</div>";
    }*/
}

function mb_in_array($_needle,array $_hayStack) {
    foreach ($_hayStack as $value) {
        //log_action(mb_strtolower($value).' '.mb_strtolower($_needle));
        if((mb_strtolower($value)) === (mb_strtolower($_needle))) {
            return true;
        }
    }
    return false;   
}
/* sql table aliases FROM clause*/
function sql_aliases($current_table) {
    global $BID;

    if ($current_table == 1) {
        if (isset($_SESSION['current_query_table']))
            $current_table = $_SESSION['current_query_table'];
        else
            $current_table = PROJECTTABLE;
    }

    $qtable = $current_table;
    $id_col = "obm_id";
    $id_alias = $qtable;
    $qtable_alias = $qtable;
    $FROM = sprintf('FROM %1$s',$qtable); 
    $geom_alias = $qtable_alias;
    if (isset($_SESSION['st_col']['GEOM_C']) and $_SESSION['st_col']['GEOM_C']!='')
        $GEOM_C = $qtable_alias.".".$_SESSION['st_col']['GEOM_C'];

    $cmd1 = sprintf("SELECT layer_query 
        FROM project_queries 
        WHERE project_table='%s' AND layer_type='query' AND enabled=TRUE AND main_table=%s 
        ORDER BY rst DESC",
            PROJECTTABLE,quote($current_table));

    // WHICH ROW????
    $res = pg_query($BID,$cmd1);
    $row = pg_fetch_assoc($res);
    
    $joins = 0;
    // FROM %F%dinpi d%F%
    if (preg_match('/%F%(.+)%F%/',$row['layer_query'],$m)) {
        
        $FROM = sprintf('FROM %s',$m[1]);

        // dinpi d
        if (preg_match('/([a-z0-9_]+) (\w+)/i',$m[1],$mt)) {
            $qtable = $mt[1];
            $qtable_alias = $mt[2];
            $geom_alias = $qtable_alias;
            $GEOM_C = $qtable_alias.".".$_SESSION['st_col']['GEOM_C'];
            $id_alias = $qtable_alias;
            $id_col = $qtable_alias.".obm_id";
            
            // geometry can come from any joined tables!!
            if (preg_match("/([a-z0-9_]+)\.obm_geometry/",$row['layer_query'],$mg)) {
                $geom_alias = $mg[1];
                $GEOM_C = $geom_alias.".obm_geometry";
            }
            /*if (preg_match("/([a-z_]+)\.%transform_geometry%/",$row['layer_query'],$mg)) {
                $geom_alias = $mg[1];
                $geom_col = $geom_alias.".obm_geometry";
            }*/
            if (preg_match("/([a-z0-9_]+)\.obm_id/",$row['layer_query'],$mg)) {
                $id_alias = $mg[1];
                $id_col = $id_alias.".obm_id";
            }

        }
        if (preg_match_all('/%J%(.+)%J%/',$row['layer_query'],$mj)) {
            foreach($mj[1] as $mje) {
                // LEFT JOIN public_nestbox_data n ON (nestbox_id=n.obm_id)
                if (preg_match('/JOIN ([a-z0-9_]+) (\w+)/i',$mje,$mt)) {
                    $jtable = $mt[1];
                    $jtable_alias = $mt[2];
                    $joins++;
                }
                $FROM .= " ".$mje;
            }
        }
    }
    return(array("from"=>$FROM,"qtable"=>$qtable,"qtable_alias"=>$qtable_alias,"geom_col"=>$GEOM_C,"geom_alias"=>$geom_alias,"id_col"=>$id_col,"id_alias"=>$id_alias,"joins"=>$joins));
}

/* unzip file to destination directory 
 * destination directory is typically a temp directory, which shlould be deleted!!
 * */
function unpack_zip($file,$dest) {
    $zip = new ZipArchive;
    if ($zip->open($file) === TRUE) {
        if (!mkdir($dest, 0700, true)) {
            log_action("Failed to create UNZIP directory: $dest",__FILE__,__LINE__);
            return 0;
        }
        $zip->extractTo($dest);
        $zip->close();
        return 1;
    } else {
        log_action("Failed to read ZIP archive: $file",__FILE__,__LINE__);
        return 0;
    }
}
function unpack_rar($file,$dest) {
    if ($rar_open($file) === TRUE) {
        if (!mkdir($dest, 0700, true)) {
            log_action("Failed to create UNRAR directory: $dest",__FILE__,__LINE__);
            return 0;
        }

        $list = rar_list($rar_file);
        foreach($list as $file) {
            $entry = rar_entry_get($rar_file, $file);
            $entry->extract($dest); // extract to the current dir
        }
        rar_close($rar_file);
        return 1;
    } else {
        log_action("Failed to read Rar archive: $file",__FILE__,__LINE__);
        return 0;
    }
}

/* zip data line process
 * unzip, attachments processing
 * create sheet_data,theader SESSION variables for upload_processing
 *
 * */
function packed_data_line_process($F,$append=false) {
    switch ($F['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            echo common_message('error','No file sent.');
            return;
        case UPLOAD_ERR_INI_SIZE:
            echo common_message('error','The uploaded file exceeds the upload_max_filesize directive in php.ini.');
            return;
        case UPLOAD_ERR_FORM_SIZE:
            echo common_message('error','Exceeded filesize limit.');
            return;
        default:
            echo common_message('error','Unknown errors.');
            return;
    }

    // unpack zip destination temporary directory 
    $dest = sprintf('%s%s/zipdir/%s_dir/',OB_TMP,session_id(),$F['name']);
    if (is_dir($dest)) {
        echo common_message('error','Failed upload files, see logs for more details.');
        log_action("Temporary directory unexpectedly exists: $dest",__FILE__,__LINE__);
        return;
    }

    if (!unpack_zip($F['tmp_name'],$dest)) {
        echo common_message('Failed unpacking files');
        if (is_dir($dest)) {
            //might be not empty...
            //foreach($drop_files as $f)
            //    unlink($dest.$f);
            rmdir($dest);
            rmdir(sprintf('%s%s/zipdir/',OB_TMP,session_id()));
            rmdir(sprintf('%s%s/',OB_TMP,session_id()));
        }
        return;
    }
    
    include_once(getenv('OB_LIB_DIR').'read_uploaded_file.php');
    $ruf = new read_uploaded_file();

    // create $_FILES like array from zip content
    
    # Old Mobile App export data format
    # Archive:  Mon May 07 11:29:03 CEST 2018.zip
    #  Length      Date    Time    Name
    #---------  ---------- -----   ----
    #       12  2018-05-14 10:38   note.txt
    #       26  2018-05-14 10:38   geometry.wkt
    #  1590977  2018-05-14 10:38   IMG_20180507_112730.jpg
    #  1508545  2018-05-14 10:38   IMG_20180507_112743.jpg
    #  1216705  2018-05-14 10:38   IMG_20180507_112800.jpg
    #---------                     -------
    #  4316265                     5 files

    $zip_contents = array('data'=>'','type'=>'','geometry'=>'','attachments'=>array('tmp_name'=>array(),'name'=>array(),'type'=>array(),'error'=>array()));
    $handle = opendir($dest);
    $drop_files = array();

    while (false !== ($entry = readdir($handle))) {
        if (!is_file($dest.$entry))
            continue;
        $drop_files[] = $entry;

        if ($entry == 'note.txt') {
            // old obm_app compatibility
            //$zip_contents['data'] = $entry;
            $zip_contents['type'] = 'CSV';
            $zip_contents['data'] = 'note.csv';
            $note = file($dest.$entry);
            
        } elseif ($entry == 'data.json') {
            // theoretical advanced zip upload
            $zip_contents['data'] = $entry;
            $zip_contents['type'] = 'JSON';
            $note = file($dest.$entry);

        } elseif ($entry == 'geometry.wkt') {
            // old obm_app compatibility
            $wkt = file($dest.$entry);

        } else {
            $zip_contents['attachments']["tmp_name"][] = $dest.$entry;
            $zip_contents['attachments']['name'][] = $entry;
            $zip_contents['attachments']['type'][] = '';
            $zip_contents['attachments']['error'][] = UPLOAD_ERR_OK;

        }
    }
    closedir($handle);

    //old style zip export processing
    if ($zip_contents['data']=='note.csv') {
        
        // flip coordinates due to old app format...
        // POINT(47.171012 19.055104)
        if (preg_match('/\((\d+\.\d+) (\d+\.\d+)\)/',$wkt[0],$m)) {
            $wkt[0] = "POINT($m[2] $m[1])";
        }
        $datum = preg_replace('/\.zip$/','',$F['name']);
        $files_list = sprintf(implode(',',$zip_contents['attachments']['name']));
        $zip_contents['data'] = array_combine($_SESSION['theader'],array($wkt[0],$files_list,$note[0],$datum));
    } elseif ($zip_contents['data']=='data.json') {
        $zip_contents['data'] = json_decode($note,true);
    }

    $r = move_upl_files($zip_contents['attachments'],getenv('PROJECT_DIR').'attached_files');
    $rr = json_decode($r);
    $rr->{'file_names'} = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});
    
    if (isset($zip_contents['data']['obm_files_id']))
        $zip_contents['data']['obm_files_id'] = implode(',',$rr->{'file_names'});
    
    $res = create_upload_temp(array($zip_contents['data']),$append);

    // cleaning zip directory
    // unlink(/mnt/data/tmp/j1n34pc8o55i1u2uagn3312r7t/zipdir/stuff.zip_dir/Sun May 13 08:52:51 CEST 2018.zip)
    if (is_dir($dest)) {
        foreach($drop_files as $f)
            unlink($dest.$f);
        rmdir($dest);
        rmdir(sprintf('%s%s/zipdir/',OB_TMP,session_id()));
        rmdir(sprintf('%s%s/',OB_TMP,session_id()));
    }
    return "done";
}

/* Copy readed CSV, GPX,... content to temporary database table
 * Used in read_uploades_file.php, pds_class.php 
 * */
function create_upload_temp($sheetData=array(),$append=false,$interconnect=false) {
    global $ID;

    if ($append) {
        $cmd = sprintf("SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables 
                WHERE  table_schema = 'temporary_tables' AND table_name = 'upload_%s_%s')",PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_row($res);
            if ($row[0]=='f')
                $append = false;
        }
    }
    if (!$append) {
        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.upload_%s_%s",PROJECTTABLE,session_id()));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.upload_%s_%s (row_id integer, skip_marked boolean DEFAULT false NOT NULL, data json)",PROJECTTABLE,session_id());
        pg_query($ID,$cmd);

        update_temp_table_index (PROJECTTABLE.'_'.session_id(),'upload',$interconnect);
        $row_num = 1;
    } else {
        $cmd = sprintf("SELECT max(row_id) FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_row($res);
            $row_num = $row[0]+1;
        } else
            return false;
    }

    pg_query($ID, sprintf("COPY temporary_tables.upload_%s_%s FROM stdin",PROJECTTABLE,session_id()));
    
    foreach ($sheetData as $data_line) {
        # skip empty lines
        if (implode("",$data_line) == "") continue;
        
        $line = array();
        $line[] = $row_num;
        $line[] = 0;
        $n_line = json_encode($data_line,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT);
        # \n problem
        # invalid input syntax for type json\nDETAIL:  The input string ended unexpectedly....
        $n_line = str_replace("\\n", "", $n_line);
        # \t problem
        $n_line = str_replace("\\t", "", $n_line);
        # backslash problem
        $n_line = preg_replace('/\\\+/','',$n_line);

        $line[] = $n_line;
        $line_string = implode("\t",$line);
        pg_put_line($ID, $line_string."\n");
        $row_num++;
    }

    pg_put_line($ID, "\\.\n");
    pg_end_copy($ID);
    
    return true;
}
/* Read offset - count rows data from sheetData temp table
 *
 * */
function get_sheet_page($from,$count) {
    global $ID;

    if ($from==0 and $count==-1)
        $cmd = sprintf("SELECT row_id,skip_marked,data FROM temporary_tables.upload_%s_%s ORDER BY row_id",PROJECTTABLE,session_id());
    else
        $cmd = sprintf("SELECT row_id,skip_marked,data FROM temporary_tables.upload_%s_%s ORDER BY row_id LIMIT %d OFFSET %d",PROJECTTABLE,session_id(),$count,$from);

    $res = pg_query($ID,$cmd);
    $d = array();
    while ($row = pg_fetch_assoc($res)) {
        $d[] = json_decode($row['data'],true);
    }
    return $d;
}
/*
 *
 * */
function get_upload_count() {
    global $ID;

    $cmd = sprintf("SELECT COUNT(*) AS c FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return $row['c'];
    }
    return 0;
}
/*
 *
 * */
function get_sheet_row($n) {
    global $ID;
    $cmd = sprintf("SELECT * FROM temporary_tables.upload_%s_%s WHERE row_id=%d",PROJECTTABLE,session_id(),$n);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        return pg_fetch_assoc($res);
    }
    return false;
}
/*
 *
 * */
function update_sheet_row($n,$data,$skip='NA') {
    global $ID;
    if ($skip!='NA') {
        $skip_marked = 'false';
        if ($skip) $skip_marked = 'true';
        $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET data=%s,skip_marked=%s WHERE row_id=%d",PROJECTTABLE,session_id(),quote(json_encode($data)),$skip_marked,$n);
    } else
        $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET data=%s WHERE row_id=%d",PROJECTTABLE,session_id(),quote(json_encode($data)),$n);

    $res = pg_query($ID,$cmd);
    if (pg_affected_rows($res)) {
        return true;
    }
    return false;
}
/*
 *
 * */
function update_sheet_skips($n,$skip) {
    global $ID;
    $skip_marked = 'false';
    if ($skip) $skip_marked = 'true';

    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked=%s WHERE row_id=%d",PROJECTTABLE,session_id(),$skip_marked,$n);

    $res = pg_query($ID,$cmd);
    if (pg_affected_rows($res)) {
        return true;
    }
    return false;
}
/*
 *
 * */
function add_sheet_row($n,$data,$skip='false') {
    global $ID;

    if ($skip == 1)
        $skip = 'true';
    elseif ($skip == 0)
        $skip = 'false';
    
    if ($skip!='true') 
        $skip_marked = 'false';
    else 
        $skip_marked = 'true';

    if ($n == '') {
        $cmd = sprintf("SELECT max(row_id)+1 FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_row($res);
            $n = $row[0];
        }
    }

    $cmd = sprintf("INSERT INTO temporary_tables.upload_%s_%s (row_id,data,skip_marked) VALUES (%d,%s,%s)",PROJECTTABLE,session_id(),$n,quote(json_encode($data)),$skip_marked);
    $res = pg_query($ID,$cmd);
}
/*
 *
 * */
function get_sheet_skips($from,$count) {
    global $ID;

    if ($from==0 and $count==-1)
        $cmd = sprintf("SELECT skip_marked FROM temporary_tables.upload_%s_%s ORDER BY row_id",PROJECTTABLE,session_id(),$count,$from);
    else
        $cmd = sprintf("SELECT skip_marked FROM temporary_tables.upload_%s_%s ORDER BY row_id  LIMIT %d OFFSET %d",PROJECTTABLE,session_id(),$count,$from);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        return pg_fetch_all($res);
    }
    return false;
}
/*
 *
 * */
function get_row_skip($n) {
    global $ID;

    $cmd = sprintf("SELECT skip_marked FROM temporary_tables.upload_%s_%s WHERE row_id = %d",PROJECTTABLE,session_id(),$n);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return $row['skip_marked'];
    }
    return false;

}
/*
 *
 * */
function get_sheet_header() {
    global $ID;

    $cmd = sprintf("SELECT data FROM temporary_tables.upload_%s_%s WHERE row_id = 1 LIMIT 1",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return array_keys(json_decode($row['data'],true));
    }
    return false;
}

/* Processes the trigger function from the upload form definition */ 
function process_trigger($row) {
    $t = ['function' => ''];
    //{"list": {}, "table": "milvus_sampling_units","valueColumn": "obm_geometry", "labelColumn": "", "filterColumn": "obm_id", "pictures": [],"triggerTargetColumn": [],"Function":"get_value"}
    if ($row['list_definition']!='') {
        $j = json_decode($row['list_definition'],TRUE);
        if (isset($j['Function'])) {
            $t['foreign_key'] = isset($j['filterColumn']) ? $j['filterColumn'] : "";
            $t['function'] = isset($j['Function']) ? $j['Function'] : "";
            if (isset($j['triggerTargetColumn']) and is_array($j['triggerTargetColumn'])) $t['target_column'] = $j['triggerTargetColumn'];
            elseif (isset($j['triggerTargetColumn'])) $t['target_column'] = explode(';',$j['triggerTargetColumn']);
            else $t['target_column'] = array();
            $t['table'] = isset($j['optionsTable']) ? $j['optionsTable'] : "";
            $t['value_col'] = isset($j['valueColumn']) ? $j['valueColumn'] : "";
            $t['labelAsValue'] = isset($j['labelAsValue']) ? $j['labelAsValue'] : "";

            return $t;
        }
        return false;
    }
    /*if ($row['control'] == 'trigger') {
        if (preg_match('/get_value\((\w+)?\.(\w+)\:?(\w+)?\)/',$row['custom_function'],$sl)) {
            $t['function'] = 'get_value';
            $t['table'] = $sl[1];
            $t['id_col'] = $sl[2];
            $t['value_col'] = $sl[3];
        }
        elseif (preg_match('/select_list\((\[?\w+(?:(?:,\w+)+)?\]?)?\,(\^?\w+)?\)/',$row['custom_function'],$sl)) {
            $t['function'] = 'select_list';
            $t['target_column'] = explode(',',trim($sl[1],'[]'));
            $t['foreign_key'] = $sl[2];
        } 
        else 
            return false;
        return $t;
    }
    else*/ 
        return false;
}

class upload_select_list {

    const PATTERN = '/SELECT:(\w+)\[?(\w+(?:(?:,\w+)+)?)?\]?\.(\w+):?(\w+)?/'; //pattern with filter (eg. SELECT:table[filter_col,filter_value].value:label)
                                                                               //                         SELECT:milvus_sampling_units.obm_id:alapegyseg
    var $options = [];
    var $data_attr = '';
    var $multiselect = false;
    var $selected = null;

    function __construct ($form_element, $term = '') {

        $this->list_definition = $form_element['list_definition'];
        $this->list = $form_element['list'];
        $this->type = $form_element['type'];
        $this->control = $form_element['control'];
        $this->custom_function = $form_element['custom_function'];
        $this->default_value = $form_element['default_value'];
        $this->ajaxCall = (substr((debug_backtrace()[0]['file']),-10) == 'afuncs.php');

        if ($this->list_definition != '') {
            $j = json_decode($this->list_definition,TRUE);
            if (isset($j['list']) and count($j['list'])) {
                // simple list
                $this->from_list(0,$j);
            
            } elseif (isset($j['optionsTable']) and isset($j['valueColumn']) ) {
                if (isset($j['preFilterColumn']) and $j['preFilterColumn']!='') {
                    $relation = (isset($j['preFilterRelation'])) ? $j['preFilterRelation'] : null;
                    $preFilter = prepareFilter($j['preFilterColumn'],$j['preFilterValue'],$relation);
                }
                else
                    $preFilter = "1 = 1";

                $preFilter .= ($term) 
                    ? ' AND ' . prepareFilter($j['valueColumn'],"$term%",'ILIKE')
                    : '';

                if (isset($j['labelColumn']) and $j['labelColumn']!='' )
                    $labelColumn = $j['labelColumn'];
                else
                    $labelColumn = $j['valueColumn'];

                $schema = (isset($j['optionsSchema']) && $j['optionsSchema'] !== '') ? $j['optionsSchema'] : 'public';

                $this->from_table(array('',$j['optionsTable'],$preFilter,$j['valueColumn'],$labelColumn, $schema));
            }
            else {
                log_action('Missing list definition elements',__FILE__,__LINE__);
            }

            if (isset($j['multiselect']) and $j['multiselect']=='true') {
                $this->multiselect = true;
            }

            if (isset($j['selected']) and is_array($j['selected'])) {
                $this->selected = $j['selected'];
            }
        
        } else {
            //backward compatibility
            if (preg_match(self::PATTERN,$this->list,$m)) {
                $this->from_table($m);
            } else
                $this->from_list(1);
        }
        /*if (preg_match(self::PATTERN,$this->list,$m)) {
            $this->from_table($m);
        }
        else {
            $this->from_list();
        }*/
    }

    /***************
     * private functions
     * ****************/

    private function from_table($m) {
        global $ID;

        $schema = $m[5];
        $this->table = $m[1];
        $value_col = $m[3];
        if (isset($m[4]))
            $label_col = $m[4];
        else
            $label_col = $m[3];
        $f = explode(',',$m[2]);        // hol létezik ilyen?????

        if ($this->has_default_value()) {
            $filter = sprintf('%s = %s',$value_col,$this->def);
            $this->data_attr = 'data-nested="default"';
        }
        else {
            $filter = $m[2]; 
        }

        $bar = sprintf("%s AS bar",$value_col);
        $baro = $value_col;
        if (isset($label_col) and $label_col != '') {
            $bar = sprintf("%s AS bar",$label_col);
            $baro = $label_col;
        }

        $sl = process_trigger(["control" => $this->control,"custom_function" => $this->custom_function,"list_definition" => $this->list_definition]);

        $rules = $this->sensitivity_check();

        if ($sl && $sl['target_column'] != [] && $this->data_attr == '') {
            $this->data_attr = 'data-nested="parent"';
        }
        if ($sl && $sl['foreign_key'] != []) {
            $this->data_attr .= ' data-nested_child="true"';
        }

        if (!isset($sl['foreign_key']) || $sl['foreign_key'] == 'NULL' || $sl['foreign_key'] == '' || $this->ajaxCall) {
            $conntable = '';
            $a = '';
            if ($this->ajaxCall && !in_array($this->type, ['autocomplete','autocompletelist'])) {
                if ($sl['foreign_key'][0] == '^') {
                    // very special thing, would be better in module, 
                    // table exists check missing....
                    $conntable = sprintf('JOIN public.%s_connections c ON t.obm_id = c.child_id AND c.conn_name = %s',PROJECTTABLE,quote(trim($sl['foreign_key'],'^')));
                    $a = sprintf('AND c.parent_id=%s',quote($_POST['condition']));
                }
                else {
                    $a = sprintf('AND %s = %s',$sl['foreign_key'], quote($_POST['condition']));
                }
            }

            $cmd = sprintf('SELECT DISTINCT %1$s foo, %3$s FROM %10$s.%2$s t %9$s %6$s WHERE %5$s %7$s %8$s ORDER BY %4$s',$value_col,$this->table, $bar, $baro, $filter, $rules['join'], $rules['filter'], $a, $conntable, $schema);
            $res = pg_query($ID,$cmd);

            while ($row = pg_fetch_assoc($res)) {
                $rowbar = (defined($row['bar'])) ? constant($row['bar']) : $row['bar'];
                
                $dataAttr = (substr($this->data_attr,0,11) == 'data-nested') ? ['fkey' => $row['foo']] : [];

                if ($sl['labelAsValue'] != '') 
                    $this->options[] = array('value' => $rowbar, 'selected' => '', 'disabled' => '', 'label' => array($rowbar), 'data' => $dataAttr);
                else
                    $this->options[] = array('value' => $row['foo'], 'selected' => '', 'disabled' => '', 'label' => array($rowbar), 'data' => $dataAttr);
            }

        }
        $this->ssiz = (count($this->options)==2) ? "size='2'" : "";
    }

    // old_style : backward compatibility bit
    private function from_list($old_style,$j=null) {

        if (!$old_style) {

            foreach ($j['list'] as $value=>$keys) {
                if ($value === '_empty_')
                    $value = '';
                if (!count($keys)) {
                    // if no label defined e.g. true,false, label is created from the value
                    $keys[] = $value;
                }
                $l_translated = array();
                foreach($keys as $label) {
                    if ($label === 'true') $l_translated[] = constant("str_".$label);
                    elseif ($label === 'false') $l_translated[] = constant("str_".$label);
                    elseif( defined($label)) $l_translated[] = constant($label);
                    else $l_translated[] = $label;
                }
                $disabled = '';
                if (isset($j['disabled']) and array_search($value,$j['disabled'])!==false)
                    $disabled = 'disabled';
                $selected = '';
                if (isset($j['selected']) and array_search($value,$j['selected'])!==false)
                    $selected = 'selected';
                
                $bgImage = '';
                if (isset($j['pictures']) and array_key_exists($value,$j['pictures'])) {
                    $bgImage = $j['pictures'][$value];
                }
                $this->options[] = array('value' => $value, 'selected' => $selected, 'disabled' => $disabled, 'label' => $l_translated, 'bgImage' => $bgImage);  
            }

            $this->ssiz = (count($this->options)==2) ? "size='2'" : "";
            return;
        }
        /* normál lista név:érték párok
         * female#nőstény:f,male#hím:m
         * female#nőstény:f:disabled,male#hím:m
         * */
        foreach( str_getcsv($this->list,",","'") as $l) {
            //foreach(explode(',',$r2['list']) as $l) 
            $tl = mb_split(':',$l);
            $disabled = '';
            $selected = '';
            if(count($tl)>1) {
                // multi lables support; in web form - the first will be used
                #if ($table_type=='file') {
                $l_text = preg_split('/#/',$tl[0]);
                #} else {
                #    $labels = preg_split('/#/',$tl[0]);
                #    $l_text = $labels[0];
                #}

                $l_val = $tl[1];
                if(isset($tl[2]))
                    $disabled = 'disabled';
            } else {
                $l_text = array($l);
                $l_val = $l;
            }
            $l_translated = array();
            foreach($l_text as $l_text_p) {
                if ($l_text_p == 'true') $l_translated[] = constant("str_".$l_text_p);
                elseif ($l_text_p == 'false') $l_translated[] = constant("str_".$l_text_p);
                elseif( defined($l_text_p)) $l_translated[] = constant($l_text_p);
                else $l_translated[] = $l_text_p;
            }

            if(trim($l_val)=='') $l_val=$l_text[0];
            $this->options[] = array('value' => $l_val, 'disabled' => $disabled, 'label' => $l_translated, 'selected' => '');
        }
        // ez kell????
        $this->ssiz = (count($this->options)==2) ? "size='2'" : "";
    }

    private function has_default_value() {
        if (preg_match('/^_list:(.+)/',$this->default_value,$def)) {
            $this->def = $def[1];
            return true;
        }
        else {
            if ($this->type == 'list') 
                $this->add_empty_line();
            return false;
        }
    }

    private function add_empty_line() {
        $this->options[] = ['value' => '','label' => [''], 'disabled' => '', 'selected' => ''];
    }

    /****************
     * public functions
     * *****************/

    public function get_data_attribute() {
        return $this->data_attr;
    }

    public function get_options($format = 'html') {
        if ($format == 'array')
            return $this->options;
        elseif ($format == 'html') {
            $html = [];


            foreach ($this->options as $row) {

                $dataAttr = '';
                if (isset($row['data']) && !empty($row['data'])) {
                    foreach ($row['data'] as $dataKey => $dataValue) {
                        $dataAttr .= "data-$dataKey=\"$dataValue\" ";
                    }
                }
                $html[] = "<option value=\"{$row['value']}\" {$row['disabled']} {$row['selected']} $dataAttr>{$row['label'][0]}</option>";
            }
            return $html;
        }
        else
            log_action('Unknown format',__FILE__,__LINE__);
            
    }

    /* Sensitivity check and rules table join for dynamic select lists */
    public function sensitivity_check() { 
        $tid = (!isset($_SESSION['Tid'])) ? 0 : $_SESSION['Tid'];
        $tgroups = (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='') ? 0 : $_SESSION['Tgroups'];
        $st_col = st_col($this->table,'array');

        $rules['filter'] = '';
        $rules['join'] = '';
        /* Nem működnek így sok felhasználónak a select listák, amiknek nem is kellene szabályozottnak lennie!!!!
         *
         * if ($st_col['RESTRICT_C']) {
            if (!grst(PROJECTTABLE,4)) {
                $rules['filter'] = " AND ( (sensitivity::varchar IN ('1','no-geom') AND ARRAY[$tgroups] && read)  OR sensitivity::varchar IN ('0','public') ) ";
                $rules['join'] = sprintf("LEFT JOIN %s_rules r ON (obm_id=r.row_id AND data_table = %s)",PROJECTTABLE,quote($this->table));
            }
        }*/
        return $rules;
    }
}
/* terms and conditions modal dialog
 *
 * */
function agree_new_terms() {
    global $BID;

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    if (defined(LANGUAGES)) 
        $languages = (is_array(LANGUAGES)) ? LANGUAGES : explode(',',LANGUAGES); 
    else 
        $languages = ['en','hu','ro'];
    
    $consts = ['str_privacy_policy','str_terms_and_conditions','str_cookie_policy','str_private_policy_info','str_policy_agree_info','str_agree_new_terms','str_agree_and_continue','str_decline'];

    $cmd = sprintf("SELECT * FROM translations WHERE (scope = 'global' OR project = '%s') AND lang IN (%s) AND const IN (%s)", PROJECTTABLE, implode(',',array_map('quote',$languages)), implode(',', array_map('quote',$consts)));

    if ($res = pg_query($BID, $cmd)) {
        while ($row = pg_fetch_assoc($res)) {
            $translations[$row['lang']][$row['scope']][$row['const']] = $row['translation'];
        }
    }

    if (isset($_SESSION['Tid']) and (!isset($_SESSION['Tterms_agree']) or (isset($_SESSION['Tterms_agree']) && !$_SESSION['Tterms_agree'] ) ) ) {
        $out = "<script> $('body').css('overflow-y','hidden'); </script>
            <div id='new-terms-agree-div'>
            <div id='new-terms-container'>";
        foreach ($languages as $L) {
            $active = ($L == 'en') ? 'active' : '';
            $out .= "<span class='terms-langswitch flag-icon flag-icon-$L $active' data-lang='$L' style='margin: 0 5px;'></span>";
        }

        $out .= " <a href='$protocol://".URL."/index.php?logout' class='terms-cancel'>X</a>";

        foreach ($languages as $lang) {
            $tr = [];
            foreach ($consts as $const) 
                $tr[$const] = (isset($translations[$lang]['local'][$const])) ? $translations[$lang]['local'][$const] : $translations[$lang]['global'][$const] ;

            $hidden = ($lang == 'en') ? '' : 'hidden';
            $out .= "<div id='terms-$lang' class='$hidden'>
            <div id='private-policy-info'>".$tr['str_private_policy_info']."</div>
            ".$tr['str_policy_agree_info']." 
            <br><br>
            ".$tr['str_agree_new_terms']."
            <div style='display: flex; justify-content: space-around; margin: 20px 0;'>
                <a href='' class='termsText' data-target='privacy'> " . $tr['str_privacy_policy'] . " </a> 
                <a href='' class='termsText' data-target='terms'> " . $tr['str_terms_and_conditions'] . " </a> 
                <a href='' class='termsText' data-target='cookies'> " . $tr['str_cookie_policy'] . " </a> 
            </div>
            <div class='terms-text'>ide jön a szöveg</div>
            <div id='new-terms-buttons'>
                <button id='agree-new-terms' class='pure-button button-large'>".$tr['str_agree_and_continue']."</button>  
                <button id='drop_my_account' class='pure-button button-large'>".$tr['str_decline']."</button>
                </div></div>";
        }
        $out .= "
            </div>
            </div>";
        echo $out;
    }
}

/* interconnect_request handling
 * Called in prepare_vars.php when connection request arrived
 * */
function interconnect_request($key,$request_project) {
    global $BID;

    if (defined("OB_PROJECT_DOMAIN")) {
        $domain = constant("OB_PROJECT_DOMAIN");
        $server_email = PROJECTTABLE."@".parse_url("$protocol://".$domain,PHP_URL_HOST);
        $reply_email = "noreply@".parse_url("$protocol://".$domain,PHP_URL_HOST);
    } else {
        $domain = $_SERVER["SERVER_NAME"];
        $server_email = PROJECTTABLE."@".$domain;
        $reply_email = "noreply@".$domain;
    }

    $cmd = sprintf("SELECT u.email AS email,p.email AS aemail, u.username AS name FROM projects p 
        LEFT JOIN project_users pu ON (pu.project_table=p.project_table) 
        LEFT JOIN users u ON (user_id=u.id) 
        WHERE p.project_table='%s' AND user_status::varchar IN ('2','master') AND receive_mails!=0",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $emails = array();
    $names = array();
    while ( $row = pg_fetch_assoc($res)) {
        $names[] = $row['name'];
        $emails[] = $row['email'];
        //$emails[] = $row['aemail'];
    }
    $emails = array_unique($emails);

    // key processing
    $accept_key = '';
    $cmd = sprintf("SELECT accept_key FROM interconnects_slave WHERE accept_key=%s AND remote_project=%s AND pending=false AND local_project=%s",quote($key),quote($request_project),quote(PROJECTTABLE));
    $res = pg_query($BID,$cmd);
    
    if (pg_num_rows($res)) {
        return true;

    } else {
        if (!count($emails)) return "Mailing to admins not enabled, or no mail addresses available.";

        // no connection established yet
        // send alert admin to accept or decline connection request
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.=!+[]$*&@{}#<>";
        $hash = substr( str_shuffle( $chars ), 0, 16 );
        $accept_key = crypt($hash.microtime(),$request_project);
        
        $cmd = sprintf("INSERT INTO interconnects_slave (local_project,remote_project,accept_key,pending) VALUES(%s,%s,%s,true)",quote(PROJECTTABLE),quote($request_project),quote($accept_key));
        $res = pg_query($BID,$cmd);
    
        $Subject = "$request_project interconnect request in ". PROJECTTABLE;

        $Title = 'Hi '.implode(', ',$names).'!';

        $Message = "Follow this link to accept interconnect request:<br>";
        $Message .= "&nbsp; &nbsp; <a href='http://".URL."/index.php?interconnect_project=".base64_encode($request_project)."&interconnect_request_key=$key&interconnect_accept_key=$accept_key&decision=accept'>http://".URL."/index.php?interconnect_project=".base64_encode($request_project)."&interconnect_request_key=$key&interconnect_accept_key=$accept_key&decision=accept</a><br><br>";
        $Message .= "Follow this link to reject interconnect request:<br>";
        $Message .= "&nbsp; &nbsp; <a href='http://".URL."/index.php?interconnect_project=".base64_encode($request_project)."&interconnect_request_key=$key&interconnect_accept_key=$accept_key&decision=decline'>http://".URL."/index.php?interconnect_project=".base64_encode($request_project)."&interconnect_request_key=$key&interconnect_accept_key=$accept_key&decision=reject</a>";

        $m = mail_to($emails,$Subject,"$server_email","$reply_email",$Title,$Message,"multipart");

    }

    return true;
}
/* Interconnect function
 * local admin decision to accept or reject remote OBM server/project connection
 * Called in prepapre_vars.php
 * */
function interconnect_key_accept($decision,$accept_key,$request_key,$request_project) {
    global $BID,$ID;

    if ($decision=='accept') {
        $cmd = sprintf("UPDATE interconnects_slave SET pending=false WHERE accept_key=%s AND remote_project=%s AND local_project=%s",quote($accept_key),quote(base64_decode($request_project)),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);

        if (pg_affected_rows($res)) {
            
            $answer = file_get_contents("http://".base64_decode($request_project)."/index.php?interconnect_request_decision=accept&accept_key=$accept_key&request_key=$request_key");
            // update temporary_tables post file 
            $old_session_id = '';
            $cmd = sprintf("SELECT table_name FROM system.temp_index WHERE interconnect='t' AND table_name ~ 'upload_%s_[A-Za-z0-9]+_nk_%s'",PROJECTTABLE,$request_key);
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if (preg_match('/upload_'.PROJECTTABLE.'_([A-Za-z0-9]+)_nk_'.$request_key.'/',$row['table_name'],$m)) {
                    $old_session_id = $m[1];
                }
            }

            $cmd = sprintf('UPDATE system.temp_index SET table_name=\'upload_%1$s_%2$s_wk_%4$s\' WHERE interconnect=\'t\' AND table_name=\'upload_%1$s_%2$s_nk_%3$s\'',PROJECTTABLE,$old_session_id,$request_key,$accept_key);            
            if (pg_query($ID,$cmd)) {
                $cmd = sprintf('ALTER TABLE temporary_tables."upload_%1$s_%2$s_nk_%3$s" RENAME TO "upload_%1$s_%2$s_wk_%4$s"',PROJECTTABLE,$old_session_id,$request_key,$accept_key);
                $res = pg_query($ID,$cmd);
            
                return $accept_key;
            }
        } else {
            log_action('Error in interconnects update',__FILE__,__LINE__);
        }
        

    } elseif($decision=='decline') {
        $cmd = sprintf("DELETE FROM interconnects_slave WHERE pending=true AND accept_key=%s AND remote_project=%s AND local_project=%s",quote($accept_key),quote(base64_decode($request_project)),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);

        $answer = file_get_contents("http://".base64_decode($request_project)."/index.php?interconnect_request_decision=decline&accept_key=$accept_key&request_key=$request_key");
        
        // Delete post data from temporary_tables
        // upload_sandbox_q201ltm400uhnp56hj80p6k740_nk_request_key
        $cmd = sprintf("DELETE FROM system.temp_index WHERE interconnect='t' AND table_name ~ 'upload_%s_[A-Za-z0-9]+_nk_%s'",PROJECTTABLE,$request_key);
        $res = pg_query($ID,$cmd);

        if (pg_affected_rows($res))
            return true;
        else
            log_action('Error in interconnects update',__FILE__,__LINE__);
    }
    return false;
}
/* Interconnect request sent and answered: "request received"
 * Called in afuncs.php as an AJAX action: User initiated a data sharing  
 * */
function interconnect_master_connect($remote_project) {
    global $BID;

    $cmd = sprintf("SELECT accept_key FROM interconnects_master WHERE local_project=%s AND remote_project=%s AND accept_key!='' AND pending=false",quote(PROJECTTABLE),quote($remote_project));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return array('accept_key'=>$row['accept_key']);
    }

    // generating a request key
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.=!+[]$*&@{}#<>";
    $hash = substr( str_shuffle( $chars ), 0, 16 );
    $request_key = crypt($hash.microtime(),$remote_project);

    $cmd = sprintf("SELECT 1 FROM interconnects_master WHERE pending=true AND local_project=%s AND remote_project=%s",quote(PROJECTTABLE),quote($remote_project));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        return array('failed','There is an active connection request to this project which not yet accepted by remote admin. Please wait for response and share this data again.');
    }

    $cmd = sprintf("INSERT INTO interconnects_master (local_project,remote_project,request_key,pending) VALUES(%s,%s,%s,true)",quote(PROJECTTABLE),quote($remote_project),quote($request_key));
    $res = pg_query($BID,$cmd);
    if (pg_affected_rows($res))
        return array('request_key'=>$request_key);
    else {
        log_action("failed to insert request_key to interconnects_master",__FILE__,__LINE__);
        return array('failed','An error occured in key processing.');;
    }
}
/* Remote project's admin answered: accept or decline 
 * Called: prepare_vars.php
 * when remote admin accepted or declined connection request
 * */
function interconnect_got_decision($decision,$accept_key,$request_key) {
    global $BID;

    if ($decision == 'accept') {
        $cmd = sprintf("UPDATE interconnects_master SET accept_key=%s,pending=false WHERE request_key=%s AND pending=true AND local_project=%s",quote($accept_key),quote($request_key),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res))
            return true;
    } else if ($decision == 'decline') {
        $cmd = sprintf("DELETE FROM interconnects_master WHERE request_key=%s AND pending=true AND local_project=%s",quote($request_key),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res))
            return true;
    }
    return false;
}

/**
 * Converts array to the WHERE part of an SQL string
 *
 * @param string|array $col the column or the list of columns used in the filter
 * @param string|array $val the value or list of values used in the filter 
 * @param string|array $rel optional parameter if relation is other then '=' or 'IN'
 * complex example 
 * $col = ['col1','col2','col3']
 * $val = ['val1','val2',['val3a',val3b']]
 * $rel = ['=','<','IN']
 *
 * @return string $preFilter
 *
 * $preFilter == "col1 = 'val1' AND col2 < 'val2' AND col3 IN ('val3a','val3b') "
 **/
function prepareFilter($col, $val, $rel = NULL) {
    if (is_null($col) || is_null($val)){
        log_action('ERROR: filter col or val missing');
        return;
    }
    if (!is_array($col) && !is_array($val)) {
        $col = array($col);
        $val = array($val);
    }
    elseif (!is_array($col) && is_array($val)) {
        $col = array($col);
        $val = array($val);
    }
    elseif (is_array($col) && (!is_array($val) || (count($val) !== count($col)))) {
        log_action('ERROR: Pre filter column and value number do not match');
        return;
    }

    $n = count($col);
    if (isset($rel)) { 
        if (!is_array($rel))
            $rel = array($rel);
        if ($n !== count($rel)) 
            log_action('WARNING: pre-filter columns, values and relations number do not match!',__FILE__,__LINE__);
    }

    $preFilter = [];
    for ($i = 0; $i < $n; $i++) {
        if (is_array($val[$i])) { 
            $value = "(".implode(',',array_map('quote',$val[$i])) . ")";
            $relation = (isset($rel[$i])) ? $rel[$i] : 'IN';
        }
        else {
            if ($val[$i] === 'NULL') {
                $value = $val[$i];
                $relation = (isset($rel[$i])) ? $rel[$i] : 'IS';
            }
            else { 
                $value = quote($val[$i]);
                $relation = (isset($rel[$i])) ? $rel[$i] : '=';
            }
        }

        $preFilter[] = sprintf( " %s %s %s ", $col[$i], $relation, $value); 
    }
    return implode('AND',$preFilter);
}
class input_field {
    private $defaults = [
        'element' => 'input',
        'type' => 'text',
        'id' => '',
        'class' => '',
        'placeholder' => '',
        'alt' => '',
        'checked' => '',
        'disabled' => '',
        'max' => '',
        'min' => '',
        'maxlength' => '',
        'size' => '',
        'multiple' => '',
        'name' => '',
        'pattern' => '',
        'required' => '',
        'step' => '',
        'value' => '',
        'empty_line' => true,
        'tdata' => [],
        'data_atts' => '',
        'onlyoptions' => false,
        'data' => [],
        'selected' => '',
        'style' => 'width: 100%',
        'debug' => false
    ];
    private $args = [];

    function __construct($args) {
        // reading the arguments
        foreach ($this->defaults as $prop => $value) {
            $this->args[$prop] = (!isset($args[$prop])) ? $value : $args[$prop] ;
        }

        if (!empty($this->args['tdata'])) {
            $this->args['data_atts'] = $this->tdata($this->args['tdata']);
        }

        if ($this->args['debug'])
            log_action($this->args,__FILE__,__LINE__);
    }

    public function getElement() {
        $type = 'push_'.$this->args['element'];
        return $this->$type();
    }

    // creates an input field for autocomplete input
    private function push_input() {

        $input_atts = ['id','type','class','placeholder','name','pattern','autofocus','autocomplete','max','min','maxlength','required','size','step','value'];
        $atts = [];
        foreach ($input_atts as $attr) {
            if (isset($this->args[$attr]) && $this->args[$attr]) 
                $atts[] = "$attr='{$this->args[$attr]}'";
        }
        return sprintf("<input %s %s >",implode(' ',$atts),$this->args['data_atts']);
    }

    // creates a select input fields with options queried from a table
    private function push_select($filter='') {
        $op = ($this->args['empty_line']) ? "<option value=''></option>" : "";

        if (($this->args['placeholder'] != '') && ($this->args['selected'] == ''))
            $op .= "<option hidden selected disabled value=''>{$this->args['placeholder']}</option>";
            
        foreach ($this->args['data'] as $row) {
            if ($this->args['size'] > 1 and $row['value'] == '') {
                continue;
            }
            
            $value = $row['value'];
            $label = (isset($row['label'])) ? $row['label'] : $row['value'];
            $label = (defined($label)) ? constant($label) : $label;
            $tdata = (isset($row['tdata'])) ? $this->tdata($row['tdata']) : '';

            if (isset($this->args['boolen']) and $label == 1) {
                $label = str_true;
            } elseif (isset($this->args['boolen']) and $label == 0) {
                $label = str_false;
            }

            $selected = (($this->args['selected']) && $this->args['selected'] === $value) ? 'selected' : '';

            $op .= sprintf('<option value="%1$s" title="%2$s" %4$s %3$s>%2$s</option>',$value,$label,$tdata, $selected);
        }

        $c = count($this->args['data']);
        if ($this->args['size'] > 1 and $c < $this->args['size']) 
            $this->args['size'] = $c;

        $select_atts = ['id','class','name','autofocus','required','size','disabled','multiple','style'];
        $atts = [];
        foreach ($select_atts as $attr) {
            if (isset($this->args[$attr]) && ($this->args[$attr])) 
                $atts[] = "$attr='{$this->args[$attr]}'";
        }

        $m = sprintf("<select %s %s>", implode(' ',$atts), $this->args['data_atts']);

        if ($this->args['onlyoptions'])
            return $op;
        else
            return $m.$op."</select>";
    }

    public function remove_empty_line() {
        $this->empty_line = false;
    }

    function format_query($e) {
        $this->format_query = $e;
    }

    private function tdata($arr) {
        $tdata = '';
        foreach ($arr as $target=>$value) {
            $tdata .= "data-" . strtolower($target) . "='$value' ";
        }
        return $tdata;
    }
}

function fill_stable_with_columns($vars,$session_id,$output_limit) {
    global $ID;

    $data_csv_header = $vars['stable_header'];
    $protocol = $vars['protocol'];
    if (!$output_limit)
        $_SESSION = $vars['session'];
    $tgroups = $vars['tgroups'];
    $allowed_cols = $vars['allowed_cols'];
    $allowed_general_columns = $vars['allowed_general_columns'];
    $general_restriction = $vars['general_restriction'];
    $GRST = $vars['GRST'];

    
    if (!isset($_SESSION['orderby'])) {
        $_SESSION['orderby'] = 'obm_id';
        $_SESSION['orderad'] = 1;
        $_SESSION['orderascd'] = '';
    }
    if (!isset($_SESSION['orderad']) or $_SESSION['orderad']==0) {
        $_SESSION['orderad'] = 1;
    }

    $ACC_LEVEL = ACC_LEVEL;
    $modules = new modules();

    // ignore joins - should be optional!!
    if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
        $res = pg_query($ID,sprintf('SELECT * FROM (SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s) t ORDER BY %3$s %4$s',PROJECTTABLE,$session_id,$_SESSION['orderby'],$_SESSION['orderascd']));
    else
        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY %3$s %4$s',PROJECTTABLE,$session_id,$_SESSION['orderby'],$_SESSION['orderascd']));

    //
    $rows_count = pg_num_rows($res);

    // an other separateble async process...
    $n = 0;
    $cmd = "";
    $cmd_n = 0;
    while ($row = pg_fetch_assoc($res)) {
            
        if ($output_limit and $n==$output_limit)
            break;
        
        if (!$output_limit and $n<80) {
            $n++;
            continue;
        }

            // skipping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($row)==1) {
                if ($row[0]==$chk) continue;
                else $chk = $row[0];
            }

            # Nem lehet rst query-t csinálni rolltáblában, mert kifagy!!!
            $acc = 1;
            if (!rst('acc',$row['obm_id'],$_SESSION['current_query_table'])) {
                $acc = 0;
            }
            
            $geometry_restriction = 0;
            $column_restriction = 0;
            if (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='1' or $row['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $geometry_restriction = 1;
            }
            elseif (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='2' or $row['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $column_restriction = 1;
            } elseif (!$acc) {
                $column_restriction = 1;
            }

            $line = array();
            foreach ($data_csv_header as $k=>$v) {
                if (isset($row[$k])) {

                    if ($modules->is_enabled('transform_data')) {
                        //default itegrated module
                        $line[$k] = "<div class='viewport'>".$modules->_include('transform_data','text_transform',array($row[$k],$k,''),false)."</div>";
                    } else {
                        $line[$k] = "<div class='viewport'>".$row[$k]."</div>";
                    }

                    if ($column_restriction) {
                        if (isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group' )) {
                            $general_restriction = 1;

                        } elseif ( !in_array($k,$allowed_cols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }
                    elseif ($geometry_restriction ) {
                        if ( $k == 'obm_geometry')
                            $line[$k] = '&nbsp;';
                        if ( !in_array($k,$allowed_gcols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }

                    //non sensitivity based restrictions 
                    if ($general_restriction) {
                        if ( !in_array($k,$allowed_general_columns) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }

                    if ( $k == 'obm_files_id') {
                        if ($column_restriction) {
                            if ( !in_array($k,$allowed_cols) ) {
                                $line[$k] = '&nbsp;';
                            }
                        }
                        elseif ($geometry_restriction ) {
                            if ( !in_array($k,$allowed_gcols) ) {
                                $line[$k] = '&nbsp;';
                            }
                        } 
                        if ($general_restriction) {
                            if ( !in_array($k,$allowed_general_columns) ) {
                                $line[$k] = '&nbsp;';
                            }
                        }
                        else
                            $line[$k] = "<a href='getphoto?connection={$row[$k]}' class='photolink'><i class='fa fa-camera'></i></a>";
                    }
                    if ( $k == 'obm_uploader_user') {
                        $line[$k] = $row['obm_uploader_name'];
                    }
                    
                } else {
                    $line[$k] = "&nbsp;";
                }
            }

            //prepend details buttun
            $id = $row['obm_id'];
            $uid = $row['upid'];
            // SHINYURL global control variable
            // create readable links instead of long get urls
            if (defined('SHINYURL') and constant("SHINYURL")) {
                $eurl = sprintf('%://'.URL.'/%s/data/%s/',$protocol,$_SESSION['current_query_table'],$id);
                $hurl = sprintf('%://'.URL.'/%s/uplinf/%s/',$protocol,$_SESSION['current_query_table'],$uid);
            } else {
                $eurl = sprintf('%://'.URL.'/index.php?data&id=%s&table=%s',$protocol,$id,$_SESSION['current_query_table']);
                $hurl = sprintf('%://'.URL.'/index.php?history=upload&id=%s&table=%s',$protocol,$uid,$_SESSION['current_query_table']);
            }

            $b_id = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary'><i class='fa fa-info'></i> %s</a>",$eurl,str_details);
            $b_upid = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary'><i class='fa fa-info'></i> %s</a>",$hurl,str_upload);
            array_unshift($line,$b_upid);
            array_unshift($line,$b_id);
            
            $cmd .= sprintf("UPDATE temporary_tables.temp_roll_stable_%s_%s SET rlist=%s WHERE \"ord\"=%d;",PROJECTTABLE,$session_id,quote(json_encode($line,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT)),$n);
            $n++;
            $cmd_n++;
            if ($cmd_n>20) {
                pg_query($ID,$cmd);
                $cmd = "";
                $cmd_n = 0;
            }
        }
        if ($cmd != '')
            pg_query($ID,$cmd);

}


#xdebug_stop_trace();
?>
