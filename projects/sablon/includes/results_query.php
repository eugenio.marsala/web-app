<?php
/* This is same as results_builder, just create results for showing on the map, it uses the same assemble_query() function to create the SQL query for further actions
 *
 * */

if (session_status() == PHP_SESSION_NONE) {
    //$lifetime=3600;
    session_start();
    //setcookie(session_name(),session_id(),time()+$lifetime,"/");
}

if (isset($_SESSION['load_loadquery']))
    unset($_SESSION['load_loadquery']);

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
if(!isset($_SESSION['Tproject'])) { 
    require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    //st_col(PROJECTTABLE);
}
require_once(getenv('OB_LIB_DIR').'languages.php');



if (isset($_SESSION['morefilter']) and $_SESSION['morefilter']!='clear') {

    $res = pg_query($ID,sprintf("SELECT EXISTS (
       SELECT 1
       FROM   information_schema.tables 
       WHERE  table_schema = 'temporary_tables'
       AND    table_name = 'temp_%s_%s');",PROJECTTABLE,session_id()));
    $row = pg_fetch_assoc($res);

    if ($row['exists']=='t') {
        $cmd = sprintf('DELETE FROM temporary_tables.temp_filter_%1$s_%2$s;INSERT INTO temporary_tables.temp_filter_%1$s_%2$s SELECT obm_id FROM temporary_tables.temp_%1$s_%2$s',PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if(pg_affected_rows($res)) {
            //the whole JOIN query
            $_SESSION['morefilter_query'] = sprintf("RIGHT JOIN temporary_tables.temp_filter_%s_%s on rowid=obm_id",PROJECTTABLE,session_id());
        } else {
            $_SESSION['morefilter_query'] = '';
        }
    } else {
            $_SESSION['morefilter_query'] = '';
    }
}
elseif (isset($_SESSION['morefilter']) and $_SESSION['morefilter']=='clear') {
    $_SESSION['morefilter_query'] = '';
}


//assembling SQL query string using ACCESS level text filters and spatial filters and subfilters
$qq = assemble_query('','query');

//debug($qq,__FILE__,__LINE__);

//in the query type query we don't need anything else than the obm_id!
/*$cmd = array();
foreach ($qq as $query ) {
    #$cmd[] = sprintf('SELECT DISTINCT ON (igid) igid FROM (%1$s) AS new_table WHERE ST_SRID(the_geometry)=%2$d ORDER BY igid',$query,$_SESSION['st_col']['SRID_C']);
    //$cmd[] = sprintf('SELECT DISTINCT ON (obm_id) obm_id FROM (%1$s) AS new_table WHERE ST_SRID(obm_geometry)=%2$d ORDER BY obm_id',$query,$_SESSION['st_col']['SRID_C']);
    $cmd[] = $query;
}*/
if (is_array($qq) and count($qq) > 1) {
    // more than 1 data layer queried
    //$id_query = '('.join(') UNION (',$qq).')';
    $id_query = $qq[0];
} elseif (is_array($qq) and isset($qq[0])) {
    $id_query = $qq[0];
} else {
    log_action('No query, exit',__FILE__,__LINE__);
    exit;
}

// debugging
//log_action($id_query,__FILE__,__LINE__);


//$cmd = sprintf("CREATE UNLOGGED TABLE IF NOT EXISTS temporary_tables.temp_%s_%s ( rowid INTEGER )",PROJECTTABLE,session_id());

pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));

// It can be very slow - ten minutes or more if the query is very big...
// If I create the table and call parallel inserts??? how?? exec('php parallel.php $id_query')
$cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_%s_%s AS %s",PROJECTTABLE,session_id(),$id_query);
$res = pg_query($ID,$cmd);
if (!pg_affected_rows($res)) {
    if ($s = pg_last_error($ID)) {
        log_action("Create temp table error: $s",__FILE__,__LINE__);
        log_action($cmd,__FILE__,__LINE__);
    }
}

$cmd = sprintf("GRANT SELECT ON temporary_tables.temp_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
pg_query($ID,$cmd);

update_temp_table_index(PROJECTTABLE.'_'.session_id());


$n = pg_affected_rows($res);

// $_GET['report'] & csv output request - do not print output
if (isset($_SESSION['show_results']['type']) and $_SESSION['show_results']['type'] == 'csv')
    return;
elseif (isset($_SESSION['show_results']['type']) and $_SESSION['show_results']['type'] == 'json')
    return;

// ajax return value
if ($n)
    echo $n;
else
    echo 0;

if ($n==0) {
    
    if (isset($_SESSION['filter_type'])) {
        unset($_SESSION['filter_type']); 
    }

}

//    $cmd = sprintf("DELETE FROM temporary_tables.temp_%s_%s",PROJECTTABLE,session_id());
//    $res = pg_query($ID,$cmd);


//$insert_id_list_to_tmp_cmd = sprintf("INSERT INTO temporary_tables.temp_%s_%s SELECT obm_id FROM (%s) AS foo",PROJECTTABLE,session_id(),$id_query);
//log_action($insert_id_list_to_tmp_cmd,__FILE__,__LINE__);

/* id lista lekérdezése a queryből */
/*$res = pg_query($ID,$insert_id_list_to_tmp_cmd);
if (pg_last_error($ID) or $res===false)
    log_action($insert_id_list_to_tmp_cmd,__FILE__,__LINE__);

if (pg_affected_rows($res)) {
    echo pg_affected_rows($res);

} else {
    // no results: clear previous
    //$_SESSION['wfs_array'] = array("id"=>array(),"geom"=>array(),"data"=>array());
    echo 0;
}*/
?>
