<?php
/*  User, Group settings on projects
 *  
 *  
 * Ajax function 
 *  
 *  
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

//* only for logined users
if(!isset($_SESSION['Tid'])) {
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

/* update members / users 
 * POST user_id
 * POST groups
 * POST user_status
 * */
if (isset($_POST['user_id']) and isset($_POST['groups'])) {
    if(!grst(PROJECTTABLE,4)) exit;

    $groups = explode(',',$_POST['groups']);
    $user_id = $_POST['user_id'];
    $user_status = $_POST['user_status'];
    
    if ($user_id == '') exit;

    $cmd = sprintf("UPDATE project_users SET user_status=%s WHERE user_id=%s AND project_table='%s'",quote($user_status),quote($user_id),PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if (!pg_affected_rows($res)) {
        log_action("Update project_user's user_status failed for $user_id",__FILE__,__LINE__);
    }


    $cmd = sprintf("SELECT role_id FROM project_roles WHERE user_id=%s AND project_table='%s'",quote($user_id),PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $role_id = $row['role_id'];

        // remove user's role from the roles which includes it.
        $cmd = sprintf("UPDATE project_roles SET container = array_remove(container, %d) WHERE project_table='%s' AND (user_id IS NULL OR user_id!=%s)",$role_id,PROJECTTABLE,quote($user_id));
        $res = pg_query($BID,$cmd);

        foreach ($groups as $role) {
            if ($role == '') continue;
            # SELECT ARRAY(SELECT DISTINCT UNNEST(container || %d) FROM project_roles WHERE role_id=201 ORDER BY 1);
            $cmd = sprintf('UPDATE project_roles SET container = ARRAY(SELECT DISTINCT UNNEST(container || %1$d) FROM project_roles WHERE role_id=%2$s AND project_table=\'%3$s\' ORDER BY 1)  WHERE role_id=%2$s AND project_table=\'%3$s\'',$role_id,$role,PROJECTTABLE);
            $res = pg_query($BID,$cmd);
            if (!pg_affected_rows($res)) {
                log_action("Update project_roles' container failed for $user_id",__FILE__,__LINE__);
            } 
        }
    }

    $cmd = sprintf("SELECT array_to_string(array_agg(role_id),',') AS groups FROM project_roles WHERE %s=ANY(container) AND project_table='%s'",quote($role_id),PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_assoc($res);
    print $row['groups'];
    exit;
} 
/* Create new role (group) 
 *
 * */
if (isset($_POST['new_group'])) {
    if(!grst(PROJECTTABLE,4)) exit;

    mb_internal_encoding("UTF-8");
    mb_regex_encoding("UTF-8");    
    $new_group = mb_ereg_replace('[^a-zöüóéáűíA-ZÖÜÓŐÚÉŰÍ0-9_]','',$_POST['new_group']);
    if ($new_group=='') exit;

    $cmd = sprintf("INSERT INTO project_roles (project_table,description) VALUES ('%s',%s)",PROJECTTABLE,quote($new_group));
    if(!pg_query($BID,$cmd)) {
        print 'failed';
        log_action('Create role (group) failed',__FILE__,__LINE__);
    } else
        print 'ok';
    exit;
}
/* drop groups*/
if (isset($_POST['drop_group_id'])) {
    if(!grst(PROJECTTABLE,4)) exit;

    $group = $_POST['drop_group_id'];
    // drop only empty roles !!!
    $cmd = sprintf('DELETE FROM project_roles WHERE role_id=%s AND (container IS NULL OR array_length(container,1) IS NULL OR (array_length(container,1)=1 AND container[1]=role_id))',quote($group)); 
    $res = pg_query($BID,$cmd);
    if (!$res) {
        print 'failed';
        log_action('Drop role (group) failed',__FILE__,__LINE__);
    } else {
        if (pg_affected_rows($res))
            print 'ok';
        else
            print 'non empty group';
    }
    exit;

}
/* Update group's rights
 *
 * */
if (isset($_POST['group_id']) and isset($_POST['nested'])) {
    if(!grst(PROJECTTABLE,4)) exit;

    $group = $_POST['group_id'];
    if ($group == '') exit;
    
    $cmd = sprintf('UPDATE project_roles SET container=\'{%s}\' WHERE role_id=%s',$_POST['nested'],quote($group)); 

    if(!pg_query($BID,$cmd))
        print 'failed';
    else
        print 'ok';

    exit;
}
// assign column type and name to columns
if (isset($_POST['t_post'])) {
    if(!grst(PROJECTTABLE,4)) exit;

    $j = json_decode($_POST['t_post']);
    #$j->{'colname'};
    #$j->{'name'};
    $n = -1;
    $data = array();
    $table = $j->{'table'};
    $alters = array();
    $f_x_column = '';
    $f_y_column = '';
    #$f_id_column = '';
    $f_date_column = array();
    $f_cite_person = array();
    $f_quantity_column = '';
    $f_species_column = '';
    $f_geom_column = '';
    $f_geom_srid = 4326;
    $f_restrict_column = '';
    $f_attachment_columns = array();
    $comments = array();
    foreach($j->{'type'} as $t) {
        $n++;
        if ($t=='') continue;

        $comments[$j->{'colname'}[$n]] = $j->{'comment'}[$n];

        if ($t=='data') {
            #$data[]='"'.$j->{'colname'}[$n].'"=>"'.$j->{'name'}[$n].'"';
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='alternames') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $alters[]= $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='species') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_species_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='numind') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_quantity_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='datum') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_date_column[] = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        #} elseif ($t=='id') {
        #    $f_id_column = $j->{'colname'}[$n];
        } elseif ($t=='Xc') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_x_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='Yc') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_y_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='cp') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_cite_person[] = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='geometry') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_geom_column = $j->{'colname'}[$n];
            if (preg_match('/srid:(\d+)/',$j->{'comment'}[$n],$m))
                $f_geom_srid = $m[1];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='restrict') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_restrict_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='attachment') {
            if($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_attachment_columns[] = $table;
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        }

    }

    //order
    $n=-1;
    $order = array();
    foreach($j->{'order'} as $o) {
        $n++;
        if ($o=='') continue;
        if (!isset($order[$table])) {
            $order[$table] = array();
        }
        //$order[$o] = $j->{'colname'}[$n];
        $order[$table][$j->{'colname'}[$n]] = $o;
    }
    $os = array();
    foreach($order as $k=>$v) {
        asort($v);
        $os[$k] = $v;
    }
    $order = json_encode($order);
    $order = json_encode($os);

    // Clean metaname table
    $error = '';
    $cmd = sprintf('BEGIN;DELETE FROM project_metaname WHERE project_table=\'%s\'',$table);
    pg_query($BID,$cmd);

    // check header_names entry exists
    $cmd = sprintf("SELECT f_table_name FROM header_names WHERE f_table_name='%s' AND f_main_table=%s",PROJECTTABLE,quote($table));
    $res = pg_query($BID,$cmd);
    if (!pg_num_rows($res)) {
        $cmd = sprintf("INSERT INTO header_names (f_table_schema, f_table_name,f_main_table) VALUES ('public','%s',%s)",PROJECTTABLE,quote($table));
        $res = pg_query($BID,$cmd);
    }

    
    // update columns' order
    $cmd = sprintf('UPDATE header_names SET f_order_columns=%s WHERE f_table_name=\'%s\' AND f_main_table=%s',quote($order),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    
    $n = 0;
    foreach ($data as $k=>$v) {
        if ($v=='') {
            echo "$k:empty value";
            continue;
        }
        $cmd = sprintf('INSERT INTO project_metaname (project_table,column_name,short_name,rights,description) VALUES (\'%s\',%s,%s,\'%s\',%s)',$table,quote($k),quote($v),'{0}',quote($comments[$k]));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);

        //$cmd = sprintf('COMMENT ON COLUMN %s.%s IS \'%s\'',$table,$k,$comments[$k]);
        //pg_query($ID,$cmd);
        //$error .= pg_last_error($ID);
        $n++;
    }

    #
    $alters = implode(",",$alters);
    $cmd = sprintf('UPDATE header_names SET f_alter_speciesname_columns=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote("{".$alters."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $cmd = sprintf('UPDATE header_names SET f_species_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($f_species_column),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $f_date_column = implode(",",$f_date_column);
    $cmd = sprintf('UPDATE header_names SET f_date_columns=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote("{".$f_date_column."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $cmd = sprintf('UPDATE header_names SET f_quantity_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($f_quantity_column),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    #$cmd = sprintf('UPDATE header_names SET f_id_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($f_id_column),PROJECTTABLE,quote($table));
    #pg_query($BID,$cmd);
    #$error .= pg_last_error($BID);
    #
    if (isset($f_y_column)) {
        $r = $f_y_column;
        $cmd = sprintf('UPDATE header_names SET f_y_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($r),PROJECTTABLE,quote($table));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }
    #
    if (isset($f_x_column)) {
        $r = $f_x_column;
        $cmd = sprintf('UPDATE header_names SET f_x_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($r),PROJECTTABLE,quote($table));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }
    # geometry
    $r = $f_geom_column;
    $cmd = sprintf('UPDATE header_names SET f_geom_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($r),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);

    # geometry srid
    $r = $f_geom_srid;
    $cmd = sprintf('UPDATE header_names SET f_srid=%1$d WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',$r,PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);

    # cite person
    $f_cite_person = implode(",",$f_cite_person);
    $cmd = sprintf('UPDATE header_names SET f_cite_person_columns=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote("{".$f_cite_person."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $f_attachment_columns = implode(",",$f_attachment_columns);
    $cmd = sprintf('UPDATE header_names SET f_file_id_columns=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote("{".$f_attachment_columns."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $r = $f_restrict_column;
    $cmd = sprintf('UPDATE header_names SET f_restrict_column=%1$s WHERE f_table_name=\'%2$s\' AND f_main_table=%3$s',quote($r),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    
    #commit or not
    if ($error=='') {
        $result = pg_query($BID,"COMMIT");
        print $table;
    } else {
        log_action($error,__FILE__,__LINE__);
        $result = pg_query($BID,"ROLLBACK");
        print "error";
    
    }
    exit;
}
// adding new column 
if (isset($_POST['new_column'])) {
    if(!grst(PROJECTTABLE,4)) exit;
    
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");

    $error = '';

    $cmd = sprintf("SELECT f_table_name FROM header_names WHERE f_table_name='%s' AND f_main_table=%s",PROJECTTABLE,quote($_POST['table']));
    $res = pg_query($BID,$cmd);
    if (!pg_num_rows($res)) {
        exit;
    }

    $name = $_POST['nc_name'];
    $type = $_POST['nc_type'];
    $length = $_POST['nc_length'];
    $array = $_POST['nc_array'];
    $default = $_POST['nc_default'];
    $check = $_POST['nc_check'];

    $label = $_POST['nc_label'];
    $comment = $_POST['nc_comment'];

    $def = sprintf('%s %s',$name,$type);
    if ($length!='' and $type=='character varying')
        $def .= sprintf("(%d)",$length);

    //ALTER TABLE dinpi ADD column asd character varying(4)[] default '{asda}'

    if ($array!='')
        $def .= '[]';

    if ($default!='') {
        $def .= ' DEFAULT ';
        if ($array!='')
            $def .= sprintf('{%s}',preg_replace("/[\"']/",'',$default));
        else
            $def .= $default;
    }

    pg_query($ID,'BEGIN');

    # adding column
    $cmd = sprintf("ALTER TABLE %s ADD COLUMN %s",$_POST['table'],$def);
    pg_query($ID,$cmd);
    $error .= pg_last_error($ID);

    # create metaname entry
    if ($label != '') {
        $cmd = sprintf('INSERT INTO project_metaname (project_table,column_name,short_name,rights,description) VALUES (\'%s\',%s,%s,\'%s\',%s)',$_POST['table'],quote($name),quote($label),'{0}',quote($comment));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }

    # adding comment
    if ($comment!='') {
        //COMMENT ON COLUMN dinpi.eov_x IS 'EOV-ban az nem az mint WGS-ben';
        $cmd = sprintf('COMMENT ON COLUMN "%s"."%s" IS \'%s\'',$_POST['table'],$name,preg_replace("/[\"']/",'',$comment));
        pg_query($ID,$cmd);
        $error .= pg_last_error($ID);
    }

    # adding check constraint
    if ($check!='' and $check!='()') {
        //ALTER TABLE prices_list ADD CONSTRAINT price_discount_check CHECK (...)
        //ALTER TABLE link ADD CHECK (target IN ('_self', '_blank', '_parent', '_top'));
        $cmd = sprintf('ALTER TABLE %1$s ADD CHECK %2$s',$_POST['table'],$check);
        pg_query($ID,$cmd);
        $error .= pg_last_error($ID);
    }

    #commit or not
    if ($error=='') {
        $result = pg_query($ID,"COMMIT");
        print $_POST['table'];
    } else {
        log_action($error,__FILE__,__LINE__);
        $result = pg_query($ID,"ROLLBACK");
        print "error";
    }
    exit;
}

/* create new upload / insert forms kitöltő ív, feltöltő űrlap
 *
 *
 * */
if (isset($_POST['new_form'])) {
    if(!grst(PROJECTTABLE,3)) exit;

    $j = json_decode($_POST['new_form']);

    // existing form as new form if its name has changed 
    if ($j->{'edit_form_value'}!='') {
        $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id=%d",$j->{'edit_form_value'});
        $r = pg_query($BID,$cmd);
        if (pg_num_rows($r)) {
            $row = pg_fetch_assoc($r);
            if ($row['form_name'] != $j->{'form_name'})
                $j->{'edit_form_value'}='';
        }
    }


    pg_query($BID,'BEGIN');
    if ($j->{'edit_form_value'}!='') {

        #update
        $e = quote(preg_replace('/[^0-9]/','',$j->{'edit_form_value'}));
        $n = 0;
        $error = 0;
        
        if (!is_array($j->{'form_type'})) $j->{'form_type'} = array($j->{'form_type'});
        if (!is_array($j->{'form_group_access'})) $j->{'form_group_access'} = array($j->{'form_group_access'});
        if ($j->{'form_access'}<2) $j->{'form_group_access'} = array();

        if (!is_array($j->{'form_data_access'})) $j->{'form_data_access'} = array($j->{'form_data_access'});

        $cmd = sprintf("UPDATE project_forms SET user_id=%d,form_name=%s,form_type=%s,form_access=%s,groups=%s,description=%s,destination_table=%s,srid=%s,data_groups=%s WHERE form_id=%s",$_SESSION['Tid'],quote($j->{'form_name'}),quote('{'.implode(',',$j->{'form_type'}).'}'),quote($j->{'form_access'}),quote('{'.implode(',',$j->{'form_group_access'}).'}'),quote($j->{'form_description'}),quote($j->{'form_table'}),quote('{'.$j->{'form_srid'}.'}'),quote('{'.implode(',',$j->{'form_data_access'}).'}'),$e);
        $r = pg_query($BID,$cmd);
        if ($r) {
            $form_id = $j->{'edit_form_value'};
            $cmd = sprintf("DELETE FROM project_forms_data WHERE form_id=%d",$j->{'edit_form_value'});
            pg_query($BID,$cmd);
        }
    } else {
        #insert
        $n = 0;
        $error = 0;
        if (!is_array($j->{'form_type'})) $j->{'form_type'} = array($j->{'form_type'});
        if (!is_array($j->{'form_group_access'})) $j->{'form_group_access'} = array($j->{'form_group_access'});
        if (!is_array($j->{'form_data_access'})) $j->{'form_data_access'} = array($j->{'form_data_access'});

        $cmd = sprintf("INSERT INTO project_forms (user_id,project_table,form_name,form_type,form_access,active,groups,description,destination_table,srid,data_groups) VALUES (%d,'%s',%s,%s,%s,1,%s,%s,%s,%s,%s) RETURNING form_id",$_SESSION['Tid'],PROJECTTABLE,quote($j->{'form_name'}),quote('{'.implode(',',$j->{'form_type'}).'}'),quote($j->{'form_access'}),quote('{'.implode(',',$j->{'form_group_access'}).'}'),quote($j->{'form_description'}),quote($j->{'form_table'}),quote('{'.$j->{'form_srid'}.'}'),quote('{'.implode(',',$j->{'form_data_access'}).'}'));
        $res = pg_query($BID,$cmd);
        $form_id = '';
        if ($res) {
            $row = pg_fetch_assoc($res);
            $form_id = $row['form_id'];
        } else $error++;
        /*foreach($j->{'projmet'} as $k) {
            $cmd = sprintf("UPDATE %s_projects SET ob_forms=array_append(ob_forms,$form_id) WHERE id=$k",PROJECTTABLE);
            pg_query($ID,$cmd);
        }*/
        
    }
    if ($form_id!='') {
        foreach($j->{'id'} as $id) {
            $pl = '';
            //autocomplete
            if ($j->{'type'}[$n]=='autocomplete') {
                // old style to new
                if (!preg_match('/^\{/',$j->{'list'}[$n])) {
                    $m = preg_split("/\./",$j->{'list'}[$n]);
                    $j->{'list'}[$n] = '{"optionsTable":"'.$m[0].'","valueColumn":"'.$m[1].'","labelColumn":""}';
                }
                $pl = $j->{'list'}[$n];
            }
            elseif ($j->{'type'}[$n]=='list' or $j->{'list'}[$n]!='') {
                
                // old style processing
                if (!preg_match('/^\{/',$j->{'list'}[$n])) {
                    $PATTERN = '/SELECT:(\w+)\[?(\w+(?:(?:,\w+)+)?)?\]?\.(\w+):?(\w+)?/'; //pattern with filter (eg. SELECT:table[filter_col,filter_value].value:label)
                    if (preg_match($PATTERN,$j->{'list'}[$n],$m)) {
                        $label = '';
                        if (isset($m[4]))
                            $label = $m[4];

                        if ($m[2]!='') {
                            $pmp = preg_split('/,/',$m[2]);
                            $preFilterColumn = $pmp[1];
                            $preFilterValue = $pmp[2];
                            $list_definition = array('optionsTable'=>$m[1],'preFilterColumn'=>$preFilterColumn,'preFilterValue'=>$preFilterValue,'valueColumn'=>$m[3],'labelColumn'=>$label);
                        } else {
                            $list_definition = array('optionsTable'=>$m[1],'valueColumn'=>$m[3],'labelColumn'=>$label);
                        
                        }

                    } else {
                        $list_definition = array();
                        $list = preg_split('/,/',$j->{'list'}[$n]);
                        foreach ($list as $l) {
                            $lv = preg_split('/:/',$l);
                            if ($lv[0] === '_empty_') $lv[0] = '';
                            if (count($lv)>1)
                                $list_definition[trim($lv[1])] = array_map('trim',explode('#',$lv[0]));
                            else
                                $list_definition[trim($lv[0])] = array();
                        }

                        $list_definition = array("list"=>$list_definition);
                    }

                    $pl = json_encode($list_definition,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_PRESERVE_ZERO_FRACTION);
                } else {
                    //list
                    //$pl = '{"'.implode('","',$list).'"}';
                    /*{
                        "list": {
                                "val1": ["label1", "label2"]
                        },
                        "optionsTable": "",
                        "valueColumn": "",
                        "labelColumn": "",
                        "filterColumn": "",
                        "pictures": {
                                "val1": "url-string"
                        },
                        "triggerTargetColumn": "",
                        "Function": "",
                        "disabled": ["val1"],
                        "preFilterColumn": "",
                        "preFilterValue": ""
                    }*/
                    $pl = $j->{'list'}[$n];
                }
            }
            //default value
            if ($j->{'default_value'}[$n]=='') $def_val = 'NULL';
            else $def_val = quote($j->{'default_value'}[$n]);

            //api params
            $api_params = $j->{'api_params'}[$n];
            if (is_array($api_params))
                $ap = '["'.implode('","',$api_params).'"]';
            else
                $ap = '[]';

            //if($j->{'api_params'}[$n]=='') $api_params = 'NULL';
            //else $api_params = quote($j->{'api_params'}[$n]);
            
            if ($j->{'relation'}[$n]=='') $relation = 'NULL';
            else $relation = quote($j->{'relation'}[$n]);

            $regexp = NULL;
            $custom_fun = NULL;
            $spatial = quote('');
            $pseudo_columns = quote('');
            //control
            if ($j->{'length'}[$n] == 'regexp') {
                $regexp = $j->{'count'}[$n];
                $j->{'count'}[$n] = NULL;
            }
            elseif ($j->{'length'}[$n] == 'spatial' and $j->{'count'}[$n] !='' ) {
                $spatial = sprintf("ST_GeomFromText('%s')",$j->{'count'}[$n]);
                $j->{'count'}[$n] = NULL;
            }
            elseif ($j->{'length'}[$n] == 'custom_check' and $j->{'count'}[$n] !='' ) {
                $custom_fun = $j->{'count'}[$n];
                $j->{'count'}[$n] = NULL;
            }
            
            if ($j->{'pseudo_columns'}[$n] == '') $pseudo_columns = 'NULL';
            else $pseudo_columns = quote($j->{'pseudo_columns'}[$n]);

            $j->{'count'}[$n] = '{'.preg_replace('/:/',',',$j->{'count'}[$n]).'}';

            //SQL INSERT
            $cmd = sprintf("INSERT INTO project_forms_data (form_id,\"column\",description,type,control,list_definition,
                                                    count,regexp,obl,fullist,default_value,
                                                    api_params,relation,spatial,pseudo_columns,custom_function,position_order) 
                                            VALUES (%d,%s,%s,%s,%s,%s,
                                                    %s,%s,%d,%d,%s,
                                                    %s,%s,%s,%s,%s,%s)",
            $form_id,quote($id),quote($j->{'description'}[$n]),quote($j->{'type'}[$n]),quote($j->{'length'}[$n]),quote($pl),
            quote($j->{'count'}[$n]),quote($regexp),$j->{'obl'}[$n],$j->{'fullist'}[$n],$def_val,
            quote($ap),$relation,$spatial,$pseudo_columns,quote($custom_fun),quote($j->{'position_order'}[$n]));

            $r = pg_query($BID,$cmd);
            if (!$r) {
                $error++;
                log_action($cmd.__FILE__,__LINE__);
            }
            $n++;
        }
    }
    if ( $error>0 ) {
        $result = pg_query($BID,"ROLLBACK");
        echo common_message('error',pg_last_error($BID));
    } else {
        $result = pg_query($BID,"COMMIT");
        echo common_message('ok','done');
    }
    exit;
    #print $_SESSION['upl_form_listfile'];
}
/* modules
 *
 *
 * */
if (isset($_POST['update_modules'])) {
    if(!grst(PROJECTTABLE,4)) exit;

    $id = preg_replace("/[^0-9new]/","",$_POST['update_modules']);
    $name = preg_replace("/[^A-Za-z0-9-_]/","",$_POST['name']);
    $file = basename(preg_replace("/[\/\n]/","",$_POST['file']));
    $func = preg_replace("/[^A-Za-z0-9-_]/","",$_POST['func']);
    $enab = preg_replace("/[^tf]/i","",$_POST['enab']);
    $enab = preg_replace("/t/i","TRUE",$enab);
    $enab = preg_replace("/f/i","FALSE",$enab);
    $access = preg_replace("/[^0-2]/","",$_POST['access']);
    $gaccess = preg_replace("/[^0-9,]/","",$_POST['gaccess']);
    if ($gaccess == '') $gaccess = array(0);
    $mtable = $_POST['mtable'];
    $params = array();
    $p = preg_split("/\n/",$_POST['params']); 
    foreach ($p as $param) {
        if ($param == "") continue;
        if (preg_match("/^[{\[].*[}\]]$/",$param)) {
            $params[] = 'JSON:'.base64_encode($param);
            continue;
        }
        $param = preg_replace("/,/","**",$param);
        $param = preg_replace('/"/','\"',$param);
        $params[] = $param;
    }
    //$params = "{".preg_replace('/[,]+$/','',preg_replace("/~^/",",",preg_replace("/[\/\\\\]/","",$params)))."}";
    $params = implode(',',$params);
    $params = "{".preg_replace("/\*\*/","\,",$params)."}";

    if ($id=='new') {
        $cmd = sprintf("INSERT INTO modules (project_table,module_name,file,function,params,enabled,module_access,main_table,group_access) VALUES ('".PROJECTTABLE."',%s,%s,%s,%s,%s,%d,%s,%s)",quote($name),quote($file),quote($func),quote($params),quote($enab),$access,quote($mtable),quote('{0}'));
        $res = pg_query($BID,$cmd);
        if ($res) {
            $m = new modules(false);
            $_SESSION['modules'] = $m->set_modules();

            $m->_include($name,'init');
            echo common_message('ok',$mtable);
        }
        else
            echo common_message('error',pg_last_error($BID));

    } else {
        if (trim($name)=='') {
            $cmd = sprintf('DELETE FROM modules WHERE id=\'%d\' AND project_table=%s AND main_table=%s',$id,quote(PROJECTTABLE),quote($mtable));
        } else {
            $cmd = sprintf('UPDATE modules SET file=%1$s,function=%2$s,params=%3$s,module_name=%4$s,enabled=%5$s,module_access=%8$d,group_access=%10$s WHERE id=\'%7$d\' AND project_table=%6$s AND main_table=%9$s',quote($file),quote($func),quote($params),quote($name),quote($enab),quote(PROJECTTABLE),$id,$access,quote($mtable),quote('{'.implode(',',$gaccess).'}'));
        }
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res)) {
            $m = new modules(false);
            $_SESSION['modules'] = $m->set_modules();
            echo common_message('ok',$mtable);
        }
        else {
            echo common_message('error',pg_last_error($BID));
            log_action('module update failed',__FILE__,__LINE__);
        }
    }
    exit; 
}
?>
