<?php
    $out = "";
    // running r-server
    if (defined('RSERVER') and constant("RSERVER")) {
        
        $rport = 'RSERVER_PORT_'.PROJECTTABLE;
        if (defined($rport)) {
            $rport = constant($rport);
            // test, shiny server is running
            if (!stest('localhost',$rport)) {
                log_action("Starting R-server",__FILE__,__LINE__);
                //shell_exec("Rscript --vanilla ".getenv('PROJECT_DIR')."shiny/run_server.R ".RSERVER_PORT." ".getenv('PROJECT_DIR')."shiny/ 2>&1 > ".getenv('PROJECT_DIR')."shiny/r-server.log");
                $pid = exec("Rscript --vanilla --no-save --slave --no-restore ".getenv('PROJECT_DIR')."shiny/run_server.R --port $rport --project ".PROJECTTABLE." --path ".getenv('PROJECT_DIR')."shiny/ > ".getenv('PROJECT_DIR')."shiny/r-server.log & echo $! &");
                if ($pid) {
                    log_action("R-server PID: $pid",__FILE__,__LINE__);
                    file_put_contents(getenv("PROJECT_DIR")."r-server.pid", $pid);
                }
                else
                    log_action("R-server starting failed",__FILE__,__LINE__);

            } 
            $out .= "<iframe style='border:none;height:650px;min-width:400px' src='http://localhost:$rport'></iframe>";
        }
    }
    echo $out;
?>
