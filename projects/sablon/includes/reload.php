<?php
session_start();

$_SESSION['LAST_ACTIVITY'] = time();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database with biomaps.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

// scheduled or topical updates
#require_once(getenv('OB_LIB_DIR').'db_updates.php');
#new db_updates('language files to db'); # 2018-08-
#new db_updates('project user status to varchar');   # 2018-09-08
#new db_updates('access & select_view to varchar');  # 2018-09-10

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');

// do nothing....
//
?>
