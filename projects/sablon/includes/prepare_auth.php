<?php

/* Update Login vars - refresh auth tokens */
if (isset($_COOKIE['access_token'])) {
    $cookie = json_decode( $_COOKIE[ "access_token" ] );

    $res = true;

    if (time() > $cookie->expiry) {
        // expired
        $res = refresh_token();
        $cookie = json_decode( $_COOKIE[ "access_token" ] );
    }

    if ($res) {
        // JUST check token validity by a resource request
        $result = oauth_webprofile_request($cookie->data->access_token);

        if ($result === FALSE) { 
            /* Handle error */ 
            log_action("Ouath resource request failed ({$cookie->data->access_token}):",__FILE__,__LINE__);
            setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
            unset($_COOKIE['access_token']);
        }
        else {
            if (!isset($_SESSION['Tid'])) {
                if(!oauth_login($cookie->data->access_token)) {
                    setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
                    unset($_COOKIE['access_token']);
                }
                // restore previous SESSION variables somehow? I think it is too expensive to save logined SESSION variables in the database...
            }
        }
    } else {
            setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
            unset($_COOKIE['access_token']);
    }
} elseif (isset($_COOKIE['refresh_token'])) {
    $cookie = json_decode( $_COOKIE[ "refresh_token" ] );

    if (time() < $cookie->expiry) {
        $res = refresh_token();
        if ($res!==false) {
            $cookie = json_decode( $_COOKIE[ "access_token" ] );
            oauth_login($cookie->data->access_token);
        } else {
            setcookie("access_token",$_COOKIE['access_token'],time()-1,"/");
            unset($_COOKIE['access_token']);
        
        }
    }   
}

?>
