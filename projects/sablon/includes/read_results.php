<?php
/* should be renamed to print_slides.php
 * AJAX
 * main.js: toggleSlides()
 * ez olvassa be az egyes blokkok adatát
 * */
session_start();

include_once(getenv('OB_LIB_DIR').'db_funcs.php');
$id = preg_replace('/[^slideb0-9:]/','',$_POST['id']);

include_once(getenv('OB_LIB_DIR').'results_builder.php');
$r = new results_builder(['method'=>'slide','slide'=>$id,'clear_current_rows'=>'off']);

$id = preg_replace('/[^0-9]/','',$_POST['id']);
if($r->results_query($id)) {
    if ($r->fetch_data()) {
        // print out slide
        if ($r->printOut()) {
            exit;
        }
    } 
}
echo json_encode(array('res'=>'','geom'=>'','error'=>$r->error));
#unset($_SESSION['wfs_array']);
unset($_SESSION['roll_array']);
exit;
?>
