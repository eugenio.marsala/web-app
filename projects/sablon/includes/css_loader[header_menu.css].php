<?php

header("Content-type: text/css", true);

if (file_exists('../css/private/header_menu.css')) {

    include_once('../css/private/header_menu.css');

} elseif (file_exists('../css/header_menu.css')) {
    
    include_once('../css/header_menu.css');

}
?>
