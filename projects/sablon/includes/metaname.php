<?php

function metaname() {
    /* Taxon name info page +
     * Taxon name modify
     * Called from index.php
     * */
    global $ID, $BID; 

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';


    if (isset($_GET['mispell'])) {
        $out = "<form class='pure-form pure-u-1-4'>";
        $out .= "<fieldset>
        <legend>Update mispelled taxon name:</legend>";
        $out .= "<input type='hidden' id='mispelled-taxonname-to-update' value='{$_GET['mispell']}'>";
        $out .= "<input type='hidden' id='mispelled-taxonname-id' value='{$_SESSION['getid']}'>";
        $out .= "<input type='hidden' id='mispelled-taxonname-lang' value='{$_GET['langc']}'>";
        $out .= "<input id='new-taxonname'  value='{$_GET['mispell']}'>";
        $out .= "<button id='update-mispelled-taxonname'>".str_send."</button>";
        $out .= "</fieldset><form>";
        $out .= "<div id='response'></div>";
        $out .= "Az elírás javítása esetén a faj listában és az adatbázisban is kijavításra kerülnek az érintett taxon nevek.";
        return $out;
    } 

    $species_array = array_unique(array_merge($_SESSION['st_col']['ALTERN_C'],array($_SESSION['st_col']['SPECIES_C'])));
    $cell = array();
    $acell = array();
    $ns_array = array('',str_accepted_name,str_mispelled_name,str_synonym_name,str_common_name);
    $GetId = array();

    if (isset($_GET['name']) and is_array($_GET['name']) and count($_GET['name'])) {

        $cmd = sprintf("SELECT taxon_id FROM ".PROJECTTABLE."_taxon WHERE word IN (%s)",implode(',',array_map('quote',$_GET['name'])));
        $result = pg_query($ID,$cmd);
        while ($row = pg_fetch_assoc($result)) {
            $GetId[] = $row['taxon_id'];
        }
    
    } elseif (isset($_GET['name']) and trim($_GET['name'])!='') {
        $cmd = sprintf("SELECT taxon_id FROM ".PROJECTTABLE."_taxon WHERE word=%s",quote($_GET['name']));
        $result = pg_query($ID,$cmd);
        while ($row = pg_fetch_assoc($result)) {
            $GetId[] = $row['taxon_id'];
        }
    }

    if (isset($_SESSION['getid']) and trim($_SESSION['getid'])!='') {
        $GetId[] = $_SESSION['getid'];
    }
    
    if ( isset($_SESSION['getids']) and count($_SESSION['getids'])!=0) {
        $GetId = array_merge($GetId,array_filter($_SESSION['getids']));
    }

    if (count($GetId)) {
        $cmd = sprintf("SELECT word,lang,taxon_id,status FROM ".PROJECTTABLE."_taxon WHERE taxon_id IN (%s)",join(',',$GetId));
        echo str_query_by_id.": ".join(',',$GetId)."<br>";
        echo str_found_names.":<br><input type='hidden' id='ops-connect'>";

    } else {
        return;
    }
    $result = pg_query($ID,$cmd);

    $alter_names = array();
    $scientific_name = array();
    $sc_id = array();
    $fn = array();
    while ($rp = pg_fetch_assoc($result)) {
        $sc_id[] =  $rp['taxon_id'];
        if ($rp['lang']==$_SESSION['st_col']['SPECIES_C']) {
            $scientific_name[] = $rp['word'];
            $cell[] = array($rp['taxon_id'],$rp['word'],$rp['status'],$rp['lang'],0); 
        }
        else {
            $alter_names[$rp['lang']][] = $rp['word'];
            $acell[] = array($rp['taxon_id'],$rp['word'],$rp['status'],$rp['lang'],0); 
        }
        $fn[] = "<span class='name-connect-{$rp['taxon_id']}'>".$rp['word']."</span>";
    }
    // külön nevek listája
    if(count($fn)) {
        echo "<b>".implode(", ",$fn)."</b><br><br>";
    }
    #cell
    #[["8396","Abramis ballerus","1","species",0],["8396","Abramis balerus","0","species",0]]
    #acell
    #[["8396","keszeg","2","magyar",0]]

    // Similar names and id-s to Scientific names
    $LIMIT = "LIMIT 5";
    $WHERE = "WHERE dist<0.4 "; //very small distance
    for($i=0;$i<count($scientific_name);$i++) {
        $string = preg_replace('/\s+/','',$scientific_name[$i]);
        $W = sprintf("$WHERE AND taxon_id NOT IN (%s)",implode(',',$sc_id));
        $cmd = sprintf("SELECT *
        FROM (
             SELECT DISTINCT ON (meta) meta,word as name, meta <->%s AS dist,taxon_id,lang,status
             FROM ".PROJECTTABLE."_taxon
        ) p
        $W ORDER BY dist $LIMIT",quote($string));
    
        $result = pg_query($ID,$cmd);
        while ($row=pg_fetch_assoc($result)) {
            $cell[] = array($row['taxon_id'],$row['name'],$row['status'],$row['lang'],$row['dist']);    
        }
    }

    /* scientific names ****************************************************************
     * 
     *
     * */
    $sorok = array();
    $sorok_s = array();
    $sa = '';
    $counter = 0;
    $dns = array();

    $syslangs = array();
    //{"faj":"en","magyarnev":"hu"}
    $cmd = sprintf("SELECT langs FROM project_variables WHERE project_table='%s' AND langs IS NOT NULL",PROJECTTABLE);
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) {
        $row = pg_fetch_assoc($result);
        $syslangs = json_decode($row['langs'],TRUE);
    } 
    
    foreach($cell as $c) {
        $counter++;
        // search terms for COL and wiki
        $ker = array();
        $id = $c[0];$name = $c[1];$status = $c[2];$lang = $c[3];$dist = $c[4];
        $wiki_url='';

        mb_internal_encoding("UTF-8");
        mb_regex_encoding("UTF-8");
        $ker['wiki'] = mb_ereg_replace('\s+','_',$name);
        $ker['col'] = mb_ereg_replace('\s+','+',$name);
        $ker['gni'] = mb_ereg_replace('\s+','+',$name);
        $ker['eol'] = mb_ereg_replace('\s+','+',$name);

        if ($lang == $_SESSION['st_col']['SPECIES_C']) 
            $wiki_url = "http://en.wikipedia.org/wiki/{$ker['wiki']}";
        
        elseif (count($syslangs)) {

            $w_url = sprintf("%s.wikipedia.org",$syslangs[$lang]);
            
            if (!isset($dns[$w_url])) {
                $w_url_response = checkdnsrr($w_url, 'A');
                $dns[$w_url] = $w_url_response;
            }

            if (!$w_url_response) $wiki_url = "http://en.wikipedia.org/wiki/{$ker['wiki']}";
            else $wiki_url = sprintf("http://%s.wikipedia.org/wiki/{$ker['wiki']}",$syslangs[$lang]);
        } else {
            //log_action("project variables: 'langs' not set but needed for create wiki species links.",__FILE__,__LINE__);
        }

        // data count/species name
        $cmd = sprintf('SELECT count(*) AS "count" FROM "%1$s" WHERE %2$s=%3$s GROUP BY %2$s',PROJECTTABLE,$_SESSION['st_col']['SPECIES_C'],quote($name));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);
        $count = $row['count'];
        
        //sorok
        $sa .= "<tr>";

        //count, name
        $sa .= "<td style='background-color:#efeeee'>";
        if (!in_array($id,$GetId)) {

            // a pds nem megy direkt hívással már, de az indirekthez meg kell authentikáció 
            // Ezt le kellene cserélni egy a pds eredmény generálással egybefűzött belős API hívásra!
            $sa .= "<a href='$protocol://".URL."/?metaname&id=$id' style='font-weight:bold;font-variant:normal;color:#E34545'>$name</a> <abbr title='előfordulások száma'>(<a href='$protocol://".URL."/?query={$_SESSION['st_col']['SPECIES_C']}:{$ker['col']}' target='_blank'>$count</a>)</abbr><br>";
        } else {
            $sa .= "<a href='$protocol://".URL."/?metaname&id=$id' style='font-weight:bold;font-variant:normal' target='_blank'>$name</a> <abbr title='előfordulások száma'>(<a href='$protocol://".URL."/?query={$_SESSION['st_col']['SPECIES_C']}:{$ker['col']}' target='_blank'>$count</a>)</abbr><br>";
        }
        //státusz
        if ($status!='') $sa .= "<span id='b-$id' style='font-style:italic'>".$ns_array[$status]."</span>";
        $sa .= "</td>";

        //műveletek
        $sa .= "<td style='background-color:#f3d1ed'>";
        if (grst(PROJECTTABLE,3)) {
            $sa .="<input type='radio' disabled style='display:none' class='name_order' name='sciname_$counter-$id' id='sm_$counter-$id' value='2'>";
            $sa .="<input type='radio' class='name_order' name='accepted' id='sa_$counter-$id' value='1'><label for='sa_$counter-$id'> ".str_accepted_name."</label><br>";
            $sa .="<input type='radio' class='name_order altname' name='sciname_$counter-$id' id='ss_$counter-$id' value='2'><label for='ss_$counter-$id'> ".str_synonym_name."</label><br>";
            $sa .="<input type='radio' class='name_order altname' name='sciname_$counter-$id' id='sc_$counter-$id' value='2'><label for='sc_$counter-$id'> ".str_common_name."</label><br>";
            $sa .="<a href='$protocol://".URL."/?metaname&id=$id&mispell=$name&langc={$_SESSION['st_col']['SPECIES_C']}' class='modname' id='smnewn_$counter-$id'>".str_mispelled_name."</a><br>";

            $sa .="<input type='hidden' class='name_order' value='{$_SESSION['st_col']['SPECIES_C']}' id='sl_$counter-$id'>";
            $sa .="<input type='hidden' class='name_order' value='$name' id='sn_$counter-$id'>";
        }
        $sa .="</td>"; 
        $sa .= "<td style='background-color:#f3d1ed'>";
        if (grst(PROJECTTABLE,3))
            $sa .= str_join."<br><input type='checkbox' class='ops-connect con-$id' id='cons$status-$id' value=''>";
        $sa .= "</td>";
        
        //wiki
        $sa .= "<td style='background-color:#efeeee'><a href='$wiki_url' target='_blank'><button title='Wiki Species'><img src='css/img/wikispecies.png' title='WikiSpecies' style='height:48px;padding:4px 2px 2px 2px'></button></a></td>";

        //CoL
        $sa .= "<td style='background-color:#efeeee'>";
        $sa .= "<div id='CoL-data$counter'><button title='Catalogue of Life' class='fetch-xml' id='CoL' data-target='CoL-data$counter' data-url='{$ker['col']}' alt='fetch CoL data'><img src='css/img/col_icon.jpg' style='height:48px;padding:4px 2px 2px 2px'></button></div>";
        $sa .= "</td>";

        //GNI
        //http://gni.globalnames.org/name_strings.html?search_term=acer%20negundo
        $sa .= "<td style='background-color:#efeeee'>";
        $sa .= "<div id='GNI-data$counter'><button title='Global Name Index' class='fetch-xml' id='GNI' data-target='GNI-data$counter' data-url='{$ker['gni']}' alt='fetch GNI data'><img src='css/img/gni_icon.jpg' style='padding:4px 2px 2px 2px'></button></div>";
        $sa .= "</td>";

        //EOL
        //http://eol.org
        $sa .= "<td style='background-color:#efeeee'>";
        $sa .= "<div id='EoL-data$counter'><button title='Enciclopedia of Life' class='fetch-xml' id='EoL' data-target='EoL-data$counter' data-url='{$ker['eol']}' alt='fetch EoL data'><img src='css/img/eol_icon.jpg' style='padding:4px 2px 2px 2px'></button></div>";
        $sa .= "</td>";
        $sa .= "</tr>";

        //külön lehet pakolni a lekérdezett nevet a többitől...
        if (!in_array($id,$GetId))
            $sorok_s[] = $sa;
        else
            $sorok[] = $sa;
        $sa = '';
    }
    
    /* common names *******************************************************
     *
     *
     * */
    // Similar names and id-s to Scientific names
    /*$LIMIT = "LIMIT 5";
    $WHERE = "WHERE dist<0.4 "; //very small distance
    for($i=0;$i<count($scientific_name);$i++) {
        $string = preg_replace('/\s+/','',$scientific_name[$i]);
    }*/

    //minden egyes lekérdezett nyelv
    foreach ($alter_names as $lang=>$ar) {
        //minden hozzá tartozó neve
        for ($i=0;$i<count($ar);$i++) {
            $string = preg_replace('/\s+/','',$ar[$i]);
            $W = sprintf("$WHERE AND taxon_id NOT IN (%s)",implode(',',$sc_id));

            $cmd = "SELECT *
            FROM (
                SELECT DISTINCT ON (meta) meta,word as name, meta <->'$string' AS dist,taxon_id,lang,status
                FROM ".PROJECTTABLE."_taxon
            ) p
            $W ORDER BY taxon_id,dist $LIMIT";

            //$cmd = "SELECT * FROM ( SELECT meta,word as name, meta <->'$string' AS dist,taxon_id,lang,status FROM dinpi_taxon ) p $WHERE ORDER BY taxon_id,dist LIMIT 5";
            //a kis nagy betűt ignorálja, ezért kidobhat egyéb egyezéseket is!! Ezt szűröm alább... 
            $result = pg_query($ID,$cmd);
            while ($row=pg_fetch_assoc($result)) {
                $acell[] = array($row['taxon_id'],$row['name'],$row['status'],$row['lang'],$row['dist']);
            }
        }
    }

    /* common name's array: cell
     *
     * */ 
    $sorok_c = array();
    $sorok_cs = array();
    $sa_c = '';
    $counter = 0;
    foreach($acell as $c) {
        $counter++;
        // search terms for COL and wiki
        $ker = array();
        $id = $c[0];$name = $c[1];$status = $c[2];$lang = $c[3];$dist = $c[4];
        $wiki_url='';

        mb_internal_encoding("UTF-8");
        mb_regex_encoding("UTF-8");    
        $ker['wiki'] = mb_ereg_replace('\s+','_',$name);
        $ker['col'] = mb_ereg_replace('\s+','+',$name);
        $ker['gni'] = mb_ereg_replace('\s+','+',$name);
        $ker['eol'] = mb_ereg_replace('\s+','+',$name);
        if ($lang == $_SESSION['st_col']['SPECIES_C']) 
            $wiki_url = "http://en.wikipedia.org/wiki/{$ker['wiki']}";

        elseif (count($syslangs)) {
            $w_url = sprintf("%s.wikipedia.org",$syslangs[$lang]);
            
            if (!isset($dns[$w_url])) {
                $w_url_response = checkdnsrr($w_url, 'A');
                $dns[$w_url] = $w_url_response;
            }

            if (!$w_url_response) $wiki_url = "http://en.wikipedia.org/wiki/{$ker['wiki']}";
            else $wiki_url = sprintf("http://%s.wikipedia.org/wiki/{$ker['wiki']}",$syslangs[$lang]);
        } else {
            //log_action("project variables: 'langs' not set but needed for create wiki species links.",__FILE__,__LINE__);
        }

        // data count/species name
        $sindex = array_search($lang,$_SESSION['st_col']['ALTERN_C']);
        if ($sindex!==FALSE)
            $species = $_SESSION['st_col']['ALTERN_C'][$sindex];

        if (isset($species) and $species != '') {
            $cmd = sprintf('SELECT count(*) AS "count" FROM "%1$s" WHERE %2$s=\'%3$s\' GROUP BY %2$s',PROJECTTABLE,$species,$name);
            $result = pg_query($ID,$cmd);
            $row = pg_fetch_assoc($result);
            $count = $row['count'];
        } else
            $count = 0;

        $sa_c .= "<tr>";
        $sa_c .= "<td style='background-color:#efeeee'>";
        
        //név
        if (!in_array($id,$GetId))
            $sa_c .= "<a href='$protocol://".URL."/?metaname&id=$id' style='font-variant:normal;font-weight:bold;color:#E34545'>$name</a> <abbr title='előfordulások száma'>(<a href='$protocol://".URL."/?query={$_SESSION['st_col']['SPECIES_C']}:{$ker['col']}'>$count</a>)</abbr>";
        else
            $sa_c .= "<a href='$protocol://".URL."/?metaname&id=$id' style='font-variant:normal;font-weight:bold' target='_blank'>$name</a> <abbr title='előfordulások száma'>(<a href='$protocol://".URL."/?query={$_SESSION['st_col']['SPECIES_C']}:{$ker['col']}'>$count</a>)</abbr>";

        //státusz       
        if ($status!='') $sa_c .= "<br><span id='ab_$counter-$id' style='font-style:italic'>".$ns_array[$status]."</span><br>".str_lang.": <i>$lang</i>";
        $sa_c .= "</td>";

        //műveletek
        $sa_c .= "<td style='background-color:#f3d1ed'>";
        if (grst(PROJECTTABLE,3)) {
            $sa_c .="<input type='radio' disabled style='display:none' class='cname_order' name='altname_$counter-$id' id='am_$counter-$id' value='2'>";
            $sa_c .="<input type='radio' class='cname_order' name='altname-accepted' id='aa_$counter-$id' value='1'><label for='aa_$counter-$id'> ".str_accepted_name."</label><br>";
            $sa_c .="<input type='radio' class='cname_order altname' name='altname_$counter-$id' id='as_$counter-$id' value='2'><label for='as_$counter-$id'> ".str_synonym_name."</label><br>";
            $sa_c .="<input type='radio' class='cname_order altname' name='altname_$counter-$id' id='ac_$counter-$id' value='2'><label for='ac_$counter-$id'> ".str_common_name."</label><br>";
            $sa_c .="<a href='$protocol://".URL."/?metaname&id=$id&mispell=$name&langc=$lang' class='modname' id='smnewn_$counter-$id'>".str_mispelled_name."</a><br>";

            $sa_c .="<input type='hidden' class='cname_order' value='$lang' id='al_$counter-$id'>";
            $sa_c .="<input type='hidden' class='cname_order' value='$name' id='an_$counter-$id'>";
        }
        $sa_c .= "</td>";
        $sa_c .= "<td style='background-color:#f3d1ed'>";
        if (grst(PROJECTTABLE,3))
            $sa_c .= str_join."<br><input type='checkbox' class='ops-connect con-$id' id='conc$status-$id' value=''>";
        $sa_c .= "</td>";


        //wiki
        $sa_c .= "<td style='background-color:#efeeee'><a href='$wiki_url' target='_blank'><button title='Wiki Species'><img src='css/img/wikispecies.png' title='WikiSpecies' style='height:48px;padding:4px 2px 2px 2px'></button></a></td>";

        //CoL
        $sa_c .= "<td style='background-color:#efeeee'>";
        $sa_c .= "<div id='CoL-data_$counter'><button class='fetch-xml' id='CoL' data-target='CoL-data_$counter' data-url='{$ker['col']}' alt='fetch CoL data'><img src='css/img/col_icon.jpg' style='height:48px;padding:4px 2px 2px 2px'></button></div>";
        $sa_c .= "</td>";
        
        //GNI pl.: http://gni.globalnames.org/name_strings.html?search_term=acer%20negundo
        $sa_c .= "<td style='background-color:#efeeee'>";
        $sa_c .= "<div id='GNI-data_$counter'><button class='fetch-xml' id='GNI' data-target='GNI-data_$counter' data-url='{$ker['gni']}' alt='fetch GNI data'><img src='css/img/gni_icon.jpg' style='padding:4px 2px 2px 2px'></button></div>";
        $sa_c .= "</td>";
    
        //EOL http://eol.org
        $sa_c .= "<td style='background-color:#efeeee'>";
        $sa_c .= "<div id='EoL-data_$counter'><button class='fetch-xml' id='EoL' data-target='EoL-data_$counter' data-url='{$ker['eol']}' alt='fetch EoL data'><img src='css/img/eol_icon.jpg' style='padding:4px 2px 2px 2px'></button></div>";
        $sa_c .= "</td>";

        $sa_c .= "</tr>";
        //szét lehet dobni ID-kként

        if (!in_array($id,$GetId))
            $sorok_cs[] = $sa_c;
        else
            $sorok_c[] = $sa_c;
        $sa_c = '';
    }

    /* assemble all result **********************************
     *
     * */
    $out  =  "<table class='specmod pure-form'>";
    $out  .=  "<th>".str_name_status."</th><th colspan=2>";
    if (grst(PROJECTTABLE,3))
        $out .= str_operations;
    $out .= "</th><th colspan='4'>".str_external_refs."</th>";
    
    //scientific name
    if (count($sorok)) {
        $out .= "<tr><td colspan='7' style='background-color:#dedddd'><div style='font-weight:bold;font-style:italic;'>".str_sci_names."</div>Selected and connected scientific names</td></tr>";
        foreach ($sorok as $s) {
            $out .= $s;
        }
    }
    //similar scientific names
    if (count($sorok_s)) {
        $out .= "<tr><td colspan='7' style='background-color:#dedddd'><div style='font-weight:bold;font-style:italic;'>".str_sim_names."</div>Scientific names which are  similar to the selected name. Probably mispelled names.</td></tr>";
        foreach ($sorok_s as $s) {
            $out .= $s;
        }
    }
    // alternative/common names
    if (count($sorok_c)) {
        $out .= "<tr><td colspan='7' style='background-color:#dedddd'><div style='font-weight:bold;font-style:italic;'>".str_non_sci_names."</div>A tudományos név alapján lekérdezve</td></tr>";
        foreach ($sorok_c as $s) {
            $out .= $s;
        }
    }
    //similar alternative/common names
    if (count($sorok_cs)) {
        $out .= "<tr><td colspan='7' style='background-color:#dedddd'><div style='font-weight:bold;font-style:italic;'>".str_non_sci_sim_names."</div>Common names which are similar to the selected name. Probably mispelled names.</td></tr>";
        foreach ($sorok_cs as $s) {
            $out .= $s;
        }
    }
    $out .= "</table>";

    if (grst(PROJECTTABLE,3)) {
        $out .= "<br><div class='infotitle'>
            Minden adatbázishoz tartozik egy automatikasan generált  <a href='$protocol://".URL."/?specieslist'>fajlista adattábla</a>. Ezek a fajlista táblák kapcsolják össze a fajok különböző elnevezéseit ill. tartalmazhatnak név státusz információt.<br><br>
            Státusz:<br>
            elfogadott név - elfogadott tudományos név<br>
            szinoním név - alternatív tudományos vagy nemzeti elnevezés<br>
            közönséges név - elfogadott nemzeti név<br>
            </div>";
        $out .= "<input type='button' class='button-warning button-xlarge pure-button' value='".str_modify."' id='name-change'>";
        $out .= "<br><span style='color:red;font-weight:bold' id='response'></span>";
    }
    return $out;
}

?>
