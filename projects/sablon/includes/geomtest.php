<?php
/* type-1 (url, wkt)
 *      wkt
 *      srid
 *      name
 *
 * type-2 (session)
 *      d
 *
 * type-3 (url, geojson)
 *      geojson
 *
 * */

session_start();

require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
include_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'languages.php');


if (isset($_POST['geojson_post'])) {
    //upload_dynamic_js call
    //itt rakjuk össze az adatot, ami visszejön majd ide
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random_string = substr( str_shuffle( $chars ), 0, 16 );

    $_SESSION['geomtest']=array();
    $_SESSION['geomtest']['id'] = $random_string;
    $_SESSION['geomtest']['srid'] = $_POST['srid'];
    $_SESSION['geomtest']['data'] = $_POST['geojson_post'];
    echo $random_string;
    exit;
}

$input_srid = '4326';
$errlog = "";
$input_geometry = "''";
$external_service = false;

/* We expecting WKT text or GeoJSON for parsing coordinates
 * This is the normal openbiomaps way */
if (isset($_GET['d']) and isset($_SESSION['geomtest']) and $_GET['d']==$_SESSION['geomtest']['id']) {
    $input_geometry = $_SESSION['geomtest']['data'];
    $input_srid =  $_SESSION['geomtest']['srid'];
    if(!is_geojson($input_geometry)){
        $errlog .= "JSON Parse Error";
        $geojson_parse = $input_geometry;
    }
}
/* We can display normal GET input (wkt or geojson) as well*/
else if(isset($_GET['wkt']) and $_GET['wkt']!='') {

    $external_service = true;

    $name = "Something";
    if (isset($_GET['name'])) $name = $_GET['name'];

    // dupla zárójeles stringet nem kezeli!!!
    if(!is_wkt($_GET['wkt'])) {
        $errlog .= "WKT Parse Error";
        $input_geometry = "''";//$_GET['wkt'];
    }
    else {
        $input_geometry = wkt_to_json($_GET['wkt']);
        $geojson = json_decode($input_geometry,true);
        #{
        #  "type": "Feature",
        #  "geometry": {
        #    "type": "Point",
        #    "coordinates": [125.6, 10.1]
        #  },
        #  "properties": {
        #    "name": "Dinagat Islands"
        #  }
        #}
        #{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[18.324707031553004,47.21072318701318],[18.445556640911015,47.5751526532584],[19.225585937677675,47.411842993008],[19.40136718765248,47.11361470808899],[19.335449218912153,46.75352215615761],[18.522460937774877,46.768574359974494],[18.072021484712543,46.557460775005204],[18.12695312533023,46.963870389447926],[17.984130859725145,47.158456062472325],[17.96215820347807,47.37465632407284],[18.324707031553004,47.21072318701318]]]},"properties":{"name":"Ember"}}],"crs":{"type":"name","properties":{"name":"EPSG:4326"}}}
        #
        $geojson = array("type"=>"FeatureCollection",
            "features"=>array(
                array("type"=>"Feature",
                      "geometry"=>array(
                          "type"=>$geojson['type'],
                          "coordinates"=>$geojson['coordinates']),
                      "properties"=>array("name"=>"$name"))),
                "crs"=>array("type"=>"name",
                             "properties"=>array("name"=>"EPSG:$input_srid")));

        $input_geometry = json_encode($geojson);
        $geojson_parse = $input_geometry;
    }
} elseif(isset($_GET['geojson'])) {
    
    $external_service = true;

    if (is_geojson($_GET['geojson']))
        $input_geometry = $_GET['geojson'];
    else {
        $errlog .= "JSON Parse Error";
        $input_geometry = $_GET['geojson'];
        $geojson_parse = $input_geometry;
    }
}

// processing srid from url parameters
if (isset($_GET['srid']) and is_numeric($_GET['srid'])) 
    $input_srid = $_GET['srid'];


$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title>OpenBioMaps - GEOMETRY TEST</title>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/mapsdata.css?rev=<?php echo rev('css/mapsdata.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/style.css?rev=<?php echo rev('css/style.css'); ?>" type="text/css" />

    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/OpenLayers.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/proj4js-compressed.js"></script>
    <!--<script type="text/javascript" src="http://<?php echo URL ?>/js/javascript.util.js"></script>-->
    <!--<script type="text/javascript" src="http://<?php echo URL ?>/js/jsts.js"></script>-->

    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://www.google.com/jsapi"></script>
<?php
    ## get google api key from:
    ## https://console.developers.google.com/apis/
    if ( file_exists( getenv('OB_LIB_DIR').'private/private_map_js.php' ) )
        include_once( getenv('OB_LIB_DIR').'private/private_map_js.php' );
    else
        echo "<script src='$protocol://maps.google.com/maps/api/js?v=3.5'></script>";

?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/ui/jquery-ui.min.js'); ?>"></script>

    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.cookie.js?rev=<?php echo rev('js/jquery.cookie.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('js/functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/maps.js?rev=<?php echo rev('js/maps.js'); ?>"></script>
    <script>
    Proj4js.defs["EPSG:32627"] = "+proj=utm +zone=27 +datum=WGS84 +units=m +no_defs";
    var zoom = 11, map, layer,
        inputProj = new OpenLayers.Projection('EPSG:<?php echo $input_srid ?>'), outputProj = new OpenLayers.Projection('EPSG:900913');
    var map,nav,drawControls,highlightLayer,drawLayer,popup,markerLayer;
    var zoom = 16;
    /* OpenLayers styles */
    var highlight_style = { 
    pointRadius:5,
    strokeColor:'#FFAC00',//orange
    strokeWidth: 2, 
    fillColor:'#529ccd', 
    fillOpacity:1 
    };
    var click_style = { 
    pointRadius:5,
    strokeColor:'#f60000',//red
    strokeWidth: 2, 
    strokeDashstyle: "solid",
    fillColor:'#000000', 
    fillOpacity:0,
    };

    var styleMap = new OpenLayers.StyleMap({
        "default": new OpenLayers.Style({
            pointRadius:6,
            strokeColor: "#f60000",//red
            strokeOpacity: 0.9,
            strokeWidth: 2,
            fillColor: "#000000",
            fillOpacity: 0.1,
            cursor: "pointer",
            graphicName: 'circle',
        }),
        "temporary": new OpenLayers.Style({
            pointRadius:"${getSize}",
            strokeColor: "#f60000",//red
            strokeOpacity: 1,
            strokeWidth: 3,
            fillColor: "#fff500",
            fillOpacity: .2,
            cursor: "default",
            graphicName: 'circle',
            },{ context: {
                getSize: function(feature) {
                return $("#buffer").val() / feature.layer.map.getResolution();
            }}
        }),
        "select": new OpenLayers.Style({
            pointRadius:6,
            strokeColor: "#0033ff",//blue
            strokeOpacity: .7,
            strokeWidth: 5,
            fillColor: "#0033ff",
            fillOpacity: 0,
            graphicZIndex: 2,
            cursor: "default",
            graphicName: 'circle',
        })
    });

    function init(){
        map = new OpenLayers.Map('map', {
                units: 'm',
                numZoomLevels: 30,
                controls: [
                    new OpenLayers.Control.Navigation(),
                    new OpenLayers.Control.ScaleLine(),
                    new OpenLayers.Control.MousePosition(),
                    new OpenLayers.Control.LayerSwitcher(),
                    new OpenLayers.Control.PanPanel(),
                    new OpenLayers.Control.ZoomPanel()
                ],
                projection: outputProj,
                displayProjection: inputProj 
        });
        var goomap = new OpenLayers.Layer.Google('Google satellite', {
                type: google.maps.MapTypeId.SATELLITE,
                sphericalMercator: true
        });
        var goohy = new OpenLayers.Layer.Google('Google Hybrid',{type:google.maps.MapTypeId.HYBRID,'sphericalMercator':'true'});
        var osmmap = new OpenLayers.Layer.OSM();
        var pointLayer = new OpenLayers.Layer.Vector("Point Layer");
        /* Ez a réteg veszi fel a selection rajzokat,
         * a pont és vonal után a 'buffer készítés közben törlődik */
        drawLayer = new OpenLayers.Layer.Vector( "Select features",{
            styleMap:styleMap,
            displayInLayerSwitcher: true,
            isBaseLayer: false,
            visibility:true,
            features:[],
        });

        pointLayer.events.on({ "sketchcomplete": OpenLayers.Function.bind(addpoint, this)});
        drawControls = {
            addPolygon: new OpenLayers.Control.DrawFeature(pointLayer,      OpenLayers.Handler.Polygon),
            addPoint: new OpenLayers.Control.DrawFeature(pointLayer,      OpenLayers.Handler.Point),
            addLine: new OpenLayers.Control.DrawFeature(pointLayer,      OpenLayers.Handler.Path),
        };
        for(var key in drawControls) { map.addControl(drawControls[key]); }
        /* highlight the selected points */ 
        highlightLayer = new OpenLayers.Layer.Vector("dummy", {});
        //map.addControl(panel);
        nav = new OpenLayers.Control.Navigation({
            'zoomBoxEnabled' : true,
            'handleRightClicks' : true,
            'defaultDblClick': function ( event ) { return; }
        });
        map.addControl(nav);
        map.addControl(new OpenLayers.Control.ScaleLine({"geodesic":true}));

        var featurecollection = <?php echo $input_geometry."\n"; ?>
        var geojson_format = new OpenLayers.Format.GeoJSON({
            'internalProjection': outputProj,
            'externalProjection': inputProj});
        var geojson_layer = new OpenLayers.Layer.Vector("Data test", {
            styleMap: new OpenLayers.StyleMap({
                "default": new OpenLayers.Style({
                    fillColor: 'purple',
                    fillOpacity: .8,
                    strokeColor: '#FFC800',
                    strokeWidth: 2,
                    pointRadius: 7,
                    label: '${name}',
                    fontColor: "white",
                    fontSize: "12px",
                    fontFamily: "Courier New, monospace",
                    fontWeight: "bold",
                    labelAlign: "lb",
                    labelXOffset: "8",
                    labelYOffset: "8",
                    labelOutlineColor: "black",
                    labelOutlineWidth: 2
                })
            }),
            projection: inputProj,
            strategies: [new OpenLayers.Strategy.Fixed()],
            protocol: new OpenLayers.Protocol()
        });
        map.addLayers([osmmap,goohy,goomap,drawLayer,geojson_layer]);
        eval(obj.loadq);

        if (featurecollection != "") {
            //console.log(featurecollection)
            geojson_layer.addFeatures(geojson_format.read(featurecollection));
            extent = geojson_layer.getDataExtent();
            
            //console.log(new OpenLayers.LonLat(extent.left, extent.top).transform(inputProj, new OpenLayers.Projection('EPSG:4326')));

            if(eval(extent.left - extent.right)==0 && eval(extent.top - extent.bottom)==0) {
                map.setCenter(
                    new OpenLayers.LonLat(extent.left, extent.top), 7
                );
            } else {
                map.zoomToExtent(extent);
            }
        } else {
            //v = JSON.parse($.cookie('lastZoom'));
            //console.log(v);
            if (typeof $.cookie('lastZoom')!=='undefined') {
                try {
                    v = JSON.parse($.cookie('lastZoom'));
                    var point = new OpenLayers.LonLat(v.center.lon, v.center.lat);
                    zoom = v.zoom;
                } catch (e) {}
            } else {
                var point = new OpenLayers.LonLat(obj.lon, obj.lat);
                point.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                zoom = obj.zoom;
            }
            map.setCenter(point, zoom);
        }
    };
</script>
</head>
<body onload='init();'>
<?php

if (count($_GET)==0) {

    echo "<h1>GeoJSON and WKT testing interface</h1><h3>This is a public OpenBioMaps service.</h3>
        Usage:<br>
        <a href='?wkt=POINT(21.13 47.23)'>?wkt=POINT(21.13 47.23)</a><br>
        <a href='?geojson={\"type\": \"Point\", \"coordinates\": [21.13, 47.45] }'>?geojson={\"type\": \"Point\", \"coordinates\": [21.13, 47.45] }</a><br>
        </body></html>";
    exit;

}

if ($errlog!='') {

    echo "<h3>$errlog</h3><div style='border:2px solid gray;padding:4px;width:auto'>";
    echo $input_geometry;
    if (isset($geojson_parse))
        echo format_json($geojson_parse,$html=true);
    echo "</div>";
    echo "<br><br><a href='https://en.wikipedia.org/wiki/Well-known_text' target='blank'>Well-known text on Wikipedia</a>";
    echo "<br><br><a href='http://geojsonlint.com/' target='blank'>GeoJSON tester</a>";
    echo "<br><br><a href='http://geojson.org/geojson-spec.html#id4' target='blank'>GeoJSON format specification</a>";

} else {

?>
<div id='map' style='position: absolute; right: 0px; top: 0px; width: 100%; height: 100%'></div>
<?php
    if ($external_service) {
        echo "<form method='get' style='position:absolute;right:20px;bottom:20px;z-index:100;'><textarea style='height:70px' name='wkt' placeholder='POINT(21.13 47.23)'>";
        if (isset($_GET['wkt'])) echo $_GET['wkt'];
        echo "</textarea><input type='submit' value='Map it!'></form>";
    }

    // upload process
    if (isset($_GET['addgeom'])) {
        //$buff = CLICK_BUFFER;
        $buff = 0;
        $out = "<div style='position:absolute;bottom:20px;right:10px;z-index:102'>". t(str_add_features).":&nbsp; 
    <label style='float:right'>
      <input type='radio' name='type' value='addPoint' id='pointToggle1' onclick='toggleControl(this);' />
      <span style='display:block;background-image:url($protocol://".URL."/css/img/add_point_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
    </label>
    <label style='float:right'>
      <input type='radio' name='type' value='addLine' id='pointToggle2' onclick='toggleControl(this);' />
      <span style='display:block;background-image:url($protocol://".URL."/css/img/add_line_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
    </label>
    <label style='float:right'>
      <input type='radio' name='type' value='addPolygon' id='pointToggle3' onclick='toggleControl(this);' />
      <span style='display:block;background-image:url($protocol://".URL."/css/img/add_polygon_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
    </label>";
        $out .= "</div>";
        $out .= "<div style='position:absolute;left:100px;bottom:20px' class='pure-form'>
    ".str_select_buffer_size.": <input style='vertical-align:middle' class='pure-input' type='range' id='bufferslide' min='0' max='1000' value='$buff'><input class='pure-input-rounded' id='buffer' value='$buff' style='width:5em;'> ".str_meter."</div>";
        $out .= "<div id='matrix' style='position:absolute;top:5px;left:30px;width:90%'></div>";
        echo $out;
    }
}
?>
</body>
</html>

