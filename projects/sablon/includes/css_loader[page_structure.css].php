<?php

header("Content-type: text/css", true);

if (file_exists('../css/private/page_structure.css')) {

    include_once('../css/private/page_structure.css');

} elseif (file_exists('../css/page_structure.css')) {
    
    include_once('../css/page_structure.css');

}
?>
