<?php
class transform_data {
    function  text_transform($p,$text,$col,$data_id='') {
        global $BID;
        $params = preg_split('/;/',$p);
        #Array("modszer:shunt","hely:geom");
        $pmi = array();
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);
            $pmi[$c[0]] = $c[1];
        }
        //multi column support in geom transformation
        //pmi {"end_point,start_point":"geom","uploading_id":"uplid"}
        //geometry to link
        $i = array_search('geom',$pmi);
        $iv = preg_split('/,/',$i);
        if ($i !== false and in_array($col,$iv)) {
            if (preg_match('/^point|line|polygon/i',$text))
                $cmd = "SELECT st_AsGeoJSON('$text'::text,15,0) as gjson, st_AseWKT('$text'::text) as wkt";
            else
                $cmd = "SELECT st_AsGeoJSON('$text'::geometry,15,0) as gjson, st_AseWKT('$text'::geometry) as wkt";

            /* Ha lenne a pontnak neve és van json type (psql 9.3<)
             * $cmd = "SELECT row_to_json(fc) FROM (SELECT 'Feature' As type, 
                      ST_AsGeoJSON('$text')::json As geometry, 
                      row_to_json((SELECT l FROM (SELECT 'valami' AS name) As l)) As properties) As fc";*/
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                            //{"type":"Point","coordinates":[18.767587411656972,47.785560590815244]}
                
                //$geojson = '{"type":"Feature","geometry": {"type": "Point","coordinates": [125.6, 10.1]},"properties": {"name": "Dinagat Islands"}}';
                $wkt = preg_replace('/SRID=\d+;/','',$row['wkt']);
                $w = strtolower(preg_replace('/[^MUPOINTLGESYR]/','',$wkt));
                return "<a href='' target='_blank' alt='".addslashes($row['gjson'])."' title='{$row['wkt']}' id='$data_id' class='coord_query'>$w</a>";
            }
        }
        //geometry to wkt
        $i = array_search('geom_nolink',$pmi);
        if ($i !== false and $i == $col) {
            $cmd = "SELECT st_AsGeoJSON('$text'::geometry,15,0) as gjson, st_AseWKT('$text'::geometry) as wkt";
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $wkt = preg_replace('/SRID=\d+;/','',$row['wkt']);
                $w = strtolower(preg_replace('/[^MUPOINTLGESYR]/','',$wkt));
                return "$w";
            }
        }
        //geometry to wkt
        $i = array_search('geom_wkt',$pmi);
        if ($i !== false and $i == $col and $text!='') {
            $cmd = "SELECT st_AseWKT('$text'::geometry) as wkt";
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                return $row['wkt'];
            }
        }

        // only year part from date string
        $i = array_search('date_yearonly',$pmi);
        if ($i !== false and $i == $col) {
           return sprintf('%d',preg_replace('/^(\d+).+/','$1',$text));
        }
        //translate text - allow multiple
	    $i = array_keys($pmi,'translate');
        if ($i !== false and in_array($col,$i)) {
            $translated = $text;
            if( preg_match('/^str_/',$text) and defined($text)) $translated = constant($text);
            return sprintf('%s',$translated);
        }

        /* test example
         * $i = array_search('idotartam',$pmi);
        if ($i !== false and $i == $col) {
           $m = $text%60;
           $h = floor($text/60);
           return "$h:$m";
        }*/
        return $text;

    }
}
?>
