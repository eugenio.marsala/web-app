<?php
class box_custom {
    
    private $path;
    
    function __construct($private_path)
    {
        $this->path = $private_path;
    }
    function print_box ($p='') {
        $params = preg_split('/;/',$p);
        $pmi = array();
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);

            if(file_exists($this->path."$c[0].php")) {
                if( php_check_syntax($this->path."$c[0].php",'e')) {
                    include_once($this->path."$c[0].php");
                    $classname=$c[0].'_Class';
                    $bc = new $classname();
                    $args = isset($c1) ? $c[1] : "";
                    return $bc->print_box($args);
                } else {
                    log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
                }
            }
        }
    }
    function print_js ($p='') {
        $params = preg_split('/;/',$p);
        $pmi = array();
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);
            if(file_exists($this->path."$c[0].php")) {
                if( php_check_syntax($this->path."$c[0].php",'e')) {
                    include_once($this->path."$c[0].php");
                    $classname=$c[0].'_Class';
                    $bc = new $classname();
                    $args = isset($c1) ? $c[1] : "";
                    return $bc->print_js($args);
                } else {
                    log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
                }
            }
        }
    }
}
?>
