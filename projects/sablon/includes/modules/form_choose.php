<?php
class form_choose {
    function print_list($params) {
        global $ID,$BID;

        $p = preg_split('/;/',$params);
        foreach ($p as $pm) {
            $list = preg_split('/:/',$pm);
        }
        
        $li = array();
        $list = form_choose_list();
        $g = array('ZZZZ'=>array());
        $c_order = array(0);

        foreach ($list as $form_id) {

            $row = form_element($form_id);

            if ($row['ft'] === 'api')
                continue;

            $f = array('<span class="fa-stack fa-lg" style="color:#C4C4C4"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x"></i></span>','<span class="fa-stack fa-lg" style="color:#C4C4C4"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x"></i></span>');

            foreach(explode('|',$row['ft']) as $ft) {
                if($ft=='web') {
                    $f[0] = sprintf('<a href="?form=%1$d&type=%2$s" title="%3$s"><span style="color:#3BA3C4" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-table"></i></span></a>',$row['form_id'],'web',str_upload_from_web);
                }
                elseif($ft=='file') {
                    $f[1] = sprintf('<a href="?form=%1$d&type=%2$s" title="%3$s"><span style="color:#3BA3C4" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-file"></i></span></a>',$row['form_id'],'file',str_upload_from_file);
                }
            }
            $label = $row['form_name'];
            if (preg_match('/^str_/',$label))
                if (defined($label))
                    $label = constant($label);

            $l = sprintf('<span>%2$s</span><span style="font-size:15px">%1$s</span>',$label,implode(' ',$f));

            //
            $cmd = sprintf("SELECT name,c_order FROM project_forms_groups WHERE project_table='%s' AND $form_id = ANY(form_id)",PROJECTTABLE);
            $res = pg_query($BID,$cmd);
            if(pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                if (!isset($g[$r['name']])) {
                    $g[$r['name']] = array();
                    $c_order[] = $r['c_order'];
                }
                $g[$r['name']][] = $l; 
            } else {
                $g['ZZZZ'][] = $l; 
            }

        }
        $g_sort = array();
        $count = count($c_order);
        $g_keys = array_keys($g);
        for($n=0;$n<$count;$n++) {
            #0 2 4 3 1
            $sort = $c_order[$n];
            $g_sort[$sort] = $g_keys[$n];
        }
        ksort($g_sort);
        $gk = array();
        foreach($g_sort as $n) {
            $gk[$n] = $g[$n];
        }
        $g = $gk;
        $list = "<ul id='formMenu'>";
        foreach($g as $k=>$v) {
            $sor = "<li>";
            $label = $k;
            if (preg_match('/^str_/',$label))
                if (defined($label))
                    $label = constant($label);

            if ($k!='ZZZZ') $sor .= "<h3><i class='fa fa-caret-right fa-fw'></i> $label</h3><ul class='noJS'>";
            foreach ($v as $ls) {
                if ($k!='ZZZZ')
                    $sor .= "<li>$ls</li>";
                else
                    $sor .= "<li style='padding-left:10px'>$ls</li>";
            }
            if ($k!='ZZZZ') $sor .= "</ul>";
            $sor .= "</li>";

            $list .= $sor;
        }
        $list .= "</ul>";

        return array($list);
    }   
}
            
?>
