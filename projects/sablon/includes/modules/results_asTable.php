<?php
/* roll table (full size) result view
 * 
 * */



if (isset($_GET['show']) or isset($_GET['view'])) {

    session_start();

    require_once(getenv('OB_LIB_DIR').'db_funcs.php');
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");
    if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        die("Unsuccesful connect to UI database.");
    if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");

    require_once(getenv('OB_LIB_DIR').'modules_class.php');
    require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    if(!isset($_SESSION['Tproject'])) { 
        require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
        st_col('default_table');
    }
    require_once(getenv('OB_LIB_DIR').'languages.php');

    $modules = new modules();

    if (isset($_GET['view'])) {
        $table = PROJECTTABLE;

        $_SESSION['force_use'] = array('results_asCSV' => true);
        $_SESSION['force_use'] = array('results_asTable' => true);
        $modules->_include('results_asTable','print_table_page',array());
        unset($_SESSION['force_use']);

    } else {
        $table = "";

        if ($modules->is_enabled('results_asTable',$table)) {
            $modules->_include('results_asTable','print_table_page',array());
        } else {
            echo "<h1>Sorry, you have no access to this function.</h1>";
            log_action("results_asTable module not enabled",__FILE__,__LINE__);
        }
    }
}

class results_asTable {

    function print_table_page($params) {

        /*
         * Show the current result as a table
         * it is called from the map page when the users click on the "show results as table button" 
         * EZ a wfs_response session változót dolgozza fel, habár jobb lenne ha önálló process hívást csinálna az XML fájlra!!
         * */

        global $ID;

        include_once(getenv('OB_LIB_DIR').'results_builder.php');

        $reverse = '';
        if (isset($_SESSION['orderby']))
            $reverse = $_SESSION['orderby'];

        $rows = 0;
        $table_type='';
        $encrypted_table_reference = '';
        $encrypted_order_reference = '';
        $ks = array('w'=>0,'i'=>'');

        if (isset($_GET['show'])) {

            if(!isset($_SESSION['token'])) exit;
            
            // prepare query results
            $r = new results_builder( ['method'=>'rolltable', 'print'=>'on', 'reverse'=>$reverse ]);
            if ($r->results_query()) {
                $ks = $r->printOut();
                $rows = $r->results_length();
            }
            $table_type = 'table';
        } elseif (isset($_GET['view']) and isset($_GET['table'])) {
            /* Direct table or report (VIEW) view
             *
             * */

            $header = array();

            require_once(getenv('OB_LIB_DIR'). 'modules_class.php');

            $state = false;
            $modules = new modules();

            if ($modules->is_enabled('read_table',PROJECTTABLE)) {
                list($schema,$table,$module_order) = $modules->_include('read_table','decode_link',$_GET['table']);

                $state = $modules->_include('read_table','check_table',"$schema.$table");
                
            }

            if ($state) {

                if (!isset($_SESSION['private_key']))
                    $_SESSION['private_key'] = genhash();


                $cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_schema = %s AND table_name = %s",quote($schema),quote($table));

                $orderby = 1;
                $ob = 1;
                if (isset($_SESSION['orderby'])) 
                    $ob = $_SESSION['orderby'];

                $gob = array();
                if (isset($module_order)) {
                    $gob = preg_split('/,/',$module_order);
                }

                if (isset($_GET['orderby'])) {
                    $gob = preg_split('/,/',$_GET['orderby']);
                }

                $res = pg_query($ID,$cmd);
                //$rows = pg_num_rows($res);
                $rows = '~';

                while($row = pg_fetch_assoc($res)) {
                    $header[$row['column_name']] = $row['column_name'];
                    if ($ob == $row['column_name'])
                        $orderby = $ob;

                }
                $hk = array_keys($header);
                $orderby_array = array();
                foreach ($gob as $g) {
                    $gn = preg_replace("/ DESC/","",$g);
                    $gn = preg_replace("/ ASC/","",$gn);
                    $gn = trim($gn);

                    if (in_array($gn,$hk))
                        $orderby_array[] = $g; 
                }
                if (count($orderby_array)) {
                    $orderby = implode(',',$orderby_array);
                }

                // prepare a table
                //$header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'upid'=>str_dataupid),$header);
                $ks['w'] = count($header)*121;

                //$columns = array_keys($data->csv_header);
                $viewports = '';
                foreach($header as $key=>$val) {
                    $viewports .= "<div id='$key' class='viewport-header' data-type='rolltable'><span style='display:inline-block;width:110px'>$val</span><i class='fa fa-sort' style='float:right'></i></div>";
                }

                $ks['i'] = "$viewports<div class='viewport-block'></div>";
                $table_type = 'normaltable';
                $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
                $iv = openssl_random_pseudo_bytes($ivlen);
                $_SESSION['openssl_ivs']['asTable_table'] = $iv;
                $encrypted_table_reference = base64_encode(openssl_encrypt("$schema.$table", $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));
                $iv = openssl_random_pseudo_bytes($ivlen);
                $_SESSION['openssl_ivs']['asTable_orderby'] = $iv;
                $encrypted_order_reference = base64_encode(openssl_encrypt($orderby, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));
            }
        }

        ?>
        <!DOCTYPE html>
        <html>
          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title><?php echo OB_DOMAIN ?> - <?php echo $rows.' rows' ?></title>
            <link rel="stylesheet" href="../../js/theme/default/style.css" type="text/css" />
            <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css?rev=<?php echo rev('css/font-awesome.min.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="../../css/style.css?rev=<?php echo rev('css/style.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="../../css/mapsdata.css?rev=<?php echo rev('css/mapsdata.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="http://<?php echo URL ?>/css/roller.css?rev=<?php echo rev('css/roller.css'); ?>" type="text/css" />
            <style>
            body {
                margin:0px 0px 0px 0px;
                background:#c4c4c4;
            }
            </style>
            <script type="text/javascript" src="../../js/jquery.min.js"></script>
            <script type="text/javascript" src="../../js/ui/jquery-ui.min.js"></script>
            <script type="text/javascript" src="../../js/dropit.js"></script>
            <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
            <script type="text/javascript" src="http://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('js/roller.js'); ?>"></script>
            <script type="text/javascript" src="http://<?php echo URL ?>/js/main.js?rev=<?php echo rev('js/main.js'); ?>"></script>
        </head>
        <body>
            <script type='text/javascript'>
        <?php
            echo "
            $(document).ready(function() {

                rr.set({rowHeight:20,cellWidth:120,borderRight:'1px solid #bababa',borderBottom:'1px solid #bababa',arrayType:'$table_type',Table:'$encrypted_table_reference',Order:'$encrypted_order_reference'});
                rr.prepare({$ks['w']},\"{$ks['i']}\");
                rr.render();

            });";
        ?>
            </script>
            <div id="scrollbar-header"></div>
            <div id="scrollbar" class="scrollbar"></div>
        </body>
        </html>
    <?php

    }

    function init_table($params,$data) {

        global $ID;
    
        $ACC_LEVEL = ACC_LEVEL;
        $modules = new modules();

        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_table_%s_%s",PROJECTTABLE,session_id()));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_roll_table_%s_%s (rlist text, ord integer)",PROJECTTABLE,session_id());
        pg_query($ID,$cmd);

        update_temp_table_index('roll_table_'.PROJECTTABLE.'_'.session_id());

        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY %3$s %4$s',PROJECTTABLE,session_id(),$_SESSION['orderby'],$_SESSION['orderascd']));

        $n=0;
        $cmd = '';

        $main_cols = dbcolist('columns',PROJECTTABLE);
        $main_cols[] = 'obm_id';
        $allowed_cols = $main_cols;
        $allowed_general_columns = array();

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
            $allowed_gcols = $modules->_include('allowed_columns','return_gcolumns',array($main_cols),true);
            $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array($main_cols),true);
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                $allowed_cols = array();
            }
        }

        $intersected_allowed_columns_count = count(array_intersect($_SESSION['current_query_keys'],$allowed_cols));
        $query_column_count = count($_SESSION['current_query_keys']);
        $GRST = grst(PROJECTTABLE,4);
        $general_restriction = 0;
        if (!$GRST and count($allowed_general_columns)) {
            $general_restriction = 1;
        }

        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_table_%s_%s FROM stdin",PROJECTTABLE,session_id()));
        $order = 0;
        while($row = pg_fetch_assoc($res)) {
            $line = array();
            $id = $row['obm_id'];
            $b_id = sprintf("%s",$id);
            $b_upd = $row['uploading_date'];
            $b_upid = $row['upid'];
            $b_upname = $row['uploader_name'];

            //add meta columns
            array_unshift($line,$b_upid);
            array_unshift($line,$b_upd);
            array_unshift($line,$b_upname);
            array_unshift($line,$b_id);

            $geometry_restriction = 0;
            $column_restriction = 0;
            if (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='1' or $row['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $geometry_restriction = 1;
            }
            elseif (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='2' or $row['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $column_restriction = 1;
            }

            $drop_line = 0;
            foreach($data->csv_header as $k=>$v) {
                if ($k == 'hist_time')
                    continue;
                if (isset($row[$k]))
                    $line[$k] = $row[$k];
                else
                    $line[$k] = "&nbsp;";

                // Sensitivity based restrictions
                // cleaning non-allowed content
                if ($column_restriction) {
                    if ( !in_array($k,$allowed_cols) ) {
                        $line[$k] = '';
                    }
                }
                elseif ($geometry_restriction ) {
                    if ( $k == 'obm_geometry')
                        $line[$k] = '';
                    if ( !in_array($k,$allowed_gcols) ) {
                        $line[$k] = '';
                    }
                }

                //non sensitivity based restrictions 
                if ($general_restriction) {
                    if ( !in_array($k,$allowed_general_columns) ) {
                        $line[$k] = '';
                    }
                }
            }
            pg_put_line($ID, json_encode($line,JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE)."\t$order\n");
            $order++;
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);
        
        unset($data->csv_header['hist_time']);

        $data->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'upid'=>str_dataupid),$data->csv_header);
        $k = count($data->csv_header)*121;

        //$columns = array_keys($data->csv_header);
        $viewports = '';
        foreach($data->csv_header as $key=>$val) {
            $viewports .= "<div id='$key' class='viewport-header' data-type='rolltable'><span style='display:inline-block;width:110px'>$val</span><i class='fa fa-sort' style='float:right'></i></div>";
        }

        $s = "$viewports<div class='viewport-block'></div>";

        return array('w'=>$k,'i'=>$s);

    }
}

