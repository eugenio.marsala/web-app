<?php
/* parameters:
 * sep=,;quote="
 *
 * */
class results_asCSV {
    var $filesize = 0;
    var $tmpfile = '';
    function print_data($params,$filename='',$q="'",$sep=',',$data) {
        global $ID,$BID;

        $params = preg_replace('/;;/','**;',$params);
        $params = preg_replace('/;/','##',$params);
        $params = preg_replace('/\*\*/',';',$params);

        $enc = '';
        if ($sep == '' or $q =='') {
            // default sep and quote as parameter
            $p = preg_split('/##/',$params);
            foreach($p as $param) {

                $list = preg_split('/=/',$param);
                if (count($list)>1) {
                    if ($list[0] == 'sep')
                        $sep = $list[1];
                    elseif($list[0] == 'quote')
                        $q = $list[1];
                    elseif($list[0] == 'encoding')
                        $enc = $list[1];
                }
            }
        }
        if ($sep == '' or $q =='') {
            $sep = ',';
            $q = '"';
        }

        /* MODULE INCLUDES HERE */
        $ACC_LEVEL = ACC_LEVEL;
        $modules = new modules();

        // potenciális óriás méret és a file méret kalkulálás kezelés miatt fájl írás olvasás
        // be lehetne olvasni a $_SESSION['wfs_full_array']-ból is!!!
        $filesize = 0;
        $row = "";
        foreach($data->csv_header as $cell) {
            $row .= sprintf('%1$s%2$s%1$s%3$s',$q,$cell,$sep);
        }
        $tmpfile = sprintf("%s%s_download.csv",OB_TMP,session_id());
        $handle = fopen($tmpfile, "w+");
        if (!$handle)
            log_action("Failed to create tmp file in tmp folder. The ".OB_TMP." directory should be exists and writable for webserver!",__FILE__,__LINE__);

        // header row
        $row = preg_replace("/$sep$/","",$row);
        $row .= "\n";
        if ($enc != '')
            $row = iconv("UTF-8", "$enc", $row);
        fwrite($handle,$row);
        $track_id = array();

        $main_cols = dbcolist('columns',PROJECTTABLE);
        $main_cols[] = 'obm_id';
        $allowed_cols = $main_cols;
        $allowed_cols = array_merge($allowed_cols,array('obm_id','uploader_name','uploading_date','upid'));
        $allowed_gcols = $allowed_cols;
        $allowed_general_columns = array();
        $general_restriction = 0;

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            if (!isset($_SESSION['load_loadquery']))
                $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
                $allowed_gcols = $modules->_include('allowed_columns','return_gcolumns',array($main_cols),true);
                $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array($main_cols),true);
                
                if (!grst(PROJECTTABLE,4) and count($allowed_general_columns)) {
                    $general_restriction = 1;
                }

                $allowed_cols = array_merge($allowed_cols,array('obm_id','uploader_name','uploading_date','upid'));
                $allowed_gcols = array_merge($allowed_gcols,array('obm_id','uploader_name','uploading_date','upid'));
                $allowed_general_columns = array_merge($allowed_general_columns,array('obm_id','uploader_name','uploading_date','upid'));
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                // drop all column for non logined users if access level is higher
                $allowed_cols = array('obm_id','uploader_name','uploading_date','upid');
                $allowed_gcols = $allowed_cols;
            }
        }


        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];

        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
            $res = pg_query($ID,sprintf('SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY obm_id',PROJECTTABLE,session_id()));
        else
            $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY obm_id',PROJECTTABLE,session_id()));

        $rows = pg_num_rows($res);

        while($line = pg_fetch_assoc($res)) {
            //add meta columns
            #array_unshift($line,$line['upid']);
            #array_unshift($line,$line['uploading_date']);
            #array_unshift($line,$line['uploader_name']);
            #array_unshift($line,$line['obm_id']);
            $cell = '';
            // ski0pping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($line)==1) {
                if ($line[0]==$chk) continue;
                else $chk = $line[0];
            }
            #$acc = 1;
            #if (!rst('acc',$line['obm_id'])) {
            #    $acc = 0;
            #}

            $geometry_restriction = 0;
            $column_restriction = 0;
            if (!grst(PROJECTTABLE,4) and isset($line['obm_sensitivity']) and ($line['obm_sensitivity']=='1' or $line['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$line['obm_read']))))
                    $geometry_restriction = 1;
            }
            elseif (!grst(PROJECTTABLE,4) and isset($line['obm_sensitivity']) and ($line['obm_sensitivity']=='2'or $line['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$line['obm_read']))))
                    $column_restriction = 1;
            }

            // print cells
            foreach($data->csv_header as $k=>$v) {

                if ($column_restriction) {
                    if ( !in_array($k,$allowed_cols) ) {
                        $line[$k] = 'NA';
                    }
                }
                elseif ($geometry_restriction ) {
                    if ( $k == 'obm_geometry')
                        $line[$k] = 'NA';
                    if ( !in_array($k,$allowed_gcols) ) {
                        $line[$k] = 'NA';
                    }
                }
                //non sensitivity based restrictions 
                if ($general_restriction) {
                    if ( !in_array($k,$allowed_general_columns) ) {
                        $line[$k] = 'NA';
                    }
                }

                if (isset($line[$k])) {
                    if ($enc != '')
                        $cl = iconv("UTF-8", "$enc", $line[$k]);
                    else
                        $cl = $line[$k];
                } else
                    $cl = "";

                if (is_numeric($cl)) $text = $cl;
                else $text = sprintf('%1$s%2$s%1$s',$q,$cl);
                $cell .= sprintf('%s%s',$text,$sep);
            }
            $row = sprintf("%s\n",$cell);
            fwrite($handle,$row);
            // ez a tömb túl nagyra nőhet!!
            $track_id[] = $line['obm_id'];
        }

        track_download($track_id);
        fclose($handle);
        $filesize = filesize($tmpfile);
        if ($modules->_include('download_restricted','downloadAccepted')) {
            // if not only the users own data is in the results download is restricted
            $modules->_include('download_restricted','newRequest',['tmpfile' => $tmpfile, 'filename' => $filename, 'message' => $_POST['message']],true);
            return;
        }

        # header definíció!!!
        header("Content-Type: text/csv");
        header("Content-Length: $filesize"); 
        header("Content-Disposition: attachment; filename=\"$filename\";");
        header("Expires: -1");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);

        //easter eggs
        if (date('m-d')=='04-01' and isset($_GET['42'])) {
            if (file_exists(getenv('OB_LIB_DIR').'ascii')) {
                $f = getenv('OB_LIB_DIR').'ascii';
                $str = array(); 
                $handle = fopen($f, "r");
                if ($handle) {
                    $i = 0;
                    while (($line = fgets($handle)) !== false) {
                        if ($line!="\n") {
                            $str[$i] .= $line;
                        } else {
                            $i++;
                        }
                    }
                    fclose($handle);
                }
                echo base64_decode($str[array_rand($str)]);
                exit;
            }
        }
        $handle = fopen($tmpfile, "r");
        print(fread($handle, $filesize));
        fclose($handle);
        unlink($tmpfile);

        return;
    }

}
?>
