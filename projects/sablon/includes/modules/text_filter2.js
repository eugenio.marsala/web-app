/* text filter module
 *
 * */
var species_plus = new Array();

// taxon_filter function
function setTaxon( message ) {
    var a = message.split('#');
    species_plus = species_plus.concat(a[0] + '#' + a[1]); 
    $('<button class="pure-button button-href remove-species" value="' + a[0] + '#' + a[1] + '">' + a[2] + ' &nbsp; <i class="fa fa-close"></i></button>').prependTo( '#tsellist' );
    $('#taxon_sim').val('');
    $('#taxon_sim').html('');
    $('#taxon_sim').text('');
    $('#taxon_sim').focus();
}
function getParamsOf_text_filter() {
    // text filter variables processing
    var taxon_val = '';
    var trgm_string = '';
    var allmatch = 0;
    var onematch = 0;
    var qids = new Array();
    var qval = new Array();
    //cleaning taxon filter
    //$("#tsellist").html('');
    //
    // a map_filter_box.php.inc-ben lévő 
    // qf class input/select metők tartalmai
    // kerülnek a myVar objectbe
    $('.qf').each(function(){
        var stri = '';
        var stra = new Array();
        var qid=$(this).attr('id');
        if( $(this).prop('type') == 'text' ) {
            stra.push($(this).val());
        } else if ($(this).prop('type') == 'button' ) {
            stra.push(($(this).find('i').hasClass('fa-toggle-on')) ? 'on' : '');
        }
        else if ($(this).hasClass('divInput')) {
            const id = [];
            $(this).children().each(function() {
                if ($(this).hasClass('search-relation'))
                    id.push($(this).html());
                else
                    id.push($(this).data('id'));
            });
            stra = id;
        } else {
            $('.mapfb').find('#' + qid + ' option:selected').each(function () {
                stra.push($(this).val());
            });
        }
        if (stra.length){
            qids.push(qid);
            qval.push(JSON.stringify(stra));
        }
    });
    /* többszörös fajválasztó */
    if (species_plus.length){
        trgm_string = JSON.stringify(species_plus);
        //species_plus = new Array();
    }
    if ($('#allmatch').is(':checked')) {
        allmatch = 1;
    }
    if ($('#onematch').is(':checked')) {
        onematch = 1;
    }

    var myVar = { trgm_string:trgm_string,allmatch:allmatch,onematch:onematch, }
    // text filter variables
    for (var i=0; i < qids.length; i++) {
        myVar['qids_' + qids[i]] = qval[i];
    }
    return myVar;
}

$(document).ready(function() {

    text_filter_enabled = 1;
    $('#mapholder').on('click','#taxon-search-settings',function() {
        $('#taxon-search-settings-box').toggle();
    });


    $('#taxon_sim').autocomplete({
        source: function( request, response ) {
            $.post(projecturl+'includes/getList.php', {type:'taxon',term:$('#taxon_sim').val()}, function (data) {
                    response(JSON.parse(data));
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            setTaxon( ui.item.id + '#' + ui.item.meta + '#' + ui.item.value);
            $(this).val('');
            return false;
        }
    });
    /* remove species
     * */
    $('body').on('click','.remove-species',function() {
        for( var i = 0; i < species_plus.length; i++) { 
            if ( species_plus[i] === $(this).val()) {
                species_plus.splice(i, 1); 
            }
        }
        $(this).remove();
    });

    $('#colourringdiv').appendTo('#body');
    

    /* Clear incremntal filters
     *
     * */
    $('#mapfilters').on('click','#clear-filters',function(){
        species_plus = new Array();
        $('#tsellist').find('button').remove();
        $('.divInput').find('div').remove();
        $('.mapfb').find('input:text').val('');
        $('.mapfb').find('textarea').html('');
        $('.mapfb').find('button').each(function() {
            if ($(this).find('i').hasClass('fa-toggle-on')) {
                $(this).find('i').toggleClass('fa-toggle-on');    
                $(this).find('i').toggleClass('fa-toggle-off');    
                $(this).removeClass('button-success');
            }
        });
        $('.mapfb').find('select').find('option').removeAttr('selected');
        geometry_query_neighbour = 0;
        geometry_query_session = 0;
        geometry_query_selection = 0;
        drawLayer.destroyFeatures();
        $('#taxon_trgm').html('');
    });


    $(".filter-sh-advopt").on("change",function() {
        var val = $(this).val();
        const group = $(this).data('group');
        //turning on/off the groupped-nested filters
        if (group) {
            const nextElem =$("input.filter-sh-advopt[value="+group+"]");
            const checked = nextElem.is(":checked");
            nextElem.prop('checked', !checked).trigger('change');
        }
        if($(this).is(':checked')){
            $(this).closest('table').find('.text-options_'+$(this).val()).each(function(){
                if($(this).hasClass('def-hidden')){
                    $(this).removeClass('def-hidden');
                }
            });
        } else {
            $(this).closest('table').find('.text-options_'+$(this).val()).each(function(){
                if(!$(this).hasClass('def-hidden')){
                    $(this).addClass('def-hidden');
                    //clear search text if search item is closed
                    $("#" + val).val('');
                }
            });
        }
    });

    // mapfilterbox query text filter autocomplete wrapper
    $('#mapfilters').on('input','.qfautocomplete',function() {
        var ref = $(this).attr('id');
        var thise = $(this);
        // query table 
        var etr = $(this).data('etr');
        var nested_column = $(this).data('nested_column');
        var nested_element = $(this).data('nested_element');
        var all_options = $(this).data('all_options');
        $('#'+nested_element+' option').remove();

        // optional extension for speeding up query, if autocomplete table contains a column with clustered/indexed value
        var speedup_filter = 'off';
        var speedup = $(this).closest('.mapfb').find('.speedup_filter');
        if (typeof speedup != 'undefined') {
            if (speedup.val()=='on')
                speedup_filter = 'on';
        }
        thise.css('background-color','white');


        $('.qfautocomplete').autocomplete({

            source: function( request, response ) {
                thise.css('cursor','progress');
                $.post('ajax', {'m':'text_filter','qflist':1,'id':ref,'etr':etr,'term':request.term,'speedup_filter':speedup_filter}, function (data) {
                    var retval = jsendp(data);
                    if (retval['status']=='error') { 
                        thise.css('cursor','default');
                        thise.css('background-color','red');

                        if (retval['message'] == 'Page expired') {
                            alert(retval['message']);
                            location.reload();
                        }
                    } else if (retval['status']=='fail') {
                        alert('autocompleting failed');
                    } else if (retval['status']=='success') { 
                        response(JSON.parse(retval['data']));
                    }
                    thise.css('cursor','default');
                });
            },
            select: function( event, ui ) {
                // select element from list fireing the nested list creation
                if (typeof nested_element != 'undefined') {
                    if (ui.item.value=='')
                        return;

                    thise.css('cursor','progress');
                    $.post('ajax', {'m':'text_filter','create_dynamic_menu':1,'filter_column':ref,'etr':etr,'filter_term':ui.item.value,'nested_column':nested_column,'nested_element':nested_element,'all_options':all_options}, 
                        function (data) {
                            $('#'+nested_element).append(data);
                            thise.css('cursor','default');
                        });
                }
            },
            minLength: 1,
        });
    });

    /* obm_datum
     * date query events 
     * */
    $( "#obm_uploading_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      constrainInput: false,
      showAnim: "fold"
    });
    $( "#exact_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      constrainInput: false,
      showAnim: "fold"
    });
    $( "#exact_from" ).datepicker({
      defaultDate: "-1w",
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      onClose: function( selectedDate ) {
        $( "#exact_to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#exact_to" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      onClose: function( selectedDate ) {
        $( "#exact_from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    $( "#exact_mfrom" ).datepicker({
      changeMonth: true,
      dateFormat:'mm-dd',
      firstDay:1,
      onClose: function( selectedDate ) {
        $( "#exact_to" ).datepicker( "option", "minDate", selectedDate );
      },
      beforeShow: function (input, inst) {
        inst.dpDiv.addClass('NoYearDatePicker');
      },
      onClose: function(dateText, inst){
                inst.dpDiv.removeClass('NoYearDatePicker');
      }
    });
    $( "#exact_mto" ).datepicker({
      changeMonth: true,
      dateFormat:'mm-dd',
      firstDay:1,
      onClose: function( selectedDate ) {
        $( "#exact_from" ).datepicker( "option", "maxDate", selectedDate );
      },
      beforeShow: function (input, inst) {
        inst.dpDiv.addClass('NoYearDatePicker');
      },
      onClose: function(dateText, inst){
                inst.dpDiv.removeClass('NoYearDatePicker');
      }
    });

    // nested menus ajax trigger call
    $('#mapfilters').on('change select','.nested',function() {
        var ref = $(this).attr('id');
        // query table 
        var nested_element = $(this).data('nested_element');
        $("#"+nested_element+" option").remove();

        // if this list creates an other list depending on the chosen value
        var filter_column = $('#'+nested_element).data('filtercolumn');
        var etr = $('#'+nested_element).data('etr');
        var value_column = $('#'+nested_element).data('valuecolumn');
        var label_column = $('#'+nested_element).data('labelcolumn');

        var selectted_option =$("option:selected",this);
        var filter_term = (selectted_option.data('id')) ? selectted_option.data('id') : selectted_option.attr('value');

        var all_options = $(this).data('alloptions');
        if ($("option:selected",this).text()=='')
            return;
        $.post("ajax", {'m':'text_filter2','create_dynamic_menu':1,'filterColumn':filter_column,'etr':etr,'filterValue': filter_term,'nested_element':nested_element,'allOptions':all_options,'valueColumn':value_column,'labelColumn':label_column}, 
            function (data) {
                $('#'+nested_element).append(data);
            });
    });


    /* Text filter admin interface
     *
     * */
    $("body").on("click",".delete-text-filter-param",function() {
        let data = JSON.parse(window.atob($("#text-filter-params").data('json')));
        const target = $(this).next('span').html();
        delete data[target];

        $('#text-filter-params').data('json', window.btoa(JSON.stringify(data)))
        $('#text-filter-params').siblings('textarea').html(JSON.stringify(data));
        $(this).parent().remove();
    });

    $("body").on("click",".add-text-filter-param",async function() {
        $('.basic-settings').removeClass('hidden');

        let buttons = $('.text-filter-buttons');

        buttons.html("<button class='pure-button button-small button-warning add-text-filter-param'><i class='fa fa-refresh'></i> Reset </button>");
        buttons.append("<button class='pure-button button-small button-success set-text-filter-param'><i class='fa fa-floppy-o'></i> Set </button>");
        buttons.append("<button class='pure-button button-small button-success advanced-settings-btn'><i class='fa fa-wrech'></i> Advanced settings </button>");

    });


    $('body').on('click','.advanced-settings-btn', async function() {
        $(this).remove();
        $('.advanced-settings').removeClass('hidden');

    });
    
    
    $('body').on('change','#optionsTable', async function() {
        let mtable = this.value;

        const column_list = await $.get('ajax',{
            'm':'text_filter2',
            'action': 'getColumnList',
            'mtable': mtable,
            'id': 'valueColumn',
            'oo': true,
        });

        $('#valueColumn').html(column_list);
        $('#preFilterColumn').html(column_list);


    });

    $('body').on('change','#preFilterColumn', async function() {
        let table = $('#optionsTable option:selected').text();
        if (this.value == '') {
            $('#preFilterValue').html('');
        }
        else {
            const preFilterValue = await $.get('ajax',{
                'm':'text_filter2',
                'action': 'getDistinctValues',
                'mtable': table,
                'column': this.value,
                'id': 'preFilterValue',
                'oo': true,
            });

            $('#preFilterValue').html(preFilterValue);
        }
    });

    $("body").on("click", ".set-text-filter-param", async function() {
        var tfp = $('.tfp');
        const params_string = window.atob($("#text-filter-params").data('json'));
        console.log(params_string);
        let data = (params_string.length) ? JSON.parse(params_string) : {};
        let newParams = '';
        let target = '';
        
        tfp.each(function(elem) {
            if (this.id == 'target_column') {
                target = this.value;
                data[target] = {};
                newParams += '<div class="text-filter-param" style="background: wheat"> <button class="edit-text-filter-param pure-button button-small button-warning"><i class="fa fa-edit"> </i> </button> <button class="delete-text-filter-param pure-button button-small button-danger"><i class="fa fa-trash-o"> </i> </button> <span>'+ this.value +'</span> </div>';
            }
            const paramOptions = ['optionsTable','valueColumn','preFilterColumn'];
            if ((paramOptions.indexOf(this.id) >= 0) && (this.value !== '')) {
                console.log(this.value);
                data[target][this.id] = this.value
            }
            if (this.id == 'preFilterValue' && this.value !== '') {
                const options = this.selectedOptions;
                let values = [];
                $.each(options,function(key, elem) {
                    values.push(elem.value);
                });
                data[target]['preFilterValue'] = values;
            }
            if (this.id == 'autocomplete' && this.checked) {
                data[target]['autocomplete'] = 'true';
            }


        });

        $('#text-filter-params').data('json', window.btoa(JSON.stringify(data)));
        $('#text-filter-params').append(newParams);
        $('#text-filter-params').siblings('textarea').html(JSON.stringify(data));
    });




});
