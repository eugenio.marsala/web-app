<?php
class results_specieslist {
    function print_speciestable($params) {
        global $ID,$BID;
        $specieslist='';

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $st_col = st_col($_SESSION['current_query_table'],'array');
        $SPECIES_C = $st_col['SPECIES_C'];

        $p = preg_split('/;/',$params);
        foreach($p as $pm) {
            if ($pm != '')
                $SPECIES_C = $pm;
        }

        if ($SPECIES_C=='') return;

        /* MODULE INCLUDES HERE */
        $ACC_LEVEL = ACC_LEVEL;
        $modules = new modules();

        // visible columns restriction
        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $allowed_cols = $main_cols;

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            if (isset($_SESSION['load_loadquery']) and !$_SESSION['load_loadquery'])
                $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                $allowed_cols = array();
            }
        }

        // default integrated module
        // a fajnév mezőt berakja az uspecies tömbbe
        $SPECIES_C_colname = $SPECIES_C;
        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$SPECIES_C,$m)) {
            $SPECIES_C_prefix = $m[1];
            $SPECIES_C_colname = $m[2];
        }

        if (isset($st_col['NUM_IND_C']) and $st_col['NUM_IND_C']!='')
            $sum_ind = sprintf('SUM(%s::integer)',$st_col['NUM_IND_C']);
        else
            $sum_ind = "'NA'";

        // snesitive data handling
        // query only for the allowed users or groups
        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        if (grst(PROJECTTABLE,4))
            $super = TRUE;
        else
            $super = FALSE;

        // we do not overwrite the results of loaded queries!!!
        if (isset($_SESSION['load_loadquery']) and $_SESSION['load_loadquery'])
            $super = TRUE;

        if ($super) {
            $cmd = sprintf('SELECT DISTINCT ON (%1$s) %1$s AS species,COUNT(%1$s) AS counter,%4$s AS num FROM temporary_tables.temp_query_%2$s_%3$s GROUP BY %1$s',$SPECIES_C_colname,PROJECTTABLE,session_id(),$sum_ind);
        } else {

            if (isset($_SESSION['st_col']['RESTRICT_C']) and $_SESSION['st_col']['RESTRICT_C']) {

                $puy = 1;
                $intersected_allowed_columns_count = count(array_intersect($_SESSION['current_query_keys'],$allowed_cols));
                $query_column_count = count($_SESSION['current_query_keys']);
                if ($intersected_allowed_columns_count != $query_column_count) {
                    $puy = 0;
                }

                // exclude sensitive data if rules allowed
                $cmd = sprintf('SELECT DISTINCT ON (%1$s) %1$s AS species,COUNT(%1$s) AS counter,%4$s AS num FROM temporary_tables.temp_query_%2$s_%3$s
                    LEFT JOIN %2$s_rules r ON ("data_table"=\'%7$s\' AND obm_id=r.row_id) 
                    WHERE 
                    (sensitivity::varchar IN (\'1\',\'2\',\'3\',\'no-geom\',\'restricted\',\'only-own\') AND ARRAY[%5$s] && read) OR (sensitivity::varchar IN (\'1\',\'2\',\'no-geom\',\'restricted\') AND 1=%6$s ) OR sensitivity::varchar IN (\'0\',\'public\') OR sensitivity IS NULL
                    GROUP BY %1$s',$SPECIES_C_colname,PROJECTTABLE,session_id(),$sum_ind,$tgroups,$puy,$_SESSION['current_query_table']);

            } else {
                $cmd = sprintf('SELECT DISTINCT ON (%1$s) %1$s AS species,COUNT(%1$s) AS counter,%4$s AS num FROM temporary_tables.temp_query_%2$s_%3$s GROUP BY %1$s',$SPECIES_C_colname,PROJECTTABLE,session_id(),$sum_ind);
            }
        }


        $res = pg_query($ID,$cmd);
        if(pg_last_error($ID)) {
            log_action("specieslist module sql execution failed: $cmd",__FILE__,__LINE__);
            return;
        }
        $uspecies = array();
        $uspecies_counter = array();
        $uspecies_num = array();
        while($row = pg_fetch_assoc($res)) {
            $uspecies[] = $row['species'];
            $uspecies_counter[$row['species']] = $row['counter'];
            $uspecies_num[$row['species']] = $row['num'];
        }
        //$res2 = pg_query($ID,sprintf('SELECT %1$s,COUNT(%1$s) AS counter FROM temporary_tables.temp_query_%2$s_%3$s GROUP BY %1$s',$_SESSION['st_col']['SPECIES_C'],PROJECTTABLE,session_id()));

        //create specieslist div

        if (count($uspecies)) {

            $u = implode(',',array_map('quote',$uspecies));
            
            if (!isset($_SESSION['match']) || $_SESSION['match'] == 'all') {
                $cmd = sprintf("SELECT taxon_id FROM %s_taxon WHERE word IN (%s) GROUP BY taxon_id",PROJECTTABLE,$u);
                $res = pg_query($ID,$cmd);
            }
            else {
                $cmd = sprintf("SELECT meta FROM %s_taxon WHERE word IN (%s)",PROJECTTABLE,$u);
                $res = pg_query($ID,$cmd);
            }

            $k = '';
            $kn = 0;
            $tn = array();
            $taxon_names = array();
            $taxon_names_counter = array();
            $we = array();

            while ($row = pg_fetch_assoc($res)) {
                if (!isset($_SESSION['match']) || $_SESSION['match'] == 'all') {
                    $cmd = sprintf("SELECT * FROM %s_taxon WHERE taxon_id='%d' AND lang='%s' ORDER BY status",PROJECTTABLE,$row['taxon_id'],$SPECIES_C_colname);
                }
                else {
                    $cmd = sprintf("SELECT * FROM %s_taxon WHERE meta LIKE '%s' AND lang='%s' ORDER BY status",PROJECTTABLE,$row['meta'],$SPECIES_C_colname);
                }
                $res2 = pg_query($ID,$cmd);
                ## more than one status name available

                if (pg_num_rows($res2)>1) {
                    $e = 1; 
                } else {
                    $e = 0;
                }
                $trans = array();
                while ($row2 = pg_fetch_assoc($res2)) {
                    if ($e) {
                        $we[$row2['word']] = $row['taxon_id'];

                        if ($row2['status']==1) {
                            //status is 1
                            $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                            $trans[$row2['taxon_id']] = 1;
                            continue;
                        }
                        elseif($row2['status']==0) {
                            //status is 0
                            if ($key = array_search($row2['taxon_id'], array_column($tn, 'id'))) {
                                $tn[$key] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                            }
                            else
                            $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                            $trans[$row2['taxon_id']] = 0;
                        }
                        elseif(!isset($trans[$row2['taxon_id']]))  {
                            //status is 2,3
                            //only one alternative name can come into this array
                            $trans[$row2['taxon_id']] = 0;
                            $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                        }
                    }
                    else {

                        $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                    }
                }
            }

            foreach ($tn as $key => $row) {
                $names[$key]  = $row['name'];
            }
            array_multisort($names, SORT_ASC, $tn);


            $_SESSION['current_specieslist'] = array();
            ## create species list DIV

            $taxon_name_vals = array_values($names);
            $kfin = array();

            foreach ($tn as $taxon_names) {
                // más státusz név esetén darabszám átmentése a másik névhez
                // {"Periparus ater":"265","Parus ater":"265","Chloris chloris":"40","Carduelis chloris":"40","Buteo buteo vulpinus":"397","Buteo buteo":"397","Poecile montanus":"332","Parus montanus":"332"}
                // nem értem, nem tudom, hogy ez jó-e valamire

                $id = $taxon_names['id'];
                $name = $taxon_names['name'];
                if (!isset($uspecies_counter[$name])) {
                    $weid = $we[$name];
                    $wekeys = array_keys($we);
                    foreach($wekeys as $key) {
                        $val = $we[$key];
                        if ($val==$weid and isset($uspecies_counter[$key])) {
                            $uspecies_counter[$name] = 0;
                            $uspecies_num[$name] = 0;
                        }
                    }

                }
                // ez így biztos jó!
                // joined names exists!
                // synonim names
                if (isset($we[$name])) {
                    foreach($we as $we_key=>$we_id) {
                        if ($id == $we_id and !in_array($we_key,$names) and isset($uspecies_counter[$we_key])) {
                            $uspecies_counter[$name] += $uspecies_counter[$we_key];
                            $uspecies_num[$name] += $uspecies_num[$we_key];
                        }
                    }

                }

                $kfin[] = sprintf('<tr><td style="white-space:nowrap"><a href="'.$protocol.'://'.URL.'/?metaname&id=%1$d" target="_blank">%2$s</a></td>
                    <td style="background-color:#dadada;text-align:center"><a href="'.$protocol.'://'.URL.'/?query_filter=%2$s" class="recursive_species_filter" name="%2$s" id="%5$s">%3$d</a></td>
                    <td style="background-color:#dadada;text-align:center"><a href="'.$protocol.'://'.URL.'/?query_filter=%2$s" class="recursive_species_filter" name="%2$s" id="%5$s">%4$s</a></td></tr>',$id,$name,$uspecies_counter[$name],$uspecies_num[$name],$id);

                //its needed for save specieslist as txt
                $_SESSION['current_specieslist'][] = sprintf('"%s","%d","%s"',$name,$uspecies_counter[$name],$uspecies_num[$name]);

                $kn++;
            }

            if (count($kfin)<30 ) {
                $k = sprintf("<p>".str_no_of_taxons .": $kn </p><table class='specieslist' style='border-collapse:separate;border-spacing:1px'><tr><th>".str_species."</th><th>".str_no_records."</th><th>".str_no_inds."</th></tr>%s</table><br>",sprintf("%s",implode('',$kfin)));

            } else {
                $count_kfin = count($kfin)/3;

                $a = array_slice($kfin,0,$count_kfin);
                $b = array_slice($kfin,$count_kfin,$count_kfin);
                $c = array_slice($kfin,$count_kfin+$count_kfin);
                $k = sprintf("<p>".str_no_of_taxons .": $kn </p>".'<table style="border-spacing:25px;border-collapse:separate;table-layout:fixed"><tr><td style="vertical-align:top;width:%4$s">%1$s</td><td style="vertical-align:top;width:%4$s">%2$s</td><td style="vertical-align:top;width:%4$s">%3$s</td></tr></table>',
                    sprintf("<table class='specieslist' style='border-collapse:separate;border-spacing:1px'><tr><th>".str_species."</th><th>".str_no_records."</th><th>".str_no_inds."</th></tr>%s</table>",implode('',$a)),
                    sprintf("<table class='specieslist' style='border-collapse:separate;border-spacing:1px'><tr><th>".str_species."</th><th>".str_no_records."</th><th>".str_no_inds."</th></tr>%s</table>",implode('',$b)),
                    sprintf("<table class='specieslist' style='border-collapse:separate;border-spacing:1px'><tr><th>".str_species."</th><th>".str_no_records."</th><th>".str_no_inds."</th></tr>%s</table>",implode('',$c)),'33.3%'
                );
            }

            $download_button = sprintf("%s",button("queries.php?splist=list.txt",str_download_specieslist,'fa fa-download'));
            return $k.$download_button;
        }


    }

    function print_citetable($params) {
        global $ID;

        $st_col = st_col($_SESSION['current_query_table'],'array');

        $cite = array();
        //create data->cite
        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%2$s_%3$s','obm_id',PROJECTTABLE,session_id()));
        while($row = pg_fetch_assoc($res)) {
            $row_keys = array_keys($row);
            foreach($row_keys as $i) {
                if (in_array($i,$st_col['CITE_C'])) {
                    $cite[] = $row[$i];
                }
            }
        }

        $names = array();
        foreach(array_unique($cite) as $n1){
            foreach( explode(',',$n1) as $n2) {
                $names[] = trim($n2);
            }
        }
        $names = array_filter(array_unique($names));
        asort($names);

        return implode(', ',$names);
    }
    function print_js() {
          echo "
    // results specieslist module
    $(document).ready(function() {
    /* modules/result_specieslist.php
     * Click on taxon num on results_specieslist (module) output
     * Creating a subfilter
     * */
    $(document).on('click','.recursive_species_filter',function(e){

        e.preventDefault();

        var id = $(this).attr('id');
        var name = $(this).attr('name');
        
        var trgm_string = JSON.stringify(new Array(''+id+'#'+name+''));
        var myVar = { trgm_string:trgm_string,allmatch:1,onematch:0,morefilter:2 }
        loadQueryMap(myVar);
    });

    });";
  
    }
}
?>
