<?php
class custom_data_check {
    function list_checks($params) {
        $params = preg_split('/;/',$params);
        return $params;
    }
    function custom_check($params,$function,$data_pile,$column,$header) {
        if (preg_match('/^select_list\(/',$function))
            return true;
        $params = preg_split('/;/',$params);
        if ($function=='') {
            log_action('Function name is missing!',__FILE__,__LINE__);
            return;
        }
        if (function_exists($function))
            return call_user_func_array(array($this,$function),array($params,$data_pile,$column,$header));
        else
            log_action("Function does not exist: $function!",__FILE__,__LINE__);
            return;
    }
    
    /*  PUT UNDER THIS BLOCK YOUR CUSTOM FUNCTIONS
     *
     *
     *
     *  */

    /* example custom function 
     * return value: TRUE/FALSE
     *
     * params:
     * header: column names of form columns
     * */
    function colour_rings($params,$data_pile,$column,$header) {
        global $ID;
        // data_pile: whole row
        // row: row number
        // column: selected column number

        $population_index = array_search('population',$header);
        $ring_index = array_search('ring',$header);
        $species_index = array_search('species',$header);
        $code_index = array_search('code',$header);

        // Test 1: Does the given METAL RING NUMBER exist in the database for the given population and species?
        $cmd = sprintf("SELECT code,sex FROM plover_captures WHERE ring=%s AND population=%s AND species=%s",quote($data_pile['ring']),quote($data_pile['population']),quote($data_pile['species']));
        $res = pg_query($ID,$cmd);
        // Test 1 is Yes:
        if (pg_num_rows($res)) {
            // existing metal ring
            $codes = array();
            $return = array();
            while($pgrow = pg_fetch_assoc($res)) {
                // only one test in this way
                $codes[] = $pgrow['code'];

                // Test 2: Is the COLOUR RING CODE the same for the existing and uploading record?
                if ($pgrow['code'] == $data_pile['code']) {
                    // Test 3: Does the given COLOUR RING CODE exist in the database for the given population and species with a different METAL RING NUMBER?
                    if ($pgrow['sex'] == 'J' or $pgrow['sex'] == 'j') {
                        // chicks within and between broods often have the same COLOUR RING CODE
                        $return[] = true;
                    } else {
                        // existing combination -> reshights
                        $return[] = false;
                    }
                } else {
                    // new colour ring for an existing metal ring!
                    // list the records with the given METAL RING NUMBER and give a soft-error (re-coding may occur e.g. if a bird ringed as chick with 1 colour ring is recaptured as adult and receives two more colour rings). If the soft-error is OK'd then go to Test 3
                    $return[] = 2;
                }
            }
            if (array_search(false,$return) !== false)
                return false;
            elseif (array_search(2,$return) !== false)
                return implode(' ',$codes);
            else
                return true;
        // Test 1 is No: 
        } else {
            // Test 3: Does the given COLOUR RING CODE exist in the database for the given population and species with a different METAL RING NUMBER?
            $cmd = sprintf("SELECT 1 FROM plover_captures WHERE code=%s AND ring=%s AND population=%s AND species=%s",quote($data_pile['code']),quote($data_pile['ring']),quote($data_pile['population']),quote($data_pile['species']));
            $res = pg_query($ID,$cmd);
            if (!pg_num_rows($res)) {
                // new colour + metal combination
                return true;
            } else {
                // existing colour + new metal combination
                log_actin('VERY STRANGE colour code + ring check option!!!',__FILE__,__LINE__); 
            }

        }
        

        return false;
    }
}
