<?php
class photos {
    function print_js () {

        $js = (file_exists(getenv('OB_LIB_DIR').'modules/photos.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/photos.js') : '';
        return $js;
    }

    // attachment upload dialog
    function upload_box ($params) {
        /* upload file modal dialog:
         * - change color of buttons
         * - triggering upload event
         * */ 

        $div = "<div id='photo_upload_div' class='fix-modal' style='border:1px solid #666;border-bottom:4px solid #666'><h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:-10px -10px 10px -10px !important'>".t(str_upload)."</h2><button style='margin: -11px' class='pure-button button-passive fix-modal-close' id='pud-close'><i class='fa fa-close'></i></button><form method='post' enctype='multipart/form-data' class='pure-form'><input type='file' id='photo_upload-file' name='image_files[]' multiple='multiple' class='pure-button button-secondary'><br><br>
        <button id='photo_upload' style='display:none' class='button-xlarge pure-button'><i class='fa fa-upload'></i> ".t(str_upload)."</button>
        </form><div id='uresponse'></div>
        <button id='photo_anchor' style='margin-top:8px'  class='button-xlarge pure-button'><i class='fa fa-anchor'></i> ".t(str_set)."</button>
        </div>";
        return $div;
    }

    // photo/attachment viewer
    function photo_viewer($params,$def) {
        if ($def == 'upload') {
            $drop_btn = '<button class="pure-button button-error button-large p-delete" title="'.str_delete.'"><i class="fa fa-trash fa-lg"></i></button>';
            $download_btn = '';
        } else {
            $drop_btn = '<button class="pure-button button-error button-large p-delete" title="'.str_unlink_file_from_data.'"><i class="fa fa-unlink fa-lg"></i></button>';
            $download_btn = '<button class="pure-button button-success button-large p-download" title="'.str_download_full_size.'"><i class="fa fa-download fa-lg"></i></button>';
        }

        $div = '
<div id="photodiv">
    <div id="photodiv-carpet"></div>
    <div id="photodiv-buttons">
        '.$download_btn.'
        <button class="pure-button button-secondary button-large p-expand"><i class="fa fa-arrows-alt fa-lg"></i></button> 
        <button class="pure-button button-secondary button-large p-exif" >Info</button>
        <button class="pure-button button-gray button-large p-save" ><i class="fa fa-floppy-o fa-lg"></i></button>
        '.$drop_btn.'
        <button class="pure-button button-secondary button-large p-close"><i class="fa fa-times fa-lg"></i></button> 
    </div>
    <ul id="photodiv-exif"></ul>
    <div id="photodiv-frame">
        <textarea id="photodiv-comment"></textarea>
        <div style="position:absolute;right:0;top:0">
            <div id="photodiv-data"></div>
            <div id="photodiv-thumbnails"></div>
        </div>
    </div>
</div>';

        return $div;
    }
}
?>
