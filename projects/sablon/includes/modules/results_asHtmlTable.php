<?php
/* Simple html table result view
 * Limited rows
 * */
class results_asHtmlTable {
    function print_table($params,$data,$def) {
        global $ID;
        $table = new createTable();
        $table->def(['tid'=>'mytable','tclass'=>'resultstable']);
        $out = "";
        $chk = '';
        #$i = 0;
        //foreach($data->csv_rows as $line) {

        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%s_%s LIMIT 2000',PROJECTTABLE,session_id()));
        while($row = pg_fetch_assoc($res)) {
            $line = array();
            foreach($data->csv_header as $k=>$v) {
                if(isset($row[$k]))
                    $line[] = "<div class='viewport'>".$row[$k]."</div>";
                else
                    $line[] = "<div class='viewport'>&nbsp;</div>";
            }
            $table->addRows($line);
        }
        $table->addHeader($data->csv_header);
        /*
         * */
        if ($def['print']!='off') {
            $out .= $table->printOut();
            return $out;
        }
        else $_SESSION['trows'] = $table->tr;

    }
}
?>
