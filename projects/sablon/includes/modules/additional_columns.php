<?php
class additional_columns {
    function return_columns($p) {
        $c = array();
        $co = array();
        $params = preg_split('/;/',$p);
        if (count($params)) {
            foreach ($params as $pm) {
                $j = preg_split('/:/',$pm);
                if (isset($j[0]) and isset($j[1])) {
                    if(preg_match("/([a-z0-9._]+) as ([a-z0-9_]+)/i",$j[0],$m)) {
                        $c[$m[2]] = $j[1];
                        $co[] = "$m[1] as $m[2]";
                    } else {
                        $c[$j[0]] = $j[1];
                        $co[] = $j[0];
                    }
                } else {
                    $c[$j[0]] = $j[0];
                    $co[] = $j[0];
                }
            }
        }
        // column => visible name
        return(array($co,$c));
   
    }
}
?>
