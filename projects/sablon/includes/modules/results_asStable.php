<?php
class results_asStable {
    function print_roll_table($params,$data) {
        global $ID,$BID;
        
        $ACC_LEVEL = ACC_LEVEL;
        $modules = new modules();

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        $session_id = session_id();

        $out = "";
        $chk = '';
        // params = columns
        $a = explode(';',$params);
        foreach($a as &$sValue)
        {
             if ( $sValue=='obm_uploader_name' || $sValue=='obm_uploader_user' ) $sValue='uploader_name';
        }

        $aa = array();
        foreach ($data->csv_header as $k=>$v) {
            if (!in_array($k,$a)) {
                $aa[] = $k;
                unset($data->csv_header[$k]);
            }
        }
        //looking for geometry column, if exists, search for it's id
        //$geom_key = $_SESSION['st_col']['GEOM_C'];
        $i=0;

        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        if (!isset($_SESSION['lock_tables'])) $_SESSION['lock_tables'] = array();
        
        /*$lock_table = sprintf("temp_roll_stable_%s_%s",PROJECTTABLE,$session_id);

        if (in_array($lock_table,$_SESSION['lock_tables'])) {
            # send wait for the response message to user | or offer kill option?
            # Debugging misterious MILVUS's COPY ... bug
            debug('WAIT! '.$lock_table." is locked!!!");
        } else {
            //debug("Locking table: ".$lock_table);
            $_SESSION['lock_tables'][] = $lock_table;
        }*/

        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_stable_%s_%s",PROJECTTABLE,$session_id));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_roll_stable_%s_%s (rlist text, ord integer)",PROJECTTABLE,$session_id);
        pg_query($ID,$cmd);
        update_temp_table_index('roll_stable_'.PROJECTTABLE.'_'.$session_id);

        if (isset($_SESSION['orderby']))
            $orderby = sprintf('ORDER BY %1$s %2$s',$_SESSION['orderby'],$_SESSION['orderascd']);
        else
            $orderby = '';

        // ignore joins - should be optional!!
        if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
            $res = pg_query($ID,sprintf('SELECT * FROM (SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s) t %3$s',PROJECTTABLE,$session_id,$orderby));
        else
            $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s %3$s',PROJECTTABLE,$session_id,$orderby));

        //
        $rows_count = pg_num_rows($res);

        // create the temp_roll_stable_... table with empty rows.
        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_stable_%s_%s FROM STDIN",PROJECTTABLE,$session_id));
        $order = 0;
        for ($i=0; $i<$rows_count; $i++ ) {
            pg_put_line($ID, NULL."\t$order\n");
            $order++;
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

        // visible columns restriction
        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $main_cols[] = 'obm_id';
        $main_cols[] = 'obm_uploading_id';
        $allowed_cols = $main_cols;
        $allowed_general_columns = array();

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
            $allowed_gcols = $modules->_include('allowed_columns','return_gcolumns',array($main_cols),true);
            $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array($main_cols),true);
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                $allowed_cols = array();
            }
        }

        $GRST = grst(PROJECTTABLE,4);
        $general_restriction = 0;
        if (!$GRST and count($allowed_general_columns)) {
            $general_restriction = 1;
        }

        // fill_stable_with_columns();
        // Fill stable with column content as background process as it slow to wait and can hung up the site
        #foreach($_SESSION as $key=>$val){
        #    debug($key);
        #    debug($val);
        #}
        $fsession = array();
        $fsession['orderby'] = $_SESSION['orderby']; 
        $fsession['orderad'] = $_SESSION['orderad'];
        $fsession['orderascd'] = $_SESSION['orderascd'];
        $fsession['ignore_joined_tables'] = $_SESSION['ignore_joined_tables'];
        $fsession['current_query_table'] = $_SESSION['current_query_table'];
        $fsession['Tid'] = $_SESSION['Tid'];
        $fsession['Tgroups'] = $_SESSION['Tgroups'];
        $fsession['st_col'] = $_SESSION['st_col'];
        $fsession['Trole_id'] = $_SESSION['Trole_id'];
        $fsession['modules'] = $_SESSION['modules'];

        $vars = array();
        $vars['stable_header'] = $data->csv_header;
        $vars['protocol'] = $protocol;
        $vars['session'] = $fsession;
        $vars['tgroups'] = $tgroups;
        $vars['allowed_cols'] = $allowed_cols;
        $vars['allowed_general_columns'] = $allowed_general_columns;
        $vars['general_restriction'] = $general_restriction;
        $vars['GRST'] = $GRST;
        $vars['SERVER'] = $_SERVER;

        $svars = base64_encode(json_encode($vars));
        putenv("allvars_$session_id=$svars");
        
        fill_stable_with_columns($vars,$session_id,80);

        if (!file_exists('fill_stable_with_column.php'))
            debug('fill_stable_with_column.php not exists in '.dirname(__FILE__));

        exec("nohup /usr/bin/php ./fill_stable_with_column.php $session_id >/dev/null 2>/dev/null &");



/*            
        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_stable_%s_%s FROM STDIN",PROJECTTABLE,$session_id));
        $order = 0;
        while ($row = pg_fetch_assoc($res)) {

            #$h = array();
            #$td[$i] = array();
            // skipping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($row)==1) {
                if ($row[0]==$chk) continue;
                else $chk = $row[0];
            }

            # Nem lehet rst query-t csinálni rolltáblában, mert kifagy!!!
            #$acc = 1;
            #if (!rst('acc',$row['obm_id'],$_SESSION['current_query_table'])) {
            #    $acc = 0;
            #}
            
            $geometry_restriction = 0;
            $column_restriction = 0;
            if (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='1' or $row['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $geometry_restriction = 1;
            }
            elseif (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='2' or $row['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $column_restriction = 1;
            }


            $line = array();
            //$drop_line = 0;
            foreach ($data->csv_header as $k=>$v) {
                if (isset($row[$k])) {

                    if ($modules->is_enabled('transform_data')) {
                        //default itegrated module
                        $line[$k] = "<div class='viewport'>".$modules->_include('transform_data','text_transform',array($row[$k],$k,''),true)."</div>";
                    } else {
                        $line[$k] = "<div class='viewport'>".$row[$k]."</div>";
                    }

                    if ($column_restriction) {
                        if ( !in_array($k,$allowed_cols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }
                    elseif ($geometry_restriction ) {
                        if ( $k == 'obm_geometry')
                            $line[$k] = '&nbsp;';
                        if ( !in_array($k,$allowed_gcols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }

                    //non sensitivity based restrictions 
                    if ($general_restriction) {
                        if ( !in_array($k,$allowed_general_columns) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }

                    if ( $k == 'obm_files_id') {
                        if ($column_restriction) {
                            if ( !in_array($k,$allowed_cols) ) {
                                $line[$k] = '&nbsp;';
                            }
                        }
                        elseif ($geometry_restriction ) {
                            if ( !in_array($k,$allowed_gcols) ) {
                                $line[$k] = '&nbsp;';
                            }
                        } 
                        if ($general_restriction) {
                            if ( !in_array($k,$allowed_general_columns) ) {
                                $line[$k] = '&nbsp;';
                            }
                        }
                        else
                            $line[$k] = "<a href='getphoto?connection={$row[$k]}' class='photolink'><i class='fa fa-camera'></i></a>";
                    }
                    if ( $k == 'obm_uploader_user') {
                        $line[$k] = $row['obm_uploader_name'];
                    }
                    
                } else {
                    $line[$k] = "&nbsp;";
                }
            }
            
            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

            //prepend details buttun
            $id = $row['obm_id'];
            $uid = $row['upid'];
            // SHINYURL global control variable
            // create readable links instead of long get urls
            if (defined('SHINYURL') and constant("SHINYURL")) {
                $eurl = sprintf('%://'.URL.'/%s/data/%s/',$protocol,$_SESSION['current_query_table'],$id);
                $hurl = sprintf('%://'.URL.'/%s/uplinf/%s/',$protocol,$_SESSION['current_query_table'],$uid);
            } else {
                $eurl = sprintf('%://'.URL.'/index.php?data&id=%s&table=%s',$protocol,$id,$_SESSION['current_query_table']);
                $hurl = sprintf('%://'.URL.'/index.php?history=upload&id=%s&table=%s',$protocol,$uid,$_SESSION['current_query_table']);
            }

            $b_id = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary'><i class='fa fa-info'></i> %s</a>",$eurl,str_details);
            $b_upid = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary'><i class='fa fa-info'></i> %s</a>",$hurl,str_upload);
            array_unshift($line,$b_upid);
            array_unshift($line,$b_id);

            //if (!$drop_line)
            pg_put_line($ID, json_encode($line,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT)."\t$order\n");
            $order++;
            //else
            //    pg_put_line($ID, json_encode(array('content disabled'),JSON_UNESCAPED_UNICODE)."\n");
            //$values_long[] = sprintf('%s',json_encode($l,JSON_HEX_QUOT));

            //sleep(1);
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

*/

        /*if (($key = array_search($lock_table, $_SESSION['lock_tables'])) !== false) {
            unset($_SESSION['lock_tables'][$key]);
            //debug("Unlock table: ".$lock_table);
        }*/

        //pg_close($ID);
        //temp_stable_list_::
        //$res1 = pg_copy_from($ID, sprintf('temporary_tables.temp_roll_stable_%s_%s',PROJECTTABLE,$session_id), $values_long);
        //if($res1===false) {
        //    log_action("FAILED to insert temporary_tables.temp_roll_stable: cmd",__FILE__,__LINE__);
        //}

        $data->csv_header = array_merge(array(str_details,str_upload),$data->csv_header);

        $k = count($data->csv_header)*121;
        //$columns = array_keys($data->csv_header);
        $viewports = '';
        foreach($data->csv_header as $key=>$value) {
            $viewports .= "<div id='stable-$key' class='viewport-header' data-type='stable'><span style='display:inline-block;width:110px'>$value</span><i class='fa fa-sort' style='float:right'></i></div>";
        }

        $s = "$viewports<div class='viewport-block'></div>";
        return array('w'=>$k,'i'=>$s);
    }
}
?>
