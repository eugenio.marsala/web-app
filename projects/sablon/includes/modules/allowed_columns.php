<?php
/*
 *
 * */
class allowed_columns {
    /* returns with a subset of given list of columns
     *
     * */
    function return_columns ($p,$main_cols,$for_data='for_sensitive_data') {
        $c = array();
        $params = preg_split('/;/',$p);

        if (count($params)) {
            foreach ($params as $pm) {
                $poptions = preg_split('/:/',$pm);
                if ($poptions[0] == $for_data) {
                    $c = preg_split('/,/',$poptions[1]);
                }

                // backward compatibility ??,
                //if ($pm == '-')
                //    return(array());
                //$c[] = $pm;
            }
        } else {
            //default options: Consortium statement
            if ($_SESSION['st_col']['SPECIES_C']!='') $c[] = sprintf('%s',$_SESSION['st_col']['SPECIES_C']);
            if ($_SESSION['st_col']['NUM_IND_C']!='') $c[] = sprintf('%s',$_SESSION['st_col']['NUM_IND_C']);
            foreach($_SESSION['st_col']['DATE_C'] as $i) if ($i!='') $c[] = sprintf('%s',$i);
            foreach($_SESSION['st_col']['CITE_C'] as $i) if ($i!='') $c[] = sprintf('%s',$i);
        }

        $final_list = array();
        foreach($c as $ce) {
            if ( in_array($ce,$main_cols) )
                $final_list[] = $ce;
            
        }

        return($final_list);

    }
    /* list of allowed column for geometry restricted data 
     * these are the list of previous list PLUS * marked columns
     * */
    function return_gcolumns ($p,$main_cols) {
        return $this->return_columns($p,$main_cols,'for_no-geom_data');
    }
    /* returns with a subset of given list of columns
     * It used for a general restriction in group access level if no rules table
     *
     * */
    function return_general_columns($p,$main_cols) {
        $final_list = array();
        $ACC_LEVEL = ACC_LEVEL;
        if ( "$ACC_LEVEL"=='2' or "$ACC_LEVEL" == 'group' ) {
            $c = array();
            $params = preg_split('/;/',$p);

            if (count($params)) {
                foreach ($params as $pm) {
                    $poptions = preg_split('/:/',$pm);
                    if ($poptions[0] == 'for_general') {
                        $c = preg_split('/,/',$poptions[1]);
                    }

                }
                foreach($c as $ce) {
                    if ( in_array($ce,$main_cols) )
                        $final_list[] = $ce;    
                }
            }
        }
        return($final_list);
    }
}
?>
