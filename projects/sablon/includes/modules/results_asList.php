<?php
class results_asList {
    function print_roll_list($params,$data) {
        global $ID,$BID;

        //kiürítjük a slidokat minden új beolvasásnál
        $_SESSION['slides'] = array();
        
        $ID_C = $_SESSION['st_col']['ID_C'];

        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_list_%s_%s",PROJECTTABLE,session_id()));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_roll_list_%s_%s (rlist text, ord integer)",PROJECTTABLE,session_id());
        pg_query($ID,$cmd);
        update_temp_table_index('roll_list_'.PROJECTTABLE.'_'.session_id());
    
        // slide title as parameter: SLIDE_TITLE:column_name
        $pmi = array();
        $control = '';

        $p = preg_split('/;/',$params);
        foreach($p as $pm) {
            if (preg_match('/^SLIDE_TITLE:(.+)/',$pm,$m)) {
                $control = $m[1];
            }
        }

        if ($control != '')
            $c = ",$control";
        else
            $c = '';
        $c='';

        $res = pg_query($ID,sprintf('SELECT DISTINCT obm_id obm_id%s FROM temporary_tables.temp_query_%s_%s ORDER BY obm_id',$c,PROJECTTABLE,session_id()));

        $k = pg_num_rows($res)*121;
        $n = 0;
        $cmd = '';

        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_list_%s_%s FROM STDIN",PROJECTTABLE,session_id()));
        $order = 0;
        while($row = pg_fetch_assoc($res)) {

            $slide_title = '';
            $slide_id = $row['obm_id'];
            
            if ($control!='' and isset($row[$control])) {
                // substring
                $slide_title = mb_strimwidth($row[$control], 0, 20, "...");
            }
            if ($ID_C != 'obm_id' and isset($row[$ID_C])) {
                $slide_id = $row[$ID_C];
            }

            pg_put_line($ID, $data->list_ref_ass($slide_id,$slide_title)."\t$order\n");
            $order++;

        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

        return array('w'=>400,'i'=>'');
   
    }
}

class slide {
    function print_slide($params,$data,$COLUMNS=NULL,$def=NULL) {
        global $ID;

        $st_col = st_col($_SESSION['current_query_table'],'array');

        $SPECIES_C = $st_col['SPECIES_C'];
        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
            $SPECIES_C = $m[2];

        $table = PROJECTTABLE;
        
        preg_match('/(slide[b]?)(.+)/',$def['slide'],$m);
        $hb = "body";
        if($m[1]=='slide') $hb = "header";

        /*$limit = '';
        if(preg_match('/(\d+):?(\d+)?/',$m[2],$mm)){
            $m[2] = $mm[1];
            if (isset($mm[2]))
                $limit = sprintf('LIMIT 1 OFFSET %1d',$mm[2]);
        } else {
            //first row if no :n JOIN attribute
            //$limit = 'LIMIT 1 OFFSET 0';
        }*/

        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%s_%s WHERE obm_id=%s',PROJECTTABLE,session_id(),quote($m[2])));
        $rows = pg_fetch_all($res);
        
        $row = $rows[0];
        $r_id = $row['obm_id'];                   // data row's id
        // default slide title: species name
        // here we can apply a module again but not really necessary!
        if (isset($row[$SPECIES_C]) and $SPECIES_C!='')
            $r_spec = $row[$SPECIES_C];             // data row's species name
        else
            $r_spec = '';
        $r_valid=''; 
        $u_id = '';
        $u_date = '';
        $upl_id = '';
        $upl_name = '';
        if (isset($row['upid']))            $u_id = $row['upid'];               // data row's uploading id
        if (isset($row['uploading_date']))  $u_date = $row['uploading_date'];   // data row's uploading date
        if (isset($row['uploader_id']))     $upl_id = $row['uploader_id'];      // uploader's id
        if (isset($row['uploader_name']))   $upl_name = $row['uploader_name'];  // uploaders's name
        if (isset($row['data_eval']))       $r_valid = $row['data_eval'];       // data row's votes

        if($hb == "header") 
            return $this->header_assemble($table,$r_id,$r_valid,$upl_id,$upl_name,$u_id,$u_date);
        else
            return $this->body_assemble($params,$COLUMNS,$rows,$r_id);
   
    }
    /* slide header
     * set: SESSION['slide']
     * */
    function header_assemble($qtable,$r_id,$r_valid,$upl_id,$upl_name,$u_id,$u_date) {
        global $ID;
        $t = $r_id;
        $m = array();
        
        $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",$qtable)); 

        if(!isset($_SESSION["slide"]["slide$t"])) { 
            $dh = sprintf('<button class="pure-button button-gray close" style="position:absolute;top:4px;right:4px"><i class="fa fa-close"></i></button>
                <div style="position:absolute;top:0;left:0;width:100&#37;;height:37px;background-color:#afafaf;border-bottom:1px solid #999;z-index:-1"></div>
                <div>
                <table style="border-spacing:1px !important;border-collapse:unset;height:200px">
                <thead><tr>
                    <th style="background-color:#606060;height:34px" id="opb1_%1$s" class="rs_hb rs_hbs">%2$s</th>
                    <th style="background-color:#686868;height:34px" id="opb2_%1$s" class="rs_hb">%3$s</th>
                    <th style="background-color:#707070;height:34px" id="opb3_%1$s" class="rs_hb">%4$s</th>
                    <th style="background-color:#787878;height:34px" id="opb4_%1$s" class="rs_hb">%5$s</th>
                </tr></thead>',$r_id,t(str_upload),t(str_changes),t(str_metrics),t(str_comments));

            $dh .= sprintf('<tbody><tr><td colspan="7" id="optd_%1$s" style="padding:10px 0 10px 10px;vertical-align:top">',$r_id);
            

            // adatfeltöltés adatai
            $dh .= sprintf("<div id='opbd1_%s' class='rs_hbd' style='display:block'><b>%s: </b>",$r_id,t(str_uploader));
            if ($ulink=fetch_user($upl_id,'roleid','link')) 
                $dh .= $ulink; 
            else 
                $dh .= $upl_name; 
            $dh .= sprintf('<br><b>%1$s: </b><a href=\'?history=upload&id=%2$d\' target=\'_blank\'>%2$d</a>',t(str_dataupid),$u_id);
            $dh .= sprintf("<br><b>%s: </b>%s",t(str_date),$u_date);
            $dh .= sprintf("</div>");

            // data changes
            $last_mod = data_changes($qtable_history,PROJECTTABLE,$r_id);
            $dh .= sprintf("<div style='' id='opbd2_%s' class='rs_hbd'><b>%s: </b>%s %s<br>",$r_id,t(str_last_modify),$last_mod['time'],$last_mod['link']);
            $dh .= sprintf("<b>%s: </b><a href='?history=data&id=%s' target='_blank'>%s</a></div>",t(str_modcount),$r_id,$last_mod['count']);
            
            //metrics - views
            $views = metrics($r_id,'data_views');
            
            //metrics - citations
            $citations = cite_metrics($r_id,'citations');
            
            //metrics - downloads
            $downloads = metrics($r_id,'downloads');

            if ($r_valid=='') $evaluation = '';
            else $evaluation = sprintf("%.2f",$r_valid*100)."%";
            // metrics - validitás + evaluation
            $dh .= sprintf("<div style='' id='opbd3_%s' class='rs_hbd'>
                <b><abbr title='Known citations'>%s</abbr>: </b>%s<br>
                <b><abbr title='Estimated value'>%s</abbr>: </b>%s<br>
                <b>%s: </b>%s<br>
                <b><abbr title='mean value of project+data+uploading+user evaluates'>%s</abbr>: </b>%s<br>
                <b>%s: </b><a href='?history=validation&id=%s&blt=%s' target='_blank'>%s</a></div>",$r_id,t(str_citations),$citations,t(str_views),$views,t(str_downloads),$downloads,t(str_validation),validation($t),t(str_valuation),$r_id,PROJECTTABLE,$evaluation);

            // obm_comments
            $dh .= sprintf("<div style='' id='opbd4_%s' class='rs_hbd'>%s</div>",$r_id,comments($r_id,PROJECTTABLE,'data'));


            $dh .= sprintf('</tr></tbody></table>');
            //
            $dh .= sprintf('<div style="padding:10px 0 5px 7px"><a href="?data&id=%1$s" target="_blank" class="button-secondary button-href pure-button"><i class="fa fa-external-link"></i> %2$s</a> 
                <button class="button-href pure-button togglerbutton" data-id="toggler-slideb%1$s"><i class="fa fa-newspaper-o"></i> %3$s</button></div>',$r_id,str_datasheet,str_details);
            $dh .= "</div>";

            return $dh;
        }    

    }
    /* slide body
     * set: SESSION['slide']
     *
     * */
    function body_assemble($params,$COLUMNS,$rows,$r_id) {
        global $ID,$BID; 

        $p = preg_split('/;/',$params);

        foreach( $p as $pm ) {
            $pm = preg_split('/:/',$pm);
            
            if (isset($COLUMNS[$pm[0]]) and isset($pm[1])) {
                $COLUMNS[$pm[1]] = $COLUMNS[$pm[0]];
                unset($COLUMNS[$pm[0]]);
            }
        }

        $ACC_LEVEL = ACC_LEVEL;
        $acc = 1;
        if (!rst('acc',$r_id,$_SESSION['current_query_table'])) 
            $acc = 0;

        $modules = new modules(); 

        // e = postgres result rows
        // COLUMNS = associative array of data fields (keys=value, value=label)
        // t = body slide ID
        $t = 'b'.$r_id;
        
        if(!isset($_SESSION["slide"]["slide$t"])) { 
            //track data view
            track_data($r_id);

            // dh = an <ul> block of details
            $dh = sprintf("<button class='pure-button button-gray close' style='position:absolute;top:4px;right:4px'><i class='fa fa-close'></i></button><span style='font-size:150&#37;;margin-left:5px'>ID: %s</span><div style='position:absolute;top:0;left:0;width:100&#37;;height:37px;background-color:#afafaf;border-bottom:1px solid #999;z-index:-1'></div><br><br><ul class='slide-body'>",$r_id);
        
            // ez kötelezően csak ez lehet!
            $FILE_ID = 'obm_files_id';

            $BOLD_YELLOW = ($modules->is_enabled('bold_yellow')) 
                ? $modules->_include('bold_yellow','mark_text',[],true)
                : array();

            //default - all columns accessible
            $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
            $main_cols = array_merge($main_cols,dbcolist('columns','dinpi_meta'));
            $main_cols[] = 'obm_id';
            $main_cols[] = 'obm_uploading_id';
            $allowed_cols = $main_cols;
            $allowed_general_columns = array();

            //a subset of the main cols, depending on user's access rights
            if ($modules->is_enabled('allowed_columns')) {
                $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
                $allowed_gcols = $modules->_include('allowed_columns','return_gcolumns',array($main_cols),true);
                $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array($main_cols),true);
            } else {
                if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                    // drop all column for non logined users if access level is higher
                    $allowed_cols = array();
                }
            }

            
            $general_restriction = 0;
            if (!grst(PROJECTTABLE,4) and count($allowed_general_columns)) {
                $general_restriction = 1;
            }
        
            $non_repetative = array();

            if (!isset($_SESSION['Tid']))
                $tid = 0;
            else
                $tid = $_SESSION['Tid'];
            if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
                $tgroups = 0;
            else
                $tgroups = $_SESSION['Tgroups'];

            //log_action($COLUMNS);
            /* loop of data rows 
             * it is more than 1 for JOINED data
             * */
            foreach($rows as $e) {
                //log_action($e);
                
                $geometry_restriction = 0;
                $column_restriction = 0;
                if (!grst(PROJECTTABLE,4) and isset($e['obm_sensitivity']) and ($e['obm_sensitivity']=='1' or $line['obm_sensitivity']=='no-geom')) {
                    if (!count(array_intersect(explode(',',$tgroups),explode(',',$e['obm_read']))))
                        $geometry_restriction = 1;
                }
                elseif (!grst(PROJECTTABLE,4) and isset($e['obm_sensitivity']) and ($e['obm_sensitivity']=='2' or $e['obm_sensitivity']=='restricted')) {
                    if (!count(array_intersect(explode(',',$tgroups),explode(',',$e['obm_read']))))
                        $column_restriction = 1;
                } elseif (!$acc) {
                    $column_restriction = 1;
                }

                foreach($COLUMNS as $cn=>$label) {
                    if (preg_match('/(\w)+\.(.+)$/',$cn,$m)) {
                        $cn = $m[2];
                        if (preg_match('/(\w)+\.(.+)$/',$label,$m)) {
                            $label = $m[2];
                        }
                    }

                    if ($column_restriction) {
                        if ( !in_array($cn,$allowed_cols) ) {
                            continue;
                        }
                    }
                    elseif ($geometry_restriction ) {
                        if ( $cn == 'obm_geometry')
                            continue;
                        if ( !in_array($cn,$allowed_gcols) ) {
                            continue;
                        }
                    }
                    //non sensitivity based restrictions 
                    if ($general_restriction) {
                        if ( !in_array($cn,$allowed_general_columns) ) {
                            continue;
                        }
                    }

                    $jts = ""; //gray style
                    if (!in_array($cn,$main_cols)) {
                        $jts = "graybg";
                    }

                    /*
                     * */
                    if (in_array($cn,$BOLD_YELLOW) and isset($e[$cn]) and !in_array($cn,$non_repetative)) { 
                        if ($jts=='')
                            $non_repetative[] = $cn;
                        
                        // bold yellow attributes: Dátum, Hivatkozás, Mennyiség, Tudományos név
                        $dh .= sprintf("<li class='lmh $jts'><span class='obm_label'>%s:</span> %s</li>",t($label),$e[$cn]);
                    }
                    elseif ($cn==$FILE_ID and $FILE_ID!='' and isset($e[$cn]) and !in_array($cn,$non_repetative)) {

                        if ($jts=='')
                            $non_repetative[] = $cn;
                        
                        // csatolmány megjelenítése!!
                        $cmd = sprintf("SELECT id,reference,substring(comment,1,15) as comment FROM system.files LEFT JOIN system.file_connect ON (files.id=file_connect.file_id) WHERE conid='%s'",$e[$cn]);
                        $res = pg_query($ID,$cmd);
                        $p = array();
                        while ($row = pg_fetch_assoc($res)) {
                            if ($thumb = mkThumb($row['reference'],60)) {
                                $thf = "http://".URL."/getphoto?ref=/thumbnails/{$row['reference']}";
                                $p[] = "<a href='http://".URL."/getphoto?c={$e[$cn]}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$thf' title='{$row['comment']}' class='thumb'></a>";
                            } else {
                                $mime_url = mime_icon($row['reference'],32);
                                $p[] = "<a href='http://".URL."/getphoto?c={$e[$cn]}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$mime_url' title='{$row['comment']}' class='thumb'></a>";
                            }
                        }
                        $e[$cn] = implode(" ",$p);
                        $dh .= sprintf("<li class='lm $jts'><span class='obm_label'>%s:</span> <span style='display:table-cell;vertical-align:bottom;padding-top:15px'>%s</span></li>",t($label),$e[$cn]);
                    }
                    elseif (isset($e[$cn]) and !in_array($cn,$non_repetative)) {

                        if ($jts=='')
                            $non_repetative[] = $cn;

                        // normal attributes
                        // link handling
                        // ezeket module hívással kéne kezelni!
                        // transform module: oszlop név -> do transform
                        if ($modules->is_enabled('transform_data')) {
                            //default itegrated module
                            $e[$cn] = $modules->_include('transform_data','text_transform',array($e[$cn],$cn,''),true);
                        }

                        if (preg_match('/^http:\/\/.+/',$e[$cn])) {
                            $e[$cn] = "<a href='{$e[$cn]}' class='photolink' target='_blank'>{$e[$cn]}</a>";
                        }
                        if (preg_match('/^str_/',$e[$cn]))
                            if (defined($e[$cn]))
                                $e[$cn] = constant($e[$cn]);
                        $dh .= sprintf("<li class='lm $jts'><span class='obm_label'>%s:</span> %s</li>",t($label),$e[$cn]);
                    }                }
            }

            $dh .= sprintf("</ul>");
            // SHINYURL global control variable
            // create readable links instead of long get urls
            if (defined('SHINYURL') and constant("SHINYURL"))
                $eurl = sprintf('http://'.URL.'/data/%s/',$r_id);
            else
                $eurl = sprintf('http://'.URL.'/index.php?data&id=%s',$r_id);

            $dh .= sprintf('<a href="%1$s" target="_blank" class="button-secondary button-href pure-button" style="margin:15px"><i class="fa fa-external-link"></i> %3$s</a> <button class="button-href pure-button togglerbutton" style="margin:15px" data-id="toggler-slide%2$s"><i class="fa fa-newspaper-o"></i> %4$s</button>',$eurl,$r_id,str_datasheet,str_header_information);
            return $dh;
        }
    }
}

?>
