<?php
 /**
  * Custom admin pages module
  *
  * This module makes possible for other modules to create an entry in the 
  * Custom modules submenu of the Project admin menu. An example with custom 
  * admin page can be found in the 
  *
  * @example examples/custom_module_with_admin_page.php
  * @version 1.0
  * @author Bóné Gábor <gabor.bone@milvus.ro>
  */
class custom_admin_pages {
    var $error = '';
    var $params = [];
    var $retval = '';

    function __construct($action, $params) {
        $this->params = preg_split('/;/',$params);
        $this->modules = [];

        $this->includeModules();

        $this->$action();
    }

    public function availableModules() {
        return $this->modules;
    }

    public function menuItems() {
        $out = "<li><a href='#' class='sno to'>".str_custom_modules."<i class='fa fa-caret-right' style='float: right;'></i></a> <ul class='subsubnav'>";
        foreach ($this->modules as $p) {

            $m = call_user_func( $p . '::getMenuItem');
            $out .= "<li><a href='#tab_basic' data-url='includes/project_admin.php?options={$m['url']}'>{$m['label']}</a></li>";
        }
        $out .= "</ul> </li>";
                                
        $this->retval = $out;
        return;
    }

    public function profileSections() {
        $out = '';
        foreach ($this->modules as $p) {
            $m = call_user_func( $p.'::getProfileSection');

            $out .= "<label class='profile_table_label'><i class='fa {$m['fa']} fa-lg'></i> {$m['label']}</label>";
            $out .= "<table class='profile_table'>";
            $out .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='{$m['href']}' data-url='{$m['data_url']}' class='profilelink'>{$m['label']}</a>");
            $out .= sprintf("<tr><td>%s</td></tr>",$m['help']);
            $out .= sprintf("</table><br><br>");
        }
        $this->retval = $out;
        return;
    }

    public function adminPage() {

        $module = new $this->params[0];
        $out = $module->adminPage();
        $this->retval = $out;
        return;
    }


    private function includeModules() {
        foreach ($this->params as $p) {
            $default_module = getenv('PROJECT_DIR').'includes/modules/' . $p . '.php';
            $private_module = getenv('PROJECT_DIR').'includes/modules/private/' . $p . '.php';

            if (file_exists($private_module)) {
                if( php_check_syntax($private_module,'e')) {
                    include_once($private_module);
                } else {
                    $this->error = str_module_include_error.': '.$e;
                    return;
                }
            } 
            elseif (file_exists($default_module)) {
                include_once($default_module);
            }
            else {
                $this->error = "$p module not found";
                return;
            }


            if (!class_exists($p)) {
                $this->error = "$p class does not exists!";
                continue;
            }

            if (!method_exists($p,'adminPage')) {
                $this->error = "$p::adminPage() method not defined";
                continue;
            }

            if (!method_exists($p,'getMenuItem')) {
                $this->error = "$p::getMenuItem() method not defined";
                continue;
            }


            $this->modules[] = $p;
        }
        $this->retval = $this->modules;
    }

    public function displayError() {
        log_action("ERROR: ".$this->error,__FILE__,__LINE__);
    }

    public function print_js() {
        $js = "
$(document).ready(function() {
    $(document).click(function(e) {
        $('.topnav').find('ul.subnav').find('.subsubnav').hide();
    });

    $('.topnav').find('ul.subnav').find('.sno').on('mouseover',function(ev) {
        $('.topnav').find('ul.subnav').find('.subsubnav').show();
    });
});
    ";
        $this->retval = $js;
    }
}

