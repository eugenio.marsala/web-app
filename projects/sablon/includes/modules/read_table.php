<?php
class read_table {
    private $hash;

    function __construct() {
        if (defined('MyHASH'))
            $this->hash = MyHASH;
        else
            $this->hash = PROJECTTABLE.gisdb_user.gisdb_pass;
    }

    function check_table($params,$data) {
        
        $p = explode(';',$params);
        foreach ($p as $a) {
            $as = preg_split('/:/',$a);
            if ($as[0] == $data) {
                return true;
            }
        }
        return false;
    }

    function list_links($params) {
        $links = array();
        $a = explode(';',$params);

        foreach ($a as $table) {
            $links[] = sprintf("%s",encode($table,$this->hash));
        }

        return $links;
    }

    function decode_link($params,$data) {
        
        $st = decode($data,$this->hash);
        $link = array('','','');
        $sta = preg_split('/\./',$st);
        for ($i=0;$i<count($sta);$i++) {
            $link[$i] = $sta[$i];
            if ($i == 1 and preg_match("/:/",$sta[1])) {
                $tsta = preg_split("/:/",$sta[1]);
                $link[1] = $tsta[0];
                $link[2] = $tsta[1];
            }
        }
        return $link;
    }
    function adminPage() {

        if (!grst(PROJECTTABLE,'master')) return;

        global $ID,$BID;


        $modules = new modules();
        $l = $modules->_include('read_table','list_links');

        $em = "<h2>".t(str_read_table)."</h2><div class='shortdesc'>sharable links</div>";

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $links = array();

        //$l = $this->list_links;
        foreach ($l as $link) {
            $links[] = "<li><a href='$protocol://".URL."/view-table/$link/' target='_blank'>$protocol://".URL."/view-table/$link/</a></li>";
        }
        $em .= "<ul>".implode('',$links)."</ul>";

        return $em;
    }

    function getMenuItem() {
        return ['label' => 'shareable table links', 'url' => 'read_table' ];
    }

}

?>
