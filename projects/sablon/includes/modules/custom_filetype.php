<?php
class custom_filetype {

    public function option_list($params) {
        global $ID;

        $cmd = sprintf("SELECT DISTINCT file_type, variant FROM %s", PROJECTTABLE . '_custom_filetype');

        $res = pg_query($ID,$cmd);

        while ($row = pg_fetch_assoc($res)) {
            $filetypes[] = $row;
        }

        $options = ['','CSV'];
        foreach ($filetypes as $f) {
            $options[] = (isset($f['variant'])) ? $f['file_type'] . '/' . $f['variant'] : $f['file_type'];
        }

        return $options;
    }

    public function custom_read($file, $filetype) {
        global $ID;
        
        
        $ft = explode('/',$filetype);
        $variant = (isset($ft[1])) ? sprintf(' AND variant = %s',quote($ft[1])) : '';
        $form_id = $_SESSION['current_page']['form'];




        //build the transformation array from the database

        $cmd = sprintf("SELECT from_column, action, to_column, from_value, to_value FROM %s_custom_filetype WHERE file_type = %s AND form IN (0,%d) %s ORDER BY action_order;",PROJECTTABLE,quote($ft[0]),$form_id,$variant);
        $res = pg_query($ID,$cmd);

        while ($row = pg_fetch_assoc($res)) {
            $t[] = $row;
        }

        $j = 0;
        for ($i = 0, $l = count($t); $i < $l; $i++) {
            //            log_action($t[$i]['from_column'] . ' ==? ' .$transform[$j - 1]['from_column'],__FILE__,__LINE__);
            if ($j > 0 && ($transform[$j - 1]['from_column'] == $t[$i]['from_column'] && $transform[$j - 1]['action'] == $t[$i]['action'] && (isset($transform[$j - 1]['to_column']) && $transform[$j - 1]['to_column'] == $t[$i]['to_column']))) {
                $transform[$j - 1]['pairs'][$t[$i]['from_value']] = html_entity_decode($t[$i]['to_value']);
            }
            else {
                $transform[$j]['from_column'] = $t[$i]['from_column'];
                $transform[$j]['action'] = $t[$i]['action'];
                $transform[$j]['to_column'] = $t[$i]['to_column'];
                $transform[$j]['pairs'][$t[$i]['from_value']] = $t[$i]['to_value'];
                $j++;
            }
        }

        $deleteRows = [];
        $complist = FALSE;
        $list_id = 0;

        for ($i = 0, $l = count($file); $i < $l; $i++) {

            for ($j = 0, $ll = count($transform); $j < $ll; $j++) {
                $column = $transform[$j]['from_column'];
                $action = $transform[$j]['action'];
                switch ($action) {

                case 'user name':
                    $file[$i]['user_name'] = $_SESSION['Tname'];
                    break;

                case 'rename':
                    $newcol = $transform[$j]['to_column'];
                    $file[$i][$newcol] = $file[$i][$column];
                    unset($file[$i][$column]);
                    break;

                case 'filter':
                    $drop_rows = $transform[$j]['pairs'];
                    if (isset($drop_rows[$file[$i][$column]]))
                        $deleteRows[] = $i;
                    break;

                case 'drop': 
                    unset($file[$i][$column]);
                    break;

                case 'replace':
                    $toColumn = $transform[$j]['to_column'];

                    if (isset($file[$i][$column])) {
                        if (isset($file[$i][$toColumn])) {
                            if ($file[$i][$column] != '') {
                                $file[$i][$toColumn] =  $file[$i][$column];
                            }
                        }
                        else {
                            $file[$i][$toColumn] =  $file[$i][$column];
                        }
                    }
                    break;

                case 'append':
                    $toColumn = $transform[$j]['to_column'];
                    $toValue = (isset($file[$i][$toColumn])) ? $file[$i][$toColumn] : '';

                    $condition = $transform[$j]['pairs'];
                    $appendValue = '('.$column . ': ' . trim($file[$i][$column],'"') . ')';

                    if (isset($condition['_all'])) {
                        $toValue = ($file[$i][$column] != '') ? $toValue . $appendValue  : $toValue;
                    }
                    else {
                        $toValue = (isset($condition[$file[$i][$column]])) ? $toValue . $appendValue  : $toValue;
                    }

                    $file[$i][$toColumn] = $toValue;
                    break;

                case 'coord':
                    $file[$i][$column] = str_replace(',','.',$file[$i][$column]);
                    break;

                case 'new column':
                    $newcol = $transform[$j]['to_column'];
                    $pairs = $transform[$j]['pairs'];
                    
                    $from_value = trim($file[$i][$column],'"');

                    $else = (isset($pairs['_else'])) 
                        ?  $pairs['_else'] 
                        : ((isset($file[$i][$newcol])) ?  $file[$i][$newcol] : '');

                    if (isset($pairs[$from_value])) {
                        $to_values = explode(',',$pairs[$from_value]);

                        $file[$i][$newcol] = $to_values[0];

                        if (isset($to_values[1])) {
                            for ($k = 1, $m = count($to_values); $k < $m; $k++) {
                                $cond = preg_split("/[:=]+/",$to_values[$k]);
                                if (count($cond) != 3) {
                                    log_action("conditions not formated properly at column: $column, value: $from_value!",__FILE__,__LINE__);
                                    break;
                                }
                                if ($file[$i][$cond[0]] == $cond[1]) {
                                    $file[$i][$newcol] = $cond[2];
                                    break;
                                }
                            }
                        //    if (!isset($file[$i][$newcol]))
                        //        $file[$i][$newcol] = $to_values[0];
                        }
                        //else 
                        //    $file[$i][$newcol] = $to_values[0];
                    }
                    else 
                       $file[$i][$newcol] = $else;

                       //log_action($file[$i][$newcol]);
                    break;

                case 'complete list':
                    $remarks = preg_split("/[\s,.]+/",trim($file[$i][$column],'"'));
                    $pairs = $transform[$j]['pairs'];

                    if (isset($pairs['_single'])) {

                        $only_duration = TRUE;

                    }

                    if (isset($pairs['_multiple']) || isset($pairs['_alternative']) || isset($pairs['_alternative_noduration'])) {

                        $dur = TRUE;
                        if (isset($pairs['_alternative_noduration']))
                            $dur = FALSE;

                        $list[] = $i;

                        if (!$complist)
                            $this->complist_drop($file,$list,$dur);

                        if (in_array('stop',array_map('strtolower',$remarks))) {
                            // Ha hiányzik a start, akkor törölje a megkezdett teljes listát
                            if ($complist) {
                                $this->complist_drop($file,$list,$dur);
                            }
                            $complist = TRUE;
                            $list[] = $i;
                        }


                        if (in_array('start',array_map('strtolower',$remarks)) && $complist) {
                            $complist = FALSE;
                            $this->complist_validate($file,$list,$list_id,$dur);
                        }

                        // Ha hiányzik a start az utolsó listából, akkor törölje a megkezdett teljes listát
                        if ($i == $l-1 && $complist)
                            $this->complist_drop($file,$list,$dur);
                    }
                    break;
                }
            }
        }

        if ($list_id == 0) 
            $this->only_duration($file,$dur);

        if (isset($only_duration) && $only_duration) 
            $this->only_duration($file);


        foreach ($deleteRows as $del) {
            unset($file[$del]);
        }

        unset($deleteRows);
        return array_values($file);
    }

    private function only_duration(&$file,$dur) {

        $starttime = new DateTime($file[count($file)-1]['exact_time']);
        $endtime = new DateTime($file[0]['exact_time']);
        $duration = $endtime->diff($starttime);

        for ($i = 0, $l = count($file); $i < $l; $i++) {
            $file[$i]['time_of_start'] = $starttime->format('H:i');
            $file[$i]['time_of_end'] = $endtime->format('H:i');
            if ($dur)
                $file[$i]['duration'] = $duration->format("%H:%I");
        }

    }
    private function complist_validate(&$file, &$list, &$list_id, $dur) {

        $list_id++;

        $starttime = new DateTime($file[end($list)]['exact_time']);
        $endtime = new DateTime($file[reset($list)]['exact_time']);
        $duration = $endtime->diff($starttime);

        foreach ($list as $row) {
            $file[$row]['time_of_start'] = $starttime->format('H:i');
            $file[$row]['time_of_end'] = $endtime->format('H:i');
            if ($dur)
                $file[$row]['duration'] = $duration->format("%H:%I");
            $file[$row]['complete_list'] = 1;
            $file[$row]['list_id'] = $list_id;
        }
        $list = [];
    }

    private function complist_drop(&$file, &$list, $dur) {
        foreach ($list as $row) {
            $file[$row]['time_of_start'] = '';
            $file[$row]['time_of_end'] = '';
            if ($dur) 
                $file[$row]['duration'] = '';
            $file[$row]['complete_list'] = 0;
            $file[$row]['list_id'] = '';
        }
        $list = [];
    }
}
?>
