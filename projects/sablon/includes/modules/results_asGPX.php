<?php
class results_asGPX {
    function print_data($params,$filename='',$data) {
        global $ID,$BID;
        //include_once(getenv('OB_LIB_DIR').'geoPHP/geoPHP.inc');

        $ACC_LEVEL = ACC_LEVEL;
        $modules = new modules();
        $sep = ',';

        $st_col = st_col($_SESSION['current_query_table'],'array');

        if (!isset($st_col['GEOM_C']) or $st_col['GEOM_C']=='') echo "No geometry column defined.";

        $GEOM_C = $st_col['GEOM_C'];

        if (defined('GPX_NAME_COLUMN'))
            $SPECIES_C =  GPX_NAME_COLUMN;
        else {
            if (!isset($st_col['SPECIES_C']) or $st_col['SPECIES_C']=='') {
                $SPECIES_C = "'SPECIES_C'";
            }
            else {
                if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
                    $SPECIES_C = $m[2];
                else
                    $SPECIES_C = $st_col['SPECIES_C'];
            }
        }
        
        if (defined('GPX_DESC_COLUMN'))
            $NUM_IND_C =  GPX_DESC_COLUMN;
        else {
            if (!isset($st_col['NUM_IND_C']) or  $st_col['NUM_IND_C']=='')
                $NUM_IND_C = "'NUM_IND_C'";
            else {
                $NUM_IND_C = $st_col['NUM_IND_C'];
                if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['NUM_IND_C'],$m))
                    $NUM_IND_C = $m[2];
            }
        }

        $filesize = 0;
        $row = sprintf('Latitude%1$sLongitude%1$sName%1$sComment%1$sDescription%2$s',$sep,"\n");
        $n = 0;
        $geom_i;

        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];

        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];


        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $main_cols[] = 'obm_id';
        $main_cols[] = 'obm_uploading_id';
        $allowed_cols = $main_cols;
        $allowed_general_columns = array();

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            $allowed_cols = $modules->_include('allowed_columns','return_columns',array($main_cols),true);
            $allowed_gcols = $modules->_include('allowed_columns','return_gcolumns',array($main_cols),true);
            $allowed_general_columns = $modules->_include('allowed_columns','return_general_columns',array($main_cols),true);
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                // drop all column for non logined users if access level is higher
                $allowed_cols = array();
            }
        }

        $track_id = array();
        $general_restriction = 0;
        if (!grst(PROJECTTABLE,4) and count($allowed_general_columns)) {
            $general_restriction = 1;
        }

        $n = 0;
        
        // if dates columns are missing from the table
        $datums = ($st_col['DATE_C'][0] === '') ? "'NA'" : implode(',',$st_col['DATE_C']);

        //DISTINCT obm_id due to possible query joins which are not good in this result
        if ($st_col['RESTRICT_C']) {
            $cmd = sprintf('SELECT DISTINCT obm_id,obm_sensitivity,obm_read,ST_X(ST_Centroid(obm_geometry)) AS x,ST_Y(ST_Centroid(obm_geometry)) AS y ,%s,%s,%s FROM temporary_tables.temp_query_%s_%s ORDER BY obm_id',$SPECIES_C,$NUM_IND_C,$datums,PROJECTTABLE,session_id());
            $res = pg_query($ID, $cmd);
        } else {
            $res = pg_query($ID,sprintf('SELECT DISTINCT obm_id,ST_X(ST_Centroid(obm_geometry)) AS x,ST_Y(ST_Centroid(obm_geometry)) AS y ,%s,%s,%s FROM temporary_tables.temp_query_%s_%s ORDER BY obm_id',$SPECIES_C,$NUM_IND_C,$datums,PROJECTTABLE,session_id()));
        }
        //log_action(sprintf('SELECT ST_X(ST_Centroid(obm_geometry)) AS x,ST_Y(ST_Centroid(obm_geometry)) AS y ,%s,%s,%s FROM temporary_tables.temp_query_%s_%s',$SPECIES_C,$NUM_IND_C,$datums,PROJECTTABLE,session_id()));
        while($line = pg_fetch_assoc($res)) {
            
            $cell = array();
            $column_restriction = 0;

            if (!grst(PROJECTTABLE,4) and isset($line['obm_sensitivity']) and ($line['obm_sensitivity']=='1' or $line['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$line['obm_read']))))
                    continue;
            }
            elseif (!grst(PROJECTTABLE,4) and isset($line['obm_sensitivity']) and ($line['obm_sensitivity']=='2' or $line['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$line['obm_read']))))
                    $column_restriction = 1;
            }
            
            # allowed_gcols handling not implemented yet :(
            if ($column_restriction) {
                if ( !in_array($SPECIES_C,$allowed_cols) ) {
                    $line[$SPECIES_C] = '';
                }
                if ( !in_array($NUM_IND_C,$allowed_cols) ) {
                    $line[$NUM_IND_C] = '';
                }
            }

            //non sensitivity based restrictions 
            if ($general_restriction) {
                if ( !in_array($SPECIES_C,$allowed_general_columns) ) {
                    $line[$SPECIES_C] = '';
                }
                if ( !in_array($NUM_IND_C,$allowed_general_columns) ) {
                    $line[$NUM_IND_C] = '';
                }
            }


            // ski0pping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($line)==1) {
                if ($line[0]==$chk) continue;
                else $chk = $line[0];
            }
            // print cells
            /*if ($polygon = geoPHP::load($line[$GEOM_C],'wkt')){
                $area = $polygon->getArea();
                $centroid = $polygon->getCentroid();
                $cell[1] = sprintf('%s',$centroid->getY());
                $cell[0] = sprintf('%s',$centroid->getX());
            }*/
            // coordinates
            $cell[0] = $line['y'];
            $cell[1] = $line['x'];
            
            //name
            if (!isset($line[$SPECIES_C]) or $line[$SPECIES_C] == '')
                $cell[2] = "NA";
            else
                $cell[2] = $line[$SPECIES_C];

            //desc
            if (isset($line[$NUM_IND_C]) and $line[$NUM_IND_C]!='')
                $cell[3] = $line[$NUM_IND_C];
            else
                $cell[3] = "NA";
            
            //cmt
            $cmt = "NA";
            foreach($st_col['DATE_C'] as $p) {
                $DATE_C = $p;
                if (!$DATE_C)
                    continue;
                if ($column_restriction) {
                    if ( !in_array($DATE_C,$allowed_cols) ) {
                        $line[$DATE_C] = 'NA';
                    }
                }

                if ($general_restriction) {
                    if ( !in_array($DATE_C,$allowed_general_columns) ) {
                        $line[$DATE_C] = 'NA';
                    }
                }

                if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$p,$m))
                $DATE_C = $m[2];

                if($line[$DATE_C]!='') {
                    $cmt = $line[$DATE_C];
                    break;
                }
            }
            $cell[4] = $cmt;
            $row .= implode($cell,$sep)."\n";
            $n++;

            $track_id[] = $line['obm_id'];
        }
        // creaate a gpx using gpsbabel and return its content
        $gpx = gpsbabel($row);

        if ($modules->_include('download_restricted','downloadAccepted')) {
            // if not only the users own data is in the results download is restricted
            $modules->_include('download_restricted','newRequest',['tmpfile' => null, 'filename' => $filename, 'message' => $_POST['message'], 'content' => $gpx],true);
            return;
        }

        if ($gpx!==false) {
            if (function_exists('mb_strlen')) {
                $filesize = mb_strlen($gpx, '8bit');
            } else {
                $filesize = strlen($gpx);
            }

            track_download($track_id);
            # header definíció!!!
            header("Content-Type: text/xml");
            header("Content-Length: $filesize"); 
            header("Content-Disposition: attachment; filename=\"$filename\";");
            header("Expires: -1");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            printf("%s",$gpx);
            return;
        } else {
            echo "Error. See the system log.";
            return;
        }

    }
}
?>
