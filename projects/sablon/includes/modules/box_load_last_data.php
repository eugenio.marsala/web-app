<?php
class box_load_last_data {
    function print_box () {
        global $BID;

        $report_options = array();
        if (isset($_SESSION['Tid'])) {
            $cmd = sprintf("SELECT key,id FROM custom_reports WHERE project_table='%s' AND user_id=%d",$_SESSION['current_query_table'],$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $report_options[] = sprintf('%s::obm_custom_report_%d',$row['key'],$row['id']);
            }
        }

        $t = new table_row_template();
        $t->cell(mb_convert_case(t(str_quick_queries), MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        if (isset($_SESSION['Tid']))
            $mine = sprintf('%s::obm_uploading_id_mine',str_my_last_upload);
        else
            $mine = '';

        $m = new modules();
        $n = "";
        $num = $m->get_params('box_load_last_data');

        $options = selected_option(array_merge(array(sprintf('%s::obm_uploading_id',str_last_upload),sprintf('%s::obm_id',str_last_rows),$mine),$report_options));
        $t->cell("<select id='last_data_query_option'><option></option>$options</select>",2,'content');
        $t->row();
        //$t->cell("<button id='last_data_query' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
        $t->row();
        return sprintf("<table class='mapfb'>%s</table>",$t->printOut());
    }
    function print_js() {
          echo "
    // box load last data module
    $(document).ready(function() {
        $(document).on('change','#last_data_query_option',function(e){
            last_data_enabled = 1;
        });

    });";
    }

}
?>
