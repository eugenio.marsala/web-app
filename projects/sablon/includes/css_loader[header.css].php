<?php

header("Content-type: text/css", true);

if (file_exists('../css/private/header.css')) {

    include_once('../css/private/header.css');

} elseif (file_exists('../css/header.css')) {
    
    include_once('../css/header.css');

}
?>
