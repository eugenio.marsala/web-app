<?php
/* profile main page 
 * included in interface.php profile()
 *
 *
 *
 *
 *
 * */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_session.php');
require_once(getenv('OB_LIB_DIR').'interface.php');
require_once(getenv('OB_LIB_DIR').'languages.php');
#require_once(getenv('OB_LIB_DIR').'languages-admin.php');

if (isset($_SESSION['token']) and $_SESSION['token']['projecttable']!=PROJECTTABLE) {
    echo "Stranger database connection.";
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}
if (!isset($_SESSION['current_query_table'])) $_SESSION['current_query_table'] = PROJECTTABLE;


/* Update Login vars - refresh auth tokens */
if (isset($_COOKIE['access_token'])) {
    $cookie = json_decode( $_COOKIE[ "access_token" ] );

    $res = true;

    if (time() > $cookie->expiry) {
        // expired
        $res = refresh_token();
        $cookie = json_decode( $_COOKIE[ "access_token" ] );
    }

    if ($res) {
        // JUST check token valisity by a resource request
        $result = oauth_webprofile_request($cookie->data->access_token);

        if ($result === FALSE) { 
            /* Handle error */ 
            log_action("Ouath resource request failed ({$cookie->data->access_token}):",__FILE__,__LINE__);
            setcookie("access_token",$_COOKIE['access_token'],time()-1);
            unset($_COOKIE['access_token']);
        }
        else {
            if (!isset($_SESSION['Tid'])) {
                if(!oauth_login($cookie->data->access_token)) {
                    setcookie("access_token",$_COOKIE['access_token'],time()-1);
                    unset($_COOKIE['access_token']);
                }
                // restore previous SESSION variables somehow? I think it is too expensive to save logined SESSION variables in the database...
            }
        }
    }
} elseif (isset($_COOKIE['refresh_token'])) {
    $cookie = json_decode( $_COOKIE[ "refresh_token" ] );

    if (time() < $cookie->expiry) {
        $res = refresh_token();
        if ($res!==false) {
            $cookie = json_decode( $_COOKIE[ "access_token" ] );
            oauth_login($cookie->data->access_token);
        }
    }   
}

if(!isset($_SESSION['Tid'])) {
    // csak bejelentkezve és csak a saját profilunkat tudjuk szerkeszteni
    echo "Session expired.";
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}


/* MODULE INCLUDES HERE */
$modules = new modules();

/*
 * amikor az interface.php profile() függvényből includáljuk az ua. mintha ha GET['profile'] módban hívtuk volna
 * isset($inc)...
 * */
if (!isset($_GET['options'])) {
    $_GET['options']='profile';
}

/* read news 
 *
 * */
if(isset($_GET['options']) and $_GET['options']=='newsread') {

        
    $in =  "Broadcast a message:<br><textarea id='message-body' style='width:600px;height:200px'></textarea><br>";
    if (grst(PROJECTTABLE,"master"))
        $in .= "Message as email? <input type='checkbox' id='message-as-email' value='1' class='pure-button'><br>";
    $in .= "<button id='message-send' class='pure-button button-success'>".str_send." <i class='fa fa-lg fa-send-o'></i></button>";

    echo $in;

    $out = "<ul style='padding:30px 0 30px 14px;list-style:none;display:table;width:800px;border-collaps:separate;border-spacing:5px'>";
    $f = array();
    if (!isset($_SESSION['Tid']))
        $f = "level='project' OR level='public'";
    else
        $f = "project_table='".PROJECTTABLE."'";

    $cmd = "SELECT username,to_char(datum, 'Dy, Month DD. YYYY, HH24:MI') as d,news,uploader,receiver
        FROM project_news_stream LEFT JOIN users ON (users.id=uploader) 
        WHERE project_table='".PROJECTTABLE."' AND (level='project' OR (level='personal' AND receiver={$_SESSION['Tid']})) 
        ORDER BY datum DESC";
    $res = pg_query($BID,$cmd);
    while($row=pg_fetch_assoc($res)) {
        if($row['username']=='') {
            $u = 'system message';
        }
        else $u = $row['username'];
        $out .= sprintf("<li style='display:table-row'><div style='display:table-cell;color:#acacac'>%s</div></li>
                         <li style='display:table-row'><div style='display:table-cell;border-bottom:1px solid #cacaca;padding:1px'>%s</div></li>
                         <li style='display:table-row'><div style='text-align:right;font-size:80%s;font-family:monospace'> $u</div></div></li>",$row['d'],$row['news'],'%');
    }
    // Nincs még kidolgozva
    //$out .= "<li><a href=?news&orderby=date>...".str_morenews."</a></li>";
    $out .= "</ul>";

    echo $out;

    
/* Create new project
 *
 * */
} elseif(isset($_GET['options']) and $_GET['options']=='new_project_form') {
    $typo = "<option value='numeric'>numeric</option>
             <option value='integer'>integer</option>
             <option value='character varying(255)'>string</option>
             <option value='text'>text</option>
             <option value='date'>datum</option>
             <option value='timestamp without time zone'>datetime</option>";

?>

<div id='newpr'>
<h2><?php echo str_new_project; ?> </h2>
<div class='infotitle'><?php echo str_new_project_welcome ?></div>
<br>
<?php 
// Language vars
echo "<script>";
echo "\nvar str_recommended='".t(str_recommended)."';\n";
echo "\nvar str_obligatory='".t(str_obligatory)."';\n";
echo "</script>";
?>
<form method="post" enctype="multipart/form-data" id='new_project' class='pure-form'>
<table style='border-bottom:1px dotted gray;padding-bottom:1em;margin-bottom:1em;' class='create_project'>
<tr><td><?php echo str_nameofproject ?></td><td><input id='project_dir' class='pure-u-1-2' name='project_dir' placeholder='<?php echo  t(str_obligatory).': '.t(str_newform_help_1) ?>' required></td></tr>
<tr><td><?php echo str_proj_sdesc ?></td><td><input id='project_sdesc' class='pure-u-1-2' name='project_sdesc' class="pure-input-1" placeholder='<?php echo  t(str_obligatory).': '.t(str_newform_help_2) ?>' required></td></tr>
<tr><td><?php echo str_proj_desc ?></td><td><textarea name='project_desc' style='width:500px;height:120px' placeholder='<?php echo t(str_obligatory).': '.t(str_newform_help_3) ?>' required></textarea></td></tr>
<tr><td><?php echo str_proj_comment ?></td><td><textarea name='project_comment' style='width:500px;height:120px' placeholder="<?php echo str_newform_help_4 ?>"></textarea></td></tr>
<tr><td><?php echo str_map_access ?></td><td><select name='project_acc'>
    <option value='public'><?php echo str_everyone ?></option>
    <option value='login'><?php echo str_members ?></option>
    <option value='group'><?php echo str_group ?></option></select>
</td></tr>
<tr><td><?php echo str_data_mod ?></td><td><select name='project_mod'>
    <option value='public'><?php echo t(str_everyone) ?></option>
    <option value='login'><?php echo t(str_members) ?></option>
    <option value='group'><?php echo t(str_group) ?></option></select>
</td></tr>
<tr><td><?php echo str_default_lang ?></td><td><select name='project_lang'>
    <option value='hu'><?php echo t(str_hungarian) ?></option>
    <option value='en'><?php echo t(str_english) ?></option>
    <option value='de'><?php echo t(str_deutsch) ?></option>
    <option value='ru'><?php echo t(str_russian) ?></option>
    <option value='fr'><?php echo t(str_french) ?></option>
    <option value='du'><?php echo t(str_dutch) ?></option>
    <option value='it'><?php echo t(str_italian) ?></option>
    <option value='ro'><?php echo t(str_romanian) ?></option>
</td></tr>

<tr><td>PostgreSQL <?php echo str_user ?></td><td><input id='pgsql_admin' name='pgsql_admin' readonly> <span style='color:red'><?php echo str_saveit ?>!</span></td></tr>
<tr><td>PostgreSQL <?php echo str_password ?></td><td><input id='pgsql_passwd' name='pgsql_passwd' readonly> <span style='color:red'><?php echo str_saveit ?>!</span></td></tr>

<tr><td colspan=2>&nbsp;</td></tr>
<tr><td colspan=2><legend><?php echo str_non_obligatory_settings; ?></legend></td></tr>

<tr><td><?php echo str_example_fields ?></td><td></td></tr>
<tr><td colspan=2 id='csvres'>

<table class="resultstable" id='fields_def_table'>
    <th><?php echo t(str_def_columns_names) ?></th><th><?php echo t(str_short_desc) ?></th><th><?php echo t(str_desc_desc) ?></th><th><?php echo str_type ?></th>
    <tr><td><input name='name[]' value='<?php echo str_species; ?>' placeholder='<?php echo  t(str_obligatory) ?>' required></td><td><input name='desc[]' placeholder='<?php echo  t(str_obligatory) ?>' required></td><td><textarea name='meta[]' placeholder='<?php echo  t(str_recommended) ?>'></textarea></td><td><select name='type[]'><?php echo $typo; ?></select></td></tr>
    <tr><td><input name='name[]' value='<?php echo str_date; ?>' placeholder='<?php echo  t(str_obligatory) ?>' required></td><td><input name='desc[]' placeholder='<?php echo  t(str_obligatory) ?>' required></td><td><textarea name='meta[]' placeholder='<?php echo  t(str_recommended) ?>'></textarea></td><td><select name='type[]'><?php echo $typo; ?></select></td></tr>
    <tr><td><input name='name[]' value='<?php echo str_comment; ?>' placeholder='<?php echo  t(str_obligatory) ?>' required></td><td><input name='desc[]' placeholder='<?php echo  t(str_obligatory) ?>' required></td><td><textarea name='meta[]' placeholder='<?php echo  t(str_recommended) ?>'></textarea></td><td><select name='type[]'><?php echo $typo; ?></select></td></tr>
</table>

<button id='fields-def_add-cell' style='padding:2px 20px 2px 20px;' class='button-secondary pure-button'><i class='fa fa-plus-square-o fa-2x'></i></button>
</td></tr>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td><?php echo str_center_x ?></td><td><input name='map_c_x' value='19.22221'></td></tr>
<tr><td><?php echo str_center_y ?></td><td><input name='map_c_y' value='47.30086'></td></tr>
<tr><td>EPSG srid</td><td><input name='map_c_srid' value='4326'></td></tr>
</table>
<button type='button' id='send_newproj_req' class='button-success button-xlarge pure-button pure-u-1-3'><i class='fa fa-cogs'></i> <?php echo str_create ?></button>
<input type='hidden' name='mehet' value=1>
</form>
<br>
<br>
</div> <!--newpr-->
<div id='new_proj_messages'></div>
<?php
} elseif(isset($_GET['options']) and $_GET['options']=='invitations') {
    if (defined('INVITATIONS')) $inv = constant('INVITATIONS');
    else $inv = 10;

    //zero invitation project allow admins to send invitations
    if ($inv==0 and grst(PROJECTTABLE,3))
        $inv = 20;


    echo "<h2>".t(str_invitationsend)."</h2>";
    /********************
    * Send invitation form
    * ******************/
    //1 nap után akkor is tud mehívót küldeni, ha még vannak aktív meghívói
    $cmd = "SELECT count(id) as c FROM invites WHERE user_id='{$_SESSION['Tid']}' AND created + interval '1 days' > now()";
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);
        if ($r['c']<$inv) {

            $button = sprintf("<button id='soi' class='button-success button-large pure-button pure-u-1-4'><i class='fa fa-envelope-o'></i> %s (%d/<span id='invleft'>%d</span>)</button>",str_invitationsend,$inv,$inv-$r['c']);

            printf('<div class="pure-u-2-3">
    <form class="pure-form pure-form-stacked"><fieldset>
    <label for="name">%s</label>
    <input type="text" name="name" id="name" value="" class="pure-u-1-2"><br>
    <label for="email">%s</label>
    <input type="text" name="email" id="email" value="" class="pure-u-1-2"><br>
    <label for="invcomment">%s</label>
    <textarea name="invcomment" id="invcomment" class="pure-u-1-2"></textarea><br>
    </fieldset></form>%s<br><br><hr>
    </div>',str_invited_name,str_invited_email,str_personal_message,$button);
        } else {
            echo "Invitation left: 0";
        }
    }


    // echo active invites
    $cmd = "SELECT user_id,id,name,created,CASE WHEN created + interval '60 days' > now() THEN '0' ELSE '1' END as v,project_table,mail FROM invites WHERE user_id=".$_SESSION['Tid']." OR project_table='".PROJECTTABLE."' ORDER BY name";
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        echo "<br><br><br><br>
            <h2>".t(str_pending_invites)."</h2>
            <table class='resultstable'><tr><th></th><th>".str_who."?</th><th>".str_when."?</th><th>".str_db."</th><th>".str_active."</th></tr>";
        while ($row = pg_fetch_assoc($res)) {
            if ($row['v']=='nem') $c = 'checked';
            else $c = '';

            $delinv = "<input type='checkbox' class='delinv' name='delinv[]' $c value='{$row['id']}'>";
            $bcolor = "";
            if ($row['user_id'] != $_SESSION['Tid']) {
                $delinv = "";
                $bcolor = "style='background-color:#b0b0b0'";
            }
            if ($row['v']) {
                $e = str_expired;
                $bcolor = "style='background-color:#baa070'";
            }
            else $e = str_yes;


            printf("<tr><td $bcolor>$delinv</td><td $bcolor><abbr title='%s'>%s</abbr></td><td $bcolor>%s</td><td $bcolor>%s</td><td $bcolor>%s</td></tr>",$row['mail'],$row['name'],$row['created'],$row['project_table'],$e);
        }
        echo "</table><br><button type='button' id='dropInv' class='button-warning pure-button'><i class='fa fa-trash'></i> ".str_dropselinv."</button>";
    }

} elseif(isset($_GET['options']) and $_GET['options']=='profile') {
    /********************
    * User profile Page
    * ******************/
    $get_own_profile = 0;
        
    if (isset($_GET['get_own_profile'])) {
        $get_own_profile = 1;
    } else if (isset($_SESSION['Tid']) and (!isset($_GET['profile']) or $_GET['profile']==''))
        // just loggen in - no tabs yet
        $get_own_profile = 2;
    
    //if (isset($_GET['userid']) and isset($_SESSION['Tcrypt']))
    //    $userid = $_GET['userid'];

    if (isset($_GET['userid']) and isset($_SESSION['Tcrypt'])) {
        $userid = $_SESSION['Tcrypt'];
        $get_own_profile = 1;
    } elseif (isset($_GET['userid']) and !isset($_SESSION['Tcrypt'])) {
    //if (isset($_GET['userid']) and !isset($_SESSION['Tcrypt'])) {
        echo "<center><h2>Error 404</h2>user not found</center>";
        return;
    }
    // new user privacy checkboxes
    if (isset($_SESSION['register_upw'])) {
        $userid = $_SESSION['Tcrypt'];
        $get_own_profile = 1;
    }

    // last security check
    if ((!isset($userid) or $userid=='' or $userid===0 or $userid==1) and isset($_SESSION['Tcrypt'])) 
        $userid=$_SESSION['Tcrypt'];

    # icons
    if ($get_own_profile) {
        $eicon = "<i class='fa fa-fw fa-pencil'></i>";
        #$excl = "<i class='fa fa-exclamation fa-lg text-important'></i>";
    }
    else {
        $eicon = "";
        #$excl = "";
    }

    $userobj = json_decode(get_profile_data(PROJECTTABLE,$userid),true);
    $user_numid = $_SESSION['Trole_id'];
    $userdata = new userdata($userid,'hash');

    if (!count($userobj)) {
        echo "<center><h2>Error 404</h2>user not found</center>";
        return;
    }

    // SHINYURL global control variable
    // create readable links instead of long get urls
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $baseurl = sprintf($protocol.'://'.URL.'/');

    /* 
     * ORCID get/set name */
    $orcid = $userobj['orcid'];
    $orcid_gn = '';
    $orcid_fn = '';
    $orcid_dept = '';
    $orcid_inst = '';
    $orcid_address = '';
    $orcid_country = '';
    if ($orcid!='') {
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pub.orcid.org/v1.2_rc7/$orcid/orcid-profile",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Accept: application/orcid+xml'),
            CURLOPT_POST => false,
            )
        );
        $result = curl_exec($curl);
        if ($result) {
            if (false === $result) {
                $n='';
                foreach(libxml_get_errors() as $error) {
                    $n.= "\n".$error->message;
                }
                log_action("orcid fetch failed: ".$n,__FILE__,__LINE__);
            }
            $xml = simplexml_load_string($result);
            if (isset($xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'given-names'})) $orcid_gn = $xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'given-names'};
            if (isset($xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'family-name'})) $orcid_fn = $xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'family-name'};
            if (isset($xml->{'orcid-profile'}->{'orcid-activities'}->{'affiliations'}->{'affiliation'})) {
                foreach($xml->{'orcid-profile'}->{'orcid-activities'}->{'affiliations'}->{'affiliation'} as $afl) {
                    if ($afl->{'type'}=='employment') {
                        $orcid_dept = $afl->{'department-name'};
                        $orcid_inst = $afl->{'organization'}->{'name'};
                        $orcid_address = $afl->{'organization'}->{'address'}->{'city'};
                        $orcid_country = $afl->{'organization'}->{'address'}->{'country'};
                    }
                }
            }
        }
    }
    $aa=$wa=$na=0;
    if (isset($_GET['sopd'])){
        if($orcid_gn!='' and $orcid_fn!='') {
            $userobj['username'] =  "$orcid_gn $orcid_fn";
            $userobj['givenname'] =  "$orcid_gn";
            $userobj['familyname'] =  "$orcid_fn";
            $na = 1;
        }
        if($orcid_dept!='' or $orcid_inst!='') {
            $userobj['institute'] =  "$orcid_inst $orcid_dept";
            $wa = 1;
        }
        if($orcid_address!='') {
            $userobj['address'] =  "$orcid_address $orcid_country";
            $aa = 1;
        }
    }

    $login_group = '';
    if(isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='' and $get_own_profile) {
        if(count(explode(",",$_SESSION['Tgroups']))==1) {
            $cmd = sprintf("SELECT description FROM groups WHERE group_id=%d",$_SESSION['Tgroups']);
            $res = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($res);
            $login_group = '['.$row['description'].']';
        }
    }
    $em = "";

    if ($_SESSION['Tcrypt']!=$userid) {
        $em .= "<div style='padding:30px 0px 0px 20px' id='tab_basic'>";
    
        //$username = $userdata->get_username();
        $user_numid = $userdata->get_roleid();
    }
    $em .= "<h2>{$userobj['username']} $login_group";
    if ($get_own_profile == 2) {
        $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2>";
    

    // USER DATA
    $em .= "<label class='profile_table_label'><i class='fa fa-user fa-2x'></i> ".t(str_userdata)."</label>";
    $em .= "<table class='profile_table'>";
    if ($get_own_profile) {
        $gicon = $ficon = $eicon;
        $name_parts = preg_split('/ /',$userobj['username']);
        if ($userobj['familyname']=='') {
            $userobj['familyname'] = $name_parts[0];
            $ficon = "<i class='fa fa-fw fa-question'></i>";
        }
        if ($userobj['givenname']=='' and isset($name_parts[1])) {
            $userobj['givenname'] = $name_parts[1];
            $gicon = "<i class='fa fa-fw fa-question'></i>";
        }

        if ($userobj['givenname']=='') $userobj['givenname'] = '....................';
        if ($userobj['familyname']=='') $userobj['familyname'] = '....................';
        
        $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </td><td>$gicon %s</td></tr>",'%',t(str_given_name),edit($userobj['givenname'],'given_name','givenname',$get_own_profile,'edbox_send_profile',$na));
        $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </td><td>$ficon %s</td></tr>",'%',t(str_family_name),edit($userobj['familyname'],'family_name','familyname',$get_own_profile,'edbox_send_profile',$na));
        // Notice for New User - at first login:
        if (isset($_SESSION['register_upw'])) {
            $em .= sprintf("<tr><td colspan=2><div id='pwnotice' class='button-warning button-xlarge' style='padding:4px'>%s</div></td></tr>",t(str_set_password));
            $stars = $_SESSION['register_upw'];
        } else {
            $stars = '*****';
        }
        $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </td><td>$eicon %s</td></tr>",'%',t(str_password),edit($stars,'password','password',$get_own_profile,'edbox_send_profile'));
    }
    if ($userobj['email']=='' and $get_own_profile) $userobj['email'] = '....................';
    if ($userobj['institute']=='' and $get_own_profile) $userobj['institute'] = '....................';
    if ($userobj['address']=='' and $get_own_profile) $userobj['address'] = '....................';
    if ($userobj['references']=='' and $get_own_profile) $userobj['references'] = '....................';
    if ($userobj['orcid']=='' and $get_own_profile) $userobj['orcid'] = '....................';

    if ($get_own_profile) {
        if (!isset($_SESSION['change_email_response']) or $_SESSION['change_email_response']=='') $cer = str_conf_necessary;
        else {
            $cer = $_SESSION['change_email_response'];
            unset($_SESSION['change_email_response']);
            if ($cer==1) $cer = str_success;
        }
        $put_cer = "(<span id='cer'>$cer</span>)";
    } else 
        $put_cer = "";
    
    if (!$get_own_profile and $userobj['visible_mail']==0) {
        $userobj['email'] = "";
    }
    
    $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </td><td>$eicon %s $put_cer </td></tr>",'%',t(str_email),edit($userobj['email'],'email','email',$get_own_profile,'edbox_send_profile'));
    $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s: </td><td>$eicon %s</td></tr>",'%',t(str_pworkpl),edit($userobj['institute'],'institute','institute',$get_own_profile,'edbox_send_profile',$wa));
    $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s: </td><td>$eicon %s</td></tr>",'%',t(str_paddress),edit($userobj['address'],'address','address',$get_own_profile,'edbox_send_profile',$aa));
    $em .= sprintf("<tr><td style='width:1%s;padding-right:10px;white-space:nowrap'>%s: </td><td>$eicon %s</td></tr>",'%',t(str_references),edit($userobj['references'],'references','references',$get_own_profile,'edbox_send_profile'));
    $em .= "</table><br><br>";

    // Settings
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-sliders fa-lg'></i> ".t(str_settings)."</label>";
        $em .= "<table class='profile_table'>";

        $cmd = sprintf("SELECT receive_mails,visible_mail FROM \"project_users\" WHERE user_id=%d AND project_table=%s",$_SESSION['Tid'],quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        $receive_mails = 'off';
        $visible_mail = 'off';
        $sets = array('off','on');
        $classes = array('passive','success');
        while ($row = pg_fetch_assoc($res)) {
            $receive_mails = $sets[$row['receive_mails']];
            $visible_mail = $sets[$row['visible_mail']];
            $vmc = $classes[$row['visible_mail']];
            $rmc = $classes[$row['receive_mails']];
        }
        $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_email_visibility),"<button class='pure-button button-$vmc button-small' type='button' id='email-visible'><i class='fa fa-lg fa-toggle-$visible_mail'></i></button>");
        $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_email_receive),"<button class='pure-button button-$rmc button-small' type='button' id='email-receive'><i class='fa fa-lg fa-toggle-$receive_mails'></i></button>");
        $em .= sprintf("<tr><td colspan=2><a href='' id='drop_my_account'>%s</a></td></tr>",t(str_drop_profile));
        $em .= "</table><br><br>";
    }

    //ORCID
    $em .= "<label class='profile_table_label'><img src='$protocol://".URL."/css/img/icon-orcid.png' style='width:16px;margin-bottom:-3px'> ".t(str_orcid_profile)."</label>";
    $em .= "<table class='profile_table'>";
    if ($get_own_profile) 
        $em .= sprintf("<tr><td>ORCID id: </td><td>$eicon %s<br><a href='%sindex.php?profile&sopd' id='orcid_profile_update' class='button-href pure-button'>%s</a></td></tr>",edit($userobj['orcid'],'orcid','orcid',$get_own_profile,'edbox_send_profile'),$baseurl,str_load_orcid_profile);
    else {
        $em .= sprintf('<tr><td>ORCID id: </td><td>%1$s</td></tr>',$userobj['orcid']);
    }
    $em .= "</table><br><br>";

    // PROJECT DATA
    $em .= "<label class='profile_table_label'><i class='fa fa-home fa-lg'></i> ".str_userinfo."</label>";
    $em .= "<table class='profile_table'>";
    $transl = array('0'=>str_parked,'1'=>str_user,'2'=>str_master,'3'=>str_operator,'4'=>str_assistant,'banned'=>str_parked,'normal'=>str_user,'master'=>str_master,'operator'=>str_operator,'assistant'=>str_assistant);
    $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_status),$transl[$userobj['user_status']]);
    $label = '';
    if (floatcmp($userobj['validation'],'0')) $label = 'warning';
    if (floatcmp($userobj['validation'],'-0.3')) $label = 'swarning';
    if (floatcmp($userobj['validation'],'-0.5')) $label = 'sswarning';
    $em .= sprintf("<tr><td>%s: </td><td>%.3f</td></tr>",t(str_valuation),$userobj['validation']);
    
    // Ha van csoport hozzárendelése a felhasználónak
    if (isset($userobj['groups']) and $userobj['groups']!='') {
        $cmd = "SELECT * FROM groups WHERE group_id IN ({$userobj['groups']}) AND project_table='".PROJECTTABLE."' ORDER BY description";
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $n = array();
            $i = array();
            while ($row = pg_fetch_assoc($res)) {
                $n[] = sprintf("%s",$row['description']);
            }
            $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_groups),implode(', ',$n));
        }
    }
    $em .= "</table><br><br>";

    // PROJECT ADMIN
    if ($userobj['status'] == '1') { 
        //some message about the extra rights??
    }
    $cmd = "SELECT p.project_table,domain FROM projects p LEFT JOIN project_users pu ON (pu.project_table=p.project_table) LEFT JOIN users u ON u.id=pu.user_id WHERE user_status::varchar IN ('2','master') AND \"user\"='$userid'";
    $rs = pg_query($BID,$cmd);
    if(pg_num_rows($rs)) {
        $em .= "<label class='profile_table_label'><i class='fa fa-database fa-lg'></i> ".str_other_databases."</label>";
        $em .= "<table class='profile_table'>";
        $st = array();
        while($row = pg_fetch_assoc($rs)) {
            $st[] = sprintf('<a href="http://%s" target=_blank>%s</a>',$row['domain'],$row['project_table']);
        }
        $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_project),implode(', ',array_values($st)));
        $em .= "</table><br><br>";
    }

    // USER ACTIVITY
    $em .= "<label class='profile_table_label'><i class='fa fa-bar-chart fa-lg'></i> ".t(str_activity)."</label>";
    $em .= "<table class='profile_table'>";
    if (defined('SHINYURL') and constant("SHINYURL")) #includes/profile.php?options=profile&userid=$userid$ol
        $em .= sprintf("<tr><td>%s: </td><td><a href='%sprofile/uploads/$userid/#tab_basic' data-url='/includes/profile.php?options=uploads&uid=$userid' class='profilelink'>%s</a></td></tr>",t(str_uploadcount),$baseurl,$userobj['uplcount']);
    else
        $em .= sprintf("<tr><td>%s: </td><td><a href='%sindex.php?profile&showuploads&uid=$userid#tab_basic' data-url='/includes/profile.php?options=uploads&uid=$userid' class='profilelink'>%s</a></td></tr>",t(str_uploadcount),$baseurl,$userobj['uplcount']);
    $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_modcount),$userobj['modcount']);
    $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_rowcount),$userobj['upld']);
    $em .= "</table>";

    // activity species list
    //$em .= sprintf("<a href='%sindex.php?user_specieslist=%s'>".t(str_species_stat)."</a><br><br><br>",$baseurl,$_SESSION['Tcrypt']);
    if (defined('SHINYURL') and constant("SHINYURL"))
        $em .= sprintf('<a href=\''.$baseurl.'profile/%1$s/user_specieslist/%1$s/#tab_basic\' data-url=\'/includes/profile.php?options=user_specieslist&userid=%1$s\' class=\'profilelink\'>'.t(str_species_stat).'</a><br>',$userid);
    else
        $em .= sprintf('<a href=\''.$baseurl.'index.php?profile=%1$s&user_specieslist=%1$s#tab_basic\' data-url=\'/includes/profile.php?options=user_specieslist&userid=%1$s\' class=\'profilelink\'>'.t(str_species_stat).'</a><br>',$userid);
    
    // activity area polygon:
    $cmd = sprintf("SELECT ST_AsText(ST_ConvexHull(ST_Collect(obm_geometry))) as area, ST_Area(ST_ConvexHull(ST_Collect(obm_geometry))::geography)/1000000 AS size 
        FROM %s LEFT JOIN system.uploadings ON obm_uploading_id=id 
        WHERE uploader_id=%d",PROJECTTABLE,$user_numid);
    
    //validity check of geoms
    //SELECT ST_AREA(the_geom), ST_ISVALID(the_geom), ST_ISVALIDREASON(the_geom), ST_SUMMARY(the_geom) FROM (SELECT ... ) as foo;
    $resa = pg_query($ID,$cmd);
    if (pg_num_rows($resa)) {
        $row = pg_fetch_assoc($resa);
        $em .= sprintf('<a href="%sgeomtest/?wkt=%s&name=%s&srid=4326" target="_blank">%s: %dkm<sup>2</sup></a>',$baseurl,$row['area'],$userobj['username'],t(str_activity_area),round($row['size']));
    }

    $em .= "<br><br><br>";

    // Postgres users 
    if ($modules->is_enabled('create_pg_user')) {
        $em .= $modules->_include('create_pg_user','show_button');
    }

    /* Saját importok betöltése */
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-floppy-o fa-lg'></i> ".t(str_load_imports)."</label>";
        $em .= "<table class='profile_table'>";
        if (defined('SHINYURL') and constant("SHINYURL"))
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/saveduploads/#tab_basic' data-url='/includes/profile.php?options=saveduploads' class='profilelink'>".str_load_imports."</a>");
        else
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."index.php?profile&showiups#tab_basic' data-url='/includes/profile.php?options=saveduploads' class='profilelink'>".str_load_imports."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('user_interface.html#interrupted-imports',str_what_are_load_imports));
        $em .= sprintf("</table><br><br>");
    }

    // saved queries
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-folder fa-lg'></i> ".t(str_savedqueries)."</label>";
        $em .= "<table class='profile_table'>";
        if (defined('SHINYURL') and constant("SHINYURL"))
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/savedqueries/#tab_basic' data-url='/includes/profile.php?options=savedqueries' class='profilelink'>".str_savedqueries."</a>");
        else
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."index.php?profile&showsavedqueries#tab_basic' data-url='/includes/profile.php?options=savedqueries' class='profilelink'>".str_savedqueries."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('user_interface.html#saved-queries',str_what_are_savedqueries));
        $em .= sprintf("</table><br><br>");
    }

    // saved results
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-inbox fa-lg'></i> ".t(str_savedresults)."</label>";
        $em .= "<table class='profile_table'>";
        if (defined('SHINYURL') and constant("SHINYURL"))
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/savedresults/#tab_basic' data-url='/includes/profile.php?options=savedresults' class='profilelink'>".str_savedresults."</a>");
        else
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."index.php?profile&showsavedresults#tab_basic' data-url='/includes/profile.php?options=savedresults' class='profilelink'>".str_savedresults."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('user_interface.html#saved-results',str_what_are_savedresults));
        $em .= sprintf("</table><br><br>");
    }

    //api keys
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-key fa-lg'></i> ".t(str_apikeys)."</label>";
        $em .= "<table class='profile_table'>";
        if (defined('SHINYURL') and constant("SHINYURL"))
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/apikeys/#tab_basic' data-url='/includes/profile.php?options=apikeys' class='profilelink'>".str_apikeys."</a>");
        else
            $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."index.php?profile&showapikeys#tab_basic' data-url='/includes/profile.php?options=apikeys' class='profilelink'>".str_apikeys."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('user_interface.html#api-keys',str_what_are_apikeys));
        $em .= sprintf("</table><br><br>");
    }

    //custom admin pages include
    if ($get_own_profile) {

        foreach ($modules->which_has_method('profileItem') as $m) {
            if ($ps = $modules->_include($m,'profileItem')) {

                $em .= "<label class='profile_table_label'><i class='fa {$ps['fa']} fa-lg'></i> ".t($ps['label'])."</label>";
                $em .= "<table class='profile_table'>";
                $em .= $ps['item'];
                $em .= sprintf("</table><br><br>");
            }
        }

        // should be replaced in all modules to profileItem!!!
        foreach ($modules->which_has_method('profileSection') as $m) {
            if ($ps = $modules->_include($m,'profileSection')) {

                $em .= "<label class='profile_table_label'><i class='fa {$ps['fa']} fa-lg'></i> {$ps['label']}</label>";
                $em .= "<table class='profile_table'>";
                $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='{$ps['href']}' data-url='{$ps['data_url']}' class='profilelink'>{$ps['label']}</a>");
                $em .= sprintf("<tr><td>%s</td></tr>",wikilink($ps['help']));
                $em .= sprintf("</table><br><br>");
            }
        }
    } 
    
    // user opinions
    $em .= sprintf('<br><br><div style="border:2px #aaaaca dotted;width:550px;padding:10px 6px 10px 6px;"> 
              <h2><i class="fa fa-comments fa-lg"></i> Opinions</h2>');
    
    #$cmd = "SELECT comments,user_name,to_char(datum, 'YYYY-MM-DD HH24:MI') as datum,valuation,user_id,id,related_id FROM evaluations WHERE \"table\"='users' AND row='$userid' AND user_id!='$userid' ORDER BY datum";
    $cmd = sprintf("SELECT row,comments,user_name,to_char(datum, 'YYYY-MM-DD HH24:MI') as datum,valuation,user_id,e.id,related_id FROM evaluations AS e LEFT JOIN users ON (\"table\"='users' AND row=users.id AND user_id!=users.id) WHERE \"user\"=%s ORDER BY datum",quote($userid));
    
    $rs = pg_query($BID,$cmd);
    while($row = pg_fetch_assoc($rs)) {
            // colorized value on left side 
            if ($row['valuation']>0) $val = '<span style="color:blue;font-weight:bold">+1</span>';
            elseif ($row['valuation']<0) $val = '<span style="color:orange;font-weight:bold">-1</span>';
            else $val = '&nbsp; &nbsp;';
            
            // opinion row
            $cmd = sprintf('SELECT "user" FROM users WHERE id=%d',$row['user_id']);
            $resu = pg_query($BID,$cmd);
            $rowu = pg_fetch_assoc($resu);

            // SHINYURL global control variable
            // create readable links instead of long get urls
            if (defined('SHINYURL') and constant("SHINYURL"))
                $eurl = sprintf('http://'.URL.'/profile/%s/',$rowu['user']);
            else
                $eurl = sprintf($baseurl.'?profile=%s',$rowu['user']);

            $em .= '<div style="margin-top:20px;border-top:1px solid #bababa">'.$val.' '.$row['comments'];
            $em .= '<div style="text-align:right;font-size:70%;color:#bababa"><a href=\''.$eurl.'\'>'.$row['user_name'].'</a> '.$row['datum'].'</div></div>'; 
            
            // answer section to opinions
            if ($get_own_profile) {
                $cmd = "SELECT comments FROM evaluations WHERE related_id={$row['id']} and user_id={$row['id']}";
                $rsd = pg_query($BID,$cmd);
                if (!pg_num_rows($rsd)) {
                    $em .= "<div style='padding-left:10px'>Is this useful for you?<br>";
                    $em .= sprintf('<input type=\'hidden\' id=\'relid-%2$s\' value=\''.$row['id'].'\'>'.str_comment.':
                        <div id=\'yohelp-%2$s\' class=\'bubby pure-u-1-1\'>
                        <h3>'.str_thanks_for.'</h3>
                        <ul>
                            <li>You can post your answer with accept or refuse voting. </li>
                        </ul>
                        Avoid …
                        <ul>
                            <li>The negative voting without explanation.</li>
                            <li>The inconsistency between your comment and the voting.</li>
                        </ul></div>
                        <textarea class=\'expandTextarea yo pure-u-1-1\' id=\'evdt-%2$s\'></textarea><br>
                        <input type=\'button\' class=\'button-success pure-button pure-u-1-2 pm\' value=\'accept\' id=\'valp-%2$s\' rel=\'user\'><input type=\'button\' class=\'button-error pure-button pure-u-1-2 pm\' value=\'refuse\' id=\'valm-%2$s\' rel=\'user\'></div><br>',str_valuation,$row['id']);
                } else {
                    $rc = pg_fetch_assoc($rsd);
                    $em .= '<div style=\'border-top:1px dotted #bababa;padding-left:25px;font-size:90%;color:#bababa\'>'.$rc['comments'].'</div>';
                }
            }
    }
    // your opinion box
    if (!$get_own_profile) {
            $em .= sprintf('<br><br><h2>Opinioning</h2>
    <div class=\'opinioning\'><div class=\'bubby pure-u-1-1\'><h3>'.str_thanks_for.'</h3>'.str_opinion_post_1.'<br><br><span style=\'font-variant:small-caps\'>'.str_avoid.'…</span><br>'.str_opinion_post_2.'</div>
    <textarea id=\'evdt-%2$s\' class=\'yo pure-u-1-1\' style=\'border:1px solid #bababa;height:120px;\'></textarea></div>
    <div class=\'yoband\'>
                <b>%s:</b>
                <input type=\'button\' class=\'button-error pure-button pure-u-1-3 pm\' value=\'-\' id=\'valm-%2$s\' rel=\'user\'> 
                <input type=\'button\' class=\'button-secondary pure-button pure-u-1-3 pm\' value=\'ok\' id=\'valz-%2$s\' rel=\'user\'>
                <input type=\'button\' class=\'button-success pure-button pure-u-1-3 pm\' value=\'+\' id=\'valp-%2$s\' rel=\'user\'></div>',str_valuation,$userid);
    }
    // opnion div
    $em .= '</div>';

    if ($_SESSION['Tcrypt']!=$userid)
        $em .= "</div>";

    // print out 
    echo $em;
} elseif(isset($_GET['options']) and $_GET['options']=='uploads') {
    ShowUploads($_GET['uid']); 
} elseif(isset($_GET['options']) and $_GET['options']=='sharedgeoms') {
    $id = 0;
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowSharedGeoms($id,2);
} elseif(isset($_GET['options']) and $_GET['options']=='owngeoms') {
    $id = 0;
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowSharedGeoms($id,1);
} elseif(isset($_GET['options']) and $_GET['options']=='saveduploads') {
    $id = 0;
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowIUPS($id,1);
} elseif(isset($_GET['options']) and $_GET['options']=='savedresults') {
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowSavedResults();
} elseif(isset($_GET['options']) and $_GET['options']=='savedqueries') {
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowSavedQueries();
} elseif(isset($_GET['options']) and $_GET['options']=='apikeys') {
    $id = 0;
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowAPIKeys($id,1);
} elseif(isset($_GET['options']) and $_GET['options']=='user_specieslist') {
    echo usertaxonlist($_GET['userid']);
}
?>
