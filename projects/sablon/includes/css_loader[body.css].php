<?php

header("Content-type: text/css", true);

if (file_exists('../css/private/body.css')) {

    include_once('../css/private/body.css');

} elseif (file_exists('../css/body.css')) {
    
    include_once('../css/body.css');

}
?>
