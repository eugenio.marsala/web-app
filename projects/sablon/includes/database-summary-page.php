<?php
    $out = "<div style='font-size:200%;padding:20px 0 20px 0'>".t(str_db_summary)."</div>";

    # Project aim
    $out .= "<h2>". t(str_project_aims) ."</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= "<li style='width:900px;text-align:justify'>$OB_project_description</li>";

    if (defined('beta') and beta==1) {
        $out .= "<li style='font-size:130%;color:#FF7200'>".str_beta_text."</li>";
    }
    $out .= "</ul>";


    # MAINTAINERS
    $out .= "<a name='maintainers'></a><h2>".t(str_mainteners)."</h2>";
    $cmd = 'SELECT "user",username,institute,email,pu.visible_mail
    FROM users u 
    LEFT JOIN project_users pu ON (pu.user_id=u.id)
    WHERE pu.project_table=\''.PROJECTTABLE.'\' AND user_status::varchar IN (\'2\',\'master\')';
    $result = pg_query($BID,$cmd);
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    while ($row=pg_fetch_assoc($result)) {
        if ($row['visible_mail']=='0')
            $row['email'] = '';
        $out .= sprintf("<li><a href='?profile=%s'>%s</a> &nbsp; %s &nbsp;%s</li>",$row['user'],$row['username'],$row['institute'],$row['email']);
    }
    $out .= "</ul>";

    # FOUNDING
    $out .= "<a name='founder'></a><h2>".t(str_foundation)."</h2>";
    $cmd = 'SELECT "Creator" as founder,to_char(creation_date,\'YYYY mon. DD.\') as fdate,to_char(running_date,\'YYYY mon. DD.\') as rdate,doi FROM projects WHERE project_table=\''.PROJECTTABLE.'\'';
    $result = pg_query($BID,$cmd);
    $row=pg_fetch_assoc($result);
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf('<li>%s, %s</li>',$row['founder'],$row['fdate']);
    $out .= sprintf('<li>%s: %s</li>',t(str_running_date),$row['rdate']);
    $out .= sprintf('<li>DOI: <a href="https://search.datacite.org/works/%1$s">%1$s</a></li>',$row['doi']);
    $out .= sprintf("<li>%s: %s</li>",t(str_contact),$OB_contact_email);
    $out .= "</ul>";


    # MAP info
    $out .= "<a name='info'></a><h2>".t(str_map_inf)."</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $cmd = "SELECT srid,type FROM geometry_columns WHERE f_table_name='".PROJECTTABLE."'";
    if ($res = pg_query($ID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $out .= sprintf('<li>%1$s (epsg): <a href=\'http://spatialreference.org/ref/epsg/%2$s/\' target=\'_blank\'>%2$s</a></li>',t(str_projection),$row['srid']);
        $out .= sprintf("<li>PostGIS %s: %s</li>",t(str_geometry_type),strtolower($row['type']));
    }
    $out .= "<li><br>".t(str_wmswfs_access).":</li>";
    if (defined('PUBLIC_MAPFILE')) {
        //$p_p = pathinfo(PUBLIC_MAPFILE);
        //$MAP = $p_p['dirname'].'/'.$p_p['basename'];

        $url = "http://".URL."/public/proxy.php?MAP=PMAP&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities";
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if ($xml !== FALSE) {
            $out .= "<li><i>WFS</i>: <a href='$url'>$url</a></li>";
        }
        $url = "http://".URL."/public/proxy.php?MAP=PMAP&SERVICE=WMS&VERSION=1.1.0&REQUEST=GetCapabilities";
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if ($xml !== FALSE) {
            $out .= "<li><i>WMS</i>: <a href='$url'>$url</a></li>";
        }
    }
    $out .= "<li><br>".t(str_moreinfo_wms).":</li>";
    $out .= "<li><a href='http://en.wikipedia.org/wiki/Web_Map_Service' target='_blank'>http://en.wikipedia.org/wiki/Web_Map_Service</a></li>";
    $out .= "<li><a href='http://www.osgeo.org' target='_blank'>http://www.osgeo.org</a></li>";
    $out .= "<li><a href='http://www.ogcnetwork.net/node/175' target='_blank'>http://www.ogcnetwork.net/node/175</a></li>";
    $out .= "</ul>";


    ## Download
    if (defined('str_download_text')) {
        $out .= "<a name='download'></a><h2>".t(str_db_download)."</h2>";
        $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
        $out .= "<li>".t(str_download_text)."</li>";
        $out .= "</ul>";
    }
    
    ## Summary
    $out .= "<a name='summary'></a><h2>".t(str_data_summary)."</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";

    if ($_SESSION['st_col']['SPECIES_C']!='') {
        $SPECIES_C = $_SESSION['st_col']['SPECIES_C'];
        $sss = preg_replace('/[a-z0-9_]+\./','',$SPECIES_C);
        //$hely = $_SESSION['st_col'][''];

        $cmd = "SELECT COUNT($sss) FROM (SELECT DISTINCT $SPECIES_C FROM ".PROJECTTABLE.") AS temp";
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        $out .= sprintf("<li><a href='?specieslist'>%d %s</a></li>",$row['count'],str_speciescount);
    }
    
    require_once(getenv('OB_LIB_DIR').'mainpage_functions.php');
    $mf = new mainpage_functions();
    $c = $mf->count_data();
    $out .= sprintf("<li>%d %s</li>",$c,str_data_p);
    $out .= "</ul>";

    # MEMBERS
    $out .= "<a name='members'></a><h2>".t(str_members)."</h2>";

    $cmd = "SELECT \"user\",username,institute
    FROM users u 
    LEFT JOIN project_users pu ON (pu.user_id=u.id)
    WHERE pu.project_table='".PROJECTTABLE."'
    GROUP BY username,u.\"user\",u.institute
    ORDER BY username";

    $result = pg_query($BID,$cmd);
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    if (isset($_SESSION['Tid']))
        while ($row=pg_fetch_assoc($result)){
            $out .= sprintf("<li><a href='?profile=%s'>%s</a> &nbsp; %s</li>",$row['user'],$row['username'],$row['institute']);
        }
    else
        $out .= "<li>".pg_num_rows($result)."</li>";
    $out .= "</ul>";

    # AUTHORS / Significant contributors
    # It not handles the all project tables!!!
    #
    $out .= "<a name='publisher'></a><h2>".t(str_significant_contributors)."</h2>";
    $cmd = "SELECT uploader_name,uploader_id,COUNT(*) as cn
            FROM system.uploadings u
            LEFT JOIN \"".PROJECTTABLE."\" d ON (d.obm_uploading_id=u.id)
            WHERE uploader_id>0 AND obm_uploading_id IS NOT NULL
            GROUP BY uploader_name,uploader_id
            HAVING count(*)>(SELECT COUNT(*)*0.01 FROM \"".PROJECTTABLE."\")
            ORDER BY cn DESC LIMIT 10";
    $result = pg_query($ID,$cmd);
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    if (isset($_SESSION['Tid']))
        while ($row=pg_fetch_assoc($result)){
            $out .= sprintf("<li>%s</li>",$row['uploader_name']);
        }
    else
        $out .= "<li>".pg_num_rows($result)."</li>";
    $out .= "</ul>";


    # NEWS STREAM
    $out .= "<a name='news'></a><h2>".t(str_news_stream)."</h2>";

    //news stream
    //
    $out .= "<ul style='padding:0 0 30px 30px;list-style:none;display:table;width:800px;border-collaps:separate;border-spacing:5px'>";
    $f = array();
    if (!isset($_SESSION['Tid']))
        $f = "level='project' OR level='public'";
    else
        $f = "project_table='".PROJECTTABLE."'";

    $cmd = "SELECT username,to_char(datum, 'Dy, Month DD. YYYY, HH24:MI') as d,news,uploader FROM project_news_stream LEFT JOIN users ON (users.id=uploader) WHERE level='public' OR (level='project' AND project_table='".PROJECTTABLE."') ORDER BY datum DESC LIMIT 20";
    $res = pg_query($BID,$cmd);
    $r = rainbow(array('00','180','42'),array('200','200','200'),pg_num_rows($res));
    $rn = rainbow(array('80','80','80'),array('200','200','200'),pg_num_rows($res));
    if (isset($_SESSION['Tid']))
        while($row=pg_fetch_assoc($res)) {
            $color = array_shift($r);
            $colorn = array_shift($rn);
            if($row['username']=='') {
                $u = 'system message';
            }
            else $u = $row['username'];
            $out .= sprintf("<li style='display:table-row'>
                                <div style='color:$color;width:190px;display:table-cell;white-space:nowrap'>%s&nbsp;</div>
                                <div style='color:$color;display:table-cell'>%s <span style='font-size:80%s;color:$colorn;font-family:monospace'> $u</span></div></li>",$row['d'],$row['news'],'%');
        }
    else
        $out .= "<li>".pg_num_rows($res)."</li>";
    // Nincs még kidolgozva
    //$out .= "<li><a href=?news&orderby=date>...".str_morenews."</a></li>";
    $out .= "</ul>";

?>
