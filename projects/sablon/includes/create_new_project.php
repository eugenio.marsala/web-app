<?php
/* Called: new_project_form.php 
 * Method: post
 * Description:
 * Ez a fájl hozza létre az új projektet
 *
 *
 *
 *
 * */
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

print "<h1>Create new project's logs</h1>";

//only for logined users
if(!isset($_SESSION['token'])) { 
    require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
require_once(getenv('OB_LIB_DIR').'languages.php');


if(!isset($_SESSION['Tid'])) {
    echo "Allowed only for logined users.";
    exit;
}


# main folders
$project_dirs = array('js','css','includes','images','private','public','attached_files','oauth','pds','uploads','uploads/datafiles','shiny');

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

# included libs for ajax and http calls
# /libs/nnn.php -> /projects/zzz/includes/nnn.php
#$lib_files = array('afuncs.php','create_new_project.php','evaluation.php','getList.php','geomtest.php','load_queries.php','logout.php','mailread.php','mod_project.php','modules.js.php','photos.php','profile.php','project_admin.php','query_builder.php','queries.php','read_results.php','results_table.php','results_query.php','session.php','update_fields.php','update_mapfile.php','update_profile.php','update_taxonc.php','upload.php','wfs_processing.php','wfs_show_results.php');
# /template/initial/root/nnn -> /projects/zzz/
$root_files = array('index.php','header.php.inc','footer.php.inc','job.php','pds.php','robots.txt','maintenance.php');
# /template/initial/includes/nnn -> /projects/zzz/includes/nnn

# This happens when the send button pressed on the bottom of the new_project page
if (isset($_POST['mehet']) and isset($_SESSION['Tid'])) {
    
    $table = preg_replace('[^a-z0-9_]','',$_POST['project_dir']);
    if ($table=='') {
        print "<span class='err'>1: should be filled correctly.</span>";
        exit;
    }
    if (preg_match('/[^a-z0-9_]/',$table)) {
        print "<span class='err'>1: illegal characters. Only allowed are letters a-z, numbers 0-9 and _ </span>";
        exit;
    }
    if ($_POST['project_sdesc']=='') {
        print "<span class='err'>2: should be filled correctly.</span>";
        exit;
    }
    $short_desc = quote($_POST['project_sdesc']);
    $long_desc = quote($_POST['project_desc']);
    $project_comment = quote($_POST['project_comment']);
    
    $admin = quote($_SESSION['Tname']);
    $mail = quote($_SESSION['Tmail']);
    $project_contains_data = 0;

    $new_project_dir = getenv("PROJECT_DIR")."../".$table;
    $new_project_acc = preg_replace('/[^a-z]/','',$_POST['project_acc']);
    $new_project_mod = preg_replace('/[^a-z]/','',$_POST['project_mod']);
    $new_project_default_lang = preg_replace('/[^A-Za-z]/','',$_POST['project_lang']);
    $new_project_srid = preg_replace('/[^0-9]/','',$_POST['map_c_srid']);
    $new_project_admin = preg_replace('/^a-z0-9_/','',$_POST['pgsql_admin']);
    $new_project_admin_pw = preg_replace('/^A-Za-z0-9_/','',$_POST['pgsql_passwd']); # BEFEJEZNI!

    $port = isset($_SERVER['HTTP_X_FORWARDED_PORT']) ? $_SERVER['HTTP_X_FORWARDED_PORT'] : $_SERVER['SERVER_PORT'];
    if ($port != 80 and $port != 443 ) {
        $port = ":".$port;
    } else {
        $port = "";
    }

    # local_vars.php.inc
    # A jelszót változóból kéne venni, ami generálva lett!!!!
    $local_vars = array(
        'gisdb_user'=>''.$new_project_admin.'',
        'gisdb_pass'=>''.$_POST['pgsql_passwd'].'',
        'gisdb_name'=>''.gisdb_name.'',
        'gisdb_host'=>GISDB_HOST,
        'PROJECTTABLE'=>"$table",
        'ACC_LEVEL'=>$new_project_acc,
        'MOD_LEVEL'=>$new_project_mod,
        'LANG'=>''.$new_project_default_lang.'',
        'PATH'=>''.PATH.'',
        'URL'=>'sprintf("%s%s%s%s",$_SERVER[\'SERVER_NAME\'],"'.$port.'",PATH,\'/\'.PROJECTTABLE)',
        #'OAUTHURL'=>'sprintf("%s%s%s",$_SERVER[\'SERVER_NAME\'],PATH,\'/\'.PROJECTTABLE)',
        'PRIVATE_MAPSERV'=>'sprintf("%s/private/proxy.php",URL)',
        'PUBLIC_MAPSERV'=>'sprintf("%s/public/proxy.php",URL)',
        'PRIVATE_MAPCACHE'=>'sprintf("%s/private/cache.php",URL)',
        'PUBLIC_MAPCACHE'=>'sprintf("%s/public/cache.php",URL)',
        'MAPSERVER'=>'http://'.MAPSERVER_HOST.'/cgi-bin/mapserv',
        'MAPCACHE'=>'http://localhost/mapcache',
        'MAP'=>'PMAP',
        'PRIVATE_MAPFILE'=>'private.map',
        'INVITATIONS'=>11,
        'SMTP_AUTH'=>'false',
        'SMTP_HOST'=>'',
        'SMTP_USERNAME'=>'',
        'SMTP_PASSWORD'=>'',
        # can be moved into project_variables
        'SHINYURL'=>0,
        'LOAD_INTROPAGE'=>0,
        'MAP_OVER_MAINPAGE'=>0,
        #'OB_PROJECT_DOMAIN'=>''.OB_DOMAIN.'' Do not put it by default!!!
    );

    /* Default local lang texts languages/local_xx.php
     **/
    $local_lang = array(
        #'str_example_local_text'=>'Example text which defined only this in this database.',
        #'str_download_text'=>''
    );

    /* Drop project's main table if stage is "forehand"
     * return if not
     * */
    $cmd = sprintf("SELECT email,stage,running_date,doi,date_part('day',now()-creation_date) as age FROM projects WHERE project_table=%s",quote($table));
    $result = pg_query($BID,$cmd);
    if(pg_num_rows($result)) {
        $row = pg_fetch_assoc($result);
        if($row['doi']!=false) {
            print "<span class='err'>This name already used an other project.</span><br>";
            return;
        }
        elseif($row['stage']!='experimental') {
            print "<span class='err'>This name already used an other project.</span><br>";
            return;
        }
        elseif($row['running_date']!=false) {
            print "<span class='err'>This name already used an other project.</span><br>";
            return;
        }
        elseif($row['email']!=$_SESSION['Tmail']) {
            print "<span class='err'>You hav no rights to overwrite this name</span><br>";
            return;
        }
        elseif($row['age']>7) {
            print "<span class='err'>This experimental project too old to overwrite, please contact an administrator.</span><br>";
            return;
        }
    }


    $cmd = "DELETE FROM projects WHERE project_table='$table' AND admin_uid='{$_SESSION['Tid']}' AND \"Creator\"=$admin AND stage='experimental'";
    $result = pg_query($BID,$cmd);
    #$project_exists = pg_affected_rows($result);
    $e = pg_last_error($BID);
    if($e) { 
        print "<span class='err'>SQL error: $e.</span><br>$cmd";
        return;
    }
    $cmd = "SELECT EXISTS (
                   SELECT 1
                   FROM   information_schema.tables 
                   WHERE  table_schema = 'public'
                   AND    table_name = '".$table."');";
    $res = pg_query($ID,$cmd);
    $row = pg_fetch_assoc($res);
    if ($row['exists']=='t') 
        $project_exists = 1;
    else
        $project_exists = 0;


    /* Creating project's Main Table
     *
     * */

    $host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];

    if (defined('OB_PROJECT_DOMAIN')) {
        $domain = quote(sprintf("%s/projects/%s",OB_PROJECT_DOMAIN,$table));
    } elseif (defined('OB_DOMAIN')) {
        $domain = quote(sprintf("%s/projects/%s",OB_DOMAIN,$table));
    } else 
        $domain = quote(sprintf("%s/projects/%s",$host,$table));

    $date = date("Y-m-d H:i:s");
    $cmd = sprintf('INSERT INTO projects (comment,admin_uid,project_table,creation_date,email,domain,"Creator",stage,doi,running_date,licence,rum,licence_uri,alternate_id,collection_dates,project_hash) 
                    VALUES (%s,%d,%s,%s,%s,%s,%s,%s,NULL,NULL,NULL,NULL,NULL,NULL,NULL,\'%s\')',
                    quote($project_comment),$_SESSION['Tid'],quote($table),"'$date'",$mail,$domain, $admin, "'experimental'",substr(base_convert(md5($date.$admin.$table.$domain),16,32),0,12));
    $n = pg_query($BID,$cmd);
    if(!$n) { 
        $e = pg_last_error($BID);
        print "<span class='err'>SQL error: $e.</span><br>$cmd";
        return;
    }

    /* Project variables
     * 
     *
     * */
    $cmd = sprintf('INSERT INTO project_variables (project_table,map_center,map_zoom,click_buffer)
                    VALUES (%1$s,st_geomfromtext(\'POINT(%2$s %3$s)\',4326),%4$d,%5$d)',
                        quote($table),preg_replace('[^0-9.]','',$_POST['map_c_x']),preg_replace('[^0-9.]','',$_POST['map_c_y']),
                        7,0);
    $n = pg_query($BID,$cmd);
    if(!$n) { 
        $e = pg_last_error($BID);
        print "<span class='err'>SQL error in project variables: $e.</span><br>$cmd";
        return;
    }

    /* Project's descriptions table
     *
     * */ 
    $cmd = sprintf('INSERT INTO project_descriptions (projecttable,language,short,long) VALUES (\'%s\',\'%s\',%s,%s)',
                    $table,$new_project_default_lang,$short_desc,$long_desc);
    $n = pg_query($BID,$cmd);
    if(!$n) { 
        $e = pg_last_error($BID);
        print "<span class='err'>SQL error: $e.</span><br>$cmd";
        return;
    }

    /* 8.
     * database column definitions
     * */
    $defs = "";
    $col_comm = "";
    $col_name = "";
    for($i=0;$i<count($_POST['name']);$i++) {
        $name = $_POST['name'][$i];
        if ($name=='') continue;
        $desc = $_POST['desc'][$i];
        if ($desc=='') continue;
        $meta = $_POST['meta'][$i];
        $type = $_POST['type'][$i];
        $defs .= "$name $type,\n";
        if ($meta!='') $col_comm .= "COMMENT ON COLUMN $table.$name IS '$meta';\n";
        //if ($desc!='') $col_name .= "\"$name\"=>\"$desc\",";
    }
    $defs = preg_replace("/,\n$/",'',$defs);
    if ($defs != "")
        $defs .= ",";

    if ($project_exists) {

        $cmd = "SELECT count(*) FROM $table";
        $res = pg_query($ID,$cmd);
        if ($res) {
            if ($row=pg_fetch_assoc($res)) {
                if($row['count']>0) {
                    echo "<span class='err'>The $table project contains data</span><br>";
                    $project_contains_data = 1;
                }
            }
        } else {
            $e = pg_last_error($ID);
            $project_contains_data = 0;
            echo "<span class='err'>The empty $table table will be redefined!</span><br>";
        }
    }

    $admin_user_exists = 0;
    $cmd = sprintf("SELECT u.usename AS \"User name\", u.usesysid AS \"User ID\", CASE WHEN u.usesuper AND u.usecreatedb THEN CAST('superuser, create
database' AS pg_catalog.text)
       WHEN u.usesuper THEN CAST('superuser' AS pg_catalog.text)
       WHEN u.usecreatedb THEN CAST('create database' AS pg_catalog.text)
       ELSE CAST('' AS pg_catalog.text)
       END AS \"Attributes\"
    FROM pg_catalog.pg_user u
    WHERE u.usename=%s",quote($new_project_admin));
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) $admin_user_exists = 1;

    if (!$admin_user_exists) {
        $cmd = "CREATE USER $new_project_admin WITH PASSWORD '{$_POST['pgsql_passwd']}' INHERIT";
        if (pg_query($ID,$cmd)) {
            echo "SQL: '$new_project_admin' created with password '{$_POST['pgsql_passwd']}' <span class='done'>done</span>.<br>";
            echo "This user can connect to the PostgreSQL database.<br><span style='font-size:150%' class='err'>Save this account informations.</span><br>";
        } else {
            $e = pg_last_error($ID);
            echo "<span class='err'>Create user error: $e</span><br>";
        }
    } else {
        #$e = pg_last_error($ID);
        #if (preg_match("/role \"".$table."_admin\" already exists/",$e)) {
            echo "<span class='err'>$table admin already exists! Skipping.</span><br>";
            $admin_user_exists = 1;
        #} else
        #    echo "<span class='err'>Create user error: $e</span><br>";
        //return;
    }

    /* taxon list trigger
     * Nem jó itt létrehozni, mert nincs hozzárendelve a faj és alternatív név!!!
     * */
    //$cmd = Create_Project_SQL_functions($table,'taxonlist');
    $cmd = sprintf('CREATE OR REPLACE FUNCTION update_%1$s_taxonlist()
        RETURNS TRIGGER AS
        \'
        BEGIN
        END 
        \' LANGUAGE plpgsql;ALTER FUNCTION update_%1$s_taxonlist() OWNER TO %2$s;',$table,$new_project_admin);
    
    if (pg_query($ID,$cmd)) print "SQL: Create taxon list function  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($ID);
        print "<span class='err'>SQL error: $e<br>$cmd</span><br>";
        exit;
    }

    /* taxon name update trigger
     * */
    //$cmd = Create_Project_SQL_functions($table,'taxonname');
    $cmd = sprintf('CREATE OR REPLACE FUNCTION update_%1$s_taxonname()
        RETURNS TRIGGER AS
        \'
        BEGIN
        END 
        \' LANGUAGE plpgsql;;ALTER FUNCTION update_%1$s_taxonname() OWNER TO %2$s;',$table,$new_project_admin);
    
    if (pg_query($ID,$cmd)) print "SQL: Create taxon name update function  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($ID);
        print "<span class='err'>SQL error: $e<br>$cmd</span><br>";
        exit;
    }

    //$cmd = Create_Project_SQL_functions($table,'history');
    $cmd = sprintf('CREATE OR REPLACE FUNCTION history_%1$s()
        RETURNS TRIGGER AS
        \'
        BEGIN
        END 
        \' LANGUAGE plpgsql;;ALTER FUNCTION history_%1$s() OWNER TO %2$s;',$table,$new_project_admin);

    /* history trigger function
     */
    if (pg_query($ID,$cmd)) print "SQL: Create history function  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($ID);
        print "<span class='err'>SQL error: $e</span><br>";
        return;
    }

    // data table
    $cmd = sprintf('SET statement_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s (
        obm_id integer NOT NULL,
        obm_geometry geometry,
        obm_datum timestamp with time zone,
        obm_uploading_id integer,
        obm_validation numeric,
        obm_comments text[],
        obm_modifier_id integer,
        obm_files_id character varying(32),
        '.$defs.'
        CONSTRAINT enforce_dims_obm_geometry CHECK ((st_ndims(obm_geometry) = 2)),
        CONSTRAINT enforce_geotype_obm_geometry CHECK (((geometrytype(obm_geometry) = \'POINT\'::text) OR (geometrytype(obm_geometry) = \'LINE\'::text) OR (geometrytype(obm_geometry) = \'POLYGON\'::text) OR (obm_geometry IS NULL))),
        CONSTRAINT enforce_srid_obm_geometry CHECK ((st_srid(obm_geometry) = 4326))
    );
    COMMENT ON TABLE %1$s IS \'user defined table:%2$s\';
    '.$col_comm.'
    CREATE SEQUENCE %1$s_obm_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    ALTER SEQUENCE %1$s_obm_id_seq OWNED BY %1$s.obm_id;
    ALTER TABLE ONLY %1$s ALTER COLUMN obm_id SET DEFAULT nextval(\'%1$s_obm_id_seq\'::regclass);
    ALTER TABLE ONLY %1$s ALTER COLUMN obm_datum SET DEFAULT now();
    ALTER TABLE ONLY %1$s ADD CONSTRAINT %1$s_pkey PRIMARY KEY (obm_id);
    CREATE INDEX %1$s_obm_geometry_idx ON %1$s USING gist (obm_geometry);
    CREATE INDEX %1$s_obm_uploading_id_idx ON %1$s USING btree (obm_geometry);
    DROP TRIGGER IF EXISTS history_update_%1$s ON public.%1$s;
    CREATE TRIGGER history_update_%1$s AFTER DELETE OR UPDATE ON %1$s FOR EACH ROW EXECUTE PROCEDURE history_%1$s();
    ALTER TABLE %1$s DISABLE TRIGGER history_update_%1$s;
    DROP TRIGGER IF EXISTS taxon_update_%1$s ON public.%1$s;
    CREATE TRIGGER taxon_update_%1$s BEFORE INSERT ON %1$s FOR EACH ROW EXECUTE PROCEDURE update_%1$s_taxonlist();
    ALTER TABLE %1$s DISABLE TRIGGER taxon_update_%1$s;
    CREATE TRIGGER file_connection AFTER INSERT ON %1$s FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();
    ALTER TABLE %1$s DISABLE TRIGGER file_connection;
    ALTER TABLE ONLY %1$s ADD CONSTRAINT obm_uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES system.uploadings(id);
    ALTER TABLE public.%1$s OWNER TO '.$new_project_admin.';
    ',$table,$_SESSION['Tname']);

    if(!$project_contains_data) {

        if ($project_exists) {
            # clear project
            $cmde = sprintf('
            ALTER TABLE ONLY public."%1$s" DROP CONSTRAINT IF EXISTS %1$s_pkey;
            ALTER TABLE public."%1$s" ALTER COLUMN obm_id DROP DEFAULT;
            DROP SEQUENCE IF EXISTS public.%1$s_obm_id_seq CASCADE;
            DROP TABLE IF EXISTS public."%1$s";
            SET search_path = public, pg_catalog;',$table);
        
            $res = pg_query($ID,$cmde);
        }

        if (pg_query($ID,$cmd)) echo "SQL: Create data table  <span class='done'>done</span>.<br>";
        else {
            $e = pg_last_error($ID);
            echo "<span class='err'>SQL error: $e<br>$cmd</span><br>";
            return;
        } // Data Table
    } 

    // rules table
    //

    $cmd = Create_Project_plus_tables($table,'rules',$new_project_admin);

    // clear rules
    if ($project_exists) {
        $cmde = sprintf('
        ALTER TABLE ONLY public."%1$s_rules" DROP CONSTRAINT IF EXISTS %1$s_rules_ukey;
        DROP TABLE IF EXISTS public.%1$s_rules;
        SET search_path = public, pg_catalog;',$table);

        $res = pg_query($ID,$cmde);
    }
        
    if (pg_query($ID,$cmd)) echo "SQL: Create data-rules table  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($ID);
        echo "<span class='err'>SQL error: $e</span><br>";
        return;
    }

    // History Table 
    $cmd = Create_Project_plus_tables($table,'history',$new_project_admin);

    $cmd .= sprintf('DROP TRIGGER IF EXISTS history_update ON public.%1$s;
                     CREATE TRIGGER history_update AFTER DELETE OR UPDATE ON %1$s FOR EACH ROW EXECUTE PROCEDURE history_%1$s();',$table);

    // clear history
    if ($project_exists) {
        $cmde = sprintf('
        ALTER TABLE ONLY public."%1$s_history" DROP CONSTRAINT IF EXISTS %1$s_history_pkey;
        ALTER TABLE public."%1$s_history" ALTER COLUMN hist_id DROP DEFAULT;
        DROP SEQUENCE IF EXISTS public.%1$s_history_hist_id_seq CASCADE;
        DROP TABLE IF EXISTS public.%1$s_history;
        SET search_path = public, pg_catalog;',$table);

        $res = pg_query($ID,$cmde);
    }
        
    if (pg_query($ID,$cmd)) echo "SQL: Create data-history table  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($ID);
        echo "<span class='err'>SQL error: $e</span><br>";
        return;
    }
    /* header_names table */
    $col_name = preg_replace('/,$/','',$col_name);
    $cmd = "INSERT INTO header_names (f_table_schema, f_table_name, f_srid, f_main_table) VALUES ('public','$table','$new_project_srid','$table')";
    if (pg_query($BID,$cmd)) echo "SQL: Create default data headers  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($BID);
        echo "<span class='err'>SQL error: $e</span><br>";
        //return;
    }

    /* project_metaname table */
    if (!$project_exists) {
        $cmd = "SELECT column_name FROM information_schema.columns WHERE table_schema='public' AND table_name='$table'";
        $result = pg_query($ID,$cmd);
        while($row=pg_fetch_assoc($result)) {
            $m = '';
            $d = '';
            $key = -1;
            if(array_search($row['column_name'], $_POST['name'])!==false) {
                // valami pozíció hivatkozás kéne ide inkább,,,
                $key = array_search($row['column_name'], $_POST['name']);
                if ($key!==false) {
                    $m = $_POST['meta'][$key];
                    $d = $_POST['desc'][$key];
                }
            } else continue;
            //$name = $_POST['name'][$i];
            $cf = sprintf("INSERT INTO project_metaname (project_table,column_name,description,short_name,rights) VALUES (%s,%s,%s,%s,'{0}')",quote($table),quote($row['column_name']),quote($m),quote($d));
            //echo $cf.'<br>';
            pg_query($BID,$cf);
        }
    } else {
        $cf = sprintf("DELETE FROM project_metaname WHERE project_table=%s",quote($table));
        pg_query($BID,$cf);
    }

    $cmd = "INSERT INTO project_users (project_table,user_id) VALUES ('$table',{$_SESSION['Tid']})";
    if (pg_query($BID,$cmd)) echo "SQL: Create project admin  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($BID);
        echo "<span class='err'>SQL error: $e</span><br>";
    }

    // taxon name's table
    $cmd = Create_Project_plus_tables($table,'taxon',$new_project_admin);

    $cmd .= sprintf('DROP TRIGGER IF EXISTS %1$s_name_update ON public.%1$s_taxon;
                     CREATE TRIGGER %1$s_name_update AFTER UPDATE ON %1$s_taxon FOR EACH ROW EXECUTE PROCEDURE update_%1$s_taxonname();',$table);

    // clear taxon tabla
    if ($project_exists) {
        $cmde = sprintf('
        ALTER TABLE ONLY public."%1$s_taxon" DROP CONSTRAINT IF EXISTS %1$s_taxon_metaword_key;
        ALTER TABLE public."%1$s_taxon" ALTER COLUMN taxon_id DROP DEFAULT;
        DROP SEQUENCE IF EXISTS public.%1$s_taxon_taxon_id_seq CASCADE;
        DROP TABLE IF EXISTS public.%1$s_taxon;
        SET search_path = public, pg_catalog;',$table);
        $res = pg_query($ID,$cmde);
    }
    
    if (pg_query($ID,$cmd)) echo "SQL: Create taxon name table  <span class='done'>done</span>.<br>";
    else {
        $e = pg_last_error($ID);
        echo "<span class='err'>SQL error: $e</span><br>";
        return;
    }

    
    // SET PRIVILEGES
    if(!$admin_user_exists) {
        $r = '';

        //function owner
        $cmd = sprintf("ALTER FUNCTION update_%s_taxonlist() OWNER TO %s;",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on update-taxonlist function to '%s'  <span class='done'>done</span>.<br>",$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));
        
        $cmd = sprintf("ALTER FUNCTION update_%s_taxonname() OWNER TO %s;",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on update-taxonname function to '%s'  <span class='done'>done</span>.<br>",$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));
        
        $cmd = sprintf("ALTER FUNCTION history_%s() OWNER TO %s;",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on history-update function to '%s'  <span class='done'>done</span>.<br>",$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));


        //table owner
        /*$cmd = sprintf("ALTER TABLE %s OWNER TO %s;",$table,$_POST['pgsql_admin']);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on %s to '%s'  <span class='done'>done</span>.<br>",$table,$_POST['pgsql_admin']);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));

        $cmd = sprintf("ALTER TABLE %s_taxon OWNER TO %s;",$table,$_POST['pgsql_admin']);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on %s_taxon to '%s'  <span class='done'>done</span>.<br>",$table,$_POST['pgsql_admin']);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));

        $cmd = sprintf("ALTER TABLE %s_history OWNER TO %s;",$table,$_POST['pgsql_admin']);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on %s_history to '%s'  <span class='done'>done</span>.<br>",$table,$_POST['pgsql_admin']);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));
        */

        //table privileges
        #$cmd = sprintf("GRANT ALL PRIVILEGES ON %s TO %s",$table,$_POST['pgsql_admin']);
        #if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed %s to '%s' <span class='done'>done</span>.<br>",$table,$_POST['pgsql_admin']);
        #else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));
        
        $cmd = sprintf("REVOKE DELETE ON %s_history FROM %s",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on %s_history to '%s'  <span class='done'>done</span>.<br>",$table,$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));

        $cmd = sprintf("GRANT ALL PRIVILEGES ON %s_taxon TO %s",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on %s_taxon to '%s'  <span class='done'>done</span>.<br>",$table,$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));

        $cmd = sprintf("GRANT ALL PRIVILEGES ON %s_rules TO %s",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on %s_taxon to '%s'  <span class='done'>done</span>.<br>",$table,$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));

        $cmd = sprintf("GRANT project_admin TO %s",$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed to '%s'  <span class='done'>done</span>.<br>",$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));

        //sequences' usage rights
        $cmd = sprintf("GRANT SELECT,USAGE ON %s_obm_id_seq TO %s",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on sequences main to '%s'  <span class='done'>done</span>.<br>",$table,$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));
        
        $cmd = sprintf("GRANT SELECT,USAGE ON %s_history_hist_id_seq TO %s",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on sequences history to '%s'  <span class='done'>done</span>.<br>",$table,$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));
            
        $cmd = sprintf("GRANT SELECT,USAGE ON %s_taxon_taxon_id_seq TO %s",$table,$new_project_admin);
        if (pg_query($ID,$cmd)) $r .= sprintf("SQL: privileges passed on sequences taxon_list to '%s'  <span class='done'>done</span>.<br>",$table,$new_project_admin);
        else $r .= sprintf("<span class='err'>Privileges error: %s</span><br>",pg_last_error($ID));        

        echo $r;
    } else {
        //$cmd ="ALTER USER %s_admin WITH PASSWORD '{$_POST['pgsql_passwd']}'";
    }

    $failed_create_dir = 0;
    echo '<b>Creating project folder:</b> ';

    if (is_dir("$new_project_dir")) {
        echo "<span class='err'>The project directory already exists. No way to overwrite the files.</span><br>";
        $failed_create_dir = 1;
    } elseif(!mkdir("$new_project_dir", 0755, true)) {
        echo "<span class='err'>failed (permission denied)</span><br>";
        $failed_create_dir = 1;
        log_action("Creating new project directory failed: $new_project_dir",__FILE__,__LINE__);
    }
    else echo "<span class='done'>done</span><br>";

    if (!$failed_create_dir) {
        echo '<b>Creating project config file:</b> ';
        create_define_file("$new_project_dir/local_vars.php.inc",$local_vars) ? print "<span class='done'>done</span><br>" : print "<span class='err'>local_vars.php.inc create error</span><br>";
        echo "<b>Creating project folders:</b> <br>";
        foreach ($project_dirs as $d) {
            echo "$d ";
            mkdir("$new_project_dir/$d", 0755, true) ? print "<span class='done'>ok</span><br>" : print "<span class='err'>err</span><br>";
        }
        # root files
        echo '<b>Copying root files:</b> ';
        if (is_dir(OB_RESOURCES)) {
            $e=0;
            foreach($root_files as $f) {
                if (is_file(OB_RESOURCES."$f")) {
                    copy(OB_RESOURCES."$f","$new_project_dir/$f") ? $e+=1 : print "<span class='err'>$f err</span><br>";
                }
            }
        } else echo "<span class='err'>/ template directory not found.</span><br>";

        # js files
        echo '<b>Copying js files:</b> ';
        if (is_dir(OB_RESOURCES."js")) {
            $res = copyr(OB_RESOURCES.'js/',"$new_project_dir/js/");
            if ($res > 0) echo $res.' error<br>';
            else echo "<span class='done'>done</span><br>";
        } else echo "<span class='err'>js template directory not found.</span><br>";

        # css files
        echo '<b>Copying css files:</b> ';
        if (is_dir(OB_RESOURCES."css")) {
            $res = copyr(OB_RESOURCES.'css/',"$new_project_dir/css/");
            if ($res > 0) echo $res.' error<br>';
            else  echo "<span class='done'>done</span><br>";
        } else echo "<span class='err'>css template directory not found.</span><br>";

        # image files
        echo '<b>Copying image files:</b> ';
        if (is_dir(OB_RESOURCES."images")) {
            $res = copyr(OB_RESOURCES.'images/',"$new_project_dir/images/");
            if ($res > 0) echo $res.' error<br>';
            else  echo "<span class='done'>done</span><br>";
        } else echo "<span class='err'>image template directory not found.</span><br>";

        # libs files
        echo '<b>Copying lib files:</b> ';
        if (is_dir(OB_RESOURCES."libs")) {
            # libs to includes
            $res = copyr(OB_RESOURCES.'libs/',"$new_project_dir/includes/");
            //debug("copyr ".OB_RESOURCES."libs/ => $new_project_dir/includes/",__FILE__,__LINE__);
            if ($res > 0) echo $res.' error<br>';
            else  echo "<span class='done'>done</span><br>";

            # nem kell, mert a linkelve vannak a private/public könyvtárakba
            #foreach(array('private','public') as $pdir) {
            #    copy(OB_RESOURCES."libs/cache.php","$new_project_dir/$pdir/cache.php") ? $e+=1 : print "<br><span class='err'>cache.php error</span><br>";
            #    copy(OB_RESOURCES."libs/proxy.php","$new_project_dir/$pdir/proxy.php") ? $e+=1 : print "<span class='err'>proxy.php error</span><br>";
            #} 
        } else echo "<span class='err'>libs template directory not found.</span><br>";

        # oauth files
        echo '<b>Copying authentication layer files:</b> ';
        if (is_dir(OB_RESOURCES."oauth")) {
            $e=0;
            $res = copyr(OB_RESOURCES."oauth/","$new_project_dir/oauth/");
            if ($res > 0) echo $res.' error<br>';
            else echo "<span class='done'>done</span><br>";
        }  else echo "<span class='err'>authentication template directory not found.</span><br>";
        
        # pds files
        echo '<b>Copying API layer files:</b> ';
        if (is_dir(OB_RESOURCES."pds")) {
            $e=0;
            $res = copyr(OB_RESOURCES."pds/","$new_project_dir/pds/");
            if ($res > 0) echo $res.' error<br>';
            else echo "<span class='done'>done</span><br>";
        }  else echo "<span class='err'>API template directory not found.</span><br>";

        echo '<b>Copying template files:</b> <br>';
        $e=0;
        
        # copy mapserver files from tempalte
        foreach (array('private','public') as $d) {
            $files = scandir(OB_RESOURCES."$d/");
            foreach($files as $f) {
                if (is_file(OB_RESOURCES."$d/$f")) {
                    copy(OB_RESOURCES."$d/$f","$new_project_dir/$d/$f") ? $e+=1 : print "<span class='err'>$d/$f err</span><br>";
                }
            }
            // map file templates
            $files = scandir(OB_RESOURCES."templates/$d/");
            foreach($files as $f) {
                if (is_file(OB_RESOURCES."templates/$d/$f")) {
                    copy(OB_RESOURCES."templates/$d/$f","$new_project_dir/$d/$f") ? $e+=1 : print "<span class='err'>$d/$f err</span><br>";
                }
            }
        }

        # copy and process project's .htaccess file
        echo "Configuring .htaccess file...";
        copy(OB_RESOURCES."templates/.htaccess","$new_project_dir/.htaccess") ? $e+=1 : print "<span class='err'>.htaccess err</span><br>";
        $md = pathinfo(getenv("PROJECT_DIR"));
        $md = $md['dirname']."/".basename($new_project_dir)."/";
        $str=implode("",file("$new_project_dir/.htaccess"));
        
        $fp=fopen("$new_project_dir/.htaccess",'w');
        $str=str_replace('@@1@@',"$md",$str);
        $str=str_replace('@@2@@',"$md"."includes/",$str);
        $str=str_replace('@@3@@',$table,$str);
        //$str=str_replace('@@4@@',$_SERVER['SERVER_NAME'],$str);

        $r = explode('/',constant("OB_DOMAIN"));
        $host = $r[0];
        if (isset($r[1])) {
            $obm_root = "/$r[1]/";
        } else {
            $obm_root = "/";
        }
        $obm_root = preg_replace('/\/\//','/',$obm_root);
        $str=str_replace('@@5@@',$obm_root,$str);

        fwrite($fp,$str,strlen($str));
        echo "done. The base path can vary depending on the global settings, if the AJAX is not reachable you need to modify the base path in .htaccess file.<br>";


        echo "<span class='done'>$e files copied</span><br>";

        // Create map files
        echo '<b>Configuring mapserver\'s map files:</b> ';
        if (defined("OB_SYSDIR"))
            $enc_passwd = exec("msencrypt -key ".OB_SYSDIR."maps/access.key {$_POST['pgsql_passwd']}");
        else
            $enc_passwd = exec("msencrypt -key /var/lib/openbiomaps/maps/access.key {$_POST['pgsql_passwd']}");

        $str=implode("",file("$new_project_dir/private/private.map"));
        $fp=fopen("$new_project_dir/private/private.map",'w');
        $str=str_replace('@@admin@@',"$new_project_admin",$str);
        $str=str_replace('@@name@@',"$table",$str);
        $str=str_replace('@@title@@',"{$_POST['project_sdesc']}",$str);
        $str=str_replace('@@passwd@@',"$enc_passwd",$str);
        $str=str_replace('@@srid@@',"$new_project_srid",$str);
        fwrite($fp,$str,strlen($str)); 

        $str=implode("",file("$new_project_dir/public/public.map"));
        $fp=fopen("$new_project_dir/public/public.map",'w');
        $str=str_replace('@@admin@@',"$new_project_admin",$str);
        $str=str_replace('@@name@@',"$table",$str);
        $str=str_replace('@@title@@',"{$_POST['project_sdesc']}",$str);
        $str=str_replace('@@passwd@@',"$enc_passwd",$str);
        $str=str_replace('@@srid@@',"$new_project_srid",$str);
        fwrite($fp,$str,strlen($str)); 

        # Map extent
        #SELECT Extent(the_geom) AS extent FROM dinpi;
        #if ($new_project_acc>0) ...
        echo "<span class='err'>Update your map files!</span><br>";
    }

    // Registering default modules
    echo '<b>Registering default modules:</b> ';
    $r = '';
    $cmd = sprintf("SELECT count(id) as c FROM modules WHERE project_table=%s",quote($table));
    $res = pg_query($BID,$cmd);

    if (!pg_last_error($BID)) {
        $row = pg_fetch_assoc($res);
        if ($row['c']>0) {
            $r .= sprintf("<span class='err'>Modules have already created.</span><br>");
        } else {
            $content = file(OB_RESOURCES.'libs/default_modules.php');
            $modules = array();
            $modules_files = array();
            $m_name = 0;
            $f_name = 0;
            foreach ($content as $rows) {
                if (preg_match('/# Module/',$rows))
                    $m_name = 1;
                elseif (preg_match('/# File/',$rows))
                    $f_name = 1;

                $m = array();
                if ($m_name and preg_match('/## (.+)/',$rows,$m)) {
                    $m_name = 0;
                    $modules[$m[1]] = '';
                    $last_module = $m[1];
                } elseif ($f_name and preg_match('/## (.+)/',$rows,$m)) {
                    $f_name = 0;
                    $modules[$last_module] = $m[1];
                }
            }
            #$cmd = sprintf("INSERT INTO modules
            #(module_name,project_table,file,function,enabled) 
            #( SELECT DISTINCT module_name,%s,file,'',FALSE 
            #  FROM modules WHERE file != 'NOT EXISTS' )",quote($table));
            $mrows = array();
            foreach ($modules as $name=>$file) {
                $mrows[] = sprintf("%s,%s,%s,'',FALSE,%s",quote($name),quote($table),quote($file),quote($table));
            }
            $cmd = sprintf("INSERT INTO modules (module_name,project_table,file,function,enabled,main_table) VALUES %s",'('.implode('),(',$mrows).')');
            
            if (pg_query($BID,$cmd)) $r .= sprintf("SQL: modules created <span class='done'>done</span>.<br>");
            else 
                $r .= sprintf("<span class='err'>Modules error: %s</span><br>",pg_last_error($ID));
        }
    }
    else $r .= sprintf("<span class='err'>Modules error: %s</span><br>",pg_last_error($ID));

    echo "$r";


    // Final Message:

    echo "<br>
    <b>The project is accessible, however some more basic settings needed after you logged in first time.<br>Follow this link to leave this database and login to your new database:</b><br>";
    printf('&nbsp; &nbsp; <a href="'.$protocol.$port.'://%s/projects/%2$s/index.php" style="font-size:2em">%2$s</a><br><br><br>',OB_DOMAIN,$table);

    $username = $_SESSION['Tname'];
    $Title = t(str_dear)." user!<br><br>";
    if ($username != '')
        $Title = t(str_dear)." $username!<br><br>";

    if (defined("OB_PROJECT_DOMAIN")) {
        $domain = constant("OB_PROJECT_DOMAIN");
        $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
    } elseif (defined("OB_DOMAIN")) {
        $domain = constant("OB_DOMAIN");
        $server_email = PROJECTTABLE."@".parse_url('http://'.$domain,PHP_URL_HOST);
    } else {
        $domain = $_SERVER["SERVER_NAME"];
        $server_email = PROJECTTABLE."@".$domain;
    }

    $Message = "You have created a new database [$table] on the $domain server.<br><br>";
    $Message.= sprintf('You can find it here: <a href="'.$protocol.$port.'://%s/projects/%2$s/index.php" style="font-size:2em">%2$s</a><br><br>',OB_DOMAIN,$table);
    $Message.= "Use this email address and your password to sign in.<br>";
    $Message.= "After you logged in your database, you should set some or several things depending on your demands:<ol>";
    $Message.= "<li>Assign your database columns to OBM</li>";
    $Message.= "<li>Set your map files</li>";
    $Message.= "<li>Set your trigger functions</li>";
    $Message.= "<li>Set your SQL queries</li>";
    $Message.= "<li>Set your map layers</li>";
    $Message.= "<li>Enable modules you need</li>";
    $Message.= "<li>Set up upload forms</li>";
    $Message.= "<li>Set up user groups</li>";
    $Message.= "<li>Set up local language files</li>";
    $Message.= "</ol><br> Please note, some of these settings can be complicated at the first time.<br>Read the manuals, ask on the forum, ask ".PROJECTTABLE."'s administrator or ask the OBM Team<br>";
    $Message.= "sign up on [admin mailing list]: http://lists.openbiomaps.org/cgi-bin/mailman/listinfo/administrator<br><br>";
    $Message.= "If you have any problem or you need help, please contact the OpenBioMaps at help@openbiomaps.org.<br><br><br>";
    $Message.= "Best wishes,<br>The OpenBioMaps Team<br>http://openbiomaps.org/";
    mail_to($_SESSION['Tmail'],"[".PROJECTTABLE."] new database: $table","$server_email","$server_email",$Title,$Message,"multipart"); 

    insertNews("{$_SESSION['Tname']} has founded a new database: $table",'public');
}
?>
