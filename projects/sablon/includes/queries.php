<?php
/* sime html kérésre válaszol és GET stringet dolgoz fel
 * Ebben a fájlban csak a GET['gpx'] és GET['csv'] opciók érdekesek, a többi ajax hívás a mehet máshova
 *
 *
 * */
session_start();
//csak akkor hívható, ha a főlap is volt már hívva
if(!isset($_SESSION['token'])) return;

/* SAVE: csv */
if (isset($_GET['csv'])) {
    #if(isset($_SESSION['wfs_full_array']))
    #    $put = $_SESSION['wfs_full_array'];
    #elseif (isset($_SESSION['wfs_array']))
    #    $put = $_SESSION['wfs_array'];
    #else 
    $sep = '';
    $quote = '';
    
    $put = new stdClass();
    if (isset($_GET['sep'])) $sep = $_GET['sep'];
    if (isset($_GET['quote'])) $quote = $_GET['quote'];
    #dinpi_2016-02-01_13:16_[DegzsMx].csv
    //$csvfilename = preg_replace("/[^a-zA-Z0-9\[\]:._-]/","",$_GET['csv']);
    $filename = $_GET['csv'];
    require_once(getenv('OB_LIB_DIR').'results_builder.php');
    $r = new results_builder(['method'=>'csv','filename'=>$filename,'sep'=>$sep,'quote'=>$quote]);
    if($r->results_query()) {
        if ($r->printOut()) exit;
    }
    echo json_encode(['error'=>$r->error]);
    exit;
}

/* SAVE: gpx */
if (isset($_GET['gpx'])) {
    #if(isset($_SESSION['wfs_full_array']))
    #    $put =$_SESSION['wfs_full_array'];
    #elseif (isset($_SESSION['wfs_array']))
    #max 2000 rows to GPX
    #if (isset($_SESSION['wfs_array']))
    #    $put = $_SESSION['wfs_array'];
    #else 
    
    $put = new stdClass();

    if (isset($_GET['sep'])) $s = $_GET['sep'];
    else $s = ",";
    if (isset($_GET['quote'])) $q = $_GET['quote'];
    else $q = "'";
    $filename = $_GET['gpx'];

    require_once(getenv('OB_LIB_DIR').'results_builder.php');
    $r = new results_builder(['method'=>'gpx','filename'=>$filename,'sep'=>$s,'quote'=>$q]);
    if($r->results_query()) {
        if ($r->printOut()) exit;
    }
    echo json_encode(['error'=>$r->error]);
    exit;
}
/* SAVE: species list */
if (isset($_GET['splist']) and isset($_SESSION['current_specieslist'])) {
    echo 1;
    $row = implode("\n",$_SESSION['current_specieslist']);
    if (function_exists('mb_strlen')) {
       $filesize = mb_strlen($row, '8bit');
    } else {
       $filesize = strlen($row);
    }
    # header definíció!!!
    header("Content-Type: text/csv");
    header("Content-Length: $filesize"); 
    header("Content-Disposition: attachment; filename=\"species_list.csv\";");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    printf("%s",$row);
    exit;
}
?>
