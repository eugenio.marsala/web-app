<?php
/* maintenance scheduler code */


$schedule_day = "2011/12/13";
$from = "23:00";
$length = "2 hours";
$address = "127.0.0.1";
$local_mconf = 0;
$system_mconf = 0;
$mconf = "../maintenance.conf";
$maintenance_message = "";

if (file_exists('./maintenance.conf'))
    $local_mconf = filemtime('./maintenance.conf');

if (file_exists('../maintenance.conf'))
    $system_mconf = filemtime('../maintenance.conf');

if ($local_mconf>$system_mconf)
    $mconf = "./maintenance.conf";


if (file_exists($mconf)) {
    $contents = file($mconf); 
    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>"0");

    foreach ($contents as $line) {

        if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $schedule_day = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $from = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $length = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $address = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }

        if (preg_match('/^## start day/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['day'] = 1;
        }
        elseif (preg_match('/^## from/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['from'] = 1;
        }
        elseif (preg_match('/^## length/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['length'] = 1;
        }
        elseif (preg_match('/^## maintenance address/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['address'] = 1;
        }
    }
    $date = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start_format = $date->format('Y-m-d H:i');
    $length = preg_replace("/hour.?/","H",$length);
    $length = preg_replace("/minute.?/","M",$length);
    $date_end = $date->add(new DateInterval('PT'.str_replace(" ","",$length)));
    $date_end_format = $date_end->format('Y-m-d H:i');

    $date_now = new DateTime();
    $interval = $date_now->diff($date_start);
    $diff = $interval->format('%r%d %h %i');
    $maintenance_message = "";

    #var_dump($interval);
    #echo 

    if ($_SERVER['REMOTE_ADDR']!=$address) {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24)
            $maintenance_message = "<span style='padding:10px;display:inline-block;'>Database maintenance will be scheduled between $date_start_format and $date_end_format!</span>";
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "<span style='padding:10px;display:inline-block;'>Database maintenance!</span>";
            include("maintenance.php");
        }
    } else {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24)
            $maintenance_message = "<span style='padding:10px;display:inline-block;background-color:pink;width:100%'>Database maintenance will be scheduled between $date_start_format and $date_end_format!</span>";
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "<span style='padding:10px;display:inline-block;background-color:pink;width:100%'>Database maintenance!</span>";
        }
    }
}
// **************************************************************************************************** \\

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

/* Uploading process
 * Standalone page for choose upload method, prepare data, send data...
 *
 *
 * */

session_cache_limiter('nocache');
session_cache_expire(0);
session_start();

$_SESSION['LAST_ACTIVITY'] = time();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'upload_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

if (!isset($_SESSION['private_key']))
    $_SESSION['private_key'] = genhash();

$modules = new modules();

if (!isset($_SESSION['openpage_key'])) $_SESSION['openpage_key'] = genhash();

track_visitors('upload');

st_col('default_table');

$data_length = 0;
$Error = '';
$show_geom_ref = '';
//$show_geom_type = '';
$upload_comment = '';
$file_process = 0;
$geom_process = 0;
$edit_process = 0;
$interconnect_process = 0;
$insert_allowed = -1;
$selected_form_id = -1;
$selected_form_type = '';
$selected_form_table = '';
$form_chooser = 1;
$module_chooser = 0;

/* Project name and description */
$cmd = "SELECT short,long,language FROM project_descriptions WHERE projecttable='".PROJECTTABLE."'";
$res = pg_query($BID,$cmd);
$row = pg_fetch_all($res);
$k = array_search($_SESSION['LANG'], array_column($row, 'language'));
if(!$k) $k = 0;
$OB_project_title = $row[$k]['short'];
$OB_project_description = $row[$k]['short'];

/* Default error message, if no forms available 
 *
 * */
if (count(form_choose_list())==0) 
    $Error = str_no_forms_available."!";


// ezt még rendezni kell...
if (count($_GET)==0 and count($_POST)==0) {
    $_SESSION['upload'] = array();
    // module beállítások törlése
    /*if (isset($_SESSION['upload_module'])) {
        foreach($_SESSION['upload_module'] as $l) {
            unset($_SESSION[$l]);
        }
        unset($_SESSION['upload_module']);
    }*/
}

if (!isset($_SESSION['upload'])) $_SESSION['upload'] = array();

/* A get módszer megengedi, hogy bookmarkból olvassuk ki a formot
 * és a html link választás végső pontja is egyúttal
 * */
if (isset($_GET['form'])) {
    $selected_form_type='';
    $selected_form_id = preg_replace('/[^0-9,]/','',$_GET['form']);
        
    if (isset($_GET['type'])) {
        $selected_form_type = preg_replace('/[^filewebap]/','',$_GET['type']);
    } 
    
    if (isset($_GET['set_fields'])) {
        $preSetFields = json_decode($_GET['set_fields']);
    }

    if (form_access_check($selected_form_id,$selected_form_type)) {
        
        $_SESSION['upload']['saveref'] = crc32(session_id().$selected_form_id.$selected_form_type.time());

        if($selected_form_type=='file')
            $insert_allowed = -1;
        else
            $insert_allowed = $selected_form_id;

        //$_SESSION['upload']['selected_form_id'] = array($form_id);
        //$_SESSION['upload']['selected_form_type'] = array($form_type);

        $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%d and active=1",$selected_form_id);
        $res = pg_query($BID,$cmd);
        if($row=pg_fetch_assoc($res)) {
            if ( $row['destination_table'] == '')
                $selected_form_table = PROJECTTABLE;
            else
                $selected_form_table = $row['destination_table'];
        }
        //added for files insert
        $_SESSION['current_query_table'] = $selected_form_table;
    }
}
/* Load previously saved upload session
 *
 * */
elseif (isset($_GET['load'])) {
    unset($_SESSION['sheetDataPage']);
    unset($_SESSION['sheetDataPage_counter']);
    unset($_SESSION['upload']);
    $_SESSION['upload'] = array();

    $REF = quote(preg_replace("/[^0-9a-zA-Z]/",'',$_GET['load']));
    $cmd = "SELECT data,header,form_type,form_id,file,ref,massive_edit FROM system.imports WHERE ref=$REF";
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);

        $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%d AND project_table=%s",$row['form_id'],quote(PROJECTTABLE));
        $res2 = pg_query($BID,$cmd);
        if (pg_num_rows($res2)) {
            $row2 = pg_fetch_assoc($res2);
            $_SESSION['current_query_table'] = $row2['destination_table'];
        } else {
            log_action("form {$row['form_id']} in ".PROJECTTABLE." doen not exists!!!");
        }

        //clean_upload_temp();
        restore_upload_temp($row['file'],$row['ref']);
        $_SESSION['upload']['loadref'] = $_GET['load'];
        if ($row['massive_edit']!='') {
            $_SESSION['upload']['massiveedit'] = 3;
            $_SESSION['upload']['massiveedit_ids'] = json_decode($row['massive_edit']);
        }
        unset($_GET['load']);
        
        if (form_access_check($row['form_id'],$row['form_type'])) {

            $_SESSION['upload']['saveref'] = crc32(session_id().$selected_form_id.$selected_form_type.time());

            // decode saved header and data
            $jh = json_decode($row['header']);
            //$jd = json_decode($row['data'],true);
            $jda = json_encode($row['data']);
            $insert_allowed=$row['form_id'];
            $selected_form_type=$row['form_type'];
            $selected_form_id=$row['form_id'];
            $form_chooser = 0;

            if ($row['form_type']=='file') {
                //$_SESSION['sheetData'] = json_decode($row['file'],true);
                $_SESSION['theader'] = get_sheet_header();
            }
            $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%d and active=1",$selected_form_id);
            $res = pg_query($BID,$cmd);
            
            if ($row=pg_fetch_assoc($res)) {
                if ( $row['destination_table'] == '')
                    $selected_form_table = PROJECTTABLE;
                else
                    $selected_form_table = $row['destination_table'];
            }
        } else {
            $_SESSION['upload'] = array();
            //$_SESSION['sheetData'] = array();
            $_SESSION['theader'] = array();
            // translations!!!
            $Error = "The requested form does not exist or has no access.";
        }
    } else {
        $_SESSION['upload'] = array();
        //$_SESSION['sheetData'] = array();
        $_SESSION['theader'] = array();
        $Error = "Requested form id does not exists, data can't be load.";
    }
} elseif (isset($_GET['massiveedit'])) {
    //massive edit 
    $_SESSION['upload']['massiveedit'] = 1;
    unset($_SESSION['upload']['saveref']);

} elseif (isset($_GET['geom'])) {
    $geom_process = 1;
}
/* IMPORT FILE
 * itt kell a session változó mert egy form submit után érünk ide!!!
 *
 * */
// very simple method to get file from net!
if (isset($_POST['url']) and $_POST['url']!='') {

    /* google client api - for downloading files from google drive
     * per-user client credentials needed!
    require_once getenv('OB_LIB_DIR').'/google-api-php-client-2.2.1/vendor/autoload.php';
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    $client = new Google_Client();
    $client->setAuthConfig(getenv('OB_LIB_DIR').'/google-api-php-client-2.2.1/client_secret.json');
    $client->setRedirectUri($redirect_uri);
    $client->addScope("https://www.googleapis.com/auth/drive");
    $driveService = new Google_Service_Drive($client);
    $fileId = '1Vuk9t0oXopzHLoJN0adiTP3kcCmRY2MP';
    $response = $driveService->files->get($fileId, array('alt' => 'media'));
    $content = $response->getBody()->getContents();
    */

    $url = $_POST['url'];
    $filename = basename($url);
    $data_dir = getenv('OB_LIB_DIR')."../uploads/datafiles/";
    file_put_contents($data_dir.$filename, file_get_contents($url));
    $_FILES['file'] = array();
    $_FILES['file']['error'] = 0;
    $_FILES["file"]["tmp_name"] = $data_dir.$filename;
    $_FILES['file']['name'] = $filename;
    $_FILES['file']['type'] = pathinfo($url, PATHINFO_EXTENSION);
}
/* interconnect 
 *
 * */
if (isset($_POST['iclist'])) {
    $interconnect_process = 1;
    $insert_allowed = $_SESSION['upload']['selected_form_id'];
    $selected_form_id = $_SESSION['upload']['selected_form_id'];
    $selected_form_type = 'file';

    //upload_sandbox_l8a6m68nfabtrj26jr6reg5e93_wk_httxLlSE31jf6
    $m = preg_split('/_/',$_POST['iclist']);
    $icfile = $m[0].'_'.$m[1].'_'.$m['2'];
    $icref = $m[3].'_'.$m[4];
    unset($_FILES);
}

/* File upload
 *
 * */
if (isset($_FILES['file']) and isset($_SESSION['upload']) and $_SESSION['upload']['selected_form_type']=='file' and $_SESSION['upload']['selected_form_id']!='') {
    $file_process = 1;
    if (isset($_POST['file_type_as']))
        $file_type_as = $_POST['file_type_as'];
    else
        $file_type_as = '';
    if (isset($_POST['extra_file_arguments']))
        $extra_file_arguments = $_POST['extra_file_arguments'];
    else
        $extra_file_arguments = '';
    $insert_allowed = $_SESSION['upload']['selected_form_id'];
    $selected_form_id = $_SESSION['upload']['selected_form_id'];
    $selected_form_type = 'file';
}
//unset($_SESSION['upload']['massiveedit_ids']);
/* massive edit
 *
 * */
if (isset($_SESSION['upload']['massiveedit']) and $_SESSION['upload']['massiveedit']==2) {
    unset($_SESSION['sheetDataPage']);
    unset($_SESSION['sheetDataPage_counter']);

    if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
        $cmd = sprintf('SELECT * FROM (SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s) t',PROJECTTABLE,session_id());
    else
        $cmd = sprintf("SELECT * FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id());

    $res = pg_query($ID,$cmd);
    if(pg_num_rows($res)) {
        $row = pg_fetch_all($res);

        $_SESSION['sheetData'] = $row;
        $ak = $row[0];
        unset($ak['uploading_date']);
        unset($ak['uploader_name']);
        unset($ak['uploader_id']);
        unset($ak['data_eval']);
        unset($ak['upid']);
        unset($ak['obm_modifier_id']);
        unset($ak['obm_read']);
        unset($ak['obm_write']);
        unset($ak['obm_sensitivity']);
        unset($ak['obm_files_id']);

        $_SESSION['theader'] = array_keys($ak);
        $insert_allowed = $selected_form_id;
        /*unset($jid['uploading_date']);
        unset($jid['uploader_name']);
        unset($jid['uploader_id']);
        unset($jid['data_eval']);
        unset($jid['upid']);
        unset($jid['obm_modifier_id']);
        unset($jid['obm_read']);
        unset($jid['obm_write']);
        unset($jid['obm_sensitivity']);
        unset($jid['obm_files_id']);*/
    }

    pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.upload_%s_%s",PROJECTTABLE,session_id()));
    $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.upload_%s_%s (row_id integer, skip_marked boolean DEFAULT false NOT NULL, data json)",PROJECTTABLE,session_id());
    pg_query($ID,$cmd);
    update_temp_table_index (PROJECTTABLE.'_'.session_id(),'upload');

    if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
        $cmd = sprintf('SELECT * FROM (SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s) t',PROJECTTABLE,session_id());
    else
        $cmd = sprintf("SELECT * FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    $row_id = 1;
    $lines = "";
    while($row=pg_fetch_assoc($res)) {
        $_SESSION['upload']['massiveedit_ids'][] = $row['obm_id'];
        unset($row['uploading_date']);
        unset($row['uploader_name']);
        unset($row['uploader_id']);
        unset($row['data_eval']);
        unset($row['upid']);
        unset($row['obm_modifier_id']);
        unset($row['obm_read']);
        unset($row['obm_write']);
        unset($row['obm_sensitivity']);
        unset($row['obm_files_id']);
        
        $lines .= sprintf("INSERT INTO temporary_tables.upload_%s_%s (row_id, data) VALUES (%d,%s);",PROJECTTABLE,session_id(),$row_id,quote(json_encode($row,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT)));
        $row_id++;
    }
    pg_query($ID,$lines);

} elseif(isset($_SESSION['upload']['massiveedit'])) {
    $_SESSION['upload']['massiveedit']++;
}

/* module include 
 *
 * */
$error = array();

?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <title><?php echo $OB_project_title.' - '.str_upload ?></title>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/grids-responsive-min.css?rev=<?php echo rev('css/pure/grids-responsive-min.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/mapsdata.css?rev=<?php echo rev('../css/mapsdata.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/style.css?rev=<?php echo rev('../css/style.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/uploader.css?rev=<?php echo rev('../css/uploader.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/dropit.css?rev=<?php echo rev('../css/dropit.css'); ?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/roller.css?rev=<?php echo rev('../css/roller.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fSelect.css?rev=<?php echo rev('../css/fSelect.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fTSelect.css?rev=<?php echo rev('../css/fTSelect.css'); ?>" type="text/css" />
<?php
// Include custom css if exists
if (file_exists('../css/private/custom.css')) {
    echo '<link rel="stylesheet" href="'.$protocol.'://'. URL .'/css/private/custom.css?rev='.rev('../css/private/custom.css').'" type="text/css" />';
}
?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('../js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('../js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/fSelect.js?rev=<?php echo rev('../js/fSelect.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/fTSelect.js?rev=<?php echo rev('../js/fTSelect.js'); ?>"></script>
    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('../js/functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/uploader.js?rev=<?php echo rev('../js/uploader.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/colour-ring-box.js?rev=<?php echo rev('../js/colour-ring-box.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/dropit.js?rev=<?php echo rev('../js/dropit.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('../js/roller.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/main.js?rev=<?php echo rev('../js/main.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/includes/modules.js.php?rev=<?php echo rev('includes/modules.js.php'); ?>"></script>
<?php
if ($insert_allowed>-1) {
?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/geojson.min.js"></script>
    <!--<script type="text/javascript" src="http://<?php echo URL ?>/js/javascript.util.js"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/jsts.js"></script>-->
<?php
    include(getenv('OB_LIB_DIR').'upload_dynamic_js.php');
}

    if (file_exists('../includes/local_upload_javascripts.php'))
        include_once('../includes/local_upload_javascripts.php');
?>
</head>
<?php $page = (!empty($_SESSION['current_page'])) ? array_keys($_SESSION['current_page'])[0] : 'upload'; ?>
<body id='upload' class="<?= $page ?>">


<div id='holder'>
<?php

$upload_header_off = (defined('UPLOAD_HEADER_OFF') && UPLOAD_HEADER_OFF);

if ($insert_allowed>0 and $selected_form_type!='api' and $selected_form_type!='' && !$upload_header_off) {

    // form header
    $cmd = sprintf("SELECT description,form_name,ARRAY_TO_STRING(srid,',') as srid FROM project_forms WHERE form_id=%d",$insert_allowed);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_assoc($res);
    $form_title = $row['form_name'];
    $srid_array = explode(',',$row['srid']);
    $srid_options = array();
    foreach ($srid_array as $srid) {
        if (trim($srid)=='') continue;
        $srid_split = preg_split('/:/',$srid);
        if (count($srid_split)==1) {
            $srid_value = $srid_split[0];
            $srid_label = $srid_split[0];
        } else {
            $srid_value = $srid_split[0];
            $srid_label = $srid_split[1];
        }
        $srid_options[] = "<option value='$srid_value'>$srid_label</option>";
    }

    if (preg_match('/^str_/',$form_title))
        if (defined($form_title))
            $form_title = constant($form_title);

    if($row['description']!='') {
        $form_text = $row['description'];
        if (preg_match('/^str_/',$form_text))
            if (defined($form_text))
                $form_text = constant($form_text);
    } else $form_text = "";

    if ($selected_form_type=='web') $icon = '<span style="font-size:300%;color:#3BA3C4" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-table"></i></span>';
    elseif ($selected_form_type=='file') $icon = '<span style="font-size:300%;color:#3BA3C4;padding-top:2px" class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stack-1x fa-file"></i></span>';

    $ref =  sprintf('%s://%s/',$protocol,URL);
    echo "<div style='box-shadow: 0 3px 8px rgba(0,0,0,.24);position:relative;border-bottom:1px solid #acacac' id='upload-header'>
        <div style='background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAK0lEQVQImWO4devW/1u3bv1//PjxfxibAVkARjMgq4BJYFeJbubjx4//AwBde1m5G0ti6gAAAABJRU5ErkJggg==);position:relative;padding-bottom:5px'><a href='".$ref."upload/' title='".str_uploads."' class='uploads-home'>$icon</a><div style='font-size:150%;padding:10px 0 10px 90px;'>$OB_project_title: $form_title</div>";

    if (isset($_SESSION['Tid'])) {
        echo "<div style='padding-left:90px'>";
        
        echo "<div style='display:inline-block;padding-right:7px;vertical-align:top;border-right:1px solid #989898;margin-right:7px'><button id='save-state' class='pure-button button-secondary' style='vertical-align:top;height:34px' title='".t(str_save).', '.str_currentstate." (F3)'><i class='fa fa-fw fa-floppy-o fa-lg'></i></button></div>";

        echo "<div style='display:inline-block;vertical-align:bottom;margin-right:7px'>".str_last_save.": <a href='".$ref."index.php?profile&showiups#tab_basic'><span id='save_state' style='font-family:monospace'>--:--</span></a></div>";
        echo "<div id='reloaddiv' style='display:none'></div>";

        if ($selected_form_type=='file') {
            echo "<div style='display:inline-block;padding-left:7px;vertical-align:top;border-left:1px solid #989898;margin-right:7px'><input placeholder=\"".t(str_save).' '.str_columnorder."\" name='template_name' id='template_name' style='vertical-align:top;height:34px' value='";
            if (isset($_POST['column-assign-template']))
                echo $_POST['column-assign-template'];
            echo "'><button id='save-order' class='pure-button button-gray' style='height:34px;vertical-align:top'><i class='fa fa-fw fa-floppy-o fa-lg'></i></button></div>";
        }

        echo "</div>";
    } else {
        echo "<div style='padding-left:90px'>";
        echo "<div style='display:inline-block;padding-right:7px;vertical-align:top;border-right:1px solid #989898;margin-right:7px'><button class='pure-button button-passive'><i class='fa fa-fw fa-floppy-o fa-lg'></i></button></div>";
        echo "</div>";
    }

    if(!isset($_SESSION['Tid'])) $reglink = "<a href='".$ref."?login' id='login'><span class='navlabel'><i class='fa fa-sign-in fa-lg'></i> ".t(str_login)."</span></a>";
    else $reglink = "<a href='#' class='to'>".$_SESSION['Tname']."&nbsp;</a><ul class='subnav'><li><a href='".$ref."?profile'><span class='navlabel'><i class='fa fa-user fa-fw fa-lg'></i> ".t(str_profile)."</span></a></li><li><a href='".$ref."index.php?logout' id='logout'><span class='navlabel'><i class='fa fa-sign-out fa-fw fa-lg'></i> ".t(str_logout)."</span></a></li></ul>";

?>
    <div style='width:auto;top:15px;right:5px;position:absolute'>
    <div id='nav'>
     <ul class='topnav tablink'>
        <li><?php echo $reglink ?></li>
        <li><a href='#' class='to'><i class='fa fa-navicon'></i></a>
            <ul class='subnav'>
                <li><a href='<?php echo $ref ?>upload/'><i class='fa fa-upload fa-fw'></i> <?php echo t(str_upload) ?></a></li>
                <li><a href='<?php echo $ref ?>index.php?map'><i class='fa fa-globe fa-fw'></i> <?php echo t(str_map) ?></a></li>
                <li><a href='<?php echo $ref ?>index.php?database'><i class='fa fa-info fa-fw'></i> <?php echo t(str_summary) ?></a></li>
                <li><a href='<?php echo $protocol.'://'.OB_DOMAIN; ?>/projects/'><i class='fa fa-database fa-fw'></i> <?php echo t(str_dbs) ?></a></li>
            </ul></li>
        <li><a href='#' class='to'><span class="flag-icon flag-icon-<?= $_SESSION['LANG']; ?>"></span></a>
            <ul class="subnav">
            <?php
                foreach ($LANGUAGES as $L=>$label) {
                    //if ( file_exists(sprintf("%slanguages/%2s.php",getenv('PROJECT_DIR'),$L))) 
                        echo "<li><a href='?lang=$L' class='fl'><span class='flag-icon flag-icon-$L'></span>$label</a></li>";
                }
            ?>
            </ul>
        </li>
    </ul></div>
    </div>
   
        
<?php        

    echo "</div>";
        echo "</div>";
} else {
    include('../header.php.inc');
}

agree_new_terms();

// file and photo viewer DIV - module include here
if ($modules->is_enabled('photos')) {
    echo $modules->_include('photos','photo_viewer',['upload']);
}
?>

<div id='body'>
<div id="dialog" title="Dialog"><p id='Dt'></p></div>
<div id="dialog-confirm" title="<?php echo t(str_confirm) ?>"><p id='Dtc'></p></div>
<?php


if ($file_process) {
    // restore $_FILES on reload page
    if (count($_SESSION['keep_files'])) {
        $_FILES = $_SESSION['keep_files'];
        
        $data_dir = getenv('OB_LIB_DIR')."../uploads/datafiles";
        $i = 0;
        foreach ($_SESSION['file_ref'] as $ref) {
            file_put_contents(sprintf("%s",$_FILES['file']['tmp_name'][$i]), file_get_contents(sprintf("%s/%s_%s",$data_dir,$ref,$_FILES['file']['name'][$i]), false));
            $i++;
        }
    }
    // restore $_FILES on reload page
    if (!is_array($_FILES["file"]["tmp_name"])) {
        $_FILES['file']['name'] = array($_FILES['file']['name']);
        $_FILES['file']['type'] = array($_FILES['file']['type']);
        $_FILES["file"]["tmp_name"] = array($_FILES["file"]["tmp_name"]);
        $_FILES['file']['error'] = array($_FILES['file']['error']);
        $_FILES['file']['size'] = array($_FILES['file']['size']);
    }
    // restore $_FILES on reload page
    $_SESSION['keep_files'] = $_FILES;

    // oszlopok hozzárendlése mentett állapotból,
    // de itt lehetne egy automata előfeldolgozást csinálni szerver oldalon, hogy ami egyértelmű azt hozzárendelje a program
    if (isset($_SESSION['upload_column_assign']))
        unset($_SESSION['upload_column_assign']);

    //file based upload
    $fp = new file_process();
    $log = $fp->fprocess($file_type_as,$extra_file_arguments);
    if (is_array($log) and isset($log['comment'])) {
        $upload_comment = $log['comment'];
        $_SESSION['theader'] = $fp->theader;
        $_SESSION['feed_header'] = $fp->feed_header; // excel first line processing
    } else {
        $Error = $log;
        clean_upload_temp();
        unset($_SESSION['theader']);
    }
} 
elseif ($edit_process) {
    if (isset($_SESSION['upload_column_assign']))
        unset($_SESSION['upload_column_assign']);
    //file based upload
    if (1==1) {
        $upload_comment = 'Massive edit data by query';
    } else {
        $Error = $log;
        clean_upload_temp();
        unset($_SESSION['theader']);
        unset($_SESSION['sheetData']);
    }
}
else if ($geom_process) {
    //geometry based upload
    $gp = new geom_process();
    $log = $gp->gprocess();
    if (is_array($log) and isset($log['comment']))
        $upload_comment = $log['comment'];
    else $Error = $log;
}
else if ($interconnect_process) {
    // interconnect import
    if (isset($icfile) and isset($icref)) {
        restore_upload_temp($icfile,$icref);
        $_SESSION['theader'] = get_sheet_header();
    }
}

/* initial state - no query or
 * GET file
 * */
if ($form_chooser==1 and $insert_allowed==-1 and $selected_form_type=='') {
   $upload_question = str_more_uploadform_options."!";
   // custom upload message
   if (defined('str_upload_description_paragraph'))
       $upload_description_text = str_upload_description_paragraph;
   else
       $upload_description_text = "";
    
    /* LOAD?... */
    /*if (isset($_SESSION['column-assign-template'])) {
        $cot = "&column-assign-template=".$_SESSION['column-assign-template'];
    } else $cot = '';*/

    $list = array();
    //egy id-hoz tartozó formok felsorolása
    if($selected_form_id==-1) {
        $r = form_choose_list($selected_form_id);
        foreach($r as $id){
            $row = form_element($id);
            $list[] = format_form_list($id,$row['form_name'],$row['ft'],'');
        }
        if($modules->is_enabled('form_choose')) {
            $list = $modules->_include('form_choose','form_list',[],true);
        }

    } else {
        //?
        $list[] = form_choose_list('');
    }
    clean_upload_temp();
    unset($_SESSION['upload_file']);
    unset($_SESSION['sheetDataPage']);
    unset($_SESSION['sheetDataPage_counter']);
    unset($_SESSION['theader']);
    unset($_SESSION['upload']['loadref']);
    unset($_SESSION['upload_column_assign']);

} // form choice

/* Többféle form kérdések ha van lista
 *
 * */
if ($insert_allowed==-1 and isset($list) and count($list) and $selected_form_id==-1) {
    echo "<div style=';margin-top:25px;line-height:1;font-size:120%'>
        <div style='max-width:600px;text-align:justify'>$upload_description_text</div>
        <h2>$upload_question</h2>
        <ul class='fa-ul' style='padding-left:50px;padding-top:20px'>";
    foreach($list as $l)
        echo "<li style='margin-bottom:10px'>$l</li>";
    echo '</ul></div>';
} 


/* if the user chosed file upload form
 * choose file -> read file -> show table
 * */
if ($selected_form_type=='file' and $selected_form_id>0 and $insert_allowed==-1) {

    $_SESSION['keep_files'] = array();

    $_SESSION['upload']['selected_form_id'] = $selected_form_id;
    $_SESSION['upload']['selected_form_type'] = $selected_form_type;

    echo '<form method="post" enctype="multipart/form-data" class="pure-form pure-form-stacked" style="font-size:125%">
            <fieldset>
                <h2 style="margin-left:0px !important">'.t(str_data_upload_from_file).' <br>(csv, dsv, gpx, ods, shape, tsv, xls, xlsx, sqlite, fasta, json):</h2>';
    echo '
        <label for="file">'.t(str_choose_file).':</label>
            <input type="file" name="file[]" id="file" multiple=multiple class="multi pure-button button-secondary pure-u-1-2">
        <label for="file">URL:</label>
            <input name="url" id="url" class="pure-u-1-2">';

    if (isset($_SESSION['Tid'])) {
        // column assign template
        $cmd = sprintf("SELECT name FROM settings_import WHERE project_table=%s AND user_id=%d",quote(PROJECTTABLE),$_SESSION['Tid']);
        $res = pg_query($BID,$cmd);
        echo t(str_columnorder).' '.str_template.": <select name='column-assign-template' class='pure-u-1-2'><option label='--'></option>";
        $p =  sprintf($protocol.'://%s/upload/',URL);
        while($row=pg_fetch_assoc($res)) {
            $name = (defined($row['name'])) ? t(constant($row['name'])) : $row['name'];
            echo "<option value='{$row['name']}'>$name</option>";
        }
        echo "</select>";

        //interconnects
        if (grst(PROJECTTABLE,'master')) {
            $cmd = sprintf("SELECT table_name,to_char( datum, 'DD-MON-YYYY HH24:MI') AS date FROM system.temp_index WHERE interconnect='t' AND table_name ~ 'upload_%s_[A-Za-z0-9]+_wk_[A-Za-z0-9]+'",PROJECTTABLE);
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                echo t(str_interconnect_posted_data).": <select name='iclist' id='iclist'><option></option>";
                while ($row = pg_fetch_assoc($res)) {
                    $key = '';
                    if (preg_match("/upload_".PROJECTTABLE."_[A-Za-z0-9]+_wk_([A-Za-z0-9]+)/",$row['table_name'],$m))
                        $key = $m[1];
                    $cmd = sprintf('SELECT remote_project FROM interconnects_slave WHERE accept_key=\'%s\' AND pending=\'f\'',$key);
                    $res2 = pg_query($BID,$cmd);
                    $row2 = pg_fetch_assoc($res2);

                    echo sprintf("<option value='%s'>%s %s</option>",$row['table_name'],$row2['remote_project'],$row['date']);
                }
                echo "</select>";
            }
        }
    }

    // Bóné Gábor OBSERVADO modulja
    // ezt a tömböt egyéni paraméterezéssel lehet majd vele létrehozni
    $options = ($modules->is_enabled('custom_filetype')) 
        ? $modules->_include('custom_filetype','option_list',[],true) 
        : array('','CSV','FASTA','SHEETS');

    echo '<div style="width:50%;margin:10px 0 20px 30px">';

    echo sprintf("%s <select name='file_type_as' class='pure-u-1-2'>%s</select>",str_read_file_as, implode(array_map(function($a) { return "<option>$a</option>"; },$options)));
    $s = array();
    unset($_SESSION['field_separator']);
    unset($_SESSION['character_encoding']);
    if (isset($_SESSION['field_separator'])) $s[] = "s:".$_SESSION['field_separator'];
    if (isset($_SESSION['character_encoding'])) $s[] = "c:".$_SESSION['character_encoding'];
    if (isset($_SESSION['feed_header_line'])) $s[] = "h:".$_SESSION['feed_header_line'];
    if (isset($_SESSION['decimal'])) $s[] = "d:".$_SESSION['decimal'];
    echo sprintf("%s <input class='pure-u-1-2' name='extra_file_arguments' placeholder='c:utf8 s:, d:. h:yes' value='%s'>",str_file_processing_extra_arguments, implode(' ',$s));
    echo '</div>';

    echo '<button type="submit" id="file-upload-submit" name="submit" class="pure-button button-xlarge pure-u-1-2" disabled style="height:3em"><i class="fa fa-upload"></i> '.str_upload.'</button>
    <input type="hidden" name="token" value="">
    </fieldset></form>';
}


/* default route
 *
 * */
if($insert_allowed>0 and $selected_form_type!='api' and $selected_form_type!='') {

    #if($_SESSION['upload']['selected_form_type']=='web') include(getenv('OB_LIB_DIR').'upload_show-web-form.php');
    #elseif($_SESSION['upload']['selected_form_type']=='file') include(getenv('OB_LIB_DIR').'upload_show-file-form.php');
    if ($selected_form_type=='web') $table_type = 'web';
    elseif ($selected_form_type=='file') $table_type = 'file';
    
    // how many rows displayed per page
    if (!isset($_SESSION['perpage']))
        $_SESSION['perpage'] = 20;
    
    include(getenv('OB_LIB_DIR').'upload_show-table-form.php');
}

/* Error messages
 *
 * */
if ($Error!='') {
    printf("<div class='log'>%s:<div>%s</div></div>",t(str_messages),$Error);
    $Error = '';
}
?>

</div><!--/body-->

<!-- colour rings box -->
<?php
$colors = array('red','pink','green','lightgreen','orange','yellow','blue','lightblue','white','black','brown','purple','violet','metal');
$codes = array('R','P','G','g','O','Y','B','b','W','K','N','U','V','M');
$textcolors = array('K','K','K','K','K','K','W','K','K','W','W','W','K','K');
$custom_codes = array();
$custom_colors = array();

// It is coming from the form settings
// e.g.
//$available_ring_colors = "[XX],blue:B,red:B";
if (isset($available_ring_colors) and count($available_ring_colors)!=0) {
    $arc = array_keys($available_ring_colors);
    if (preg_match('/^\[(.+)\]$/',$arc[0],$m)) {
        array_shift($available_ring_colors);
        $ring_position_pattern = $m[1]; 
    }
    foreach ($available_ring_colors as $cc=>$clabel) {
        $label = strtolower($clabel[0]);
        $custom_colors[] = $label;
        if ($cc!='')
            $custom_codes[] = $cc;
        else {
            $ci = array_search($label,$colors);
            $custom_codes[] = $codes[$ci];
        }
    }
}

$li = array();
for($i=0;$i<count($colors);$i++) {

    $dca = array($colors[$i],$codes[$i],$textcolors[$i]);
    
    if (count($custom_codes)) {
        $index = array_search($dca[0],$custom_colors);
        if ($index===false) continue;
        if (isset($custom_codes[$index]))
            $dca[1] = $custom_codes[$index];
    }
    $title = t(constant('str_'.$dca[0]));
    if ($dca[0]=='metal') $dca[0] = 'silver';
    if ($dca[2]=='K') $textcolor = '#000';
    else $textcolor = '#fff';
    $li[] = "<li style='background-color:$dca[0];color:$textcolor' data-color='$dca[1]' data-text='$dca[2]' title='$title'>$dca[1]</li>";
}

?>

<div id="colourringdiv">
    <div style='display:table;border-spacing:2px'>

        <div style='display:table-row;'>
            <div style='display:block;text-align:right;background-color: #b3cee6;margin-top: -7px;margin-left: -7px;margin-right: -7px;'>
                <button id='colourringdiv-close' class='pure pure-button button-gray' style='height:30px'><i class='fa fa-close fa-lg'></i></button>
            </div>
        </div>

        <div style='display:table-row'>
            <div style='display:table-cell;vertical-align:top;background: repeating-linear-gradient( 45deg,  #888,  #888 3px,  #aaa 3px,  #aaa 6px);width:100%'>
            <?php
                //$default_colours = array('red:R:K,','pink:P:K','green:G:K','lightgreen:g:K','orange:O:K','yellow:Y:K','blue:B:W','lightblue:b:K','white:W:K','black:K:W','brown:N:W','purple:U:W','violet:V:K','metal:M:K');
                if (isset($ring_position_pattern)) 
                    echo "<input type='hidden' value='$ring_position_pattern' id='ring_position_pattern'>";
                else
                    //two rings on each position
                    echo "<input type='hidden' value='XX' id='ring_position_pattern'>"; ?>
                <ul class='ring-box ring'><?php echo implode('',$li); ?></ul>
            </div>
        </div>

        <div>
            <div id='legbox'>
                <div class='left'>
                    <ul id="leg-left-top" class="ring-position ring"></ul>
                    <ul id="leg-left-bottom" class="ring-position ring"></ul>
                    <span id='leftlabel' class='leftrightlabel'><?php echo str_left?></span>
                </div>
                <div class='right'>
                    <ul id="leg-right-top" class="ring-position ring"></ul>
                    <ul id="leg-right-bottom" class="ring-position ring"></ul>
                    <span id='rightlabel' class='leftrightlabel'><?php echo str_right?></span>
                </div>
            </div>
            <div style='float:right'>
                <button id='rotate' class='pure pure-button button-gray' style='width:40px;'><i class='fa fa-refresh fa-lg fa-rotation'></i></button>
            </div>
        </div>

        <div>
            <div><input id='cr_code' style='border:none;background:transparent'></div>
            <div id='ring-text-div'><?php echo str_ring_label?>: <input id='ring-text' class='pure-input pure-u-1' data-text='' data-color=''></div>
            <div><button class='pure-button button-success pure-u-1' id='addcolourings'><?php echo str_set?></button></div>
            <input type='hidden' id='colour-ring-referenceid'>
        </div> 
    </div>
</div> <!-- colourringdiv -->

<?php
# bug report
if (isset($_SESSION['Tid']) and defined("AUTO_BUGREPORT_ADDRESS")) {
    echo "<div id='bugreport' title='".str_report_an_error."' style='background:url($protocol://".URL."/images/Diaperis_boleti2.png);'></div>";
    $included_files = json_encode(get_included_files());
    $defined_vars = json_encode(get_defined_vars());

    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $_SESSION['openssl_ivs']['eincf'] = $iv;
    $encrypted_included_files = base64_encode(openssl_encrypt($included_files, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));
    $iv = openssl_random_pseudo_bytes($ivlen);
    $_SESSION['openssl_ivs']['edefv'] = $iv;
    $encrypted_defined_vars = base64_encode(openssl_encrypt($defined_vars, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));

    echo "<div id='bugreport-box'>
    <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:0px'>".str_report_an_error."</h2>
    <div style='padding:5px;text-align:center'>
    <button id='bugreport-cancel' style='position:absolute;top:0;right:0' class='pure-button button-large button-passive'><i class='fa fa-close'></i></button>
    <form class='pure-form pure-u-1'>
        <input type='hidden' id='page' value='upload.php'>
        <input type='hidden' id='eincf' value='$encrypted_included_files'>
        <input type='hidden' id='edefv' value='$encrypted_defined_vars'>

        <fieldset class='pure-group'>
            <input type='text' id='issue-title' maxlength='32' class='pure-u-1' placeholder='".str_subject."'>
            <textarea id='issue-body' style='min-height:10em' class='pure-u-1' placeholder='".str_bug_description."'></textarea>
        </fieldset>
        <button id='bugreport-submit' class='pure-button button-large button-warning pure-u-2-3'>".str_send." <i class='fa fa-lg fa-bug'></i></button>
        
    </form>
    </div>
</div>";
}

include('../footer.php.inc');

?>
</div><!--/holder-->
</body>
</html>
