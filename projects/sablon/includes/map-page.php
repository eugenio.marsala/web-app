<?php

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $m = new modules();

    $filter = "";

    //grid view module
    $filter .= $m->_include('grid_view','print_box');

    //taxon and other filters
    $filter .= $m->_include('text_filter','print_box');

    //advanced taxon and other filters => text_filter2
    $filter .= $m->_include('text_filter2','print_box');

    //selections
    //'box_load_selection','shared_geometries'
    $filter .= $m->_include('box_load_selection','print_box');

    //last data
    $filter .= $m->_include('box_load_last_data','print_box');

    // custom box
    $filter .= $m->_include('box_custom','print_box');

    $button = 0;
    foreach ($m->which_has_method('query_button') as $module)  {
        $button++;
    }

    if ($button) {

        $filter .= "<script>

        var geometry_query_obj;
        var geometry_query_neighbour = 0;
        var geometry_query_session = 0;
        var geometry_query_selection = 0;
        var skip_loadQueryMap_customBox = 0;
        var text_filter_enabled = 0;
        var last_data_enabled = 0;

        /* Text query with text filter box module 
         * */
        $('#mapfilters').on('click','#sendQuery',function(){
            $('#navigator').hide();
            //drawLayer.destroyFeatures();            
            markerLayer.clearMarkers();
            $('#navigator').hide();

            if (text_filter_enabled) {
                myQVar = getParamsOf_text_filter();
            }
            
            // geometry selection
            if (geometry_query_session!=0) {
                myQVar.geom_selection = \"session\";
                myQVar.neighbour = geometry_query_neighbour;
                myQVar.ce = JSON.stringify(geometry_query_obj);
            }
            
            // box load last data
            /* modules/box_load_selection.php
             * Box filter
             * query last data
             * last upload, last 10 rows, ...
             * */
            if (last_data_enabled) {
                var qname = ''
                qname = $('#last_data_query_option').val();
                if (qname != '')
                    myQVar['qids_' + qname] = 'last_data';

            }

            // shared polygons
            if (geometry_query_selection!=0) {
                myQVar.geom_selection = 'shared_polygons';
                myQVar.spatial_function = $('#spatial_function').val();
            }

            if ($('#ignoreJoins').prop('checked') === true)
                var ignoreJoins = $('#ignoreJoins').val();
            else
                var ignoreJoins = 0;

            myQVar.ignoreJoins = ignoreJoins;

            if ( skip_loadQueryMap_customBox ) {

                var customBox_elements = $('.custom_box');
                var myLQVar = {} // local myQVAR

                $.when.apply($, customBox_elements.map(function(item) {
                    var custom_box_name = $(this).val();
                    var custom_function_name = 'custom_' + custom_box_name;
                    var oVar = window[custom_function_name]();
                    myLQVar = {...myQVar, ...oVar }
                })).then(function() {
                    // create map with custom box parameters
                    loadQueryMap(myLQVar);
                    // all ajax calls done now
                });
            }

            // create map
            if (skip_loadQueryMap_customBox==0) {
                loadQueryMap(myQVar);
            }
        });
</script>";
        
        // this is not a text filter module thing!!!!
        $sqla = sql_aliases(1);
        if ($sqla['joins'])
            $ignore_joins_tr = "<tr><td colspan=2 class='title'><input id='ignoreJoins' value=1 type='checkbox' style='vertical-align:bottom'> ignore table JOINS</td></tr>";
        else
            $ignore_joins_tr = "<tr><td colspan=2><input id='ignoreJoins' value=0 type='hidden'></td></tr>";


        $filter .= "<table class='mapfb'>
            <tr><td colspan=2 class='title'><button class='button-gray button-success pure-button pure-u-1' style='font-size: 200%;' id='sendQuery'><i class='fa-search fa'></i> ".str_query."</button></td></tr>
            <tr><td colspan=2 class='title'><button id='clear-filters' class='pure-button button-href' style='float:right'><i class='fa fa-eraser'></i> ".str_clear_filters."</button></td></tr>
            $ignore_joins_tr
            </table>";
            
        //$t->cell("<button id='select_with_shared_geom' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
        //$t->cell("<button id='last_data_query' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
    }

    
    if(defined('ZOOM_WHEEL_ENABLED') and constant("ZOOM_WHEEL_ENABLED")!='false') {
        $zw = 'on';
        $zwpass = 'button-success';
    } else {
        $zw = 'off';
        $zwpass = 'button-passive';
    }
    if (isset($_COOKIE[ "wz" ])) {
        if ($_COOKIE[ "wz" ]==='true') {
            $zw = 'on';
            $zwpass = 'button-success';
        } else {
            $zw = 'off';
            $zwpass = 'button-passive';
        }
    }
    
    if(defined('DEFAULT_VIEW_RESULT_METHOD')) {
        $default_view_result_method = constant("DEFAULT_VIEW_RESULT_METHOD");
    } else {
        $default_view_result_method = 'list';
    }

    $buff = CLICK_BUFFER;

    //<img src='http://localhost/cgi-bin/mapserv?LAYER=dinpi_points&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&MAP=/var/www/html/biomaps/projects/dinpi/private/private.map'>
    if (SHOW_LEGEND and isset($_SESSION['ms_layer'])) {
        $legend = "<div id='legend'>
            <img src='$protocol://".PRIVATE_MAPSERV."?LAYER={$_SESSION['ms_layer']}&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&MAP=PMAP'></div>";
    } else 
        $legend = "";

    $out = "
    <div id='mapholder'>
    <div class='mapholder-cell'>$legend
        <div id='map' class='maparea'></div>
        <div id='mapcontrols'>
            <div class='mcbox'>".str_mouse_zoom.": <button id='wz' class='pure-button $zwpass button-small'><i class='fa fa-lg fa-toggle-$zw'></i></button></div>
            <div class='mcbox'> <button id='reset-map' class='button-small pure-button button-href'><i class='fa fa-refresh'></i> ".str_reset_map."</button></div>
            ";

    $out .= "<div class='mcbox' style='position:relative'>";
    $out .= "   <div style='display:none;position:absolute;top:24px;width:400px' id='spatial-query-settings-box'>
                    <div class='mcbox pure-form'>
".str_select_buffer_size.": <input style='vertical-align:middle' class='pure-input' type='range' id='bufferslide' min='0' max='1000' value='$buff'>&nbsp;<input type='text' id='buffer' value='$buff' style='width:5em;border:1px solid lightgray;padding:4px;border-radius:3px;max-height:26px'> ".str_meter."</div>
                </div>";

    $out .= "<span style='display:inline-block'>&nbsp;".mb_convert_case(str_spatial_query, MB_CASE_UPPER, "UTF-8").":&nbsp;</span>";
    if ($buff == 0) {
        $pointToggle = "disabled";
        $pointToggle_display = "none";
    }
    else {
        $pointToggle = "";
        $pointToggle_display = "block";
    }

    if ($m->is_enabled('text_filter') || $m->is_enabled('text_filter2')) {
        $out .= "<label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='type' value='selectPoint' id='pointToggle' $pointToggle class='maphands' onclick='toggleControl(this);' onLoad='toggleControl(this);' />
                    <span id='pointToggle_icon' style='display:$pointToggle_display;background-image:url(css/img/draw_point_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='type' value='selectLine' id='lineToggle' class='maphands' onclick='toggleControl(this);' />
                    <span style='display:block;background-image:url(css/img/draw_line_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='type' value='selectPolygon' id='polygonToggle' class='maphands' onclick='toggleControl(this);' />
                    <span style='display:block;background-image:url(css/img/draw_polygon_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom;' id='spatial-query-settings' title='show settings'>
                    <span class='pure-button button-href' style='width:22px;height:22px;padding:0'><i class='fa fa-cog fa-lg' style='vertical-align:-25%'></i></span>
                </label>

                </div>";
    }
    
    $out .= "<div class='mcbox'>";
    $out .= "<span style='display:inline-block'>&nbsp;".mb_convert_case(str_navigation, MB_CASE_UPPER, "UTF-8").":&nbsp;</span>";
    $out .= "<label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='type' value='zoomArea' id='zoomToggle' class='maphands' onclick='toggleControl(this);' />
                    <span style='display:block;background-image:url(css/img/zoom_box_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='type' value='none' id='noneToggle' class='maphands' onclick='toggleControl(this);' checked='checked'/>
                    <span style='display:block;background-image:url(css/img/pan_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label></div>";

    //a subset of the main cols, depending on user's access rights
    $out .= $m->_include('identify_point','print_button');

    $out .= "<div class='mcbox'>".str_apply_text_query.":&nbsp;<button class='pure-button button-passive button-small' id='apptq'><i class='fa fa-lg fa-toggle-off'></i></button></div>";
    if (!$m->is_enabled('box_load_coord'))
        $out .= "<div class='mcbox'>".str_position.": <div id='mouse-position'></div></div>";
    $out .= "<div class='mcbox'><button title='Show my position' id='show-my-position' class='pure-button'><i class='fa fa-lg fa-map-marker' style='color:#59c3e8'></i></button></div>";
    // show coordinates module
    if ($m->is_enabled('box_load_coord'))
        $out .= "<div class='mcbox'>".$m->_include('box_load_coord','print_box')."</div>";
    
    $out .= "</div>";
    
    // results-area
    $out .= "<div id='results-area'>
        <div id='error'></div>
        <div id='matrix'></div>";

    $out .= '<script>var default_view_result_method = "'.$default_view_result_method.'";</script>';
    // rollable results view area
    $out .= '<div id="drbc" style="position:relative;margin-left:20px"><div id="dynamic-results-block" style="position:absolute;display:inline-block;z-index:2">';
    $out .= '      <div id="scrollbar-header" style="display:none"></div>';
    $out .= '      <div id="scrollbar" class="scrollbar" style="display:none"></div>';
    $out .= '</div></div>';
    $out .= '</div>';
    // results-area-end

    // results-area-end

    $out .= "</div>"; // mapcontrols

    $out .= "<div class='mapholder-cell' id='mapfilters'>
        <div id='smallheader'></div>";

    $cmd = sprintf("SELECT main_table FROM project_queries WHERE project_table='%s'",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $main_tables = array('');
    while ($row = pg_fetch_assoc($res) ) {
        if (defined('str_'.$row['main_table']))
            $main_tables[] = constant('str_'.$row['main_table']).'::'.$row['main_table'];
        else
            $main_tables[] = $row['main_table'];
    }
    if (!isset($_SESSION['current_query_table'])) {
        $_SESSION['current_query_table'] = PROJECTTABLE;
    }

    $options = selected_option($main_tables,$_SESSION['current_query_table']);

    if (!isset($_GET['query_api']) and !isset($_GET['query'])) {
        $out .= "
        <div class='mapfb' style='border:1px solid #eaeaea'>
            <div class='title center'>".str_query_table."</div>
            <div class='content'><select style='text-align:center;font-weight:bold' class='pure-u-1' id='current_query_table'>$options</select></div>
        </div>";
        $out .= $filter;
    }
    $out .= "<!--w1--></div><!--w2--></div>";
?>
