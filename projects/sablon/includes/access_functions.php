<?php

/* Return with a value between 0-3
 * 0 non logined
 * 1 --- 
 * 2 logined and project member
 * 3 logined and (founder or admin group member)
 * */
function prst() {
    global $BID;

    //non-logined
    $rst = 0;

    //logined
    if (isset($_SESSION['Tid']))
        $rst = 1;
    
    if ($rst==1) {
        $acc_cached = obm_cache('get',"prst");
        if ($acc_cached!==FALSE) {
            return $acc_cached;
        }

        // check user is founder or maintainer
        if (grst(PROJECTTABLE,'master')) {
            $rst = 3;
        }
        else {
            // check project member
            $cmd = sprintf("SELECT 1 as \"enabled\" FROM project_users WHERE project_table='%s' AND user_id=%d  AND user_status::varchar IN ('1','2','3','4','normal','master','operator','assistant')",PROJECTTABLE,$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)){
                $row = pg_fetch_assoc($res);
                if($row['enabled']=='1') 
                    $rst = 2;
            }
        }
        obm_cache('set',"prst",$rst,300);
    }
    
    return $rst;
}

/* query current restrictions related with the project definitions and login state
 * Check relation to the Project's general rules
 * ACC_LEVEL and MOD_LEVEL defined in local_vars.php
 *
 * */
function rst($acc,$c='',$table=PROJECTTABLE) {
    global $ID,$BID;

    /*  acc:    read or write access check: 'acc' 'mod'
        c:      a data id
        table:  a project_table

        this function return TRUE if the current access level >= than the function' restriction level
    
        ACC and MOD LEVELS:
        0/public = Open project:              Accessible for Everybody
        1/login = Closed project:             Accessible for Logined Members
        2/group = Paranoid project:           Accessible for specified group members

    */

    $ACC_LEVEL = ACC_LEVEL;
    $MOD_LEVEL = MOD_LEVEL;

    // RIGHTS FOR READING DATA 
    if ($acc=='acc' and ( "$ACC_LEVEL" == '0' or "$ACC_LEVEL" == 'public' )) {
        //ACCESSIBLE DATA FOR EVERYBODY
        return TRUE;
    }
    elseif ($acc=='acc' and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == 'login' )) {
        //ACCESSIBLE DATA FOR LOGINED USERS
        if(!isset($_SESSION['Tid'])) return FALSE;

        //if(isset($_SESSION['Tid'])) 
        //    return TRUE;

        $acc_cached = obm_cache('get',"rst.$acc");
        if ($acc_cached!==FALSE) {
            if ($acc_cached == 'TRUE')
                return TRUE;
            else
                return FALSE;
        }

        //check founder, project_managers and maintainers
        if (grst(PROJECTTABLE,'master')) {
            return TRUE;
        }

        $cmd = sprintf("SELECT 1 as \"enabled\" FROM project_users WHERE project_table='%s' AND user_status::varchar IN ('1','2','3','4','normal','master','operator','assistant') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)){
            $row = pg_fetch_assoc($res);
            if($row['enabled']=='1') {
                obm_cache('set',"rst".$acc,'TRUE',300);
                return TRUE;
            }
        }
        
        obm_cache('set',"rst".$acc,'FALSE',90);
        return FALSE;
    }
    elseif ($acc=='acc' and ( "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group' )) {

        //ACCESSIBLE DATA FOR SPECIFIED GROUPS

        if (isset($_SESSION['Tid'])) {
            //check founder, project_managers and maintainers
            if (grst(PROJECTTABLE,'master'))
                return TRUE;
    
            $acc_cached = obm_cache('get',"rst$acc$table$c");
            if ($acc_cached!==FALSE) {
                if ($acc_cached == 'TRUE')
                    return TRUE;
                else
                    return FALSE;
            }


            // parked users get false allways
            $cmd = sprintf("SELECT 1 as \"banned\" FROM project_users WHERE project_table='%s' AND user_status::varchar IN ('0','banned') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if($row['banned']=='1') { 
                    obm_cache('set',"rst".$acc.$table.$c,'FALSE',300);
                    return FALSE;
                }
            }

            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='')
                $login_groups = $_SESSION['Tgroups'];
            else
                $login_groups = 0;

            
            if ($_SESSION['st_col']['RESTRICT_C'] and $c!='') {
                
                $cmd = sprintf('SELECT CASE WHEN (read && ARRAY[%s] OR sensitivity::varchar IN (\'0\',\'public\')) THEN 1 ELSE 0 END FROM %s_rules WHERE "row_id"=%d AND "data_table"=%s',$login_groups,PROJECTTABLE,$c,quote($table));
                $res = pg_query($ID,$cmd);
                if (!pg_num_rows($res)) {
                    // no rules for data
                    obm_cache('set',"rst".$acc.$table.$c,'TRUE',300);
                    return true;
                }
                $data_acc_row = pg_fetch_assoc($res);
                
                if ($data_acc_row['case'] == 1) {
                    // ok
                    obm_cache('set',"rst".$acc.$table.$c,'TRUE',300);
                    return true;
                } 
            } elseif ($c!='') {
                    // if there is no rules for data lines lets see who is the uploader
                    // it is important because the base policy is FALSE, see below
                    $cmd = sprintf('SELECT uploader_id FROM system.uploadings LEFT JOIN %1$s ON (obm_uploading_id=uploadings.id) WHERE "%1$s".obm_id=\'%2$d\'',$table,$c);
                    $res = pg_query($ID,$cmd);
                    if ($row = pg_num_rows($res)) {
                        if ($_SESSION['Trole_id'] == $row['uploader_id']) {
                            obm_cache('set',"rst".$acc.$table.$c,'TRUE',300);
                            return true;
                        }
                    }
            }
        } else {
            $acc_cached = obm_cache('get',"rst$acc$table$c",'','',FALSE);
            if ($acc_cached!==FALSE) {
                if ($acc_cached == 'TRUE')
                    return TRUE;
                else
                    return FALSE;
            }

            // rules for non logined users
            if ($c!='' and $_SESSION['st_col']['RESTRICT_C'] ) {
                $cmd = sprintf('SELECT sensitivity FROM %s_rules WHERE row_id=%d AND "data_table"=%s',PROJECTTABLE,$c,quote($table));
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $data_acc_row = pg_fetch_assoc($res);
                    if ($data_acc_row['sensitivity'] == 0 or $data_acc_row['sensitivity'] == 'public') {
                        obm_cache('set',"rst".$acc.$table.$c,'TRUE',3600,FALSE);
                        return true;
                    }
                } else {
                    // no rules for data - default action is accessible
                    obm_cache('set',"rst".$acc.$table.$c,'TRUE',3600,FALSE);
                    return true;
                }
            }
        }
        //default non handled cases
        obm_cache('set',"rst".$acc.$table.$c,'FALSE',300);
        return false;
    }
    
    if ($acc=='mod' and ( "$MOD_LEVEL" == '2' or "$MOD_LEVEL" == 'group' )) {

        if(!isset($_SESSION['Tid'])) return FALSE;

        //MODIFIBLE DATA FOR SPECIFIED GROUPS
        //
        $mod_cached = obm_cache('get',"rst$acc$table$c");
        if ($mod_cached!==FALSE) {
            if ($mod_cached == 'TRUE')
                return TRUE;
            else
                return FALSE;
        }

        //check founder, project_managers and maintainers
        if (grst(PROJECTTABLE,'master'))
            return TRUE;

        if (isset($_SESSION['Tid'])) {

            // parked users get false allways
            $cmd = sprintf("SELECT 1 as \"banned\" FROM project_users WHERE project_table='%s' AND user_status::varchar IN ('0','banned') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if($row['banned']=='1') { 
                    obm_cache('set',"rst".$acc,'FALSE',900);
                    return FALSE;
                }
            }

            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='')
                $login_groups = $_SESSION['Tgroups'];
            else
                $login_groups = 0;

            
            if ($_SESSION['st_col']['RESTRICT_C'] and $c!='') {

                $cmd = sprintf('SELECT CASE WHEN (write && ARRAY[%s]) THEN 1 ELSE 0 END FROM %s_rules WHERE "row_id"=%d AND "data_table"=%s',$login_groups,PROJECTTABLE,$c,quote($table));
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $data_acc_row = pg_fetch_assoc($res);
                    if ($data_acc_row['case'] == 1) {
                        // ok
                        obm_cache('set',"rst".$acc.$table.$c,'TRUE',300);
                        return true;
                    } 
                }
            } elseif (!$_SESSION['st_col']['RESTRICT_C'] and $c!='') {
                // group level project without _rules table
                $cmd = sprintf('SELECT uploader_id FROM system.uploadings LEFT JOIN %1$s ON (obm_uploading_id=uploadings.id) WHERE "%1$s".obm_id=\'%2$d\'',$table,$c);
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    if ($_SESSION['Trole_id'] == $row['uploader_id']) {
                        obm_cache('set',"rst".$acc.$table.$c,'TRUE',300);
                        return true;
                    }
                }
            } 
        } 
        obm_cache('set',"rst".$acc.$table.$c,'FALSE',300);
 
    } elseif ($acc=='mod' and ( "$MOD_LEVEL" == '1' or "$MOD_LEVEL" == 'login') ) {
        // EDITABLE DATA FOR non disabled logined users
        if(!isset($_SESSION['Tid'])) return FALSE;

        //check founder, project_managers and maintainers
        if (grst(PROJECTTABLE,'master'))
            return TRUE;

        $cmd = sprintf("SELECT 1 as \"enabled\" FROM project_users WHERE project_table='%s' AND user_status::varchar IN ('1','2','3','4','normal','master','operator','assistant') AND user_id=%d",PROJECTTABLE,$_SESSION['Tid']);
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)){
            $row = pg_fetch_assoc($res);
            if($row['enabled']=='1') 
                return TRUE;
        }

    }
    elseif ($acc=='mod' and ( "$MOD_LEVEL" == '0' or "$MOD_LEVEL" == 'public')) {
        // EDITABLE DATA FOR EVERYBODY
        return TRUE;
    }

    // default 
    return FALSE;
}

/* Query project founder/maintainers
 * Typically for administrative function's access check
 * access: 3 (sub-maintainer: user_status:3)
 * access: 4 (maintainers: user_satuts:4)
 * Return: TRUE/FALSE
 * */
function grst ($table,$access) {
    global $BID;

    $status = obm_cache('get',"grst".$access);
    if ($status !== FALSE ) {
        if ($status == 'TRUE')
            return TRUE;
        else
            return FALSE;
    }

    if ($access == 'master') $access = 4;
    elseif ($access == 'operator') $access = 3;
 
    if (!is_numeric($access)) return;
   
    if (isset($_SESSION['Tid'])) {
        # if no groups defined but the user is the founder
        $cmd = sprintf("SELECT 1 FROM projects WHERE admin_uid='%d' AND project_table='%s'",$_SESSION['Tid'],$table);
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            
            obm_cache('set',"grst".$access,'TRUE',900);
            return TRUE;
        } else {
            // project maintainers are super admin without any specified group memberships
            $cmd = sprintf("SELECT user_status FROM project_users WHERE user_id='%d' AND project_table='%s' AND user_status::varchar IN ('2','3','master','operator')",$_SESSION['Tid'],$table);
            $res = pg_query($BID,$cmd);
            if(pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                //if ($row['user_status']==1) {
                    //normal users
                    // query group admin level!!
                
                //}
                if ($row['user_status']==2 or $row['user_status']=='master') {
                    # mainteners have access to everything
                    obm_cache('set',"grst".$access,'TRUE',300);
                    return TRUE;
                }
                elseif (($row['user_status']==3 or $row['user_status']=='operator') and $access == '3') {
                    # sub-mainteners have less
                    obm_cache('set',"grst".$access,'TRUE',300);
                    return TRUE;
                }
            }
        }
    }
    obm_cache('set',"grst".$access,'FALSE',300);
    return FALSE;
}

?>
