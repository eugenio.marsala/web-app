<?php
/* Ennek semmi mást nem kéne csinálni csak a GML feldolgozását, amit egy json tömbbként ad vissza a $_SESSION['wfs_response'] 
 * változóban. Az echo-s sorok miatt ez kissé zavaros működésű és nehezen felhasználható többféle célra.
 * A merge részben nem működik a sok sor korlátozás
 * A korlátozott sorokat egyben is vissza kéne tudni adni, amikor fájl kimenet a kérés!
 * */

session_start();

/*These includes not open a new session*/
#xdebug_start_trace('/tmp/xdebug');
#require_once(getenv('OB_LIB_DIR').'system_vars.php.inc');
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
$post_id = array();
$post_data = array();
$post_geom = array();

if (isset($_POST['skip_processing']) and $_POST['skip_processing']=='yes') {
        // Read previously processed XML data
        #syslog(LOG_WARNING,"wfa-get-count:".count($_SESSION['wfs_full_array']));
        if (isset($_SESSION['wfs_full_array'])) {
            $post_id = $_SESSION['wfs_full_array']['id'];
            $post_data = $_SESSION['wfs_full_array']['data'];
            $post_geom = $_SESSION['wfs_full_array']['geom'];
        }
} elseif (isset($_POST['skip_processing']) and $_POST['skip_processing']=='no') {
        // Processing XML
        //
        $ses = session_id();
        $no_input = 1;

        //if (file_exists("/mnt/data/tmp/conts_$ses.xml")) {
        $list = glob(OB_TMP."conts_$ses*.xml");
        //log_action(OB_TMP."conts_$ses*.xml");
        $xml = false;
        $json = false;
        if (isset($_SESSION['loaded_xml_content'])) {
            //from load_queries
            $xml = simplexml_load_string($_SESSION['loaded_xml_content']); 
            unset($_SESSION['loaded_xml_content']);
            $no_input = 0;
        } elseif(count($list)) {
            natsort($list);
            $file = array_pop($list);
            $xml = simplexml_load_file($file);
            // A merge fájlkat nem lehet lementeni!!!
            // a mentés csak egy xml-t tárol egyszerre!!
            //if ($merge) unlink($file);
            $no_input = 0;
        } elseif (isset($_SESSION['loaded_geojson_content'])) {
            $json = json_decode($_SESSION['loaded_geojson_content']);
            unset($_SESSION['loaded_geojson_content']);
            $no_input = 0;
        }
        
        if ($no_input) {
            echo json_encode(array("res"=>'',"geom"=>'',"error"=>'Proxy error: no xml files to read'));
            exit;
        }
        if($xml ===  FALSE and $json === FALSE) {
            echo json_encode(array("res"=>'',"geom"=>'',"error"=>'Proxy error: failed to read xml contents'));
            exit;
        }
        if ($xml !== FALSE) {
            $namespaces = $xml->getDocNamespaces();
            //$xml->registerXPathNamespace("ms", $namespaces['ms']);
            //$xml->registerXPathNamespace("gml", $namespaces['gml']);
            //var_dump($namespaces);
            //var_dump($xml->asXML());
        
            /*Mapserver errors*/
            if(isset($namespaces['ows'])) {
                $e = "";
                foreach($xml->children($namespaces['ows']) as $child) {
                    foreach($child->children($namespaces['ows']) as $data){
                        if ($data->getName() == 'ExceptionText') {
                            $e .= (string) $data;
                        }   
                    }
                }
                $_SESSION['wfs_array'] = array("id"=>'',"geom"=>'',"data"=>'');
                echo json_encode(array("geom"=>'',"error"=>"mapserver exception: $e"));
                exit;
            }
            /*GML processing*/
            if(isset($namespaces['gml'])) foreach($xml->children($namespaces['gml']) as $child) {
                if (isset($namespaces['ms'])) foreach($child->children($namespaces['ms']) as $data){
                    foreach($data->children($namespaces['ms']) as $i){
                        if ($i->getName() == 'id') {
                            $post_id[] = (string) $i;
                        } else {
                            $var = $i->getName();
                            if (!isset($post_data[$var])) {
                                $post_data[$var] = array();
                            }
                            $post_data[$var][] = (string) $i;
                        }
                        foreach($i->children($namespaces['gml']) as $j){

                            if ($j->getName() == 'MultiPoint') {
                                $post_pgeom = array();
                                foreach(GML_coords_from_Mpoints($j,$namespaces) as $k){
                                    list($x,$y) = explode(' ',$k);
                                    $post_pgeom[] = array($y,$x);
                                }
                                $post_geom[] = array('linearRing'=>$post_pgeom);
                            }
                            if ($j->getName() == 'Point') {
                                list($x,$y) = explode(' ',GML_coords_from_points($j,$namespaces));
                                $post_geom[] = array('point'=>array($y,$x));
                            }
                            //echo $j->getName() . ":". GML_coords_from_points($j,$namespaces);
                            //echo "<br>";
                        }
                    }
                    //foreach($data->children($namespaces['gml']) as $i){
                        //echo $i->getName() . ":". GML_coords_from_bounds($i,$namespaces);
                        //echo "<br>";
                    //}
                }
            }
        } elseif ($json !== FALSE) {

            require_once(getenv('OB_LIB_DIR').'db_funcs.php');
            if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
                die("Unsuccessful connect to GIS database.");

            if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
                die("Unsuccessful connect to GIS database.");

            // JSON processing

            //push selected result into temp_ table
            pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));

            $columns = json_decode(json_encode($json->{'features'}[0]->{'properties'}),true);
            $c = array_keys($columns);
            
            $index = array_search('obm_id',$c);
            unset($c[$index]);

            //$index = array_search($_SESSION['st_col']['SPECIES_C'],$c);
            //unset($c[$index]);
            $typecmd = sprintf("SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '%s'",PROJECTTABLE);
            $res = pg_query($ID,$typecmd);
            $cols = array();
            $col_types = array();
            while ($row = pg_fetch_assoc($res)) {
                $index = array_search($row['column_name'],$c);
                if ($index !== false) {
                    if ($row['data_type'] == 'USER-DEFINED') {
                        $cols[$row['column_name']] = $row['column_name']." geometry";
                        $col_types[$row['column_name']] = 'geometry';
                    } else {
                        $cols[$row['column_name']] = $row['column_name']." ".$row['data_type'];
                        $col_types[$row['column_name']] = $row['data_type'];
                    }
                } else {
                    $cols[$row['column_name']] = $row['column_name']." text";
                }
            }
            // change type USER-DEFINED to geometry
            //$index = array_search('obm_geometry',$c);
            //if ($index !== false)
            //    $cols['obm_geometry'] = "obm_geometry geometry";

            foreach(array_values($c) as $cn) {
                $index = array_search($cn,array_keys($cols));
                if ($index === false)
                    $cols[$cn] = "$cn text";
            }
            $arr2ordered = array() ;

            foreach (array_values($c) as $key) {
                $arr2ordered[$key] = $cols[$key];
            }
            $cols = $arr2ordered;
            
            $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_query_%s_%s (obm_id integer,%s)",PROJECTTABLE,session_id(),implode(',',$cols));
            pg_query($ID,$cmd);
            update_temp_table_index('aquery_'.PROJECTTABLE.'_'.session_id());

            pg_query($ID, sprintf("COPY temporary_tables.temp_query_%s_%s FROM stdin",PROJECTTABLE,session_id()));

            require_once('vendor/autoload.php');

            // LoadQuery feature collection
            //$num = 0;
            foreach($json->{'features'} as $feature) {
                $g = $feature->{'geometry'};
                if ($g->{'type'}=='Point') $post_geom[] = array('point'=>array($g->{'coordinates'}[0],$g->{'coordinates'}[1]));
                elseif ($g->{'type'}=='LineString') {
                    foreach ($g->{'coordinates'} as $xy) {
                        $post_geom[] = array('linearRing'=>array($xy[0],$xy[1]));
                    }
                }
                elseif ($g->{'type'}=='Polygon') {
                    foreach ($g->{'coordinates'} as $xy) {
                        $post_geom[] = array('linearRing'=>array($xy[0],$xy[1]));
                    }
                }

                $t = $feature->{'type'};
                $line = array();
                foreach($feature->{'properties'} as $k=>$v) {
                    if ($k=='obm_id') {
                        $post_id[] = $v;
                    }
                    if (isset($col_types[$k])) {
                        if ($col_types[$k]=='integer' and $v == ''){ 
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='numeric' and $v == ''){ 
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='geometry' and $v != '') {
                            //$line[$k] = "$v::geometry";
                            $line[$k] = "$v";
                        } elseif ($col_types[$k]=='geometry' and $v == '') {
                            $line[$k] = "\N";
                        } elseif ($col_types[$k]=='boolean' and $v == '') {
                            $line[$k] = "\N";
                        } elseif ($col_types[$k]=='double precision' and $v == '') {
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='timestamp without time zone' and $v == '') {
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='date' and $v == '') {
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='time without time zone' and $v == '') {
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='time with time zone' and $v == '') {
                            $line[$k] = '\N';
                        } elseif ($col_types[$k]=='timestamp with time zone' and $v == '') {
                            $line[$k] = '\N';
                        } else {
                            // replace backslashes 
                            $v = preg_replace('/\\\+/','-',$v);
                            $line[$k] = "$v";
                            //if ($num == 972) debug($line);
                        }
                    } else {
                        $line[$k] = "$v";
                    }
                }
                $line_string = implode("\t",$line);
                pg_put_line($ID, $line_string."\n");
                //$num++;
            }

            pg_put_line($ID, "\\.\n");
            pg_end_copy($ID);
            $_SESSION['do-not-create-query'] = 'it is already done in wfs_processing';

            pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));

            // It can be very slow - ten minutes or more if the query is very big...
            // If I create the table and call parallel inserts??? how?? exec('php parallel.php $id_cmd')
            $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_%s_%s AS %s",PROJECTTABLE,session_id(),sprintf("SELECT * FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
            $res = pg_query($ID,$cmd);

            $cmd = sprintf("GRANT SELECT ON temporary_tables.temp_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
            pg_query($ID,$cmd);

            update_temp_table_index(PROJECTTABLE.'_'.session_id());
        }



        # Először azt nézzük meg, hogy meg lett-e hívva a maps.js-ből
        # amikor még a böngésző processzálta az xml-t
        #$_SESSION['wfs_response'] = $_POST['wfs_response'];
        #$wfs_response = $_POST['wfs_response'];
        if (!count($post_id)) {
            //if (isset($_POST['xy']) and $_POST['xy']!='') {
            //    $out .= ' around '.preg_replace('/[^0-9. ]/','',$_POST['xy']);
            //    $out .= sprintf("<br><a href='http://%s/upload/?geom=POINT(%s)'>Make a data point here</a>",URL,preg_replace("/ /",",",$_POST['xy']));
            //}
            //No matching records found
            echo json_encode(array("geom"=>'',"error"=>'0'));
            exit;
        }
}
    //if (isset($_POST['skip_processing']) and $_POST['skip_processing']=='yes') {
    //}
/*if(count($post_geom)>2000) {
        if (isset($_POST['skip_processing']) and $_POST['skip_processing']=='no') {
            $_SESSION['wfs_full_array'] = array("id"=>$post_id,"geom"=>$post_geom,"data"=>$post_data);
        }
        $_SESSION['wfs_rows']= count($post_geom);
        if(!isset($_SESSION['wfs_rows_counter'])) 
            $_SESSION['wfs_rows_counter']=0;
        $post_geom = array_slice($post_geom, $_SESSION['wfs_rows_counter'],2000); 
        $post_data = array_slice($post_data, $_SESSION['wfs_rows_counter'],2000);
        $post_id   = array_slice($post_id,$_SESSION['wfs_rows_counter'],2000);
        $_SESSION['wfs_rows_counter']+= 2000;
        #syslog(LOG_WARNING,"wfa-set-count:".count($_SESSION['wfs_full_array']));
} else {*/
        if (isset($_SESSION['wfs_rows'])) {
            unset($_SESSION['wfs_rows']);
            unset($_SESSION['wfs_rows_counter']);
        }
        if (isset($_SESSION['wfs_full_array'])) {
            unset($_SESSION['wfs_full_array']);
        }
        //$_SESSION['wfs_full_array'] = array("id"=>$post_id,"geom"=>$post_geom,"data"=>$post_data);
//}

$_SESSION['wfs_array'] = array("id"=>$post_id,"geom"=>$post_geom,"data"=>$post_data);

if (isset($_POST['return']) and $_POST['return']=='id')
    echo json_encode(array("id"=>$post_id,"error"=>''));
elseif (isset($_POST['return']) and $_POST['return']=='none')
    echo json_encode(array("error"=>''));
else
    echo json_encode(array("geom"=>$post_geom,"error"=>''));

#xdebug_stop_trace();
?>
