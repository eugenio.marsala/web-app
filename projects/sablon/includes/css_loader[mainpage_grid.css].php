<?php

header("Content-type: text/css", true);

if (file_exists('../css/private/mainpage_grid.css')) {

    include_once('../css/private/mainpage_grid.css');

} elseif (file_exists('../css/mainpage_grid.css')) {
    
    include_once('../css/mainpage_grid.css');

}
?>
