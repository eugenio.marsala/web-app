<?php

header("Content-type: text/css", true);

if (file_exists('../css/private/footer.css')) {

    include_once('../css/private/footer.css');

} elseif (file_exists('../css/footer.css')) {
    
    include_once('../css/footer.css');

}
?>
