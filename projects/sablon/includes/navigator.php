<?php

$modules = new modules();
$out = '';
$out .= "<button class='pure-button button-large button-transp' id='shownavigator'><i class='fa-angle-double-left fa fa-fw fa-lg'></i></button>";
$out .= "
<div id='navigator'>
    <button class='pure-button button-large button-transp' id='hidenavigator'><i class='fa-angle-double-right fa fa-fw fa-lg'></i></button>
    <div id='navbuttons'>";

if ($modules->is_enabled('results_summary') or $modules->is_enabled('results_specieslist'))
    $out .= "<a href='#nav_summary' class='button-href button-large pure-button'><i class='fa fa-fw fa-lg fa-info'></i></a><br>";

if ($modules->is_enabled('results_buttons'))
    $out .= "<a href='#save_options' class='button-href button-large pure-button'><i class='fa fa-fw fa-lg fa-save'></i></a><br>";
        
if ($modules->is_enabled('results_asTable') or $modules->is_enabled('results_asStable') or $modules->is_enabled('results_asList'))
    $out .= "<a href='#view_options' class='button-href button-large pure-button'><i class='fa fa-fw fa-lg fa-list'></i></a>";

$out .= "</div>
    <div style='position:relativ;height:30px;width:50px'>
        <button style='position:absolute' class='pure-button button-large button-gray' id='scrollup'><i class='fa-arrow-up fa fa-fw fa-lg'></i></button>
        <button style='position:absolute' class='pure-button button-large button-gray' id='scrolldown'><i class='fa-arrow-down fa fa-fw fa-lg'></i></button>
    </div>
</div>";

?>
