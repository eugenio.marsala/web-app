<?php

/* Alias for log_action */
function debug($message,$file=NULL,$line=NULL) {
    log_action($message,$file,$line);
}

/* Create a sylog message
 * */
function log_action($message,$file=NULL,$line=NULL) {
    if(is_array($message)) {
        $message = json_encode($message,JSON_PRETTY_PRINT);
    }
    elseif(is_object($message)) {
        $message = json_encode($message,JSON_PRETTY_PRINT);
    }
    $custom_log = "/var/log/openbiomaps.log";

    $pointer = "";
    if($file!=NULL or $line!=NULL)
        $pointer = " $file $line";

    if (is_writable($custom_log)) {
        $date = date('M j h:i:s'); 
        error_log("[OBM_".PROJECTTABLE."] $date$pointer: $message\n", 3, "/var/log/openbiomaps.log");
    } else {
        openlog("[OBM_".PROJECTTABLE."]", 0, LOG_LOCAL0);
        syslog(LOG_WARNING,$pointer.$message);
        closelog();
    }
}

/* string quoting for sql queries
 * return a random delimetered string
 * */
function quote($text,$force_quote=false) {
    //if ($text=='NULL' or trim($text)=='') return "NULL";
    if ($text==='NULL') return "NULL";
    if ($force_quote===false)
        if (gettype($text)=='integer') return $text;

    if (trim($text)=='') return "NULL";

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $random_delimeter = substr( str_shuffle( $chars ), 0, 16 );
    $text = '$'.$random_delimeter.'$'.trim($text).'$'.$random_delimeter.'$';

    return $text;
}

?>
