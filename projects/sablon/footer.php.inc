<?php 

?>
<div id='footer'>
<table style="width:100%;padding:10px 30px 0 30px">
    <tr>
        <td style='vertical-align:top;text-align:center'>
            <a href='<?php echo $protocol ?>://<?php echo URL ?>/upload/' class='fl'><?php echo t(str_upload) ?></a>
        <span class='fl-sep'></span>
            <a href='<?php echo $ref ?>index.php?map' class='fl'><?php echo t(str_map) ?></a>
        <span class='fl-sep'></span>
            <a href='<?php echo $ref ?>index.php?database' class='fl'><?php echo t(str_about_db) ?></a>
        <span class='fl-sep'></span>
            <a href='<?php echo $protocol ?>://<?php echo OB_DOMAIN ?>/terms/' class='fl'><?php echo t(str_tc) ?></a>
        <span class='fl-sep'></span>
            <a href='<?php echo $protocol ?>://<?php echo OB_DOMAIN ?>/privacy/' class='fl'><?php echo t(str_privacy_policy) ?></a>
        </td>
    </tr>
    <tr>
        <td style='vertical-align:top;text-align:center'><br><br>
        <?php
        echo str_other_langs.":<br>";
        foreach ($LANGUAGES as $L=>$label) {
            //if ( file_exists(sprintf("%slanguages/%2s.php",getenv('PROJECT_DIR'),$L))) 
                echo "<a href='?lang=$L' class='fl'>$label</a>";
        }
        ?>
        </td>
    </tr>
    <tr>
        <td style='vertical-align:top;text-align:center'><br><br>
        <?php echo str_contrib_partner ?>:
        <br>

        <a href='<?php echo $protocol ?>://openbiomaps.org' class='flb'><div class='inset' style='width:220px;background-image:url(<?php echo $protocol ?>://<?php echo URL ?>/images/OpenBioMaps_200.png)'></div></a> 
        <a href='<?php echo $protocol ?>://www.unideb.hu' class='flb' target='_blank'><div class='inset' style='background-image:url(<?php echo $protocol ?>://<?php echo URL ?>/images/unideb_logo.png)'></div></a>
        </td>
    </tr>
</table>
</div><!--/footer-->
