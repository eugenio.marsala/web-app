/*
 *
 * */
var mapwindow;
var active;
var passive;
var focus = '';
var lastSel;
var lastSelOption;
var shiftDown = false;
var currentShift = false;
var fill_function = {
    fun:''
};

var UgU;
var auto_save_wait = 0;

//$( window ).unload(function(e) {
    //jQuery.ajax({url:"ajax", async:false,type: 'POST', data:{'closepage':sfid}});
    //return "Bye now!";
//});

$(document).ready(function() {

    //$('.fSelect').fSelect({'placeholder':'&nbsp;'});
    /*$('#rightTable').on("click",".SumoSelect",function(){
        console.log(1);
        $(this).find('.optWrapper').offset($(this).offset);
    });*/

    var screenWidth = $(window).width();
    var lTWidth = $("#leftTable").width();
    if (lTWidth>0)
        lTWidth += 10;


    // catch ctrl down
    // ctrl + s
    var ctrlDown = false,
        ctrlKey = 17,
        shftKey = 16,
        cmdKey = 91,
        sKey = 83;
    $(document).keydown(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey || e.keyCode == shftKey) ctrlDown = true;
    }).keyup(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey || e.keyCode == shftKey) ctrlDown = false;
    });

    $("#rightTable").css({'width': (screenWidth-lTWidth-25 + 'px')});

    // set upload table horizontal sizes according to screen width
    $("#longin_container").css({'width': ($("#rightTable").width()-42 + 'px') //($("#upload-data-table").width() + 'px')
    });

    $(".longin").css({'width': ($("#longin_container").width()-38 + 'px')});


    //resizable table cells
    $(".setsize").resizable({});

    /* fill+ button select menu: toggle and trigger */
    $("body").on('click','#fillfunction_chooser > option',function(e){
        //functions.js
        fillplus_on_selected( $(this).parent().val() );
        $(this).parent().toggle();

        // hide params select menu
        $("#obfun-params").parent().hide();
        $("#obfun-params").val('');
        // hide references select menu
        $("#obfun-references").parent().parent().hide();

        if ($(this).val()=='`apply:replace.match:///' || $(this).val()=='`apply:[]replace.all:///') {
            $("#obfun-params").parent().show();
            $("#obfun-params").val('///');
        }
        if ($(this).val()=='`apply:[]replace.all:///' || $(this).val()=='`apply:[]math.sum' || $(this).val()=='`apply:[]math.avg') {
            $("#obfun-references").parent().parent().show();
        }
    });

    /* toggle fill+ button select menu 
     * show the functions' list */
    $(".rightTable_container").on('click','#fillfunction',function() {
        $('#fillfunction_chooser').toggle();
    });
    
    /* pen button click
     * set/fill active input cell with longin's value
     * */
    $("#longin_container").on('focus','#longin',function(e){
        var con = $("#upload-data-table tbody").find('.activeInput');
        if (!con.length) {
             // more than one cell is possible!
            $("#upload-data-table tbody").find('.focusInput').each(function() {
                if ( $(this).closest('tr').attr('class')!='skippedRow') {
                    $(this).removeClass('focusInput');
                    $(this).addClass('activeInput');
                }
            });
        }

    });

    // pencil button at the right side of longin field - apply fill value on the selected cells
    $("#longin_container").on('click','#fillit',function(e){

        //save current sheet content into session table content
        var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
        TableData = JSON.stringify(storeTblValues('upload-data-table'));
        auto_save_wait = 1;

        $.post("ajax", {'upload_table_page':TableData,'read_visible_cells':1,'print':1,'page':page,'form_page_id':$("#form_page_id").val(),'form_page_type':$("#form_page_type").val()}, 
            function(data) {
                var t = $('#longin').val();
                var con = $("#upload-data-table tbody").find('.activeInput');
                var hcon = $("#upload-data-table thead").find('.activeInput');
                if (hcon.length) {
                    //active autofill cell
                    $("#upload-data-table thead").find('.activeInput').each(function() {
                        $(this).val(t);
                        if ($(this).val().match(/^`apply:.+/)) { 
                            //` restore syntax highlight in vi
                            //function autofill
                            fillfunction($(this),$(this).val());
                        } else {
                            //normal autofill
                            fillfunction($(this),'fill');
                        }
                    });

                } else if (con.length) {
                    $("#upload-data-table tbody").find('.activeInput').each(function() {
                        if ( $(this).closest('tr').attr('class')!='skippedRow') {
                            if ($(this).prop('multiple')) {
                                var split_t = t.split(',');
                                $(this + " > option").each(function() {
                                    if (split_t.includes(this.value)) {
                                        $(this).prop("selected", true);
                                    }
                                });
                            } else
                                $(this).val(t);
                            //console.log($(this).val())
                        }
                    });
                }
            auto_save_wait = 0;
        });
    });
    // flask button at the left side of longin field - apply fill value on the selected cells
    $("#longin_container").on('click','#apply',function(e) {

        // do not fire flaska if function editor is hidden
        if ($('#longin-function-editor').is(":hidden")) {
            return false;
        }
        $(this).hide();
        //$("#fillfunction").show();
        $('#fillit').show();
        $('#obfun-close').hide();

        //save current sheet content into session table content
        var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
        TableData = JSON.stringify(storeTblValues('upload-data-table'));
        auto_save_wait = 1;
        $.post("ajax", {'upload_table_page':TableData,'read_visible_cells':1,'print':1,'page':page,'form_page_id':$("#form_page_id").val(),'form_page_type':$("#form_page_type").val()}, function(data){
                var obfun_name = $('#obfun-name').val();
                var obfun_references = $('#obfun-references').val();
                var obfun_params = $('#obfun-params').val();
                //`apply:[]replace.all:///
                var fun = "`apply:"; // `apply
                if (obfun_references != '') {
                    fun = fun + "[" + obfun_references + "]";
                }
                fun = fun + obfun_name;
                if (obfun_params != '') {
                    fun = fun + ":" + obfun_params;
                }
                if ($("#" + $( "#obfun-on" ).val()).closest('th').length == 0) {
                    $( "#dialog" ).html('"<i>' + $( "#obfun-on" ).val() + '</i>" column is not assigned yet.');
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                } else {
                    fillfunction($("#" + $( "#obfun-on" ).val()),fun);
                
                }

                //var on = $( "#obfun-on" ).data( "id" );
                //$("#" + on).addClass('activeInput');
                
                //var hcon = $("#upload-data-table thead").find('.activeInput');
                //if (hcon.length) {
                    //active autofill cell
                    //$("#upload-data-table thead").find('.activeInput').each(function() {
                        //function autofill
                    //    fillfunction($(this),fun);
                    //});
                //}
            $('#longin-function-editor').hide();
            $('#longin').focus();
            $('#longin').show();
            auto_save_wait = 0;

            });
    });
    /*
     * */                
    $("#longin_container").on("change click","#obfun-name",function(){
        $("#obfun-references").parent().parent().hide();
        $("#obfun-params").parent().hide();
        $("#obfun-params").val('');
        if ($(this).val()=='replace.match' || $(this).val()=='replace.all') {
            $("#obfun-params").parent().show();
            $("#obfun-params").val('///');
        }
        if ($(this).val()=='replace.all' || $(this).val()=='math.sum' || $(this).val()=='math.avg') {
            $("#obfun-references").parent().parent().show();
        }

    });
    $('#obfun-references').fSelect({'placeholder':'&nbsp;','showSearch':false});

    $("#longin_container").on('click','#obfun-close',function(e){
            $('#longin-function-editor').hide();
            $('#longin').focus();
            $('#longin').show();

            $("#apply").hide();
            //$("#fillfunction").show();
            $('#fillit').show();
        $(this).hide();
    });

    /* filter option - show list */
    $("#upload-data-table").on("click",".filterplus",function() {
        var t = $(this);
        var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
        TableData = JSON.stringify(storeTblValues('upload-data-table'));
        
        t.parent().find('.filterplus_chooser').html('');
        t.parent().find('.filterplus_chooser').toggle();
        
        if (t.parent().find('.filterplus_chooser').is(':visible')) {
            auto_save_wait = 1;
            $.post("ajax", {'upload_table_page':TableData,'read_visible_cells':1,'print':1,'page':page,'form_page_id':$("#form_page_id").val(),'form_page_type':$("#form_page_type").val()}, function(data){

                var idx = 2+(t.closest('th').index());
                $.post("ajax", {'filterplus':idx}, function(data) {
                    var j = JSON.parse(data);
                    for (ji=0;ji<j.length;ji++) {
                        var o = new Option(j[ji],j[ji]);
                        $(o).html(j[ji]);
                        t.parent().find(".filterplus_chooser").append(o);
                    } 
                });
                auto_save_wait = 0;
            });
        }
    });

    /* filter option - set marks */
    $("#upload-data-table").on('click','.filterplus_chooser > option',function(e){

        var t = $(this);
        var idx = 2+(t.closest('th').index());
        var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
        TableData = JSON.stringify(storeTblValues('upload-data-table'));
        auto_save_wait = 1;
        $.post("ajax", {'filterplus_skipmark':t.val(),'filterplus_idx':idx}, function(data) {
            if (data!='') {
                $('.skip').each(function() {
                    if ($(this).is(":checked")) {
                        $(this).trigger('click');
                    }
                });

                var j = JSON.parse(data);
                if ($('.page-group').length > 0) {
                    var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
                    j = j.slice(page);
                }
                $(".skip").each(function(){
                    var ss = j.shift();
                    if (typeof ss !== 'undefined' && ss.skip_marked == 't') {
                        $(this).trigger('click');
                    }
                });
            }
            auto_save_wait = 0;
        });
    });

    /* multiselect dropdown list in upload table: dynamic positioning */
    $('.fTSelect').fTSelect({'placeholder':'&nbsp;','showSearch':false});

    $(window).scroll(function() { //when window is scrolled
        var window_post = $(window).scrollTop();
        var window_posl = $(window).scrollLeft();
        var pos = $("#upload-data-table").find('.fst-open').offset();
        if (typeof pos !== 'undefined') {
            $("#upload-data-table").find('.fst-open').find('.fst-dropdown').css({left: eval(pos.left - window_posl), top: eval(pos.top - window_post + 16)});
        }
    });
    $("#rightTable").scroll(function(){
        var window_post = $(window).scrollTop();
        var window_posl = $(window).scrollLeft();
        var pos = $("#upload-data-table").find('.fst-open').offset();
        if (typeof pos !== 'undefined') {
            $("#upload-data-table").find('.fst-open').find('.fst-dropdown').css({left: eval(pos.left - window_posl), top: eval(pos.top - window_post + 16)});
        }
    });
    /* multiselect end */


    //kiteszi az aktív cella értékét a kiemelt hosszú mezőbe gépelés közben
    $("#upload-data-table tbody").on('input','.ci',function(e){
        if ($(this).hasClass('activeInput')) {
            var type = $(this).getType();
            if (type=='text') {
                $('#longin').val($(this).val());
                var this_autoskip = $(this).closest('tr').find(".autoskip");
                if (this_autoskip.val()==1 & $(this).val()!='') {
                    this_autoskip.val(0);
                    this_autoskip.removeClass('autoskip');
                }
            }
        }
    });
    //kiteszi a select option értékét a kiemelt hosszú mezőbe kiválasztás után
    $("#upload-data-table tbody").on('change','.ci',function(e){
        var type = $(this).getType();
        if(type=='select') {

            if ($(this).prop('multiple')) {
                var txt = new Array;
                $(this).find('option:selected').each(function () {
                    txt.push($(this).text());
                });
                $('#longin').val(txt.join(','));
            } else {
                $('#longin').val($(this).find('option:selected').text());
            }
            var this_autoskip = $(this).closest('tr').find(".autoskip");
            if (this_autoskip.val()==1 & $(this).val()!='') {
                this_autoskip.val(0);
                this_autoskip.removeClass('autoskip');
            }
        }
    });
    //gépelés
    $("#upload-data-table tbody").on('keyup','.ci',function(e){
        //hit enter
        if(e.keyCode == 13)
        {
            var type = $(this).getType();
            if (type=='text') {
                $('#longin').val($(this).val());
                var this_autoskip = $(this).closest('tr').find(".autoskip");
                if (this_autoskip.val()==1 & $(this).val()!='') {
                    this_autoskip.val(0);
                    this_autoskip.removeClass('autoskip');
                }
            }
            else if(type=='select') {
                $('#longin').val($(this).find('option:selected').text());
                var this_autoskip = $(this).closest('tr').find(".autoskip");
                if (this_autoskip.val()==1 & $(this).val()!='') {
                    this_autoskip.val(0);
                    this_autoskip.removeClass('autoskip');
                }
            }
        }
        //hit shift
        else if(e.keyCode == 16) {
            currentShift=false;
        }
    });

    $("#upload-data-table tbody").on('click','.ci',function(e){
        //kiteszi a cella értékét a kiemelt hosszú mezőbe 
        $('#longin').val($(this).val());

        var this_autoskip = $(this).closest('tr').find(".autoskip");
        if (this_autoskip.val()==1 & $(this).val()!='') {
            this_autoskip.val(0);
            this_autoskip.removeClass('autoskip');
        }

        if (currentShift) {
            //console.log('shift')
            if(typeof active == 'undefined') {
                active = $(this).closest('td').addClass('active');
            }
            passive = active;
            active = $(this).closest('td').addClass('active');
            var x = active.index();
            var y = active.closest('tr').index();
            var px = passive.index();
            var py = passive.closest('tr').index();

            var from;
            var to;
            if (x == px) {
                if (y<py) {
                    from = y;
                    to = py;
                } else {
                    from = py;
                    to = y+1;
                }
                var i;
                for (i=from;i<to;i++) {
                    $('#upload-data-table tbody tr').eq(i).find('td').eq(x).find('select').addClass('focusInput');
                    $('#upload-data-table tbody tr').eq(i).find('td').eq(x).find('input').addClass('focusInput');
                }
            } else if (y == py) {
                if (x<px) {
                    from = x;
                    to = px;
                } else {
                    from = px;
                    to = x+1;
                }
                var i;
                for (i=from;i<to;i++) {
                    $('#upload-data-table tbody tr').eq(y).find('td').eq(i).find('select').addClass('focusInput');
                    $('#upload-data-table tbody tr').eq(y).find('td').eq(i).find('input').addClass('focusInput');
                }
            }
        } else {
            $('select').removeClass('focusInput');
            $('input').removeClass('focusInput');
            $('select').removeClass('activeInput');
            $('input').removeClass('activeInput');
            active = $(this).closest('td').addClass('active');
            var x = active.index();
            var y = active.closest('tr').index();
            $('#upload-data-table tbody tr').eq(y).find('td').eq(x).find('select').addClass('focusInput');
            $('#upload-data-table tbody tr').eq(y).find('td').eq(x).find('input').addClass('focusInput');
        }
        return false;
    });

    var downval = '';
    var downtarget = '';
    var upval = '';
    var uptarget = '';
    $("#upload-data-table tbody").on('keyup','.ci',function(e){
        if( $(this).is('input:text, select:focus') && !$(this).hasClass('ui-autocomplete-input') ) {
            if (e.keyCode == 40 ) {
                $(downtarget).val(downval).change();
            } 
            else if (e.keyCode == 38) {
                $(uptarget).val(upval).change();
            }
        }
    });
    //typing key down
    $("#upload-data-table tbody").on('keydown','.ci',function(e){
        //shift
        if (e.keyCode == 16) {
            currentShift=true;
            return false;
        } 

        $('td').removeClass('active');
        if(!currentShift) {
            $('select').removeClass('activeInput');
            $('input').removeClass('activeInput');
        }
        active = $(this).closest('td').addClass('active');
        $(this).closest('td').find('select').addClass('activeInput');
        $(this).closest('td').find('input').addClass('activeInput');
        var x = active.index();
        var y = active.closest('tr').index();
        var len =$('td', active.closest('tr')).length;

        var move = 0;
        if (e.keyCode == 27) {
            //esc
            focus = '';
            $('td').removeClass('active');
            $('select').removeClass('activeInput');
            $('input').removeClass('activeInput');
            $(this).closest('td').find('input').addClass('focusInput');
            $(this).closest('td').find('select').addClass('focusInput');
            return false;
        }
        else if (e.keyCode == 37) {
            //left
            if (focus=='') {
                x--;
                move++;
            }
        }
        else if (e.keyCode == 38) {
            //up
            if( $(this).is('input:text, select') && !$(this).hasClass('ui-autocomplete-input') ) {
                upval = $(this).val();
                uptarget = e.target;
                y--;
                move++;
                focus = '';
            }
        }
        else if (e.keyCode == 39) { 
            //right
            if (focus=='') {
                x++
                move++;
            }
        }
        else if (e.keyCode == 9) { 
            //tab
            x++
            move++;
            focus = '';
        }
        else if (e.keyCode == 40) {
            //down
            if( $(this).is('input:text, select:focus')  && !$(this).hasClass('ui-autocomplete-input')) {
                downval = $(this).val();
                downtarget = e.target;
                // it was an input
                y++
                move++;
                focus = '';
            }
        } 
        else if (e.keyCode == 33) {
            //pgup
            if (focus=='') {
                x = -1;
                move++;
            }
        } 
        else if (e.keyCode == 34) {
            //pgdown
            if (focus=='') {
                x = len;
                move++;
            }
        } else {
            //anything else not handled
            focus = 'on';
            $(this).closest('td').find('select').removeClass('focusInput');
            $(this).closest('td').find('input').removeClass('focusInput');
            $(this).closest('td').find('select').addClass('activeInput');
            $(this).closest('td').find('input').addClass('activeInput');
            return true;
        }
        if(move) {
            //end of line, start from next line, first cell
            if (x==len) {
                x = 1;
                y = y+1;
            } else if (x==0) {
                x = len-1;
                y = y-1;
            } else if (x==-1) {
                x = 1;
                y = y-1;
            }

            //focus on selected input
            $('#upload-data-table tbody tr').eq(y).find('td').eq(x).find('input').each(function(){
                $(this).focus();
            });
            $('#upload-data-table tbody tr').eq(y).find('td').eq(x).find('select').each(function(){
                if(focus=='') {
                    $(this).focus();
                    focus = 'on';
                }
            });
            $('input').removeClass('activeInput');
            $('select').removeClass('activeInput');
            if(!currentShift) {
                $('select').removeClass('focusInput');
                $('input').removeClass('focusInput');
            }
            $('td').removeClass('active');
            $('td').removeClass('focus');
            active = $('#upload-data-table tbody tr').eq(y).find('td').eq(x).addClass('active');
            $('#upload-data-table tbody tr').eq(y).find('td').eq(x).find('select').addClass('focusInput');
            $('#upload-data-table tbody tr').eq(y).find('td').eq(x).find('input').addClass('focusInput');
            return false;
        }
    });

    //$(".dragbox").draggable({ handle: "legend",axis: "x" } );

    /* save dragndrop column order in file upload
     *
     * */
    $("#save-order").click(function(event){
        event.preventDefault();
        var template = {
            assignments:{},
        };
        var arr = new Array();

        /*$("#sList").children().each(function(){
            var id = $(this).attr('id');
            var val= $(this).html();
            arr[id]=val;
        });
        template['slist']=arr;*/
        $(".oListElem").each(function(){
            var colvalue=$(this).find('li').eq(0).text();
            var ref = $(this).find('.field-title').text();
            var id = $(this).find('li').eq(0).attr('id');

            if (colvalue!='') {
                arr.push(ref+"::"+id);
            } else {
                arr.push("");
            }
        });
        template['assignments']=arr;

        $.post("ajax", { 'template_post':JSON.stringify(template),'template_name':$("#template_name").val(),'sheet_name':$("#change_exs_sheet").val() },
        function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                $( "#dialog" ).text(retval['message']);
            } else if (retval['status']=='fail') {
                $( "#dialog" ).text("Invalid response received.");
            } else if (retval['status']=='success') {
                $( "#dialog" ).text(retval['data']);
            }
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if (!isOpen) $( "#dialog" ).dialog( "open" );

        });
    });

    /* save current fields
     * */
    $("#save-state").click(function(event){
        event.preventDefault();
        save_sheet();
    });

    //autofill trigger for chrome for filling the current rows
    $('#upload-data-table').on('keyup','.fill',function(event){
        //hit enter in autofill cells
        if(event.keyCode==13) {
            var t = $(this);

            //save current sheet content into session table content
            var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
            TableData = JSON.stringify(storeTblValues('upload-data-table'));
            auto_save_wait = 1;
            $.post("ajax", {'upload_table_page':TableData,'read_visible_cells':1,'print':1,'page':page,'form_page_id':$("#form_page_id").val(),'form_page_type':$("#form_page_type").val()}, function(data) {

                //paging('',page,1);
            
                if (t.val().match(/^`apply:.+/)) {
                    //function autofill
                    fillfunction(t,t.val());
                } else {
                    //normal autofill
                    fillfunction(t,'fill');
                }
                auto_save_wait = 0;
            });
            //` restore syntax
        }
    });

    // fill the non visible rows
    $('#upload-data-table').on("click",".fill",function(event){
        $('input').removeClass('activeInput');
        $(this).addClass('activeInput');
    });

    // double click on skip row -> remove row - It is too easy to drop lines with skip rows!!!!
    /*$('#upload-data-table').on('dblclick','.skip',function(event){
        var id = $(this).closest('tr').index();
        $("#upload-data-table tbody tr:eq("+id+")").remove();
    });*/
    // reverse skips
    $('#upload-data-table').on('click','#reverseskip',function(event){
        $('.skip').trigger('click');
        var chk = 0;
        if($(this).is(":checked")) {
            chk = 1;
        }
        auto_save_wait = 1;
        $.post("ajax", {'reverse_table_skip':chk}, function() { 
            auto_save_wait = 0;
        });
    });
    // deselect skips
    $('#upload-data-table').on('click','#deselectskip',function(event){
        $('.skip').each(function() {
            if ($(this).is(":checked")) {
                $(this).trigger('click');
            }
        });
        auto_save_wait = 1;
        $.post("ajax", {'deselect_table_skip':1}, function() {
            auto_save_wait = 0;
        });
    });


    //shiftclick skip function
    //probably not necesarry
    this.onkeydown = function(evt){
        var evt2 = evt || window.event;
        var keyCode = evt2.keyCode || evt2.which;       
        if(keyCode==16) {
            shiftDown = true;
        }
    }
    this.onkeyup = function(){
        shiftDown = false;
    }
    //skip click
    $('#upload-data-table tbody').on('change','.skip',function(event) {
        //var id = $(this).attr('id');
        //var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var id = $(this).closest('tr').index();

        if (shiftDown) {
            var b;
            var d=new Array();
            var di=new Array();
            var chk=2;
            if($(this).is(":checked")) {
                chk = 1;
            }
            $(".skip").each(function(){
                var b = $(this).closest('tr').index();
                if (b==id) {
                    return false;        
                }
                if($(this).is(":checked")) {
                    d.push(1);
                } else {
                    d.push(2);
                }
                di.push(b);
            });
            while( i = d.pop() ) {
                k = di.pop();
                if (chk==1) {
                    if (i==2) {
                        $("#upload-data-table tbody tr:eq("+k+")").find('td:eq(0)').find('input').prop('checked', true);
                        $("#upload-data-table tbody tr:eq("+k+")").addClass('skippedRow');
                        $("#upload-data-table tbody tr:eq("+k+")").find('td:eq(0)').addClass('skippedCell');
                        $("#upload-data-table tbody tr:eq("+k+")").find('td').each(function(){
                            $(this).find('input').addClass('skippedInput');
                        });
                        $("#upload-data-table tbody tr:eq("+k+")").find('td').each(function(){
                            $(this).find('button').addClass('skippedInput');
                        });
                    } else break;
                } else {
                    if (i==1) {
                        $("#upload-data-table tbody tr:eq("+k+")").find('td:eq(0)').find('input').prop('checked', false);
                        $("#upload-data-table tbody tr:eq("+k+")").removeClass('skippedRow');
                        $("#upload-data-table tbody tr:eq("+k+")").find('td:eq(0)').removeClass('skippedCell');
                        $("#upload-data-table tbody tr:eq("+k+")").find('td').each(function(){
                            $(this).find('input').removeClass('skippedInput');
                        });
                        $("#upload-data-table tbody tr:eq("+k+")").find('td').each(function(){
                            $(this).find('button').removeClass('skippedInput');
                        });
                   } else break;
               }
            }
        }
        if($(this).is(":checked")) {
            $(this).closest('tr').addClass('skippedRow');
            $(this).closest('td').addClass('skippedCell');
            $(this).closest('tr').find('input').addClass('skippedInput')
            $(this).closest('tr').find('select').addClass('skippedInput')
            $(this).closest('tr').find('button').addClass('skippedButton')
        } else {
            $(this).closest('tr').removeClass('skippedRow');
            $(this).closest('td').removeClass('skippedCell');
            $(this).closest('tr').find('input').removeClass('skippedInput')
            $(this).closest('tr').find('select').removeClass('skippedInput')
            $(this).closest('tr').find('button').removeClass('skippedButton')
        }
    });
    /* change sheet on file upload */
    $("body").on('change','#changepp',function(event){
        auto_save_wait = 1;
        $.post("ajax", {'changepp':$('#changepp').val()}, function(data) {
            paging(event,0);
            auto_save_wait = 0;
        });
    });
    /* change sheet on file upload */
    $("body").on('click','.page-group li',function(event){
        var page = $(this).find('a').attr('href');
        paging(event,page);
    });
    /* change sheet on file upload: jump to*/
    $("body").on('click','.pagel',function(event){
        var target = $(this).data('target');
        var page = $(this).attr('href');
        var rowindex = eval($(this).data('index')-page);
        paging(event,page);
        // set the column position
        $('html, body').animate({ scrollTop: $("#upload-data-table").offset().top }, "slow", function() {
                $('#upload-data-table tbody tr').eq(rowindex).find('td').css({'border-bottom':'1px solid purple'});
            });
        var spos = $('#rightTable').offset().left;
        var tpos = $('#upload-data-table').offset().left;
        var cpos = $('#upload-data-table').find("#"+target).offset().left;
        var dist = eval(cpos-tpos);
        var tdist = eval(spos-tpos);
        var e = eval(dist-tdist);

        if (e > 0) {
            $('#rightTable').animate({scrollLeft: '+='+e}, "slow", function() {
                $('#upload-data-table tbody tr').eq(rowindex).find('td').css({'border-bottom':'1px solid purple'});
            });
        } else {
            $('#rightTable').animate({scrollLeft: '-='+eval(e*-1)}, "slow" , function() {
                $('#upload-data-table tbody tr').eq(rowindex).find('td').css({'border-bottom':'1px solid purple'});
            });
        }
    });
    
    //prevent copy-paste "I'm sure text"
    $(document).on("cut copy paste",".iambeer",function(event) {
        event.preventDefault();
    });

    //mezők elküldése -- adatok feltöltése -- importálás
    $("#send").click(function(event){
         
        auto_save_wait = 1;
        event.preventDefault();
        var mezo = {
            //id:[],
            header:[],
            default_data:[],
            rows:[],
        };
        /*kiolvassuk a fejléceket: bedobott fejléc cellák
         * */
        var one = 0;
        $(".oListElem").each(function(){
            var colvalue=$(this).find('li').eq(0).text();
            var rows = new Array();
            mezo['header'].push($(this).find('li').eq(0).attr('id'));
            if (colvalue!='') {
                $("#upload-data-table > tbody > tr").each(function(){
                    //if ($(this).attr('class')!='skippedRow') {
                    if ($(this).find('td:eq(0)').find('.skip').val()==0) {
                        rows.push($(this).index()+1);
                    }
                });
                if (one==0) {
                    mezo['rows'].push(rows);
                    one++;
                }
            }
        });
        $(".default").each(function() {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-')+1,id.length);

            // prevent add same column multiple times
            var index = jQuery.inArray(widgetId,mezo['header']);
            if (index == -1) {
                mezo['header'].push(widgetId);
                var val = $(this).val();
                mezo['default_data'].push(val);        
            } else {
                var val = $(this).val();
                var lastEl = mezo['default_data'].pop();
                lastEl += "," + val;
                mezo['default_data'].push(lastEl);
            }
        });
        $('body,html').css( 'cursor', 'progress' );

        var TableData;
        TableData = JSON.stringify(storeTblValues('upload-data-table'));

        var se = new Array();
        if ( $('.iambeer').length ) {

            $(".iambeer").each(function(){
                se.push($(this).val());
            });
        }

        //save current sheet into session table content
        $.post("ajax", {
            'upload_table_page':TableData,
            'form_page_id':$("#form_page_id").val(),
            'form_page_type':$("#form_page_type").val(),
            'default_data':mezo['default_data'],
            'print':0
        }, function(data) {
            if(data=='') alert('An error occured.');
            else {
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                    return;
                } else if (retval['status']=='fail') {
                    alert("An error occured, try to reload page.");
                    return;
                }

                var re = new RegExp("^force_skip:");
                if (retval['data']!='' && re.test(retval['data'])) {
                    var forces = retval['data'].split(';');
                    for (var i=0;i<forces.length;i++) {
                        var f = forces[i];
                        pos = f.substring(retval['data'].indexOf(':')+1,f.length);
                        if (pos!='') {
                            //var defs = mezo['default_data'].join('');
                            if (!$("#upload-data-table tbody tr:eq("+eval(pos)+")").find("td:eq(0)").find("input").is(":checked")) {
                                $("#upload-data-table tbody tr:eq("+eval(pos)+")").find("td:eq(0)").find("input").trigger("click");
                            }
                        }
                    }
                }

                var jqxhr = $.post("ajax", {
                    'upload_table_post':JSON.stringify(mezo),
                    'description':$("#upl-desc").val(),
                    'srid':$("#upl-srid").val(),
                    'form_page_id':$("#form_page_id").val(),
                    'form_page_type':$("#form_page_type").val(),
                    'soft_error':se },
                        function(data) {
                            $('body,html').css( 'cursor', 'initial' );
                            $("#dbresp").addClass('dbresp');
                            if(data=='') alert('Session expired or other error occured.');
                            else {
                                var retval = jsendp(data);
                                if (retval['status']=='error') {
                                    $( "#dbresp" ).html(retval['message']);
                                } else if (retval['status']=='fail') {
                                    $( "#dbresp" ).html("An error occured while uploading data.");
                                } else if (retval['status']=='success') {
                                    v = retval['data'];
                                    var v4 = v.substring(0,4);
                                    var ve = v.substring(4,v.length);

                                    if (v4=='OK.w') {
                                        v = ve;
                                        // clean table elements
                                        $("#upload-data-table input").val('');
                                        
                                        // clean table's select elements
                                        $('#upload-data-table select').each(function(){
                                            $(this).find("option").removeAttr('selected');
                                        });
                                        
                                        //$('.mezok input').not(':input[readonly]').val('');
                                        $('.mezok input').each(function(){
                                            var ctrl = $(this).data('control');
                                            if (typeof ctrl != "undefined") {
                                                var spl = ctrl.split(',');
                                                if (spl.indexOf('sticky')==-1 && spl.indexOf('readonly')==-1) {
                                                    $(this).val('');
                                                }
                                            }
                                        });
                                        $('.mezok select').each(function(){

                                            //$(this).not(':disabled').find("option").removeAttr('selected').find('option:first').attr('selected', 'selected');

                                            var ctrl = $(this).data('control');
                                            if (typeof ctrl != "undefined") {
                                                var spl = ctrl.split(',');
                                                if (spl.indexOf('sticky')==-1 && spl.indexOf('readonly')==-1) {
                                                    $(this).find("option").removeAttr('selected');
                                                    // reset selection to the initial
                                                    $(this).find("option[value='" + $(this).data('selected') + "']").prop("selected","selected");
                                                }
                                            }
                                        });


                                        // set initial position of skip/autoskip
                                        $('.skip').each(function(){
                                            var id = $(this).attr('id');
                                            var wid = id.substring(id.indexOf('-')+1,id.length);
                                            
                                            if ($(this).is(":checked") ) {
                                                $(this).trigger("click");
                                            }

                                            if (wid != 0) {
                                                $(this).addClass('autoskip');
                                                $(this).val(1);
                                            }
                                        });
                                        $("#longin").text('');
                                        $("#longin").val('');

                                        $("#upl-desc").text('');
                                        $("#upl-desc").val('');
                                    }
                                    else if (v4=='OK.f') {
                                        v = ve;
                                        $('.upload_fin_button').hide();
                                        $('#new').show();
                                    }
                                    $("#dbresp").html(v);
                                }
                            }
                        }).fail(function(data) {
                            var retval = jsendp(data);
                            if (retval['status']=='error') {
                                alert(retval['message']);
                            } else if (retval['status']=='fail') {
                                alert(retval['data']);
                            } else {
                                alert( jqxhr.status );
                            }
                        });
                    }

            auto_save_wait = 0;
        });
    });
    /* Cancel massive edit
     *
     * */
    $("#cancel_massiveedit").click(function(){
        $.post("ajax", {'cancel_massiveedit':1}, function(data) {
            $("#cancel_massiveedit").removeClass('button-secondary');
            $("#cancel_massiveedit").addClass('button-passive');
            $("#send").hide();
            location.reload();

        });
    });
    /* drag and drop column's label */
    //var m;
    //var m_id;
    /*$( "body" ).on('click','ul.sList',function(){
        sortevent();
    });
    $( "body" ).on('click','ul.oList',function(){
        sortevent();
    });*/
    sortevent();
    // sortable plugin init
    $("ul.sList").sortable({
        connectWith: "ul",
        dropOnEmpty: true,
        snap: "ul",
        helper: function( event,ui ) {
            var text = ui.find('.field-title').text();
            return $( "<div style='width:auto;height:auto;padding:2px;border:1px solid gray;background:white;opacity:0.5'>"+text+"</div>" );
        },
        cursorAt: { bottom:0, left:0 },    
        placeholder: "drop-highlight",
        receive: function(event,ui){
            //Egy lehetséges megoldás kezdemény arra, hogy lehessen tetszőleges aktív mezőket (pl. dátum, lista) használni fájl feltöltésnél
            //a tömbbe írásig ok, de mindegyikre kell egyedi class azonosító!
            //if(datum_id_column.indexOf(ui.item.attr('id'))) {
            //    $('#upload-data-table .datum-here').remove();
            //}
            auto_save_wait = 1;
            $.post("ajax", {'column_remove':ui.item.attr('id')}, function(data) {
                paging(event,0);
                auto_save_wait = 0;
            });
        },
        stop:  function(event, ui) {
            $("#leftTable_in").css({'height':$("#rightTable").height()+60})
            //hide database columns table if it is empty
            if ($("ul.sList li").length==0) {
                $("#leftTable").hide();
                $("#rightTable").width(eval($("#rightTable").width() + $("#leftTable").width()));
            }
        },
    });
    $("#sList, .oList").disableSelection();

    //?
    $("#resizable").resizable({ ghost: "true", grid: [ 20, 50 ] });
    
    //?
    //$(".hdiv").resizable({handles: "e",helper: "resizable-helper"});
    
    /*
     * */
    $("body").on("click",".colhelptext",function(){
        //$(this).closest("li").find(".colhelptext").toggle();
        var div = $(this).closest("li").find(".colhelptext");
        if (div.hasClass('croptext'))
            div.removeClass('croptext');
        else
            div.addClass('croptext');
    });

    $(".mezok").on('click','.datepickerb',function(){
        var target = $(this).data('target');
        if (typeof id != "undefined") {
            target = $(this).attr('target');
        }
        $(this).parent().find('input').datepicker({changeMonth:true,changeYear:true,dateFormat:'yy-mm-dd',firstDay:1,constrainInput: false,maxDate:'0',onClose:function(){
            this.focus();
            var this_autoskip = $(this).closest('tr').find(".autoskip");
            if (this_autoskip.val()==1 & $(this).val()!='') {
                this_autoskip.val(0);
                this_autoskip.removeClass('autoskip');
            }
        }});
        var $this = $(this).parent().find('input');
        if(!$this.data('datepicker')) {
            $this.removeClass("hasDatepicker");
            $this.datepicker({changeMonth:true,changeYear:true,dateFormat:'yy-mm-dd',firstDay:1,constrainInput: false,maxDate:'0',onClose:function(){this.focus();}});
            $this.datepicker("show");
        } else {
            $(this).parent().find('input').datepicker("show");
        }
    });
    /* upload table - set geometry icon */
    $(".mezok").on('click','.geom-down',function(){
        var target = $(this).data('target');
        if (typeof id != "undefined") {
            target = $(this).attr('target');
        }
        //gm_data-5
        var ri,ci,pos,data;
        if (target == 'data') {
            ri = $(this).closest('tr').index();
            ci = (1+(+$(this).closest('td').index()));
            pos = ri+','+(1+(+$(this).closest('td').index()));
            data = $("#upload-data-table tbody tr:eq("+ri+") td:nth-child("+ci+")").find('input').val();
        } else if (target=='header') {
            ri = $(this).closest('tr').index();
            ci = (1+(+$(this).closest('th').index()));
            pos = ri+','+(1+(+$(this).closest('th').index()));
            data = $("#upload-data-table thead tr:eq("+ri+") th:nth-child("+ci+")").find('input').val();
        } else {
            //default
            var id=$(this).attr('id');
            target = 'default';
            pos = id.substring(id.indexOf('-')+1,id.length);
            data = $("#default-"+pos).val();
        }
        //továbbadott értékek a felugró ablaknak
        //
        $(".goptchoice").attr('id',"gopt_"+target+"-"+pos);
        $(".mapclick").attr('id',"map_"+target+"-"+pos);
        //wkt string átadása &wkt=... formátumban
        var url = $('.mapclick').attr('href').replace(/&wkt=.+$/,'');
        $('.mapclick').attr('href',url);
        $(".mapclick").attr('href',$('.mapclick').attr('href')+'&wkt='+data+'&srid='+$('#upl-srid').val());
        $("#gpm").show();
    });
    /* Add extra plus rows to end of upload table 
     *
     * */
    $("#plusclick").click(function(){
        auto_save_wait = 1;
        var val = $("#plusrow").val();
        var td = "";
        
        var j=new Array();
        var o = 0;
        var rown;

        var o = $("#upload-data-table tbody").find("tr:last td:first");
        rown = 1 + (+o.find('span').html());

        var r = $("#upload-data-table tbody").find("tr:last").clone();
        for (var k = 0; k < val; k++) {
            r.find('td:first').find('span').text(rown + '.');
            var td = '';
            var f = 0;
            r.find("td").each(function() {
                var fr = '';
                if (f==0) {
                    fr = "class='headcol'";
                }
                td += "<td "+fr+">"+$(this).html()+"</td>";
                f++;
            });
            $("#upload-data-table > tbody:last").append('<tr id="tr-'+rown+'">'+td+'</tr>');
            $("#upload-data-table").find("tr:last").find('input').val('');
            //generate uniq id for input fields
            var uid = 1;
            $("#upload-data-table").find("tr:last").find('input').each(function(){
                if ($(this).hasClass('skip')) {
                    $(this).attr('id','skip-'+eval(rown-1));
                } else {
                    $(this).attr('id','uid'+rown+'-'+uid);
                }
                uid++;
            });
            $("#upload-data-table").find("tr:last").find('.skip').each(function(){
                $(this).val(1);
                $(this).addClass('autoskip');
                uid++;
            });

            // multiselect reinitialization
            $("#upload-data-table").find("tr:last").find('.fst-wrap').each(function(){
                selectelement = $(this).find('.fTSelect');
                selectelement.appendTo($(this).parent());
                $(this).remove();

            });
            $("#upload-data-table").find("tr:last").find('.fTSelect').each(function(){
                $(this).fTSelect({'placeholder':'&nbsp;','showSearch':false});
                
            });
            
            rown++;
        }

        var page = $(".page-group").find(".pure-button-active").find(".paging").attr('href');
        TableData = JSON.stringify(storeTblValues('upload-data-table'));
        $.post("ajax", {'plusrow':val,'upload_table_page':TableData,'read_visible_cells':1,'print':1,'page':page,'form_page_id':$("#form_page_id").val(),'form_page_type':$("#form_page_type").val()}, 
            function(data){
                auto_save_wait = 0;
            });
    });

    $('#formMenu > li').hover(
        function() {
            var i=$(this).find('h2').find('i');
            if ( i.hasClass('fa-caret-right') ) {
                i.removeClass('fa-caret-right');    
                i.addClass('fa-caret-down');
            }
        },
        function() {
            var i=$(this).find('h2').find('i');
            if ( i.hasClass('fa-caret-down') ) {
                i.removeClass('fa-caret-down');    
                i.addClass('fa-caret-right');    
            }
        }
    );
    $('#formMenu > li').click(function(e) {
        e.stopPropagation();
        var $el = $('ul',this);
        $('#formMenu > li > ul').not($el).slideUp();
        $el.stop(true, true).slideToggle(400);
    });
    $('#formMenu > li > ul > li').click(function(e) {
        e.stopImmediatePropagation();  
    });
    /* feltöltő form autocomplete list generating */
    $(".mezok").on('input','.genlist',function() {
        var ref = $(this).closest('td').index();
        if (ref==-1) ref = $(this).attr('id');

        $(".genlist").autocomplete({
	    source: function( request, response ) {
                //Default opcióban így nem működik!
                auto_save_wait = 1;
                $.post("ajax", {'genlist':1,'term':request.term,'id':ref,'form_page_id':$("#form_page_id").val()}, function (data) {
                    response(JSON.parse(data));
                    auto_save_wait = 0;
                });
            },
	        minLength: 2,
            /*close: function( event, ui ) {
             * //nyitva marad az autocomplete menu...
                $("#upload-data-table thead tr:eq(2) th:nth-child("+refh+")").find('input').trigger('input');
            }*/
        })
    });
   /* feltöltő form autocompletelist list generating 
    * */
    $(".mezok").on('input','.genwlist',function() {
        var ref = $(this).closest('td').index();
        if (ref==-1) ref = $(this).attr('id');
		$( ".genwlist" )
		// don't navigate away from the field on tab when selecting an item
			.on( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
					$( this ).autocomplete( "instance" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				source: function( request, response ) {
                                    auto_save_wait = 1;
				    $.post("ajax", {'genlist':1,'term': extractLast(request.term),'id':ref,'form_page_id':$("#form_page_id").val()}, function (data) {
                                        response(JSON.parse(data));
                                        auto_save_wait = 0;
                                    });
				},
				search: function() {
					// custom minLength
					var term = extractLast( this.value );
					if ( term.length < 2 ) {
						return false;
					}
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( ", " );
					return false;
				}
			});
	});

    $(".mezok").on('change','.list-warning',function() {
        if ($(this).find('option:selected').hasClass('list-ok')) {
            $(this).removeClass('list-warning');
            $(this).addClass('list-ok');
        }
    });
    $(".mezok").on('change','.list-ok',function() {
        if ($(this).find('option:selected').hasClass('list-warning')) {
            $(this).removeClass('list-ok');
            $(this).addClass('list-warning');
        }
    });
    $('#body').on("click","#text-size-increase",function() {
        $('.ci').css({'height':(parseInt($('.ci').css('height'))+4)+'px','font-size':(parseInt($('.ci').css('font-size'))+4)+'px'});
    });

    $('#body').on("click","#text-size-decrease",function() {
        $('.ci').css({'height':(parseInt($('.ci').css('height'))-4)+'px','font-size':(parseInt($('.ci').css('font-size'))-4)+'px'});
    });

    // active sheet changeing - excel file
    $("#change_exs_sheet").change(function(){
        
        $.post("ajax", {'change_exs_sheet':$(this).val()}, function (data) {
            location.reload();
        });
    });
    
    // interconnect choose - update upload button's color
    $("#iclist").change(function() {
        $("#file-upload-submit").addClass('button-success').prop("disabled", false);
    });

    // url choose - update upload button's color
    $("#url").change(function() {
        // javascript url validation
        // ...
        $("#file-upload-submit").addClass('button-success').prop("disabled", false);
    });

    // file choose - update upload button's color
    $("#file").change(function() {
        $(this).removeClass('button-secondary');
        $("#file-upload-submit").addClass('button-success').prop("disabled", false);
    });

    // dynamic select list on default drop-down menus
    $(".mezok").on('change','.default',function() {
        getNestedOptions(this,'default');
    });

    $('[data-nested="default"]').each(function() {
        getNestedOptions(this,'default');
    });

    /****
     * Upload table nested selects
     * ****/

    //$("#upload-data-table tbody").on("change",'.ci', function(e) {
    $("body").on("change",'.ci', function(e) {
        getNestedOptions(this,'table');
    });

    /* Training question - answers
     * */
    $("#training-form").on('click','#post_answers',function(event) {
        event.preventDefault();
        var qlog = new Array();
        $('.training-answer').each(function() {
            var log = 0;
            if ($(this).data('isright')==true) {
                // true
                if (this.checked==false) {
                    log = 1;
                }
            } else {
                if (this.checked==true) {
                    log = 1;
                }
            }
            if (typeof qlog[$(this).data('name')] === 'undefined' )
                qlog[$(this).data('name')] = log;
            else
                qlog[$(this).data('name')] += log;
        });
        var errors = 0;
        Object.keys(qlog).forEach(function(key, index) {    
            if(this[key]>0) {
                $("#"+key).css('color','red');
                errors++;
            } else {
                $("#"+key).css('color','black');
            }
        }, qlog);
        if (!errors) {
            window.location.href = $("#training_done_url").val();
        }
    });
    
    // radio or checkbox buttons as buttons in default fields
    $('.options li').click(function() {
        if ($(this).find('input[type="radio"]').length) {
            $(this).find('input[type="radio"]').prop('checked', true);
            $(this).find('input[type="radio"]').parent().addClass('button-secondary').siblings().removeClass('button-secondary');
            $(this).find('input[type="radio"]').addClass('default');
            $(this).siblings().find('input[type="radio"]').removeClass('default');
        } else if ($(this).find('input[type="checkbox"]').length) {
            if ($(this).find('input[type="checkbox"]:checked').length) {
                $(this).find('input[type="checkbox"]').prop('checked', false);
                $(this).find('input[type="checkbox"]').parent().removeClass('button-secondary');
                $(this).find('input[type="checkbox"]').removeClass('default');
            
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', true);
                $(this).find('input[type="checkbox"]').parent().addClass('button-secondary');
                $(this).find('input[type="checkbox"]').addClass('default');
            }
        }
    });
    // sticky - tuhumb click
    $(".sticky-thumb").click(function() {
        var ctrl = $(this).parent().find('.default').data('control');
        if (typeof ctrl != "undefined") {
            var spl = ctrl.split(',');
            if (spl.indexOf('sticky')==-1) {
                $(this).find('i').removeClass('fa-rotate-45');
                $(this).find('i').addClass('sticky');
                $(this).parent().find('.default').data('control',ctrl + ',sticky');
            } else {
                $(this).find('i').addClass('fa-rotate-45');
                $(this).find('i').removeClass('sticky');
                var index = spl.indexOf('sticky');
                spl.splice(index, 1);
                $(this).parent().find('.default').data('control',spl.join(','));
            }
        }
    });
});


/* Functions
 *
 * */

function save_sheet(echo=true) {
    // check other ajax in progress 
    if (auto_save_wait==1)
        return;
    
    // simple trick to refreshing php session
    $('#reloaddiv').load(projecturl+'includes/reload.php')

    //fejlécek
    var template = {
        slist:{},
        olist:{},
    };
    var arr = {};
    //file feltöltés - adatbázis oszlopok
    $("#sList").children().each(function(){
        var id = $(this).attr('id');
        var val= $(this).html();
        arr[id]=val;
    });
    template['slist']=arr;
    //file feltöltés - hozzárendelt oszlopok
    /*var arr = {};
    $(".oListElem").each(function(){
        var colvalue=$(this).find('li').eq(0).text();
        if (colvalue!='') {
            var ref = 1+(+$(this).closest('th').index());
            var id = $(this).find('li').eq(0).attr('id');
            var val= $(this).find('li').eq(0).html();
            arr[ref]=Array(id,val);
        }
        template['olist']=arr;
    });*/

    // web form - default data
    var mezo = {
        id:[],
        data:[],
    };
    $(".default").each(function(){
        mezo['id'].push($(this).attr('id'));
        mezo['data'].push($(this).val());
    });

    TableData = JSON.stringify(storeTblValues('upload-data-table'));

    $.post("ajax", { 'state_posth':JSON.stringify(template),
        'state_postd':TableData,
        'state_postm':JSON.stringify(mezo),
        'form_type':$('#sftype').val(),
        'form_id':$('#sfid').val() },
        function(data) {
            var retval = jsendp(data);
            if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='error') {
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                var today = new Date();
                var time = today.getHours() + ":" + (today.getMinutes()).pad();
                $("#save_state").html(time); 
            }
        });
    return;
}
Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}
//ez a függvény szervesen ide kötődik, de nem itt kéne lennie, csak most lusta vagyok a helyére rakni
function paging(event,page,read_visible_cells=1) {

    auto_save_wait = 1;
    if (event!='')
        event.preventDefault();
    var TableData;

    TableData = JSON.stringify(storeTblValues('upload-data-table'));
    //save current sheet content into session table content
    $.post("ajax", {'upload_table_page':TableData,'read_visible_cells':read_visible_cells,'print':1,'page':page,'form_page_id':$("#form_page_id").val(),'form_page_type':$("#form_page_type").val()}, function(data){
        if(data!='') {
            j = JSON.parse(data);
            $('#upload-data-table tbody').html(j['body']);
            $('#upload-data-table thead tr').eq(2).html(j['header']);
            
            var nested = $('[data-nested="parent"]');

            nested.each(function() {
                if ($(this).val() != '')
                    getNestedOptions(this,'table');
            });
            //change page
            $.post("ajax", {'upload-file-pager':page}, function(data){
                $('.page-group').html(data);
                //read skipped rows
                $.post("ajax", {'upload-file-skip':page}, function(data){
                    j = JSON.parse(data);
                    var n = 0;
                    //set skipped rows
                    $("#upload-data-table tbody tr").find("td:eq(0)").each(function(){
                        if(j[n]==1) {
                            $(this).find("input").trigger("click");
                        }
                        n++;
                    });

                    // multiselect dynamic update
                    $('.fTSelect').fTSelect({'placeholder':'&nbsp;','showSearch':false});

                    /*var spos = $('#rightTable').offset().left;
                    $(".headcol").each(function(){
                        $(this).css({left:eval(spos+36)});
                    });*/
                });
            });
            auto_save_wait = 0;
        }
    });
}

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}
/* colour ring box functions */
// set colour-ring-box value into table
function set_ring_value(vj) {
    var pos=$("#colour-ring-referenceid").val();
    var spl=pos.split(',');
    var rowIndex=spl[0];
    var colIndex=spl[1]; 
    $("#upload-data-table tbody tr:eq("+rowIndex+") td:nth-child("+colIndex+")").find('input').val(vj);
}

