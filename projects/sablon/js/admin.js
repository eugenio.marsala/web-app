/* trigger events for logined users
 * */
var CM = new Array();
var species_plus = new Array();

$(document).ready(function() {
    // ezt miért így csináltam????
    sendInputProfile();
    //$(".customScroll").mCustomScrollbar({theme:'dark',scrollInertia:100,scrollButtons:{ enable: true }});

    /*$('#tab_basic').on('click','.CodeMirror',function() {
        var mode = $(this).attr('name');
        var id = $(this).find('textarea').attr('id');
        var idx = jQuery.inArray(id, CM);
        if (idx == -1) {
            var editor = CodeMirror.fromTextArea($(this).find('textarea')[0], {
                lineNumbers: true,
                lineWrapping: true,
                mode: mode,
                scrollbarStyle: "simple",
                theme: "eclipse"
            });
            CM.push(id);
        } 
    });*/


    // ctrl + s
    var ctrlDown = false,
        ctrlKey = 17,
        cmdKey = 91,
        plusKey = 107,
        minusKey = 109;

    $(document).keydown(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
    }).keyup(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
    });

    /* catch tab press change to 4 whitespaces in private_map */
    $("#tab_basic").on('keydown', 'textarea', function(e) { 
        var keyCode = e.keyCode || e.which; 


        if (ctrlDown && (e.keyCode == plusKey )) {
            e.preventDefault();
            $(this).css({'font-size':(parseInt($(this).css('font-size'))+4)+'px'});
            return false;
        }
        else if (ctrlDown && (e.keyCode == minusKey )) { 
            e.preventDefault();
            $(this).css({'font-size':(parseInt($(this).css('font-size'))-4)+'px'});
            return false;
        }

        if (keyCode == 9) { 
            e.preventDefault(); 
            // call custom function here
            var start = $(this).get(0).selectionStart;
            var end = $(this).get(0).selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            $(this).val($(this).val().substring(0, start)
                + "    "
                + $(this).val().substring(end));

            // put caret at right position again
            $(this).get(0).selectionStart =
            $(this).get(0).selectionEnd = start + 4;
        } 
    }); 
    /* logout button click */
    $('body').on('click','#logout',function(){
        $.post(projecturl+"includes/logout.php", {html:1}, function(){
            document.location.href='index.php';
        }); 
    });
    /* Drop profile */
    $('body').on('click','#drop_my_account',function(e){
        e.preventDefault();
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: obj.confirmation_yes,
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post(projecturl+'includes/update_profile.php', {name:'dropmyaccount',text:1 },
                        function(data){
                            var retval = jsendp(data);
                            if (retval['status']=='error') {
                                alert(obj.confirmation_email_failed);
                            } else if (retval['status']=='fail') {
                                alert(obj.confirmation_email_failed);
                            } else if (retval['status']=='success') {
                                alert(obj.confirmation_email_sent);
                            }
                    }); 
                    $( this ).dialog( "close" );
                }
            },
            {
                text: obj.confirmation_no,
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).html( obj.drop_profile_confirm_text );

    });

    /* load subnav link from outside of topnav menu */
    $('body').on('click','.admin_direct_link',function (e) {
        e.stopPropagation();
        e.preventDefault();
        var url = $(this).attr("data-url");
        href = this.hash;

        if (typeof url !== "undefined") {
            $(href).load(projecturl+url);
        }
    });

    /* tabs menu
     * click on links
     * load AJAX pages
     * */
    $('#tabs').on('click','.tablink,#prodTabs a',function (e) {
        if ($(this).attr('id')=='noload') {
            return;
        }
        e.stopPropagation();
        e.preventDefault();
        $(".topnav").find("ul.subnav").slideUp('fast');  
        $(".to").removeClass('to-t');
        var url = $(this).attr("data-url");
        if (typeof url !== "undefined") {
            var pane = $(this), href = this.hash;
            if (pane.closest('ul').hasClass('subnav')) {
                pane.closest('.topnav').find(".button-tab").removeClass("button-tab");
                $('#admin').addClass("button-tab");
            } else {
                pane.parent().closest('ul').find(".button-tab").removeClass("button-tab");
                pane.addClass("button-tab");
            }
            // ajax load from data-url
            $(href).load(projecturl+url);
            //$(href).load(url,function(result){
                //$(".customScroll").mCustomScrollbar({theme:'dark',scrollInertia:100,scrollButtons:{ enable: true }});
            //});
            //refreshTimer();
        }
    });
    $('body').on('click','.module_submenu',function (e) {
        var url = $(this).attr("data-url");
        if (typeof url !== "undefined") {
            $("#tab_basic").load(projecturl+url);
        }
    });

    /* profile links click
     *
     * */
    $('#tab_basic').on('click','.profilelink',function(e) {
        e.stopPropagation();
        e.preventDefault();
        var href = this.hash;
        var url = $(this).attr("data-url");
        $(href).load(projecturl+url);
    });
    // project_admin page: set columns
    $('body').on('click','#proj_column',function() {
        var template = {
            colname:{},
            name:{},
            type:{},
            order:{},
            table:{},
            comment:{},
        };
        carr = new Array();
        narr = new Array();
        tarr = new Array();
        oarr = new Array();
        karr = new Array();
        marr = new Array();
        $('#cola').find('tr').each(function() {
            $(this).find('td').each(function() {
                var id = $(this).attr('id')
                var sId=id.substring(id.length - 1);
                var val = $(this).find('.element').val();
                
                if (sId==0) carr.push(val);
                else if (sId==1) narr.push(val);
                else if (sId==2) marr.push(val);
                else if (sId==3) tarr.push(val);
                else if (sId==4) oarr.push(val);
            });
       }); 
       template['colname']=carr;
       template['name']=narr;
       template['type']=tarr;
       template['order']=oarr;
       template['comment']=marr;
       template['table']=$(".main_table_selector").val();

       $.post(projecturl+"includes/mod_project.php", {t_post:JSON.stringify(template)},
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=dbcols',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=dbcols&mtable='+data);
            });
    });
    // project_admin page: set columns
    $("body").on('change','#cola',function(){
        $("#ficon").removeClass('fa-floppy-o'); 
        $("#ficon").addClass('fa-refresh fa-spin'); 
        $("#proj_column").removeClass('button-success'); 
        $("#proj_column").addClass('button-warning'); 
    });

    // project_admin page: set columns
    $('body').on('click','#add_new_column',function() {
        $.post(projecturl+"includes/mod_project.php", {new_column:1,nc_name:$('#column_name').val(),nc_type:$('#column_type').val(),nc_length:$('#column_length').val(),nc_default:$('#column_default').val(),nc_array:$('#column_array').val(),nc_comment:$('#column_comment').val(),nc_check:$('#column_check').val(),nc_label:$('#column_label').val(),table:$(".main_table_selector").val()},
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=dbcols',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=dbcols&mtable='+data);
            });

    });

    /* project admin - mod user's groups
     * */
    $('body').on('click','.mrb_send',function() {
            var id=$(this).attr('id');
            var rn=($(this).closest("tr")[0].rowIndex)-1;
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            var inputId='dc-'+widgetId;
            var selectedOptions = $.map($('#'+inputId+' :selected'),
                function(e) { return $(e).val(); } );
            var str = selectedOptions.join(',');
            var user_status=$('#sdc-'+widgetId).val();
            $.post(projecturl+"includes/mod_project.php", {user_id:widgetId,groups:str,user_status:user_status,html:1},
                function(data){
                    //reload page
                    //$('#tab_basic').load('includes/project_admin.php?options=members',function(result){});
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=members');
                    //$('#tagok-'+rn+'-5').html(data);
            }); 
    });
    // on project_admin page: mod groups' rights
    $('body').on('click','.mrg_send',function() {
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            
            var inputId='ng-'+widgetId;
            var selectedOptions = $.map($('.'+inputId+':checked'),
                function(e) { return $(e).val(); } );
            var nested = selectedOptions.join(',');

            $.post(projecturl+"includes/mod_project.php", {group_id:widgetId,nested:nested},
                function(data){
                    //reload page
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=groups');
                    if (data!='ok') {
                        // some error message
                        alert(data);
                    }
            }); 

    });
    $('body').on('click','.mrg_drop',function() {
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);

            $.post(projecturl+"includes/mod_project.php", {drop_group_id:widgetId},
                function(data){
                    //reload page
                    //$('#tab_basic').load('includes/project_admin.php?options=groups',function(result){});
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=groups');
                    if (data!='ok') {
                        // some error message
                        alert(data);
                    }
            }); 

    });
    // on project_admin page: add group
    $('body').on('click','#crgr',function() {
        var text = $("#crgr-name").val();
        $.post(projecturl+"includes/mod_project.php", {new_group:text},
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=groups',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=groups');
                //$("#crgr-name").val(text+' done');
        }); 
    }); 

    // taxon maintenance : search
    $("body").on('input','#taxon_sim',function(){
        var t = $(this);
        t.autocomplete({
            source: function( request, response ) {
                $.post(projecturl+"includes/getList.php", {type:'taxon',term:$("#taxon_sim").val()}, function (data) {
                        response(JSON.parse(data));
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                setTaxon( ui.item.id + "#" + ui.item.meta + "#" + ui.item.value);
                $(this).val('');
                return false;
            }
        });
    });
    /* remove species
     * */
    $('body').on('click','.remove-species',function() {
        for( var i = 0; i < species_plus.length-1; i++){ 
            if ( species_plus[i] === $(this).val()) {
                species_plus.splice(i, 1); 
            }
        }
        $(this).remove();
    });

    //project_admin, taxon search 
    $("body").on('click','#taxonname_load',function(e){
        e.preventDefault();
        var taxon_val = "";
        var trgm_string = new Array();
        var allmatch = 0;
        var onematch = 0;
        var qids = new Array();
        var qval = new Array();
        $("#tsellist").html('');

        //if ($("#taxon_sim").val()!='') {
        if ($('#taxon_trgm > option').length) {
            var stra = new Array();
            if ($("#allmatch").is(":checked")) { allmatch = 1; }
            if ($("#onematch").is(":checked")) { onematch = 1; }
            
            $("#taxon_trgm option:selected").each(function(){ 
                stra.push($(this).val()); 
            });
            if (stra.length==0){
                $("#taxon_trgm option").each(function(){ stra.push($(this).val()); });
            }    
            var trgm_string = "";
            species_plus = species_plus.concat(stra); 
        }
        if (species_plus.length){
           trgm_string = JSON.stringify(species_plus);
           species_plus = new Array();
        }

        var myVar = { trgm_string:trgm_string,allmatch:allmatch,onematch:onematch,tpost:1 }
        for (i=0;i<qids.length;i++) {
            myVar["qids_" + qids[i]] = qval[i];
        }
        $.post("ajax", myVar,
        function(data){
            $('#taxonlist').html(data);
        });
    });
    /* project admin: mapserver test
     * */
    $("body").on('click','.maptest',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length); //public,private
        var icon = $(this).find("i");
        $(icon).removeClass('fa-eye');
        $(icon).addClass('fa-spinner fa-spin');
        $.post(projecturl+"includes/afuncs.php", {'maptest':widgetId },
            function(data){
                if (data=='no') {
                    $(icon).removeClass('fa-spinner fa-spin');
                    $(icon).addClass('fa-eye');
                    $("#maptest-"+widgetId).html("an error occured while the request");
                } else if (data=='.') {
                    $("#maptest-"+widgetId).html("processing... click on view button later to refresh");
                } else {
                    $("#maptest-"+widgetId).html(data);
                    $(icon).removeClass('fa-spinner fa-spin');
                    $(icon).addClass('fa-eye');
                }
            });
    });
    /* project admin  - create function */
    $("body").on('click','.createfunction',function(){
        var id=$(this).attr('id');
        var table=id.substring(id.indexOf('-')+1,id.length);
        var funcs=id.substring(0,id.indexOf('-'));
        $.post(projecturl+"includes/afuncs.php", {'create-function':funcs,'table':table },
            function(data){
                alert(data);
                //$( "#dialog" ).text(data);
               //var isOpen = $( "#dialog" ).dialog( "isOpen" );
               //if(!isOpen) $( "#dialog" ).dialog( "open" );
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=functions');
            });
    });
    /* project admin  - create function */
    $("body").on('click','.enablefunction',function(){
        var id=$(this).attr('id');
        var table=id.substring(id.indexOf('-')+1,id.length);
        var funcs=id.substring(0,id.indexOf('-'));
        $.post(projecturl+"includes/afuncs.php", {'enable-trigger':funcs,'table':table },
            function(data){
                alert(data);
                //$( "#dialog" ).text(data);
               //var isOpen = $( "#dialog" ).dialog( "isOpen" );
               //if(!isOpen) $( "#dialog" ).dialog( "open" );
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=functions');
            });
    });

    /* profile - project admin - upload forms
     * delete form
     * */
    $('#tab_basic').on('click','#delform',function(){
        var tag = $(this).attr("data-tag");
        if (typeof tag !== "undefined") {
            $.post(projecturl+"includes/afuncs.php", {'delform':tag },
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=upload_forms',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms');
            });
        }
    });
    /* profile - project admin - upload forms
     * edit form
     * */
    $('#tab_basic').on('click','#editform',function(){
        var tag = $(this).attr("data-tag");
        if (typeof tag !== "undefined") {
            //$('#tab_basic').load('includes/project_admin.php?options=upload_forms&edit_form='+tag,function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms&edit_form='+tag+'&simplified_list=0');
        }
    });
    $('#tab_basic').on('click','.list-editor',function() {
        $(".list-editor").prop('disabled',false);
        var id=$(this).attr('id');
        var wid=id.substring(id.indexOf('-')+1,id.length);
        var t = $(this).attr("data-simplified");
        var d = $(this).text();
        $("#list-editor-return").val(id);
        $("#list-editor-json").text(d);
        $("#list-editor-json").val(d);
        $("#list-editor-simple").text(t);
        $("#list-editor-simple").val(t);
        $("#list-editor").show();
        $("#list-editor").draggable();
        $(this).prop('disabled',true);
    });

    $('#tab_basic').on('change','#list-editor-json',function() {
        $.post(projecturl+"includes/afuncs.php",{'form-list-convert':'json','json':$("#list-editor-json").val(),'text':$("#list-editor-simple").val()},function(data){
            
            $("#list-editor-simple").text(data);
            $("#list-editor-simple").val(data);
        });
    });
    $('#tab_basic').on('change','#list-editor-simple',function() {
        $.post(projecturl+"includes/afuncs.php",{'form-list-convert':'text','json':$("#list-editor-json").val(),'text':$("#list-editor-simple").val()},function(data){
            
            $("#list-editor-json").text(data);
            $("#list-editor-json").val(data);
        });
    });

    $('#tab_basic').on('focus','#list-editor-simple',function() {
        $('#list-editor-help-text').hide('slow');
    });

    $('#tab_basic').on('dblclick','#list-editor-help-text',function() {
        $(this).hide('slow');
    });

    $('#tab_basic').on('click','#list-editor-help-box',function() {
        $("#list-editor-help-text").show('slow');
    });

    $('#tab_basic').on('click','#list-editor-cancel',function() {
        var id = $("#list-editor-return").val();
        $("#list-editor").hide();
        $("#"+id).prop('disabled',false);
    });
    $('#tab_basic').on('click','#list-editor-save',function() {
        var id = $("#list-editor-return").val();

        $("#"+id).val($("#list-editor-json").val());
        $("#"+id).text($("#list-editor-json").val());
        $("#"+id).attr("data-simplified",$("#list-editor-simple").val());
        $("#list-editor").hide();
        $("#"+id).prop('disabled',false);
    });

    /* profile - project admin - upload forms
     * activate/deactivate form
     * */
    $('#tab_basic').on('click','#deacform',function(){
        var tag = $(this).attr("data-tag");
        var val = $(this).attr("data-val");
        if (typeof tag !== "undefined") {
            if (typeof val === "undefined") { 
                val = '';
            }
            $.post(projecturl+"includes/afuncs.php", {'deacform':tag,'active':val },
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=upload_forms',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms');
            });
        }
    });
    /* profile - project admin - upload form
     * select a field: change background color
     * */
    $("#tab_basic").on('click','.uplf_ckb',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($(this).is(":checked")) {
            if ($('#uplf_obl_y-'+widgetId).is(":checked")) {
                $('#uplf_color-'+widgetId).css('background-color', '#FF636E');//set red
            }
            else if ($('#uplf_obl_n-'+widgetId).is(":checked")) {
                $('#uplf_color-'+widgetId).css('background-color', '#B6B6B6'); //set gray
            }
            else if ($('#uplf_obl_s-'+widgetId).is(":checked")) {
                $('#uplf_color-'+widgetId).css('background-color', '#FFB6BB'); //set pink
            } 
            else {
                $('#uplf_color-'+widgetId).css('background-color', '#FF636E');//set red
                $('#uplf_obl_y-'+widgetId).attr('checked',true);
            }
        } else {
            $('#uplf_color-'+widgetId).css('background-color', '#FFFFFF'); //not selected
        }
    });
    /* project admin - upload form
     * */
    $("#tab_basic").on("change","#form_table",function(){
        var table = $(this).val();
        //$('#tab_basic').load('includes/project_admin.php?options=upload_forms&new_form_table='+table,function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms&new_form_table='+table);
    });
    /* project admin - upload form
     * input mode: list/text change
     * */
    $("body").on('change','.uplf_type',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($('#uplf_type-'+widgetId).val()=='boolen') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_list-'+widgetId).val('false,true,');
        } else if ($('#uplf_type-'+widgetId).val()=='autocomplete') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_list-'+widgetId).val('');
            $('#uplf_list-'+widgetId).attr("placeholder", "SELECT:table.column");
        } else if ($('#uplf_type-'+widgetId).val()=='list') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_list-'+widgetId).val('');
            $('#uplf_list-'+widgetId).attr("placeholder", "name:value,name:value OR \nvalue,value,value OR \nSELECT:table.column");
        } else if ($('#uplf_type-'+widgetId).val()=='crings') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_list-'+widgetId).val('');
            $('#uplf_list-'+widgetId).attr("placeholder", "name:value,name:value OR \nvalue,value,value OR \nSELECT:table.column");
        } else if ($('#uplf_type-'+widgetId).val()=='text' || $('#uplf_type-'+widgetId).val()=='numeric' || $('#uplf_type-'+widgetId).val()=='time' || $('#uplf_type-'+widgetId).val()=='point' ||  $('#uplf_type-'+widgetId).val()=='line' || $('#uplf_type-'+widgetId).val()=='polygon' || $('#uplf_type-'+widgetId).val()=='wkt') {
            $('#uplf_list-'+widgetId).val('');
        } else {
            $('#uplf_list-'+widgetId).val('');
        }
    });
    /* choose 
     *
     * */
    $("body").on('change','#file_manager_choose_table',function(){
        var t = $(this).val();
        //$('#tab_basic').load('includes/project_admin.php?options=files&choose='+t,function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=files&choose='+t);
    });
    
    /* project admin - upload form
     * obligatory: change background color
     * */
    $("body").on('change','.uplf_obl',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($(this).is(":checked")) {
            if($(this).val()==1)
                $('#uplf_color-'+widgetId).css('background-color', '#FF636E') //set red
            else if($(this).val()==3)
                $('#uplf_color-'+widgetId).css('background-color', '#FFB6BB') //set pink
            else
                $('#uplf_color-'+widgetId).css('background-color', '#B6B6B6') // set gray
            $('#uplf_ckb-'+widgetId).attr('checked','true');
        }
    });
    $("body").on('change','#form_access',function(){
        if($(this).val()==2) {
            $("#form_group_access").attr('disabled',false);
            $("#form_group_access").removeClass('button-passive');    
        } else {
            $("#form_group_access").attr('disabled',true);
            $("#form_group_access option:selected").removeAttr("selected");
            $("#form_group_access").addClass('button-passive');    
        }
    });
    /* project admin - upload form - upload a list file
     * Save file content through afuncs.php
     * */
    $("body").on('change','.uplf_file',function(event) {
        //ennek a tartalma kimehetne egy paraméterezhető függvénybe...
        //file_auto_upload('4096',ajax,[files=>file,list=>0])
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var files = document.getElementById("uplf_file-"+widgetId).files;
        //file_upload(files,'list');
        var u = new Uploader;
        u.param('list');
        u.file_upload(files);
    });

    //Array.prototype.diff = arr1.filter(x => arr2.includes(x));
    Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
    };

    $("body").on('input','.available_item_trigger',function(event) {
        
        var nameArray = new Array();
        var allItems = new Array();
        var o = 1;
        $('.available_item_trigger').each(function() {
            nameArray.push(Number($(this).val()));
            allItems.push(o);
            o++;
        });
        var uA = nameArray.filter(function (el) {
          return el != null;
        });

        var uAdiff = allItems.diff(uA)
	
        var dataList = $("#available_items");
	dataList.empty();

        for (var i = 0; i < uAdiff.length; i++) {
            dataList.append("<option value='" + uAdiff[i] + "'>");
        }
    });

    /* Turn off local language files for the next reload
     * */
    $("body").on('click','#local-language-files-export',function(event) {
        $.post("ajax", {lang_export:'local'},function(data){
        });
    });
    /* Turn off local language files for the next reload
     * */
    $("body").on('click','#local-language-files-turnoff',function(event) {
        $.post("ajax", {turn_off_lang:1},
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=languages',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=languages');
        });
    });
    /* project admin - language files
     * Save file content through afuncs.php
     * */
    $("body").on('change','.language_file',function(event) {
        //ennek a tartalma kimehetne egy paraméterezhető függvénybe...
        //file_auto_upload('4096',ajax,[files=>file,list=>0])
        var id=$(this).attr('id');
        var files = document.getElementById("language_file").files;
        var u = new Uploader;
        u.param('language-files');
        u.file_upload(files);
        //$('#tab_basic').load('includes/project_admin.php?options=languages',function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=languages');
    });
    /* project admin - language files
     * Save file content through afuncs.php
     * */
    $("body").on('change','#module_file-upload',function(event) {
        var id=$(this).attr('id');
        var files = document.getElementById("module_file-upload").files;
        var u = new Uploader;
        u.param('module-files');
        u.param2($(this).attr('name'));
        u.file_upload(files);
        //$('#tab_basic').load('includes/project_admin.php?options=modules',function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=modules');
  
    });
    /* project_admin - upload forms - submit form
     * */
    $("body").on('submit','#upl-form',function(event) {    
        event.preventDefault();
        var template = {
            name:{},access:{},type:{},description:{},type:{},length:{},count:{},file:{},list:{},obl:{},id:{},fullist:{},
            form_name:{},form_type:{},form_access:{},edit_form_value:{},form_group_access:{},form_description:{},form_table:{},form_srid:{},form_data_access:{},
            position_order:{}
        };
        var a = new Array(),b = new Array(),c = new Array(),d = new Array(),e = new Array(),f = new Array(),g = new Array(),h = new Array(),i = '',j = new Array(),k = '',m = '',n = new Array(),p = new Array(),q = new Array(),o='',r='',t = new Array(),s = new Array(),ps = new Array(),sr = '',no = new Array(),po = new Array();
        i = $("#form_name").val();
        j = $("#form_type").val();
        k = $("#form_access").val();
        m = $("#edit_form_value").val();
        n = $("#form_group_access").val();
        no = $("#form_data_access").val();
        o = $("#form_description").val();
        r = $("#form_table").val();
        sr = $("#form_srid").val();

        $(".uplf_ckb").each(function() {
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            if ($("#uplf_ckb-"+widgetId).is(":checked")) {
                a.push($("#uplf_description-"+widgetId).val());
                b.push($("#uplf_type-"+widgetId).val());
                c.push($("#uplf_length-"+widgetId).val());
                d.push($("#uplf_count-"+widgetId).val());
                e.push($("#uplf_file-"+widgetId).val());
                f.push($("#uplf_list-"+widgetId).val());
                g.push($("input[name='obl-"+widgetId+"']:checked","#upl-form").val());
                h.push(widgetId);
                p.push($("#uplf_fullist-"+widgetId).val());
                q.push($("#uplf_defval-"+widgetId).val());
                t.push($("#uplf_api_params-"+widgetId).val());
                s.push($("#uplf_relation-"+widgetId).val());
                ps.push($("#uplf_pseudocol-"+widgetId).val());
                po.push($("#uplf_order-"+widgetId).val());
            }
        });
        template['description']=a;template['type']=b;template['length']=c;template['count']=d;template['file']=e;template['list']=f;template['obl']=g;template['id']=h;
        template['form_name']=i;template['form_type']=j;template['form_access']=k;template['edit_form_value']=m;template['form_group_access']=n;template['fullist']=p;
        template['default_value']=q;template['form_description']=o;template['form_table']=r;
        template['api_params']=t;template['relation']=s;template['pseudo_columns']=ps;template['form_srid']=sr;template['form_data_access']=no;template['position_order']=po;

        $.post(projecturl+"includes/mod_project.php", {new_form:JSON.stringify(template)},
        function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') 
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms');
        });
    });

    //show-hide min-max value input field
    $("body").on('change','.uplf_length',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if($(this).val()=='minmax') {
            $("#uplf_count-"+widgetId).attr('placeholder','0:20');
        } else if ($(this).val()=='regexp') {
            $("#uplf_count-"+widgetId).attr('placeholder','/[a-z]{2}\.[a-z]{2}\|[a-z]{2}\.[a-z]{2}/');
        } else {
            $("#uplf_count-"+widgetId).attr('placeholder','');
        }
    });

    /* project_admin - upload forms */
    $("body").on('change','.fullist_option',function(){
        var this_id = $(this).attr('id');
        var this_widgetId=this_id.substring(this_id.indexOf('-')+1,this_id.length);
        var this_val= $(this).val();
        if (this_val==0) return;
        $(".fullist_option").each(function() {
            var id = $(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            if(this_id!=id && $(this).val()==1){
                    $("#uplf_fullist-"+widgetId).val('0').change();
            }
        });
    });
    /* project_admin, module update */ 
    $("body").on('click','.module_update',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        var name=$("#module-name-"+widgetId).val();
        var func=$("#module-function-" +widgetId).val();
        var file=$("#module-file-" +widgetId).val();
        var pars=$("#module-params-" +widgetId).val();
        var enab=$("#module-ena-" + widgetId).val();
        var acce=$("#module-access-" + widgetId).val();
        var gacce=$("#module-gaccess-" + widgetId).val();
        var mtable=$(".main_table_selector").val();

        $.post(projecturl+"includes/mod_project.php", { 'update_modules':widgetId,'file':file,'name':name,'func':func,'params':pars,'enab':enab,'access':acce,'mtable':mtable,'gaccess':gacce },
        function(data) {
            var retval = jsendp(data);
            var mtable = '';
            if (retval['status']=='error') {
                //$("#response").html(retval['message']);
            } else if (retval['status']=='fail') {
                //$("#response").html(retval['data']);
            } else if (retval['status']=='success') 
                mtable = retval['data'];

            //$('#tab_basic').load('includes/project_admin.php?options=modules',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=modules&mtable='+mtable);
        });
    });
    /* project_admin, query set update */ 
    $("body").on('click','.q_update',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        var name=$("#qnam-" +widgetId).val();
        var typ=$("#qtype-" +widgetId).val();
        var query=$("#qquery-" + widgetId).val();
        var cname=$("#qcname-" + widgetId).val();
        var rst=$("#qrst-" + widgetId).val();
        var geom_type=$("#qgt-" + widgetId).val();
        var enab=$("#qena-" + widgetId).val();

        $.post(projecturl+"includes/update_mapfile.php", { 'qq':widgetId,'name':name,'type':typ,'query':query,'enab':enab,'rst':rst,'cname':cname,'geom_type':geom_type },
        function(data){
            //$('#tab_basic').load('includes/project_admin.php?options=query_def',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=query_def');
        });
    });

    /* project_admin, openlayers update */ 
    $("body").on('click','.opl_update',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        var order=$("#oplorder-"+widgetId).val();
        var name=$("#oplnam-" +widgetId).val();
        var typ=$("#opltype-" +widgetId).val();
        var def=$("#opldef-" + widgetId).val();
        var url=$("#oplurl-" + widgetId).val();
        var map=$("#oplmap-" + widgetId).val();
        var desc=$("#opldesc-" +widgetId).val();
        var enab=$("#oplena-" + widgetId).val();
        var mslayer=$("#oplmslayer-" + widgetId).val();

        $.post(projecturl+"includes/update_mapfile.php", { 'op':widgetId,'order':order,'name':name,'type':typ,'def':def,'url':url,'map':map,'desc':desc,'enab':enab,'mslayer':mslayer },
        function(data){
            //$('#tab_basic').load('includes/project_admin.php?options=openlayes_def',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=openlayes_def');
        });
    });
    //project_admin, mapfile update
    $("body").on('click','.map_update',function(){
        var k = $(this).attr('id');
        //$.post(projecturl+"includes/update_mapfile.php", { 'map_post':$("#" + k + "_map").val(),'p':k,'map_layers':$('#mapfile-form').serialize() },
        var values = $('#mapfile-form').serializeArray();
        values.push({
            name: "map_post",
            value: $("#" + k + "_map").val()
        });
        values.push({
            name: "p",
            value: k
        });
        values = jQuery.param(values);
        
        $.post(projecturl+"includes/update_mapfile.php", values,
        function(data){
            //$("#" + k + "_map").html(data);
            //$('#tab_basic').load('includes/project_admin.php?options=mapserv',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=mapserv');
        });
    });
    /* project_admin - mapserver */
    $("body").on('change','#public_map',function(){
        $("#pub-icon").addClass('fa-spin'); 
        $("#public").removeClass('button-success'); 
        $("#public").addClass('button-warning'); 
    });
    /* project_admin - mapserver */
    $("body").on('change','#private_map',function(){
        $("#priv-icon").addClass('fa-spin'); 
        $("#private").removeClass('button-success'); 
        $("#private").addClass('button-warning'); 
    });

    /* 
     * profile -> mainpage: POLYGON SETTINGS 
     * */

    // change save button color to green when changing polygon name
    $("body").on('change','.polygon_name',function(){
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var save_button = $(this).parent().find('.polygon_name_save');
        save_button.removeClass('button-secondary');
        save_button.removeClass('button-gray');
        save_button.addClass('button-success');
    });
    // access options updates: private, public, logined, project
    $("body").on('change','.polygon_access',function(){
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        $.post("ajax",{'own_polygon_update':widgetId,'own_poly_access':$(this).val()},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') {
                alert('Ok.');
            }
        });
    });
    // access options updates: private, public, logined, project
    $("body").on('change','.polygon_forceaccess',function(){
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        $.post("ajax",{'set_polygon_update':widgetId,'set_poly_access':$(this).val()},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') {
                alert('Ok.');
            }
        });
    });

    //save button
    // update own polygon name 
    $("body").on('click','.polygon_name_save',function(){
        var id = $(this).attr('id');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var this_ = $(this);

        polygon_name = $(this).parent().find('.polygon_name').val();

        $.post("ajax",{'own_polygon_update':widgetId,'own_poly_name':polygon_name},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
                this_.removeClass('button-success');
                this_.addClass('button-fail');
            } else if (retval['status']=='fail') {
                alert(retval['data']);
                this_.removeClass('button-success');
                this_.addClass('button-fail');
            } else if (retval['status']=='success') {
                alert(retval['data']);
                this_.removeClass('button-gray');
                this_.removeClass('button-success');
                this_.addClass('button-secondary');
            }
        });
    });
    //save button
    // update force polygon name 
    $("body").on('click','.polygon_forcename_save',function(){
        var id = $(this).attr('id');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var this_ = $(this);

        polygon_name = $(this).parent().find('.polygon_name').val();

        $.post("ajax",{'set_polygon_update':widgetId,'set_poly_name':polygon_name},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
                this_.removeClass('button-success');
                this_.addClass('button-fail');
            } else if (retval['status']=='fail') {
                alert(retval['data']);
                this_.removeClass('button-success');
                this_.addClass('button-fail');
            } else if (retval['status']=='success') {
                alert(retval['data']);
                this_.removeClass('button-gray');
                this_.removeClass('button-success');
                this_.addClass('button-secondary');
            }
        });
    });

    // select / upload view options for own polygons
    $("body").on('click','.polygon_show_options',function(){
        var id = $(this).attr('id');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var current_view_setting=$(this).val();
        var type = '';
        if ($(this).hasClass('sel'))
            type = 'select';
        if ($(this).hasClass('upl'))
            type = 'upload';

        $.post("ajax",{'showhide_polygon':widgetId,'show':current_view_setting,'type':type},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') {
                if ( $("#i"+id).hasClass('fa-eye') ) {
                     $("#i"+id).removeClass('fa-eye');
                     $("#"+id).removeClass('button-gray');
                     $("#"+id).removeClass('button-secondary');
                     $("#"+id).removeClass('button-error');
                     $("#i"+id).addClass('fa-eye-slash');
                     $("#"+id).addClass('button-warning');
                } else {
                     $("#i"+id).removeClass('fa-eye-slash');
                     $("#"+id).removeClass('button-gray');
                     $("#"+id).removeClass('button-secondary');
                     $("#"+id).removeClass('button-error');
                     $("#i"+id).addClass('fa-eye');
                     $("#"+id).addClass('button-warning');
                }
                if (retval['data'] == 'none') {
                    $("#ops_"+widgetId).val('none');
                    $("#opu_"+widgetId).val('none');
                }
                else if (retval['data'] == 'only-select') {
                    $("#ops_"+widgetId).val('only-select');
                    $("#opu_"+widgetId).val('only-select');
                }
                else if (retval['data'] == 'only-upload') {
                    $("#ops_"+widgetId).val('only-upload');
                    $("#opu_"+widgetId).val('only-upload');
                }
                else if (retval['data'] == 'select-upload') {
                    $("#ops_"+widgetId).val('select-upload');
                    $("#opu_"+widgetId).val('select-upload');
                }
            }
        });
    });
    // select / upload view options for forced polygons
    $("body").on('click','.polygon_showforce_options',function(){

        var id = $(this).attr('id');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_')).match(/\d+$/)[0];
        var current_view_setting=$(this).val();
        var type = '';
        if ($(this).hasClass('sel'))
            type = 'select';
        if ($(this).hasClass('upl'))
            type = 'upload';

        var force_users = $("#poly-users-force"+row_id+"_"+widgetId).val();

        $.post("ajax",{'force_showhide_polygon':widgetId,'show':current_view_setting,'type':type,'force_users':force_users,def_row:0},function(data) {
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') {
                alert("Updated state: " + retval['data']);
                
                // set eye
                if ( $("#i"+id).hasClass('fa-eye') ) {
                     $("#i"+id).removeClass('fa-eye');
                     $("#"+id).removeClass('button-gray');
                     $("#"+id).removeClass('button-secondary');
                     $("#"+id).removeClass('button-error');
                     $("#i"+id).addClass('fa-eye-slash');
                     $("#"+id).addClass('button-warning');
                } else {
                     $("#i"+id).removeClass('fa-eye-slash');
                     $("#"+id).removeClass('button-gray');
                     $("#"+id).removeClass('button-secondary');
                     $("#"+id).removeClass('button-error');
                     $("#i"+id).addClass('fa-eye');
                     $("#"+id).addClass('button-warning');
                }
                
                // set value
                if (retval['data'] == 'none') {
                    $("#ops"+row_id+"_"+widgetId).val('none');
                    $("#opu"+row_id+"_"+widgetId).val('none');
                }
                else if (retval['data'] == 'only-select') {
                    $("#ops"+row_id+"_"+widgetId).val('only-select');
                    $("#opu"+row_id+"_"+widgetId).val('only-select');
                }
                else if (retval['data'] == 'only-upload') {
                    $("#ops"+row_id+"_"+widgetId).val('only-upload');
                    $("#opu"+row_id+"_"+widgetId).val('only-upload');
                }
                else if (retval['data'] == 'select-upload') {
                    $("#ops"+row_id+"_"+widgetId).val('select-upload');
                    $("#opu"+row_id+"_"+widgetId).val('select-upload');
                }
            }
        });
    });
    // force users update
    $("body").on('click','.poly_users_force',function(){
        var id = $(this).attr('id');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_')).match(/\d+$/)[0];
        var force_users = $(this).val();

        var current_view_setting =  $("#ops"+row_id+"_"+widgetId).val();

        var def_row = $(this).parent().find('.admin-option');


        $.post("ajax",{'force_showhide_polygon':widgetId,'show':current_view_setting,'type':'','force_users':force_users,'def_row':def_row.length},function(data) {
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') {
                alert("Updated state: " + retval['data']);
                
                if (def_row.length != 0) {
                    $("#tab_basic").load(projecturl+def_row.attr("data-url"));
                }
            }
        });
    });
    // drop own polygon
    // drop polygons by admin
    $("body").on('click','.droppolygon',function(){
        $(this).css('background-color','gray');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var id = $(this).attr('id');
        var row_id = id.substring(id.indexOf('_'),id.length).match(/\d+$/)[0];
        var name = $("#ope-name"+row_id+"_"+widgetId).val();

        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: "Yes, I'm sure!",
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'own_polygon_drop':widgetId},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            $("#ope-name_"+widgetId).closest('tr').hide();
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Oh, no!",
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop this polygon?" );
    });

    // drop own polygon
    // drop polygons by admin
    $("body").on('click','.dropforcepolygon',function(){
        $(this).css('background-color','gray');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var id = $(this).attr('id');
        var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_')).match(/\d+$/)[0];
        var name = $("#ope-name"+row_id+"_"+widgetId).val();

        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: "Yes, I'm sure!",
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'force_polygon_drop':widgetId},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=box_load_selection');
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Oh, no!",
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop `"+name+"` polygon?" );
    });


    /* show saved queries page on profile
     * drop selected queries
     * */
    $("#tab_basic").on("click","#dropsq",function(e) {
        var d = [];
        $(".dropsq:checked").each(function() {
            d.push(this.value);
        });
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: "Yes, I'm sure!",
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'dropsq':d},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/profile.php?options=savedqueries');
                            //setTimeout(function(){location.reload();},1000);
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Oh, no!",
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop the selected "+d.length+" queries?" );
    });

    $("#tab_basic").on("click","#droprq",function(e) {
        var d = [];
        $(".droprq:checked").each(function() {
            d.push(this.value);
        });
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: "Yes, I'm sure!",
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'droprq':d},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/profile.php?options=savedresults');
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Oh, no!",
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop the selected "+d.length+" results?" );
    });


    /* drop saved import */
    $("#body").on("click","#dropiups",function(e) {
        var d = [];
        $(".dropiups:checked").each(function() {
            d.push(this.value);
        });
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: "Yes, I'm sure!",
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'drop_import':d},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/profile.php?options=saveduploads');
                            //setTimeout(function(){location.reload();},1000);
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Oh, no!",
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop the selected "+d.length+" imports?" );
    });

    // save import as template
    $("#body").on("click","#saveiups",function(e) {
        var d;
        var i;
        $(".dropiups:checked").each(function() {
            d = this.value;
            i = $("#name_"+this.value).val();
            $.post("ajax",{'save_import_template':d,'name':i},function(data){
            });
        });
        //setTimeout(function(){location.reload();},1000);
    });

    /* profile api key manage */
    $("#body").on('click','.dropapikey',function() {
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var tokenType = $(this).data('tokentype');
        $.post("ajax",{'drop_apikey':widgetId,'tokenType':tokenType},function(data){
            //$('#tab_basic').load('includes/profile.php?',function(result){});
            $('#tab_basic').load(projecturl+'includes/profile.php?options=apikeys');
        });
    });

    /* profile - create new project
     * add new database fields row
     * */
    $("body").on('click','#fields-def_add-cell',function(e) {
        e.preventDefault();
        $("#fields_def_table").append("<tr><td><input name='name[]' placeholder='"+str_obligatory+"' require></td><td><input name='desc[]' placeholder='"+str_obligatory+"' require></td><td><textarea name='meta[]' placeholder='"+str_recommended+"'></textarea></td><td><select name='type[]'><option value='numeric'>numeric</option><option value='integer'>integer</option><option value='character varying(255)'>string</option><option value='text'>text</option><option value='date'>datum</option><option value='timestamp without time zone'>datetime</option></select></td></tr>");
    });
    /* profile - create new project */
    $("body").on('dblclick',"#new_proj_messages",function(){
        $(this).hide();
    });
    /* profile - invitation */
    $( '#body' ).on( "click","#soi", function() {
         sendInvitation();
    });
    // drop invites button click
    $('#body').on('click','#dropInv',function(){
        var d = [];
        $(".delinv:checked").each(function() {
            d.push(this.value);
        });
        $.post(projecturl+"includes/afuncs.php", {delinv:d}, function(data){
            var url = $("#invites").attr("data-url");
            if (typeof url !== "undefined") {
                var pane = $("#invites"), href = $('#invites').attr('href');
                // ajax load from data-url
                //$(href).load(url,function(result){ pane.tab('show'); });
                $(href).load(projecturl+url);
            }
        });
    });
    /* profile - new project - generate password */ 
    $("body").on('change','#project_dir',function () {
        $("#pgsql_admin").val($(this).val()+'_admin');
        $("#pgsql_passwd").val(Math.random().toString(36).slice(2));
    });
    /* profile - create new project
     * create new project button
     * */
    $("body").on('click','#send_newproj_req',function () {
        var d = $(this).closest('form').serialize();
        $.post(projecturl+"includes/create_new_project.php", d,
        function(data) {
            $("#new_proj_messages").html(data);
            $("#new_proj_messages").css({height: '600px'});
            $("#new_proj_messages").show();
            //$('#message').css({top:'50%',left:'50%',margin:'-'+($('#message').height() / 2)+'px 0 0 -'+($('#message').width() / 2)+'px'});
            //$('#message').show();
        });
    });
    /* FILTERBOX
     * This called when somebody read shp files using filterbox_load_layer()
     * steps:
     * 1.: move files to a temporary place
     * 2.: read data from files
     * 3.: draw polygon
     * */
    $('body').on('click', '#shp_upload', function(e){
        e.preventDefault();
        //get fa icon id
        var xicon = $(this).find('i').attr('id');
        //we are get only the first element of the form 'form'!
        var formData = new FormData()
        var shp_label = $('#shp_label').val();

        var files = document.getElementById("shpfile").files;
        for(var i in files) {
            formData.append("files[]", files[i]);
        }
        formData.append("load-files", 1);
        //formData.append("filename", 'shpfile');
        formData.append("ext", 'shp');
        $.ajax({
            url: 'ajax',
            type: 'POST',
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            success: function (data) {
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['data']);
                } else if (retval['status']=='success') {
                    //homokórás, forgó ikon kell ide...
                    //$("#"+xicon).removeClass('fa-upload');
                    //$("#"+xicon).addClass('fa-spinner fa-pulse');
                    $.post("ajax", {'shaperead':1,'shp_label':shp_label},
                    function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['data']);
                        } else if (retval['status']=='success') {
                            alert('Processing shp OK!');
                        }

                        $('#tab_basic').load(projecturl+'includes/profile.php?options=get_own_profile');
                        //nem csinálja meg a szelekciót, csak felrajzolja!!!
                        //zoomol
                        //features_length = drawPolygonFromWKT(data,1);
                        //lbuf_reread();
                    });
                }
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    // project_admin page: syslog
    $("body").on('change','#syslog_source',function(){
        var n = $(this).val();
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=server_logs&source='+n);
    });
    $("body").on('click','#syslog_refresh',function(){
        var n = $('#syslog_source').val();
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=server_logs&source='+n);
    
    });
    
    // project_admin page: main table choose in several pages
    $("body").on('change','.main_table_selector',function() {
        var id = $(this).attr('id');
        var n = $(this).val();
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options='+id+'&mtable='+n);
    
    });

    $("body").on('click','.main_table_refresh',function(){
        var id = $(this).attr('id');
        var n = $('.main_table_selector').val();
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options='+id+'&mtable='+n);
    
    });
    // r-shiny server control
    $("body").on('click','#control-R-server',function(e) {
        var control = $(this).val();
        $.post('ajax',{'r_server':control},function(data){
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=r_server');
        });
    });
    // create table call
    $("body").on('click','#create_new_table',function(e) {
        e.preventDefault();
        $.post('ajax',{'create_new_table':$("#new_table").val(),'new_table_comment':$("#new_table_comment").val()},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['data']);
            } else if (retval['status']=='success') {
                alert('Ok');
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=dbcols');
            }
        });
    });

    // photo filter button toggle
    $("body").on('click',"#email-visible",function(){
        var e;
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).removeClass('button-success');
            $(this).addClass('button-passive');
            e = 0;
        } else {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).addClass('button-success');
            $(this).removeClass('button-passive');
            e = 1;
        }

        $.post('ajax',{'email-visible':e},function(data){
            $('#tab_basic').load(projecturl+'includes/profile.php?options=get_own_profile');
        });
    });
    // photo filter button toggle
    $("body").on("click","#email-receive",function(){
        var e;
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).removeClass('button-success');
            $(this).addClass('button-passive');
            e = 0;
        } else {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).addClass('button-success');
            $(this).removeClass('button-passive');
            e = 1;
        }
        $.post('ajax',{'email-receive':e},function(data){
            $('#tab_basic').load(projecturl+'includes/profile.php?options=get_own_profile');
        });

    });

    $("body").on("submit","#update_translations",async function(ev) {
        ev.preventDefault();
		try {
            const form_data = new FormData(this);
            const results = await $.ajax({
                url: "ajax", 
                type: "POST",
                enctype: 'multipart/form-data',
                data: form_data,
                processData: false,
                contentType: false
            });

            var retval = jsendp(results);
            
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
            }
            else if (retval['status'] == 'success') {

                $("#update_translations button[type='submit']").removeClass("button-warning");
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=languages');
            }
		}
		catch (error) {
			console.log(error);
        }
    });
    $("body").on("change","#update_translations .red-border",function(ev) {
        ev.preventDefault();
        $(this).removeClass('red-border');
    });

    $("body").on("change","#update_translations input, #update_translations textarea",function(ev) {
        ev.preventDefault();
        $("#update_translations button[type='submit']").removeClass("button-success");
        $("#update_translations button[type='submit']").addClass("button-warning");
        $("#ficon").removeClass('fa-floppy-o');
        $("#ficon").addClass('fa-refresh fa-spin'); 
    });

    $("body").on("click", "#new_translation", function(ev) {
        ev.preventDefault();
        let id = $(this).data('id') + 1;
        $("#translationsTable").append('<tr><td><input name="new-const-' + id + '" style="width: 300px" value=""></td><td><textarea name="new-translation-' + id + '" style="width:550px;height:40px" placeholder=""></textarea></td></tr>');
        $(this).data('id',id);
    });
});
$(document).ready(function() {
    $(document).click(function(e) {
        $('.topnav').find('ul.subnav').find('.subsubnav').hide();
    });

    $('.topnav').find('ul.subnav').find('.sno').on('mouseover',function(ev) {
        $('.topnav').find('ul.subnav').find('.subsubnav').show();
    });
});

