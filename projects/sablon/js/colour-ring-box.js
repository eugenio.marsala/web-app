$(document).ready(function() {

    $("#colourringdiv").draggable();

    // set rings into upload form 
    $("body").on("click","#addcolourings",function(){
        var v=new Array();
        // left top
        $("#leg-left-top").each(function(){
            var a = new Array;
            $(this).find('li').each(function(){
                var alt = $(this).data('color');  // ring color code
                var ringtext = $(this).text();    // ring text
                var ctc = $(this).data('text');   // text color code
                if (ringtext!='')
                    if( alt==ctc || ctc == '')
                        alt = alt.concat("["+ringtext+"]");
                    else
                        alt = alt.concat("["+ctc+"#"+ringtext+"]");
                if (alt===undefined) {
                    alt = '';
                }
                a.push(alt);
            });
            if (a.length>0) {
                rings = fitrings(a,$('#ring_position_pattern').val()).join('');
                v.push(rings);
            } else 
                v.push($('#ring_position_pattern').val());
        });
        // left bottom
        $("#leg-left-bottom").each(function(){
            var a = new Array;
            $(this).find('li').each(function(){
                var alt = $(this).data('color');
                var ringtext = $(this).text();
                var ctc = $(this).data('text');
                if (ringtext!='')
                    if( alt==ctc || ctc == '' )
                        alt = alt.concat("["+ringtext+"]");
                    else
                        alt = alt.concat("["+ctc+"#"+ringtext+"]");
                if (alt===undefined) {
                    alt = '';
                }
                a.push(alt);
            });
            if (a.length>0) {
                rings = fitrings(a,$('#ring_position_pattern').val()).join('');
                v.push(rings);
            } else 
                v.push($('#ring_position_pattern').val());
        });
        // right top
        v.push('|');
        $("#leg-right-top").each(function(){
            var a = new Array;
            $(this).find('li').each(function(){
                var alt = $(this).data('color');
                var ringtext = $(this).text();
                var ctc = $(this).data('text');
                if (ringtext!='')
                    if( alt==ctc || ctc == '' )
                        alt = alt.concat("["+ringtext+"]");
                    else
                        alt = alt.concat("["+ctc+"#"+ringtext+"]");
                if (alt===undefined) {
                    alt = '';
                }
                a.push(alt);
            });
            if (a.length>0) {
                rings = fitrings(a,$('#ring_position_pattern').val()).join('');
                v.push(rings);
            } else 
                v.push($('#ring_position_pattern').val());
        });
        // right bottom
        $("#leg-right-bottom").each(function(){
            var a = new Array;
            $(this).find('li').each(function(){
                var alt = $(this).data('color');
                var ringtext = $(this).text();
                var ctc = $(this).data('text');
                if (ringtext!='')
                    if( alt==ctc || ctc == '' )
                        alt = alt.concat("["+ringtext+"]");
                    else
                        alt = alt.concat("["+ctc+"#"+ringtext+"]");
                if (alt===undefined) {
                    alt = '';
                }
                a.push(alt);
            });
            if (a.length>0) {
                rings = fitrings(a,$('#ring_position_pattern').val()).join('');
                v.push(rings);
            } else 
                v.push($('#ring_position_pattern').val());
        });

        var vj = v.join('.').replace(".|.","|");

        if ($('#rotate').hasClass('swapped')) {
            var vj_1 = vj.substring(0,vj.indexOf("|"));
            var vj_2 = vj.substring(vj.indexOf("|")+1,vj.length);
            vj = vj_2+'|'+vj_1;
        }
        set_ring_value(vj);
        
        $("#colourringdiv").hide();
    });
    // remove a ring
    $("#colourringdiv").on("dblclick",".ring-position li",function(){
        $(this).remove();
    });
    $("#colourringdiv").on("click","#colourringdiv-close",function(){
        $("#colourringdiv").hide();
    });
    // rotate
    $("#colourringdiv").on("click","#rotate",function(){
        if (!$(this).hasClass('swapped')) {
            $("#legbox").css('background-image','url("'+obj.url+'images/bird_leg.png")');
            $(this).addClass('swapped');
        } else {
            $("#legbox").css('background-image','url("'+obj.url+'images/bird_leg_sw.png")');
            $(this).removeClass('swapped');
        }
        // text exchange
        var c = $("#leftlabel").text();
        $("#leftlabel").text($("#rightlabel").text());
        $("#rightlabel").text(c);

        // rings exchange
        var lt = $("#leg-left-top").clone();
        var rt = $("#leg-right-top").clone();
        var lb = $("#leg-left-bottom").clone();
        var rb = $("#leg-right-bottom").clone();
        $("#leg-left-top").find('li').remove();
        $("#leg-right-top").find('li').remove();
        $("#leg-left-bottom").find('li').remove();
        $("#leg-right-bottom").find('li').remove();

        lt.find('li').each(function(){
            $("#leg-right-top").append($(this));
        });
        rt.find('li').each(function(){
            $("#leg-left-top").append($(this));
        });
        lb.find('li').each(function(){
            $("#leg-right-bottom").append($(this));
        });
        rb.find('li').each(function(){
            $("#leg-left-bottom").append($(this));
        });
    });

    //set ring label text,color,backround on label input
    $("#colourringdiv").on("click",".ring-position li",function(){
        $("#ring-text").val($(this).text());
        $('.ring-position li').removeAttr('id')
        $(this).attr('id','selected-ring');
        $("#ring-text").css('background-color',$(this).css('background-color'));
        $("#ring-text").css('color',$(this).css('color'));
        $("#ring-text").data('color',$(this).data('color'));
        $("#ring-text").data('text',$(this).data('text'));
        $("#ring-text").focus();
    });
    // type ring label text
    $("#colourringdiv").on("keyup","#ring-text",function(){
        $('#selected-ring').html($(this).val());
        $('#selected-ring').css('color',$(this).css('color'));
        $('#selected-ring').data('text',$(this).data('text'));
    });
    // show colour ring box
    $("body").on('click','.colourringb',function(){
        $("#colourringdiv").show();

        ri = $(this).closest('tr').index();
        pos = ri+','+(1+(+$(this).closest('td').index()));
        $("#colourringdiv #colour-ring-referenceid").val(pos);
        $("#colourringdiv #cr_code").val($(this).closest('td').find('input').val());
        $("#colourringdiv #cr_code").trigger('change');
    });
    // populate ring image from code
    // it is happening when ring box loaded from code click
    $("#colourringdiv").on("change","#cr_code",function() {
        $('#leg-right-bottom').empty();
        $('#leg-right-bottom').css('height',0);
        $('#leg-right-top').empty();
        $('#leg-right-top').css('height',0);
        $('#leg-left-bottom').empty();
        $('#leg-left-bottom').css('height',0);
        $('#leg-left-top').empty();
        $('#leg-left-top').css('height',0);
        $('#ring-text').css('background-color','unset');
        $('#ring-text').val('');

        var len = $('#ring_position_pattern').val().length;
        $('.ring-position').css('height',eval(len*28)+'px');

        var colors = [];
        colors['R'] = 'red';
        colors['P'] = 'pink';
        colors['G'] = 'green';
        colors['g'] = 'lightgreen';
        colors['O'] = 'orange';
        colors['Y'] = 'yellow';
        colors['B'] = 'blue';
        colors['b'] = 'lightblue';
        colors['W'] = 'white';
        colors['K'] = 'black';
        colors['N'] = 'brown';
        colors['U'] = 'purple';
        colors['V'] = 'violet';
        colors['M'] = 'silver';

        var code = $(this).val();
        if (code!='') {
            //UX.XX|XX.Y[G#assd]X
            var code_pos = code.split('|');
            var code_left = code_pos[0];
            var code_right = code_pos[1];
            var code_left_pos = code_left.split('.');
            var code_right_pos = code_right.split('.');

            var code_left_bottom = code_left_pos[1];
            var code_left_top = code_left_pos[0];
            var code_right_bottom = code_right_pos[1];
            var code_right_top = code_right_pos[0];

            var directions = ['right_top','right_bottom','left_top','left_bottom'];

            $("#legbox").css('background-image','url("'+obj.url+'images/bird_leg_sw.png")');
            if ($("#rotate").hasClass('swapped')) {
                // text exchange
                var c = $("#leftlabel").text();
                $("#leftlabel").text($("#rightlabel").text());
                $("#rightlabel").text(c);
                $("#rotate").removeClass('swapped');
            }
            
            for (var dn = 0; dn < directions.length; dn++) { 
                var d = directions[dn];
                // R[K#asd]xx.G[K#ert]xx|xxx.xxx
                var z = eval('code_'+d);
                
                var label = 0;
                var labtext = "";
                var c = '';
                //R[K#asd]xx
                //Gxx
                //xxx
                for (var i = 0; i < eval(z.length + 1); i++) {
                    d = directions[dn];
                    if (c=='X' || c=='x') continue;
                    if (i > 0 && label == 0 ) {
                        if (c == ']') {
                            c = save_color;
                        }
                        d = d.replace('_','-');
                        if (labtext != '') {
                            // processing text color
                            var labtext_split = labtext.split('#');
                            if ( typeof labtext_split[1] === 'undefined') {
                                labtext_split[1] = labtext_split[0];
                                labtext_split[0] = '';
                            }

                            $('#leg-'+d+' li:last-child').remove();
                            if (labtext_split[0] == '')
                                $('#leg-'+d).append("<li style='background-color:"+colors[c]+"' data-color='"+c+"' data-text='"+labtext_split[0]+"'>"+labtext_split[1]+"</li>");
                            else
                                $('#leg-'+d).append("<li style='background-color:"+colors[c]+";color:"+colors[labtext_split[0]]+"' data-color='"+c+"' data-text='"+labtext_split[0]+"'>"+labtext_split[1]+"</li>");
                        } else {
                            $('#leg-'+d).append("<li style='background-color:"+colors[c]+"' data-color='"+c+"' data-text=''>"+labtext+"</li>");
                        }
                        labtext = "";
                    }

                    var c = z.charAt(i);

                    // label processing
                    if (c == '[') {
                        label = 1;
                        save_color = z.charAt(i-1);
                    }
                    else if (c == ']') {
                        label = 0;
                    }
                    if (label == 1 && c != '[') {
                        labtext = labtext + c;
                    }
                }
                d = d.replace('_','-');

                var zindex = 20;
                $('#leg-'+d).find('li').each(function() {
                    $(this).css('z-index',zindex);
                    zindex = eval(zindex - 1)
                });
            }
        }
    });

    // Drag'n Drop colour rings
    var ring_pos;
    $( "ul.ring-box" ).sortable({
      connectWith: ".ring",
      dropOnEmpty: true,
      snap: "ul",
      cursor: 'move',
      placeholder: "ring-drop-highlight",
      stop:  function(event, ui) {
            $(this).sortable('cancel');
      }, 
      receive: function(event,ui){
        ui.item.remove();
      }
    });
    $( "ul.ring-position" ).sortable({
      connectWith: ".ring",
      dropOnEmpty: true,
      snap: "ul",
      cursor: 'move',
      placeholder: "ring-drop-highlight",
      receive: function(event,ui){
          var pos = ui.item.index();
          var text = $(this).find('li:eq('+eval(pos)+')').text();
          if (ui.sender.hasClass('ring-box')) {
            var len = $('#ring_position_pattern').val().length;
            if ($(this).find('li').length>1) {
                if ( $(this).find('li').length<=len) {
                    ui.item.last().clone().insertBefore($(this).find('li:eq('+eval(pos)+')'));
                    $(this).find('li:eq('+eval(pos)+')').text('');
                } else {
                    return;
                }
            } else {
                ui.item.last().clone().appendTo($(this)); 
                $(this).find('li:eq('+eval(pos-1)+')').text('');
            }
            text = "";
          }
          var zindex = 20;
          $(this).find('li').each(function() {
                $(this).css('z-index',zindex);
                zindex = eval(zindex - 1)
          });
          $('.ring-position').find('li').removeAttr('id')
          $(this).find('li:eq('+eval(pos-1)+')').attr('id','selected-ring');
          $("#ring-text").val(text);
          $("#ring-text").css('background-color',$(this).find('li:eq('+eval(pos+1)+')').css('background-color'));
          $("#ring-text").css('color',$(this).find('li:eq('+eval(pos+1)+')').css('color'));
          $("#ring-text").data('text',$(this).find('li:eq('+eval(pos+1)+')').data('text'));
          $("#ring-text").focus();
      },
      stop: function(event,ui) {
          var zindex = 20;
          $(this).find('li').each(function() {
                $(this).css('z-index',zindex);
                zindex = eval(zindex - 1)
          });
      }
    });
    $( ".ring-box,.ring-position" ).disableSelection();
    // set ring label color from input
    $( "#colourringdiv" ).on( "click",".ring-box li",function(){
        $("#ring-text").css('color',$(this).css('background-color'));
        $("#ring-text").data('text',$(this).data('color'));
        $('#selected-ring').data('text',$(this).data('color'));
    });
});
/* sprintf like function for create colour ring pattern
     * rings: array of colour codes 
     * pattern: expected NULL text pattern on each leg node */
function fitrings(rings,pattern) {
    var p = pattern.split('');
    var fitring = Array()
    for(i=0;i<p.length;i++) {
        if (rings[i]!==undefined) {
            fitring.push(rings[i]);
        } else {
            fitring.push(p[i]);
        }
    }
    return fitring;
}
