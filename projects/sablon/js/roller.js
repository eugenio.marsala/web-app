/* Functions for displaying very large object
   It is requesting a subset of the large object and rendering it in the viewport
   The scrollbar length showing the object size, so scrolling the viewport call the rendering.
   There are is buffer around the viewport, therefore the scroll event call the rendering only if we reach the the top or the bottom of the current subset.
   The idea is coming from https://github.com/6pac/SlickGrid
   however I did not read that code just watched this example: SlickGrid-master/examples/example6-ajax-loading.html
   Than you for sharing this great idea!
   call jquery!
   Miklos Ban
   2016.05.15 - OpenBioMaps
   Usage:
   $(document).ready(function() {
       //40 rows coming from the server at once.
       var rr = new Roller({rollerSend:40,ajax:'roller.php',target:'.scrollbar'});
       rr.prepare();
       rr.render();
       $('.scrollbar').on('scroll', function() {
           rr.render();
       });
   });
 *
 * */
function Roller (e) {
    this.from = 0;
    this.skipRead = 1;
    this.rollerSend = e.rollerSend;
    this.pos = 0;
    this.target = "#rollertable";
    this.scrolldiv = e.target;
    this.ajax = e.ajax;
    this.rowHeight;
    this.cellWidth;
    this.borderRight;
    this.borderBottom;
    this.arrayType;
}


Roller.prototype.readData = function(pos) {
    var t = this;
    t.skipRead--;
    if (t.skipRead!=0) {
        return false;
    }
    if (pos<0) {
        pos = 0;
    }
        //nem működik...
        //$('.viewport-header').resizable({});
    $.post(t.ajax, {'RreadFrom':pos, 'arrayType':t.arrayType, 'Order':t.Order, 'Table':t.Table}, function(data){

            t.skipRead=1;
            $(t.target).html('');
            var j = JSON.parse(data);
            //$(t.target).height(t.rowHeight*t.dataLength);
            //console.log(t.rowHeight)
            for (f = 0; f < j.length; f++) {
                t.from++;
                var d = "";
                for (k = 0; k<j[f].length; k++) {
                    if (t.arrayType == 'table' || t.arrayType == 'normaltable')
                        d += "<div class='cdiv'><div class=viewport>" + j[f][k] + "</div></div>";
                    else
                        d += "<div class='cdiv'>" + j[f][k] + "</div>";
                }
                //ez a 120px az ablak tetejének a mérete talán.
                //ezt valahogy dinamikusan kell számolni a felső elem pozíciójából
                //var h = (pos*t.rowHeight)-120;
                
                var h = (pos*t.rowHeight);
                $(t.target).append('<div class="rdiv" style="top:'+h+'px">'+d+'</div>')
            }
            $('.cdiv').css('border-right',t.borderRight);
            $('.cdiv').css('border-bottom',t.borderBottom);
            $('.cdiv').width(t.cellWidth-3);
            $('.rdiv').height(t.rowHeight);
    });
}


Roller.prototype.render = function() {
    var t = this;
    var rpos = $(t.target).position().top;
    
    /*$('.rdiv').each(function(n) {
            console.log(n+": "+" "+rpos+" "+$(this).position().top + " " + $(this).height() + " " + $("#rollertable").height() + " " + $('.scrollbar').height());
    });*/
        
    // renderer számláló
    // csak teszt, de egyébként le lehet véle kérdezni a legfelső látszó elemet
    /*$('.rdiv').each(function(n) {
            //top visible rdiv index of current render
            if ($(this).position().top>-8) {
                 $('#visible-items').html(  n );
                 return false;  // break once the first displayable value has been returned.
            }
    });*/

    var l = $('.rdiv').length-1;
    if (typeof l !== typeof undefined && l !== false && l > 0) {
        //var averageHeight = $('.rdiv:eq(0)').height();
        if ($('.rdiv:eq(0)').position().top>0) {
            //scroll up
            //ne legyen több a kivont érték mint a roller által visszaadott érték + a tábla maximuma!

            var sm = t.rollerSend-Math.round($('#scrollbar').height()/t.rowHeight);
            t.readData(Math.abs(Math.round(rpos/t.rowHeight))-sm);
        } else if ($('.rdiv:eq('+l+')').position().top < $('#scrollbar').height()-t.rowHeight) {
            //scroll down
            //read from rpos/row-height if last element position lower than scrollbar_height - averageHeight
            //the averageHeight border means: start rendering before show empty area
            t.readData(Math.abs(Math.round(rpos/t.rowHeight)));
        }
    }
}
Roller.prototype.set = function(e) {
    this.rowHeight = e.rowHeight;
    this.cellWidth = e.cellWidth;
    this.borderRight = e.borderRight;
    this.borderBottom = e.borderBottom;
    this.arrayType = e.arrayType;
    this.Order = e.Order;
    this.Table = e.Table;
}

Roller.prototype.prepare = function(w,i) {
    var t = this;
    $(t.scrolldiv).html("<div id='rollertable' class='force-overflow'></div>");
    if(i.length) {
        $("#scrollbar-header").html(i);
        $("#scrollbar-header").show();
        $("#scrollbar-header").width(w);
    } else {
        $("#scrollbar-header").hide();
    }
    //console.log(e);
    //get div's length
    $.post(t.ajax, {'Rprepare':1,'arrayType':t.arrayType, 'Table':t.Table}, function(data) {
        //initial height
        //var d = $(t.target).css('line-height').match(/(\d+)/);
        //$(t.target).height(d[1]*data);

        $(t.target).height(t.rowHeight*data);
        if (t.arrayType=='table' || t.arrayType=='normaltable') {
            var overbox = $(document).height()-44;

        } else {
            var overbox = 542;
        
        }

        //if (d[1]*data < 550) {
        if (t.rowHeight*data < overbox) {
            $(".force-overflow").css('min-height',30+t.rowHeight*data+'px');
            $("#scrollbar").height(overbox);
        } else {
            $(".force-overflow").css('min-height',overbox+100+'px');
            $("#scrollbar").height(overbox);
        }
        $("#scrollbar").width(w+15);
        $("#scrollbar").scrollTop(0);

        //if table prepared we read the firs block into from 0 pos
        t.readData(0);
    });
}

