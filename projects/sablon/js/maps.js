/* Openlayers map event handling
 *
 */

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    alert( "Geolocation is not supported by this browser." );
  }
}
function showPosition(position) {

        var X = position.coords.latitude;
        var Y = position.coords.longitude;
        var p = new OpenLayers.Geometry.Point(Y, X); 
        
        var currentZoom = map.getZoom();
        p.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
        markerLayer.clearMarkers();
        var size = new OpenLayers.Size(48,48);
        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
        var icon = new OpenLayers.Icon('js/img/map-marker.png',size,offset);
        markerLayer.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(p.x,p.y),icon));
        map.setCenter(new OpenLayers.LonLat(p.x, p.y), currentZoom);
}

var header_on_top = true;
$(document).ready(function() {

    $('#mapholder').on('click','#show-my-position',function(){
        getLocation();
    });

    if(typeof $.cookie('buffer')!=='undefined'){$('#buffer').val($.cookie('buffer'));$('#bufferslide').val($.cookie('buffer'));}

    // Initial map size
    var h=$('body').innerHeight(); // New height
    if (obj.fixheader==0) {
        $('.maparea').height(h-76);
        $('#bheader').css('position','fixed');
        $('#mapfilters').css('padding-top','80px');
    } else {
        $(".maparea").height(h-156);
    }
    
    $( window ).resize(function() {
        h=$('body').innerHeight(); // New height
        $(".maparea").height(h-156);
    });

    // result area of map queries
    $('#matrix').on('click','#flyingadd',function(){
        $(this).hide('slow');
    });

    /* set query table */
    $('#current_query_table').change(function(){
        $.post('ajax',{'set-current-query-table':$(this).val()},function(data) {
            var retval = jsendp(data);
            if (retval['status']=='success') { 
                window.location = retval['data'];
            }

        });
    });

    $(document).scroll(function() {

        if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
            // rightside menu
            $('#scrolldown').after($('#scrollup'));
        } else {
            $('#scrollup').after($('#scrolldown'));
        }
        
        if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
            
            // top menu
            if (obj.fixheader==0 && header_on_top) {
                $( "#bheader" ).animate({
                    top: "0",
                    right: "0",
                    height: "60px",
                    width: $('.mapfb').width(),
                }, 
                {
                    duration: 500,  
                    start: function() {
                        $('#bheader').css("border-bottom",'none'),
                        $('#bheader .main-logo-text').hide(),
                        $('#bheader .main-logo-img').addClass('logo-img_small'),
                        $('#bheader .htitle').addClass('htitle_small'),
                        $("#bheader").appendTo("#smallheader"),
                        $('#nav').parent().css('padding-top','0'),
                        $('#nav').parent().css('padding-right','0'),
                        $('#nav .topnav li:eq(0)').hide(),
                        $('#nav .topnav li:eq(1)').hide(),
                        $('#nav .topnav').css('padding','0'),
                        $('#mapfilters').css('padding-top','0'),
                        header_on_top = false
                    },
                    complete: function() {
                        $('#maintenance').hide();
                        $('#bheader').css('position','initial'),
                        $('.olControlZoomPanel').css('top','71px'), // 71
                        $('.olControlPanPanel').css('top','10px'),   // 10
                        $('.olControlLayerSwitcher').css('top','5px') // 5
                    }
                });
            }
        }
    });
    $("#scrollup").click(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false; 
    });
    $("#scrolldown").click(function(){
        $("html, body").animate({ scrollTop:  $(document).height()-$(window).height() }, "slow");
        return false; 
    });
    $("#hidenavigator").click(function(){
        $("#navigator").hide('slow');
        $("#shownavigator").show();
    });
    $("#shownavigator").click(function(){
        $("#shownavigator").hide();
        $("#navigator").show();
    });

    var we = 0;
    $('#smallheader').on('click','.subnav', function(e) {
         we = 1;
    });

    /* fixing bheader - only in mappage!! */
    $('#smallheader').on('click','#bheader', function(e) {
        if (we == 0) {
            e.preventDefault();

        $( "#bheader" ).animate({
            width: '100%',
            height: 'auto',
        }, 
        {
            duration: 500,  
            start: function() {
                $('#maintenance').show();
                $('#bheader').css('position','fixed'),
                $('#bheader').css('border','none'),
                $('#bheader').css('border-bottom','1px solid #acacac')
            },
            complete: function() {
                $('#bheader .main-logo-text').show(),
                $('#bheader .htitle').removeClass('htitle_small'),
                $('#nav').parent().css('padding-top','10'),
                $('#nav').parent().css('padding-right','20'),
                $('#nav .topnav li:eq(0)').show(),
                $('#nav .topnav li:eq(1)').show(),
                $('#nav .topnav').css('padding','10px'),
                $('.olControlZoomPanel').css('top','151px'), // 71
                $('.olControlPanPanel').css('top','90px'),   // 10
                $('.olControlLayerSwitcher').css('top','85px'), // 5
                $("#bheader .main-logo-img").removeClass('logo-img_small'),
                $("#bheader").prependTo("#holder"),
                $('#mapfilters').css('padding-top','80px'),
                header_on_top = true
            }
        });
        }
    });

    //clear morefilter action
    $("body").on('click','.clear-morefilter',function(){
        $.post('ajax',{'clear-morefilter':1},function(data){
            $(".clear-morefilter").removeClass('button-warning');
            $(".clear-morefilter").removeClass('clear-morefilter');
            $(".clear-morefilter").addClass('button-passive');
            //$("#apptq").find('i').toggleClass('fa-toggle-on');
            //$("#apptq").find('i').toggleClass('fa-toggle-off');
            //$("#apptq").removeClass('button-success');

        });
    });
    // results box
    $("body").on('click','#slider',function(){
        $(this).draggable({handle:'div'});
        $(this).resizable();
        if ($('.slide-body').height() > h-100) {
            $('.slide-body').height(h-100);
        }
    });


    
    //map.toggleControl("#pointToggle1");
    $('body').on('click','.maphands',function(e){
        $("#map").css('cursor','default');
        $("#identify_point").removeClass('button-secondary');
    });
    $('body').on('click','#pointToggle',function(e){
        $("#map").css('cursor','help');
        $("#identify_point").removeClass('button-secondary');
    });
    $('body').on('click','#noneToggle',function(e){
        $("#map").css('cursor','pointer');
        $("#identify_point").addClass('button-secondary');
    });
    $('body').on('click','#zoomToggle',function(e){
        $("#map").css('cursor','default');
        $("#identify_point").removeClass('button-secondary');
    });


    $('body').on('click','#identify_point',function(e) {
        $("#noneToggle").trigger("click");
        $("#map").css('cursor','help');
        $(this).addClass('button-secondary');
    });

    $('body').on('click','#zoomToggle',function(e) {
        $("#map").css('cursor','crosshair');
    });

    

    //map page more than 2000 records links
    /*$('body').on('click','.paging',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        $( "#dialog" ).text("Waiting for the WFS response...");
        var isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog" ).dialog( "open" );
        var mapurl = obj.query_url + '&repeat='+page;
        //proxy and wfs processing
        OpenLayers.Request.GET({url:mapurl,callback:WFSGet,scope:{"skip_processing":'yes'}});
    });*/
    // buffer change on map selection
    $('body').on('change','#buffer', function(){
        var buffer = $(this).val();
        $("#bufferslide").val(buffer);
        $.cookie("buffer",$(this).val());
        if (buffer == 0) {
            $('#pointToggle').prop('disabled', true);
            $('#pointToggle_icon').css('display','none');
        }
        else {
            $('#pointToggle').prop('disabled', false);
            $('#pointToggle_icon').css('display','block');
        }
    });
    $('body').on('input','#bufferslide', function() {
        $("#buffer").val($(this).val());
        var buffer = $(this).val();
        $.cookie("buffer",$(this).val());
        if (buffer == 0) {
            $('#pointToggle').prop('disabled', true);
            $('#pointToggle_icon').css('display','none');
        }
        else {
            $('#pointToggle').prop('disabled', false);
            $('#pointToggle_icon').css('display','block');
        }

    });
    // turn on-off mousewheel on map
    $('body').on('mousewheel DOMMouseScroll','#buffer', function(event){
        event.preventDefault();
        var n=+$(this).val();
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            // scroll up
            n=eval(n+1);
            $(this).val(n)
            $("#bufferslide").val(n);
        }
        else {
            // scroll down
            n=eval(n-1)
            $(this).val(n)
            $("#bufferslide").val(n);
        }
    });
    /* mouse ZoomWheel enabled/disabled
     *
     * */
    $("body").on('click','#wz',function(){
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).removeClass('button-success');
            nav.disableZoomWheel();

            $.cookie("wz",false);
        } else {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).addClass('button-success');
            nav.enableZoomWheel();
            
            $.cookie("wz",true);
        }
    });
    //Upload - webform - geometry from map selection
    //click on green button
    //the setValue function is defined in the uploader.js
    $('body').on('click','#setValue',function(e){
        var v=$(this).val();
        if(window.opener) window.opener.setValue(v);
    });

    // photo filter button toggle
    $("#hasphoto").on('click',function(){
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).removeClass('button-success');
        } else {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).addClass('button-success');
        }
    });
    // apply text query over spatial query  - button click
    // turn on - off
    $("body").on('click',"#apptq",function(){
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');
            $(this).find('i').toggleClass('fa-toggle-off');
            $(this).removeClass('button-success');
            $.post('ajax',{'clear-morefilter':1},function(data){
                $.post('ajax',{'set-morefilter':0},function(data){});
            });
        } else {
            $.post('ajax',{'set-morefilter':1},function(data){});
            $(this).find('i').toggleClass('fa-toggle-on');
            $(this).find('i').toggleClass('fa-toggle-off');
            $(this).addClass('button-success');
        }
    });
    /* CONTROL BOX 
     * MapfilterBox:
     * send text query to WFS server
     * */
    $("#mapcontrols").on('click',"#reset-map",function(){
        //clear filters
        $.post('ajax',{'clear-morefilter':1},function(data){});
        $.post('ajax',{'clear-session_querybuildstring':1},function(data){
            drawLayer.destroyFeatures();
            for(var i=0;i<dataLayers.length;i++) {
                dataLayers[i].redraw();
            }

            var point = new OpenLayers.LonLat(obj.lon, obj.lat);
            point.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
            map.setCenter(point, obj.zoom);
            $("#buffer").val(obj.buffer);
            $("#bufferslide").val(obj.buffer);
            geometry_query_neighbour = 0;
            geometry_query_session = 0;
            geometry_query_selection = 0;
        });
        // set dataLayers visible
        /*for(var i=0;i<dataLayers.length;i++) {
            var o = dataLayers[i];
            o.setVisibility(true);
            o.setOpacity(1);
        }
        // turn off query layers
        for(var i=0;i<queryLayers.length;i++) {
            var o = queryLayers[i];
            o.setVisibility(false);
        }*/

    });

    // coordinate click on result slides
    // mark point on the map
    // transform_data module
    // module-ba áttenni!!!
    $('body').on('click',".coord_query",function(e) {
        e.preventDefault();
        var u = $(this);
        var coords = $(this).attr('alt');
        
        // track data view
        
        if ($(this).attr('id')) {
            $.post("ajax", {track_data_view:$(this).attr('id')},function(data){});
        }

        // scroll to map
        $("html, body").animate({ scrollTop: 0 }, "slow");
        
         /* JSTS nyűg nem megy... */
        //{"type":"Point","coordinates":[18.767587411656972,47.785560590815244]}
        coords = coords.replace(/\\/g,'');

        var geojson_format = new OpenLayers.Format.GeoJSON();
        var feature = geojson_format.read(coords);
        var p = feature[0].geometry.getCentroid();
        p.transform(new OpenLayers.Projection("EPSG:"+obj.selectedLayer_srid), new OpenLayers.Projection("EPSG:900913"));
        // A buffer méretének 2x-re zoomolunk
        if ($('#buffer').length > 0) {
           b = $("#buffer").val();
        } else { 
           b = obj.buffer; 
        }
        if (b=='' || b==0) {
            b= 500;
        }
        
        //var jsts = require("jsts");
        var reader = new jsts.io.WKTReader();
        var input = reader.read(new String(p));
        var buffer = input.buffer(b*10);
        var parser = new jsts.io.OpenLayersParser();
        //var parser = new jsts.io.OL3Parser();
        buffer = parser.write(buffer);
        var Zoomfeature = new OpenLayers.Feature.Vector(buffer, null, null);
        map.zoomToExtent(Zoomfeature.geometry.getBounds(), closest=false);
        //draw marker icon 
        markerLayer.clearMarkers();
        var size = new OpenLayers.Size(48,48);
        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
        var icon = new OpenLayers.Icon('js/img/map-marker-pink.png',size,offset);
        markerLayer.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(p.x,p.y),icon));
        
    });
    $("body").on("click","#legend",function(e){
        $(this).css('left',"-"+eval($(this).width()-10)+"px");
    });
    $("body").on("dblclick","#legend",function(e){
        $(this).css('left',"0");
    });
    /* a colour ring box  function */
    $( ".mapfb" ).on( "click",".show-colour-ring-box",function() {
        $("#colourringdiv").show();
    });
    $("#mapholder").on("click","#spatial-query-settings",function() {
        $("#spatial-query-settings-box").toggle();
    });
    $("#mapholder").on("click","#shared-geom-settings",function() {
        $("#shared-geom-settings-box").toggle();
    });
    $("body").on("click",".mtitle",function() {
        $(this).parent().find('.options-box').toggle();
    });
    $(".mapfb").on("click",".title",function(){
        $(this).closest(".mapfb").toggleClass("mapfb-pos");
    });
});
/* a colour ring box  function */
// set colour-ring-box value into text-filter box  
function set_ring_value(vj) {
   var pos=$("#colour-ring-referenceid").val();
   vj = vj.replace(/X/g,'_');
   $("#"+pos).val(vj);
}

