<?php
/* ***************************************************************************  /
    
GENERAL and GLOBAL FUNCTIONS
Do not change them
Use the local_funcs.php if you need specific functions

****************************************************************************** */

function pds_form_choose_list($project,$user,$groups) {
    #var_dump(debug_backtrace());
    global $BID;
    $list = array();
    /* összes id lekérdezése */
    $cmd = sprintf("SELECT form_id FROM project_forms WHERE project_table=%s AND form_access<3 AND active=1 AND 'api'=ANY(form_type) ORDER BY form_name",quote($project));
    $res = pg_query($BID,$cmd);
    $n = array();
    while($row = pg_fetch_assoc($res)){
        //hozzáférés ellenőrzése
        if(pds_form_access_check($row['form_id'],$project,$user,$groups)===1) {
            $n[] = $row['form_id'];
        }
    }
    //lista a megengedett form azonosítókról
    return $n;
}
function pds_form_access_check($form_id,$project,$user,$groups) {
    if(!$form_id) return;
    global $BID;

    $form_type = 'api';

    $f1 = $f2 = $ft= $fn=$ft_str='';
    $acc = 0; #0:public, 1:logined, 2:closed, 3:inactive
    $group_filter='';
 
    //access level - we create a filter for it
    if ($user) {
        $acc = 1;
        /* the form is available only for specified groups
         * and the logined user is member of these groups */
        $group_filter = sprintf(' OR (form_access=2 AND \'{%s}\'&&groups)',$groups);
    }

    // van-e olyan aktív form a projektben aminek input_form_id az azonosítója és (form típusa input_form_type) és a hozzáférés kisebb/egyenlő mint acc(0|1)
    // több ilyen form is lehet!!!
    $cmd = sprintf("SELECT form_id,'api' as form_type FROM project_forms WHERE project_table=%s AND form_id=%s AND 'api'=ANY(form_type) AND (form_access<='%s' $group_filter)",quote($project),quote($form_id),$acc);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        return 1;
    }
    return 0;
}
// List of API forms
function pds_form_element($form_id,$project) {
    global $BID;
    $cmd = sprintf("SELECT form_id,form_name,'api' as ft,round(extract(epoch from last_mod)) as last_mod FROM project_forms WHERE project_table=%s AND 'api'=ANY(form_type) AND form_id='%d' and active=1",quote($project),$form_id);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_array($res);
    return $row;
}
// kisegítő függvény: Array to JSON object
function array2json($arr) {
    //$json = array();
    return json_encode($arr, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_FORCE_OBJECT);
}
    
// kisegítő függvény: PRS
// Return with an array of Column's names of a table 
function glue($table) {
    global $ID;
    $list = '*';
    $cmd = "SELECT * FROM $table WHERE 1=1 LIMIT 1";
    $res = pg_query($ID,$cmd);
    while($r=pg_fetch_assoc($res)){
       $list = array_keys($r);
    }
    return $list;
}

// PFS: user's profile
// users' profile query function
function pds_profile($table,$userid) {
    global $BID,$ID;
    # Ha a userid tömb: (id1,id2,...)...
    # akkor nem ad vissza semmit
    # ha az egyes lekérdezések atomi funkciók volnának, akkor a profile, össze tudná rakni, de lényegében nincs értelme
    
    # crypted userid
    $cmd = sprintf("SELECT id FROM users WHERE \"user\"=%s",quote($userid));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $userid = $row['id'];
    } else return FALSE;

    // feltöltések száma
    $cmd = sprintf("SELECT id FROM system.uploadings WHERE uploader_id=%s",quote($userid));
    $res = pg_query($ID,$cmd);
    $uplcount = pg_num_rows($res);
    $upid = array();
    while ($row=pg_fetch_assoc($res)){
        array_push($upid,$row['id']);
    }

    // feltöltött rekordok és módosítások száma
    $modcount='';
    $upld='';
    if ($table) {
        if (count($upid)) {
            // feltöltött rekordok száma
            $cmd = sprintf("SELECT count(*) AS c FROM \"%s\" WHERE obm_uploading_id IN (%s)",$table,implode(',',$upid));
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $row=pg_fetch_assoc($res);
                $upld = $row['c'];
            }
        }
        // módosított sorok száma
        $cmd = sprintf("SELECT count(*) AS c FROM \"%s\" WHERE obm_modifier_id=%s",$table,quote($userid));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row=pg_fetch_assoc($res);
            $modcount = $row['c'];
        }
    }
    $st = "";
    $pu = "pu";
    if ($table) {
        $st = "LEFT JOIN project_users pup ON (pup.user_id=users.id AND pup.project_table='$table')";
        $pu = "pup";
    }
    $cmd = sprintf("SELECT %d AS uplcount,'%s' AS upid,%d AS upld,%d AS modcount,status,
        institute,orcid,address,username,array_to_string(givenname,' ') as givenname,array_to_string(familyname,' ') as familyname,
        array_to_string(\"references\",',') as \"references\",\"user\",email,validation,pu.project_table
        FROM public.users 
        LEFT JOIN project_users pu ON (pu.user_id=users.id )
        $st
        WHERE public.users.id=%s 
        GROUP BY users.id,pu.project_table
        ORDER BY pu.project_table",$uplcount,implode(',',$upid),$upld,$modcount,quote($userid));
    $res = pg_query($BID,$cmd);
    return $res;
}

// PFS: history
// getFunction: history
function histRows($table,$value) {
    global $ID;
    $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",$table)); 
    $genid = quote($value);

    $cmd = "SELECT COUNT(hist_id) as cn FROM $qtable_history WHERE obm_id=$genid LIMIT 1";
    #print $cmd.'<br>';
    $res = pg_query($ID,$cmd);
    return $res;
}

// PFS: history
// History header function
function histHeader($table,$value) {
    global $ID;
    $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",$table)); 
    $genid = quote($value);

    $cmd = "SELECT to_char(hist_time,'YYYY-MM-DD HH24:MI') as hist_time,modifier_id FROM $qtable_history WHERE obm_id=$genid ORDER BY hist_time DESC LIMIT 1";
    #print $cmd.'<br>';
    $res = pg_query($ID,$cmd);
    return $res;
}

// PFS functions
// load saved queries
function loadq($value) {
    global $BID;
    list($id,$ses) = preg_split('/@/',$value);
    $id = preg_replace("/[^0-9]/","", $id);
    $ses = preg_replace("/[^a-zA-Z0-9]/","", $ses);
    if (strlen($ses)>30) return;
    $cmd = "SELECT result,comment FROM queries WHERE id='$id' and sessionid='$ses'";
    $res = pg_query($BID,$cmd);
    $post_id = array();
    $table = '';
    if (pg_num_rows($res)) {
        $row=pg_fetch_assoc($res);
        $table = $row['comment'];
        $xml = simplexml_load_string($row['result']);
        $namespaces = $xml->getDocNamespaces();
        foreach($xml->children($namespaces['gml']) as $child) {
            foreach($child->children($namespaces['ms']) as $data){
                foreach($data->children($namespaces['ms']) as $i){
                    if ($i->getName() == 'id') {
                        $post_id[] = (string) $i;
                    }
                }
            }
        }
        return array($table,$post_id);
    } else {
        return common_message('fail',$cmd);
    }
}

// array_walk function:
// quotes column's names
function wquote(&$i, $key) {
    if ($i=='obm_comments')
        /* array_to_string FUNCTION should be exists!! */
        $i = "array_to_string(obm_comments,'~~') as obm_comments";
    #elseif ($i=='validation_')
    #    $i = "array_to_string(validation_,',') as validation_";
    else
        $i = '"'.$i.'"';
}

// PRS function
// adat lekérdezés elmentett egyedi SQL  alapján: service->PRS
function reportQuery($key,$table) {
    global $ID,$BID;

    $cmd = sprintf("SELECT id FROM users 
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email) 
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table=%s",quote($_REQUEST['access_token']),quote($table));

    $res = pg_query($BID,$cmd);

    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);

        $cmd = sprintf("SELECT command FROM custom_reports WHERE user_id=%d AND key=%s",$row['id'],quote($key));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);

            $command = $row['command'];

            #$command = "SELECT comment, datum FROM public_nestbox_data_observations WHERE datum=(SELECT datum FROM public_nestbox_data_observations ORDER BY datum DESC LIMIT 1)";
            $cmd = sprintf("SELECT row_to_json(t) FROM ( %s ) t",$command);
            return common_message('ok',$cmd);
        } else {
            return common_message('error','No reports for the requested user and key');
        }
    } else {
        return common_message('fail','access denied');
    }
}

// PRS function
// adat lekérdezés function: service->PRS
function dataQuery($table,$value,$clmn,$request_type) {
    global $ID,$BID;
    $columns = array();
    $ref_column = '';
    $request_type = "JSON";

    $st_col = st_col($table,'array');

    $obm_columns = dbcolist('columns',$table);
    $obm_columns[] = "obm_id";
    $obm_columns[] = "obm_uploading_id";
    $obm_columns[] = "obm_validation";
    $obm_columns[] = "obm_comments";

    $geom_idx = array_search('obm_geometry',$obm_columns);

    $READ_ACC = ACC_LEVEL;

    // ACCESS LEVEL Handling
    $cmd = sprintf("SELECT layer_query FROM project_queries WHERE project_table=%s AND layer_type='query' AND rst=1 AND enabled=TRUE",quote($table));
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_assoc($res);
    $query = $row['layer_query'];
    $uid = 0;
    $rule_join = '';
    $rules = "";

    if ($READ_ACC == 1 or $READ_ACC == 'login') {
        if(!isset($_SESSION['Tid'])) {
            return common_message('error','Insufficient rights');
        }
    }
    elseif ($READ_ACC == 2 or $READ_ACC == 'group') {
        $cmd = sprintf("SELECT id,user_status FROM users 
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email) 
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table=%s",quote($_REQUEST['access_token']),quote($table));

        $res = pg_query($BID,$cmd);
        $row = pg_fetch_assoc($res);

        if ($row['user_status']==0 or $row['user_status']=='banned')
            return common_message('error','Insufficient rights');
        else {
            if ($st_col['RESTRICT_C']) {
                $tgroups = $_SESSION['Tgroups'];
                $rule_join = sprintf('LEFT JOIN "%s_rules" ON (obm_id=row_id)',PROJECTTABLE);
                $rules = "AND (sensitivity::varchar IN ('0','public') OR 
                              (sensitivity::varchar IN ('1','no-geom') AND read && ARRAY[$tgroups]) OR 
                              (sensitivity::varchar IN ('2','restricted') AND read && ARRAY[$tgroups]) OR 
                              (sensitivity::varchar IN ('3','only-owner') AND read && ARRAY[$tgroups]))";
            }
        }
    }

    # quote column' names
    array_walk($obm_columns, 'wquote');

    if ($geom_idx!==false) {
        $obm_columns[$geom_idx] = "st_asText(obm_geometry) AS obm_geometry";
    }

    if (count($obm_columns)) {
        $obm_columns = implode(',',$obm_columns);

        $a = array();
        // SET requested column
        if ($clmn == '--id--') {
            
            // data preprocessing
            // numeric value range
            if (preg_match("/(\d+):(\d+)/",$value,$m)) {
                $value = range($m[1],$m[2]);
            }
            
            // list of ids
            if (!is_array($value)) $value = array($value);
            foreach ($value as $var) {
                $var = preg_replace("/[^0-9]+/","", $var);
                $a[] = quote($var);
            }

            $request_column = 'obm_id';

            $query = preg_replace('/%morefilter%/',"",$query);
 
            // text or spatial filter string
            // applied in query layers
            // point - line - polygon

                     // query set parameters
            $q = sprintf("%s IN (%s)",$request_column,implode(",",$a));
            $q = preg_replace('/\$/',"\\\\$",$q);
            $query = preg_replace('/%qstr%/',"AND $q",$query);
            // if I could use this query it would more clear workflow

            $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM ( SELECT %s FROM \"public\".\"%s\" %s WHERE %s IN (%s) %s ) t",$obm_columns,$table,$rule_join,$request_column,implode(",",$a),$rules);
        }
        elseif ($clmn == '--history--') {
            // lekérdezendő sorok azonosítói egy tömbbe pakolva 
            if (preg_match("/(\d+):(\d+)/",$value,$m)) {
                $value = range($m[1],$m[2]);
            }
            if (!is_array($value)) $value = array($value);

            foreach ($value as $var) {
                $var = preg_replace("/[^0-9]+/","", $var);
                $a[] = quote($var);
            }

            $request_column = "row_id";

            $cmd = sprintf("SELECT %s FROM \"public\".\"%s\" WHERE %s IN (%s)",$obm_columns,$table,$request_column,implode(",",$a));
        }
        elseif ($clmn == '--species--') {
            // UPGRADE NECESSARY !!!
            foreach ($value as $var) {
                // nincs definiálva hogy tömbbként jöjjön több fajnév
                $var = preg_replace("/[+]/","",$var);
                $var = preg_replace("/[_]/","",$var);
                $var = preg_replace("/[ ]/","",$var);
                $a[] = quote(strtolower($var));
            }
            $taxon_table = sprintf("%s_taxon",$table);
            $cmd = sprintf("SELECT lang FROM %s WHERE LOWER(meta) IN (%s)",$taxon_table,implode(",",$a));
            $res = pg_query($ID,$cmd);
            $row=pg_fetch_assoc($res);

            $species_array = $st_col['ALTERN_C'];
            if (!count($species_array)) $request = $st_col['SPECIES_C'];
            else $request = array_search($row['lang'],$species_array);
            $cmd = sprintf("SELECT %s FROM \"public\".\"%s\" WHERE replace(lower(%s),' ','') IN (%s)",$obm_columns,$table,$request,implode(",",$a));
        } else {
            // normal data query
            if (in_array($clmn,glue($table)) or $clmn === 1) {
                
                if (preg_match("/(\d+):(\d+)/",$value,$m)) {
                    $value = range($m[1],$m[2]);
                }
                if (!is_array($value)) $value = array($value);

                foreach ($value as $var) {
                    $a[] = quote($var);
                }
                $WHERE = sprintf("\"%s\" IN (%s)",$clmn,implode(",",$a));
                
                // ask all data from the database
                if ($clmn === 1) $WHERE = "1=1";

                $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM ( SELECT %s FROM \"public\".\"%s\" %s WHERE %s %s ) t",$obm_columns,$table,$rule_join,$WHERE,$rules);
                //$cmd = sprintf("select row_to_json(t) from ( SELECT %s FROM \"public\".\"%s\" WHERE %s ) t",$obm_columns,$table,$WHERE);
            } else
                # no logging to the user
                log_action('Illegal query attempt',__FILE__,__LINE__);
        }
        return common_message('ok',$cmd);
    } else {
        return common_message('error','Query columns from table '."$table".' failed.');
    }
    return common_message('fail','Unexpected event');
}

?>
